<?php
return [

"roles" =>
[
    "role_creator" => 'CookieMonster'
],


"access_type" =>
[
    'listing'   => 'access_listing',
    'create'    => 'access_create',
    'show'      => 'access_show',
    'edit'      => 'access_edit',
    'delete'    => 'access_delete',
],

];
