<?php
return [

  "message" =>
  [
      "success" =>
      [
        "read" => 'Data Retrieved successfully',
        "insert" => 'Data successfully INSERTED into Database',
        "update" => 'Data successfully UPDATED into Database',
        "delete" => 'Data has been successfully (SOFT) DELETED from Database.',
      ],
      "no_access" => 'You dont have Access to this Module or Function in the Module. Please Contact Administrator.',
      "error" =>
      [
        "invalid_data" => "The given data was invalid.",
      ],
      "delete" => 'Data Successfully Change to Deleted Status.',
  ],

  "status" =>
  [
    "success" => 200,
    "no_access" => 403,
    "error" => 422,
  ],


];
