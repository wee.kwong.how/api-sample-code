-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: semcorp_db_01
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_log_documents`
--

DROP TABLE IF EXISTS `audit_log_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_log_documents` (
  `audit_log_id` bigint NOT NULL AUTO_INCREMENT,
  `audit_table` varchar(100) DEFAULT NULL,
  `audit_column` varchar(100) DEFAULT NULL,
  `audit_data_id` varchar(100) DEFAULT NULL,
  `audit_changed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `audit_change_by` varchar(100) DEFAULT NULL,
  `audit_old_value` varchar(5000) DEFAULT NULL,
  `audit_new_value` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`audit_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_log_documents`
--

LOCK TABLES `audit_log_documents` WRITE;
/*!40000 ALTER TABLE `audit_log_documents` DISABLE KEYS */;
INSERT INTO `audit_log_documents` VALUES (1,'site_documents','req_approval_project_owner','1','2022-01-17 08:52:23','5','1','0'),(2,'site_documents','req_approval_project_engineer','1','2022-01-17 08:52:23','5','1','0'),(3,'site_documents','req_approval_engineer','1','2022-01-17 08:52:23','5','1','0'),(4,'site_documents','req_approval_qa_qc','1','2022-01-17 08:52:23','5','1','0'),(5,'site_documents','req_approval_safety','1','2022-01-17 08:52:23','5','1','0'),(6,'site_documents','req_approval_onm','1','2022-01-17 08:52:23','5','1','0'),(7,'site_documents','req_approval_planner','1','2022-01-17 08:52:23','5','1','0'),(8,'site_documents','req_approval_purchasing','1','2022-01-17 08:52:23','5','1','0'),(9,'site_documents','req_approval_admin','1','2022-01-17 08:52:23','5','1','0'),(10,'site_documents','document_mandatory','1','2022-01-17 09:17:08','5','1','0'),(11,'site_documents','document_mandatory','1','2022-01-17 09:18:27','5','0','1'),(12,'project_documents','document_mandatory','1','2022-01-17 16:10:12','5','1','0'),(13,'project_documents','document_mandatory','2','2022-01-17 16:10:18','5','1','0'),(14,'project_documents','document_mandatory','1','2022-01-17 16:10:24','5','0','1'),(15,'project_documents','document_mandatory','2','2022-01-17 16:10:28','5','0','1'),(16,'site_documents','req_approval_project_owner','7','2022-01-19 07:38:18','5','1','0'),(17,'site_documents','req_approval_project_engineer','7','2022-01-19 07:38:18','5','1','0'),(18,'site_documents','req_approval_engineer','7','2022-01-19 07:38:18','5','1','0'),(19,'site_documents','req_approval_qa_qc','7','2022-01-19 07:38:18','5','1','0'),(20,'site_documents','req_approval_safety','7','2022-01-19 07:38:18','5','1','0'),(21,'site_documents','req_approval_onm','7','2022-01-19 07:38:18','5','1','0'),(22,'site_documents','req_approval_planner','7','2022-01-19 07:38:18','5','1','0'),(23,'site_documents','req_approval_purchasing','7','2022-01-19 07:38:18','5','1','0'),(24,'site_documents','req_approval_admin','7','2022-01-19 07:38:18','5','1','0'),(25,'site_document_approvals','approval_by','4','2022-01-19 07:39:56','5',NULL,'5'),(26,'site_document_approvals','code_by_approval','4','2022-01-19 07:39:56','5',NULL,'2'),(27,'site_document_approvals','approval_by','6','2022-01-19 08:16:11','5',NULL,'5'),(28,'site_document_approvals','code_by_approval','6','2022-01-19 08:16:11','5',NULL,'1'),(29,'site_document_approvals','approval_by','7','2022-01-19 08:16:58','5',NULL,'5'),(30,'site_document_approvals','code_by_approval','7','2022-01-19 08:16:58','5',NULL,'1'),(31,'site_documents','recurring_end_date','13','2022-01-19 10:22:36','5','2022-01-19 00:00:00','2022-01-26 00:00:00'),(32,'site_documents','document_title','17','2022-01-19 13:13:47','5','1-doc-1','a-doc-1'),(33,'project_documents','document_title','11','2022-01-24 09:03:46','2','ifa-document','ifa-document-asb-on'),(34,'project_documents','req_approval_project_owner','11','2022-01-24 09:05:06','2','1','0'),(35,'project_documents','req_approval_project_manager','11','2022-01-24 09:05:06','2','1','0'),(36,'project_documents','req_approval_project_engineer','11','2022-01-24 09:05:06','2','1','0'),(37,'project_documents','req_approval_engineer','11','2022-01-24 09:05:06','2','1','0'),(38,'project_documents','req_approval_qa_qc','11','2022-01-24 09:05:06','2','1','0'),(39,'project_documents','req_approval_safety','11','2022-01-24 09:05:06','2','1','0'),(40,'project_documents','req_approval_onm','11','2022-01-24 09:05:06','2','1','0'),(41,'project_documents','req_approval_planner','11','2022-01-24 09:05:06','2','1','0'),(42,'project_documents','req_approval_purchasing','11','2022-01-24 09:05:06','2','1','0'),(43,'project_document_approvals','approval_by','21','2022-01-24 09:06:03','2',NULL,'2'),(44,'project_document_approvals','code_by_approval','21','2022-01-24 09:06:03','2',NULL,'3'),(45,'project_document_approvals','approval_comments','21','2022-01-24 09:06:03','2',NULL,'comment'),(46,'project_document_approvals','approval_attachments','21','2022-01-24 09:06:03','2',NULL,'21_1643015163_TH-Implementation Plan.xlsx'),(47,'project_document_approvals','approval_by','22','2022-01-24 09:06:26','2',NULL,'2'),(48,'project_document_approvals','code_by_approval','22','2022-01-24 09:06:26','2',NULL,'1'),(49,'project_document_approvals','approval_by','23','2022-01-24 09:07:18','2',NULL,'2'),(50,'project_document_approvals','code_by_approval','23','2022-01-24 09:07:18','2',NULL,'2'),(51,'project_document_approvals','approval_comments','23','2022-01-24 09:07:18','2',NULL,'ok but need update'),(52,'project_document_approvals','approval_by','24','2022-01-24 09:07:41','2',NULL,'2'),(53,'project_document_approvals','code_by_approval','24','2022-01-24 09:07:41','2',NULL,'2'),(54,'project_document_approvals','approval_comments','24','2022-01-24 09:07:41','2',NULL,'comment'),(55,'project_document_approvals','approval_by','25','2022-01-24 09:07:59','2',NULL,'2'),(56,'project_document_approvals','code_by_approval','25','2022-01-24 09:07:59','2',NULL,'1'),(57,'project_document_approvals','approval_by','26','2022-01-24 09:08:26','2',NULL,'2'),(58,'project_document_approvals','code_by_approval','26','2022-01-24 09:08:26','2',NULL,'3'),(59,'project_document_approvals','approval_comments','26','2022-01-24 09:08:26','2',NULL,'comment'),(60,'project_document_approvals','approval_attachments','26','2022-01-24 09:08:26','2',NULL,'26_1643015306_TH-Implementation Plan.xlsx'),(61,'project_document_approvals','approval_by','27','2022-01-24 09:08:49','2',NULL,'2'),(62,'project_document_approvals','code_by_approval','27','2022-01-24 09:08:49','2',NULL,'1'),(63,'project_document_approvals','approval_by','28','2022-01-24 09:09:31','2',NULL,'2'),(64,'project_document_approvals','code_by_approval','28','2022-01-24 09:09:31','2',NULL,'2'),(65,'project_document_approvals','approval_comments','28','2022-01-24 09:09:31','2',NULL,'ok but better check again'),(66,'project_document_approvals','approval_by','29','2022-01-24 09:09:45','2',NULL,'2'),(67,'project_document_approvals','code_by_approval','29','2022-01-24 09:09:45','2',NULL,'1');
/*!40000 ALTER TABLE `audit_log_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_log_projects`
--

DROP TABLE IF EXISTS `audit_log_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_log_projects` (
  `audit_log_id` bigint NOT NULL AUTO_INCREMENT,
  `audit_table` varchar(100) DEFAULT NULL,
  `audit_column` varchar(100) DEFAULT NULL,
  `audit_data_id` varchar(100) DEFAULT NULL,
  `audit_changed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `audit_change_by` varchar(100) DEFAULT NULL,
  `audit_old_value` varchar(5000) DEFAULT NULL,
  `audit_new_value` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`audit_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_log_projects`
--

LOCK TABLES `audit_log_projects` WRITE;
/*!40000 ALTER TABLE `audit_log_projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_log_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_log_setups`
--

DROP TABLE IF EXISTS `audit_log_setups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_log_setups` (
  `audit_log_id` bigint NOT NULL AUTO_INCREMENT,
  `audit_table` varchar(100) DEFAULT NULL,
  `audit_column` varchar(100) DEFAULT NULL,
  `audit_data_id` varchar(100) DEFAULT NULL,
  `audit_changed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `audit_change_by` varchar(100) DEFAULT NULL,
  `audit_old_value` varchar(5000) DEFAULT NULL,
  `audit_new_value` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`audit_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_log_setups`
--

LOCK TABLES `audit_log_setups` WRITE;
/*!40000 ALTER TABLE `audit_log_setups` DISABLE KEYS */;
INSERT INTO `audit_log_setups` VALUES (1,'users','role_id','3','2022-01-06 13:34:55','1','18','15'),(2,'users','password','3','2022-01-06 13:34:55','1','$2y$10$B1WOwRRVKTmFpGF2lzwe1ugKN969/OtoDnXg.cKP6R5bYzysQ5Q7W','$2y$10$7vLPtWOd2mG0tyrjElBQweQagaAamPgDNtYSmTaT8/Ddf52e.QXIW'),(3,'users','password','3','2022-01-06 13:41:05','1','$2y$10$7vLPtWOd2mG0tyrjElBQweQagaAamPgDNtYSmTaT8/Ddf52e.QXIW','$2y$10$z2s4wcrRRCru09.fH3CPB.5a3T61gZ5sRyfnmXDzzWIlcfKx94gFS'),(4,'users','customer_id','1','2022-01-06 13:41:47','1','1',NULL),(5,'users','password','1','2022-01-06 13:41:47','1','$2y$10$dJeRJFaBCzn3KnyNlt9VZONx2RuhPZ2T8MKUISCkKTDmSeEqDXz8O','$2y$10$UzqYZi7ltwVt60zcOv1waOuN/mmrsnh/WWi2LSOQM3wmeYGAvtmGu'),(6,'users','password','2','2022-01-06 13:42:04','1','$2y$10$bvm/SiD//b8kJlePmY2eEOQaGId0CnW1XOaCGOpmlgTbH.EQ4F06C','$2y$10$737DBEMPZYJkB2HJzoAbKervt1xeMNxyalLwRQ2/rKb0OL0EuKVzq'),(7,'users','password','3','2022-01-06 13:42:14','1','$2y$10$z2s4wcrRRCru09.fH3CPB.5a3T61gZ5sRyfnmXDzzWIlcfKx94gFS','$2y$10$HJyqya1K.rmqzSVTK9v9yO1gFtq18y8EwxPxtolZIYRS/SB3dvN1e'),(8,'users','role_id','3','2022-01-06 13:43:14','2','15','10'),(9,'users','password','3','2022-01-06 13:43:14','2','$2y$10$HJyqya1K.rmqzSVTK9v9yO1gFtq18y8EwxPxtolZIYRS/SB3dvN1e','$2y$10$U6qi3dIdmuZ0d27zDXpQQ.G3UZgQbD.NHOPRj3ylGt.3HkUMmOxxi'),(10,'users','password','3','2022-01-06 13:45:10','2','$2y$10$U6qi3dIdmuZ0d27zDXpQQ.G3UZgQbD.NHOPRj3ylGt.3HkUMmOxxi','$2y$10$u1god0vEXj1H/2Knb8hwYO4V2H.M/.4M8gPwJeAzaYtRfDxZ8EB/K'),(11,'users','password','3','2022-01-06 13:46:50','2','$2y$10$u1god0vEXj1H/2Knb8hwYO4V2H.M/.4M8gPwJeAzaYtRfDxZ8EB/K','$2y$10$WhYkxB5FKH2tz78Tu4QB0u/.PJUlVeaHuj0FFOfiIpICZYjJ17oPC'),(12,'users','password','3','2022-01-06 13:47:49','2','$2y$10$WhYkxB5FKH2tz78Tu4QB0u/.PJUlVeaHuj0FFOfiIpICZYjJ17oPC','$2y$10$niAbWs5pUFu7pk9totQ9Y.vXvNYCEHLnDMtYv0KC28Ok.Cqo0CZZG'),(13,'users','password','3','2022-01-06 13:50:10','2','$2y$10$niAbWs5pUFu7pk9totQ9Y.vXvNYCEHLnDMtYv0KC28Ok.Cqo0CZZG','$2y$10$2nEfF6Bf39RYmR6oxDNru.n4aZxMO6HDIxtoc1..a496XThQ7KnT6'),(14,'users','role_id','3','2022-01-06 13:50:29','2','10','15');
/*!40000 ALTER TABLE `audit_log_setups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_log_tasks`
--

DROP TABLE IF EXISTS `audit_log_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_log_tasks` (
  `audit_log_id` bigint NOT NULL AUTO_INCREMENT,
  `audit_table` varchar(100) DEFAULT NULL,
  `audit_column` varchar(100) DEFAULT NULL,
  `audit_data_id` varchar(100) DEFAULT NULL,
  `audit_changed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `audit_change_by` varchar(100) DEFAULT NULL,
  `audit_old_value` varchar(5000) DEFAULT NULL,
  `audit_new_value` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`audit_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_log_tasks`
--

LOCK TABLES `audit_log_tasks` WRITE;
/*!40000 ALTER TABLE `audit_log_tasks` DISABLE KEYS */;
INSERT INTO `audit_log_tasks` VALUES (1,'itasks','assign_to_user','1','2022-01-06 14:21:18','5','2','3'),(2,'itasks','task_progress','1','2022-01-06 14:21:18','5','10','100'),(3,'itasks','status_id','1','2022-01-06 14:21:18','5','1','3'),(4,'itasks','status_id','1','2022-01-06 14:21:33','5','3','4'),(5,'project_itasks','task_description','1','2022-01-08 09:40:52','5',NULL,'null'),(6,'project_itasks','task_remarks','1','2022-01-08 09:40:52','5',NULL,'null'),(7,'project_itasks','task_progress','1','2022-01-08 09:40:52','5','0','100'),(8,'project_itasks','status_id','1','2022-01-08 09:40:52','5','1','3'),(9,'project_itasks','upload_attachment','1','2022-01-08 09:40:52','5',NULL,'1_1641634852_STRAY borderless number.xlsx'),(10,'project_itasks','task_description','1','2022-01-08 10:50:20','5','null',NULL),(11,'project_itasks','task_remarks','1','2022-01-08 10:50:20','5','null',NULL),(12,'project_itasks','upload_attachment','1','2022-01-08 10:50:20','5','1_1641634852_STRAY borderless number.xlsx','1_1641639020_TH-Implementation Plan.xlsx'),(13,'project_itasks','status_id','1','2022-01-08 10:50:37','5','3','4'),(14,'project_itasks','task_progress','2','2022-01-08 12:27:33','5','0','100'),(15,'project_itasks','status_id','2','2022-01-08 12:27:33','5','1','3'),(16,'project_itasks','upload_attachment','2','2022-01-08 12:27:33','5',NULL,'2_1641644853_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx'),(17,'project_itasks','upload_attachment','2','2022-01-08 12:27:53','5','2_1641644853_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx','2_1641644873_STRAY borderless number.xlsx'),(18,'project_itasks','status_id','2','2022-01-08 12:28:17','5','3','4'),(19,'project_itasks','status_id','3','2022-01-08 12:31:32','5','1','3'),(20,'project_itasks','upload_attachment','3','2022-01-08 12:31:32','5',NULL,'3_1641645092_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx'),(21,'project_itasks','upload_attachment','3','2022-01-08 12:31:51','5','3_1641645092_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx','3_1641645111_STRAY borderless number.xlsx'),(22,'project_itasks','upload_attachment','3','2022-01-08 12:35:47','5','3_1641645111_STRAY borderless number.xlsx','3_1641645347_STRAY borderless number.xlsx'),(23,'project_itasks','upload_attachment','3','2022-01-08 12:36:01','5','3_1641645347_STRAY borderless number.xlsx','3_1641645361_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx'),(24,'project_itasks','upload_attachment','3','2022-01-08 12:38:21','5','3_1641645361_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx','3_1641645501_TH-Implementation Plan.xlsx'),(25,'itasks','assign_to_user','2','2022-01-08 13:21:12','5','7','5'),(26,'itasks','contractor_id','2','2022-01-08 13:21:12','5','1',NULL),(27,'itasks','status_id','2','2022-01-08 13:21:12','5','1','3'),(28,'itasks','upload_attachment','2','2022-01-08 13:21:12','5',NULL,'2_1641648072_STRAY borderless number.xlsx'),(29,'itasks','upload_attachment','2','2022-01-08 13:23:47','5','2_1641648072_STRAY borderless number.xlsx','2_1641648227_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx'),(30,'project_itasks','upload_attachment','3','2022-01-08 13:24:08','5','3_1641645501_TH-Implementation Plan.xlsx','3_1641648248_Section C - RFP TH REAL ESTATE CENTRALISED SYSTEM SOLUTION (THRECS)- Update 20210507.docx'),(31,'itasks','status_id','2','2022-01-08 13:24:24','5','3','4'),(32,'itasks','task_start_date','3','2022-01-14 11:02:23','5',NULL,'2022-01-14 00:00:00'),(33,'itasks','task_end_date','3','2022-01-14 11:02:23','5',NULL,'2022-01-14 00:00:00'),(34,'itasks','contractor_id','9','2022-01-16 04:06:07','5','1',NULL),(35,'itasks','task_progress','9','2022-01-16 04:06:07','5','0','100'),(36,'itasks','status_id','9','2022-01-16 04:06:07','5','3','4'),(37,'itasks','task_progress','8','2022-01-16 04:08:11','5','0','100'),(38,'itasks','status_id','8','2022-01-16 04:08:11','5','1','3'),(39,'project_itasks','task_progress','1','2022-01-16 05:41:25','5','0','100'),(40,'project_itasks','status_id','1','2022-01-16 05:41:25','5','1','3'),(41,'project_itasks','status_id','1','2022-01-16 05:41:43','5','3','4');
/*!40000 ALTER TABLE `audit_log_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_log_templates`
--

DROP TABLE IF EXISTS `audit_log_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_log_templates` (
  `audit_log_id` bigint NOT NULL AUTO_INCREMENT,
  `audit_table` varchar(100) DEFAULT NULL,
  `audit_column` varchar(100) DEFAULT NULL,
  `audit_data_id` varchar(100) DEFAULT NULL,
  `audit_changed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `audit_change_by` varchar(100) DEFAULT NULL,
  `audit_old_value` varchar(5000) DEFAULT NULL,
  `audit_new_value` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`audit_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_log_templates`
--

LOCK TABLES `audit_log_templates` WRITE;
/*!40000 ALTER TABLE `audit_log_templates` DISABLE KEYS */;
INSERT INTO `audit_log_templates` VALUES (1,'project_document_template_details','req_approval_onm','1','2022-01-06 15:18:33','5','1','0'),(2,'project_document_template_details','req_approval_onm','1','2022-01-06 15:18:38','5','0','1');
/*!40000 ALTER TABLE `audit_log_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `check_table`
--

DROP TABLE IF EXISTS `check_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `check_table` (
  `idcheck_table` int NOT NULL AUTO_INCREMENT,
  `aaa` varchar(45) DEFAULT NULL,
  `bbb` varchar(45) DEFAULT NULL,
  `ccc` varchar(45) DEFAULT NULL,
  `ddd` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcheck_table`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `check_table`
--

LOCK TABLES `check_table` WRITE;
/*!40000 ALTER TABLE `check_table` DISABLE KEYS */;
INSERT INTO `check_table` VALUES (1,'before','1','0','2'),(2,'after','1','0','2'),(3,'before-completed','1','0','1'),(4,'before','1','2','2'),(5,'after','1','2','0'),(6,'before-completed','1','1','1'),(7,'after-completed','1','1','0'),(8,'before','1','2','2'),(9,'after','1','2','0'),(10,'before-completed','1','1','1'),(11,'after-completed','1','1','0'),(12,'1x',NULL,NULL,NULL),(13,'before','6','1','3'),(14,'after','6','1','2'),(15,'1x',NULL,NULL,NULL),(16,'1x',NULL,NULL,NULL),(17,'1x',NULL,NULL,NULL),(18,'before','1','0','4'),(19,'after','1','0','4'),(20,'1x',NULL,NULL,NULL),(21,'1x',NULL,NULL,NULL),(22,'1x',NULL,NULL,NULL),(23,'before','2','0','2'),(24,'after','2','0','2'),(25,'1x',NULL,NULL,NULL),(26,'1x',NULL,NULL,NULL),(27,'1x',NULL,NULL,NULL),(28,'before','4','0','2'),(29,'after','4','0','2'),(30,'1x',NULL,NULL,NULL),(31,'1x',NULL,NULL,NULL),(32,'1x',NULL,NULL,NULL),(33,'before','5','0','2'),(34,'after','5','0','2'),(35,'1x',NULL,NULL,NULL),(36,'1x',NULL,NULL,NULL),(37,'1x',NULL,NULL,NULL),(38,'1x',NULL,NULL,NULL),(39,'before','6','0','2'),(40,'after','6','0','2'),(41,'1x',NULL,NULL,NULL),(42,'1x',NULL,NULL,NULL),(43,'1x',NULL,NULL,NULL),(44,'2x',NULL,NULL,NULL),(45,'1x',NULL,NULL,NULL),(46,'before','1','0','2'),(47,'after','1','0','2'),(48,'1x',NULL,NULL,NULL),(49,'1x',NULL,NULL,NULL),(50,'1x',NULL,NULL,NULL),(51,'before','1','0','2'),(52,'after','1','0','2'),(53,'1x',NULL,NULL,NULL),(54,'1x',NULL,NULL,NULL),(55,'1x',NULL,NULL,NULL),(56,'before','1','0','2'),(57,'after','1','0','2'),(58,'1x',NULL,NULL,NULL),(59,'2x',NULL,NULL,NULL),(60,'1x',NULL,NULL,NULL),(61,'1x',NULL,NULL,NULL),(62,'before','2','0','2'),(63,'after','2','0','2'),(64,'1x',NULL,NULL,NULL),(65,'1x',NULL,NULL,NULL),(66,'1x',NULL,NULL,NULL),(67,'1x',NULL,NULL,NULL),(68,'1x',NULL,NULL,NULL),(69,'1x',NULL,NULL,NULL),(70,'1x',NULL,NULL,NULL),(71,'1x',NULL,NULL,NULL),(72,'1x',NULL,NULL,NULL),(73,'1x',NULL,NULL,NULL),(74,'1x',NULL,NULL,NULL),(75,'1x',NULL,NULL,NULL),(76,'1x',NULL,NULL,NULL),(77,'1x',NULL,NULL,NULL),(78,'1x',NULL,NULL,NULL),(79,'1x',NULL,NULL,NULL),(80,'1x',NULL,NULL,NULL),(81,'1x',NULL,NULL,NULL),(82,'1x',NULL,NULL,NULL),(83,'1x',NULL,NULL,NULL),(84,'1x',NULL,NULL,NULL),(85,'1x',NULL,NULL,NULL),(86,'1x',NULL,NULL,NULL),(87,'1x',NULL,NULL,NULL),(88,'1x',NULL,NULL,NULL),(89,'1x',NULL,NULL,NULL),(90,'1x',NULL,NULL,NULL),(91,'1x',NULL,NULL,NULL),(92,'1x',NULL,NULL,NULL),(93,'1x',NULL,NULL,NULL),(94,'1x',NULL,NULL,NULL),(95,'1x',NULL,NULL,NULL),(96,'1x',NULL,NULL,NULL),(97,'1x',NULL,NULL,NULL),(98,'1x',NULL,NULL,NULL),(99,'1x',NULL,NULL,NULL),(100,'1x',NULL,NULL,NULL),(101,'1x',NULL,NULL,NULL),(102,'1x',NULL,NULL,NULL),(103,'1x',NULL,NULL,NULL),(104,'1x',NULL,NULL,NULL),(105,'1x',NULL,NULL,NULL),(106,'1x',NULL,NULL,NULL),(107,'1x',NULL,NULL,NULL),(108,'1x',NULL,NULL,NULL),(109,'1x',NULL,NULL,NULL),(110,'1x',NULL,NULL,NULL),(111,'1x',NULL,NULL,NULL),(112,'1x',NULL,NULL,NULL),(113,'1x',NULL,NULL,NULL),(114,'1x',NULL,NULL,NULL),(115,'1x',NULL,NULL,NULL),(116,'1x',NULL,NULL,NULL),(117,'1x',NULL,NULL,NULL),(118,'1x',NULL,NULL,NULL),(119,'1x',NULL,NULL,NULL),(120,'1x',NULL,NULL,NULL),(121,'1x',NULL,NULL,NULL),(122,'1x',NULL,NULL,NULL),(123,'1x',NULL,NULL,NULL),(124,'1x',NULL,NULL,NULL),(125,'1x',NULL,NULL,NULL),(126,'1x',NULL,NULL,NULL),(127,'1x',NULL,NULL,NULL),(128,'1x',NULL,NULL,NULL),(129,'1x',NULL,NULL,NULL),(130,'1x',NULL,NULL,NULL),(131,'1x',NULL,NULL,NULL),(132,'1x',NULL,NULL,NULL),(133,'1x',NULL,NULL,NULL),(134,'1x',NULL,NULL,NULL),(135,'1x',NULL,NULL,NULL),(136,'1x',NULL,NULL,NULL),(137,'1x',NULL,NULL,NULL),(138,'1x',NULL,NULL,NULL),(139,'1x',NULL,NULL,NULL),(140,'1x',NULL,NULL,NULL),(141,'1x',NULL,NULL,NULL),(142,'1x',NULL,NULL,NULL),(143,'1x',NULL,NULL,NULL),(144,'1x',NULL,NULL,NULL),(145,'1x',NULL,NULL,NULL),(146,'1x',NULL,NULL,NULL),(147,'1x',NULL,NULL,NULL),(148,'1x',NULL,NULL,NULL),(149,'1x',NULL,NULL,NULL),(150,'1x',NULL,NULL,NULL),(151,'1x',NULL,NULL,NULL),(152,'1x',NULL,NULL,NULL),(153,'1x',NULL,NULL,NULL),(154,'1x',NULL,NULL,NULL),(155,'1x',NULL,NULL,NULL),(156,'1x',NULL,NULL,NULL),(157,'1x',NULL,NULL,NULL),(158,'1x',NULL,NULL,NULL),(159,'1x',NULL,NULL,NULL),(160,'1x',NULL,NULL,NULL),(161,'1x',NULL,NULL,NULL),(162,'1x',NULL,NULL,NULL),(163,'1x',NULL,NULL,NULL),(164,'1x',NULL,NULL,NULL),(165,'1x',NULL,NULL,NULL),(166,'1x',NULL,NULL,NULL),(167,'1x',NULL,NULL,NULL),(168,'1x',NULL,NULL,NULL),(169,'1x',NULL,NULL,NULL),(170,'1x',NULL,NULL,NULL),(171,'1x',NULL,NULL,NULL),(172,'1x',NULL,NULL,NULL),(173,'1x',NULL,NULL,NULL),(174,'before','6','0','4'),(175,'after','6','0','4'),(176,'1x',NULL,NULL,NULL),(177,'1x',NULL,NULL,NULL),(178,'1x',NULL,NULL,NULL),(179,'1x',NULL,NULL,NULL),(180,'1x',NULL,NULL,NULL),(181,'before','6','0','0'),(182,'after','6','0','0'),(183,'1x',NULL,NULL,NULL),(184,'1x',NULL,NULL,NULL),(185,'1x',NULL,NULL,NULL),(186,'1x',NULL,NULL,NULL),(187,'before','11','0','0'),(188,'after','11','0','0'),(189,'1x',NULL,NULL,NULL),(190,'1x',NULL,NULL,NULL),(191,'1x',NULL,NULL,NULL),(192,'1x',NULL,NULL,NULL),(193,'1x',NULL,NULL,NULL),(194,'1x',NULL,NULL,NULL);
/*!40000 ALTER TABLE `check_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_global`
--

DROP TABLE IF EXISTS `config_global`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `config_global` (
  `config_global_id` bigint NOT NULL AUTO_INCREMENT,
  `config_global_info` varchar(100) DEFAULT NULL,
  `config_global_value` varchar(300) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`config_global_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_global`
--

LOCK TABLES `config_global` WRITE;
/*!40000 ALTER TABLE `config_global` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_global` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_types`
--

DROP TABLE IF EXISTS `config_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `config_types` (
  `config_type_id` bigint NOT NULL AUTO_INCREMENT,
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `config_information` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`config_type_id`),
  KEY `FK_ref_config_type_created_by` (`created_by`),
  CONSTRAINT `FK_ref_config_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_types`
--

LOCK TABLES `config_types` WRITE;
/*!40000 ALTER TABLE `config_types` DISABLE KEYS */;
INSERT INTO `config_types` VALUES (1,'SaaS','Full SaaS Model Configuration',1,'2021-06-13 19:54:01','2021-06-13 22:29:03'),(2,'iMaintain','iMaintain Configuration Model',1,'2021-06-13 19:54:01','2021-06-13 22:29:03'),(3,'New Config','New Config Configuration',1,'2021-06-13 22:40:06','2021-06-13 14:41:24');
/*!40000 ALTER TABLE `config_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_assigns`
--

DROP TABLE IF EXISTS `daily_assigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_assigns` (
  `daily_assign_id` bigint NOT NULL AUTO_INCREMENT,
  `assign_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `assign_to_user_id` bigint DEFAULT NULL,
  `assign_module_code` varchar(100) DEFAULT NULL,
  `project_id` bigint DEFAULT NULL,
  `group_id` bigint DEFAULT NULL,
  `site_id` bigint DEFAULT NULL,
  `assign_to_id` bigint DEFAULT NULL,
  `assign_to_title` varchar(300) DEFAULT NULL,
  `assign_link` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `completed_flag` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`daily_assign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_assigns`
--

LOCK TABLES `daily_assigns` WRITE;
/*!40000 ALTER TABLE `daily_assigns` DISABLE KEYS */;
INSERT INTO `daily_assigns` VALUES (1,'2022-01-06 06:12:24',3,'PROJECT',1,NULL,NULL,1,'project-1','projectDashboard','2022-01-06 06:12:24','2022-01-06 06:12:24',0),(2,'2022-01-06 06:12:24',6,'PROJECT',1,NULL,NULL,1,'project-1','projectDashboard','2022-01-06 06:12:24','2022-01-06 06:12:24',0),(3,'2022-01-06 06:13:22',5,'PROJECT GROUP',1,1,NULL,1,'p1-group-1','projectGroupDashboard','2022-01-06 06:13:22','2022-01-06 06:13:22',0),(4,'2022-01-06 06:14:20',5,'PROJECT GROUP',1,2,NULL,2,'p1-group-2','projectGroupDashboard','2022-01-06 06:14:20','2022-01-06 06:14:20',0),(5,'2022-01-06 06:18:16',2,'SITE TASK',1,2,1,1,'task-1','taskEdit','2022-01-06 06:18:16','2022-01-06 06:18:16',0),(6,'2022-01-06 06:21:18',3,'SITE TASK',1,2,1,1,'task-1','taskEdit','2022-01-06 06:21:18','2022-01-06 06:21:18',0),(7,'2022-01-06 06:46:08',7,'SITE TASK',1,2,1,2,'task-2','taskEdit','2022-01-06 06:46:08','2022-01-06 06:46:08',0),(8,'2022-01-06 06:46:45',7,'SITE DOCUMENT',1,2,1,1,'ifa-document-1','siteDocumentEdit','2022-01-06 06:46:45','2022-01-06 06:46:45',0),(9,'2022-01-08 16:27:08',5,'PROJECT TASK',1,NULL,NULL,1,'task-1','projectTaskEditFrProject','2022-01-08 16:27:08','2022-01-08 16:27:08',0),(10,'2022-01-08 17:25:49',1,'PROJECT TASK',1,NULL,NULL,2,'Project-Document-006-004','projectTaskEditFrProject','2022-01-08 17:25:49','2022-01-08 17:25:49',0),(11,'2022-01-08 20:31:13',5,'PROJECT TASK',1,NULL,NULL,3,'task-3','projectTaskEditFrProject','2022-01-08 20:31:13','2022-01-08 20:31:13',0),(12,'2022-01-08 21:21:12',5,'SITE TASK',1,2,1,2,'task-2','taskEdit','2022-01-08 21:21:12','2022-01-08 21:21:12',0),(13,'2022-01-10 15:09:41',5,'SITE DOCUMENT',1,1,2,2,'site-document-1','siteDocumentEdit','2022-01-10 15:09:41','2022-01-10 15:09:41',0),(14,'2022-01-10 15:09:41',5,'SITE TASK',1,1,2,3,'task-1','taskEdit','2022-01-10 15:09:41','2022-01-10 15:09:41',0),(15,'2022-01-10 16:43:37',5,'SITE DOCUMENT',1,1,3,3,'site-document-1','siteDocumentEdit','2022-01-10 16:43:37','2022-01-10 16:43:37',0),(16,'2022-01-10 16:43:37',5,'SITE TASK',1,1,3,4,'task-1','taskEdit','2022-01-10 16:43:37','2022-01-10 16:43:37',0),(17,'2022-01-10 17:06:35',5,'SITE DOCUMENT',1,2,4,4,'site-document-1','siteDocumentEdit','2022-01-10 17:06:35','2022-01-10 17:06:35',0),(18,'2022-01-10 17:06:35',5,'SITE TASK',1,2,4,5,'task-1','taskEdit','2022-01-10 17:06:35','2022-01-10 17:06:35',0),(19,'2022-01-11 01:39:11',7,'SITE DOCUMENT',1,2,1,5,'ifa-recurring','siteDocumentEdit','2022-01-11 01:39:11','2022-01-11 01:39:11',0),(20,'2022-01-15 14:21:38',3,'PROJECT',1,NULL,NULL,1,'project-1','projectDashboard','2022-01-15 14:21:38','2022-01-15 14:21:38',0),(21,'2022-01-15 14:21:38',6,'PROJECT',1,NULL,NULL,1,'project-1','projectDashboard','2022-01-15 14:21:38','2022-01-15 14:21:38',0),(22,'2022-01-15 14:21:57',5,'PROJECT GROUP',1,1,NULL,1,'p1-group-1','projectGroupDashboard','2022-01-15 14:21:57','2022-01-15 14:21:57',0),(23,'2022-01-15 14:41:37',7,'SITE TASK',1,1,1,1,'task-1','taskEdit','2022-01-15 14:41:37','2022-01-15 14:41:37',0),(24,'2022-01-15 16:38:48',5,'SITE TASK',1,1,1,2,'task-2','taskEdit','2022-01-15 16:38:48','2022-01-15 16:38:48',0),(25,'2022-01-15 16:41:41',5,'SITE TASK',1,1,1,3,'task-3','taskEdit','2022-01-15 16:41:41','2022-01-15 16:41:41',0),(26,'2022-01-15 16:53:37',5,'SITE TASK',1,1,1,4,'task-1','taskEdit','2022-01-15 16:53:37','2022-01-15 16:53:37',0),(27,'2022-01-15 17:12:53',2,'SITE TASK',1,1,1,5,'task-4','taskEdit','2022-01-15 17:12:53','2022-01-15 17:12:53',0),(28,'2022-01-15 17:14:53',2,'SITE TASK',1,1,1,6,'task-5','taskEdit','2022-01-15 17:14:53','2022-01-15 17:14:53',0),(29,'2022-01-15 18:28:15',2,'SITE TASK',1,1,1,7,'task-6','taskEdit','2022-01-15 18:28:15','2022-01-15 18:28:15',0),(30,'2022-01-15 18:28:35',2,'SITE TASK',1,1,1,8,'task-7','taskEdit','2022-01-15 18:28:35','2022-01-15 18:28:35',0),(31,'2022-01-16 12:05:30',2,'SITE TASK',1,1,1,9,'task-8','taskEdit','2022-01-16 12:05:30','2022-01-16 12:05:30',0),(32,'2022-01-16 12:08:33',5,'SITE TASK',1,1,1,10,'task-9','taskEdit','2022-01-16 12:08:33','2022-01-16 12:08:33',0),(33,'2022-01-16 12:17:21',5,'SITE TASK',1,1,1,11,'task-9','taskEdit','2022-01-16 12:17:21','2022-01-16 12:17:21',0),(34,'2022-01-16 12:23:17',5,'SITE TASK',1,1,1,12,'task-10','taskEdit','2022-01-16 12:23:17','2022-01-16 12:23:17',0),(35,'2022-01-16 12:23:37',5,'SITE TASK',1,1,1,13,'task-11','taskEdit','2022-01-16 12:23:37','2022-01-16 12:23:37',0),(36,'2022-01-16 12:29:27',5,'SITE TASK',1,1,1,14,'task-12','taskEdit','2022-01-16 12:29:27','2022-01-16 12:29:27',0),(37,'2022-01-16 12:29:41',5,'SITE TASK',1,1,1,15,'task-13','taskEdit','2022-01-16 12:29:41','2022-01-16 12:29:41',0),(38,'2022-01-16 12:29:58',5,'SITE TASK',1,1,1,16,'task-14','taskEdit','2022-01-16 12:29:58','2022-01-16 12:29:58',0),(39,'2022-01-16 12:38:46',5,'SITE TASK',1,1,1,17,'task-15','taskEdit','2022-01-16 12:38:46','2022-01-16 12:38:46',0),(40,'2022-01-16 12:39:21',5,'SITE TASK',1,1,1,18,'task-16','taskEdit','2022-01-16 12:39:21','2022-01-16 12:39:21',0),(41,'2022-01-16 12:42:48',5,'SITE TASK',1,1,1,19,'task-9','taskEdit','2022-01-16 12:42:48','2022-01-16 12:42:48',0),(42,'2022-01-16 12:43:08',5,'SITE TASK',1,1,1,20,'task-10','taskEdit','2022-01-16 12:43:08','2022-01-16 12:43:08',0),(43,'2022-01-16 12:48:10',5,'SITE TASK',1,1,1,21,'task-11','taskEdit','2022-01-16 12:48:10','2022-01-16 12:48:10',0),(44,'2022-01-16 12:48:36',5,'SITE TASK',1,1,1,22,'task-12','taskEdit','2022-01-16 12:48:36','2022-01-16 12:48:36',0),(45,'2022-01-16 13:14:50',5,'SITE TASK',1,1,1,23,'task-1','taskEdit','2022-01-16 13:14:50','2022-01-16 13:14:50',0),(46,'2022-01-16 13:20:42',5,'SITE TASK',1,1,1,24,'task-1','taskEdit','2022-01-16 13:20:42','2022-01-16 13:20:42',0),(47,'2022-01-16 13:40:55',5,'PROJECT TASK',1,NULL,NULL,1,'project-task-1','projectTaskEditFrProject','2022-01-16 13:40:55','2022-01-16 13:40:55',0),(48,'2022-01-16 13:41:14',5,'PROJECT TASK',1,NULL,NULL,2,'project-task-2','projectTaskEditFrProject','2022-01-16 13:41:14','2022-01-16 13:41:14',0),(49,'2022-01-16 13:41:59',5,'PROJECT TASK',1,NULL,NULL,3,'project-task-3','projectTaskEditFrProject','2022-01-16 13:41:59','2022-01-16 13:41:59',0),(50,'2022-01-16 13:56:48',5,'PROJECT TASK',1,NULL,NULL,4,'project-task-4','projectTaskEditFrProject','2022-01-16 13:56:48','2022-01-16 13:56:48',0),(51,'2022-01-17 16:29:07',5,'SITE DOCUMENT',1,1,1,1,'document-1','siteDocumentEdit','2022-01-17 16:29:07','2022-01-17 16:29:07',0),(52,'2022-01-17 16:52:45',5,'SITE DOCUMENT',1,1,1,2,'document-2','siteDocumentEdit','2022-01-17 16:52:45','2022-01-17 16:52:45',0),(53,'2022-01-17 17:20:45',5,'SITE DOCUMENT',1,1,1,3,'document-3','siteDocumentEdit','2022-01-17 17:20:45','2022-01-17 17:20:45',0),(54,'2022-01-17 17:23:00',5,'SITE DOCUMENT',1,1,1,4,'document-4','siteDocumentEdit','2022-01-17 17:23:00','2022-01-17 17:23:00',0),(55,'2022-01-17 23:42:02',5,'SITE DOCUMENT',1,1,1,5,'document-1','siteDocumentEdit','2022-01-17 23:42:02','2022-01-17 23:42:02',0),(56,'2022-01-17 23:43:42',5,'SITE DOCUMENT',1,1,1,6,'document-1','siteDocumentEdit','2022-01-17 23:43:42','2022-01-17 23:43:42',0),(57,'2022-01-18 00:08:40',5,'PROJECT DOCUMENT',1,NULL,NULL,1,'project-document-1','projectDocumentEditFrProject','2022-01-18 00:08:40','2022-01-18 00:08:40',0),(58,'2022-01-18 00:09:14',5,'PROJECT DOCUMENT',1,NULL,NULL,2,'project-document-2','projectDocumentEditFrProject','2022-01-18 00:09:14','2022-01-18 00:09:14',0),(59,'2022-01-18 00:10:47',5,'PROJECT DOCUMENT',1,NULL,NULL,3,'project-document-3','projectDocumentEditFrProject','2022-01-18 00:10:47','2022-01-18 00:10:47',0),(60,'2022-01-18 00:11:10',5,'PROJECT DOCUMENT',1,NULL,NULL,4,'project-document-4','projectDocumentEditFrProject','2022-01-18 00:11:10','2022-01-18 00:11:10',0),(61,'2022-01-19 15:36:22',5,'SITE DOCUMENT',1,1,1,7,'document-1','siteDocumentEdit','2022-01-19 15:36:22','2022-01-19 15:36:22',0),(62,'2022-01-19 15:36:47',5,'SITE DOCUMENT',1,1,1,8,'document-2','siteDocumentEdit','2022-01-19 15:36:47','2022-01-19 15:36:47',0),(63,'2022-01-19 15:37:43',5,'SITE DOCUMENT',1,1,1,9,'document-3','siteDocumentEdit','2022-01-19 15:37:43','2022-01-19 15:37:43',0),(64,'2022-01-19 16:14:23',5,'SITE DOCUMENT',1,1,1,10,'document-4','siteDocumentEdit','2022-01-19 16:14:23','2022-01-19 16:14:23',0),(65,'2022-01-19 16:14:47',5,'SITE DOCUMENT',1,1,1,11,'document-5','siteDocumentEdit','2022-01-19 16:14:47','2022-01-19 16:14:47',0),(66,'2022-01-19 16:15:15',5,'SITE DOCUMENT',1,1,1,12,'document-6','siteDocumentEdit','2022-01-19 16:15:15','2022-01-19 16:15:15',0),(67,'2022-01-19 18:21:58',5,'SITE DOCUMENT',1,1,1,13,'document-7','siteDocumentEdit','2022-01-19 18:21:58','2022-01-19 18:21:58',0),(68,'2022-01-19 21:02:43',5,'SITE DOCUMENT',1,1,1,14,'n-doc-1','siteDocumentEdit','2022-01-19 21:02:43','2022-01-19 21:02:43',0),(69,'2022-01-19 21:03:01',5,'SITE DOCUMENT',1,1,1,15,'n-doc-2','siteDocumentEdit','2022-01-19 21:03:01','2022-01-19 21:03:01',0),(70,'2022-01-19 21:04:15',5,'SITE DOCUMENT',1,1,1,16,'n-doc-3','siteDocumentEdit','2022-01-19 21:04:15','2022-01-19 21:04:15',0),(71,'2022-01-19 21:13:25',5,'SITE DOCUMENT',1,1,1,17,'1-doc-1','siteDocumentEdit','2022-01-19 21:13:25','2022-01-19 21:13:25',0),(72,'2022-01-19 21:14:02',5,'SITE DOCUMENT',1,1,1,18,'a-doc-2','siteDocumentEdit','2022-01-19 21:14:02','2022-01-19 21:14:02',0),(73,'2022-01-19 21:14:25',5,'SITE DOCUMENT',1,1,1,19,'a-doc-3','siteDocumentEdit','2022-01-19 21:14:25','2022-01-19 21:14:25',0),(74,'2022-01-21 15:14:08',5,'PROJECT DOCUMENT',1,NULL,NULL,5,'doc-1','projectDocumentEditFrProject','2022-01-21 15:14:08','2022-01-21 15:14:08',0),(75,'2022-01-21 15:17:10',5,'PROJECT DOCUMENT',1,NULL,NULL,6,'doc-2','projectDocumentEditFrProject','2022-01-21 15:17:10','2022-01-21 15:17:10',0),(76,'2022-01-21 15:17:28',5,'PROJECT DOCUMENT',1,NULL,NULL,7,'doc-3','projectDocumentEditFrProject','2022-01-21 15:17:28','2022-01-21 15:17:28',0),(77,'2022-01-21 15:33:55',5,'PROJECT DOCUMENT',1,NULL,NULL,8,'doc-4','projectDocumentEditFrProject','2022-01-21 15:33:55','2022-01-21 15:33:55',0),(78,'2022-01-21 15:41:19',5,'PROJECT DOCUMENT',1,NULL,NULL,9,'doc-5','projectDocumentEditFrProject','2022-01-21 15:41:19','2022-01-21 15:41:19',0),(79,'2022-01-21 15:41:56',5,'PROJECT DOCUMENT',1,NULL,NULL,10,'dco-3','projectDocumentEditFrProject','2022-01-21 15:41:56','2022-01-21 15:41:56',0),(80,'2022-01-21 15:44:53',5,'SITE DOCUMENT',1,1,1,20,'doc-1','siteDocumentEdit','2022-01-21 15:44:53','2022-01-21 15:44:53',0),(81,'2022-01-24 17:02:52',3,'PROJECT',2,NULL,NULL,2,'project-2','projectDashboard','2022-01-24 17:02:52','2022-01-24 17:02:52',0),(82,'2022-01-24 17:02:52',6,'PROJECT',2,NULL,NULL,2,'project-2','projectDashboard','2022-01-24 17:02:52','2022-01-24 17:02:52',0),(83,'2022-01-24 17:02:52',2,'PROJECT DOCUMENT',2,NULL,NULL,11,'ifa-document','projectDocumentEditFrProject','2022-01-24 17:02:52','2022-01-24 17:02:52',0),(84,'2022-01-24 17:02:52',2,'PROJECT TASK',2,NULL,NULL,5,'task-1','projectTaskEditFrProject','2022-01-24 17:02:52','2022-01-24 17:02:52',0),(85,'2022-01-24 17:04:15',2,'PROJECT DOCUMENT',2,NULL,NULL,12,'ifa-document-asb-off','projectDocumentEditFrProject','2022-01-24 17:04:15','2022-01-24 17:04:15',0);
/*!40000 ALTER TABLE `daily_assigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_document_overdue`
--

DROP TABLE IF EXISTS `daily_document_overdue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_document_overdue` (
  `daily_document_overdue_id` bigint NOT NULL AUTO_INCREMENT,
  `for_date` date DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `total_overdue_below_30` int DEFAULT '0',
  `total_overdue_30_to_60` int DEFAULT '0',
  `total_overdue_60_to_90` int DEFAULT '0',
  `total_overdue_above_90` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_document_overdue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_document_overdue`
--

LOCK TABLES `daily_document_overdue` WRITE;
/*!40000 ALTER TABLE `daily_document_overdue` DISABLE KEYS */;
INSERT INTO `daily_document_overdue` VALUES (1,'2022-01-07',1,1,0,0,0,'2022-01-07 16:01:00','2022-01-07 16:01:00'),(2,'2022-01-08',1,1,0,0,0,'2022-01-08 16:10:06','2022-01-08 16:10:06'),(3,'2022-01-09',1,1,0,0,0,'2022-01-09 17:14:41','2022-01-09 17:14:41'),(4,'2022-01-10',1,1,0,0,0,'2022-01-10 16:01:00','2022-01-10 16:01:00'),(5,'2022-01-12',1,10,0,0,0,'2022-01-12 04:00:00','2022-01-12 16:01:00'),(6,'2022-01-14',1,5,0,0,0,'2022-01-14 18:35:41','2022-01-14 18:35:41'),(7,'2022-01-19',1,4,0,0,0,'2022-01-19 16:01:00','2022-01-19 16:01:00'),(8,'2022-01-21',1,8,0,0,0,'2022-01-21 14:07:37','2022-01-21 16:01:00'),(9,'2022-01-22',1,4,0,0,0,'2022-01-22 17:14:52','2022-01-22 17:14:52'),(10,'2022-01-24',1,8,0,0,0,'2022-01-24 10:01:46','2022-01-24 16:01:00');
/*!40000 ALTER TABLE `daily_document_overdue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_document_summary`
--

DROP TABLE IF EXISTS `daily_document_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_document_summary` (
  `daily_document_summary_id` bigint NOT NULL AUTO_INCREMENT,
  `for_date` date DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `document_total` int DEFAULT '0',
  `document_completed` int DEFAULT '0',
  `document_pending` int DEFAULT '0',
  `document_overdue` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_document_summary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_document_summary`
--

LOCK TABLES `daily_document_summary` WRITE;
/*!40000 ALTER TABLE `daily_document_summary` DISABLE KEYS */;
INSERT INTO `daily_document_summary` VALUES (1,'2022-01-07',1,1,0,0,1,'2022-01-07 16:01:00',NULL),(2,'2022-01-08',1,1,0,0,1,'2022-01-08 16:10:06',NULL),(3,'2022-01-09',1,1,0,0,1,'2022-01-09 17:14:41',NULL),(4,'2022-01-10',1,2,0,1,1,'2022-01-10 16:01:00',NULL),(5,'2022-01-12',1,5,0,0,5,'2022-01-12 04:00:00',NULL),(6,'2022-01-14',1,5,0,0,5,'2022-01-14 18:35:41',NULL),(7,'2022-01-15',1,0,0,0,0,'2022-01-15 16:12:01',NULL),(8,'2022-01-16',1,0,0,0,0,'2022-01-16 22:26:39',NULL),(9,'2022-01-17',1,0,0,0,0,'2022-01-17 16:01:00',NULL),(10,'2022-01-18',1,0,0,0,0,'2022-01-18 21:35:02',NULL),(11,'2022-01-19',1,5,1,0,4,'2022-01-19 16:01:00',NULL),(12,'2022-01-21',1,6,5,-3,4,'2022-01-21 14:07:37','2022-01-21 16:01:00'),(13,'2022-01-22',1,6,5,-3,4,'2022-01-22 17:14:52',NULL),(14,'2022-01-24',1,6,5,-3,4,'2022-01-24 10:01:46',NULL);
/*!40000 ALTER TABLE `daily_document_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_overdue_projects`
--

DROP TABLE IF EXISTS `daily_overdue_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_overdue_projects` (
  `daily_overdue_project_id` bigint NOT NULL AUTO_INCREMENT,
  `for_date` date DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `total_site` bigint DEFAULT '0',
  `total_overdue_below_30` int DEFAULT '0',
  `total_overdue_30_to_60` int DEFAULT '0',
  `total_overdue_60_to_90` int DEFAULT '0',
  `total_overdue_above_90` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_overdue_project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_overdue_projects`
--

LOCK TABLES `daily_overdue_projects` WRITE;
/*!40000 ALTER TABLE `daily_overdue_projects` DISABLE KEYS */;
INSERT INTO `daily_overdue_projects` VALUES (19,'2022-01-07',1,1,1,0,0,0,'2022-01-07 16:01:00','2022-01-07 16:01:00'),(25,'2022-01-08',1,1,1,0,0,0,'2022-01-08 16:10:06','2022-01-08 16:10:06'),(31,'2022-01-09',1,1,1,0,0,0,'2022-01-09 17:14:41','2022-01-09 17:14:41'),(37,'2022-01-10',1,1,1,0,0,0,'2022-01-10 16:01:00','2022-01-10 16:01:00'),(43,'2022-01-12',1,4,4,0,0,0,'2022-01-12 04:00:00','2022-01-12 04:00:00'),(54,'2022-01-14',1,4,4,0,0,0,'2022-01-14 18:35:41','2022-01-14 18:35:41'),(66,'2022-01-16',1,2,2,0,0,0,'2022-01-16 22:26:39','2022-01-16 22:26:39'),(72,'2022-01-17',1,2,2,0,0,0,'2022-01-17 16:01:00','2022-01-17 16:01:00'),(78,'2022-01-18',1,2,2,0,0,0,'2022-01-18 21:35:02','2022-01-18 21:35:02'),(84,'2022-01-19',1,2,2,0,0,0,'2022-01-19 16:01:00','2022-01-19 16:01:00'),(90,'2022-01-21',1,2,2,0,0,0,'2022-01-21 14:07:37','2022-01-21 14:07:37'),(101,'2022-01-22',1,2,2,0,0,0,'2022-01-22 17:14:52','2022-01-22 17:14:52'),(107,'2022-01-24',1,2,2,0,0,0,'2022-01-24 10:01:46','2022-01-24 10:01:46');
/*!40000 ALTER TABLE `daily_overdue_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_overdue_turn_on`
--

DROP TABLE IF EXISTS `daily_overdue_turn_on`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_overdue_turn_on` (
  `daily_overdue_turn_on_id` bigint NOT NULL AUTO_INCREMENT,
  `project_type_id` bigint DEFAULT NULL,
  `for_date` date DEFAULT NULL,
  `mwp_total_overdue` decimal(12,2) DEFAULT '0.00',
  `mwp_below_1` int DEFAULT NULL,
  `mwp_1_to_5` int DEFAULT NULL,
  `mwp_5_to_10` int DEFAULT NULL,
  `mwp_above_10` int DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_overdue_turn_on_id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_overdue_turn_on`
--

LOCK TABLES `daily_overdue_turn_on` WRITE;
/*!40000 ALTER TABLE `daily_overdue_turn_on` DISABLE KEYS */;
INSERT INTO `daily_overdue_turn_on` VALUES (19,1,'2022-01-07',0.00,1,0,0,0,'2022-01-07 16:01:00','2022-01-07 16:01:00'),(25,1,'2022-01-08',0.00,1,0,0,0,'2022-01-08 16:10:06','2022-01-08 16:10:06'),(31,1,'2022-01-09',0.00,1,0,0,0,'2022-01-09 17:14:41','2022-01-09 17:14:41'),(37,1,'2022-01-10',0.00,1,0,0,0,'2022-01-10 16:01:00','2022-01-10 16:01:00'),(43,1,'2022-01-12',0.06,4,0,0,0,'2022-01-12 04:00:00','2022-01-12 04:00:00'),(54,1,'2022-01-14',0.06,4,0,0,0,'2022-01-14 18:35:41','2022-01-14 18:35:41'),(66,1,'2022-01-16',0.01,2,0,0,0,'2022-01-16 22:26:39','2022-01-16 22:26:39'),(72,1,'2022-01-17',0.01,2,0,0,0,'2022-01-17 16:01:00','2022-01-17 16:01:00'),(78,1,'2022-01-18',0.01,2,0,0,0,'2022-01-18 21:35:02','2022-01-18 21:35:02'),(84,1,'2022-01-19',0.01,2,0,0,0,'2022-01-19 16:01:00','2022-01-19 16:01:00'),(90,1,'2022-01-21',0.01,2,0,0,0,'2022-01-21 14:07:37','2022-01-21 14:07:37'),(101,1,'2022-01-22',0.01,2,0,0,0,'2022-01-22 17:14:52','2022-01-22 17:14:52'),(107,1,'2022-01-24',0.01,2,0,0,0,'2022-01-24 10:01:46','2022-01-24 10:01:46');
/*!40000 ALTER TABLE `daily_overdue_turn_on` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_project_milestones`
--

DROP TABLE IF EXISTS `daily_project_milestones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_project_milestones` (
  `daily_project_milestone_id` bigint NOT NULL AUTO_INCREMENT,
  `for_date` date DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `milestone_code` varchar(50) DEFAULT NULL,
  `milestone_task_total` int DEFAULT '0',
  `milestone_task_completed` int DEFAULT '0',
  `milestone_document_total` int DEFAULT '0',
  `milestone_document_completed` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_project_milestone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_project_milestones`
--

LOCK TABLES `daily_project_milestones` WRITE;
/*!40000 ALTER TABLE `daily_project_milestones` DISABLE KEYS */;
INSERT INTO `daily_project_milestones` VALUES (1,'2022-01-07',1,'design',2,1,1,0,'2022-01-07 16:01:00',NULL),(2,'2022-01-08',1,'design',2,1,1,0,'2022-01-08 16:10:06',NULL),(3,'2022-01-09',1,'design',2,2,1,0,'2022-01-09 17:14:41',NULL),(4,'2022-01-10',1,'design',3,2,2,0,'2022-01-10 16:01:00',NULL),(5,'2022-01-10',1,'installation',0,0,0,0,'2022-01-10 16:01:00',NULL),(6,'2022-01-12',1,'design',5,2,5,0,'2022-01-12 04:00:00',NULL),(7,'2022-01-12',1,'installation',0,0,0,0,'2022-01-12 04:00:00',NULL),(8,'2022-01-14',1,'design',5,2,5,0,'2022-01-14 18:35:40',NULL),(9,'2022-01-14',1,'installation',0,0,0,0,'2022-01-14 18:35:41',NULL),(10,'2022-01-15',1,'design',-1,0,0,0,'2022-01-15 16:12:01',NULL),(11,'2022-01-15',1,'Installation',0,0,0,0,'2022-01-15 16:12:01',NULL),(12,'2022-01-16',1,'design',1,1,0,0,'2022-01-16 22:26:39',NULL),(13,'2022-01-16',1,'Installation',0,0,0,0,'2022-01-16 22:26:39',NULL),(14,'2022-01-17',1,'design',1,1,0,0,'2022-01-17 16:01:00',NULL),(15,'2022-01-17',1,'Installation',0,0,0,0,'2022-01-17 16:01:00',NULL),(16,'2022-01-18',1,'design',1,1,0,0,'2022-01-18 21:35:02',NULL),(17,'2022-01-18',1,'Installation',0,0,0,0,'2022-01-18 21:35:02',NULL),(18,'2022-01-19',1,'design',1,1,2,1,'2022-01-19 16:01:00',NULL),(19,'2022-01-19',1,'Installation',0,0,3,0,'2022-01-19 16:01:00',NULL),(20,'2022-01-21',1,'design',1,1,2,2,'2022-01-21 14:07:36',NULL),(21,'2022-01-21',1,'Installation',0,0,4,3,'2022-01-21 14:07:37','2022-01-21 16:01:00'),(22,'2022-01-22',1,'design',1,1,2,2,'2022-01-22 17:14:52',NULL),(23,'2022-01-22',1,'Installation',0,0,4,3,'2022-01-22 17:14:52',NULL),(24,'2022-01-24',1,'design',1,1,2,2,'2022-01-24 10:01:46',NULL),(25,'2022-01-24',1,'Installation',0,0,4,3,'2022-01-24 10:01:46',NULL);
/*!40000 ALTER TABLE `daily_project_milestones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_project_turn_on`
--

DROP TABLE IF EXISTS `daily_project_turn_on`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_project_turn_on` (
  `daily_project_turn_on_id` bigint NOT NULL AUTO_INCREMENT,
  `for_date` date DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `project_total` int DEFAULT NULL,
  `pending_total` int DEFAULT '0',
  `completed_total` int DEFAULT '0',
  `overdue_total` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_project_turn_on_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_project_turn_on`
--

LOCK TABLES `daily_project_turn_on` WRITE;
/*!40000 ALTER TABLE `daily_project_turn_on` DISABLE KEYS */;
INSERT INTO `daily_project_turn_on` VALUES (1,'2022-01-07',1,0,0,0,0,'2022-01-07 16:01:00','2022-01-07 16:01:00'),(2,'2022-01-08',1,0,0,0,0,'2022-01-08 16:10:06','2022-01-08 16:10:06'),(3,'2022-01-09',1,0,0,0,0,'2022-01-09 17:14:42','2022-01-09 17:14:42'),(4,'2022-01-10',1,0,0,0,0,'2022-01-10 16:01:00','2022-01-10 16:01:00'),(5,'2022-01-12',1,0,0,0,0,'2022-01-12 04:00:00','2022-01-12 04:00:00'),(6,'2022-01-14',1,0,0,0,0,'2022-01-14 18:35:41','2022-01-14 18:35:41'),(7,'2022-01-15',1,0,0,0,0,'2022-01-15 16:12:01','2022-01-15 16:12:01'),(8,'2022-01-16',1,0,0,0,0,'2022-01-16 22:26:39','2022-01-16 22:26:39'),(9,'2022-01-17',1,0,0,0,0,'2022-01-17 16:01:00','2022-01-17 16:01:00'),(10,'2022-01-18',1,0,0,0,0,'2022-01-18 21:35:02','2022-01-18 21:35:02'),(11,'2022-01-19',1,0,0,0,0,'2022-01-19 16:01:00','2022-01-19 16:01:00'),(12,'2022-01-21',1,0,0,0,0,'2022-01-21 14:07:38','2022-01-21 14:07:38'),(13,'2022-01-22',1,0,0,0,0,'2022-01-22 17:14:52','2022-01-22 17:14:52'),(14,'2022-01-24',1,0,0,0,0,'2022-01-24 10:01:46','2022-01-24 10:01:46');
/*!40000 ALTER TABLE `daily_project_turn_on` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_top_overdue_document`
--

DROP TABLE IF EXISTS `daily_top_overdue_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_top_overdue_document` (
  `daily_top_overdue_document_id` bigint NOT NULL AUTO_INCREMENT,
  `for_date` date DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `site_id` bigint DEFAULT NULL,
  `total_overdue_document` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_top_overdue_document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_top_overdue_document`
--

LOCK TABLES `daily_top_overdue_document` WRITE;
/*!40000 ALTER TABLE `daily_top_overdue_document` DISABLE KEYS */;
INSERT INTO `daily_top_overdue_document` VALUES (1,'2022-01-07',1,1,1,'2022-01-07 16:01:00','2022-01-07 16:01:00'),(2,'2022-01-08',1,1,1,'2022-01-08 16:10:06','2022-01-08 16:10:06'),(3,'2022-01-09',1,1,1,'2022-01-09 17:14:41','2022-01-09 17:14:41'),(4,'2022-01-10',1,1,1,'2022-01-10 16:01:00','2022-01-10 16:01:00'),(6,'2022-01-12',1,1,4,'2022-01-12 04:00:00','2022-01-12 16:01:00'),(7,'2022-01-12',1,2,2,'2022-01-12 04:00:00','2022-01-12 16:01:00'),(8,'2022-01-12',1,3,2,'2022-01-12 04:00:00','2022-01-12 16:01:00'),(9,'2022-01-12',1,4,2,'2022-01-12 04:00:00','2022-01-12 16:01:00'),(10,'2022-01-14',1,1,2,'2022-01-14 18:35:40','2022-01-14 18:35:40'),(11,'2022-01-14',1,2,1,'2022-01-14 18:35:40','2022-01-14 18:35:40'),(12,'2022-01-14',1,3,1,'2022-01-14 18:35:40','2022-01-14 18:35:40'),(13,'2022-01-14',1,4,1,'2022-01-14 18:35:40','2022-01-14 18:35:40'),(14,'2022-01-19',1,1,4,'2022-01-19 16:01:00','2022-01-19 16:01:00'),(15,'2022-01-21',1,1,8,'2022-01-21 14:07:36','2022-01-21 16:01:00'),(16,'2022-01-22',1,1,4,'2022-01-22 17:14:52','2022-01-22 17:14:52'),(17,'2022-01-24',1,1,8,'2022-01-24 10:01:46','2022-01-24 16:01:00');
/*!40000 ALTER TABLE `daily_top_overdue_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_top_overdue_project`
--

DROP TABLE IF EXISTS `daily_top_overdue_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_top_overdue_project` (
  `daily_top_overdue_project_id` bigint NOT NULL AUTO_INCREMENT,
  `for_date` date DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `site_id` bigint DEFAULT NULL,
  `overdue_mwp` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`daily_top_overdue_project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_top_overdue_project`
--

LOCK TABLES `daily_top_overdue_project` WRITE;
/*!40000 ALTER TABLE `daily_top_overdue_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `daily_top_overdue_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_categories`
--

DROP TABLE IF EXISTS `document_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_categories` (
  `document_category_id` bigint NOT NULL AUTO_INCREMENT,
  `document_category` varchar(50) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`document_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_categories`
--

LOCK TABLES `document_categories` WRITE;
/*!40000 ALTER TABLE `document_categories` DISABLE KEYS */;
INSERT INTO `document_categories` VALUES (1,'IFA Document',1,'2021-10-28 16:34:34','2021-10-28 16:34:34'),(2,'IFI Document',1,'2021-10-28 16:34:34','2021-10-28 16:34:34');
/*!40000 ALTER TABLE `document_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_comments`
--

DROP TABLE IF EXISTS `document_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_comments` (
  `document_comment_id` bigint NOT NULL AUTO_INCREMENT,
  `document_detail_id` bigint DEFAULT NULL,
  `document_comment` varchar(1000) DEFAULT NULL,
  `document_comment_sequence` int DEFAULT '1',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`document_comment_id`),
  KEY `FK_ref_comment_created_by_idx` (`created_by`),
  KEY `FK_ref_comment_comments_document_details_id_idx` (`document_detail_id`),
  CONSTRAINT `FK_ref_comment_comments_document_details_id` FOREIGN KEY (`document_detail_id`) REFERENCES `document_details` (`document_detail_id`),
  CONSTRAINT `FK_ref_document_comments_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_comments`
--

LOCK TABLES `document_comments` WRITE;
/*!40000 ALTER TABLE `document_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_comments` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `document_comments_insert_before` BEFORE INSERT ON `document_comments` FOR EACH ROW BEGIN
     
	SET @count_sequence = 0;
	SET @count_sequence = (Select count(document_comment_sequence) from document_comments where document_detail_id = NEW.document_detail_id);

	SET NEW.document_comment_sequence = @count_sequence + 1;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `document_comments_insert` AFTER INSERT ON `document_comments` FOR EACH ROW BEGIN
     
	SET @document_comment_no = (Select document_comment_no from document_details where document_detail_id = NEW.document_detail_id);
 	SET @document_comment_no = @document_comment_no + 1;
     
 	Update document_details
 	SET
 	document_comment_no = @document_comment_no
 	Where document_detail_id = NEW.document_detail_id;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `document_comments_delete` AFTER DELETE ON `document_comments` FOR EACH ROW BEGIN
     
	SET @document_comment_no = (Select document_comment_no from document_details where document_detail_id = OLD.document_detail_id);
 	SET @document_comment_no = @document_comment_no - 1;
     
 	Update document_details
 	SET
 	document_comment_no = @document_comment_no
 	Where document_detail_id = OLD.document_detail_id;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `document_details`
--

DROP TABLE IF EXISTS `document_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_details` (
  `document_detail_id` bigint NOT NULL AUTO_INCREMENT,
  `document_id` bigint DEFAULT NULL,
  `document_information` varchar(300) DEFAULT NULL,
  `document_version` int DEFAULT '0',
  `document_file` varchar(1000) DEFAULT NULL,
  `document_comment_no` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `req_approval_project_owner` tinyint(1) DEFAULT '0',
  `req_approval_project_manager` tinyint(1) DEFAULT '0',
  `req_approval_project_engineer` tinyint(1) DEFAULT '0',
  `req_approval_engineer` tinyint(1) DEFAULT '0',
  `req_approval_qa_qc` tinyint(1) DEFAULT '0',
  `req_approval_safety` tinyint(1) DEFAULT '0',
  `req_approval_planner` tinyint(1) DEFAULT '0',
  `req_approval_purchasing` tinyint(1) DEFAULT '0',
  `req_approval_admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`document_detail_id`),
  KEY `FK_ref_document_details_created_by_idx` (`created_by`),
  KEY `FK_ref_document_details_document_id_idx` (`document_id`),
  CONSTRAINT `FK_ref_document_details_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_document_details_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_details`
--

LOCK TABLES `document_details` WRITE;
/*!40000 ALTER TABLE `document_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `document_details_insert_before` BEFORE INSERT ON `document_details` FOR EACH ROW BEGIN
     
	SET @document_version = 0;
	SET @document_version = (Select count(document_detail_id) from document_details where document_id = NEW.document_id);
    SET @document_version = @document_version + 1;
 	SET NEW.document_version = @document_version;
    
    UPDATE documents
    SET document_version = @document_version
    Where document_id = NEW.document_id;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `document_project_classifications`
--

DROP TABLE IF EXISTS `document_project_classifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_project_classifications` (
  `project_class_id` bigint NOT NULL AUTO_INCREMENT,
  `project_class_code` varchar(50) DEFAULT NULL,
  `project_class_info` varchar(100) DEFAULT NULL,
  `milestone_project_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `project_class_latest_version` int DEFAULT '1',
  `project_class_total_doc` int DEFAULT '0',
  `project_class_mandatory` tinyint(1) DEFAULT '1',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `from_document_template_id` bigint DEFAULT NULL,
  `from_document_template_detail_id` bigint DEFAULT NULL,
  PRIMARY KEY (`project_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_project_classifications`
--

LOCK TABLES `document_project_classifications` WRITE;
/*!40000 ALTER TABLE `document_project_classifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_project_classifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_site_classifications`
--

DROP TABLE IF EXISTS `document_site_classifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_site_classifications` (
  `site_class_id` bigint NOT NULL AUTO_INCREMENT,
  `site_class_code` varchar(50) DEFAULT NULL,
  `site_class_info` varchar(100) DEFAULT NULL,
  `milestone_site_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `site_class_latest_version` int DEFAULT '1',
  `site_class_total_doc` int DEFAULT '0',
  `site_class_mandatory` tinyint(1) DEFAULT '1',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `from_document_template_id` bigint DEFAULT NULL,
  `from_document_template_detail_id` bigint DEFAULT NULL,
  PRIMARY KEY (`site_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_site_classifications`
--

LOCK TABLES `document_site_classifications` WRITE;
/*!40000 ALTER TABLE `document_site_classifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_site_classifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_template_details`
--

DROP TABLE IF EXISTS `document_template_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_template_details` (
  `document_template_detail_id` bigint NOT NULL AUTO_INCREMENT,
  `document_template_id` bigint DEFAULT NULL,
  `document_template_title` varchar(100) DEFAULT NULL,
  `document_template_mandatory` tinyint(1) DEFAULT '1',
  `document_category_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `recurring_interval_id` bigint DEFAULT NULL,
  `milestone_template_detail_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `req_approval_project_owner` tinyint(1) DEFAULT '0',
  `req_approval_project_manager` tinyint(1) DEFAULT '0',
  `req_approval_project_engineer` tinyint(1) DEFAULT '0',
  `req_approval_engineer` tinyint(1) DEFAULT '0',
  `req_approval_qa_qc` tinyint(1) DEFAULT '0',
  `req_approval_onm` tinyint DEFAULT '0',
  `req_approval_safety` tinyint(1) DEFAULT '0',
  `req_approval_planner` tinyint(1) DEFAULT '0',
  `req_approval_purchasing` tinyint(1) DEFAULT '0',
  `req_approval_admin` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`document_template_detail_id`),
  KEY `FK_ref_document_template_details_created_by_idx` (`created_by`),
  KEY `FK_ref_document_template_details_document_template_id_idx` (`document_template_id`),
  KEY `FK_ref_document_template_details_milestone_template_detail__idx` (`milestone_template_detail_id`),
  KEY `FK_ref_document_template_details_document_type_id_idx` (`document_type_id`),
  KEY `FK_ref_document_template_details_document_type_id_idx1` (`document_category_id`),
  KEY `FK_ref_document_template_details_recurring_interval_id_idx` (`recurring_interval_id`),
  CONSTRAINT `FK_ref_document_template_details_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_document_template_details_document_template_id` FOREIGN KEY (`document_template_id`) REFERENCES `document_templates` (`document_template_id`),
  CONSTRAINT `FK_ref_document_template_details_document_type_id` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`document_type_id`),
  CONSTRAINT `FK_ref_document_template_details_milestone_template_detail_id` FOREIGN KEY (`milestone_template_detail_id`) REFERENCES `milestone_template_details` (`milestone_template_detail_id`),
  CONSTRAINT `FK_ref_document_template_details_recurring_interval_id` FOREIGN KEY (`recurring_interval_id`) REFERENCES `recurring_intervals` (`recurring_interval_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_template_details`
--

LOCK TABLES `document_template_details` WRITE;
/*!40000 ALTER TABLE `document_template_details` DISABLE KEYS */;
INSERT INTO `document_template_details` VALUES (1,1,'site-document-1',1,1,1,NULL,1,5,'2022-01-06 07:12:41','2022-01-06 07:12:41',1,1,1,1,1,1,1,1,1,1,1,NULL);
/*!40000 ALTER TABLE `document_template_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `document_template_details_insert` AFTER INSERT ON `document_template_details` FOR EACH ROW BEGIN
  
	Select document_details_no into @document_details_no from document_templates where document_template_id = NEW.document_template_id;
    
    SET @document_details_no = @document_details_no + 1;
    
    Update document_templates
    SET
    document_details_no = @document_details_no
    Where document_template_id = NEW.document_template_id;
       
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `document_template_details_delete` AFTER DELETE ON `document_template_details` FOR EACH ROW BEGIN
  
	Select document_details_no into @document_details_no from document_templates where document_template_id = OLD.document_template_id;
    
    SET @document_details_no = @document_details_no - 1;
    
    Update document_templates
    SET
    document_details_no = @document_details_no
    Where document_template_id = OLD.document_template_id;
       
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `document_templates`
--

DROP TABLE IF EXISTS `document_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_templates` (
  `document_template_id` bigint NOT NULL AUTO_INCREMENT,
  `document_template_name` varchar(100) DEFAULT NULL,
  `document_details_no` int DEFAULT '0',
  `milestone_template_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`document_template_id`),
  KEY `FK_ref_document_templates_created_by_idx` (`created_by`),
  KEY `FK_ref_document_templates_milestone_template_id_idx` (`milestone_template_id`),
  CONSTRAINT `FK_ref_document_templates_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_document_templates_milestone_template_id` FOREIGN KEY (`milestone_template_id`) REFERENCES `milestone_templates` (`milestone_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_templates`
--

LOCK TABLES `document_templates` WRITE;
/*!40000 ALTER TABLE `document_templates` DISABLE KEYS */;
INSERT INTO `document_templates` VALUES (1,'site-documen-template',1,1,5,'2022-01-06 07:12:30','2022-01-06 07:12:41');
/*!40000 ALTER TABLE `document_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_types`
--

DROP TABLE IF EXISTS `document_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document_types` (
  `document_type_id` bigint NOT NULL AUTO_INCREMENT,
  `document_type_code` varchar(50) DEFAULT NULL,
  `document_type_info` varchar(100) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`document_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_types`
--

LOCK TABLES `document_types` WRITE;
/*!40000 ALTER TABLE `document_types` DISABLE KEYS */;
INSERT INTO `document_types` VALUES (1,'ONE-TIME','One-Time Document Type',1,'2021-08-01 23:15:58','2021-08-01 23:16:43'),(2,'RECURRING','Recurring Document Type',1,'2021-08-01 23:15:58','2021-08-01 23:16:43');
/*!40000 ALTER TABLE `document_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `documents` (
  `document_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `site_id` bigint DEFAULT NULL,
  `document_classification_code` varchar(50) DEFAULT NULL,
  `document_information` varchar(100) DEFAULT NULL,
  `document_version` int DEFAULT '0',
  `document_mandatory` tinyint(1) DEFAULT '1',
  `document_type_id` bigint DEFAULT NULL,
  `milestone_id` bigint DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from_document_template_id` bigint DEFAULT NULL,
  `from_document_template_detail_id` bigint DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  KEY `FK_ref_documents_created_by_idx` (`created_by`),
  KEY `FK_ref_documents_document_type_id_idx` (`document_type_id`),
  KEY `FK_ref_documents_milestone_id_idx` (`milestone_id`),
  KEY `FK_ref_documents_project_id_idx` (`project_id`),
  KEY `FK_ref_documents_site_id_idx` (`site_id`),
  KEY `FK_ref_documents_site_id_idx1` (`status_id`),
  CONSTRAINT `FK_ref_documents_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_documents_document_type_id` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`document_type_id`),
  CONSTRAINT `FK_ref_documents_milestone_id` FOREIGN KEY (`milestone_id`) REFERENCES `project_milestones` (`milestone_id`),
  CONSTRAINT `FK_ref_documents_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`),
  CONSTRAINT `FK_ref_documents_site_id` FOREIGN KEY (`site_id`) REFERENCES `project_sites` (`site_id`),
  CONSTRAINT `FK_ref_documents_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_documents` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `documents_insert` AFTER INSERT ON `documents` FOR EACH ROW BEGIN
 
	SET @group_id = (Select group_id from project_sites where site_id = NEW.site_id);
	SET @project_id = (Select project_id from project_groups where group_id = @group_id);
     
	SET @site_total_document = (Select site_total_document from project_sites where site_id = NEW.site_id);
	SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);
	SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
      
 	SET @site_total_document = @site_total_document + 1;
 	SET @group_total_document = @group_total_document + 1;
	SET @project_total_document = @project_total_document + 1;
     
 	Update project_sites
 	SET
 	site_total_document = @site_total_document
 	Where site_id = NEW.site_id;
      
 	Update project_groups
 	SET
 	group_total_document = @group_total_document
 	Where group_id = @group_id;
     
 	Update projects
 	SET
 	project_total_document = @project_total_document
 	Where project_id = @project_id;
 	
 	IF ( NEW.status_id in (4) ) THEN
		 
		SET @site_completed_document = (Select site_completed_document from project_sites where site_id = NEW.site_id);
		SET @group_completed_document = (Select group_completed_document from project_groups where group_id = @group_id);
		SET @project_completed_document = (Select project_completed_document from projects where project_id = @project_id);
		  
		SET @site_completed_document = @site_completed_document + 1;
		SET @group_completed_document = @group_completed_document + 1;
		SET @project_completed_document = @project_completed_document + 1;
		 
		Update project_sites
		SET
		site_completed_document = @site_completed_document
		Where site_id = NEW.site_id;
		  
		Update project_groups
		SET
		group_completed_document = @group_completed_document
		Where group_id = @group_id;
		 
		Update projects
		SET
		project_completed_document = @project_completed_document
		Where project_id = @project_id;
    
    END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `documents_update` AFTER UPDATE ON `documents` FOR EACH ROW BEGIN
     
	IF ( NEW.status_id in (4) ) THEN
    
		SET @group_id = (Select group_id from project_sites where site_id = NEW.site_id);
		SET @project_id = (Select project_id from project_groups where group_id = @group_id);
		 
		SET @site_completed_document = (Select site_completed_document from project_sites where site_id = NEW.site_id);
		SET @group_completed_document = (Select group_completed_document from project_groups where group_id = @group_id);
		SET @project_completed_document = (Select project_completed_document from projects where project_id = @project_id);
		  
		SET @site_completed_document = @site_completed_document + 1;
		SET @group_completed_document = @group_completed_document + 1;
		SET @project_completed_document = @project_completed_document + 1;
		 
		Update project_sites
		SET
		site_completed_document = @site_completed_document
		Where site_id = NEW.site_id;
		  
		Update project_groups
		SET
		group_completed_document = @group_completed_document
		Where group_id = @group_id;
		 
		Update projects
		SET
		project_completed_document = @project_completed_document
		Where project_id = @project_id;
    
    END IF;
         
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `documents_delete` AFTER DELETE ON `documents` FOR EACH ROW BEGIN
 
	SET @group_id = (Select group_id from project_sites where site_id = OLD.site_id);
	SET @project_id = (Select project_id from project_groups where group_id = @group_id);
     
	SET @site_total_document = (Select site_total_document from project_sites where site_id = OLD.site_id);
	SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);
	SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
      
 	SET @site_total_document = @site_total_document - 1;
 	SET @group_total_document = @group_total_document - 1;
	SET @project_total_document = @project_total_document - 1;
     
 	Update project_sites
 	SET
 	site_total_document = @site_total_document
 	Where site_id = OLD.site_id;
      
 	Update project_groups
 	SET
 	group_total_document = @group_total_document
 	Where group_id = @group_id;
     
 	Update projects
 	SET
 	project_total_document = @project_total_document
 	Where project_id = @project_id;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itasks`
--

DROP TABLE IF EXISTS `itasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `itasks` (
  `task_id` bigint NOT NULL AUTO_INCREMENT,
  `site_id` bigint DEFAULT NULL,
  `task_code` varchar(50) DEFAULT NULL,
  `task_title` varchar(100) DEFAULT NULL,
  `assign_to_user` bigint DEFAULT NULL,
  `milestone_id` bigint DEFAULT NULL,
  `contractor_id` bigint DEFAULT NULL,
  `task_description` varchar(300) DEFAULT NULL,
  `task_remarks` varchar(1000) DEFAULT NULL,
  `task_progress` int DEFAULT '0',
  `status_id` bigint DEFAULT NULL,
  `task_est_start_date` datetime DEFAULT NULL,
  `task_est_end_date` datetime DEFAULT NULL,
  `task_start_date` datetime DEFAULT NULL,
  `task_end_date` datetime DEFAULT NULL,
  `upload_attachment` varchar(900) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from_task_template_id` bigint DEFAULT NULL,
  `from_task_template_detail_id` bigint DEFAULT NULL,
  `calculated_flag` tinyint DEFAULT '0',
  PRIMARY KEY (`task_id`),
  KEY `FK_ref_itask_created_by_idx` (`created_by`),
  KEY `FK_ref_itask_site_id_idx` (`site_id`),
  KEY `FK_ref_itask_contractor_id_idx` (`contractor_id`),
  KEY `FK_ref_itask_milestone_id_idx` (`milestone_id`),
  KEY `FK_ref_itask_status_id_idx` (`status_id`),
  KEY `FK_ref_itask_status_id_idx1` (`assign_to_user`),
  CONSTRAINT `FK_ref_itask_contractor_id` FOREIGN KEY (`contractor_id`) REFERENCES `project_contractors` (`contractor_id`),
  CONSTRAINT `FK_ref_itask_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_itask_milestone_id` FOREIGN KEY (`milestone_id`) REFERENCES `project_milestones` (`milestone_id`),
  CONSTRAINT `FK_ref_itask_site_id` FOREIGN KEY (`site_id`) REFERENCES `project_sites` (`site_id`),
  CONSTRAINT `FK_ref_itask_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_tasks` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itasks`
--

LOCK TABLES `itasks` WRITE;
/*!40000 ALTER TABLE `itasks` DISABLE KEYS */;
INSERT INTO `itasks` VALUES (9,1,NULL,'task-8',2,1,NULL,NULL,NULL,100,4,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,2,'2022-01-16 12:05:30','2022-01-16 04:06:07',NULL,NULL,1);
/*!40000 ALTER TABLE `itasks` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `itasks_insert` BEFORE INSERT ON `itasks` FOR EACH ROW BEGIN

	SET @group_id = (Select group_id from project_sites where site_id = NEW.site_id);
   SET @project_id = (Select project_id from project_groups where group_id = @group_id);
    
   SET @site_total_task = (Select site_total_task from project_sites where site_id = NEW.site_id);
   SET @group_total_task = (Select group_total_task from project_groups where group_id = @group_id);
   SET @project_total_task = (Select project_total_task from projects where project_id = @project_id);
     
	SET @site_total_task = @site_total_task + 1;
	SET @group_total_task = @group_total_task + 1;
   SET @project_total_task = @project_total_task + 1;
    
	Update project_sites
	SET
	site_total_task = @site_total_task
	Where site_id = NEW.site_id;
     
	Update project_groups
	SET
	group_total_task = @group_total_task
	Where group_id = @group_id;
    
	Update projects
	SET
	project_total_task = @project_total_task
	Where project_id = @project_id;
	
	SET @milestone_task_total = (Select milestone_task_total from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);
            
   SET @milestone_task_total = @milestone_task_total + 1;
            
   Update project_milestones
   SET
   milestone_task_total = @milestone_task_total
   Where project_id = @project_id and milestone_id = NEW.milestone_id;
	
	IF ( NEW.status_id = '4' ) THEN
		 
		SET @site_completed_task = (Select site_completed_task from project_sites where site_id = NEW.site_id);
		SET @group_completed_task = (Select group_completed_task from project_groups where group_id = @group_id);
		SET @project_completed_task = (Select project_completed_task from projects where project_id = @project_id);
		  
		SET @site_completed_task = @site_completed_task + 1;
		SET @group_completed_task = @group_completed_task + 1;
		SET @project_completed_task = @project_completed_task + 1;
		 
		Update project_sites
		SET
		site_completed_task = @site_completed_task
		Where site_id = NEW.site_id;
		  
		Update project_groups
		SET
		group_completed_task = @group_completed_task
		Where group_id = @group_id;
		 
		Update projects
		SET
		project_completed_task = @project_completed_task
		Where project_id = @project_id;
		
		SET @milestone_task_completed = (Select milestone_task_completed from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);
            
	   SET @milestone_task_completed = @milestone_task_completed + 1;
	            
	   Update project_milestones
	   SET
	   milestone_task_completed = @milestone_task_completed
	   Where project_id = @project_id and milestone_id = NEW.milestone_id;
	   
	   SET NEW.calculated_flag = 1;
	   
	    
   END IF;
   
   ## Update Project S-Curve EST
   SET @project_scurve_id = NULL;
   
   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task 
	FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_est_end_date) AND `year` = YEAR(NEW.task_est_end_date);
   
   IF (@project_scurve_id IS NOT NULL) THEN
   
   	SET @expected_total_task = @expected_total_task + 1;
   	
   	UPDATE project_scurves
   	SET
   	expected_total_task = @expected_total_task
   	WHERE project_scurve_id = @project_scurve_id;
   	
   ELSE
   
   	INSERT INTO project_scurves
   	(project_id, `year`, `month`, expected_total_task )
   	VALUES
   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);
   
   END IF; 
   
   ## Update Project S-Curve Actual
   SET @project_scurve_id = NULL;
   
   IF ( NEW.task_end_date IS NOT NULL ) THEN
	   SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task 
		FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_end_date) AND `year` = YEAR(NEW.task_end_date);
	   
	   IF (@project_scurve_id IS NOT NULL) THEN
	   
	   	SET @actual_total_task = @actual_total_task + 1;
	   	
	   	UPDATE project_scurves
	   	SET
	   	actual_total_task = @actual_total_task
	   	WHERE project_scurve_id = @project_scurve_id;
	   	
	   ELSE
	   
	   	INSERT INTO project_scurves
	   	(project_id, `year`, `month`, actual_total_task )
	   	VALUES
	   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);
	   
	   END IF; 
	END IF;
        
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_task_assign_after_insert` AFTER INSERT ON `itasks` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE TASK TO
    Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;
    
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link ) 
    VALUES (now(), NEW.assign_to_user, 'SITE TASK', @project_id, @group_id, NEW.site_id, NEW.task_id, NEW.task_title, 'taskEdit');
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `itasks_update` BEFORE UPDATE ON `itasks` FOR EACH ROW BEGIN
	
	SET @group_id = (Select group_id from project_sites where site_id = NEW.site_id);
	SET @project_id = (Select project_id from project_groups where group_id = @group_id);
     
	IF ( NEW.status_id = '4' AND OLD.calculated_flag = '0') THEN
    		 
		SET @site_completed_task = (Select site_completed_task from project_sites where site_id = NEW.site_id);
		SET @group_completed_task = (Select group_completed_task from project_groups where group_id = @group_id);
		SET @project_completed_task = (Select project_completed_task from projects where project_id = @project_id);
		  
		SET @site_completed_task = @site_completed_task + 1;
		SET @group_completed_task = @group_completed_task + 1;
		SET @project_completed_task = @project_completed_task + 1;
		 
		Update project_sites
		SET
		site_completed_task = @site_completed_task
		Where site_id = NEW.site_id;
		  
		Update project_groups
		SET
		group_completed_task = @group_completed_task
		Where group_id = @group_id;
		 
		Update projects
		SET
		project_completed_task = @project_completed_task
		Where project_id = @project_id;
		
		SET @milestone_task_completed = (Select milestone_task_completed from project_milestones where project_id = @project_id and milestone_id = OLD.milestone_id);
            
	   SET @milestone_task_completed = @milestone_task_completed + 1;
	            
	   Update project_milestones
	   SET
	   milestone_task_completed = @milestone_task_completed
	   Where project_id = @project_id and milestone_id = OLD.milestone_id;
	   
	   SET NEW.calculated_flag = 1;
    
    END IF;
    
   ## Update Project S-Curve EST
   IF (OLD.task_est_end_date <> NEW.task_est_end_date) THEN
	   
	   #Remove OlD One
	   SET @project_scurve_id = NULL;
	   
	   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task 
		FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(OLD.task_est_end_date) AND `year` = YEAR(OLD.task_est_end_date);
	   
	   IF (@project_scurve_id IS NOT NULL) THEN
	   
	   	SET @expected_total_task = @expected_total_task - 1;
	   	
	   	UPDATE project_scurves
	   	SET
	   	expected_total_task = @expected_total_task
	   	WHERE project_scurve_id = @project_scurve_id;
	   
	   END IF; 
	   
	   #Add New One
	   SET @project_scurve_id = NULL;
	   
	   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task 
		FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_est_end_date) AND `year` = YEAR(NEW.task_est_end_date);
	   
	   IF (@project_scurve_id IS NOT NULL) THEN
	   
	   	SET @expected_total_task = @expected_total_task + 1;
	   	
	   	UPDATE project_scurves
	   	SET
	   	expected_total_task = @expected_total_task
	   	WHERE project_scurve_id = @project_scurve_id;
	   	
	   ELSE
	   
	   	INSERT INTO project_scurves
	   	(project_id, `year`, `month`, expected_total_task )
	   	VALUES
	   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);
	   
	   END IF; 
   
   END IF;
    
   ## Update Project S-Curve Actual
   
   IF ( NEW.task_end_date is not NULL )THEN
   
	   # Remove OLD one
	 
		   SET @project_scurve_id = NULL;
		   
			SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task 
			FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(OLD.task_end_date) AND `year` = YEAR(OLD.task_end_date);
		   
		   IF (@project_scurve_id IS NOT NULL) THEN
		   
		   	SET @actual_total_task = @actual_total_task - 1;
		   	
		   	UPDATE project_scurves
		   	SET
		   	actual_total_task = @actual_total_task
		   	WHERE project_scurve_id = @project_scurve_id;
		   
		   END IF; 

	   
	   #Add New One
	   SET @project_scurve_id = NULL;
	   
	   IF ( NEW.task_end_date IS NOT NULL ) THEN
	   
			SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task 
			FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_end_date) AND `year` = YEAR(NEW.task_end_date);
		   
		   IF (@project_scurve_id IS NOT NULL) THEN
		   
		   	SET @actual_total_task = @actual_total_task + 1;
		   	
		   	UPDATE project_scurves
		   	SET
		   	actual_total_task = @actual_total_task
		   	WHERE project_scurve_id = @project_scurve_id;
		   	
		   ELSE
		   
		   	INSERT INTO project_scurves
		   	(project_id, `year`, `month`, actual_total_task )
		   	VALUES
		   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);
		   
		   END IF; 
	   END IF;
   
   END IF;
         
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_task_assign_after_update` AFTER UPDATE ON `itasks` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE TASK TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;
		
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link ) 
		VALUES (now(), NEW.assign_to_user, 'SITE TASK', @project_id, @group_id, NEW.site_id, NEW.task_id, NEW.task_title, 'taskEdit');
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `itasks_deleted`
--

DROP TABLE IF EXISTS `itasks_deleted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `itasks_deleted` (
  `task_id` bigint NOT NULL AUTO_INCREMENT,
  `site_id` bigint DEFAULT NULL,
  `task_code` varchar(50) DEFAULT NULL,
  `task_title` varchar(100) DEFAULT NULL,
  `assign_to_user` bigint DEFAULT NULL,
  `milestone_id` bigint DEFAULT NULL,
  `contractor_id` bigint DEFAULT NULL,
  `task_description` varchar(300) DEFAULT NULL,
  `task_remarks` varchar(1000) DEFAULT NULL,
  `task_progress` int DEFAULT '0',
  `status_id` bigint DEFAULT NULL,
  `task_est_start_date` datetime DEFAULT NULL,
  `task_est_end_date` datetime DEFAULT NULL,
  `task_start_date` datetime DEFAULT NULL,
  `task_end_date` datetime DEFAULT NULL,
  `upload_attachment` varchar(900) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from_task_template_id` bigint DEFAULT NULL,
  `from_task_template_detail_id` bigint DEFAULT NULL,
  `calculated_flag` tinyint DEFAULT '0',
  `deleted_by` bigint DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`task_id`),
  KEY `FK_ref_itask_created_by_idx` (`created_by`),
  KEY `FK_ref_itask_site_id_idx` (`site_id`),
  KEY `FK_ref_itask_contractor_id_idx` (`contractor_id`),
  KEY `FK_ref_itask_milestone_id_idx` (`milestone_id`),
  KEY `FK_ref_itask_status_id_idx` (`status_id`),
  KEY `FK_ref_itask_status_id_idx1` (`assign_to_user`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itasks_deleted`
--

LOCK TABLES `itasks_deleted` WRITE;
/*!40000 ALTER TABLE `itasks_deleted` DISABLE KEYS */;
INSERT INTO `itasks_deleted` VALUES (1,1,NULL,'task-1',7,1,1,NULL,NULL,10,1,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,5,'2022-01-15 14:41:37','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(2,1,NULL,'task-2',5,1,1,NULL,NULL,0,1,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,5,'2022-01-15 16:38:48','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(3,1,NULL,'task-3',5,2,NULL,NULL,NULL,0,1,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,5,'2022-01-15 16:41:41','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(4,1,NULL,'task-1',5,1,NULL,NULL,NULL,0,1,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,5,'2022-01-15 16:53:37','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(5,1,NULL,'task-4',2,1,NULL,NULL,NULL,0,1,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,2,'2022-01-15 17:12:53','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(6,1,NULL,'task-5',2,1,NULL,NULL,NULL,0,1,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,2,'2022-01-15 17:14:53','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(7,1,NULL,'task-6',2,1,NULL,NULL,NULL,0,1,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,2,'2022-01-15 18:28:15','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(8,1,NULL,'task-7',2,1,NULL,NULL,NULL,100,3,'2022-01-15 00:00:00','2022-01-15 00:00:00',NULL,NULL,NULL,2,'2022-01-15 18:28:35','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(10,1,NULL,'task-9',5,2,NULL,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:08:33','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(11,1,NULL,'task-9',5,1,NULL,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:17:21','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(12,1,NULL,'task-10',5,1,NULL,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:23:17','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(13,1,NULL,'task-11',5,2,NULL,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:23:37','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(14,1,NULL,'task-12',5,1,NULL,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:29:27','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(15,1,NULL,'task-13',5,2,NULL,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:29:41','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(16,1,NULL,'task-14',5,2,NULL,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:29:58','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(17,1,NULL,'task-15',5,2,NULL,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:38:46','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(18,1,NULL,'task-16',5,2,NULL,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:39:21','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(19,1,NULL,'task-9',5,1,NULL,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:42:48','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(20,1,NULL,'task-10',5,2,NULL,NULL,NULL,0,5,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:43:08','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(21,1,NULL,'task-11',5,2,NULL,NULL,NULL,0,6,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:48:10','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(22,1,NULL,'task-12',5,2,NULL,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 12:48:36','2022-01-16 13:16:30',NULL,NULL,0,1,'2022-01-16 13:11:33'),(23,1,NULL,'task-1',5,2,NULL,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 13:14:50','2022-01-16 13:14:50',NULL,NULL,0,1,'2022-01-16 13:16:06'),(24,1,NULL,'task-1',5,2,NULL,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 13:20:42','2022-01-16 13:20:42',NULL,NULL,0,5,'2022-01-16 13:20:49');
/*!40000 ALTER TABLE `itasks_deleted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (160,'default','{\"uuid\":\"7518dc4c-29e6-4c06-bbea-53c8224ffe4e\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"14e6a503-acd1-4381-8d0a-b889b3e460d5\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1641478878,1641478878),(161,'default','{\"uuid\":\"3541f214-f3db-4a10-b131-8bb27ed0446a\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"4885f9fb-e04b-46d9-ab25-ee413fa7617c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1641478893,1641478893),(162,'default','{\"uuid\":\"38bf1d03-8ad1-49ca-a901-b2ea15141ac5\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"b3800292-f4f9-408f-983a-f214707f21fb\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1641480438,1641480438),(163,'default','{\"uuid\":\"0e771d35-67bb-4944-b1a7-324ef1c30d4f\",\"displayName\":\"App\\\\Notifications\\\\TaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:38:\\\"App\\\\Notifications\\\\TaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectTask\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"9813775c-1de2-4209-83b9-a3344ef47ecc\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1641639037,1641639037),(164,'default','{\"uuid\":\"b90e3807-879f-4965-95c4-38d23f21d14f\",\"displayName\":\"App\\\\Notifications\\\\TaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:38:\\\"App\\\\Notifications\\\\TaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectTask\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"5e92b97f-b55f-4f13-a91e-58f9e9bf41e0\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1641644897,1641644897),(165,'default','{\"uuid\":\"dd74ee5f-e44b-4ad7-907d-2dba44419d7f\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"b5cfaa22-3c07-4ad7-bdd2-dcd0ddacc27d\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1641648072,1641648072),(166,'default','{\"uuid\":\"d4daa2a2-86fb-4287-a4f4-0f39d6d6e73b\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"e55fdf46-f835-4474-84cb-bf659ceddbd7\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1641648264,1641648264),(167,'default','{\"uuid\":\"7b2245a9-20ca-4666-b4ff-32ebf8d9d13f\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"bb57553b-6f29-44fa-b4a9-5ea3c4637e64\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642305930,1642305930),(168,'default','{\"uuid\":\"baa1a3cd-ca88-4267-afe2-20e417c88304\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"3fa703b0-d6d6-42b5-9ec6-3b3cd7dced7f\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642305967,1642305967),(169,'default','{\"uuid\":\"2f4c73f6-e111-4d1a-aacb-328d087b5c52\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"4d678c90-c6b3-48f5-9a2d-07827d798f19\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642306091,1642306091),(170,'default','{\"uuid\":\"25e3d11a-e65f-4c95-a760-2984d8c5f384\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:20;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"33a772b0-fd13-4c76-905f-4653f7e0187c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642308188,1642308188),(171,'default','{\"uuid\":\"d24d16a8-6825-4447-a291-f4459d235672\",\"displayName\":\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\SiteTaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\SemCorp\\\\Task\\\";s:2:\\\"id\\\";i:21;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"cf4852ad-2600-409f-b87a-7639a1e54c67\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642308490,1642308490),(172,'default','{\"uuid\":\"705e75d6-f58d-41a5-8801-f33af13c068c\",\"displayName\":\"App\\\\Notifications\\\\TaskChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:38:\\\"App\\\\Notifications\\\\TaskChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectTask\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"549a2aea-a76a-45db-84d3-918ea8d9fe06\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642311703,1642311703),(173,'default','{\"uuid\":\"08aa201d-83cd-4584-9dcf-29146d541306\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"26fb6029-4e0d-4254-8932-618e4e3501a1\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642577939,1642577939),(174,'default','{\"uuid\":\"2f5b7ff8-e3af-4e5b-b0b5-99030ebdb063\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectSite\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"90598049-4436-44fa-9804-d8ab41f6b8a3\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642577939,1642577939),(175,'default','{\"uuid\":\"930d4861-2ad9-4de0-8e4b-066495abe734\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:40:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocumentRecurring\\\";s:2:\\\"id\\\";i:19;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"b70f96af-7a2b-4449-8439-dc48374de36a\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642577974,1642577974),(176,'default','{\"uuid\":\"886b2f70-f64e-4768-83b0-faae6af860fc\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"6610c4f0-ee9a-4e8f-86bd-660ec56f932c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642578037,1642578037),(177,'default','{\"uuid\":\"00216b7d-1186-4c7a-8a0f-63723da7b1a3\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"bf59bb18-bdae-4721-91b4-9ac70e2cec88\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642580168,1642580168),(178,'default','{\"uuid\":\"ec2eecc5-57c2-437f-9182-a8e0feb3c32b\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:46:\\\"App\\\\Notifications\\\\SiteDocumentChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"69a837ad-fe54-48cf-a074-90e604496a07\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642580171,1642580171),(179,'default','{\"uuid\":\"040f6250-9e92-48b2-884b-ae35f9e7f552\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:10;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"2e5bd298-6c35-4d07-960f-2f8946116b59\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642580215,1642580215),(180,'default','{\"uuid\":\"76b7ffda-d081-497d-a472-2f03dbd4476e\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:46:\\\"App\\\\Notifications\\\\SiteDocumentChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:10;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"6d82cf83-4e72-45b1-8868-229244b93ded\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642580218,1642580218),(181,'default','{\"uuid\":\"18c44f0a-468c-4129-8ec4-583654013789\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"59742399-fa32-4d5a-ab41-76a13553ad00\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642580265,1642580265),(182,'default','{\"uuid\":\"e924dbef-7811-43e1-9075-add3f3804a02\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectSite\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"842ada67-3345-4f7e-bab6-a0da7e27c890\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642580265,1642580265),(183,'default','{\"uuid\":\"c80bfe0f-e9bf-4db4-9859-9a744f06bfc9\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:40:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocumentRecurring\\\";s:2:\\\"id\\\";i:19;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"072ce7b0-8eb6-4f46-b30b-d573aa1cfe85\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642580283,1642580283),(184,'default','{\"uuid\":\"6137e815-a856-404b-835d-5d74bfdd5ce6\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"9581c741-0efe-41ee-905e-b993cc8d5b54\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750466,1642750466),(185,'default','{\"uuid\":\"93c2c81b-d19b-40ab-86df-e533ad247903\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"d0b6818b-0eea-4334-b568-00b6dd79a20f\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750722,1642750722),(186,'default','{\"uuid\":\"85211c30-98c9-48b6-ad0d-ddd0f2b76e9e\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"25bd1807-1144-49eb-be7b-2d69a6bf4f1c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750889,1642750889),(187,'default','{\"uuid\":\"5ef7a1ef-95df-402f-8e16-ed0aa1952954\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:4;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:27:\\\"project.engineer@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"533ac6c3-3aa5-423b-8054-e6e23ac8613b\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750889,1642750889),(188,'default','{\"uuid\":\"6cd105f7-3c5d-447d-ac3a-b43ac4b439a4\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:3;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"user001@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"f2f6fc71-3fe9-4e29-986b-d6050ffda018\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750889,1642750889),(189,'default','{\"uuid\":\"6f3d0cad-21b0-4d5e-b521-25fe823ae96d\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:6;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:17:\\\"safety@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"94b6be00-69f6-4e2b-b550-d26ced21f8cf\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750889,1642750889),(190,'default','{\"uuid\":\"11e4dd04-a439-4311-8fee-a0da250e5e30\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"10259b41-a719-4e79-968c-9075f46392e7\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750889,1642750889),(191,'default','{\"uuid\":\"a8da8a77-7d4b-4c23-b737-c8fd8a33992b\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"1eb8e5eb-9d9e-467b-bb1e-8ab804f25c00\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750889,1642750889),(192,'default','{\"uuid\":\"4911e9f1-c9c2-4658-8341-e85354fd83fe\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:43:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocumentRecurring\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"cc855087-e7b6-49b0-ad78-17a43d0b1e0d\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750928,1642750928),(193,'default','{\"uuid\":\"ce93a9c7-c8a4-41d5-b91e-09a35b315f14\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:4;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:43:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocumentRecurring\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:27:\\\"project.engineer@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"d46cb51a-4521-48c1-a14d-16ca43fdb44b\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750928,1642750928),(194,'default','{\"uuid\":\"e05153d0-6a83-4a17-8b83-dc44e15787e0\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:3;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:43:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocumentRecurring\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"user001@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"24d5929e-b185-436c-a18a-08a0735a03f8\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750928,1642750928),(195,'default','{\"uuid\":\"a23bfad5-01dd-4a05-bc0d-f290794e4d28\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:6;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:43:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocumentRecurring\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:17:\\\"safety@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"d28f6b92-3c52-4bbc-a973-c0610b7cf30b\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750928,1642750928),(196,'default','{\"uuid\":\"dc7d8060-ead7-4f2a-ae62-86e454f1de44\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:43:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocumentRecurring\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"ffb19aae-a6f1-45e6-8d08-7a5080ec9b31\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750928,1642750928),(197,'default','{\"uuid\":\"9c440c83-0bbd-4a35-b7ba-d99133f2be2b\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:43:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocumentRecurring\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"48aa53ad-f727-4123-bd89-aa75aeeb2918\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750928,1642750928),(198,'default','{\"uuid\":\"47440ccb-1a1a-455b-9bbf-1edb81a91b52\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"dfdbbf99-e373-4ef4-97d4-5f4fcb583d5c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642750951,1642750951),(199,'default','{\"uuid\":\"f3f8c94d-61e3-41be-82cd-0bcfcdfcd557\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:20;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"6394d73a-0b80-4488-8524-e76c49f1dbb8\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751103,1642751103),(200,'default','{\"uuid\":\"6d866fe5-7709-4689-a2ff-4fdd6ce8327e\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectSite\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"267b27b4-92e2-44f4-a2df-4c8ab68e94d4\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751103,1642751103),(201,'default','{\"uuid\":\"ca5d8491-11d2-4a50-96c0-8e37cc3bf76e\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:20;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"c91450b1-0212-4fbd-80a8-5c8d2f62562c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751113,1642751113),(202,'default','{\"uuid\":\"d4c23255-295f-4e74-b262-9aa3dbe40634\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectSite\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"84e106d0-4eac-47cd-bdbc-06ffed543358\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751113,1642751113),(203,'default','{\"uuid\":\"2285cf88-92df-4945-9c9f-e87602c5beff\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"e34133dc-9a77-4dfc-9507-97209ef6e774\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751247,1642751247),(204,'default','{\"uuid\":\"b90637f8-4af2-4d8e-936b-06295c1cca94\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:20;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"63bbb0d3-ee9d-4eeb-ac0e-bb2b17c805b4\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751266,1642751266),(205,'default','{\"uuid\":\"d52cd258-216d-42d0-8f99-5f473d3c3eca\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectSite\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"9a4d123a-b42e-48d1-82de-d7b0a4a31f5b\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751266,1642751266),(206,'default','{\"uuid\":\"7c2ba3f7-5d74-493e-8801-5985a4820576\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:20;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"1a727806-6ec8-4d0a-99c0-15ec13a5ae5c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751361,1642751361),(207,'default','{\"uuid\":\"78c6963f-2478-4bc2-9967-5d9aca5b93a0\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectSite\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"b3167d5f-d672-4cf2-9bda-fe9ee6456436\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751361,1642751361),(208,'default','{\"uuid\":\"211e4bc4-2acc-47b3-9d5d-6d878678aebe\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:31:\\\"App\\\\Models\\\\SemCorp\\\\SiteDocument\\\";s:2:\\\"id\\\";i:20;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"54413b24-2df9-416d-96fa-95feb6221636\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751386,1642751386),(209,'default','{\"uuid\":\"5cc13cc1-c557-4e8f-8984-2f6b31643197\",\"displayName\":\"App\\\\Notifications\\\\SiteDocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\SiteDocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:30:\\\"App\\\\Models\\\\SemCorp\\\\ProjectSite\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"a2972f60-e088-42b8-aa4d-b2627e65e391\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1642751386,1642751386),(210,'default','{\"uuid\":\"f6667310-d815-4c8d-a0b4-ff117dcaa8ad\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"a0ea25c6-2f0a-44b7-b8cf-5ea31e3647eb\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015129,1643015129),(211,'default','{\"uuid\":\"a29d1c3a-de69-4cd0-89ca-5fb45e2d9de7\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"8711d4aa-0d3f-4717-9bd3-bfa5b201982f\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015129,1643015129),(212,'default','{\"uuid\":\"99fd8306-031a-46eb-a9a2-39414b99d19b\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"88767338-9365-4f94-8863-58e77c7bb4f6\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015178,1643015178),(213,'default','{\"uuid\":\"0bb0194d-1e7c-46b9-a204-9dc46215dddf\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"c44cb93e-7b4e-457b-a555-17529b60e33d\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015178,1643015178),(214,'default','{\"uuid\":\"4c6f133e-53fe-4edd-8560-272dfe483f6f\",\"displayName\":\"App\\\\Notifications\\\\DocumentChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\DocumentChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"672c80b0-5676-475b-bd81-0c46fba53995\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015186,1643015186),(215,'default','{\"uuid\":\"2eccf51e-3f81-4789-ba57-4fa7ee0d890b\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"4f43c72a-9087-4f50-beda-c4158fc9ac4c\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015215,1643015215),(216,'default','{\"uuid\":\"4473a5c7-5d2f-4026-b5d8-127ab5421b60\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"cbe9b5d5-2357-4f81-b557-5444420e2cc5\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015215,1643015215),(217,'default','{\"uuid\":\"2f7faf22-6f77-428f-869b-3977bf7ba8cc\",\"displayName\":\"App\\\\Notifications\\\\DocumentChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\DocumentChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"1d2ffdc5-431d-4a9c-83ca-2d3cd8e11873\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015238,1643015238),(218,'default','{\"uuid\":\"55af3189-8b2e-446e-8369-dcfa4343d8d9\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"67ea1e2d-46d2-4146-94a3-40716a46fbcd\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015247,1643015247),(219,'default','{\"uuid\":\"f06fb0ed-3120-42ec-8302-dcca92b88961\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"994a1c48-575e-418c-8c4e-8d2ca87c99fd\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015247,1643015247),(220,'default','{\"uuid\":\"b1087363-ae13-4d53-a913-da457d95f65f\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"ceedeef4-9bf3-4a9a-9ebe-9ef7f9df268e\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015275,1643015275),(221,'default','{\"uuid\":\"60d8c4c5-4485-415f-9bc5-f184dd0e2a72\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:11;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"e272902f-65c0-4ab0-803a-8199ff814a88\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015275,1643015275),(222,'default','{\"uuid\":\"54d65bbf-07b4-4f67-a43c-a15f7feb103c\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"eef06a20-5e2f-4d57-956a-cc55c6a45e49\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015295,1643015295),(223,'default','{\"uuid\":\"b954fe07-cd31-4c26-9c6a-c21c05865ab0\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"10539e49-3dd6-456e-be82-bdf7dc54f88f\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015295,1643015295),(224,'default','{\"uuid\":\"37922a64-1b4d-4f4d-81fa-89b8ec5afbf2\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"cd374b34-de72-451f-a682-06f96c5ad6eb\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015324,1643015324),(225,'default','{\"uuid\":\"0977a78e-e470-4249-af81-5243d8e3f82d\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"a36c610a-c450-4fd8-9d3d-9c8c1ab4edb5\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015324,1643015324),(226,'default','{\"uuid\":\"677a42c8-9a23-4fd6-8a78-9e12991a240a\",\"displayName\":\"App\\\\Notifications\\\\DocumentChangeStatusMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:5;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:42:\\\"App\\\\Notifications\\\\DocumentChangeStatusMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:26:\\\"project.manager@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"a5a31105-1fad-4bc0-a3dd-dc6a0f4647e8\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015329,1643015329),(227,'default','{\"uuid\":\"9740e876-1f01-420b-afb7-18d2969cfc53\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"b18e6c31-2804-4349-a5d5-230ec62ce43f\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015351,1643015351),(228,'default','{\"uuid\":\"3cf4d091-613f-4319-9e3c-8538adc75fcc\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"37946d36-957e-42a0-adce-f35d8fad20de\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015351,1643015351),(229,'default','{\"uuid\":\"d92a4be1-ba24-4209-88c8-091bff0d8764\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:18:\\\"ken.wee@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"57ae9619-3ab6-417d-a39f-6363eb42725a\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015381,1643015381),(230,'default','{\"uuid\":\"3a82283d-1581-4677-8df3-5a8622c57055\",\"displayName\":\"App\\\\Notifications\\\\DocumentUploadMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":16:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:2;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\DocumentUploadMail\\\":13:{s:7:\\\"\\u0000*\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:34:\\\"App\\\\Models\\\\SemCorp\\\\ProjectDocument\\\";s:2:\\\"id\\\";i:12;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:10:\\\"\\u0000*\\u0000send_to\\\";s:16:\\\"admin@imttech.co\\\";s:2:\\\"id\\\";s:36:\\\"ae573e3b-9593-4fe8-b7d7-aa71cde3b7fe\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:17:\\\"shouldBeEncrypted\\\";b:0;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1643015381,1643015381);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (9,'2014_10_12_000000_create_users_table',1),(10,'2014_10_12_100000_create_password_resets_table',1),(11,'2016_06_01_000001_create_oauth_auth_codes_table',1),(12,'2016_06_01_000002_create_oauth_access_tokens_table',1),(13,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(14,'2016_06_01_000004_create_oauth_clients_table',1),(15,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(16,'2019_08_19_000000_create_failed_jobs_table',1),(17,'2021_05_10_065305_create_c_e_o_s_table',2),(18,'2021_12_17_071630_create_jobs_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `milestone_template_details`
--

DROP TABLE IF EXISTS `milestone_template_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `milestone_template_details` (
  `milestone_template_detail_id` bigint NOT NULL AUTO_INCREMENT,
  `milestone_template_id` bigint DEFAULT NULL,
  `milestone_template_title` varchar(100) DEFAULT NULL,
  `milestone_template_sequence` int DEFAULT '1',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`milestone_template_detail_id`),
  KEY `FK_ref_milestone_template_details_created_by_idx` (`created_by`),
  KEY `FK_ref_milestone_template_details_milestone_template_id_idx` (`milestone_template_id`),
  CONSTRAINT `FK_ref_milestone_template_details_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_milestone_template_details_milestone_template_id` FOREIGN KEY (`milestone_template_id`) REFERENCES `milestone_templates` (`milestone_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `milestone_template_details`
--

LOCK TABLES `milestone_template_details` WRITE;
/*!40000 ALTER TABLE `milestone_template_details` DISABLE KEYS */;
INSERT INTO `milestone_template_details` VALUES (1,1,'design',1,5,'2022-01-06 06:49:35','2022-01-06 06:49:35',1,NULL),(2,1,'installation',2,5,'2022-01-06 06:49:45','2022-01-06 06:49:45',1,NULL);
/*!40000 ALTER TABLE `milestone_template_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `milestone_template_details_insert` AFTER INSERT ON `milestone_template_details` FOR EACH ROW BEGIN
  
	Select milestone_details_no into @milestone_details_no from milestone_templates where milestone_template_id = NEW.milestone_template_id;
    
    SET @milestone_details_no = @milestone_details_no + 1;
    
    Update milestone_templates
    SET
    milestone_details_no = @milestone_details_no
    Where milestone_template_id = NEW.milestone_template_id;
       
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `milestone_template_details_delete` AFTER DELETE ON `milestone_template_details` FOR EACH ROW BEGIN
  
	Select milestone_details_no into @milestone_details_no from milestone_templates where milestone_template_id = OLD.milestone_template_id;
    
    SET @milestone_details_no = @milestone_details_no - 1;
    
    Update milestone_templates
    SET
    milestone_details_no = @milestone_details_no
    Where milestone_template_id = OLD.milestone_template_id;
       
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `milestone_templates`
--

DROP TABLE IF EXISTS `milestone_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `milestone_templates` (
  `milestone_template_id` bigint NOT NULL AUTO_INCREMENT,
  `milestone_template_name` varchar(100) DEFAULT NULL,
  `milestone_details_no` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`milestone_template_id`),
  KEY `FK_ref_milestone_templates_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_milestone_templates_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `milestone_templates`
--

LOCK TABLES `milestone_templates` WRITE;
/*!40000 ALTER TABLE `milestone_templates` DISABLE KEYS */;
INSERT INTO `milestone_templates` VALUES (1,'template-1',2,5,'2022-01-06 06:49:21','2022-01-06 06:49:45'),(2,'design',0,5,'2022-01-06 06:49:26','2022-01-06 06:49:26');
/*!40000 ALTER TABLE `milestone_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `modules` (
  `module_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `module_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `module_information` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`module_code`),
  KEY `FK_ref_modules_created_by` (`created_by`),
  CONSTRAINT `FK_ref_modules_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES ('BUILDING','Customer BUILDING Module','All Customer BUILDING information',1,'2021-06-15 17:09:17','2021-06-15 17:09:17'),('CLUSTER','Customer CLUSTER / Site Module','All Customer CLUSTER / Site information',1,'2021-06-15 16:44:31','2021-06-15 16:44:31'),('COMPANY','Customer COMPANY Module','All Customer Company information',1,'2021-06-13 23:01:52','2021-06-14 17:00:37'),('CONFIGTYPE','Configuration Type Module','To set the Customer Subscription belong to which Config e.g : Saas , imaintain',1,'2021-06-13 22:05:13','2021-06-13 22:05:13'),('CONTRACTOR','SEMCORP  CONTRACTOR','SEMCORP CONTRCATOR',NULL,'2021-12-02 16:21:52','2021-12-02 16:21:52'),('CUSTOMER','Customer Module','Customer Information Module',1,'2021-06-08 20:38:51','2021-06-08 20:38:51'),('DEPARTMENT','Customer DEPARTMENT Module','All Customer DEPARTMENT information',1,'2021-06-15 17:33:33','2021-06-15 17:33:33'),('DEVELOPER','Customer DEVELOPER Module','All Customer Developers information',1,'2021-06-14 00:23:20','2021-06-14 00:23:20'),('DOCUMENT','DOCUMENT','DOCUMENT',NULL,'2021-12-05 17:19:43','2021-12-05 17:19:43'),('DOCUMENT_TEMPLATE','DOCUMENT Template','SEMCORP DOCUMENT Template',1,'2021-07-21 17:32:14','2021-07-21 17:36:03'),('DOCUMENT_TEMPLATE_DETAIL','DOCUMENT Template Detail','SEMCORP DOCUMENT Template Detail',1,'2021-07-21 20:09:10','2021-07-21 20:09:10'),('FLOOR','Customer FLOOR Module','All Customer FLOOR information',1,'2021-06-15 17:59:40','2021-06-15 17:59:40'),('MILESTONE_TEMPLATE','Milestone Template','SEMCORP Milestone Template',1,'2021-07-21 17:32:14','2021-07-21 17:36:03'),('MILESTONE_TEMPLATE_DETAIL','Milestone Template Detail','SEMCORP Milestone Template Detail',1,'2021-07-21 20:09:10','2021-07-21 20:09:10'),('MODULE','Module Module','Lookup for All the Module in Role Access Setting',1,'2021-06-09 00:07:00','2021-06-09 00:07:00'),('PROJECT','Project Information','All PROJECT information',1,'2021-06-14 17:46:01','2021-12-16 12:04:40'),('PROJECT_CATEGORY','PROJECT Category','SEMCORP PROJECT CATEGORY',1,'2021-10-15 11:53:28','2021-10-15 11:55:29'),('PROJECT_DOCUMENT','PROJECT LEVEL DOCUMENT','SEMCORP PROJECT LEVEL DOCUMENT',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('PROJECT_DOCUMENT_APPROVAL','PROJECT LEVEL DOCUMENT APPROVAL','SEMCORP PROJECT LEVEL DOCUMENT APPROVAL',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('PROJECT_DOCUMENT_RECURRING','PROJECT LEVEL DOCUMENT RECURRING','SEMCORP PROJECT LEVEL DOCUMENT RECURRING',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('PROJECT_DOCUMENT_TEMPLATE','PROJECT LEVEL DOCUMENT Template','SEMCORP PROJECT LEVEL DOCUMENT Template',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('PROJECT_DOCUMENT_TEMPLATE_DETAIL','PROJECT LEVEL DOCUMENT Template Details','SEMCORP PROJECT LEVEL DOCUMENT Template Details',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('PROJECT_DOCUMENT_UPLOADS','PROJECT LEVEL DOCUMENT UPLOADS','SEMCORP PROJECT LEVEL DOCUMENT UPLOADS',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('PROJECT_GROUP','PROJECT GROUP Module','All PROJECT GROUP  information',1,'2021-06-14 17:46:01','2021-06-14 17:46:01'),('PROJECT_MILESTONE','PROJECT MILESTONE','SEMCORP PROJECT MILESTONE',1,'2021-10-15 11:53:28','2021-10-15 11:55:29'),('PROJECT_SITE','PROJECT SITE Module','All PROJECT SITE information',1,'2021-06-14 17:46:01','2021-06-14 17:46:01'),('PROJECT_TASK','PROJECT TASK','SEMCORP PROJECT TASK',1,'2021-07-21 17:32:14','2021-07-21 17:36:03'),('PROJECT_TASK_TEMPLATE','PROJECT TASK Template','SEMCORP PROJECT TASK Template',1,'2021-07-21 17:32:14','2021-07-21 17:36:03'),('PROJECT_TASK_TEMPLATE_DETAIL','PROJECT TASK Template Detail','SEMCORP PROJECT TASK Template Detail',1,'2021-07-21 20:09:10','2021-07-21 20:09:10'),('PROJECT_TYPE','PROJECT  Type','SEMCORP PROJECT Type',1,'2021-10-15 11:53:28','2021-10-15 11:55:29'),('ROLE','Role Module','User Role Module',1,'2021-06-06 01:08:49','2021-06-06 01:08:49'),('ROLE_ACCESS','Role Access Module','Moule to Bind Role & Role Access Together with Module',1,'2021-06-09 00:07:14','2021-06-09 00:07:14'),('SITE_CONTRACTOR','SITE_CONTRACTOR','SITE_CONTRACTOR',1,'2021-12-06 19:34:14','2021-12-06 19:34:14'),('SITE_DOCUMENT','SITE LEVEL DOCUMENT','SEMCORP SITE LEVEL DOCUMENT',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('SITE_DOCUMENT_APPROVAL','SITE LEVEL DOCUMENT APPROVAL','SEMCORP SITE LEVEL DOCUMENT APPROVAL',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('SITE_DOCUMENT_RECURRING','SITE LEVEL DOCUMENT RECURRING','SEMCORP SITE LEVEL DOCUMENT RECURRING',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('SITE_DOCUMENT_UPLOADS','SITE LEVEL DOCUMENT UPLOADS','SEMCORP SITE LEVEL DOCUMENT UPLOADS',1,'2021-07-21 17:32:14','2021-10-23 16:33:33'),('STATUS','Status Module','Global Status Lookup information',1,'2021-06-14 16:40:59','2021-06-14 17:45:28'),('SUBSCRIPTION','Customer Subscription Module','All the Customer Subscription Information',1,'2021-06-08 20:41:58','2021-06-08 12:43:44'),('TASK','Customer TASK Module','All Customer TASK information',1,'2021-06-18 17:46:52','2021-06-18 17:46:52'),('TASK_ATTACHMENT_STATUS','Customer TASK_ATTACHMENT_STATUS Module','All Customer TASK_ATTACHMENT_STATUS information',1,'2021-06-18 16:20:50','2021-06-18 16:20:50'),('TASK_CATEGORY','Customer TASK_CATEGORY Module','Setting for Task Category',1,'2021-06-17 18:30:33','2021-06-17 19:29:27'),('TASK_MILESTONE_MODE','TASK MileStone Mode Module','Setting for Task Milestone Mode Currently is just : 1. STATUS, 2. CHECKLIST',1,'2021-06-15 21:20:03','2021-06-15 21:20:03'),('TASK_PRIORITY','Customer TASK_PRIORITY Module','Setting for Task Priority',1,'2021-06-17 19:27:54','2021-06-17 19:29:27'),('TASK_STATUS','TASK STATUS Module','Setting for Task Status',1,'2021-06-15 21:48:32','2021-06-17 19:29:27'),('TASK_TEMPLATE','TASK Template','SEMCORP TASK Template',1,'2021-07-21 17:32:14','2021-07-21 17:36:03'),('TASK_TEMPLATE_DETAIL','Task Template Detail','SEMCORP Task Template Detail',1,'2021-07-21 20:09:10','2021-07-21 20:09:10'),('UNIT','Customer UNIT Module','All Customer UNIT information',1,'2021-06-15 20:24:54','2021-06-15 20:24:54'),('USER','User Module','User Registration, Login',1,'2021-06-06 01:06:41','2021-06-08 13:54:21');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `client_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('02dd26a5f217fa87aa090a284fb1763485430467bf0d0a3a9c204318eed5e671bdcb913b5ec84704',2,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-20 23:20:20','2022-01-20 23:20:20','2022-02-20 07:20:20'),('050145d87f8914a639b3e81aba165c00c320775e68577731ad22718181b42778d6db85ce0f9e5d7a',5,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-21 08:18:11','2022-01-21 08:18:11','2022-02-20 16:18:11'),('1d4dad3b0f7d2cba780d695a0b2fe5fcb45aaf2ed4368ca8810c3a161e333dcb1d70f6c3a8cb396b',5,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-17 07:41:23','2022-01-17 07:41:23','2022-02-16 15:41:23'),('2841af9a767050c97d26928cf7944bb14006d4afa445d4e3611c4dd59f2640385a97d011a88532e0',1,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-17 07:24:43','2022-01-17 07:24:43','2022-02-16 15:24:43'),('4b8f2ca5e07bc9e4bc463832e6f69d62f8e981e103e0a74d0428ee46486a3f144e6967a79edc356d',2,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-24 00:17:08','2022-01-24 00:17:08','2022-02-23 08:17:08'),('6cbe105df99edb1c7a98a6737f22267182f9ee579269e70e8e59b43deddfc38dfcc4cee937ab088e',5,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-18 23:31:27','2022-01-18 23:31:27','2022-02-18 07:31:27'),('8c4a8b85181ff9520a2bf41b1ec6973a972d86ff984ff13c474b7928a673b483ceecc50d758edd2a',5,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-21 08:06:32','2022-01-21 08:06:32','2022-02-20 16:06:32'),('993494a55359aeaa774e78eef31bc5cb8e77f99f5ab0778506b26a8f2ac2450c748063c5c27651e9',1,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-17 07:23:37','2022-01-17 07:23:37','2022-02-16 15:23:37'),('a319ad3a2e57313cd49a4fa5a5bb5376ecbd3c9948d7cb8fcbe0807705b6ffa98f4a7fadd41aec81',2,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-24 00:17:26','2022-01-24 00:17:26','2022-02-23 08:17:26'),('b2d3754d1c8cc3b1832f2ffe7b2183bea04fe90c05a67de1d0b7e45124fb8cdf8e2a3fae95b5c629',5,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-20 23:33:43','2022-01-20 23:33:43','2022-02-20 07:33:43'),('c146c5be57c92015725a3d3e9424392abf8af9c6bc2e5bd516f887eb2d77f0eba82ab26acd6e210b',2,'93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'[\"*\"]',0,'2022-01-20 23:33:32','2022-01-20 23:33:32','2022-02-20 07:33:32');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `client_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_clients` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('93653827-36a0-47cd-a552-9675fb32d0fd',NULL,'Laravel Personal Access Client','59VvKtfXVzE2K3IPgwvzg5sRdZtNbxdG4RFQXttw',NULL,'http://localhost',1,0,0,'2021-05-09 22:50:30','2021-05-09 22:50:30'),('93653827-41fa-4dc4-bc42-7349280e37a7',NULL,'Laravel Password Grant Client','cyLPlmkgjRkjiVjz1z1OITA47id883CHl6QYqZX2','users','http://localhost',0,1,0,'2021-05-09 22:50:30','2021-05-09 22:50:30');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `client_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,'93653827-36a0-47cd-a552-9675fb32d0fd','2021-05-09 22:50:30','2021-05-09 22:50:30');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('00998df99767495d2472181267ec5d67393ae8dedba801902e4af3c5c9613f79781b56aeb1cc6a2b','e7872d994b7e951d00e2aee2e7310161d09bcb6d427e283dcf7ae58b75c7ebe0e5122c26cca57912',0,'2021-06-06 15:59:51'),('01552a45bef30b89ff2d3d8b46bee4f053af59f1bdeb6b31752d72b2901b33dbf6664be839eec434','dfbb494a8a58c97a5527eeb266c33486b3b1fba99fd55c096939b3c4dcfcb243288af2d7a7776273',0,'2021-12-15 06:27:53'),('01654b958f5f03892abc2435b545374b0cdbd3108436f299b109bee185867f88d53550c4c4c0af5b','d973efc0ce0acea79492c76be5dd48c9ad9afd466a0a1c4a035cb9c9c3e0e33ef03b1e0722e4f439',0,'2021-12-15 06:39:25'),('01c19b42e2dce3abe3e20ae895d786d3957b54621e2a05abdd926fa3d82baaf528c922975fa6b857','30475b6390eaf2b06d94751a0e0047e1a16450a2160704020d0527c440a99a0a5c5ed1fc1d0bdcac',0,'2021-08-18 15:19:26'),('01c762aaa59a63572aed04a288b2449467cfb5e9e4adc2cb574f76583f4aaae514cb4e1acebdad32','693c31e1676c3dc7888cbdcd2d59fadf40dceb4591cc537a716a88c51d9ea9266b9d30af74cc21ec',0,'2021-12-07 10:18:30'),('01ea9b2622ce961d985d4947c3396e1857352ceb2235e89d17c6c4b68e7936f67def5d02aeb705fc','6e75ec3881268a83f5ffeee816d5551e9a6db0ce34e9187527eb60aaf4f7dded5d61927bb9499683',0,'2021-07-02 10:10:42'),('01f8306ed8d774926ae5255dc73b466baf10968a9d74e62d4f2f75bc83437f356c44a4af36d9abe9','d89a20902df65f28aecfbecdbb681542007e763644b9435d66cb48c95217c909d1c228dff906f526',0,'2021-12-30 02:28:14'),('0287d25903806f46a86fec292f5a25fe276c3cc6ba7e65d01dd1128486b20399da93ff2fb12cc9af','942ea4f895c1e0a6ff64ba7067713630be9b6147cb624299db0721593029b85234cb427e1d6d9c5a',0,'2021-06-29 14:32:29'),('0296f2097c6ffceffc5b3d610041381d26f655d43f2f1c67b87c0518b71606fb4e8773add3ef030d','dc76adaaf0015615561e61bd2f14721217b0330af57ea51fcd0e65db5def7b1cd9812a9aca85abb9',0,'2021-07-02 09:56:28'),('0298af92cf58232bd9325be52c7a9dd740578f01a6beefd9e5c8fc33344a2155a9bba1bcf3bb3e71','23ef0ed748aadf21de3cb57247028d16c311613c16450b8ed84338f422287a5c86f3becd7f83d0e3',0,'2021-08-20 01:42:11'),('02b41229f4e2ff2ed64f476205e4b52e25c2762dacb591b2ddc86e3f3e37355602099e56f841cced','0032288682b0e9ae1fa6366038252c61cd6a30354bab8976381f11309749bc7380aa46dfef228941',0,'2022-01-13 08:40:34'),('036f0448b7285e47fcdb419d3501aad4a4f46016c780505982f4d5c05f5acd351274a62d1d958d64','e6976b06a0629d225bc8007e1af662d1441ca538093b9953c76426f2770799f443047c7d50e2d053',0,'2021-12-15 06:40:54'),('03bd5e578c044111daf7ab3b99e9fb5d4aaf75689d62ceb76f25922ba2675c7ab098727d0e48db9e','bc63ab67ae8ff08a966e48e8cd7a9a3937e456a85fc4253f96faa4fa040541809e40d7c08bda1908',0,'2021-12-23 20:26:49'),('040683027aa35ea9cc31eb14206019c7af8110640ae6c833f60b8c6cdac9dd9c330730af004719a4','f1eaef4c20499d5575aaebb1d84e11b06c83701133954a61d474c3e3077053dc51d611606d3960b5',0,'2021-07-02 10:09:45'),('04d9edb8f89c38f31e639700a2a9fbb74af032c7b6a5ac1427337d075f8f140097c43749c6dd8073','638fd1b87b83691e2da50d6cf2e07099d6d20637b2c36a597b25dad8a0a93e09233fb5d88b93c618',0,'2021-12-21 16:52:52'),('0502af63ffc4620cb69f13db24d017182d83a099555aad9b7a42a16278e263900911b39f6cb6243e','b45e2c75178b4a33f8543d69280f7c96a14c6cacbc3a09fa0aab5dfb94de7741ce63b6f57b323d65',0,'2021-12-10 06:11:27'),('05a592f756dddbac7606ceabf88927540d7ddd4e9d50b1a355ee60e06c1c820c40f17f515682aabd','7d20df9b188dc1acb8ec9a77010f72d9fc59d101638d2f5ab3b1ac966c75a51008dd6373ae387e16',0,'2021-08-21 00:30:56'),('05caa68c06b8593802d00b5e123d52a9b2100655a2096d2e3b5fc30ef5db61c2505970fa9f555782','66ab3f47b07498144bebf3c35cd3c5f3d6457b52783e1f9f294ebe75a983eded982c20dc8365d55e',0,'2021-06-29 08:50:06'),('05d660adaf3ae2fdefd376dc382715d2bf10e10cae6560f6003c38764d6fb3232045edd05a707251','449e8b462a1f7eb476cae09e20929a5c486a427ee1bdc8b37169966e3021ec241060a15ac9cbe88c',0,'2021-10-02 09:47:01'),('05db33e22ab7eea6181c3a513748066ce674badde89de50f123c4ef43ab15ceaa693d1008abeb456','7dc777a0850fe2273c3f9c0c082560d513fd80605854d0e8bdaa15fbe8ddbd90cbf0ffbb17aadbdf',0,'2021-06-19 18:31:01'),('05e01480fb401dd85ad6792cae64726e7a5597a4835509ece45b9f208bc28943c81daa8ffcba254f','ccb9a3897d01e8a40b0b86d7f9d092b6e3738cd74c2f5df44f36d3e4dcc4560eccca02f0a67c6f07',0,'2021-10-27 04:55:46'),('06456deb557b20963d541e10489bdfce3a102dc5a1846b0d6a1b6037ada8235d83a7fc15a8643051','082ccbb5545f06b43f689a129078b66147b0e762d5e79a6267864a0754d99dd6f2a342232a4662c1',0,'2021-12-06 12:44:31'),('06547a45654e3120d5bfc4172f67e3b7a43e75cdab09b7ea9e8627f9189a4e1018e5d9d97da56a5f','e36d867ca6426f6ebdc2102e78d5c2ee723832bbf73cdc014370d251a22c66aba435b6eb8b14ba2c',0,'2021-06-19 08:17:48'),('06913bcc1f8ea13f08d5442af671e23382b4fe18de8c96de14ba1f9311bed84ae1334c00879f09fb','76dbca256e79852f66605e8f5effc15a9393176ff5ae03bc4f67efb57c493387ea7fb01753aebeee',0,'2021-09-21 07:13:13'),('069f32aefb26b9a8f401ae140f021eaf01e6d575b99a0864c5782ce607123c73be425cbf7ae724a1','5c8b06a06aadec6672328a865c03ad9530a2939a6f9086056feffeeb231e9b1aadf3a54a9c8c5968',0,'2022-01-05 14:39:41'),('06a6784b1ab57067d060949767c10f3fd591f8aa176aed100713c500df670613ded52ca0293831df','8134055fd81c15308a5ff0bc7d048a38d8f8aee923f5afdcf43c5e75e2c04d3cba83d59acfa53398',0,'2021-08-21 02:02:39'),('06adfb680622268cbdbe999212da1e3d5651867bb912ae6ad4ef3ea2a429273c6dd3f39e0e124d81','c4e63d1a9e57d82b3720087f816ba878c2ac5ee33d87f5a6bb2765fab0b139fb47d7cb81b30f8ea5',0,'2021-06-29 12:26:00'),('06bd47355b5bc648dc425c212e1ffe9f3fbca4a2e0bd47cd38f23b2a9324c7bed8a3a90ca7c56933','d2cd0eb43f85f004090fb18016fba1bb99f9d887b560b7337cc12a67886995b44c3905972ce42ac5',0,'2021-07-03 09:48:31'),('07619402824ac84f9239c9658237bfc81b70822cb24facb32c6e6312c7830cf20197e616c14a71c5','2118d153a735cb7c24bb1cf5809659c5742c8c7b371f354fb36e88ae3b7ff5f6a38a7e1fb6642e0c',0,'2021-11-21 06:20:59'),('07923b00f7eace1f73bfd28d7374d9d7c4fbb5c394ca84b0b524fba9594f19c40c2c568ca8bcbdee','216ba99d310be454359cfa9f1c3e68487a35d606840e0f3ddd2c8960eac529886cd6cd6dfea5016a',0,'2021-12-07 07:38:12'),('0802fbaccac82df9b6c6b63a130d41042c13099bf60f2474ef28ebe19c88d421c8ee04818b4502a9','258874ec6cdfac20d69a7557fd166592f79e1529eacc63d65893946e7e41657fd27e2c90f76c25d7',0,'2021-12-28 06:35:44'),('081c47e0a98c71af2f70f9dfb79defcdc34c78fb3b928e1633396b76f4ef8d69ff876035807f5c0f','b0af894b4346dcc756e0024a03bc228449945d1ca4d94c02afeddc1b78f3844606c862f40dd0d61c',0,'2021-08-17 07:07:00'),('083de1a2874ec9023d8f8c31b33854664c3770f17caef6c503e2ec51de10f4c5355e332ab52bd893','e11a13e8c9539532d79bde9db17dd0786a6bca0e0d6a85391910e57a0e012ab8905de841d73809db',0,'2021-06-29 10:08:39'),('08e2ab9db691a8a2a2bdebf0cca3e84bb62258e696c5d8f156542f36cc6077eaccd735b4b304f839','fa92202210c636905f27bdb9a06161213a4f317bb2da6fdf1d50e9be9317b6660ad6c83e1eed1c15',0,'2021-12-16 06:19:20'),('090238edfe4a406e42122a7c2089971671292385f68179e27f98afe33a81b880e754934b63fd79c6','bde3b160d7893ac55979538712b2d4ac2aaea88ec8d5e1ab65957815975cb17ba48377c6a7ff8038',0,'2021-06-29 08:31:31'),('090ae8b1bd185e54d3b754298981a650f3ebc39dac68a5d9b596d5a6ab0d8b47a86790694b0029cd','33a15f620297fc42139f6af8e4998f9e92184cbe18a7e76f02302e7e6dc870e741df6e201a009d09',0,'2021-12-19 16:14:20'),('094c39cad0da6ed714b2b385a9902e265f6b084eef1199ccd089f6ffd9457dd548cba79c91ed7efa','2d2cdb8fe120bf9d0eda2bdb4d52421503d06830636dd17ac11a191d7e68bb6da80be3a178a5a00b',0,'2021-07-04 14:15:02'),('09e1914fd0c932af25792e4fb09d8328764d71bc7bed317eb10fb5083863ca76e302d927b128edfe','de21dd99af3ae6e4ee5e89cd0f680d7068e84ea148e56337e16b25ffc19d21d0f7af6939c5f50047',0,'2021-12-06 12:32:13'),('0a315c9041d77a4a9f77cd0a3a51d3429dc37e3098364cc06c6b7e7f7051380fa1f045bbc3caa3ff','7de0b57d0bd296cc5f5059c63f92ceb5df255432dfa03ab5231a76e79c4b65323e0c803b14d120f7',0,'2021-09-24 02:32:59'),('0abbf659d946e781c1a1bfa632eed6e7c55d7abfa5f9cbd80e360050278c4c50f8830984f9f21824','c53eae6dfa878ceee44f91470736cf2f2704a0c33be93e55979a072303903ca3459e9ed0f305b111',0,'2021-12-28 03:39:56'),('0ad38293771827b57947ac69f807261d253868cd646750102a2dfa1a23ed6995666466cb8d6326f5','0fe10c3eacc171518122e739c68355fbeaed4ce757bc7b562cc5683716fba13000debcc2f67f4fac',0,'2021-11-22 09:26:46'),('0ae3b0588f7b81d7a2475cb6d6ce897d2b12e8c764e35ac18136cea6c86cbf6caf47137c54c91746','4e8e5ddd8548284db8cc38fabdad887114a875cf877f4cc53cefc9f239f72c9daeb885caa2362150',0,'2021-12-03 08:16:00'),('0b5d10d17eed3c2b0f219ad58990f1618b7bc02be5fc460cb3d782cabb44264fc9658dbe854238bf','9c48cd1730daf7f4b91955c95dee596fdf2334dd3390c869340ece032edb6c236f4a3e6f746ebe8c',0,'2021-06-29 10:02:00'),('0b8e335ed410c19b172c2313246cbf7e382fd94e960a084f4b0da7a9db20848046ce6ec2189d8385','f9240d84d8924edf238963d080a95f92eac1b75a5a9e42ce7666fa848def0bc6f96805cc4a1aa56f',0,'2021-08-16 19:01:17'),('0bc10cd8368c5da1a4ddb3382fc01d5c36ac0e02a17b082becf90cb4cfecc2084370ae2c7468c9d4','0f9d2fb6dcb4d0604f1291fb963b1a099a1e5d5e0f4cbe22dc37ea427ec700cb224128a33a067ef2',0,'2021-06-29 13:33:19'),('0bda03dd1ede6da1ba3e14ef41628446fab46ebd8b257339bae92f3b54e03445516bc258da129759','0c07256a615d78e0719af9bd46f8c8d391beed03e4afb12f6cb7df3a39a7ee63a3526f2836b3f4dd',0,'2021-08-03 06:45:09'),('0c90af6e93e250df267b9942974e8386cfd3b14caa2dee551638c498cbb0a0c041b180d2dbcab16f','0508cedfa91ba75fc2d1afdc4ebcfd4c2f92f98f90e867cd69b03fdd5ca9325247462d6b01f72ef3',0,'2021-07-06 16:04:48'),('0c94051fe2d74c2ef1068f62316bf635abc25d7ce1e81c9f0570a264466a060f2b18528680a0b712','6cb5eca946de20520362bf55276a7420ce53e31d8e5adfe18da72ae832fc1965cef633b0ddc675af',0,'2021-07-30 09:43:23'),('0ce87168b99b394823962c87666b38e35c171762933d65f9199c013b4e0646997586b2a2aaad6a36','31eb2b793f54a9f3bdbf800af04234b64068f5649b9689ef841e25f44d173f64bdefd741a12ce5f3',0,'2021-07-22 12:08:28'),('0cebf69130484842aa477317764c9d50431b5611f8212ead52ecc1838ec333dc09cad9912e4ee937','af82194d7a35e1084b2278c2559e88f89735bd329092c4403a30d4c1081856cdbaf86e2b93ee2fa0',0,'2021-12-03 07:43:57'),('0d28e28a3f0d2939805c380a972538552520012be4504e1035be243610f08e7b3b6caf3b80196f80','42a81d743c70e02abc5a00889e68339983c72fefff7c11d6db3cf1c9590444c87ac166857baa337a',0,'2021-07-04 14:19:22'),('0d63c0a66d9919fd482d56056eac259c779192b6e89551e1c87fee3f3daf26ca4253512f5ba51c74','85b1eeddf7244c785f91c15502504a85bdd0ff8a07d6c0368e69a4e519946dfb4624bee05fc1b789',0,'2021-07-04 07:27:26'),('0d9d2118384b629afe1d9b40bad8299dca34e8a9b4a9b478295247c7c1b4c1189c0b5ef81ba42c41','ac6eb500df584794da42420766e66b71790582271ad6125a871d1754705b1a51281d0f2642c40eda',0,'2021-12-23 12:52:28'),('0db83b511c7ab5cd8b67adb0a89beeab12522f9f9c8422a2d952ed4b6a7f157eca12b7a02bfa5555','00658225d0c3b4b5e4217afaa93e901c28b373abec953b87c7d1e4175fa7dba839b15fe96dc64b9e',0,'2021-08-17 14:01:42'),('0dcb783fe4fdf6d03f07ee8685b73f37a3b2787640507ce187c069c9c160943823629c8a235ec172','b0eee2053f5ee25d1d59224a147ea17c8864cbe3549e29b25b6de09113d83d4146e00521a3030ecd',0,'2021-12-20 10:14:24'),('0de538b148c0fe97d98da09616afa389c81f56a1d89c51ad3c90d71bca2146c2d8bab95ad75dcd41','df5bf335f4bcf66cbd681d7270ba0bd8dce5cb02add98ee365d93b1dc103212585b28c4996efd89e',0,'2021-12-03 08:12:55'),('0dfc36c5ae1d6a6d6fdb47f6eaf63eb8016f9e5e2dc796239c707ce7129022c51bf70ac22fc74243','ee205295c8f9294c94ded2d9bbb0bf45e5510225e0e4a71d619953a1d50a83d1f3345449ac6d01ff',0,'2021-12-21 16:48:58'),('0e07c38ef8f5c0ce1f03395a18b22a72e9bab4f246f104460b16063e348f8432a644b4f6b690b125','6089d9f638e26235e5a6d8fc94f7762ca945079c89381d347869bddaebacb800dc5f44846b6cc5f0',0,'2021-12-03 07:10:02'),('0e4f2a00576ebe349ba2e2ec8a813354e7422e376477fbd36293bf26d95ac37ae03005c3fee39208','472f08baf285cc35af10360d653559a22e936f9a663508377285a23522d7de3309c75481966dbf34',0,'2021-06-29 08:28:35'),('0eccd97f10a03cef2fb4e6ac85f1a1663a51e1fbefb036ae08edbde5a080a72520f71c393e9934de','819c5952c107a6f064b3ad869449d11ba13290b887fac8a85bfe55f99a1156156278ef71f9dfb246',0,'2021-12-15 06:28:56'),('0f3238c6556f7584e473fb9ed47b504b5646036017d0c54be766bdb3026a30d40ce1bd5792633f8c','5bffddda749746016d86c1a4bfb07438bdf8ca9134a56657a621fe3872ee4ac9947cd78286c0671c',0,'2021-11-03 09:35:34'),('0f4cf44b16ea808d9f3f10655b0817a86f6f8e9b77fcdbefee2710a92ca99ca8f9070c63c4fabf2d','d59f97cae89ff1ae95cd9e13913116d7146d7a8f534ffea26639583e51b607509495c628634c3f9c',0,'2021-12-19 16:54:30'),('0f56f261bc2955c5fcf97a10a6d206353204fbd6cae6ef80b3d812ac6b210b6e31ece6093563e37b','b9765457e9bf7dc69b955e00ff7db961d8141ace763140c947f4db86e263028e7a1f4d02b52835b6',0,'2021-07-17 10:00:22'),('0f6914dff6e92c7fa6fb32761fe9250486864ad3b1631ff8368d73266e8d072aa5040403faedb51f','d6c21e61e8767836550128cbdaa27696cdf6cdb5306f4432268a09defc4ac4dd87289ab66094cf26',0,'2021-06-30 07:32:54'),('0fbd1ce210a91a78446c87c543f628125a2c27ee7a5ca1107426e9463a765ca0fe58f63ee301a6ec','d8ab33a1592d5f210188dcc96337b2509c3cd108a5ab88217648e0975d927fe4067bbd9c0ae569b5',0,'2021-12-28 06:59:28'),('0fce751c69f8e25b0c946a561ca30682dd3dec1bcb46d7d44b032b7e6826c1932a1dffefd2f58ba7','e1b52683f7683c71e23b460a170d73d5e1ac86bf5c26e643a5d86fc132032a1b3a69a71392a2e79b',0,'2021-07-05 02:05:24'),('0fdf10123a6d5385ee5a0dad77425a08b675b326a6f7db931f3e921f7db7d1bd139396d661d686b0','4cc78dc4bae9a94f631c6f40df54b6a84c1ab80d4369756329c0b0cdc273f3da476f4868e200ae0d',0,'2021-07-17 09:53:36'),('1099833af95630b9209b9672cbea34d168d49de4286a5a3398907252095430fa860cd7508ab37bbb','701af6e7c3b7e6378de9fbac870e0c9224fae6b4ca67c1b166e9e2511ce66ba2025000bf2ccff59f',0,'2021-08-03 21:24:34'),('10a142509acfd2eb434638d526cded22c281635af038ea3f3e81752dd2f74bcc559ffe4efcfee764','945a96fcc531ce3261db120c8c8c253c5f9b4c18f268dba0c626ca25cdeaae1edfac6bd607439781',0,'2021-06-18 10:17:23'),('10b66d4875140dfffb1d54af9ce84340754d2ffd047f0a0c0fe4e1629beff66bd0828c6df262ca19','fcdc5f9bf474efc79f6e39a0d4704656f031a7410cf41324875bd93c011e1d886244ca34c91bcf24',0,'2021-08-26 08:04:37'),('1107cb646b0667b21a3c8019f860e33e78b1264e8b0f0440ebe4785b6eb2f4fe0a2b1f4e2c3f7c08','2d07d0dbb29a75ef288118a38eefc084c3582718048eaf5624796f7b5c8163bd2a6796eb5e7bd1a5',0,'2021-06-29 09:42:10'),('1135ccf05bcf52f30cd48712b96e7c0f3de06d4e93e5e657a18ac63e8bcf5580576eea762a2efa7d','dac0883e2edc126adff5533d6b0e17f6eab6f5f1470d730d616359440d2462dd5c62c3c497aa48ae',0,'2021-12-03 08:57:30'),('11cce40e51ca446530e60f1783e0f0487523e9683777eb8730dfd243bde5d47897e95ecb413a20a0','afa0ae911d8ae9de7446cf7c5c7c1d208d0c46392bafc2b5a54b755552d6803b048074135c71db3a',0,'2021-12-15 06:38:46'),('120beb59f34afe4a6e6c67bc0ca4a1ff5311987314c232e66be6e81e211d6e2ec5a29b59c73231bd','cfd160fbc4c44495cd9f547267d277e4a428d42e440b388bbf932447820314bd1dc699298e4d7620',0,'2021-10-15 07:18:05'),('127904da70393fa2a1444cf94bf47bef7ed698e361e8262bd918e5ca3ac2accc1a901543a013eaed','ccfddc5939c302d2954a00b5728e28f6465d326c8db0090d33a9ad42a0a50e51f160a669f41d22bc',0,'2021-06-29 07:53:09'),('128c2e79543e9f2e871edec8852d72c8eadfdda9dcf19b52093ddeb1217d4507f819997def930a00','e1b7dcdb025e08328207eb91c5b20b25f5d412f7889ed3e6d24c5cdb143869c4740227007a7a8822',0,'2021-06-29 13:28:36'),('12a53b9495c23ccf8e71e4ca9ce07a22c1534d64a95ab1e863b4d21fe1e47cec0127d0c6bb909ba7','5cb0f0e7f4ab9f26bbedeae9f76eac6afdbaefd9cbe54597f65d9048e58d096f0dc3ac04a54521d3',0,'2021-12-15 05:39:54'),('133fc655ce86e1bec2925153bc2da50714f6a3b034f5b6bc4a9cf69747e8dbfc0419135826313ff8','39a673a4d534a29e393c6317249794356fee10d3dc02fd846c1a320783f8eaa6b2780dd53d25ca08',0,'2021-12-18 03:18:58'),('135a0d36a0b194c2fe57abea144f677e8e834341cb9636a6d02e51e3064bbaff00717849d12d14fc','b1dc36e604f640c08dcb50bb46c90e92c7c4c12c00c50269e538ad7e1887a387e07c44f9b7f68920',0,'2021-12-03 06:33:18'),('13cde98eb553e303698afb6d594f6f822982b442d5debce505c36def6d88812f3a41195e53a460cc','58bf2a591eefd8ebb1f40433f64a236970e162c5cddff10a04f6720f8b5a41b431e12fae97d13557',0,'2021-07-01 06:27:24'),('140397b48f78586e99909750eddb0fad3659784fb5335932d38ffbe903dfc183a5c9806e4e564d36','74b082c099c2fc331d1e4f87a1d0411d20a342025fdf7ac412be1df02b0d401bfc613676de157385',0,'2021-06-19 08:19:33'),('1405886f04f66491addfe656a248eb0c0350645b9f8dfdc5fe45235832f4fb040928ab5012522430','2193402e13d0c8d58524fec3085d884955491071e9586c856888fb10dbafde8b887e97c00ba2d7aa',0,'2021-06-29 07:26:18'),('141835791bc6e487f033f2b4225630db92d2317cb092d403a50c54f873a4040657cf45f57d2be27d','4da494590a1669dc24d52a5c120b28c94231c9a1f967bbb6c865355f17ee89693c335c0807e048a3',0,'2021-09-27 11:15:30'),('14adde747f5c0fe0316fc560d8a6c37d997bc8a4158990a542ee64683a74bccd38d3fd850c252e2a','40f7aa62be8bd878f8272af4e8b72f1eba171d4f0d6283754a3397b207982d250d04ca379bceba71',0,'2021-07-04 14:05:52'),('14af60d2a337cbe4ba53cf09b5792f8c04f44db61e0d94c3268b2657c629e4faeac06a271c5f4ebb','dab2645aadb8da64f204069ab5354d052db3fbdb3781b09d9e140144aa227035486da7a5cb23deca',0,'2021-09-28 07:58:34'),('14c609e4398e3b058080226935d0fb43585fea8d690997cb730be01ab19984c21f51ecb86cebf650','71b13da5da13d546dcfc4bc670fab5e4e0f89331d5d35de174b9c6ead47460d2f5824cb229fc1804',0,'2021-08-03 06:45:03'),('152ede400bf1693d32895cad1620ae4b1745494b37ab9ad7123b3e006aec1981e2a922c4e1101829','8bdcb0fe381a67dc912de2fb814401584eea0c9fc0dcd0d059c84b77331e88b0177badb7fb588897',0,'2021-07-03 10:50:08'),('1534cda0ab444eebf77b4b29301205a76189be2a013bc3b1d9777afe785c9fbe2d830579a1ddd486','493d5f830eb8bc4e5e82ef449920eca7958e76409deb37d13f4a95cf1eee498d79f9c3f2ae2ea732',0,'2021-09-28 03:12:32'),('154407542df43811a921c55e4773f88a0de3cf5a43b461e0f2d7283673ae028d84e644b142c92a04','ca268f3ff8cd627110322f5301f3035305ab3b3caecff138586f667fb2f993c66746b5f85f21db24',0,'2021-07-04 14:08:44'),('154b8c08560f0084ca8f9c0489c845bb40fd027d3b69c1be3da0d5fdb195a6d5ebe6f14bd18e13d1','3cb6d0da9c24b09902402bd7f4068a351876b3e58e9864c935410f8569a3ade4cb1c7a3fa8987ed4',0,'2021-07-04 14:35:31'),('1570e2d3efe44132115cbaec2827e0527c4dde1b06bca9828746f083b1a798815219a113444e35d1','eb236e31cfaaf991c115b3773827f04b805b06d285e365fac06d9cb5be66a55d84a1a02923607dc0',0,'2021-12-03 08:51:08'),('159ec1beb1ab56219b2e8282f10ba4a4c5fe1631120d16fa4a95c54bd045c0d968f95322c145544d','25bb9a83a17c1eb17291e55e3b4fd47d3e41a03039259ac5597bb6608ba1e4aa99d10212a4d6295d',0,'2021-06-29 08:31:33'),('15c0d224b6ca9fdd545932489130e97af1d86fa1906e87456273cb054293c7ffd61600f058af1c1b','c8ebad05c306be9cb007ecc681a8518b25568a5f5f4857c33cf5934e849e126bfbfec82cc04bdd0d',0,'2021-09-28 13:13:26'),('165ffbc646a55534af4c5b8b5fd93972742ef36188c8593f1d5b8756095afd71a6709520a9d8e9d4','4d7aff1ca49aeba3cde1878cf8cb4f64162728229a4ceb592f9085f090d681ea23cf323332ccde52',0,'2021-06-29 08:26:57'),('16824ff5165e0029f744a1d9a458045fcbd614525f22e05fa6259ad57f2c708f49d46866b1f154e2','1a062bc852075bd9cba9454824e8958f6b85e1d5d2881ae6313bd005eb749bd2b6d4401c2c59cf97',0,'2021-06-29 07:12:23'),('1694d7250cefcfe8ffaed69bf6a793fcbd775a9f0c2df8bcce321d285122034648c685e1f1443c5c','e9602edfe892a86534afef3aca44ad981c0e3d92cfdb4b0fb52643f345ef6045fa598ede5f998ff1',0,'2021-12-07 07:49:35'),('16a4f2bdc0bbd019f38dad8bfd893ff476912bb2f3bf2f60c2139ae95936f8a1ed841ab60f33f632','8fb211302d408000fc5734cd3b10b5fdc3d2c42e087bde265ff70ce394e0ebb6f4d31447cd1086de',0,'2021-12-21 02:30:03'),('16da4cdd34a9bb1eac9b1de17467956e629e8e13535610ef3f8ab01a92b132536693987fd50994a7','5f21e4f23b94117b49dce055a91e4b3d785742b2dd340776d54bdaad6ac6021ee5433e22b7ff11e3',0,'2021-08-03 06:58:35'),('178fcfd4efb0f7fcef05ed384eead013ea06ff37732f2073b8c808cefd31a3c452649533dd5bb201','82b3c062598db577d44f5c583cde09c099b7cf00500fb953f58fe9f2841abf4aded16b436bbaa265',0,'2021-12-21 16:48:57'),('17b8f68e42a2088d98c76e51e6fca86ad8d84ab06fca41d1639bb1843e2c217d368f3f48f31224dd','a6dae10b5f21d9ed5cf4fb0d4e5e3df10f85fff0332f7a046ecec5280f32da4cd43008bc5ef85706',0,'2021-07-02 08:10:06'),('17c4e1cc9f3851e9e82d519ec8ee3ff85a93b93cea5b6b8dad5f577ab0bdc0fe8f915cea3f649d12','2d09400c472ed58cf2699700a28a9efafd96b91df1f2198251dd2e1b284a6aedf44f437a2445e7f3',0,'2021-12-03 07:02:48'),('17cd69faf680cd89f92c46bacae7ec0bade7f4dbe949ee5173b0c3540c40ad6469495f545f15a764','f6af9fb98e8b9098db14817fce6837f59c0988a5a09b2c4610ce31264214c8b92ed4ca6056cd2f96',0,'2021-11-06 03:17:42'),('183959bf171bc32bdd35d2294a6a0559811bfdce1c076a0f0d4f3090168048c36389af04ba91e417','d43905ad26a401de64ac2354fc59dc392079df67af2f6d7270e74cdbe6704d1b53d9a66a95138810',0,'2021-06-29 15:29:45'),('183b3b91e7a1f927a8e2b42f4664e140e2101618581fce48872e1bc338e756ca7dc223f864d326ff','04e6f79711b78b37b29183a58b704d8f258e115ac23410de43cbbed02bb2fdd06e0e8a565bb079f0',0,'2021-06-30 07:56:31'),('18509baa421e3c4c146ca2498d5542c176fb7516f21eb4d933b1351d5ad6cbe4ae5f3f5c1b7a52e8','2f48d3c8f88879900428e876cf63f6b8a745ea6d1bab6a10986c1d949a424abed835fa993edb59fd',0,'2021-09-16 07:05:30'),('18fa3bf44fb30cf958244062deb0e4a1cf02a708de2454d9905ddabdcba938799b9d341bfbfc3cd7','08888b1f6c354e8660e79e6959ae5435aa43b76da1753bfe473126f90e91436dbfa989c3db78d006',0,'2021-08-18 15:25:49'),('192449d39b88a3c116341585822fa31b34ff8f0c17e095617d16f2daa44992ae5d075f2b8c16d072','aabea6bdab7b370661270201941d353e968099d860236df928cf0832ebed5a9620062c5d7f41cf4b',0,'2021-12-20 10:36:50'),('192bde0af05a69b7d0b71c6c9470655ab042acce3bbc9cb19027cc45178d45e19c9003bf07ccae5a','2a83753a00112345519adc646a5ab5b46bd77f551454201624353ed73491f3e7c92a20e408452450',0,'2021-08-03 07:13:37'),('1a063de331a348db113f204ccef46bca39b72df4d55e67ef19b554586a86656db3ca812d869bba30','2bcf86552a10b45f51aaf2c383eeb5a2ea5891603c7a457bd9c3357de1a2a7295fde7f85d0a4f6f4',0,'2021-12-03 06:32:41'),('1ac16a44a3ac18ae45d3eda5807f1eab5c41c4a80ea7351caa84725f160d68a019785d1918cbaea6','fc4007903e0150a1aa5fd7c06f0c415376425140369eaff0de8398a2b68d206b3dc5b0e07d1834a4',0,'2021-12-03 08:40:52'),('1ac5f1b03e37b746d8629b88b3b02516523dcf49e8d4c4dcb9c5b7bbe33a4bdaa617f73f0a207b22','bccb2a7e1255a66c02ebd6aee22dcfb18ecd0d1fdc4ad655bfc7844ebda442f436d2ecc71a934ca8',0,'2021-06-19 18:54:34'),('1b1b39acb5e12923e4c004623e52c86612791fbb8ed0596f4a01ed2cef1a7a8d613d3d9bf28ee85a','86f8dd662139c05cc19c51ad48763e6fd0d6eeab364a3200f944520d88e0777e57939d9ee047dcc3',0,'2021-12-03 08:18:47'),('1c5e332f283743efd53821b7897668827a418e71bb5f5169b24516bcf7329f36399ea372a9017577','0e11aae1445403d9dbdaa8cb40239a98510a5b085babf3260dd68945227264d9555eb9dd2c8adb77',0,'2021-12-21 16:48:10'),('1c8dfe539388891cd2728557e9d4dfb49f337e0d41178d87b5285caa435e87e7ef0ca92f57963253','84225b19d0a6596904e474a27119f4587732e5126102ea0d9853e99aaec47ab230dd6c1ba53c7b43',0,'2021-06-29 07:44:08'),('1cb54222c1d6c3df029d96b1f69da681593d5a662a97258ab863a9ffc138e01aac12aaa22811df88','594adb341ee38d4d12578015f66dfb8e1d58f12fb70f80a3025a4abdc87704fd9252dd02fb55a8a6',0,'2021-06-19 15:20:01'),('1cbf07228e87029c4e09f0671dc98aaa6f78140eab53f7507ff233e15f742a7b0f5b2e272ff345f0','1b088475a3e4e618ca996fa0cb55846f6b6e7b81bf6a8d59389a4f636df7a3b349b03928330eb86f',0,'2021-07-17 08:49:15'),('1cd29649a322bb8b1af5f58f59b1d564a2fcdae9fcc7dafbbf967f7acb0670c5530160db1aa4ac77','5232892c11047de84014f259647a5de8ff0d2b7ca9a747eead68dd4a35c848e019b678b068fa765b',0,'2021-06-29 12:27:48'),('1d3c36b872f1780750a206b9f6c5cc66ed0ba4fc154f46108f6a7b4c68aab3081db2bc89cb78cde9','8cac922022380e7e443373b64a02a1f968f81c75a3aa6b1e8d7b63c4f09bbcd429bca4857406af5b',0,'2021-06-29 13:35:39'),('1d67daf3ce9fe1868986083f6a12ad91162074b25d419ab871c38a7d1e16ac9fdf684767acd49dc7','c979604ea9e34ae5ede452e1e39a17e451ea162d7a484f91fe46ecd3a513eab5907a7f9aa58e11b9',0,'2022-01-13 07:55:11'),('1d8b4a567c58ae63fbbd38d8fb5efc4da2d5d855d4f548a7a472f6af45ce77109c88b1ac0cf8556a','9b514b5fdb5dd29982a29dfb5e3df31999e64f799644eff33d3067f13d3d9d76a330c3831e8b3d24',0,'2021-07-01 05:58:53'),('1d9ab9ce3895e3969f0bcf77b173ec18ae95a4a8c3544d0ae6f6bf3d8f31738ab0cf2d10b716f76b','6ca04bee9d555fa555d949fc126dd51c2c3b64520cb86396187acf3b6f5e96e067c331d49c6f36a4',0,'2021-12-03 06:40:04'),('1dbd0a5426c1bf897a5b637dd58725ee9a93fccf3f91b3e91b5825281061ced32e5b8df290e30e56','2551551095ff8d0764c2941b7e9baa3a30096574761ad5c726b5134ac72cf40a17ca5e91c4fc74c8',0,'2021-12-31 14:29:54'),('1de649af9fd62a0af83589a3d3611f1fb835288f6ddf3d00167501d9b92a9c222de92e8ae5fea60b','69b9175afe866a336255896c981091e5e84bae86286ce4263e98939f1404a9d8d740cce0da9c3ba3',0,'2021-12-20 10:44:47'),('1e1c47052a48c55e23ce1b0efada2555c90c7e12cdf84964072d43c48cbe6c8ce83f1ef1e3dee34a','e63ac9289ac8cd4c70e2e426e4445eaab6fad2f84d6c71045c53c5e8658f91ccd68f39162c169cc9',0,'2021-09-22 04:31:53'),('1e1e8731791bd52dc14ba9176e5e6f395e213af4f3f3477b582036cd5ae2b50e210bf7c4a41e06f9','7719d42f7e701df1aecf4625694b16f8e24218f866e24c68c297c6917d1b676e6bb4a5416771616d',0,'2021-12-07 06:25:48'),('1e38675c4b1dfabc112729c0a92eb8b4350cb4c1e1141e4c773738a55012bbd179a1b3fc1110253c','bbc855533983fa55982a097ae530735703a5c9ab918344154d8e690d22f00d699d1282b435b4cca4',0,'2021-12-19 16:15:18'),('1e72943f4d76ab4048a8d97bb7a0d54887c70c3892fd7db983d6be37f3b4f2a99ba5fe080731a502','c06354df72bb3fde0fe579a424e5110117c17d117c8908a1fcf963b688f77cb40e7a4ae1f7f01e4b',0,'2021-12-03 07:48:57'),('1e7f6efb54f2c4fd1e9a7899c287d37cb06542ae339d5d5008cabe9c63452da2959314b79f173520','c8269ee5d24cd86f562363e00ec49d1fa8b02ef2de057e010ce8cd56865b76591634d6cd0276cb4b',0,'2022-01-17 04:05:53'),('1f0201c1cec42659362b960d5afd491cf89bebd49b557a2175ec587d9dc37f2de59dab91ceb40418','85bd955895ced0d0795014b4bcb90c76ee4ca09d9ded3e1065040337e87af54d2ba4d99c4c7cc128',0,'2021-09-16 03:39:19'),('1f83983e77975c2428b2ff9a3bbc52ec71248919700bc6a31e2cc2a000510dd23a0d99f0bac3e7a0','c1663c8649955479cd2aeb5cf148b33b96938030c7aa88ceb2d9eb3abfd3adde9daf0f3e128037b0',0,'2021-12-15 05:40:30'),('1fb2a11685962547a68d04fb7a23854ff428e1aa263bcb2b0e57bd873c1cf761563b02a0dc28dd39','2f8e14fbd076cd356156514c5b9f58106f4134424349203cf4f97ac4c56f05847012d6ea9a8fd28d',0,'2021-07-01 05:55:31'),('1fdef3e83a6c287b9172484f5ab3be28fe772ea5afd651dd7a31e68fe360782b40697e6f09422b38','1dfdf0adffa2730b192279fa077de0ca7f5077e798bf3e7568eb34f2efb98ffba1e18b64041ec744',0,'2021-06-29 08:28:33'),('2079776c0f501ef5048e2933e105806e1556e1f1b7dc409b854cfc866d6e43edcfa3bd5bc3b1e5cf','8bdc04302e81bf9a29252cdcfa5894378f240cb4a857897cf02a951bfded89cf2ddef969a570b2d2',0,'2021-06-30 07:46:14'),('20849c27d15bd8f7bbe374dda8d8b8809f88bf0ea5877454fd932093f68eaff868ddace88e31ca7d','b4f32dac7bb93544f30c52cfc85145c137fb5641742ef7ba57129ee9d34a2a6143657060d1ea32eb',0,'2021-07-01 03:45:39'),('20b3c3e905a1fda4f0e5cae65d3b26936be80174251796b2ccd14acfa2b5cf0d0f0f50183756b825','d6f8137e6e2b9013efedea7a6bd68cb51316d3483acad6b67602147043fad32e4431c15083a1d3e8',0,'2021-07-01 06:11:15'),('20f53e7784a388ac97e78b9f04fe60728556dd40957a99e36fdfa3e61b7e120e322ed061fa077f30','e25907586217225ba47b78637a523324f00330988b4b82186e242b1c144bb62148be276644ef01f5',0,'2021-09-13 10:47:14'),('2109dc79adddd32f5a2873c247f42b21be182b3cb644af398b54187616c6b5e6c56d256643be24d1','4d6c074e7e8ff1ca9a1038d6604b27ae7914171d9a97a1e0d929ebf1e93d4fd72f8a7fe921b87744',0,'2021-08-04 09:32:24'),('216ecc12c88deb2ca40c3952d5e78fbf87d500a9b27037fcc37db044a0b9542b96b9cca303c510f2','3efffb9128e8800daec223a46750611a0615590b33025c20f1ac8388c507dc064ddc50b9afc401fa',0,'2021-06-29 08:52:25'),('217654b15444fd0349ed8f8aa1af64ffa3ff08a880c533fcc2c6b38894f0654ba2e4e0038a377a49','57184cf38f6c87021836fddcb0c6dff71d99fe3889e5b818e4731358a3451d062bc57665d942685f',0,'2021-08-03 16:22:19'),('219a00304aa156c2b136af441edcef869e7a0c50b974f69a33458659fe3030d02f4f1ab831e17e89','26286af6a38e7efa7efa521fb3c13db5620b3305e61f9f2245fa96017eacd343aa1b61d1cdb4072b',0,'2021-12-15 06:47:19'),('21eb74e8afe118046f9c539a7d1aec0eff5fa4200a078f1dc25c99d5ced14d9fec27fa7c15bb3366','44fe83d8a386bfdddc5cfd750bcfc810fed19935ac52ccd5bd0c161efea341b50fa02028084b4f86',0,'2021-12-19 16:13:22'),('2212a21e644a9016f2f117848f3a47276346e2968e5d50eadef587411e0967cfd277b76b2d08c5fe','c4b299f5143ecb218f591d650bb117d90674d2073b8d065d750234ee3e148b1e045f5bd8ca66d337',0,'2021-12-21 17:05:32'),('22207a087f9ce9711ff8d749534df35382fb10cd3bdd307f8d2980889d72a5384fd65838025bbf75','aaba4c58db7c2eb0707b8382ab09cc60050cba6422d599d4c3efc944546c5f4f160f18d9134b44b5',0,'2021-12-15 05:39:48'),('2247a652603ddab38262f2c9c2efdd073a7e29cad0d043ea44e4ab5df2a488724f82677e0f5929a6','289e82ef137f3cdc194a9a1135f3f9058eb44ab68fe1d25b98cf2cd64022be88fad25cbb031a85eb',0,'2021-12-03 08:57:38'),('22ad0800ff3ec4f15f7e54f53f8bf31516df641d2938ba5f20336a590e675ea6d99372f6ae42876f','7dce8090a51d6fe66413f6a9c2e222aa1498fda30fc26387fd7f84da13f2ba0ad8a620fc248240f1',0,'2021-12-06 12:38:09'),('22efbe3974f7c0b54aa7828061433f0b09d662f03ea80d3f358c68b169985b61a314308fb2a776fe','2207344b826b7f9b26a6ef9c139afca1a73414144f5a374f359c2518d5ce09396d5ce06594059b78',0,'2021-06-29 07:17:20'),('235abf05fefca1568717d0d2fc7dd0055095d9d954997908812d1fdd2ec7a1f4eeeccd87c60bc5b0','3ef24a4177f79234d443c2490f1da1c4bc1ae709989e9c9bdd58ea861c758d08590212e1dc51d42e',0,'2021-06-30 10:18:41'),('23a7290369714b43e099ba98f5f2d7ca6ff75800a7a5e2184b04cfa3a3beab42ed95d506388398f6','be2192785d0c47e257039dea1635c5ef77a909207762a73279cbddd48274412799052d0d91b80f39',0,'2022-01-05 04:44:42'),('2464f62a79450d772326be6208832353fad6debf907474491e6c9466fa4bf15053d4853ad1446a9b','b9e9d9648b71b1483905b78ecde791d5363868a7e76cfaccad18940c445ad1423cd53a497d3024e6',0,'2021-12-03 06:37:38'),('247c401137dba4605e57b8b5354bdf0d11db2060923a4225dd9272551af629d44edb6835770e2979','4745e623e5f293b354bfe5680a2900f22339ec16fad4f1fb1f1af7776623570b23747c9a56bfb5f0',0,'2021-06-29 08:29:01'),('24ce8c2934cc5b59d047b9d0bb23265c7076edbfba95cbf6f9c8a4409069fa73a5d2c2aa4d846eec','a635d6a771451549be0808da8aa80636c12438c7816cb5ae606df811c0325f3a96e0ac79d693e0b1',0,'2021-12-30 03:11:40'),('251f939b3fa0b1e3213fd7dc8b8e21bd0f58a6d76b9070d25be402d8b73ee234982e695f752878e9','daaebd0a0af5c94be8287ed3e5dabcd2170081dfbc5de792c186bb5461ddcbe7fcf52b16c44cb6f0',0,'2021-12-03 07:06:44'),('2545b1e50d1254d6147f6e48ac379b65a17effdca3d43cc691e40a4d8b8918823f9de6f1c7cc00b0','3da93aa167c11b68595d1cc265c8943bb9b50160d1baf02d8d445036bcfec76047e585b869c4b7dd',0,'2021-06-29 07:17:32'),('258cbc046518a29a28d8768e9f0254b41186e2f15835e31631a37da93485b8bdff1594c6af64248b','25d94f9219e109655da675ec4463355369a9cc642b6ad88cf49dca7a741678cbbecfc2033714a71e',0,'2021-07-22 08:28:39'),('26187eb007e1749a05e8307637aa687ee7ec0762fd89acbf3df3477adcb57f19e00ab4c301be3c9c','b945a6b0a4180fe0b7d2d3e50bb816c26bce396f0724d3b7803c7578b4e6a61fff5c4f813fa3e720',0,'2022-01-07 14:18:38'),('26b16aefa05e2ff8cc5c09af65aff302d09ba1a0a68e6f2b51876a64ca37435352cfb1c51236f176','a319ad3a2e57313cd49a4fa5a5bb5376ecbd3c9948d7cb8fcbe0807705b6ffa98f4a7fadd41aec81',0,'2022-01-25 08:17:26'),('27241059b016467bdb6befee35f136dc8da53e08c8e4518e57b1812658ef989e79c18fbca6fef858','173b733438a6cf629ded33a8c1f063a8a693804af84b16462e962caacd5fcaa03c93b3108344ab3a',0,'2021-07-01 04:04:04'),('273b310d0f5c35e77b7b62383a73e6eb4f388c827c3054914ce178f05c90dbe1d3ec998d21a6a961','42363fbf476c92b728bb82e0a30211cbaebda630d8f266bc75b6150f64f219e81e2c76a573533c78',0,'2021-06-18 10:23:43'),('2750d4201c71d79c76b777c95a8abc275e36a53cc537327836e150f8de98a7293c23127325b0bab6','4b9fa8e5c450d0c87e3ab31270f26acf86707a4959a53eb47235d78cbac9a586adfb8c096e16788e',0,'2021-12-19 15:10:30'),('279609d177eb4728eb2e02008369c6e8b9a11da2f626c7ca80ad8adc502a1b380697fdcb5585f6f4','00a82bdca9213f5cca0f83d62087d21e7d51efc972b8f95f08b480dd5799b891f4713a2388dbd6f8',0,'2021-10-15 06:19:22'),('2798bf7e33e6d07218cab8cea6a54e96095020a2804f9316395aee9361367b8c6f2f35ef1cfd4e02','7b999bfcb7120baea124ed7fadb4639e634ea22b9a8bf35662daa1a30b8094593633e2c30e56abf9',0,'2021-09-09 03:54:11'),('27c81c1133910639579a3e3f7c20c5698b112b698cb08830d74eb82815993b8be7b32d5f7f6852e2','97de15eda54bbc581bc2ecedf749c0b7c467fe5b3984af515453188c3ad94435394bb885cc15de74',0,'2021-12-06 10:34:11'),('282ee81dc79d2689508d41a6855f682fffc964d3aa094eee547a396580e659cafb9a58759c83739a','d77189764fa9995fe166ebfc4c4362bb126c6d74abfcbb22e328e87ede66a7a930fbaadd37d5a3e3',0,'2021-12-07 05:56:41'),('288b225b94e51800c7dfa2a3c28aafad6853e3c744a60deff3bf21e130476b99530b4a59b081a590','cb9564fd4168171602b58d589644c5f8c7148729cd65cd9758a79a86096f24aca45ad8aa75d3ab92',0,'2021-12-31 07:03:39'),('28a092366d6b49e9648f2e534f0f6acfc5d17475654d633cf9a6d6e250ed687f99c20cd025a3cff7','780d7314666733d0e39c5a41bcf8d1b7a724f234dce77b0558270a68933ce99543eb0a9d95557b16',0,'2021-06-20 09:47:02'),('28edf8785fc91ebdc8ea5fcf6130b5619d1c9343752b4b7bc28a62a25e51fc91c071fa684f957f5d','e70efa8afa28bc44b4abaf919c498b3e36d240cec058f04967095b9f6e7c456c8881a984ea352938',0,'2021-12-03 07:57:13'),('28efbe3927c708592c915cc86053d97130e90f1d606baa4bd38eff7abd5621e2702872c6e4cd6895','c5a516427c508b30bc5f7c0655ee2a250d1ff3d64efd508faaa5fc7e89ec6418b10d3de28a58e91a',0,'2021-06-29 07:44:57'),('294f497fc492e3d049e3692519d960020e0b918f04295b3e9e7c02a8e2c578752021c59100320a91','a7763f5f824bcdff8b3478f3f521880e80260fe3c3f234cf63c2f8b20dcdc57caf55631926eb7e70',0,'2021-06-30 09:44:04'),('29adb0d5e0aef92d902e0397e7b1f8f7c12c28e0193d2dddbfb28cfda6663bb91507ea56c382a055','97a03c01e07420c943e6df2cbdc70548f23235df265d29145b5203fc1a16ffdf300282c5fc51e387',0,'2021-12-29 05:58:42'),('29bfd03f81688ff763b20be3d8cba6d73076cc49eed43322740abcb93047654516c5a818eb592332','f562238fa2272caf5739ec61a2bc9ea6109c4b10b8c2350e719893f7592057c408720e7843027044',0,'2021-07-17 10:15:14'),('29c5d347dc2da6d5a811d450dded6f01d4ce545f97b14390365ede8b63c3f6cf9e95983991f15fd3','8bc49e4b691b55f150650d2704088250c896a211ca45f617093e4018e6e6e2ff3eb21ba7040352ea',0,'2021-06-14 13:34:00'),('2a31c15e332b7473e9f48c8a32cc9c88f12d09d52d7346f7c1c63ff49b2e3dd0a45a6e4f8ad7fbcb','3b4928449f79972439370e887ac6961c3c2e843b78666781846823a6446baca5272065af29883e1c',0,'2021-06-29 07:52:49'),('2a4301992c0451a29a7cada7bcab84f57381b3b5967fbfb887abd615a93a469bf41f914796aeb75f','d4c35a03f662e616323cb5a87eb949ca3239a95778cf94462bdd4a14008948d616aecf21640c1440',0,'2021-10-16 09:13:25'),('2a4619e76b141d3322c9f392800af9a0972cd75ae2439ef124ba247166db3503d51a0301bdf4e378','cbf328356aa726fe553e34878f8373afa743cf6d8441cab7312546e86b72ad8575bd060eacf804dc',0,'2021-12-21 16:52:33'),('2a8678249c8e53cffc5a966e21c366c6eef0567949561001e8ec5830a1506784dcf6ac0906cd540c','0ab575fdba32b908981570693260051069afeda1b73a02b65bcd7d82ddcb0863cfe3d7b3aebc58b9',0,'2021-06-29 07:21:58'),('2b6086fb64e144b8f292857dee896c30cba83859fb2811851eb964813c7b6878def85de9b06cffac','3a7c0be8b516056118a6b9f457425996d048cb3d2ea11a6358296cc8f7437b0c6be5a435f52d78f8',0,'2021-07-03 09:51:52'),('2bbdd78717f7f23ef0d39808aec5383caa9a9fa4d8fb53e4b6c938e0e36cf3536a5adf57033c8205','936d416d25b955257e2f3d743f92a4c6c3baa255d73b9fc5eeabe13eb167f74a7e25329a289f1bff',0,'2021-09-09 03:27:58'),('2bcfb003649c58ce344b6fbff36f16f3be5319d354cf57abddef2ee76dd53e1bd9c0d3b54e10758f','8db95561d2038e6503997b9b112dba249f3fc2030725c835dd056464f4b4196b2ddb13e91228f670',0,'2021-12-07 09:30:06'),('2bf17bf1c51e1b1c514466aceedf910d4c5d27bfa3fdf86ff80ff86b55d6254c4fd31d72421939d7','7be0d091f1d2a2d24952a029e2793ef14bf463072626d344589fffce5a613bc10fb141c0b6344578',0,'2021-06-29 14:59:01'),('2c032510b0632e81cf5f7d1f8b2fbc1b7025f38955bf8849054fc7d40d037ef3caf6428030c1b3f2','917883780141f5b8ca37dd569a077394022e56dcbd5605deb10860ead891594f74b4f51b8e0e72ae',0,'2021-12-07 11:36:46'),('2c6f415fc388b95327e292589fdeb2db6c1ebb71475be9a986c92839686f090a504b28dc4d3b5d32','1d48a7cb37109a5e73294f45c93bb7f1bbf89d162ef524dfa7a272d5e83ed546eec7685b6950d07d',0,'2021-12-07 06:20:06'),('2c7622b72614472643ca6eb5ecf160872197352a146f61d3db3839e05f2ae1547c277bc8241597ac','eb78342a2b64f4bef3eb07bd2797772066a89016b65e2eb528512ef0b9be00019d45446b5439fe47',0,'2021-06-29 10:18:55'),('2cf49876205e52a7bd89889d8c0f22f5156a90b17137883400de25c41b867e7913614245be48e97d','46ba7fd771eb77e21042722c9865b11a806b277ad8b3a90d6a739a81b312e1f1ed60709bbfc07fa8',0,'2021-12-23 01:32:32'),('2cfd4395a1976a9edf44aedcbc667127faaceea83ce664e6ee613c0267e58259c2b030f8abecfcca','10c3c85ab6d92c78c11684651911ce9c44745c4858f57f0991439180643f821be350a8e1b25a8727',0,'2021-06-18 10:17:29'),('2d0d29f35522e0979952894ec70ccf6434b9e76b2fee2fe858a4200d9d3975010936bdbde5381bd4','c2fe19731e17b183b3064368a9b85181cefb1405945bf7256142a58c92125f6379f4035164299f48',0,'2021-08-04 18:18:08'),('2d348603fb759437639e5953616a2a4a9b18f58510b821181239738689b691aca74c8500cd7cca75','92f47dff53aa60f6590aa5afe2d16e15365ed421c6c3863d057072fc0cdbeac10553940ebe0f64b3',0,'2021-12-07 10:59:59'),('2d3771f760fabe33d219fe4b65af0fc69763a707b7a94b180af6ab22e7dc9dad5478812edfc39e57','18a8fb3d954f128305db5e522e884b386175322ebedc91add02fbb3a8274cf449aa5c0d68e62ee2b',0,'2021-12-07 09:50:27'),('2d566e6138477d41911650212c52fd0175d2f7dce7fbf4f2bddd9597abacea5ff0ae6a9a8403dd26','7c0ecbbac9da89551d30c63436e0ae281994e2cbbe47560d28aaf2e8dae9b73a706ba38e841ff60a',0,'2021-12-09 06:45:41'),('2df021635ca9703a6700b375ce919c9a2f7b7bca0c7ef944e34d90212893a5fce94bb8ae9af3557e','0a78ce87455fc8993733dfedc39e0b7ee2801ccce604b16770355754ed1c7bea0f9c1d29a6034f70',0,'2021-08-04 09:11:25'),('2e49db359a248cd62d16c9c6b26c339487095cda70a26e4d0370ee41971c4d94a9ddce75e0ac107b','61e487d931925f9cf29847dcbd45e56bcee5ba2de16031b6f9909ebf37674ff03acd703731c9cb81',0,'2021-12-07 11:41:02'),('2e5214e7301a89f47c86c0dea100c9e8df56566046223c5da9603f0bba3201a0e0f620d9c8648d92','9569ef00a47b09ee52d41026727a5a50d00ef4d2d20bb879b041831718edc8b81f7fad8680ef643f',0,'2021-06-30 07:50:22'),('2ea252158e968e4fafffccfc516ce55229df4edb6d64adf4661bcc06e4d1638b8bc4ae9e8888916b','a0179eefed4026af82fd1a9370aa6c4f090bca74bed626db5d209a5039660d259a94a656a21aef5e',0,'2021-06-29 13:38:08'),('2ef5d5cc2513707781de47f7b6170fc403f1be47367532748482500adb2ec1f004c07d7dd69c9ce7','c493f1920af7577b4924d0045392af6d3442ec7f13bd72583a373e0e155c9f8a4658b64d8a2c0f20',0,'2021-06-16 15:02:44'),('2f3349e5b85bfc54d019d78acbc99aa5300a52a35f57ef6c4770be5c116c9a94171c1fc76b1d2267','526cd455d3647eb46140751f8ea003f8dc9895e57d3fdd1c4a6da692b5218f34e1732deb6c5ccc13',0,'2021-06-29 07:44:42'),('2fa687030f41621ca205e70a863d65d86de092a415f04d1f5776949304cff3387f9930a4eced36db','fe2318a48e7e7d60cbed1471118c1d4d84d84c1dc9fed9bf033992a071bbad2ba6a57ba025cb80da',0,'2021-12-28 07:00:35'),('3042add1c6264aa8c199bfd7d01d7116334f6513d1342a91b299c0bcdf5e2559338d67b6059abfdc','a4f4efd97c3c86cd1ad2a8a357aeb31d7f44b08892f756fb1a3c2e042552a8415d23954f6073b5c3',0,'2021-07-17 08:53:25'),('307ac77aca693ef6948d1b925128f8901eced8cec8cc256c77e98b77277588edd69a90e3f4ed94b9','2a372a9b56bbaf49c6a215af48975b8d3895294a54480855c9d697ef8eeb171367ddfdad5c89d853',0,'2021-06-29 14:31:01'),('307faafd7cc7bee09ddcbc299484fdc3506cb4495a1f4216f9c3bdbc274402548bb87858b25271ba','ad25ce9bf3ca7f8ea6c7a3d8b8d4690adf3d160c01f846b765c4f7af3e635815b9f6c4d7559095f2',0,'2021-12-15 06:15:33'),('30a83bfc3026cbfa55328d55d197921aaf4487d5f77a514a905c889d020cd663e2100dffb832617b','31f8397021b7985e854a3e38bdf08cefb4238643147b7d9eadebda6e9e3b616c5e3f55e3a3df704c',0,'2021-06-30 09:57:30'),('30e7d292b20eb40ed9e226172c94a89648e821d570a47f71ef308b9272fecee69e04b9f07914b5d7','022f9942a01b1197f83814709ea4d22875af65bd0065695f040f1d3c5dc6164a7a6d981791dc53ff',0,'2021-06-29 08:45:40'),('3170d509b4659bdd8dd28501d140db6816fa8adf2cbdc41d43eb8717ada38807135d3dcb1f2b7ded','ae4779df67fcb89869ce0f5b7d17095ae25bc05a1fba2cd4092b1034f59a389186d8f5864a7b2540',0,'2021-07-04 14:08:34'),('318f1ae26310f60d1a9f716cc362cb8bb0de45c73289d4f808ff2a62bbe2c01963dbe263d4318d93','6d7fc38b93fabe53de565e6e93fcbd9537a74c191224c961036559c0f53d849b12e119baf83b10f6',0,'2021-12-07 10:10:05'),('319faa7b601f4d388e863c24c96ba8795fa94933348b8584bd291618ab8b136f6b794b7f8f84ddb8','496e04a10a6679d078c2e22b689896b8db675ec2bde29100875059449df33936733ac77b7093e76b',0,'2021-12-06 10:17:21'),('31d8b324e186f03d3d50e860981e85f2530026f30cd995906590494b20686565b732065fff9fd329','a5a21adf9329b838c55039aa5dc7ec88cf2798fa190ecca92634a5a9b2992eae80ec89311d612010',0,'2021-12-03 06:48:58'),('32269d0f0b6f192162ef9d85247f1fcff2514b45662de094b52ea08e9568398b0186726a86d4f203','8afe7d9a5b904749f9213debd43755089dc65c6c3b7f1ce7d74b62212d68826a8c17ec32575a4eb1',0,'2021-06-29 08:17:40'),('32684222cdff4fcff6fc7804b4b5a3aab884e17939014a875cfcaf000d06cf58e8c066ea677d0a26','83913278a1da7e950998bf3c95b22f3ba2452cfedbbc876860f0ca9ca4d99cf7a2149eee7e9e48bf',0,'2021-09-09 09:41:35'),('32b95a316115da51e882e154f4c97273d086275a0e62ce51f1c97c274861703a9bc7ffe4638c44d3','ef04aeb67effa2b005a4c999a874b0f15e9f70084af4419537770b8018d5bf256fbdd632a53287d7',0,'2021-06-29 08:25:25'),('32bd92d55c2a4adaa337f6475a9aa97446d01b113884e758dce6782dd8c9ebe66e1ba361bf84fe4d','c058cf9fdbea43e0f08ae9e70a6ae92b7087cf3518e7bac7590e5c6b95c061ddb149b7013cfbb798',0,'2021-12-21 17:03:18'),('3400990ca8af7caad938b233dd5a7f0ea3f177cd72ff376d927df3cdbe31a28c0f4b77cf50db427e','df1024d2851d49387bd0c42b1df319d20c73020a42166ba70e3fd51e0dd6d5dae4bdbf68fce3b9fd',0,'2021-12-09 09:18:00'),('342ec45ebe98488065dfeb1a0a99d627db0007057bbf76fd3ff71516845d1f8875ddb8e2c1d82874','105d248ce528f0c329f5330ee33e16846ba1ccf1abbf098afca127dcd35149ab2240df2f26dab69b',0,'2021-09-27 08:30:24'),('34b973ea207b3f7a8f3a88cc68ecf2114879764ea7721ab4e56ddd107b4b2252cd7762319090b318','33589633a00300fc4ee86d2bd67b68c4e84bbfe2c89e29841860f96d38a942b379ebb3f4e4a80910',0,'2021-05-11 07:52:20'),('34d713fb6fe3531c866a3b638c5ef35bcb7262de315d9d60d1fee06a7c916735e0020a5b3751d1e7','bc68128fe4f0047e52b859321aa5a9e4d91a7e481f4756252fdb2a9ada6d23a8829c5f57142ac4c2',0,'2021-09-09 02:50:22'),('35050c4c415cfc734dae339d555d518554951a09ad97be5d8a6415a787014f5c5a2304fd0dcb417a','23abf06c7f03364fe2a84136c4393633249822f6560986197f0427c7ebe022a1334839e2d7b81954',0,'2021-12-28 06:59:25'),('3521205e5a7ad8751e04154d1749e30f01375ec6a59984b95c31f85c9945c1366441b4fcaebf5d80','930279208d4eff09bf4cb9e0d7fc3241596845d61ec30928b90b580150674aa4199dd201535025eb',0,'2021-06-29 07:22:58'),('3555a890d49c8a8700339a7dc60e1b3976242fe13246afe5c6f3dfd1c0b357f8d7c8b75adfa6a909','ec3f96722a8cdef3e0ea10684839986b5349ea0d1c0621d62ca53eb26889f4ca5ac4c8dfa6233296',0,'2021-12-03 06:35:00'),('3580383799058af4f9b7b8d15cbd613e33eee95823c32c98a25d5e92d11d1c065e237af6f9d3d395','2f984b1c2266da73bb42e8dcb94aab1f6951258973c4b11aa680780ce47023daa9917a9a9227466b',0,'2021-07-28 09:27:53'),('3600885d469c3c3a6f76ff82386a1e761829461479c0d4f9ef04ce5b9611b1440b09b5e22507d335','5858cc44cdf3c2e10a021a847543e3923aed8686a4a3a3fdee9ea2255ac7840de8bf2372d3cd0888',0,'2021-06-29 15:26:36'),('364cc5f1f253987c58088b52df647d33675e259ef165070a2af164529db2c37065981f5bfe88c392','56fb42b6f2e8fb5df48a827931124a50f561cd6ea0262d4d046675a4c17f87452d1b0f2a0b57ee69',0,'2021-09-28 07:54:25'),('367b756610afc1dc3406276b83c24e1d12f56cf491fc72066ee0a118456ac0770cf66a04e5037d0c','7a1f46a79e0ffba9da74edc3ad7eec917c3099ee0898efe3148446306b458f705efb89f76b99f41b',0,'2021-06-17 12:42:14'),('36aae49bf67a0b3e869d2df6227d56e1f58a6363f82ebed2b9240f195cc77bd741fa5e327606d9ce','da6baf97ba27854d44f410ba6274f813dda49ac239c2057a0f5cf457f1f25830f25f7154e022895d',0,'2021-12-07 11:44:11'),('36c15343c269a72acfb7c0fb49bb6145ece8c208710106304646077334b44a235514ae88d45702f9','bb2d117e64c674353c1eba83503785f400900065b2d0ccadf6692ee45192dfdade4f2093f6ea32aa',0,'2021-08-26 07:04:23'),('36d23fd83ad1910739f6bf2f5236974705d0b01fe80f197d68bc3cf723a590239747d01131b208e9','50a0174adc6ac4acb046d777a338e7a7dfb0f1cdec4096088c94e26d188a0645aeb0e6a811b50c9b',0,'2021-08-17 06:46:22'),('36df7e4510006df190b3ed049017f3bed9b803fb6cd243cc69dd3bd327c3e3f857d9fdc10468cfb7','6af29a8e6c0aec6771c71a272533e78be626ebcf008e97fafe2a684c7538b57a507bd683ec6dd8bd',0,'2021-06-18 10:17:16'),('376dca69d35c795faa5088a03ff3a861aba5d66d7ee4ab98dafb14dac8942cc627078ef679e1ebac','7d6af1d75da8e82cfbc6b3bccdccca4e423768a2b1f301a0b0b6302d850660c90066b3e78da702b2',0,'2021-09-14 09:02:12'),('37cab4d610baffacd9a797a1e989b136a7a719f1c0e895b8b0b6fb882551bcdd4fba20c0253c4544','fa7bb3907842723fe3e11f25be6bb3ed1f4c8f5e777a9377a348c32ec8f3c377d24f0aa3aff6055c',0,'2021-11-13 05:47:14'),('37e1ce3ac2c388335291e6c4929c57c344e950bf1f21e780b718c2d91d84f0391f07fcda386c6dec','239cc8a644b78fb94dbd5afb871596903a506f27deaf35b290176632899398307c79b95b791ac4a0',0,'2021-08-18 15:52:37'),('38663d44ce15c66f1f9369ca81de6cb820db62ed4de2b17e3586729f144731a0e856cee26e8c72db','9b241c6c2df6a9247eee46787b5a0c93f1873159434225a9279926674425e68920a899e35c9de90d',0,'2021-08-26 05:31:56'),('38775c2945d9c4dd1e9d53f47ae20a6ac74cb92a03c8ccdc594a9892a2802be4f2878d1f06d03869','bd441fc8b2d777650eb88de9cfa89cb18a381545b3ba588b4ee53ae4702fcd096684f0bf48ea15a2',0,'2021-12-07 06:18:11'),('38864fd8099541a451ece8910142f9b3bfc8b95c1a91d614bbfb390ef6ae0914b589c78844ae5445','71d32a40b172b53966185c7d66a1b09da1114877db94d50bfed83335b05d3034785f705cbac57a72',0,'2021-12-03 08:57:26'),('3933979f3516ffdb8b29f0871d17a77ebf18c081d89589ac59e44058bf6d1288fd0635520a1b27a4','11087e7858e70a92c004ea835f60ff61f6c0adb98af96b9aed183071e3f0a6c941019940a091d022',0,'2021-12-20 10:58:37'),('3976abbe0b29b06dc6002b32436482df779c81f219fe2259f0954bb046f9cc26926c7dc46e4f5e6b','99569189e468045e91e9c87970abd3099b0a3ef547bd29f306d7f9dadc815620cf781f4030d84c88',0,'2021-12-28 03:31:24'),('399d2627060a3ceddb97aa4535c8a322f27d86739b429da1e4b71522e2a43d76f7171f800ca19b5c','fd46c474dd5b74683e9417527f333c9a00eb745f1d6f47a88deac4a2a51093634539cfbebbe74503',0,'2021-12-08 09:00:55'),('39aae32e9ac274132c02af7bde8789b01becbd12d5856c002577cecce66570935e465f7f980990e4','3815ece7fbdcbfa853b3622872f8778a89173db3b86f46f5211d45a127c97a100a3b000c9ed18309',0,'2021-12-07 07:37:19'),('3a31e2a1f99b7166d198629dde38b6243f00cd08b21e548c4b33513d559013f22a2cef997ac35fab','b997b0174ba8f9abe68fdd7916d73a44c7293e9acc9def73a06c1e13a2bbd3224e00c10dc26f061d',0,'2022-01-07 14:48:19'),('3ad46ac1f3d5d2a77e886f8cc03f3ff3d4c981595f497c13526f9cbca0cbb1baf0f625683bea21db','f2c32767bb09dd223d9c4da49d645d73acb84b5275ce734b3f2a5307c875560a901c43ed99455d6f',0,'2021-06-29 10:01:23'),('3b299ff667134c36e08b654f95def7ecd7d727dfb9fd0a19ea4273aba0c39ff2599c80607a064e99','1283d73c466520fc6f33c2bc60380a78c7fd7c8aaff344a80a637c07774318c12e7f88c0c5172956',0,'2021-08-02 15:08:39'),('3b4febcec6bb04f511125dc5d70620253e4ea026ffe1f513474b0dd08f194798eaff4c3644e590c4','4f9a3cf3c29f7fee68043d7cb6981c21297180a79f22eb64f337f3412e32d8ce6f055b10235c80e6',0,'2021-11-15 07:39:36'),('3c049e60b65aac4867fc8fb59b54e30b13c23bb4e8d97884185dbec72762b895ed57de3dc859621a','cc5e4a1b783d00e3eddf557dc8867a5d025c610ad9fcf62f6e405c5a7acb6f03562a9deac8289d30',0,'2021-07-01 09:20:02'),('3c2133376e99d75e88581bb21a701ff521be986946b273d8e1d4c4d7d8e46c13f26fa5f266b8a927','599639325944791141c6bfaeb7bc9b8e6bafa1b8b12618c47477e1250bcc983a832d88124bb48319',0,'2021-12-21 17:04:04'),('3c26da1a584bddc116ff9afdc75c3682fa807ad9a9dafd70277afec94204cb2a6f930715ce34de52','cab88c86f4b9027445e9359c29ce09595d25d2710f24f8db948ea4de0cff6e197cdb45091b793b8d',0,'2021-09-28 01:54:42'),('3d13da67a8202b93e3f25f444690cc49dea90a76606fbe41396628fb82fc5ed5cafecf30ef9f6321','4510db40cdd9adf2e2691536791bc663890716d9464b6db40fa3d795834bed147ed2fe5edfac0159',0,'2021-12-31 08:08:11'),('3dcc90cba0e8b1016fc583d7eafb3f622c9f0b3ffa687a80b1f68255f7e68fd973e15d8e27f0b47e','2d347fd4543655934d03c3494e38529d21b8b8e23fc69c407670028e82ec8500286df4d3deec9f49',0,'2022-01-05 04:44:50'),('3dd409226be10b2e2ee9ba0d9f4ab358f5ec21be3e972cd0961cd2c5d8b816ccb8da79ef16db2a05','3d4ad8fe1eda55d8b11ec17077644ab2e2f3fd9e02eb9ebfe11cd9bf9a4038bd093aa6afc95fb99b',0,'2021-10-07 08:22:21'),('3e2e43a18491b5a3a2bc6359454a5676966a443f7eb437d2b3f5d9b2a5b1fdebfb2c9dcc265d8e3f','bea4e382c52754dbf56199fdcb71e4c0782471ed9b025535f34a226bd46d555f52658278ec241033',0,'2021-06-29 07:52:16'),('3e3001ee393cdc7d2a6fe9a3e7531fc47115ea4c82d4a1c5597b7a7a6ec4af37f6594f2012ae4af6','c39c200f11d400074d637c6a110fcdea3afa766c6914736355dc368acdaaf8ef5d0b2f4c09e3d788',0,'2021-08-19 07:03:03'),('3e7d7f84118a6fe3a87af9d5638f30213054f61bf8d72acd0237b61fe5abe3accc9da62fcc2bd25f','6283c9c199dde76e83588da6648b0100822c68a1d357c8e46e79b99422d1fffeee584038b3901747',0,'2021-06-29 14:16:35'),('3ea0b95c4933cf934716d1b82488412933b4b624387e5e4994452243dc1ecaf81dcbcadfbd147e4a','268f8cf6e90b6e9f8cd50cf5fdae15f525cb35b8ef0c1052ec0f0579830e5968d28dbebefc11a699',0,'2021-10-05 09:59:57'),('3ede0fa6fb7460b0570832fd0d2559c0ae20f224130327df874fa1f66a59239c32d1e99d482cfcba','e5cdb1111c60a8e6573de576fd46989f7fe248508a7033bb5b884cbd33c4e0c1511c50a430619dd6',0,'2022-01-09 09:21:17'),('3ef53363ad0db41e3cb0b01b91b51a20de3cc0afdd28f53b84ea284f84a0c4794ff29947a118dbe3','59e1c4bad9f0ccec539e3e611b88f57359d53333a0b49127df86179ee5617bb8022c9b23d9830c62',0,'2021-07-04 14:09:36'),('3f08a4b780c122ed0e795fb71e12464af9f96155d6162a35ec3d545396b68bcf2da048afce886335','a342a2dfa7407fa546e7fd3b4ec688770283dbe54e616c4822439bcdfe10a1c0faaa9be31487d192',0,'2021-12-07 09:52:12'),('3f565eb0ff05153c7d2ac8b87a1c8302871f184e4246964d1f2c822aeffafa75053e5d07c14d05cc','c0f84fd7bcba1d1b5e4743c6e4f02407c627803a43c807c5bb822edb2e039a9f1b4b58d580630178',0,'2021-07-22 09:31:00'),('3f7b11fdb6ce5297969bc9e83d2ecdb595cf9065761ce342ceea0b5e7f28aaa07010b259dcf9d11c','5a547de6a17946696b6da78b834e777fdaee2e22db5a880ec5ac89405f9600630b319173130f9606',0,'2021-12-19 16:10:05'),('406fc1b81276eec2e3709478413a0b010c9233e9f2fa9bb92157922a7f22b57939f3a8c67f682698','9625f9d1fef7a9023f10e4bd64eb7b5c6724ae3baa9322fb727dc78d2e5ba45e9664922f0f7719da',0,'2021-12-07 06:04:19'),('407f9c917c03078e3cdbcff1f7ec1f3259059f312ed5da1709e3b4fb91080498c5c0d84f73434bc2','b3cb2fd280426daa29246877a9fea7c90d8f692eab502fbf24dd607d5aaa7f45225ebb24edfed0c7',0,'2021-12-15 05:16:26'),('40a1ec215f5e68d0f96a14f8929001fd300ae9cc2d26308a154d3dff92a738d9899c84c5cd656fe9','f42b1ed81c9e7b131e0e20bc0cb5552e9bf4dabcd83036a25a9ace8bcdecbedee592c86df6fbd8c9',0,'2021-06-29 07:24:20'),('40c26fc7efdeb4f4e7f1b515036d37af5a3cb5e790498626bd656d5cf53cba8955d32cbb2a948757','9868310a785fba6f4b278c85ff022d4b4580e3db32e40d421ee008504cc4863ccdbb64fb32cac2e2',0,'2021-07-02 10:09:48'),('40c7dfa82dba33f4a5bfee978699db14014f45db2b05849860f3fa87e886c60a4fb1f91f5b36c4ae','79439a579eb138b677107aeca91fd0b201df0a09e49a564aefe7b54e7086f52ab5c5057d233f0b70',0,'2021-06-29 08:50:48'),('417c53516664f61e6f937e92ce6a9f527073a8ab33890e8c8cb7f3dc2b6ea9e12ed1d6524a2ecf34','28323e143dd441dfb90e49b2960a70ef2a52b0bad007f32184a6d8ac7fe8b6f211898e3225eabb05',0,'2021-12-07 07:34:29'),('4182d780ec5645bd07d3b8786a5d57b32590985858165588e636dac5a256f4f099b4b5a9e8e4da85','e320ce4e90f2f7fb42e52abe85fd444b2e0a5a0d7ecdd4471edac55592cc5018865a9d3db29698ca',0,'2021-12-31 07:06:17'),('418b59be1b33f6bde1e563a940a70af70ae755e0f0ef330c2caa2eb38b7977be437bef524d28a3d2','80de1be9eedbcdc897bbd010832ee934e005c7a02434d7189fe4618d993b9c0af48425bc7738c82b',0,'2021-10-06 02:19:11'),('41bf1d70423cef2a711cc86db26ff164bab39543d9badc4f54ceb423bafbe0ff524eb109ba4d60a1','b133f66b83a9fd55fad90b3355663e445e1f0614987cf53d23c70c96be9e0b3729f49f5a25903124',0,'2021-06-29 08:49:02'),('41f79104d6182be3f89624e4703c818edfe0efeddac0fbc833ffc2c51a2373de65260d7701900eff','18d7ddeb4e7f0271465e2d351e6611ae88ade1cb1c77f29b31b01e3c497df1e63ec8164ef71f409a',0,'2021-12-03 08:26:02'),('42502d4c12d7114be4022610ca73f095d6edcf6e5f2150d265bbedd9e902cfee02baa8c32c5c32a6','c55bbef571ce0da15a6906f963db18e4b8c7edc798128ae45206a0e52206988b4c1239e12d6ce659',0,'2021-09-09 02:49:53'),('4251615ddd5c27b68cda8a81a377722d774b8b167918db932320add4e251d5895316a9f321d6b9d3','1a4f4711907a6d9b8006bd6f1834d24824b4d09d283221d0903c250752fb0e8e9c9d407187b2843f',0,'2021-06-29 07:37:39'),('426086b6bbfc56148c48c635c81d114a959b1827431686b023609abb9ecd293496c3a96da5366449','e471ff4bb92da713a8d19a0d39980b57014962b6614e5d67342e9531c0a2c875881aae4322a80c71',0,'2021-12-07 07:40:04'),('4268bba8e85988813c40f1cab887b1c303a3c5939283d9914f41cf05bf27f6fcb7b397bb75b56ec8','1c25b34ec71da3e101df02acc4f2fb3b35a3bb40341e4c2ed53d9ea644d9bf1b236af12f239fb385',0,'2021-12-03 08:13:40'),('4284168807fc216c97eb786ddcb2cd752e3411dfe410de177c7e7846cde4dcfa47934accced469da','37abfb3897add8a188dac06f8cbef42dac294e79f62f1fca9f3ecf6ed55378fa0423fe2965e08b91',0,'2021-08-18 15:25:22'),('428567996775e0d70f1938c6da473109332dbbf9ce4ee37824c4dffe23fc7887b3fc9151df7318d8','0b776e0ecebe77c4da308a30b50f9bfbb1d34c05cb4497e751ce1a93edc165cce5f17f9b6511851c',0,'2021-09-16 02:39:37'),('4296fb3211cc529a21c624b25a3d8d78f595a06efa4f3697409255952371181adf3701bdccda00fc','b0ffb00f562ca1677cffeae3e8970e14ec80639cece788b65c225ef2b4e8eaca5c0bfc06e25a07a3',0,'2021-07-06 14:09:52'),('43238db1b856a13eae509c7629475dbc08a7ac2280736f68b329b77d84fd3bd55673384a8e12917d','a347658670c7b6d79e67df6f873f824b68a7f66aa73dfef74ca456eb7ea00a1ff16e4e2cc7a6d22d',0,'2021-08-21 00:51:21'),('43a65ea093db5cc6909a028d97b5d9e52b818a6330bb044421ac06240c78159fbd6508de68ce4eb3','8e6f3f0ff285ef8dda46dbf6caa0131ad0ae965388163f270248874d67b8ee41efa3f2ddbb042d50',0,'2021-06-29 08:48:05'),('43ccb4f09d03c3fe9c3c4f6e5fab2ff9697a60fffc3458706bde83f1b41c7654654c4c4978904fcc','889d63307220ff4536d6d673c2407fb1cd8d4984756f6087ac19dc8854c9bcd9018f7406e28aca05',0,'2021-08-04 01:36:15'),('43d8a706e01286bf8f93e880582cc3392cc861c4cf935342265b1d5b45a80c377d9517d8d436f4d6','f90b760da872e338b65d8fe5df3e789606f74a509306c4b98678b3894c6a84bec39ec45efa8ebf64',0,'2021-12-19 15:27:36'),('43fa8edde2fd02b2f9dbcbc5e3e75af8b013255dc85db97697241f9d42e1cd4dece79d04fbcaffcb','4b8f2ca5e07bc9e4bc463832e6f69d62f8e981e103e0a74d0428ee46486a3f144e6967a79edc356d',0,'2022-01-25 08:17:08'),('447377897ccad5da484d4951f1628e96ebc8c048b3bce99c0ada68ddecfb10d8eca7f1024d669980','e2f6391ef726a9551ef98a37bdcb3a4c21b00fe04be2284f1d93ed5400f535fd7352962adbd2ab88',0,'2021-12-31 08:08:38'),('44e423f163e7d040c1ae22bbb7c4750d148bb98ce1abb37c590478ab508ebdc1e6975f91d254fa2b','76c05aba072379dd7140a7441407c039aed3d3ebb985f1859f508ac8501e166f74176ef3833eabbe',0,'2021-12-03 06:38:29'),('456d9ad6c9a00747c56b58d5b09d8f7c7271c520e08c046fc47547bbea5cbd2bdbcc9c28c310aef4','c7572626592df0bcd3b56187ff6a15dbe4525168b4704089d5d1f6eb500cbf0ad2609deee185bf00',0,'2021-12-06 12:26:40'),('45712060cc7d9615b7c861da7544fdbd5135401d33dca980f0a8c7c87b7b8bb93b80ed3a6f71b82d','93b22cf25f8abc4a746b8fe8648dad0c59420484346622697cc9a332ce5fa7819a09b4854c40b97b',0,'2021-12-06 08:56:23'),('457e64c6911f0c62af63af296f04e69d677147e101ef1c949608c3ff0e5278b28137fa759f41dfd0','04a83ff237008f41706f9150f435670f26a87e56f40e4653871b9ad2f3ab6356ad0fd5de53e22277',0,'2021-09-09 03:57:21'),('45a48d38902940c41a164e9eb9fa2cef8e77d4e15d6852cb9023e1d34b21648629d093b825613c8f','c2a798221aed791dd99efbcb4f62625f28275762ef307464c45e95b1899c8026a1531228cdfeca36',0,'2021-11-05 15:08:36'),('460a0935aaf5405070f82d3595b4536140ecd4167e055c235563298f3622ddb87233c53af7ddf85b','4fa81f03b093868bbe3194f5abae5f27ecbc786c465feb44991d496968821fa8668e9efa39d5f629',0,'2022-01-05 05:08:48'),('4649e0336b3100db4c336197b3a04cec125bff762202cfdcd58947fb66faf19c6cd298d11f02cfbf','1c2d1d62520472ac9429d8e19fb5c19bb136c405d4115e0cf6cff0d292adaace11b983daf8fb6f47',0,'2021-07-17 09:08:42'),('468164dc46459d7a3720bb5864cdc9da4bb4cd64da635ee3ac398f1636642e27f9a61e601d14120c','011f4fb58acc5557c0e63ed7ae1fc4cdb83b7de08ea3415475cd2f31d4ef4a38f89d65c26d5f1fee',0,'2021-07-17 09:59:56'),('46a5ba89191bae945dc2688c7ccc894e5f9f4a80065d16f83f74f2d041fef57930dc7642a82872dc','a6d9482fa4cff96fd740c3f40af446e3bc26a3704021e0dcc8f5a057efa02f6ba6b2b84f349e8f01',0,'2021-07-03 09:33:18'),('46a5e19b13b3abbe14d790191d0ba0e126ba1d8b8df7b77d78f4737feb4f8faf1d17475f986b6e17','4ee286a37b62c36d03f9842d44d1d7382c68f572b921f1f18daf8e5188c232b2f2fc964c42583286',0,'2021-12-03 06:31:47'),('46ebea3cb2b0b4774c1444603ac40689d4af3d6a6a87b684f4c149b6272ff9e5f5768d87040985ae','0fa05fc9d67489a396bb66ada351fd781333d5e34d5f55fc9242de24faf38be2aa276ea71d1e288f',0,'2021-06-29 13:35:30'),('46ec34aaf11494a20dbeb156fa7e4a4cf0786ef4e756c0e288509e597e7a1a8a344fdf33dbf1bbd5','23ce8a530155c1547a8603d9ce43db10dc655fc6597112892bfa0e5ec7eb50ec196d20cdfe5ad3cc',0,'2021-10-31 10:12:16'),('47f9886455172d62d5a5f5c18ba532821e45be1a25278454661128469cc1afd0dcdd124410c93252','bbe24ffb21cdb973ca85dc95cfdbdf652986d5ba1e5f055cc14d692cff55b3f402f50767a40737c2',0,'2021-12-07 10:28:21'),('480be77a5800f9b2a1032bb39b62f242f3a2b77a80252e175ea8961d7decd4f3c1b885c1f07a06e7','d7e16fbd2dbac0e9d0ebfba52ffc7d3096bf5f54461b3fb1d8aabaca3de8f6b047f5f97b9eb7f207',0,'2021-09-02 05:03:44'),('480ee420ab77bcfcfc303eb6ad793401b609657fd349604879804b8b6394c6ca64c6b6ecc90a37b1','866f3620f15d0fcd9a0e716a68fde8ed7346cf7941484fbe6d742915a892cd278129b5ca44956f3d',0,'2021-12-07 07:23:02'),('487e4a4841caebf096dc2aa31265e02c38f99005d5648ed243b74ac3e43e115a0b3bcc1a1b3aa9f8','b2d3754d1c8cc3b1832f2ffe7b2183bea04fe90c05a67de1d0b7e45124fb8cdf8e2a3fae95b5c629',0,'2022-01-22 07:33:43'),('48ab56e59254d4b98de089a0855147f4509f4dc6c889d55be1952acae60f040f03214dd5565d1edf','5b9186e03e54968f4cac7c11e7801f01d7ee0740a1e962dc86118acccaaaf045d756c12324528979',0,'2021-12-03 06:46:38'),('48b0cff0db0800184271d08de1b4341982e97a3f88e1f5abac2bcf7e6e7897d50ae6d72314b425dc','5409b43a72e368f26c9cb1fe6ef207b5a8ba2e2431863477c498f4ada7f71544755d2775ae12c6fa',0,'2021-09-07 07:15:05'),('48dc79bf6df3c03970c595607beb1ddc051ddfbd90df1157efb74da31260fe54e87ddf3d3fbfc158','4b05a96a6aa81798f96c528b75d45aec9a4301f53276271343975a318a6e9a842c5c54eadd1d8653',0,'2021-08-17 08:40:52'),('49570a08443eb1fde16d45a0889537ca15b39b46ac93870dd937547efeeacce4d8e6c3206a36466d','90c08436365fb5383892deabc07d1d2cd5eecb6786c6930b63bcc21f5280a376e7ac4bb0c4167865',0,'2021-10-08 03:19:57'),('496e7061342694f945bf1eaba3bf186b46f82b82be86ca77b79cf2fa441856c59bc19b97ced325fc','bf6c80ea3609f064c1c6ed25c6277a7a69bad5630ba80d3acc61330592aa5e6b81b356ae1533b4eb',0,'2021-10-06 09:28:11'),('49e372278e096540b531f7f43838228aac090c2bfd2990bf226164145c729a2b559bed3ab603c414','60d9cfa4cee5be1e4e1f66f8f2cad20f994af30c8fa6626d640f202f90886b326e7b92bc788aa4df',0,'2021-06-29 15:51:04'),('4a36142bfa00f8bacfe6a6dd14e521cae470c19e2aff619142a6890f20608738e0999e917140d01d','35884ca9481ec2a4e572d7e62010c293b8f662caeca4836524418ec69048f524625e2684ba203311',0,'2021-06-10 06:04:50'),('4a370a0c8e98f6b36e45666e2b1ec195aa2d174d9f669e3782cf153f620eb3853bc01229c026fcbe','b12f19fc1f653daedf8ba2078711d3497c1cfcf14564e4fd90f1be1339d37b8be207a8df3137c33e',0,'2021-06-18 10:32:54'),('4a5a84d5019304f5c3acb0065ca3c3f2f0e6ce7bc80a61ca8fec547133782abc9008afe722186961','832beeca1fbf4599f61b3b32504cac6e1bfc496249558a2f802c4874cb7d845a9d05837e40bf934e',0,'2021-06-29 10:05:21'),('4a62ff5a6b1d1e38433f73ed851d5f089f6217d53f3bd7a7fad249b8ecea44c095fef2c7a5381b89','c2ad72ef8726758db869193af290284d892413e586ff206192d46faa8bc0f7071ae00cf2a34c6622',0,'2021-08-04 08:08:07'),('4a83fc2e7c5b3ffd5d2bce1de24c2f6e23997beb6cf580b0be4b4b2e17488bf202ef45eb50ef9d2e','f39382e0398a04c22fffef576a400e6a6f1d136e3a0fa8d2e9002a4f2291c2f29d63e8d17645c42b',0,'2021-12-22 01:16:09'),('4a8428f9826c982bad4f6220e311c59fe0a35a88b3bd42e4ffc9126f9a2df8dfe552fa01329a2a35','5b4ce61e872fc77578d4abea1304f8f8a1d7a8dd9ff3d72f31ae7a11e23cb6cf7ea6c9e107f7f702',0,'2021-06-30 08:46:02'),('4a88ebb75b2beac58b2b1d4fc38939b40be61089ed5506aaf74b1d63bf1c80c1cea9317dc5d9a48f','dcc114770633c65f9099830f8a050995dad8ec4cbc6823771c22bd92878bb60c32b0e457696a0294',0,'2021-06-20 07:12:30'),('4aae946678fe8230d2f9e438f2c96d49933ec6d48f75825bf4764f511765d0928b69d3f2d4a4935d','7db8291623bb049655e960886b1c09d3c7d6bdc8e3e32e38a9f92bcd1062b5d5bb6f567bf5fb89d2',0,'2021-07-01 03:40:21'),('4bbdeed530aabf7129f22480c5eb9d8192a8b28723dd1af8b39b4328a2bb5c9f43dd5528fbc18ff0','167e11f60c5a08f84b209630efc780f03a56241a2b3914b487ada6a14e11a45b04acfcc74a19b7fa',0,'2021-08-04 02:31:07'),('4bd160a4392b61717b42c0a14c22d53e06213a1b0475db516b9f389305573c4a2075212b4a04292d','b6b72b8d7180a45064113f4b929d4710ab9dc81af308fcd34931d2d1baf43381003a814260a9c1cd',0,'2021-12-07 09:35:27'),('4be564c8968a72784b9c3a5af788a6b6e88e730cff6873b9e411640378fdedc013d3c4b5c1bcf086','c46ef79927b4eec663eeed0e55b945c2edc0c554920dd7550fc815c8f538e7218bde704488151955',0,'2021-12-15 05:22:11'),('4be820441e740db12e38bedd32d3a7bb39dd0af52c546352ed7c3c0d337a5a7d9d80b778e2c411dc','d5a6dee6b657fb6a0b56fbd7bdb3c40dc4277ca16f6e17b48b28a3a33431c2c02c46bf7c4e0ac775',0,'2021-06-29 15:17:52'),('4c155df5bfb5aefd4cb5d9193cd55b4b62ab74bce42ffa72994e2e9744c4ff915820ab868c08ddbb','8897783aeea4021b857b35aa9512d27c76278485a600d76bf48bf7def3bfd756ec6015054c7e3d76',0,'2021-08-04 09:14:32'),('4c2650fdb4f16cece222e62a5631587676c16d11991f9a82a1393c15e3fef50e9120ed4d944a11de','88138f35520fe77d6d9bd933063ba0dca40311635159701d67904b2f5624ee2ffd18ec3aa91016ac',0,'2021-07-04 12:56:03'),('4c406a2eb20436beb1fe0a284a31a3b3018cf36767c453e08e1e7eb0c5dbfd183b267dfd6e1bc337','955293d928d7497921959ed2dc28c5e3deb6a18578d196e738fc5eadb2a41520c3942a7ac0e916f1',0,'2021-06-29 12:26:05'),('4c56c61e80f4c5720b7ada3ea8fceb02649a4ad20ac9ed59ba8309e2c55a8382d2cf3ddc557ca643','0782c7c0d7ca73ed21c7faa584c0325fc7e5c9f51b6e02fcb300efa119e3000ea42f29c74b7021c8',0,'2021-12-06 10:08:44'),('4cc27e5724d02267ea157c5006d6ee378cd8bcd3cc498b0e3d6221b4341b0224fe84ce6836bd0969','ad3cb90170f3897b0760007203843001e085a919f176b8dead6f6ac17866b6d9a831c90c0b2a1ebe',0,'2022-01-12 04:42:22'),('4cfa16bc420501ce0416288be3bc3bc53cf99f980495430eef7f7405d2d5686d05ef8e468e4a3164','979df751e8dc2e4fc51ce595a511e30ca7681feab4ed4e2370257a06e42de87a99765f0fe8cde89f',0,'2021-06-20 07:13:37'),('4d019fd734b6215aeffead87f87867c7dd743980f7c07eb6f2ad02e799fbe1a05c66413522074108','9c92ccbadbb5a42cd584b9bb7588be10dfacddae02a6a9518fa03c5a6e402849b54dca175a24f0dd',0,'2021-08-18 15:54:00'),('4d8fbbc29649a177b2e280e02d8ab516ccd14fda8b7fd62fd1e89be2709b583f2d5f62242859a60a','25da3ee78fde12bacb4ac153acbeb14fb798d813e16d64c12e95f766e69eea1fb73283f6eaa03d49',0,'2021-11-13 17:34:51'),('4dba4305b6372f1cdfc5bc1248ac40b1f3fc1c0ba1417411982d4771902959823fd734252d29447f','818e10bd74f37564c99f2b103f96f6c6ba3d7d8a2715ffc206ddd9c3ca004e508cc5c524dceb4dc9',0,'2021-07-17 08:53:00'),('4e97dba0769b4e44355bc1ffa7f475a27c3af0bd96e3cb0eac06f35de9b2ce5e14c9dfab03a0a974','526e55bcbbe37a100aa967b3fd696c4e413025c94f651edc74204a1b9281354979024649cfba6576',0,'2021-12-03 08:34:36'),('4ebb73d1fb12a0f5869f9448c8165bc2bd2804cbdc42aef609ccd914d773cae50604191dccaec0c3','10ff5541a3c79375862e16c1bf366cd301fc3a7603c2a2d498b6581c5d24fe65b911572fef40ed49',0,'2021-08-16 08:42:35'),('4ebc31784f414b29868a5d6752918d0552c4e34d23960e6f329cfbfa32a3a3530f25b13470e39387','c0d125866b6832cc46967c9a3e96b38d5661247789bf6a6e964e072ae907de71d4d737cc231f8473',0,'2021-12-21 16:48:37'),('4f1e12c9e441e1903e530e49bef714144d3cc2571d5103998bff2ddda3a3bd4f66705320e7cc16eb','d3eb75ba42f1e86c81913f2c90341b45e12490d463d1f2326c90257ac4a051beeee6df3aa9d55b2e',0,'2021-07-07 07:31:55'),('4f7be5d7bb435b4eeaaf3b8a36f9a704ff3c7b9301ebe9f1150bce7a904ba5a5ae657cde6f62296f','5f279407b95ffdc57ef58fd1a2c83449e3523f8eec5e77bcce86ed9b6de12ffba4e4216d02fdb19e',0,'2021-12-19 16:09:47'),('503165752950edc4bd0d0421a9772d496f513a6ba3d2b52b5fbf253a07ad6d3f18cbcda73d54469f','4bb954a4e3254934e9f6122b0fc43eaf0a5700cc6545dbfa07f74197ecec9946278bba3260f8e0bf',0,'2021-06-17 09:12:17'),('507d3e2708840f079ee5554c962b2baee8c384e1c2f20c04e94ce59fabd933bed30bff1946b5fe63','16d212e2220a4b0af1d7c4cc4a8b4899771289b59dd4ac54381d05c994c7ba820cc6b3730d0f2835',0,'2021-06-29 07:21:06'),('50a44d3fbf1149a7745b0ff81653cea163be73ea0928c12585233f1688d5fd12fecc1026944f2405','be5f927c429bc0d7c54a5d8413cad1ffea141aa7895a934b646ef88ad9ca11f33e6b7e778c37f73e',0,'2021-06-29 15:04:49'),('50ae27a43261bd04d54a49bddb3e50fdd11dc53754480fec023ed78f8058599fe91833350fd2b3c0','a8522c4f26e0c5248ed239b7f330e66138ba5a416f211f0d07f3b6da1c223229794510922ebe3232',0,'2021-06-10 08:54:23'),('50d228837fff0854806961c60ac05a800bd21bcfa7757d11fb872d3e059fda920d0d437683d4dc11','d786ff7403606b4d0c9c637e383a3cec06b60adf84dcd5e2e418e1da86362202b48c98b54b08d940',0,'2021-12-15 05:18:23'),('50ef24afba191eaa12c69d635330dc6f1d58230e41bfcf383ce91827ac76a09448beed89a1638603','32bd4493ac4797eb053ce59f55d609d26972a41a1ae6fb531202efbe19f435d5c6c769c12972f007',0,'2021-12-28 06:59:28'),('512a47a3b538ba62fb4d8c1bf2f76fc11d3fba3fa75b83bb19e5626247a73179f839286cf62bdad2','c834a826864b36c7328596cf707f5b4762c1c9bff12d9fc0b641db79ad4b9a4509fc42adaff68745',0,'2021-08-04 01:25:14'),('5131a0e00c4a7543c1d7cc2704798897d0e8919b4d03bc32cc6cd4f2471446691eda31abe797d131','78309a20be04fb49073a3f23a245db69e1867308d77b4a7b1d3f5bb833491c53f3695a2ea4ba0ce5',0,'2021-07-17 08:48:45'),('51564f8f533c6dd2c0ca943484ac0ec62c5b0b396a5833537d94f772e81a66eba155bce9dacbd80a','9434499ff3d6ea27473fdab9431bef674aba1499b36dd41b97ac28d4531dfdd930123a314105c306',0,'2021-06-29 10:09:18'),('5183c0a69cf277d4b58cd45daf9fc8d0e20a02a3843446a5368a44f9b1e4181e44f7c58f32623929','e8f4043efdf8a72052c6c9ecfb191186d6ce208631640d1c9a840322d4eb72e477126cbf8636912e',0,'2022-01-09 09:21:37'),('51e7ae6a4d6ad80ce7bb3c1ba9d13b1ce155a94a5ea326cfcbb5f4b167d3f191024f27bfa3099c96','a875e2915dea6d3c987f105726ab3bc6c26d5e0be93a4d8fb6fcd3d5a75fa611f82cb953d90c5c8b',0,'2021-07-28 13:40:31'),('522a1ce8b15c58df437ae8a0eefcc1189d267a1fc07edd0128bb6ecbcac980473e7c693bd34f569e','ad6fcc8a5983f59f9944eb6ecc73d248471dd2cefec3e9aa6aeb46062fea167679a3a677f9db0bee',0,'2021-06-29 08:31:14'),('5259a393f86ede76851e98dcb51e0ad3e478f6a431191a04c117feb8f4240eaa5f9f94e1e0db8597','d456dcb70cbd5257856e5f7e78554436c1832abec2c268a55b49e2024c9cc8d54f3bae10485dff82',0,'2021-10-05 10:00:18'),('5274afec69706abb15bad0ca2bd191546d8300d64d6ee25d913235a32fc492735e59c703a8dfc166','c53b1b60211788a22b6ded788149aa9e3f09bda0fe2e30ada86fdbdf716cfc50af75459d35f88128',0,'2021-07-04 14:10:26'),('529a6c0fe434c3dbdefa172459d60c2ebede96245c30759cf9d7b736394d1cea4f8a7ea03d97cdcd','b917dca20bb3623ed32ea5687ec6bddcf92f090d5fad175af0e706cfe1654f8afb46a316fd96073e',0,'2021-12-20 08:56:32'),('52cc219978ca874422687540e2efb7bb2bb858d6ad04b54f11bd46ed7b7a19c6a638f3dbe775697e','0fc53afff3cb3c3b97ee10d01e7c2e98e87c0092339870a15700de0bf575be9d597d6d8bf0531ac4',0,'2021-07-01 03:27:49'),('52f0c04dd4a98e1b713acea9c15b15f5589ace44026c7dd8be3d6e3643bbe5c88a93cb1318e87e38','1873583ce15109754ce116cb863109c319bcf6e255d547909b4002a08831fd34b5e465c55186fc81',0,'2021-12-15 14:00:02'),('52f17eb5dfee6f687a2f4651c081776a001c58f692526a75060265ceb5724d3924caf55d66a48695','898356696546394fdf9ecd9e326391acc4c64100684d4c48421f1ca0757b2ad0b732b1616ff981d8',0,'2021-06-29 07:53:11'),('53258fd7cfc9fc72b6ee9658ad7132801ce7bf799e54eb937b2de5d9d75968841afd4eb21d3f02bf','bc9ca7bba45dcf59aeeb37fc5ce8765429b220fe2d617b92c4e1c7edf62c4badc940428e486bfd5d',0,'2021-06-29 07:27:30'),('532c3db61deb19cfe91c408fc17dc885da97e29a3d7620e1ba14a2f5ebb00b721b40860705f52991','297611bdb9259381ae163937479a33ed912c51fda942f7186e0992ce93c18c5ab9c0b00b01b8e142',0,'2021-07-03 09:31:15'),('533797150f02dc33bd467d138d3431734960534d9c22624992a383ae14d20d9ed1b3b1115a21d78c','b41623d59acd5e1b0d885eb491df70dd9f03d45c40724116eeff14afd07693ead94b9ecf5d6531d2',0,'2021-06-29 09:29:32'),('5348ad17c4821d477b526491844c1a455f550dca64bdc26f90e688cfffc074d30029c2d6e58d5e44','d0bd47a6bd3b982b9b61e602c6f30ead76ff7a28806dff246fcfd7f85c488c877d1fb5588267905a',0,'2021-12-03 06:47:17'),('5365e0ed507e1b4d4005ce9c008c0a18acd596920138934eacc73643b686819e843ef42e0aabcee2','bf68f1715e62746bffc7065731c7d83bf639a473c91057c8062dc3a610de9650552e0fbc6c6e88f2',0,'2021-08-01 19:51:34'),('5379327adc82cc17e992db5038bebcfbd82d30f61841cf0496374718f1fedcafe115b462130451b6','fb9c74185bcf2c7151257cccc82ac61d134969e4b6d4e8369ee4c4a637d7fbf011b7d52a6c6f2fd3',0,'2021-12-30 04:37:00'),('5387ec8b8447951aec23d47d8d78c87db0a62b6edfa4b13ac30fdb4f8590193eed3fd9a3cca61b05','1cf200a79568b24c7793d0da84b106c591dc1835fbd3703c8e2da704cc8b8fab77bb15d8196cd9d6',0,'2022-01-07 14:46:54'),('53f5e7ce4177272e4b2ed9210a3406af2153932c183505c476f352bf6b03c8771bdeb7a3536c32c6','6a9f5ffdc549b6d8b853f4cbbe34cdc9c9daae7f5a1d39d5da0d7d7e0d0910bff7f9da01fe4a5590',0,'2021-12-03 06:35:58'),('542ce2673449a4124edafca2e341e1bbf09810dd318784f92792cca3c84449e97f113398b940c04d','cce16d2e58ce3be51fba9fabe3b8fa9741a3c11087e494cdb3bb5b06beb23af20e1cfb98032e4847',0,'2021-10-17 06:43:15'),('54744de0f400d025433c3b47384555f91ffb1830010887f0584f1924f596a0eb4ee67ab1223213f6','901b05f47cd28c501777f3bca6aeb8d73014044d9a2b95a2448e1b4cca3d08efc36feda71059606c',0,'2021-07-03 09:37:45'),('54bf42669d720b9bd56f8372d311cfb8c5c910ce840e5fa6fb16397af431b4f6fcf4cc7cd3d8fe7e','991f81706cd6a5721337082e87e4d2789f1b0685d34c9af6a148ecdfb8a00dfb88ac41e889a80d6c',0,'2021-12-07 10:06:38'),('54e4bca9aa32c57bac394c19b715557ffa82941da20a886d3e280639259607b28e48a8b85ec6beb7','c4a622d75162b06da613ff32b32af6d6a0788b5ad0538803251157855a92e026da13cb47f6569b89',0,'2021-09-14 00:24:03'),('552c6bb2ee544700465202ecfd4be0b77d0948f5c5ea095c820b283939693fa82f06becff94028b6','736ca4f75c3297b9c92b7a5605f32a331481cc28d72e8eb4ac354dc7078ea1f010d5b8cac70b90a0',0,'2021-07-22 12:08:34'),('55331be635fea330669525eee242e9e0939fb934bb1aa1888dc5db03bf9f300e6509e94561b743f8','114cae3291949627d319700ef36a3807c0b9a90cee799d610e27430d9cfafeca40b510dd529e7350',0,'2021-12-03 07:45:55'),('559e6863a9777d00b4c81a692e88ae2b49772cf3db0afa1da68c99f38925b6b1ff46b2557d308454','dc8b73e7c60bff052afca41c95a88e3c10a1a1f9433906cecfb2128ddb957b19a8e93bb7c0addfdd',0,'2021-12-21 16:49:51'),('55e516dd112ba3ada448a6da07fb7a808ee339791c052a6c2d4c42d1311f73f412ddd7fb8bb2bc57','aeb4fa95068616a35ffeed42f74bc0ef5165ded852c785ae0ec2efadb34800396bbcacb110f4e133',0,'2021-06-29 10:06:42'),('563b1f1c101b1dfd8d60719ef41385d35972451e0c5812ccf4322b89ca84d826c56d05621caebbd0','0793852876d46abb150ddad036108b2f7519c44b9df9d5acb5f8f6863b71916978c83bb587e17678',0,'2021-12-23 07:45:01'),('57128d4f37a3bf5584d1981b576e76f3381abf332c022025a4366f423799ecfe775b10dd00ab9f04','dca89c33a5f69e5fc460b4e313f83985f0984ca6a16c1fa354b31bd90d8d26549fd890b9d0dd091f',0,'2021-12-03 07:40:52'),('5756208623e145329e06771883c7031aeaec575efd41d2829ae2b94880f520c960c7123eb68cd58e','17f13c19f36efe9afa747fa07a2355d9d1d0854984d466ad8ee58da8417d990180a02b985576ed97',0,'2021-12-07 05:28:13'),('57621bb67d9e67692629937d00790209c3aea7abf11322f2a00bc63efd0adbc1559a4101718312f2','dc8a528142e30a098184eca23f61aa2fde7cc19b787267255ce4e2789d69eea71c661e684080ac45',0,'2021-12-15 06:25:03'),('57cc40c9ea479d4bf0384c5334ca45a40bc33dc3b28cc7235b12ed5bbacd731b03476b94549cf6f7','509a3711e3a56e5d92059c055b3a9374c8bea8924fe6c618da2f002b4451506ea09d487343efa0cb',0,'2021-09-02 06:57:27'),('582d36d1a6f7d993ad8f4c51e2007cb0e0be1baec62e1014fe495046c8acd41c204a089bf9f49b5c','abcb97c451bb030d17ca2d5aaa5b55a0f0af1bf349e01e42d1cf4cf935867046811046eef3a17ac9',0,'2021-09-22 06:04:40'),('5836cb4d7a0a8194737f69049c82698e536f10eed4d362e18a985abed777e4d2d1b3ddfd69858bf8','9c21b889fddf0c21cdd86c64425b1c96952afb86f4ceda7db0cb97736e9a95eb26abc2da4627eca9',0,'2021-06-19 08:20:04'),('58cef9fd99aa253b3923dd0bfd686733e3906cd3923818b44df8b82616746c51a526c0c73117c794','e7252531943153a8ef2f0a7c357230007de56ac2e920fc5366b0064e14d8acbde730384b04197d92',0,'2021-07-01 04:09:04'),('58e45f38f55f3c690b1c0959407ce9f625425fc9f05b9a93927e13b889acf5c57c680ee046c76988','439b4547ca72e5b53aac30cf4679e3e47aca2a4ea7c7e2d540aae43489366dd911909dced4308520',0,'2021-06-29 08:30:07'),('5902b10c5739babbed51e748c9e3bad778a3686f36a5725face8d711b4c9ae6cd73eefb95beb9584','1129da4bead76f5481ea764b873ed2408f38d1f2ba0d05c3da0fd219d38bf5d67684ceecef78e3a1',0,'2021-06-19 08:21:19'),('593bddca2fad69484e445d893d085d18741e39b16dcab947fb92126b818c01438fbfea531c0f0c43','97a8c128ab264c510a4d2eb58c9a33088028eac1c2f91e4be1f2a7c805e705aa0affd41e80cf5e23',0,'2021-12-03 08:39:57'),('5955a0e5d7ea7288c363445fec5951da5255939292e343e52c4d83703967cf3df9f64c92588b8347','f77041c27e065fe9df5149d8c08b9f8fcb21c3c4941828862504a8e75a11384c435aafd5db2b426f',0,'2021-12-15 06:31:07'),('5958478e0ca71f7cc3684cd737ef98e6b78de4e846fd772cb0df98a999bd478b49a5c3da4c3670d4','50aa8e1c3053ac1e6e927d907ecda3a3939ee59989c16cbb7e287571d3ed46271aa94d8d1643cafc',0,'2021-12-03 06:32:41'),('5990e92a6d372efbac535340eb9dcac321f234d4af9e5c8a4e72665ccbc35cc465482111d627abba','6788d34ea067893d723f9f53b5abe9853d6501c032a2b1da3309daa18727824b1469da347afdb832',0,'2021-08-03 09:00:27'),('59a94122baed2d042b4055452096a89d4093d29c904d79e1500680349a118098d05ba498c398791d','faf120dcb45eb9b4e5512433c9ed381e4b4b5f9374826a349b0055c909e183d1d56f1c1041cbb31d',0,'2021-07-01 03:44:17'),('59ecaf14ddf4390d672b7d9e0c1b471e156b92bbd513264d47007c94288d6f82ac3732fd74584d5c','4362bd792c0a3b2bfe76017e63cf0c60f57e0b401f3ae27cd247447094fc27f87c4aab03465f8448',0,'2021-12-07 06:07:43'),('5a724ce355e89b33428eac194d303d5536d676d570d616e2f3066d9331f148ffcdb30f20f651bcab','a6aa2e5d63a6a3a84445b5a965e3a14514deb16f5f15f964ed2c4ef6633483de29f6cbcd59731b41',0,'2021-07-04 12:32:16'),('5a73cdbca6e64531131b466a1103603f09097a82cc788491a8d2b21aec07a2f5d5077c5d6b55859a','3d6053f335999fa1e76eecdc209a96d43025ce0aee1fa6b38022706cc966793804f8d321030b4bf9',0,'2021-12-15 06:47:40'),('5a9ec313ad52486df3416842f7388a45270ee46d15f8889c39b0fba4dfd48433606d7eef26ed5170','a2955347c048e9c63e48d43514762ad25e3c30d1c3162607e6827d3d744291e3db44fa776b3fa57c',0,'2021-06-29 08:43:17'),('5bae6534cac662a632f91dcc7824abaca6f2d876c7d3d00b500f058b3e06625f2c73cc5014c288d7','2021fd6d34f5d14266f0763ef19184b61eae76cb2740e1e559982347e1ba00613127df1f648a3ea3',0,'2021-12-03 08:29:45'),('5c02f7a2aa801fc3f656f53d658baf63e208bbcf8e54d75da7869987cc5b05f95e74a682b9efd40b','b5993c07dee352ae6121b7dc88c2cf0e1a68aa9b5cab420fd5e664b19cf1f3c6b1a923a6c7a94a5c',0,'2021-12-03 08:03:48'),('5c81d188aa7cb135d96f73328a9e254a369bf06d84ce6ae2ffc74e5acd05f569eccb7a95f0f7c4f0','19208805e1115c8834b2848e5fa383f6f3251452aac6387a4822c364f6a7abb1647600f7e85aab74',0,'2021-08-31 00:45:15'),('5d2743ab57501a72e0b132814f3ec0c754d9aeca720360f191940a200ee263bd0c888080847bdca9','3c4e1ea4d6bde1d1670a492fb5e4d6a5286927a58d9d08ce38b833e482bd3fdc65e6cc995f7d09a1',0,'2021-12-19 16:12:03'),('5d31500d7d2c648c4db9fe9986297b10c1be610fe5781295119d61e41fc88c8dde1cbb30c11f0dfb','1b73130cc2ad87c209e909df429f92e5e3176cc8471ceaa41aa56b14d73ec17e7eb40162c9d05ab8',0,'2021-10-07 01:11:36'),('5d72a35b14888702c94be8155dfb3591a5f90138b08a8ae4e0b76a60dbeebcbcde91f4e3e6a67fae','979a761a86c66af88d6cb7694b34d1a2f5d4000551f0fe82271c0608cbce451c3f27dfffc131b12d',0,'2021-09-29 03:50:24'),('5d7a303fad20c7a2102167a4a3a3ecd7397aef2665d73adac53dfebf09828d458868bd2d5f7dbb9f','395c25e05a445769a8aada6dfea68b2dc644c6aca19d3a7df598b194c894a7ed6ad716c7b871df11',0,'2021-07-17 08:52:10'),('5de22e6137a48f011c0a10fe6f7013be2901a302944dd05eb33652f51e8081d74f358134fede2a1d','c33fc4e7c34aaa90b70f2b63aa5926ee0d8a6e6aaf1e8ab0d143307733ec71ea5c6983e842d71e55',0,'2021-06-30 10:02:46'),('5e0249527c1f2c6809627674e33c61ef40e03a85483df4c48201b70c6fc87da3e3e55fb9d0eea60b','81fe1765786e9b56a74a549b878b6915aad0a2175e6f042a241aeaed08eaa04a62c581f2279571eb',0,'2021-12-24 06:48:14'),('5e201b2b2207fe10d2dbb0a34d9065a108db7062fcb667217f8a12dfb2da073b6a08f5765e32ca97','95bca8f7d47e2227fd0abec36ffd69dacec460b9cd91755cf476ecbfb3e830b03b7fb3a4c04aa94d',0,'2021-12-20 10:00:10'),('5e676a62e104376adccc77d5cc2eb8aecc6fe35b520eed3056feb9a775797b0c408799c25031e692','306ace72a1f3f5581f16e03bec5aa12265f16fc59cdea56ae746a6d3854afe99b164b69b194ac52c',0,'2021-09-01 05:13:16'),('5ec004077c20e71d91b888e94534a272f16a1c8411d1204e528e83215fa03fa8034700873bccbec0','416f4fa3ab76873c04aad6c085fb5beb334155ba06dcded8ecb7f50d5d7ac042401c483e569bead1',0,'2021-06-11 05:59:07'),('5f395c2d76f112dff7bd638c9add17945feb5bc698101b949ba8e1624d5034c7ba0f9e50db07316c','8255599852951268836b28584ccc53f50049ca7f7f2c75a9c3a01341c5ff53da50f10ba906969ace',0,'2021-12-31 08:41:12'),('5f4676bb5036a2492b918ef086512204676a8920a096b84fe6291c6be672a321b19ed19211394184','f4b73cc10f87618e67421f184cf15cc5452ebc2922e0d8e7ad2f8dbdcf73c49fa9da1fe5bbb6addd',0,'2021-07-17 08:46:38'),('5f612702275bcd9874b7dde102adcfab412be0e228909e925bdce0a6eab69ef3515b5dde1c30e652','f9ffc3efd2c0271000f08fc12186c73ba5278cb5b4d0827f42b4da5832f5d2c8e3939415a7e041aa',0,'2021-06-29 07:51:59'),('5fccda44f58f794463037afaeeb80890011069ec56206e2fd24ede19ff14f3152cc189c354e566f3','495644b4014b7618cfa8336fb4ff71925d46c9fddb77e35c2f7f77888d05dd42527f090a82f34531',0,'2021-06-19 17:52:53'),('5fe0bca8d11c1229c406900313950d6327959d0b223f13bf5ee909ae015772e435a6d49a88fd6790','85ab7be3d5a0f9e62bb62901a4e54a49926b5f149dae16cf5967a10367319efb6fd438f789c60221',0,'2021-12-07 09:56:34'),('609bd1170ef9b119d62bb17352c08bb979502c465d11aff0ac91cd4e4be71ae71e3f3bee6fbb82a3','af5796cfabdd165010bf9fb18e0abb72cdcf868f2351da6ff3f71e90f35296806423ff82a6278d45',0,'2021-07-02 03:41:01'),('60dc67550d2cd97920f13ac171823feef8f931c55cbf4fc7f19714dfe0cda0ae0e67b27f2aa1f23a','8ea02bd4adfbc9c9d0254440db266399b13e297b8398c888927f06b317b8dbcaa54749f76640bf37',0,'2021-07-04 14:17:16'),('60eea67a373dc73c0356f9f761a0ca8e9a37f7703b6f7d822f5f083514ff96241309d35e8ce1a593','e124abb327fb3251ac83d444b00dee137f8c2208f11af726dd608b1bcd148180c8c4c614e5f0c71a',0,'2021-07-01 03:59:06'),('611d8f9b357cd9eb242edcc6a940f0d9b2a4558841023953681546d849b70ca2244e961075925c4f','92ad016912ef3bee9c7e27a5a7b7a8a01eee708e36b6dc880e1d010fd42032c3b409a20ebf3e9108',0,'2021-07-06 10:01:25'),('612a3bd63e0dead8ca37c08ccb7a3390f5f093f927f24c58f032ae56771d3d07410dd515bca4c53f','c647176a35cd3f2e00f3369e265d22dc696da253b9ba3b1a1ec86d4160453a1aea2b7e3493346ce5',0,'2021-12-23 03:43:01'),('61403b17623931d0eaab6a4de9f8151563c78a1c68f66dc108f877d8eda050443c8d8e5a3067178a','2082a094ffe3bcf3c3f4510011f0e9bd6343ed432fd3c52545f2ba972a0815588882824b7fab8bd6',0,'2021-06-30 09:32:06'),('6159adda7b9c6c5899ce3058b22d610e0563ccd0bf4096fed405ee19e5487f94f1f8144efd0df8f0','7faf05b59794999932209b699e7c21a256eab3a490ebb2b2f7a6d84199f00651861ca2236ae6ed76',0,'2021-07-04 14:23:56'),('6169e1f79c23778cb0ec2574b448b9bc3a3a8b985bb116795cdf0523e9973df40692feac1bceb78c','e35714b39c165be39181ebe209b1de9405b016436cb78998c513748a7c017e97455dc7588383754d',0,'2021-07-05 02:05:32'),('617219d67b8f5629c07972b9bf8fb34dc341556314242cfb13895fcac9d7e225cb29a90ebb6164bf','2841af9a767050c97d26928cf7944bb14006d4afa445d4e3611c4dd59f2640385a97d011a88532e0',0,'2022-01-18 15:24:43'),('617367c97520cac83edb80baec179a332b15cdf9c0febd992a51830816ed2f1867797c85a101ee48','081d914c16fbc0a0ef7c1b6318c6ed579f1b1636444656d59cb73cadecc146a25963b0f135b17167',0,'2021-06-29 12:39:37'),('61cef34e0eb9836769a86bea3f76be9a5586f8a90c88aab58bbe707cf7622cf894b99479097afc36','3181a776d6cbc65bb5a0f5f1d2b9026307cf09d8879a2c82f7d471b0e4e4a776e61f62af71b6d35d',0,'2021-11-11 11:06:46'),('61d5b774c4df3204ade3b6d8b847ee449b89a8fbe4da7d7dcb6c6403eed51424dba082e2da23ce78','79edb2118a140111f86d6a04cb541bd623ed4a2ee74a89237d901c1d5ca7546b87a36fdd039110b7',0,'2021-12-08 09:43:30'),('62239fb695c0dbd1baca93289cf402654e1d2d0c4ca21f9261057c6dba06dfa1796429acc1305750','0f454ed7320d251f7cd78e61b116fdd8ef06628612fb191fcce092e80ed02609a8d97ac43c8bf8df',0,'2021-12-03 06:55:33'),('62488ebfd3be7101cd1f73ace206fc7ee468d8c28fae379bc79d62f3d1f84909f96d17f62f33d7a2','7f1ecc508ab778f40878d29925a5dda0ff3ea50f8bcdc6c279e3c4d81e7d6efaf0a2579a4976c157',0,'2021-06-29 07:07:13'),('627be31764154bf5115bab1d363c5fc425fa07406432c5d52b88bf293108eebed772a984938e48c0','76df45a2210e9c157eba9debd323a2096c78bae63535693d9d668c55656fd52f324b656f8199ba54',0,'2021-12-08 09:25:29'),('628b15a92eef4225b4394d5d36e0c1f993826ab79a80e482596c948783ecc708856844b73696023a','caa91a96b717525664dc07f15a4232ec7464bb8cda6ae42e8da13a90b52e88c3bae6dd068da7acd5',0,'2021-06-29 10:20:46'),('62a660535dcdd243826bb16d48065858ca03dc643ea23fcb2e7113fd6b96b7339e8094414072ea88','6d0c9400f06dd753531abe5962bd65a6a0b04bc54bdd71c2ccf963ded6d9d447903a1c8fc3efd38d',0,'2021-06-29 07:15:54'),('62b7cc1c13323e0cbad37d4f29702a52065705709901b649c29fa358b0ef46052367b1c992483572','9e81aeca05d1bb48529a935e372dc9d677d624cd53f0358322194bc67051bf978655fca7d9e5dbcd',0,'2021-07-02 03:40:18'),('62db2cb1b5101ab7cc8711e10e9af337d6d10708226006755512e63ce428c18f4516ffdebc474c20','6015d7921ab1d0641ce640c5c7b2f342dcd6ecf885b9e4056b98938378e6cbf4e6372b6ff9c7aec0',0,'2021-06-29 07:12:09'),('62e2233fcb7e924aa768db0ff18e342114303b8e3a3f91769bab22abcab9b4ac5c4210af30842e04','cecc70d6c57ed51c11877334c923073a4274bda329ddf3330a567c8f0dc9f1ab14e990da8d66061d',0,'2021-06-29 07:23:00'),('6360e922365092570d32b9c0c95c9cd626a010882fe8a7ef24d0c9c8c4eff89da8660212b64adb90','1fd1e8f393e1f8eaef4eb494c13d94a45fdf10527a234ac6c1da0886c9efac1d73f392d11f8e9c5a',0,'2021-08-18 15:27:37'),('6459e79f7324eca5e857bc113943ab14adaae222587ad3da5346a20eb740ef5b8747579f14fa8c8e','1b3fe747e658b01521a06375c8e54190ac57e77dae99e09772b593b3e0947c1f557c8652ebb35d3b',0,'2021-07-05 02:37:24'),('647d85de110ed3e9a0781fefdc0478b48b9bb7e340a3e57d48188a621ab261adc6ea8698cfa85a60','8ece6ab1b3372b60d0183abd38dddb72e2d5e2ee7c02fc83373317203f85a17d0b8c62566c710881',0,'2021-12-20 10:44:41'),('64f462f66c2d8f8ebcfc1581ba13a4fcbb362f6ca6944574b436c20421696ddc97053f3a3bdf1290','7df80eb84a75e1667d06f44a4502191655110b6042142f047f7a954a2f692d936689bb17126fdfa3',0,'2021-06-29 10:09:36'),('6516c111ef31420da33eb7d1047e007e6496d0f8647d2d4abc14370d4f4df2651fd0cdee2c626922','86120f639575650559671c372154c055d9fa0a03e266f9d7049a254f4dc2d911839ebc355e4ca85e',0,'2021-07-17 10:13:38'),('654f2d2094a3d96407fcca32fd7e0f6c497e55a780f554dc042cfab03719dd6337e4410b8cf62344','0913d333e2b4b75bec07d27bcf5e2cf54d906442c27fb2b58a1184aa4e5463ed1bbf0e9b2a4ea1d0',0,'2021-12-07 09:37:37'),('6572dd0917b039d7e074f4fc9b946042a67144444664f8a28e4b283f56efa686c62d77fe2c069d1f','5a1cb1013a8b2e0f9fd3ac4ee0ce5ea282392e8e6569fbde0616eb30ef6ecea51379d3c30250a1f3',0,'2021-12-03 08:50:47'),('662ed0f186755a7ba2ed98ea5e98630442bf1373af1b4bd30bab7211896c79378c1c0c898efba3df','ca6b05c83350d73368685e279bae7b60007fdf3f8162fd92f75b211945760d6b32029b48bb257777',0,'2021-08-01 10:41:43'),('66386c41b2b4669497102e8090bc7281d711825c7716d35951e1ae21de92e945102f8398dd2ae57c','2ffda80d830cfa699c66ba36f74077ae1f2e1d92eeeed26d50887a5c6e7429770edae5aed497aba2',0,'2021-09-09 02:12:50'),('665a4f52367c59615d43ed3090f7f5621f34bcdf7528e3e531def347e3fccb26e289fc111829a09e','c0f80027796fff6d97236000c36ffe65a59ab7a78e5c97aabe93f05c924e48a3d460bdf3b5047674',0,'2021-12-21 17:03:21'),('665e260247045f357cba392a15ed468e67ff0b03651b7f0ea8761f09447589b6d29ba1d811379a9d','c7b3059f0fd8f426c98567ac357c2c83bcc8f752b945d52786b1af5b6c5ebf2a5cdbf106f6119b08',0,'2021-07-02 10:09:47'),('6665091a8cf5d8467df17e28ccbe487d46b302b2b8818c6ae980cbb07ed155ca248e1c645ff26b9e','ee21dabddde4c8b6280eba9c2ef9cdbc629e01486c74298e024798f4a60a77e944db4a6b66336b13',0,'2021-05-11 08:05:21'),('666b272d6402795e0d285ad41b8ee74236e0f9ebd31c7f1d37a4093374f3a3ed9820f1d14c31ff42','38d539b6c1e16e56dfa088e892f3e597f0615ec8c149c4051be191a38b0838c466c28b8daa809d44',0,'2021-11-14 09:34:15'),('669c9fb34747ba7daee18ca89f7c1ce77db5d337e0a0e64f1fc92749c283e9ea0436179057fd7843','b8adfb749b09107dbe2161d2cc2c511326131a95adc9f52b9185e1992760c8205e4cbd6eca2fe50e',0,'2021-06-19 17:31:37'),('66b308793b503e09b126cfa054fdfc746064eba9edb183bd7f71a1d4d3c5059d8cdcbf131717e85a','9a0c97df48e8b6b98f11588891cfdf3ba943240d394bfbf9a76c6ba70dd1087b62087c9467788672',0,'2021-08-20 03:32:17'),('66cd60704ce8b53bb4b51c6bcc5e61b8375337b8b72c52968d462078103625a83a11bee67d16a518','86a39cdf4fb0174f18f4739d63ae5335b344ed43d583b37cb1ba179e063bf0807d3358af0db5930d',0,'2021-06-29 10:02:27'),('66fb5074db29b9c7a16653791ab160cf6a12a965ebd3896d79fffaf57e5b6f11e1eb3271e0f205a3','04142114b47fc20469b7f7570761eee9e8ddfd49ecdd1f7c60b1111e6dd4d4f29fbe80201fb19f49',0,'2021-12-03 06:48:29'),('670b4bb890f66e2bd1e7c1dafb21b17f30de90518f8613f9dc77c0f18b37c2faa0b3a826eda9fdad','aaabd44e90284abc3ef3e7b6755c091990f0e52db51a998c0f91307501060460bcb58d711cac50fa',0,'2021-12-21 17:04:11'),('670b59cb62492c323d6517b9a5dae36128edbc6ae65008a14908f67d47a5651def399eba67ea4275','ae5883561226bfe99fbe8d608626c45a44568df9448b19e208ef08ba989b284a47adde43edd50159',0,'2021-08-27 05:31:55'),('67417bb88c22c4e8c9267bee9304149e9098c888acf03aa05f87fe88edab9c5c4d132466bf2e7a83','df2c8f7424c473e9c71c1281078926e0ded15e348f258f3052790bb005a0fdada01397f8bbd7ffdd',0,'2021-12-15 16:17:42'),('6745ec14d40f33946673907cac36c8835ed96be80c4e39af3df0e4cc0c19e3602bcbc6ae0f6f8a63','0f70af8e412b5d0010d0371c20d57cd4dba1c702a604e4bce911059f778441418bde5617a249bddc',0,'2021-12-15 05:41:28'),('678633ddd678356e3499cd74f86f3cb5f7d648a8b3656c6c390dd3bb6868230834eae1d0c207e17a','c81d37e4c8d0ae83b2cd511aed1a8f7009b37395b44f5fdbd4226f9d024c9fd77b47d9eea81481fa',0,'2021-07-03 15:34:17'),('67a49c585eba81082bd342bfef27ecd6742f38507ca14b0bb7314dc8ca58de886929716472e2d088','0aee8073980daf22d29e5d68e965cbf31942b466194487d27ee1863a93cb2099d28c7e9f9f607cdd',0,'2021-06-29 08:31:11'),('67a4eec3d45f57d5ea08bed09f116eceae5604035b74aa005193eef9057dcbc91021038596853aa4','9b8b25c95f40ce8c3924ed130d01e6ae4aa0f63842760a78f0145652506ebd10167f32d86768cb4b',0,'2021-10-15 06:09:43'),('67c33a7b9b7ebc050aa1e0047e28d46711cb2291ab7d87b7de9061b19702e4a5627d5e855c93f5fd','7ddc9fc1d2ffca6e5fc6a1e44331279364bc5034638a285818865a14b46fc00ebd7b348ca6b13b52',0,'2021-12-03 05:33:34'),('683ff8b6979faefbc423aba7562ba8364d67a966b449e7eaa007066429bb4072193d14d8198f6d99','ae9010b13441ca2a9654c44f410f70f56e46f4f6c298b43cfd0cc2574612b06503fcbdd6d67c0c66',0,'2021-06-14 09:54:17'),('68848a3a20275256208c6d3306229d02af1e1143d9cc7dde32756da38fa4b23b8ce65c117196db0e','c1eac988edef4e9591d4673708147fbe22d60b059475acdde3ca072ec81c74298f23d6396c86c0ee',0,'2021-12-21 16:52:01'),('6884dab328393211011bc59e3c75b1ce4dad2ef57635dda18758b0306d887c02524a1f3099cc4d81','6bce23f924485769eec8e7509b63e6946abf78c5aa2b4784092152454df481cb336c8bf9cc45ea74',0,'2021-12-03 06:33:24'),('6885e484d5d97989a8b2c75b26d54973118365c941fe30dbb9198f4115cd48241180c72e79b1c003','6a9f0993e679b041bc5a155ad06ba1c749d7068329ea511770c7a266ae226cfdfb738ca58e4421ce',0,'2021-06-14 16:55:21'),('68a8e5bb49376a54700175189324e876c7e5e86cd597d674c3d28d7de949c4d901c87136b1e70f95','7f15d24091d70c52911cf00c0808056d3199b7363914feb9d97502e841bc68deaafb8a2650877666',0,'2021-12-03 07:25:19'),('68b5b980b7b8c5a998ac86ada229cd0bfb0ce4edf05c8f63034ea61ac93261ab4d9b6fcf2f803552','492c0fbd6e5c0b3c2d2ce4030ea1038653e0416956c28fc80a13d79bc12279f17174168a4baf0700',0,'2021-10-05 07:10:19'),('693cd175c9a739588f12246aa379fea69eb45969b1ab94fbb61fc2c7458b306d8470821cf2c2c666','ba9721c2da9071b3e039b3cf62b7a42036e3b17e631c7c451f20db0c376e8b2f36cad23209151984',0,'2021-12-19 15:09:34'),('69e276d9ed4e8d0ddfc4bec4078918c18ba43fa31aeb4b7c91ec9059f4452733ad46674a077fad3d','a4aeb09a8881dcee4493c07fb8d0f794f59f562fb67ecf8c12ae58e4e2d490a61de41767259c4a5f',0,'2021-12-15 05:17:32'),('69facb4e7151fbfed420b302c228e60457cad77e65939ba5fb15c2756f915cb1a007be004b20a3a7','d453347a5872da41880b7e71a2960753f6081ac80b5940d300febcc123dedd178f5972c9bedf3c44',0,'2021-09-21 02:34:08'),('6a3f4904bff232ab4021c9d3fe28f5e36369149e7b95f86f3e922f62c5016a1c0ca5a89f18fb6789','88b02bedbdd52644c4f7225d61d136414d2ffd317b7ee64f5fd193f1d53f29d256673f90969b2a3c',0,'2021-12-03 06:32:40'),('6a75c8ea6b9fb117bf62309095ca179e4c25ec8f35abfd33ac4e8f57b9fb6d499d94b96b322fd35f','bfc28f30f608484381ccfdc22161e7a7dc1f7cfc406e91b2d61489724c09c16c6334650262b56abb',0,'2021-09-02 05:21:22'),('6ac660572555eed55129014e1fdb51d22a9e0e776d4c02fe285fb03324125c07278c206e8b1718e0','2f14b6dfd3ec5e7a69f623eb29224adddd8b483c20baac4f0685dd25a7a2427bbbaf43f7766df32b',0,'2021-09-28 02:23:26'),('6ac8d6b1c2631ce255378b023e653fa77d0e066e2207cf4cff6b87dd1af8b146bb95c73307023bf9','aaac9efa334bd7a67bf81cdc98a895056591ea1b3bc9c4c3f52e0482f9a27217ac91f0f08fcce52a',0,'2021-09-27 08:16:15'),('6ae6281b10a5404bbc336952d299e915b7a9c5ade0228a2c1d3c5cc5fe3695339bd5923b21622441','9a7a36914f1e4e038f4118a4add1fe6aa959ae4ae285ff5a76d34e81b15bcdc029966cb914cd3b30',0,'2021-12-15 05:42:41'),('6b2fd259065c562774766861efa56d4d9a1e899c86e349472e55060763fd5f845a85286a9e0b86c8','04ba4906cd4c2e83227a9f9371fdbc297cf5937e77f0403c8922d2fa5e0c7bb81b3f2e9030dfdec9',0,'2021-12-03 06:49:23'),('6b8a9096d4545d6fcef6af0e8248c029b7a9dde2d8a01c040b5e92532a4a504e7fa9331179db64c4','8c269b5b2c8b86e4f03784c9a3272ff068cef772ae27f6e4308289fbe07c6441eaea44dfed4dce97',0,'2021-12-28 06:59:05'),('6ba6b3c010a847b7b1ba94061330b0ac1f269ea94528235db8d26caea1d149a353e0d53b3e9ffd8c','b89546b723cc7154688d438fa5d3747688ee6c64ab5b7ab78eb76884af621a4826f2cc432661122e',0,'2021-12-19 16:15:59'),('6bbc74b3c85896590258a52983ed8a32f1dc7465a5b4ee3255ffd92c8eb57bd63ad8fa55f9dc7e19','c8a3a052973e59f6a1db91e14e0058e6ff9f01168cd0f7ee161ed428a000663420ecccba6dae8f1a',0,'2021-07-22 07:52:55'),('6bf9f02a14f653b93b8784a7694583c96aa7832ef2ad8b56d12e2595961098b19800d1ed8a25f070','7f6c15a0ce9c680e9915ee94fcc44210a36b2adaa92034c95d7e99dd9f7c249e521330eca30f2bf4',0,'2022-01-07 13:44:57'),('6c2aff97a2790c5613959a202150059e29cfa17f14b74810f5e0fa4bc0bf6538c771bb82f1a4af8c','659d7aa52d3d7f73b98a8cf59169541c8084441c707fabadf054cfbeee9e1618d2988b92ef9b1edc',0,'2021-09-09 03:02:13'),('6c4fc9e86b1b5b7ff38568987e28f8e7ef33b7c54fbbece9a86f4cbc6aa9049b60783ef8c830b8b3','93823a833b68845305acd559c24e45f8f7ef2e4f0071c1b61cd90ba24e796cbdf8364c8fc494de80',0,'2021-06-29 13:38:12'),('6c7af280c6ead3bfb2abbe714377da4c16dfeb4aa2ac53e9bd68034f438fa8abf4a2ed745f1b84fd','25d3df625dd4811890eca075cfcd39e09d527073143eeac48ff0e2b1181a3919311d0f5dc971aed6',0,'2021-06-29 08:25:39'),('6c7c26b50045c1dc98c22e04aa9247a5649b72991821a420ac9a267718e6c689ebfaba308f001b5e','4f1445e5ee092439569624d8e64e088d6c5513d3cf171598a8bfdf3d49d3ebb0d45dd99a0ad66f30',0,'2021-08-04 08:58:13'),('6cac9fc3e4197383763178a79f2caf4bbff4feb3b96dc1b65152f3eef04095d8471e14b6ae5fe514','2774e134fac34473838ac582a1562e046fc04cd116f939ca9fcf87dc09f51e0205e482fd0e63dc5d',0,'2021-06-29 15:05:08'),('6cb91512114f70c19da5c2789dd0ba989d6606390d78017e52f599f12fc7427966852b82cfcd6c94','2c74dd3a12d364ea1c53675db6e14390d3be240033ae45548ea97bd3d74e8591ca27e3d8275ebd5f',0,'2021-06-17 13:37:43'),('6cbcd866c66691400ec570949f8a88c352f07e7ca95f7091775abfc1adeaa021d7c4376152a98f21','28f3dfdf4feae99be310e1f78c500c26007ff3ec81a5a3a16a042b1743c9438a67d907268c05a783',0,'2021-07-02 08:02:44'),('6ce8f4af4b8123a1b0fef43fb5ee27317b68814e880e79596c940421af1391c3b750778f0b8e9ad7','e698c509c41f7adb9d6a8ccd40c66773ec214b93780a432c31a73e6b8ecc170bf09b792a227e4903',0,'2021-07-01 03:01:27'),('6d32d610bf1bfdc50f8bb163054104d6644fc748a186e8d926096d6ec57aa43913c87cb8e9af846a','2507855d55fa28bf144eb7e6b1e67c9c030b8460130ae49444b34fec0011a0536d4ca768f8e45cc6',0,'2021-06-29 08:25:13'),('6dce5a33a7e60cae6c182474bba8e317f945f62b8717e75b5b002720a239dd80a2d4e9c3924ba8ec','b181fd593515dc659aba4c0f6da920db170a7bf13e2fe6f9b6bc072dadf41706f2b48e570bbae7fb',0,'2021-12-15 05:39:10'),('6ddc0c272d46189a75706a15139bfaba5ef312ea27f86b01a048231789a5647782e43bd2d2e3907d','3cf6d3fbbb3672601fcb588eb9f300cfaefae185b218b950c5c683eb34713f99509ea9a99a789b7a',0,'2021-10-07 02:29:51'),('6def0015a1363e0d8290cbd641d082441cd424bc22592cf7b8e3a1e53924ae882a5bb57336ceda57','e33f8c5cb2372b9f5a3190408d79d54a480a1c46fd7fbafb7f314b7d202b174110ae51902a9e7a44',0,'2021-06-29 07:27:29'),('6e030ab663518581bf4ee5534792afb5ba94ae54815a8fa48989dcd8026697b25e53864013432681','18a7e9af07dc7a031bb7943322a85ef0b1ed42dab66af532ebae1f46834350ebcd73558905a46023',0,'2021-12-21 17:00:40'),('6e1fd6f305dae9464fea3326720428f24471fef9642f587571c76aaf23451ae6dac4087d5ed8db2e','160c6bb98d3eb636513ba1c699b0f112efc7991d41c2b3d7a06b46f4609b76bf6c66bff6937d67cb',0,'2021-12-18 07:58:40'),('6f021a62dec2ee9b5cb8bf6fcff03bf1328f03999bda6452dd8c6d880a862a141596ab50faeff114','a718488716d976ae9e82bf054f16e49d46f03908b534f43f0f3f94fc3c16a8d64793e01119edcb36',0,'2021-06-06 12:12:10'),('6f408b7287dbe81c90c4f6e37c97f6f4bf3957c711315253334d1b942c1e02be3ffc4d7232014326','893cd5ae8796ebfacd7157ccb05e2b01344979ef15f3e97984517ae50521b71fe14fcc33fa41c8eb',0,'2021-12-21 16:49:55'),('6f5594c135b7efe228faddfc23d7230675f4d1ffdf9de6d33d8890ad5588c5280067deedfc4bf763','2dd71821912ff7e01454add870d435161d519fa7960a45d228581e7d314bfebdc37ad256ead64c0e',0,'2021-12-03 06:34:10'),('6fbfd07b15623600d4f86941e25e1a49ff59e28eb93e21f4ec91eaa1523092d0cb61206e7eb419c3','e226f0a93b203be2b2e98db21034d5a2d02afc0139b9c36f40cfbf0376608b3ca9b429740cf3d67e',0,'2021-12-03 08:57:29'),('6ff261a17509d06d0d82db323a7b8a6f9dd4a09ef3c05fb3412f84486dfb7b2b57ffcf343fd7de01','42dab3e426d85f62653f8e6dc48af25bff547da19b7b7214053304275e1c7c5d17788dd34ffe6f70',0,'2021-12-19 15:11:52'),('6ffe99d0bb376bac6c8cbcc1fa4e582b0e29d721ad2462e5ccfc807ca367dc40fb72df7cb10217b7','ec0d47b56394d2011138513859e8dc53187ab950061b24a8d50e91f427bb44131ad8e6ad7a25ea2e',0,'2021-07-04 09:27:20'),('703473ca57e8d77bb3fc2f14080873cf4afe71b198d403327883b6ad50d0392bbdad9ae2199cfe61','51520c5b55100fc8e4fe925af4ef88c1c20e0e40fd69aa2044b63cd60c94ddd0a3b6a9d747e804d7',0,'2021-06-11 06:30:28'),('7087e48ba71d72eb41798ee6e9712d5ff185a8902c93974b5dccac94d6774fb12a7a03415c594411','00ff63ec9cc7cbabf24d9c99f70181c61bb74fceed0493f69b42deb8a6011057dc50a0aa28452c3c',0,'2021-06-29 08:29:54'),('7095c4fef8138c3ff5bfaaa209622aeffcdc49a748726ae409ad9a6c686afd0a3e907ec46f3cbb90','04a9768961c3e93a4f1fe02697473cbbb8baefb91e8d83241272d256c381b631f3685b25c2fa7e71',0,'2021-06-11 06:49:20'),('70e6de9a15f1036cc665f9ca0e35b9e8edbcc7fa8a2da69c835e33901030ab733a020b6d15e3548c','f14e1c6b3cd956f08dc19e53fdae03dc7a881c5cdce08e0beb5d4da867847f1ff7b489982a14aa39',0,'2021-07-01 03:04:01'),('718ecdc85e746359349399f3f2d4c75eed301479b2d807606cc820bf209aaceb9ac19eb42415f682','d8f309e76786641062e9b7fe3cd5096c8a3be0bb21f4454e07daadb98acb75416bb6d3cc713cae65',0,'2021-08-18 15:42:16'),('72519c339a897bc26a913956ef3319fc6e27ad94321fef138483999f0cdc8796ea9245f61c1e824f','abce8b2bdb2eae9da285c4d9d50f11d62ff3d3b5660b1781cdc55bba3847badfafad5229bd63d58d',0,'2021-06-29 10:13:56'),('7272c1dc660460634474e689395e42593a7c77c34f485e16980cba8d5b87c236b0455d1465f50c4e','4b8427e8cfb1c32dafe584a17f0baad7323763f6444981fedfa7a972110a3e82d6fa1959552819c5',0,'2021-12-03 06:37:57'),('72902b19a64b20691d0281122bfa62906a4c4cad3cf33b8739e5d26f570ee97b8b60b20db306156c','a66d3fdda23cd66c61a4547aca50aff89e1f34ed5e21522186b17e76f96022a22fb47c156925d4d3',0,'2021-12-21 16:50:21'),('72cb94e860e855bef24711d76c4f7a1adfc46c86652df60cbc7a8effedc415aef6bd83873b4f5b77','d2142f6418df0b53f06688c0616aab2270f6e6cd48e84b7a84e27da4f5ef4bd94c578518f6c2b199',0,'2021-08-25 10:49:34'),('72e49e8cb1520ee51197167f8adcbd2a99394b9bdc53239c3e16a9a43fcecff5eee42d8661a5f807','3cf2d78179a8add22953d1600dfbdbfc41acbde08c6fd84b8db4dc0a45c51c7ff78ef33ce09638a7',0,'2021-08-18 15:15:59'),('72fce8ec912f7fef18e7de358b61e0d2f3c021f7ff42384036cb2ea19bf534b7a0441fcbbaafb636','3350faad823478554225fec36d32cdf8d5ace8ba22aee826bc3174150c94c8f03fe9e7932d0ec758',0,'2021-06-29 14:30:52'),('733bc0c3bbb551000674dc2e64e98e77b48b4302b7e135271b31cba4bf09a0a2048d1e7afd7a6c02','2161180b39c05105205416e2e34d9f7ab3f17ae03e1a3c580cb3d78b4665e0ef891ab7af88554b5c',0,'2021-06-29 09:41:01'),('733d7463df3a15321ea8b587ad89c7984356fabb771cf5bec7c2f38b4e34f535653320c47ee14f82','050145d87f8914a639b3e81aba165c00c320775e68577731ad22718181b42778d6db85ce0f9e5d7a',0,'2022-01-22 16:18:11'),('733e5a6eee8300803e5251fca5a77886308fe6799ee6f38dd57d59eb7c5326801a3d9a32ff872949','0d603de3ebee6696e3ac0a3c2ef069c0286836f6005b6e23e1bd2893ba4808f8592a911210ebf309',0,'2021-12-03 06:32:59'),('73a52c8e5812986e832a2dfb56f28a14f5291327cc76329818b5661dc83c43f9bc30278ebe44edf9','8f54747667364c3322dcf54615596393324d64f806f1f8e31b6eddeabf78d6b292a454e19faa0909',0,'2021-12-07 06:17:12'),('73b17401fef41e3482352d54a122c6e05dcc9a14d2ff9fa4c627b6ddb51f7150ebb45d97bd0bdd07','ed39df58f2333c3a1ad0adb3ca4ef988e40658c76488df6fc75f0b1c8e5b0c3f8465e4bc21f2cd31',0,'2022-01-07 13:42:51'),('74017318f840c211230d8bedf02eeb62b7157784d8e3dcec8a623977e87824e0aad0c36616a1987d','43aa30012c55948f9bcc9d8466b219e7753d2e785527686ec81bcfde726bb42d4f63281b83330dd1',0,'2021-12-07 10:00:18'),('740852f04cab1d912d022719e80a6e8f983d56fdb553122176068282edac9906e6a2c938fd77f3bc','e1184cce0ee85a600a755a354e2c3df1108e7a0f96487340a72e6da341ceed52483039817325b740',0,'2022-01-07 14:49:09'),('7443028aa351d3f1d429b0535b1977eb8b0aa2e9c63f02411cfdd803e27d1646c6091fd01bf3220a','29b19757aec2826e21b35cc5d687efee7e87c4fbb77edf4c6ab91f5742a66c8cd6332aff16e8d004',0,'2021-11-14 09:29:28'),('7452b2e99bb31e3409159b79402ea4227a602c191e5eed0e1b97e1b780c5fa9b44620b9ec7551c81','844ba9b8b994e48379b8459f573ebc62314d09fabbf684b80c52f5db712a7df13d00ec742a4f4e4a',0,'2021-12-15 05:54:17'),('746bf7ef0fdd73d383e1e10445488f7057220f767c942515a892cf4d47c7535ace46073990e9cd79','b5d0011cc25c30a90455b86268d94a8180a33c88922a169506ca5229cf980899fc8d9f4e571030eb',0,'2021-08-26 00:10:19'),('747509bb675126020c02e82d79f2cb79d3058a50b88b8b7590625c128ac922c54d82ff80dcb5d3b7','c51836b9f9ab3c3269cbd9a10c099f3ead2bcf3f099699538c62ef9a32d928f01e52386ccdde6d46',0,'2021-07-06 14:29:50'),('747a6e886f72ec12ff588eba176c30d9907474eacdc1e37fc1d462ab27255a4447a6226f24fda31c','4a80ea1ef4ec3098cac600a0db0713a4e96b4b8bf7c7e571dc1822f4ff20f7bcf12899d80f90f5bc',0,'2021-11-14 09:42:38'),('74de638453907c958cb0ed5fb36b5a98f48635fad8fa1d1653b7000c1942194c45f90b22bbdc6932','c390e368226d66592d8331644ea93c7cd5a1d18d3ef06a1aac416b1f108d56cd1e36d8bbddafc34d',0,'2021-11-09 04:33:47'),('7512f215b8db88f38ba3ec6dfbe30c00cfded1cbe94d88d3b7467e5c471761df3596e7dbc9018e0a','5dee7f032b7a1b0b426516f993313993471a42a85a082444e3e362ebf2577b08993e26658761e106',0,'2021-11-18 03:41:26'),('753fded22282d9df5ee612fb4ba61874cea9003e5bb014f7ca8499b5ca9025fd0c274608892b6c93','e6f41d0134255c02914d31a5e6d7090eb1595c8b7aaffba79ac03ee37f5f0b943653b25f91201b88',0,'2021-12-06 09:51:02'),('75610dff8a60ce3eecb5897748587d75789b302e95c94209f27050ea62969feea63dfda6e6725268','3df30fa01a7ca06a916d64e628c12ebb9949a9213098323b15eae3471a9311c284c5466f55108307',0,'2021-06-29 10:07:49'),('75728b0aa12f4b4dcba0a36615f87054483baaba2ba6569419bfd4938829500273125760b6b2392c','d53d28384294a7b56adc115d40d0cb8fc67dd34771e88d8e32c7620a29129fcde845ce0f7267ee71',0,'2021-07-22 10:12:36'),('75a7ffbcc8401a0839c5ebea0614fe88bc87718a79799df4b440bb54b605599b22b1e46016d55d8f','7b310891aaf9f2069d2de8418ba48fcf4acde711e2d43066cfc4690f014234706706a7234d2b1528',0,'2021-12-08 09:38:51'),('75abf73c9b729b0ce65d28f5334f1705a673965358390683e21682547e1b51e4b11922539ef103a2','54f4e39dabc2132347313c5d0fb2beb2baadc5e524fff0879be895a0656eb82e2860fced97b562ae',0,'2022-01-13 07:00:24'),('763169933249bb7a8bb8f0632e348e497f42516b242dccc3a2b7dae81dc8fbf79830da958c288cf8','ca26f4d9936c775e9ae2bdc98ffaa5999d0d86e5468299bcea6365ef5c89e0c6ec5679adb08e840a',0,'2021-12-20 09:11:00'),('7653cd55f42a00e702271d0b263f847f960fdad91fc9d304bde11ca494b57df6d2c9c9702f721bca','25780dd577114aa0504a943e8099e760786ce47af3540fd55d26f7c31a1450900ddd54b094e374bd',0,'2021-12-07 06:26:05'),('765b3790b7b218e4f109a0ee32e6ae1a41b8c9bb03990940b3d1a44ba68050bfc8c3c43e66678eca','2b8f33d68c05a80ef1cf1acb0b4601ca8be3db9682593c5cee41f9e3f5d20b8a59a53495de0bfb14',0,'2021-12-08 09:38:24'),('76d2fc630991531c8d3edda45cf23f669bf074198162b3d61a68c27fbe1196228be1db2761d57a8e','c260c52cccfc2ac477a6d82bf62df504ef10c231e9c8e2d843f2d3dff7eb5abe5921521a4fcbbf12',0,'2021-06-29 12:39:27'),('773609c654c285fbdaa4f25a463690a479e239aa0be5d4451b3e0c668d25b7468493620d7a64955f','72f54e7e13c0b50154ecf01c6ae448374240ce6c6e8dc53579f355d41c261f39d8f52e0f8cd5c8d8',0,'2021-12-06 09:55:28'),('7743a112c31b98facf67b170076d35877c455a2777e60a506b744b64b920e578f4c1ee2954674222','993494a55359aeaa774e78eef31bc5cb8e77f99f5ab0778506b26a8f2ac2450c748063c5c27651e9',0,'2022-01-18 15:23:37'),('774dfed7fdbad4136b9d759adc9a58c6b11228fb3ddd0343ff5fd758d9eca2a71e602bf9aa8b0144','6e6077be84258e30e1bfee41154b9240d2d652eeddbdd9fcc2bc72fbab1bb106a5991ad28d2d9c0a',0,'2021-05-11 07:51:24'),('77b8d03f10a0fe247d12399d4bfbd3d37ed32353f4da1f12285780b1fd6915c4f52a1a3e514ea760','522bffce3cc0a428ba560b083f4deb272231aa811ad567de36b633619b20774b68e4611624437b14',0,'2021-12-31 09:26:01'),('77bfa3c4b7e067b84de822f7e54db4e13957dfe0a7e52a0a958e63418e909e330a501654ce02e9c7','32f49a9c238c12bf7a2b3859820cb3bee335d138dfb6a9a88a8e9c6ab435c6c4d45c95a2324c8f4b',0,'2022-01-12 04:46:43'),('77e74904b4ad94b3fe944d413eb6281802659b09ac08f10d24b381480488fa0a96dc3167a3cf1d87','b6a28b311ea02cc113e3265b02112d6be17055ce6e3e88d80d9da8d0137dad63b558102b1e7c2acc',0,'2021-09-09 03:03:50'),('780f9a363fb86f03a2419c53233fd1ef843f9a5ebf8e0d940debffa64090fdd0a5bb8ff164392dfa','7134d0c952b0887a85b67a3ff0e2cfa58d25d33b88e8f05834e621afb23a2f135a1a66b169f9662b',0,'2021-10-13 09:23:04'),('781d67d4fe7d9f3d5586ec8f2c47066b09bc0944748d67db048f6abeb785e3fd88c8d45c97bd290d','041cda1e822c364d15b5c4cb494c6a7ef546c73a2170b3bdb56eda8823788bf4b1ca52b44a711740',0,'2021-11-14 09:43:35'),('781f30a110bbeff36bc765c237bb1a0000b790de75c97e4c6d325832185b350365d14a3f07770776','46b1f0a31f68a1a7d0d27d032e454e2af66f4ed1d7b06037959bfd0538ce169fe2dc38fa8295ea54',0,'2021-06-30 09:58:07'),('7970bdddd9c436fd2bc499ffe06630e4e8b0e9ca416cd878a88e152738935985a3a70eeadff54ced','0965291000d57923611cdf1514439dc6afdf7fe1b5214c368f4823920bfac286b6af9e7ac63e48c3',0,'2021-06-29 10:12:19'),('797129852f38b716636610d5e8cfc9da9e3248fc23aa9d54bde811dd2e6842fc5f3309efa00b53d7','43896f5d2904ff6aa66eca49f515a928d3f46d91937c6058818a1bbd2b53e3c949fcb94b9c2e5d8c',0,'2021-07-02 03:45:33'),('79b32fb99816278504e040fa4ee111f548ef311a6d62df3462dc543995c46919b7baf84e4ac76aae','114a0af9a2ea6a8ddc7000c5c6ff21cfaf02ae703b11e7eb84a5b82cc4f7f88ce1748b85a860084d',0,'2021-12-03 08:36:12'),('79b79752b8f6f28b8ffb193cbf863013009cdddca34edbfb8431f09b30b0851aef6952ffd0251d90','977e27699ba4ad6340b0a63183bf540d2a21ac60fae2feb18dc23de61678371830cec825e978cb9d',0,'2021-12-15 06:30:45'),('7af7eb7301f2dcbccd36e6bc8ddf069be556630909d60253c74aa74ffb2db7891f742f8cbb34b086','d04a790d8e18d5a4ed4ebcff7a50e6fc1553e4d5e25d5bf280983a36ec757c856174a4aae25c8eab',0,'2021-06-30 09:58:14'),('7aff4b134ed114d7a42be143061459953ca233a6692911ee6f787badfe3fc104cd87fc792999a307','cfcf0e4d62413c282e2a9e83b21d56972042d82ae5aba4bca947a534e83ad33c379cbcb9599b99a2',0,'2021-06-29 07:12:07'),('7b2f53c54fcb45d6d272e59285d6fc36719825ab28fbd11079fd2a1423df606de46d9941c1392e93','d17cc8ccbb0830c91edbff9c24bee2d024221544091990d17d99c4f8c1f2c3d6d4963c69c4fe8785',0,'2021-11-14 09:44:46'),('7b3c828cf23b49ff06eb171a05dccba5b3968cf65367bc1ec02acc7c2081f00891ed496416963d87','c6e492230f4b544e072c3942af6af79dafae1664d0c361b2775495b448ff9311213b3d01d56f06f2',0,'2021-07-04 14:02:32'),('7b800ccdcd8f813ecaf2333885e513412c486ccfc3309800132b13b07a584b380f4c580487a7c0ac','e7ca2082be01e4860ce39ef5b322ce7961ebfc2c8381004db287139087b9f4b307b99d43550fe092',0,'2021-12-06 10:37:26'),('7bda269dab98be0e4c3cd93e49576af9331c495816717508c0e627216f6a42fa5c91f0d56b095b0d','207d7013fc5142cac408b8899ae5c10c61651b2445b2fbcf1ef348d7ae0373e5c8bfc1b460377805',0,'2021-12-21 16:49:35'),('7bdb9d162469ad3e0cfc7518dbf88064d474e5b2ba51d860101c485042521962b6f9f28c0795666d','27fc215096be82573f8d1e339ea4ed531b8f63957cef75201f03287e5e2ba3aadeaf0b093d5d02b2',0,'2021-12-07 06:04:01'),('7c601aa90bfe11443c6b20a4b2daa61177267c85695f781cbbb34d1abb8cf3664b2407d3d6b2971d','18b86d371ed2bdcedc009540c6c48c9ed38bfb7c4e35d2a26e73e624628a370d7423f37cfddd3f35',0,'2021-07-01 06:19:22'),('7c85f3445f3af811ae407349eb067dc9ef992b2676344efe27cd0d5c1d82b0e7c72e2fda384b9ef1','9dedf712d4d721b540090bd95282dec42ab1600e2405b8b2745ce4765a5d31e3ccf39df7da7079ff',0,'2021-06-29 12:27:34'),('7c91dc243e0710381cea4379553a62bd21e3cbd5f710dd56f6c287219d2d97878b273e21a3942968','31646c6f6a58e2ea76872e04920d5d5f99b7af6d9c4c894d1a83a18d0e46671b4d446936828a0705',0,'2022-01-07 14:45:03'),('7c9a2cb8ec814927e148ee3441b4b875959e053535330cd5e6109486be8f2ffb1e6ffadfcbd30d2f','19204e4341f802e8242f929518b7c1ef794ef8b032586cb239e4e9ca06e93f3006d07ad309bbeeac',0,'2021-12-06 12:15:04'),('7ca18e6a398a2671d7f8960d7437e31aea88ac2db3b7492455a70e89730a183078496bcadaf19083','def525fbea121d747d6df87a607a3f311b2b20996521c1464fd3b5b18b19b14ba3d86cf96febecc2',0,'2021-12-03 06:33:01'),('7ca3852c4e5a9e1d096bfaa41aeca7ff1e130152ac2ec786fbf6a2cf2a195abe338bed42996fd47f','7aae13cff7da6386ecaeeead539c7f2176c45830f322eeabe58bb683d873d4f6122df662eaf0c932',0,'2021-06-29 07:21:42'),('7cae6b27dcf01c6d2193dc5088eea26a62f08f10e1bf09d00c55908db6b5d2f9140e2525176951e1','0ec6ec77151045c3ee46058846d865063c8951c5a8005a49892e4a5750e6e1a1525b631282fb13b8',0,'2021-11-14 09:42:13'),('7cc13daf0d7b2b556f6be52ad04d427716d742eca0692c97283851204058dbdf226b63cd4fd05741','a4fc00c297ea7468ea6ea92d8019917e6413509394720eea7ac6fe1eb43f2bacef85b2df022f7149',0,'2021-11-11 05:24:09'),('7cd86e7eff9fd2cd134f92d8bfc2874969347bab4b8e4b5884fbca4aa28beada81046bad720e8f44','2645157d3fb9027863de7fb579a2c51e475c88adf0f2c6cdc8b6810143d472f1ad2a6d2844c7abba',0,'2021-12-07 05:18:37'),('7cf72920144b2ff7897cb2edd1f092acddbe429f7419d7d4bb3451745d1d8ba8e3accd46edc192b5','510e69c9ac77793637d3e9f23a641efee2e063a14cd9eaa2ed3dc9df620da30000811c402983dd4d',0,'2021-07-04 14:10:49'),('7d51e5d2c6fd189d5e78333f6ca20e03d107758ef0f3b9f61831d9fc8188f0e3c92e90e0b362b800','71b4b68970a66f60a3c79adce4e1d3a4f878cc62fbc727541bda2e29a35bfd43b105e74a2c0324fd',0,'2021-08-21 02:36:17'),('7d9ba07bf172ede934ba645d1d31eef9faf0715f124514804b90ff1b96f7dc9e40662f832f40063f','cd2f734689e83d60769a2b0c36051edbe88310b00e2472d6c70b670ad029d3ccc811412f0d63b546',0,'2021-07-05 02:10:11'),('7da6260818916d6b2c300013607563779bc5e1bc1487b431ce0cc316590a7862562e75066fa9bef2','74645e0ac8cf141e182a121e7b4f8d0e5da6e8234f03b360d5d3e360d310a4bbeb9ca56b390816bd',0,'2021-06-11 06:09:55'),('7db7669022dfd78906139319e5e5b135da4624ceabdc158e2c7c3fb93f31163bc69d009baa4da33f','34179c3e77e86ffb5214d485bc560b74dbecd648b7c23500dab3fff08b84709f160c5bf1bc3fc178',0,'2021-12-17 14:12:25'),('7dba5f1e6c3afcd0db5f59e7225867198cac510a55224fd8cb3b3bfd9c0aba5dfbc9abc4bb64ea1a','6ba8123d38529f2f7a439cb9eaf4c57095abe8f50586b429219fe19685c666e5d1fc1450e1a6c1ce',0,'2021-12-21 01:29:16'),('7e4010a0795c1a2efd1dffddf3cbbdd8f10c44239d1dfc2fa849f41294d4e6effbcb6414f36dd2a5','c8c548f8baa08384383bc4eb8478baf56be97c9c26f47e39a8a50bd649916494273ef8cbbbde6e86',0,'2021-08-17 14:00:04'),('7e63dabc234f77a3c9e38ffff3a7eeec6722e4df6ed9f72fbd76f582a3e7428cc133f0f7efc14210','56972fce5528a415905bf8888a6a151bcc15a376f405665f039aa711eb125a19963663c0dc1db1f0',0,'2021-12-08 09:08:43'),('7ea22e1b4d9a818eec37a9a1b1ac792fbeb98fbcbcacef8e52714aba3687f7089a00c48eca47f78e','ea0592372910c1d6f9e1a844749f3d1ed041781b8e8c4b45c339fe26091359a402f4bb7050f96bbe',0,'2021-06-30 08:12:37'),('7f6225b7e8f95f30adf75682aba11cf674cd85603d4bfeffe72d9c8b54bfea8e04773cfa28844347','29e9a4943971b1deedc2f0d49c89c65e00d293adf78559222513b8ded432d41a9500eab103dfa9e5',0,'2021-09-09 06:07:28'),('7f98e68da511cbc0470055c2a1392cbf02e79971d107509bd041af3586443085d7e04a3e25cc287d','e0f646845d70ccf21a65767450406ebde9f4aee658a5799c4f426fa75b6c5d95437961596217c6d8',0,'2021-09-09 08:54:22'),('7fde12f224c0825d39e76b592fc43e01366d563d88f00a0dfaa9867083db3c2fcc9b6d902ed8c8f7','a1ad1ee2730af0b80d776c7069682b7b357675913d5e6baaa47ade8acc889b4ddaffd4706e4daf03',0,'2021-08-15 22:39:10'),('7ff50db5fbcda01fe58a71f8ec6c6d19d3b14133f2566dab474afcab28a93c66293e7c9e183cc19f','e02733dfb2abb6990f92437bf2c263b385fd4105505456afbc4079f76c1403269da9d704ffdb2627',0,'2021-12-15 07:05:18'),('804d99486eded53098768928bdbd0a3c6cd96beeaba31ba44a40403a1c7f59027034e6150c359651','655a294846b8dd1252e02d0d5e9565b103af4066391ccd7c076f5919b31da5955cf20170c8ad2761',0,'2021-07-01 06:51:14'),('8055a59e5c315a60300b6b76b11531ea2cabecfe58be39413df5ca2b39bd5fd2f96a6ade2b919856','7ee0ffda2044865319e77517fc83a3eacc994b2387425cb67288ff5f7d7b1e7310f7b186af24123b',0,'2021-06-11 06:49:14'),('80b6a5fefe03963a47b603d5875a2e9ff2d7e21131ba2edbd8b9bf2a92cba762c03032e54030afed','975fabbf85e88b9d48f1905fcbdc9104792b0422e65b157710e72b546f1d77e858959352920a5ff9',0,'2021-10-25 09:09:47'),('80bb0a229e852cbf6c91d45959eaa1ca715b4d4743257f69868987b9db98649b0fccf3baafae6229','21e98808560f2c63a298278c32f3f18092d25cec7743b9ce8629c09078ed8fc27294b9f0091510ee',0,'2021-12-03 07:45:12'),('812ae6dff5fc41bc994d8836aace037fe9a91054232af49401808b37807cd1f80c92e2bc801b1f36','a85fbe659fe0bf007899785b1429ce50e46cff12651a10fb71ea54905d81dcd8818bbd4deca4910d',0,'2021-12-21 16:51:51'),('8142cac8dc18ebd2f22eec1907e1e53a738a3d25c389a197ce8af49e92fdc5824e787d78b1a382ed','75de39188d1d4df3cc7c8f5d8cb886d30a99d1eb32595d2c9299f1e1a3f7dee26055930e8cde25e3',0,'2021-07-28 07:41:36'),('815968d3d6214535e86e8292972e803b557668e0bb3352781ea4c4a0c48ddceb38afd9e623b7696f','a6cb4f42cc8bed86e2d725f66856a1dab9d96d17f1c8a1f2dbda1bf8a36c7aec65c52297a8ab8faa',0,'2021-08-18 15:23:58'),('8265db7d8acda001616cfe799a03e76e5761b9d4504f3add7eb3c3ce9e325f924b5daa9b35bfae11','faab911187cf78ed3f3684021d609041928bcd984bd6444d9f2743fc1115f660e541ff54ba82c9ac',0,'2021-10-16 09:48:00'),('828ae2b46e5e914666e28489d552da276e16f6a1a7a1c0aabc40b42560f1b1c3c98091e9715e15cc','df71dd36b711a8fa44ee236fb1752d484e7c6fdbdd7d21ef181755a4f8800034f0ff2be195013f39',0,'2021-12-03 07:49:35'),('8298e9dc5567e82455f98c82f844bbd4e8f7b92555d297b953b1dcf0f9d0ae899519a59f25f2dce2','d73ce22b72b6612fe14f68756f7a6fa17e6f0ecc292400b418ddee061b7b1afb0bbe0ddc6b0edf79',0,'2021-12-19 16:08:03'),('834504de8a2a7a6271ba1a88cbac81465633fdbdb5fc2490609a8902146c9952a45c2d58e672d6d2','8328188454bdb4bdcba4c2631f6345abc3d669a797fc505aa0000f5484a89e00b498ae1401fa8c9d',0,'2021-06-29 07:23:15'),('835a5af1b1de1a6efc3cc2895b46a0e908a50e42204868398dd5325c56929207723f76e6ece83150','4a402a4c59ec2f48022767b91dd01fa4cfa324bf10db247dcd5362ea5dc8feb029af65ec30c41cd9',0,'2021-09-15 09:08:02'),('836b3ae4d82d5b25cc96db5ab8bc5a1de2667ec68b1d98ca60fdf5b4f9d859a045fb58c034682463','c75f61b58a5a3d38b387e9e1652b6da2b46b7f44744d9627c4ba692326fadbaf670e3551693c9c43',0,'2021-09-02 07:03:59'),('838ed395a660defd1e07ba6e99992542fe54fa45d108978b500565accd4530019a14f79c9f784dd2','16cf00fa55db685c8e9d34f948a3e015925513a719f3780dc6386fefeb2f58d7d70a37fc79472fa7',0,'2021-10-24 07:54:36'),('84094be4c5e59d5ac5c0637c9dc84e9f773a39a8f4421d70e2cc2327ad24c8553f5b5b3cd8bc3842','53a95e28d2a76715225ee3e286b52b983fe9f6e945ac43282d1a4c47adbf0cc5cc8c2b06e850d2de',0,'2021-09-02 04:39:52'),('840b4a25eb92c6bcc7ed7f33efd6c1a3ed3d7112204b319a41e490e98209f330307869a6a8ff79a2','47c3fd6e7d3439a179977f531c2a9e65391d28de63e9a25aa2c6a60938a0015f98c3d29edbf52f7c',0,'2021-08-17 13:06:02'),('84132bda03c9b07003acc6b133cf52b27715106db7a9a01ebcf935d5b99146e925ce022b5a94fb86','ef32bda682537fb16e0caf58ef7bb5fe20364bff7eadba4f7fa782c10a12f9cf54f0a03ed57b1166',0,'2021-06-17 13:41:59'),('8419999bba5d0f1d9386ce81fd845ffa536eaec5d826c8558aa5d3f3db4f788e46d65e8895a747af','ba967e0df04ff65b80047877c7d647f21ca43070a46bef32d49924169b37f87e3f875dfbbba59ae2',0,'2021-12-07 05:24:07'),('8441d2792e6d37cba8a493a9a7d70e5d8f58a5ff51fbe7921ea62af0766513ba05024e4f19de64ba','261859a4ff156c755d5feee48bcd51157b7b2d52e5b0b270a4f104a7ec88c15a832014de46e77b59',0,'2021-06-30 07:45:10'),('8457472cc7dce548719898df0734ccb4af046bef85634fd10cf8c3d6475a927346d1110fa0bf7f4e','2f378cbf76f57fa23f653008533d79f46aae97ad3c114479dae4b5dbb031f072394063ff619dcfd0',0,'2021-12-08 09:06:14'),('8464c5e81ca5a560f402768f6f2e1acd14217083a4801769c70b061560249fa25f0718b5b726f8d7','21c9b718406af5a23f3322b5b7428b906d21d728cedcdfb51980f91c79b08387ba0196c5e34e1532',0,'2021-09-16 06:49:34'),('84ab300d5467e4275e5f6996bbc256927cd529d87a2e110b2122af76bec2a3e832072e02acb5aa4f','b985fd055df7c13daf178cc8e513c78e34aa4f49b56edbce0577a50fc2fb2ef56507a8922170b8e9',0,'2021-12-17 14:57:47'),('84e6263d2242649a1ffbd46f2bc54d03741019b5a710ba4df855b5eb11a6ba6443c6042e33ab514b','452e7d4371bc13ca695cca739ea39689f855e88b66276776924c6c23726619fd59fedd2b2aa3cdc1',0,'2021-12-28 06:59:12'),('84eb8209c7aeb57323a8a7f61e3e791f1c062ff33a372e43c2fc57cdbe7004d84c2e34ed2f09a79c','bfacd40620a2276002d7a1e121a2f6258a7e219184436eeded699dabd83f58e4180e597beb92a5f8',0,'2021-12-17 14:57:07'),('84f278d6a7460f9e4c06805076948365eb04c0881662328da5998eb2796c1165c51e81655681b92d','4509bc92cdf3e7b6bd4ef40cc5f9c038bd6ac95074be3e3e5d669f847670bed936203526610b44f4',0,'2021-09-29 02:08:59'),('85030bbc216f8117fd134c9238e03121ec7278de0cda8c50f43dea7c57250b5989b604f87d3181e0','3702856f8a0395f07310581c593b96d7c376acc836758b4134d3725ffe8fc0ec3e31239db179a510',0,'2021-07-03 09:30:14'),('850642fe9690b6bdb1b5892a4cf00644ae1b836d209852057a32e5c795aec7d3b91b9bc4b9f3fe21','6a19453bd67179eba801628b7ec30af134c32e34d8fd6f5ea284a18a447d656be3bed1177f65a072',0,'2021-12-06 10:16:25'),('8539f132eba1aa91e7c35d52388a26899483fd985228bb18e07928935af3eabdc8a78273b8eb9005','9b7cb1fa08fa3fc2a629dedb0053ad30d78a47d1ffd4a5749384bb52f08035db4f4b6e15840bbeb5',0,'2021-08-18 15:28:30'),('854ac5966356ef9dcf1498f2917419ae25968fbdf7447821803eb0bf1043bf2455d22c5b6c00a8e0','0b122d1a75a575a458cc5245ae2ddcc81f033df4cece228719805e701163b450e99850ccfe2162fb',0,'2021-07-03 09:33:20'),('857490ded37e5f225fe01f7c410b66aa7119700fc0512f74204f35f40ef4051fea3eccd064a5dc50','f6cf3728a750c4172f7d79c46a7074bd3cbb901b58d65a85e769b78cc30683881e402854a7e1e1c9',0,'2021-10-08 09:09:58'),('85c946ac1c819a5a4350efe01ec194d519e2158e394c146ccb5b9dd2ea7ecd691530fdf62a8e6bcd','b4c44d893d6e6b233bb7fbc40be9dccdf0e671d08c12f0d7e02fa2d14ec2b05d52d750788aa7bae5',0,'2021-12-15 05:40:34'),('85cdff35d54b5b53591fe7f6eeec0083dadc61e1f182311f8b2ea8329e07ba9f678d1a055a8273de','f5e98d1dfd32ccc311c2648af0f554ceedcf237f12b30774e2d1605c01becf765d1fc22e97b00bc6',0,'2021-06-29 09:40:13'),('85ff58c5c17b2649fe295cf2006e1612e96af81b319d28d4ec6d51baf15ce332affd1625c244d1e2','ea886e2028eaff9566f89ebdbb230896a21d55ef64479e485dc346ecaa06a94dff77cd4378cbe567',0,'2021-09-26 07:43:04'),('860d71972ed142f837dc875891c47a9f8a545920bf7b5b8454f5e7128e4302adeee40e0067b28769','befd39d055751a686c3b29f6efd7d8c86e092fecab7519bfdd78e998bc8b579d482a08cf57d8d573',0,'2021-05-11 08:03:09'),('86262a66dce08627e7c4ece775d82cb6306b71d29b7547c95df43b64939ee1d1cc4188b4171e1d2e','0a912c672933db5ccfdf4e7ebcc9b65de5934e1c95ea3f7917229f5e1153fb8ec6ec824e600de597',0,'2021-12-15 05:41:47'),('87499850c51f17715ae6a1a0b40cfbe8e563cd658cdd8566696797db2adff0cfab9d1b5953defd72','532a25fd460f7147caec7290f4011ab1b29144c1efedec85b8af20919af18587ccdd286c3737c6f2',0,'2021-10-14 02:58:58'),('8752528551acf038a1fbdcc0ec745e92b87858d2703ffcfe2d795f02fed595ea4d854da9d918c208','45786a6de8dc05dd1eb5f798052fe7a824521aeb91f69e0050738c7a0c0b88204f42efeb3afd1a45',0,'2021-09-16 03:40:31'),('8799a8a6e9b297386259fe7914f512a19f44233b30f7fb403367af386870a2dc3026866f02c80950','156571d66ab94331a07c36ab87337d73282fc922cbd0d50a490e346dab6b3bdb579ed8c4131c2022',0,'2021-06-29 12:18:07'),('87dc1d2cf4658d107d48060c2988cd2132d4e9026fe2419e740a962d34a9a6283a5bda24a408c69e','7a57e102b465258dfe139e8c4945f50375c9b8f1890957482c0763b24c0ad1f1a97b40dfe7514947',0,'2021-06-29 08:29:02'),('880cc71cdce2e0783b20948a2f360798c0b6d8ecace4f1a14754a78ba9d334814d94d0ef8db67dad','62abb6d6aa8c2b4965783cadca926a31b16d46fad831181a86ddd075136142c96fc38189d7c307fe',0,'2021-12-30 04:09:42'),('8827bad5dd7fc382a5693409d640e92f750a0ea44039d3adcdfa0a4492d40bde687a9958cee73d5b','f62f1c8c093e92827a083ab65fa0e4c5351597a36557d37c431f8e47b79bbb8c96ae1573e0a9744d',0,'2021-09-04 02:14:15'),('885e280f267d42c5c111f03864ce45429115f073b0b9cc050da33da148dfca8ff76d28ee5f32cb6f','8ec77941dbb7673f9572664b900ebd8736f90d4d73f62ef7b33575efc09cf0159d5f9abd58f5b195',0,'2021-06-29 07:32:45'),('88c990c98532eb75a6167e86f3d97962864d9d7d719b14ca869c8ae4c89c0c82b12b62dfbab7fd14','86a02025369ea6d9562149d46e98d7532fb6f22912004a0e80e7ad0de9265623b175b97e4f013fe4',0,'2021-06-29 13:50:44'),('89055347b70e1f1bf2081af8a0e4d302d17b83eaecf870cfdfe5c28c4f327e6c72456fd43f889922','d9f4b4e426e0e7dc235e0925c8e28bfb9abe48b2c1b52a4cc47eeb34d3584541c09df76ecbedfd09',0,'2021-08-01 10:03:59'),('89257a49aa25193c441215ace7bfd375c9244e7cb2e61a2802bb1649f35fa3c9823d3169a4dd7d64','6a8505ddb7080822103df86073646270347dd64fa3c73a8c530d89730042f7e0374ee7f680636a3d',0,'2021-10-17 08:13:50'),('8956f14b1a35dad245430f51e9cc3db1045dce5998a2851af9a769c02d1bf44dbf1aa7ad7814a123','e2a5808bf771090ec29046d0634469ee94d663b89954e4a32c3673e31821b03914a1d7a36b8ec448',0,'2022-01-07 15:07:51'),('897c80ea2efdfa61a5e419cda8908a63f53b4c0c6af9a5bf34294c4e41193c889de4768167db837b','ecf7aae13f7edf99f7eeb0bf3b1c1c2ec6375fc0011e6c22c548bc9fdbc0de01291964b25864ec31',0,'2021-12-03 08:36:27'),('8999cc6580410c42e145ae57f78664a1fe0b110a6bc72d79c7cabaf95577ee3328dc1ad14e76099f','c146c5be57c92015725a3d3e9424392abf8af9c6bc2e5bd516f887eb2d77f0eba82ab26acd6e210b',0,'2022-01-22 07:33:32'),('8a01cd71e00452c487e77b86a03f739f9d99b369f353c9c172bc14c3a71261a0c8561fa7054bdc7f','9ed31113ec4a439c44ceb11838496caeea5260fdf03549c59c26332d09669ca649f5582c740fb462',0,'2021-06-29 07:23:12'),('8a36bcfdda5aa88d036582bc638a355f8ced8c93fede1221c55dcec30c087d5a44569164300aeac0','e236559f600fbc9f36bc96837e3e771719d74051252b521d3658e8a1b573ba36532f3f61ed856340',0,'2021-07-02 08:13:13'),('8a8d5aa7a094b3a532738004b071f057b508e96b5fe7f081def1ca9ecf7d135a0499d06eefe940f4','87fb77371bd99467a10ab7e0fcac748605f52cfe02c44f211e52cd243834dc2924e54e18e2629f38',0,'2021-08-03 07:13:03'),('8a92c59f010d25ac4cc9c3a2c1785aff5e517702351dd6f9e85bb6256c557c598c5c16d52444b640','d0c07d363be7a09554b310b33e4dd8d58c283898db565d36a32f58c43cd3c0fe70ae1e8b81b55ac8',0,'2021-06-29 08:52:52'),('8ab0dc0294564cc0a89c4abb0f0c2a9140af772b7188097e58d83eac8bf5051bd999309930580516','b90221abf03714ba0ac3af5f44c7407387eefb2838d3c98daf74a41b4166dd1dc7a8ce9a2df39330',0,'2021-07-03 09:30:32'),('8b7c8daa9f4c7d4052b58b665e20ae169bf22239a884e1970b7920f5bef156e59c7f4cfd983c10cb','fa2cb8e212f1b52740f8100f7e87352f4c3da0b3812aa6d65569ad6b5864ceac3bb86978ce359e03',0,'2021-12-07 10:00:02'),('8bd3d7788eaf967824780bc005cdf8a5e672cd84c0d09aa4b484c1bfb0b472db1d1e388e4bc3d5ea','9980e68f58107f0469423079ee4e9799f5b4defacbb3a3fe81db0d725cb6ddd30778b97cb4f7b0ce',0,'2021-06-29 07:17:11'),('8be3e92a6d4caa8e3313a9b9e2e7b41cd050f1e835401a34509d0b839f6d1ae704e9f43bc909021c','9acf0648ee694b51b183ea51f4567d27542e6137620d0cc8f8d7ef34da326e925b902ddc67fcb1d3',0,'2021-12-03 07:48:34'),('8bf58f4ecb25d9f574325a96ac0b30c44fd5619bd600c97e9bedef1cb9440ad4e09b0d4a29bab328','f469fcb07c2aec14af38882e9697b73c664b037694127e3fcaba53605107b7b8c9e2c1fa8b21c41f',0,'2021-06-11 06:55:32'),('8c49b40b1ac3185c45d44155d662f4c15359d6e8040ea8729b8581d1e44d51cd89eaf164aa24e645','6b8815a91e40e0d939525d42075b7c1fa743295441626b377dddf3889fe244a79ff460eb2fe31b6a',0,'2021-12-19 16:09:12'),('8c6e7728b160f92d09cceac4aa848e93ea26d08b11cb9dd49cdb4ddb2d008bc3efa4b1977a4f73f4','34c09e44095fe8d3ab8123ead149357a04c1a9b2c4d07d595f9a9b82d3ebc1e2cba0f33ff6c47cc4',0,'2021-12-15 06:40:40'),('8c9edf937f80641545a21e76392907f6587616ae538feb2958548e3288b10e765a35b213d64c4c65','6c86d41c09069fb4fada77a8dfb15f8430874e7da11ca1da6c7f3893e8c53404c312173bbe3bae37',0,'2022-01-07 13:44:00'),('8ce296590f7583bc8724cebe2cf7deb7c653265709e00bb227103ccf0cabe69cf814e368a0e6a37f','f484b9d216b9e82d0d2a6e30b74bb08ba677626cceb5cbde2d2c9b11c64285bbafed2033f2a6c81e',0,'2021-08-21 02:10:14'),('8d2dd427cec700be3ee0ccdf5ad004fcd2437346880128cb4eb29e2b3c9b26f92f13941e8e74dc15','dd9a6932ce792ac8c1d6850d299788784ecb5102070651f6a2358742094a0c83d5ba67e1e181107e',0,'2021-06-29 15:35:08'),('8d3560b6ac18dd58ab1dc6703bd2bbadd6ffb0f5a1d775598c567ef42ae16e91808ae694c66644b0','297e2a5d947c3ed27411ce5340ac18c38a4b3b7f8267bf8890aaa075d7f7f22616c292b4d9639569',0,'2021-09-21 04:53:32'),('8db501fb63f5bdc5d225bf6bbc0676c8ab32706bd8f88bbfa303846c405428beec96250e972d34ad','983b626f81f1c2bdaad78032ae3746411cea6c0d69d3856df297977bae58f354cfa51c0c985fdacc',0,'2022-01-07 15:07:43'),('8dd69f9fd228a72a613f6823c4a4fd9985bff23377c7a0a13fb833829ef0b6d37b132a729eeb49c9','a2f975eb2c4b47c6142e827cb7469cf6e53ebd67ad4493202b6cf2ba1861d5e0b3aa110471ff3fc8',0,'2021-10-16 04:25:51'),('8e571c71b5fdddb2bc57437479b1e14a7aec22208013171a22bbceb2aef60d749be9b17d1c1fc675','1a82d6353f00fb79d4033236716612d3569a05f8f0acc530c1816038a2c2908b82938a23a866336d',0,'2021-07-17 08:48:09'),('8e91695136b7c267c1f83a8e3b95b06ec26c0006f9481c6612b740dfd5b44d0642b974128a583621','aa9d5c8a8bfd449e33a40d72021487b7f1e9453aabcf2ce57daa4d8f3c09081f4b4fbf5977a8ee4e',0,'2021-06-29 08:23:13'),('8f60f65d562344f0bdb55d7f8337ebdfae4b6d1f4806a99212c60419e5fa08b7462d496c9e757b7c','fba38df577bad83c5efb439d3f1763b33aa0d405548cb6139c43137276bf42f243d860fc35cac044',0,'2021-09-09 04:10:11'),('8f8d9a4963cf98911c664ca7f78e8baa347de8b63bda16f0758764057662271764f73a83f72a1139','a5ddd1553d1329e24ca8dfc41ef57a4f7cc5ca09318bc934fb5944eb5c6ff9138042f882b8d02392',0,'2021-12-29 05:42:49'),('8fab8e6602ab72974f011c1d52b2dd71df2ccaaa23d832b541f26680c3d42ffff984b8553b92b4fb','523aafba211211068549ce25dec9b70733a1955536c4503afb846f849f870efacfd80b4c67284063',0,'2021-06-29 07:52:18'),('8fdfdfd81dd51e9c8aea0515739859fd21c5745823e6cd7d1fa515dc5f9c805cd48112c15ab043f3','86fd5e0b90a072bf7ee904ec68d75c297fcadd62eef794895cce45aa5af9ebb71c916d53bda6d552',0,'2021-12-07 10:27:48'),('907aaa314c2a84127d3a61e3d4f7e573f6a241dc790bdc77e10941f5c2e1de1075fcc657cfa6e94b','c87d2e361c07d25c75bcdf14f6f27f1915ef6434dab26014a8414ea0c76523042699ac0579dbdf19',0,'2021-12-30 17:05:51'),('90b31874e7cdf7b3fc3a83a454c1b127aad629f01584ee7a3a66d7e9c8cd7a9789a6fd37957aec54','bd7d52de2dd9d5de4f5e7f42a03819f95e66746e791cffd124e1958b5158624a03dcc1acbc3f19bf',0,'2021-08-04 19:14:51'),('9179099f173a0e9e9359ea71edf02981e45910235c2e5b7a1a8b1ca0461f66461037ad1823009c2a','564fa0879686af4f3fbb34484d09400927f9efc2f1103abacf4265562d7eda86e2ba8edba8d211fe',0,'2021-12-07 06:22:23'),('91a0004eb97343323f902e76f821e947add58d0319e8e5be0d70cbfe947a06cf128976f89a702f3e','8a2f2c7912934fc7f5dde837b3fac7af520168865527d634cd630d5ad2c2cf76950008eea152a7a6',0,'2021-12-15 05:50:15'),('91a503417cef6dcaf48dea0256272f3f9a071952b36b0747c2ee3cbb54acc29132c02484324e2fb4','14ead0518dc5d72df23a8ab088c558bf70b95fe4503fd08b57d9ca202e2ea4a5f791b254151d6da2',0,'2021-07-01 03:50:13'),('9218365f94324f71820e939b8274cbad124792e73cd3c82ff7250bf2de35ad9597745d09a0c6d5ec','f2dc6558332fba1e92d651accaab8ee69ac2190678d24ccce9dc51ede0928272e0c92baeebd1136b',0,'2021-08-01 20:17:36'),('9287068949ceaf4526731485daec03b26910e7171696729d877e8e45fb538a4cad4ed0e41b1402b7','d339e9ac341b6945ff55380a6e207b9fee14cb20c567ca08074e99061c440f05cfd076cffacfbee1',0,'2021-08-19 08:11:45'),('9289736dcdfa44ebd99eb82d41205cfbd52eaae132f26e0b29382be56a55d6dae1c0241e5a3656a0','1118f909da9502e955fe2bfbb09990c38bfb57fc46431e6180680274e22bfc05cf097448f82b4106',0,'2021-10-13 04:25:01'),('9380dae4e7f7466fae9eb5ade9aa23350a449b77a39a2474c9f3241d368366709c24a880b33479c2','5dda676016ebba079a2c79b42bd115abb4417a34fdb59acb3a0582932a0afd44b72d3f764de3b5f0',0,'2021-06-10 16:00:59'),('93a3bf770888acd49172baa498f0ef411b351fb77dfa79e094fd6983d308586559fcbb814149c3c3','95b8bdac54ab69194bc86b9d775a18b056e11ef7d4f6e604c415404d6b712a2ce48b7275f2ab89eb',0,'2021-12-21 16:47:53'),('93bd50e94cff19b131b4b4816c44d8dbbd0510f9f2f3b3dbe5e1d75f14be7a6185942984896067ad','2c27634d4c8dc6eaf9f1ebbda61c4cccc8561088c3116fa1d2caa9f38607ec85f6b83f9b192f6139',0,'2021-07-28 07:33:04'),('940342a1f20ee9ed21f48d92a19e51a144ae428c8b747e94b964ed44bc2953f4864ebce3a6f19419','23517c3f05f100e5f2f5787ca869541e72afa53362d4dfd75bec1ea00dcefc96129006a711511e24',0,'2021-08-01 09:51:05'),('94f9cabb7474664a850806218e001a0783042b7b8e6c9f99fae25ac89db660979c25c68544e5c0e8','698ad082cc72d1d0b6d20acee915462edbab54771feb3426bf1f7b5e12fe8dc0e2c44b816081e4b1',0,'2021-06-29 07:20:45'),('959a3f0e70c7e2157c9bd87157c2ddb92e36b369a2c009982823b73c0ec11120a1e894df6cf1b9be','41029d380e1a9b3fd28e257c57eb70068e051555f0a987538885d8920846c606822feebfa383af11',0,'2021-12-06 12:17:43'),('95a506bcd8319647014c430247031b8aac712f244aee4ef75c5260faa5c42e12d5e111629561f879','a0ac160763063dcb9e2fece467ee5f688aa0bc4ce3ea9428e819829fb3c06645fd7ba5d46a6f0055',0,'2021-12-15 06:59:00'),('95ea92485c107b56cb0128ec1ab9aae82095fb72411491671c222e1ff7bc0eb1e1f0c52d638bcefc','315fe1a2bdeb208ca37b549f1c7a6fbf39ba7d5439e1f8373cb408f072c9d1f14fe2719f3b69861b',0,'2021-06-10 11:06:35'),('96347a82e186bb25f3f1700621581efe561b8ef5b1e5d3b52b542f2f6cc5295ecb838debb9d15411','903c941ff7e5adbac3b5e14bcdecea0f8246e69243c4876f31bb74f5b622d9d32eef2ec4f2f4a7e3',0,'2021-06-29 07:45:07'),('964d131b68ed22213ebcb406b46d3ed2da670e769c7f9bc1aa8adf94282d8c41964af424632aa53b','560a251d95aa5d7ac6d9f1fbad11b51f686cd7e887b7ba7ccf95ad7c8458c61ce1111404aef611da',0,'2021-09-18 06:11:21'),('967c27c3f25c0469ad0c52984e1c4fa3c3cbdaac499eb9d96470e6e610ee2715f9affc231e02284f','8753c06f26156b499775a56e4c482e40ff7877749c260842ae3bbd0be56365f34bfaad801c6bafcd',0,'2021-06-19 17:29:48'),('967c53707936ace5e739fccadab9519db4647d4b690927f2ee95037a7518942a7fb449eefcd1903d','fcaf5af49444ec2e7bb3c3e9a544ccd12c45a08466040995130c12dc7840e39656f328ca01772226',0,'2021-12-06 10:33:22'),('97090b050a0efa73796feaa3d48340514ef7b5e51837b4b5ea22883da54fddf6a5d446975759634c','6b37d00fef0110f8c2f33d29a32aae51b402cb58837289a714df0707e6472db03dfb01b695c1e075',0,'2021-06-19 17:16:21'),('9781cf15498825e638d11310da29141bee4c4af9e965ee0f3537c970455bebfc79e83921017c3ba6','39c27e4cf88cfac3dd5e121ade179ee2fb2aec6e746e6950e99f4c72a974b8abdc87ea3cb35c994c',0,'2021-08-26 07:03:07'),('97a279d4bb687ec98cc0fc7d27e9b3802917548a2dc7a1a3c6279f81412b96fc9e12b766fafa2f24','da0cc3bb40e64287ddf5d917ae2887b0fad0d8b98657cccdb30258cb5a5d46e36cacd2e85b225546',0,'2021-06-29 14:31:45'),('97b995ed68c188295544568a5a7d425bfe079ba29bb79920fdde7914031f4b6bc048aaea1d5e79ae','e592beff8bccbeb2c107cf22e3714eec2bf7c2b4b1d5aad0e58b1de89f2a73c50c29b4589392e1ed',0,'2021-12-30 08:50:13'),('97bd978f0ea33c45ce6abacc01c3d527998a2315ac8ef66a26f4c434be23e147f4dfe2e462484e2b','122f8ab1ec56534d48268a4dfb73638833a17ac5bd8af6a1db46a9ceef8e18acb3a845c00e1b702a',0,'2021-08-18 06:58:56'),('98184dc955fe053e7a3269ac5617a1b0abf6068ad57d8b9363ace3bba33a89351885bfc752105ca7','a1a73396c00b3f9e77ed4b4834ef3d23be15c05a94d260f1f62cacdeeb8767ca61221a0af62b03a2',0,'2021-12-17 06:30:15'),('98351c67ed6fc9030dcd0fa4512c6b4724b832a05459b7e468f1193abb46491cf2717a96615c43bd','856cf893a10a2ea3aa5d8c711ae355e32aae2d9e23dc640a68ea8e51a21784393c0762a6fd3c28ed',0,'2021-08-04 22:03:34'),('98ca2f6f553576b53949d364d72e2db7bc64c1f217b7c93c07c8a098db3566a2db0e187763561c0a','2a0bcf7b2a6e9247181db7c6f8b61eff15d02d2db88dbf6666f855cca1d6144ef6056f625bd42f5f',0,'2021-09-16 08:20:34'),('98d307e8cf9c3e1a9a5fdcce3cfff191db30dcbf490839714f9205b08cf575a598f2c19633fed334','4e684498dc2cfb02213047c345953acb15bb2a09197149c50574958dfa821e3924d15a8b567c2852',0,'2022-01-09 08:16:06'),('98fa38cb82ec6f10f46e68e93f5fc15326c12ff907c8ea6335d61ed4f0e13229615c4dd0bc02eab3','fb6bcd1efe74c8f797942e83889738fe4982d2477e1d09c91e6ffdfe57fb34bb8300c5931a419b22',0,'2021-09-18 02:41:55'),('9985b918dbc56c86ce5d579a2dd3b0e5d4bf48bd32bcdc60f4269d032b6db575da8bff2a874bfef2','688ee83c969cd26a0c6d1de255e5145d1228134cdca53e59e406a918facead962573fc425df06f70',0,'2021-06-29 10:09:20'),('99a69b109b961fd19947cdec774d3d6f12269f39f916a2ed1a41272f7c515acfc11565cda500b98b','d6a1a8af3173bb8d2339b4b906cb749520eb5c027ef4c5ae7ecd829c107d3eb5138d299200484281',0,'2021-07-01 06:00:23'),('9a32775bf62c7f596145c3a9fa9af2f20762b029cc7f54468cdb10ec13980daba5a3dc1dbab48762','8f8db84a16ce7b9d0d7dc61e424c34135862ae8c7745533522287450cb6950d1fbc2d380f0e726c3',0,'2021-12-08 09:11:00'),('9a440649825fc68c18142e977325129590434558ee7b52d190c9b963b3059f3b13abcbb6d041e866','5583f99b127d3aff9aeb162653ed84608d3282edbde4a43fc64db22e65a665965f23747aed1ab794',0,'2021-08-16 18:43:44'),('9a6191c6208e60767e26cb300b30caefbcaff2f29bb5c3e0569e7108ff396e851b115aa50ad709bb','bfd297045b53c545a49f36c0896d5c00528231ba6254e62003e85420866491cd8ea02fda4ab59edc',0,'2021-07-28 14:06:14'),('9a70cc3c38c45588b3240bb288ff7d54852c606db4cfd4e802088e469675f0b533cd79d22fa2311b','caa7b0b030714da5635e73c1d16626ddff47c8bc9623bb8f8fd19489e5f1c47a2947e6e603c7a167',0,'2021-06-29 07:35:25'),('9a98a369232a19ad345811a49a56a8cb7ccea1716064f3fc00840665f2ac44c39040d8c22b47bb05','81f781c0192b5cf93d26ecad54f0a562aef8d6dd858bc62692bc747cbb4e53a4c3843a165f9c8400',0,'2021-12-03 08:35:39'),('9ae61a792c731168c648dee81226cb624cefd61e64655b33b80d5080d3b7bf891b539f106dab4687','550bd284ca6674b13a097c581d072c513042f67cc1a9b687d65c925548a59960648b736bf1383d76',0,'2021-12-31 04:26:33'),('9b33474b1178525adcadafd32e264efe3b8cb762d39c6ae32994768ba6297230551e2a8c9cdc178a','2a76d1870b3a798a909cc06b0943af18e3b2b80a225f841dbf052a0a830077309de58ef5211059b0',0,'2021-06-29 10:01:36'),('9b3739e5cc37cdb9133891d91007f95c63e003d98aa2b0ad2296ded070ad5a094910110ee1a5b49b','849142b12dd0b6ab6df2dacf5954ff2ce7ca3acfeb251d3e05bea78ae739f9696fc7b8a806286111',0,'2021-06-06 12:18:07'),('9b9d71acc9139b913a05901d6d5edb2997213613c123237623d837d17a13b53731e5229b51365856','fdbe4d5eff83d42d6b99ead73ad6339ba17bc7305ed5c323247092dd1dc45a3388f8dfb36a69f300',0,'2021-06-29 10:16:43'),('9bcfe96bbdfd76f7a48fa11321dae2205279a5d0fcdcf364f572c4b036fab09a8e680bbffaa179b6','f29119e577e8395f96503f67067537a3d68f3915d2cb2e0730e89b8575dcbe8f273b09b7e098c7f2',0,'2021-06-29 07:35:16'),('9bf99f8e2a21d77364b8874b40da104e0c780e3feb29c1426a63d161dde3ef84b838e29239e6b156','41b26c547eab1c0b1452a0e7695a9430bb26282e390301a3843f96c8690983ca22e96c2f90ba74be',0,'2021-07-02 03:42:51'),('9c210d349cb5b9a8ecfebba73eae8a714eac0ae54792fa62478d46517f9540cebf42e0d080a6e675','2308503b7411dbad40207a1d75dbf47a413036e1cdf65fc358846df886e1cdcdc27121f4fac6a7db',0,'2021-12-20 09:09:54'),('9c38c6af8ad5245dacbf419ae2b1ca191050480f17f57b0179804697bafa8faaf96550620924275c','34fbacc9104cfbd644c84804b0b9a4c7ac26a7ef029da0a0b03555a305a9407b3821f03f25097a85',0,'2021-07-04 14:14:45'),('9c39c08ff1b873fc3bf7eeed57ce87b79d8c284687fb8d83bae0f91f31cfcc70d7b7d698fcf8dcd4','a250c17c3be029dc411ac82a2ee863a893c3f9d8b52776f2b3d4b4d95c19139583307d04dac2b2fa',0,'2021-12-03 07:00:20'),('9c840cdc85b47850a0a68baa4d31269c6ad797ed0a07f00883666e53d337d09525d2fbb891a6c774','ee7f11e19d80826b6eb523fe90159ab6633fe2542cd793a76ef17d5be3785c4dbf73bc08e18bb20b',0,'2021-12-21 16:53:14'),('9ccffe2bad3cff29250322b82368a8cd61ca7f63ce15c708d2cb5a7c36822dc8256b54a556c0bff1','e542aa6dbeb8475076c8f4b84d202eed40936118fb7456c2fee4714adacf5820208a85b1b0a63954',0,'2021-07-22 12:25:14'),('9cfe074046c1cfc6ce5755fd8333dd4562a5209a0bd832115808922842010d88e942563d99de7945','0a298590a8c2b277f8d872e8500bddeca4ad4c3fccb58a3995e81997b12ff28538979a44912b14d1',0,'2021-08-04 08:09:13'),('9d97417c09088e80ffb6c8261c632ffe15f0b1656e1eebd23492beb893c04e419616e6036734cb51','d4c39150001a823e40899cc067fc60b79ff5c76e4eae60085554b4e2f4136d8949801c6e18530d98',0,'2021-06-11 06:43:25'),('9da0da62ff065a6b507ce7bd7b9ffd4e4297538ea0656fc24668e2bd352d835ced487a8e926c1069','1d4dad3b0f7d2cba780d695a0b2fe5fcb45aaf2ed4368ca8810c3a161e333dcb1d70f6c3a8cb396b',0,'2022-01-18 15:41:23'),('9dcc2bd281d46e23dd5b527c0547932c549c8c85a89cf3159a984b0815233fa8d0bd2ddd2b7387fa','a5ebd87d7cda2e4c58c91ac6a852a0429c155daf271ffda37d4f5018c18ae70415c31371d83a7d73',0,'2021-08-04 13:30:23'),('9e177e44a506f5310166d7efe808810a666f49b4f7f41cee5a67d4d000296590e971179cc38498af','999feb61a8f6fe232350672d42d026ecbef38aa1a215d4d3d878fd22b4464e032ead01568cf248c7',0,'2021-06-29 08:35:50'),('9e2b1f1258dd5776249e4e14a28750536fec8f72eafb11f508b7dc432fe458a00b6ce7eab9ff2799','9542955bb0c7209bd7bac46b0e8fdece4f2d8f40319beb29cfc633543de3287fa7fab9fe84c14d1d',0,'2021-08-18 15:25:09'),('9e4d6fbf0603668361cc560887eb33181f8bbc49448b177207dc69ca9ca0302db320f79eacb2eebb','47a931e7a90e9212dc6f4bc82868ebc306883457aec5b36015931fea1d5218b8fa0e67d2888e3783',0,'2021-07-04 13:47:38'),('9f5a096e14dfd0ed4f04ae16a69c47ea21d73c89a7c883657a33b5d9b068c3cde86239501c98a151','4dffac9045af11bc21b9e1be2f1128fe15a67a188cf580eb50a9bdf9b8b49f9053cd27f3b3bbe512',0,'2021-12-21 17:05:45'),('9ff9d5a3a526c24aa424fbdc84e8c68b62d621e7c889a02ecab0f589a2e2669095930835054192fe','6108d795c636da58a4e1b21bb3459f14128fc5ff05b3b8a00231089929f807de60733008b9b1c1a7',0,'2021-12-21 16:48:38'),('a01d1e54acd1df110d45e9ae44c766b0f451be4709ace1b5d79ab039e43f512ec756c7f1abd7c8d3','4697ff02e460e5a9e2f8dd79e300ddf2b87b1b12fe27bf8395d7e2013b31fd405213dbdbfa305182',0,'2021-11-14 09:33:23'),('a021787c4729ecefcbf72e9ae87245ccfbdd460969632dec3c06e9784fe52a57341e11d004b7914a','02f465e4ca6182aea376da91b286e29052eb83f0718ac13bec9a7a328e420b08eac43525c0c7285c',0,'2022-01-07 13:42:58'),('a056bc791ea6b182626069069211d99b105e1d2a82465529a2c35bb78f1b2762b36292a06346f47e','1c09c1ba5e3d202d4bdd83d7b4829467b85452fcf9c4476b1e6c021813ce537519bfc55d2730743c',0,'2021-12-07 09:26:11'),('a09668da5bdaed1aa936fb41ad76b03072811702df3a51517ade8e11fb3e7d02ed0c3f376fbfd665','bb294bfbf0894a42cec054189094dd57d24bc4e210f16a2be49677019606572d2c1c0fe678e85b47',0,'2021-12-07 05:56:20'),('a0dc1ea893c535e6dbd9bc9e58315ed2def0721b271e62cd8d0d6b683c13b677ad194277274971f1','82727e8c90cfaeda9ad061ef7fac2a32a240a1e269b5c9d81bcb3dd9c9ff0465ef6296a70ec0e528',0,'2021-12-03 07:26:19'),('a0ea85503e2e1595aec9668c1abce0844da0a99fb1042994f3a36510df8b9921fe905044cbdb5cc4','ac0e4f407c8f12719286401f84fde2afe65712c008b216f006f56e4f065c903f8d0ad26ec8d28878',0,'2021-12-30 04:21:31'),('a0f9d89ad6d4db94f60fe087313a427b03cefaa56c0cbe8875b28c2f203ec9bc7ae5df16ac8e4d40','e25364c189a53dc08e482a8cc0ffc274a4fb81d95038b8f97e4307e7c398fcdf61bd6a0de8e7793a',0,'2021-12-30 17:02:58'),('a113de362debaa03bf88f7006c2b18bf8690252a2b2eb35423db1b8f478660265b64b4186781dc04','e2e162a52f76d53605b18eccfff9c9336ce2c585b523531fb119eae0493f4d335a5a7566db611751',0,'2021-10-05 09:58:18'),('a120a36e488f04c4afad0fb3916ad985ec98ef4a792695df56bafbd73c35177151a175c67129bd19','5254526d7596282265c0a5c93664126b4b0efbf24a1911b25287d41f947545747601c1bb2d703df5',0,'2021-09-09 00:46:58'),('a285365d30d142b9948dcec44f452dddf2843da18dd756068c2b78199fd62e6b072e154678990cf7','1e845314dcf8294c80c7ec5a5ac1b78584c8c9eb9e9aa04e86b86796e64fa282b2697743e55462c7',0,'2021-12-20 09:01:10'),('a290fcd159e70c1d4759a7028b6c55fcb256a561224c2d507d6c843f227d0734c3823238a7de9296','b124629622aed07e52fdd488547834084a785aa8a8431c5b1e60366cb120b68e54002090de1dc6fb',0,'2021-06-19 17:16:01'),('a32f052dee81e1e222fb48f3495d45d9a3ba1566807d6ca53ac18dbb8d5d049c3d5904c124573887','869073e71c51744591c6f13fdf54d6331741c31ce744e95bcdcb984aab097a46fff2bb678f965e4a',0,'2021-07-04 14:13:18'),('a372f4816fd0d6e3bb0addfd9052aeb855e19a38316ea54c0d393be28404c2c1b66db593da24e96f','f0694e53b259aae02c51565b17c1208b04ab5e7647e40d91e4176639d805560029e699e04147ffd1',0,'2021-12-03 08:57:28'),('a37dc41f8a540ddd5e20d19f1f33eaa68be9c5a1ae81fe84c43bcf3ff46032e703dac0a91569a2a3','67012d21cb56ca2930f258398bf0a5122b447e27c9c909e349f4b9c02e10350bb43f4a71f19ac610',0,'2021-12-08 09:30:29'),('a386c04da0e5cf7ff1d721c30a0ed1c96f1e9de43df372c54b52fa7494ad54c4ae7819662bd98234','58475fbcd87845e9ff11773b3a3198127c0677f54a6a9ad4eed1cd2215495d21611929d3bfd2a345',0,'2021-12-24 03:39:40'),('a38b56f7af0320f74462a92cd5c12e34e3bd91a56225ea2782fedef653d63c973e1c967575aeb2ad','e06a1971ebb0d2bdb5b34aaba08869739bc928a30bf257365b627733798695f7fd38626947b70f21',0,'2021-06-29 08:29:48'),('a3afe3d11f0cabada0f5afcef4dee537b03b9098b614580576f58a1ae7c112f5a778222278e00faf','aba994565c1e67b59646fbff3d4c6d2a77f6a9eb53336cd022bc85c1e03d00d231b37c96a9d29c40',0,'2021-12-18 08:33:49'),('a3bb8234f95f93783b718e177bbd6a550ff510113357fbd6891f844717a76019cca5cce3c4dc8552','1df1dbff78fb8b4938607354e3c2e0dbb61cad4e1723a7c42dd392da47067c550b44820926ab341a',0,'2021-12-03 07:00:31'),('a42eb1e4e8f85e5df60cd426f727b5acbb5176d5eba0e3a0fd577dc8179492d5f814058669698149','99e5d00559ecab6dee104753e87fd18a3015b087ad90f4b01f84d2412e7ece0b671d08f0c7db4565',0,'2021-07-28 07:10:21'),('a4429b533a505d4ccb68fd80ca0f58480829d5dadc4f3b9a0ed9572bfc8f8e933af49cdd6a450bc1','ba5c8226f3efaa99ad976b4988df77191b515f22e225c79e20c843d0b31bf367cff55b96f7b934ab',0,'2021-06-29 12:30:33'),('a45bb37038b72e0812021d173344c391872861093bb0c37a55baad500a04caae13a36cb0dc23dd81','95f7c47bd641abb1c548e4b523a1d5eb85affb611fae9dd8f0b383b7cf85178491082fdcd86225a0',0,'2021-07-03 09:30:12'),('a45e2f27bc18926d0786813d6419a0de45b57f0bb30b2a04bba04d86f01fdc54b0a799a84424a1f9','6e23003c484cf4353e696cc6c4b9bfe53646bf943a52430377ff445fea5f8affe0e1edfa19b1084a',0,'2021-09-16 03:39:54'),('a4686431e5756be0b25987ec4a945ccf24ed8b85ef22728e8896bc977be42404c4774a009f114060','0363e0f665c2cba31da7e38f21773ef46d1b2a894211202b5875a064fa5f22198e9a114913f2be3c',0,'2021-06-19 17:26:51'),('a51b661530c8c4a0442f88d27c05c577daabf1b0e8632f05a07c68e8f6d54bbcf071acb7a2cc5892','cc24d6d272d4dbb7dcf8861a96e8c4269dd56ce230d74b272fe05013e799ea3c866dfa1864667592',0,'2021-06-29 14:15:39'),('a5aeb5c43373c0052586107f5fe693226a2f65d724beda2a9171d0c706496b76bbb6849a24ae7413','5b19066dfc5d2800a7d943a0e684c1742d8d20140f8740477882a129c03012a4c4510724f6d1f52c',0,'2021-12-16 06:18:50'),('a5f020af2632f28231089f738dbd30a5bf2ec001836d6a15410e227d3ebb6a66c44d63cb3c8b948d','ec75166fd5b7f8589faf98e6bc461d137be9558a1371a5944312157abad6aadf371d0c32415bbaf8',0,'2021-12-19 16:12:52'),('a6d9d60320a1cbcbe4cb923d2b20fb450fa2aa333ed95f3a346ebec636a4c6204bdfe90c5477893b','2a25ca14cd58d49293de75f8bcc1c076f3abe04a1f69b457acfffdce76b22708ef63b232a8048ea9',0,'2021-08-24 03:10:16'),('a70083e91dbd858ac8fe5ed477ab44fd9494dc79800e1e38dbed01399aa113b023038cc8e7359906','c178137dc84ef94d6c2139ddbdb518690c04ca5fe8a396b31a8ae947cd3c94b77b5519279b09a1f8',0,'2021-12-07 05:27:40'),('a72bc4dace396833cb76941052766492c1171a6e712b8e31d648a75bcd10dd4865f594df5c009060','355c12904ac77f19b5360f1cc8d4d5ef8940d8b57d337ebd39f4d39aacf15d9c3eaa076f6f1517e2',0,'2021-12-06 12:45:47'),('a72c1d43a09c3a1126db48749ea56fe84700e96a9fbfbdfdcdfd7f8a72d6b14c7864a36df57eb1ac','f48c0b9e2bef2ead5b32d665a16379517f90b0e3b572ec4aa7a0dd214817b88eea15d03c5133eaaa',0,'2021-07-01 06:19:02'),('a7792b7d426c30624277368c4df39c69005e34c9611695cd7b7d8cd72ba4671ddcf439bebe719ee4','ef5235ff60f01b2cccfbebff5411603d9996324e9bb8438b0fc5a2d0e28604fda8e22c53cb6a33cf',0,'2021-12-07 07:22:13'),('a7afe0ee3352e200419430f0f041694b9bae8ce64144a3676421e1ccef11e4c8d4a767d007cf737a','ae5d6f8b1183f1f1e5b4ccaa10f11c460f88bb44b6600183a0637f691f3a1ceb8cff9ca6589a319c',0,'2021-07-01 06:26:17'),('a7deabb86cfdc5350ba464e95f1a0e43caa5e4d5aacb7903bfed7b7d2b1fcceb22ad150886669eec','e3561e81a8a843a24e30674b07085b6eeecd560210e80ff0711f81ea2f604f733cab503eec146c13',0,'2021-11-11 05:24:09'),('a8626bd5f020682da4987bbfebca4e50839e73b72b552815f154d2331f8eac63003dd72962ab4f94','24f6628e512b2b297725beddfe08d790cdc401dc1a05de21ebe04c64d0ef40b75d54cff0af14a384',0,'2021-12-15 06:17:26'),('a8862bd31af4c0878ac086645b7871e5244f0299604bc674db417891fa6108baa79141f2a0256c1d','574bdcbc872f8851853fc432a0635cf9d036a79ac6a6c58aa1abf9f0afc8c68639d8792cd46aa69f',0,'2021-07-05 02:07:57'),('a8dcaca93a3050c23ff147cf67a050d50e2995dc75d5e8660534aca5c61d80b39db08076e4046699','671437f05db80637489b27dafd97db24468c1e130cb08821d480adc551d1f70a4761e4fb8b6ab275',0,'2021-12-07 06:03:17'),('a9207c5158f3fdaa151366a00d731f15a7693512c1ef90588909669f3d84e0db8cc5c06531d0ef8c','fe1f70ebe790677c7e5cf7cb978c84546fe6c1de3b019c78c2c4c7c5bfef3219301b32ff0cdd2b4e',0,'2021-07-04 14:23:02'),('a94720f73b721ae4c9356b83704c4e17aacb40e10a650eeef8fa7ad94adf4c58ececad472e3fff12','b713f2beb9a35090a707051341f24ac3ab07cec6d1e308cbc8407ab26baf537e1627bf0d1524e778',0,'2021-09-28 06:11:35'),('a973e6634364bf80dd3a3db9bacc6e80dce7fe4f01c96642947536c729bac6dd33c7e8b3d1e52765','17ea4a7f40325570a632a90f60f159bdf812d32195b5acd993637ea6102e7f2bfa2acd630cfe354f',0,'2021-08-04 06:47:07'),('a9a6b2cf5ad9386f9fbe516916eaf165b312ce034e4b22783ce216471b2896ca12e0a7cf467e6e31','5e3bad37097d611530184e00ba2552a9f4deefe9fb9ed36157227a76f972581addc7e0abc0b4ef04',0,'2021-11-14 09:42:01'),('a9ea265c65203652b85cd497827934889780b00ecd3fe6248cb6cbd96c465164a67d8191074c569a','b1907fbbef2b389b0e270177a99dc1abd42f6e1c8c9f45e726a3ad847499b4080598305700d730da',0,'2021-12-03 08:15:44'),('a9fdbe4bcb634d4413e974378101bbf5647434e6ebe72d44209bec7a634c351925b617329464af5b','11aca81e1999e47df546d26aa0a534c195f86da843ab6f08694c7c66d3c41c00bc568f7392784eb0',0,'2021-08-02 15:06:17'),('aa3a8fa390d3a95c998658ff50486062941577bd368efd11aa741c1c1155a189f692f929dfdcf219','8f88225ac02929210673a543d14fe08aeea41160aebf38587d3f531df4906ea6b4275142605f486a',0,'2021-08-18 15:19:44'),('aa5f2c1f5a8636ad1d17ff81c0a1b272c48c454820ffaeb970a56b9a5ad972d2dccc8e958a31348b','cad940b192e66113dc32a8c9c2548a130b1c2c98b3a9326bb2319bdef1373565f77cf1a3de742143',0,'2021-12-08 09:32:22'),('aae19218bd9b06b532106d832c866072898c5f8e27c6602dd54e53bb20e18ce984d2224a1603f671','049a1664e24d8f1fced89ceeb2939ebc1a017ca0c37660c3161394da144820738fed7f318e77706d',0,'2021-07-01 06:32:51'),('aaebabf8aeb22276a41760c9fe5208bcd71535872aee296f5e798140b7c9a5b3e16d4fb93297265f','3a057447e894958166fad212134e585c72d36897a4d9b8a7122cb0a7e5af42b2dc9e9ba023357c09',0,'2021-06-29 09:17:12'),('ab39575d0d4ec6ea8484c8b7d204be9daffcb899b6318391a302d8f51b4048726c1f41b97271bc7a','077a577e54e175c3f093b3bef79bf8308c895a5b9c8342b66dde1e26a15ce39918e9dda54d0daabb',0,'2021-08-18 16:06:42'),('abd50c52255159980efddefa841a4f84be2d89f989debc3f093e58879afc22a9a63659607401faa3','6c4076df222fd39b8882ce37cca5720a032f1d44b2a1b6698af9622b7cbd4c8e81307165a13d7ff4',0,'2021-08-01 21:35:25'),('ac6e85c6cce807330dff32d6a8b30239fee6316a0a91352363ee3d40ec6d12cc93a58716b066b255','c4ff2f287c8c1c7489435f4575b02c9afb6c6a4d47866f1a5744f95bf0436d8ae1ccc27346d97373',0,'2021-08-19 08:05:39'),('ac7fed991b5b5ed8b3305f7006b6ff6c951c406ce94dd129bd3c76faee64d4a8298edde29ae58e2e','9cc11d4d6f47f34a7f4bec9a12ec8636e73de329c288385114296a26029bdb242231463abbe4ebe4',0,'2021-12-03 07:56:10'),('ac8418923acb0cd32dd872c03142ad5f8465221271c6c407fa0dd8d9718deed8517b1f3e3ba0e16d','53215da30e15be7a45077d19d0a387d1a37f3e7da5d1a45b4b58bc35b6d91279f1144877621b849c',0,'2021-12-21 16:51:36'),('acafb1d04694f98dea403fdd479a5a2cea0a391052613fbcf1db255c683eb2144c38c4ac46f84fca','4eb8db9dfc44375037c8614c4d7d763e532fc2173183b4ddad6fb11adb2f0791223442766f33feb6',0,'2021-12-21 17:05:52'),('acfbcc66091876ff2698ae1cc819c4893f69f820f3d618943037d36bb52d8380a3695464bfbfac78','9f54af0f3eb710d93adae9b70457171eb3694141d5a9ef9e5e6ecd16b37d3e1202509f3e87e18cd9',0,'2021-06-29 14:47:58'),('ad746f2c6d9218782c33747129edcf499af420351278eaad7b7477059df72692fbd29872bd91a5dd','afbb8517edde3e333b27e61a4a261d336216ca5f5459b722aff34cf196bf8122027e0e10b8a73277',0,'2021-07-01 03:02:35'),('ae929e5bc58ad49a26f97b016d76772b7f487856b8bd9fe6aec317ca2d3570e05d9505533ab75431','bc4ee9bc5d99882a9a4bb1dc6b92897202e55cfa07d777270c4750f9e6b58ddfb9001d5e920526ff',0,'2021-08-04 13:54:16'),('ae9516d236b456dc6bd08a1101099604c7c59af7f8d1ee155cad9bde8de72c4fa185aede4fbc93a8','679141afa665ca1a5a084080c59128b31d964f93cbf895c50f592a33f585d3060643a01c6c35177e',0,'2021-07-03 09:30:15'),('aedbdbc8de580c747776a07d0d7dcab9ebd3b28a464570205dcae8305b6c77bd09681ad5baa54160','4c66bc3ef39dd56594797e15a6f32b948b964f6879bbcfb323dd04585137c0cbfd122ab4aa3f6762',0,'2021-07-05 02:21:41'),('aeebfbd4422506fa976b1ba506db13bd0f38addf68bf358696921a7b2ba4fd8eab0f737220d57624','a21b5badfba16973a27f5c21224df65c5a0b401cc62b780014b16d32d6302043c14c13e875a51e78',0,'2021-12-03 06:40:23'),('af395c7caf201eae35f4e8a9aab353b63baa6d3365430bdd13e1adb45929b5b42f88d52129a6e2a8','527e729f95bff1f43d69f43deacb7b6519aebb6a68b464376fa90c3f70ad560addbf64884ab50ce1',0,'2021-08-18 15:45:37'),('afe61210e7cbb74a54450319fe6cc3b8604962e9d4066d349497f1b73f359ea1537f7418d68bef90','73ffe28d94ea03a74df9be6fc0ed12e410de2de9ff54cf7b72040550bfd8fa7401677a95e36c86f2',0,'2021-07-06 14:06:19'),('b04159468e1744b762aa0076a6989cdbdc206499402965bdf1fc9cb54240e5cbd0434eebad86b296','fb78675abd3c84d1ae8e96c1b89bd412d573f434e738ac88b2d72a3a7fafd8880e3e27dd0bc9c4d1',0,'2021-06-29 15:24:58'),('b0a0d5793ceeb6e578a3d979ab87f82634285fa42098496205d901a992082d3b89da7f13c58eb7f8','cd626e69f0b475bf0c748ee787303d83fd3731259a34e5229bc403177b5294286c0a49db46646a35',0,'2021-12-03 06:40:49'),('b0c33f80561bd3dd6a98f6802585af628691b6f9bd12c81aa6c6c70f8ae757695260f1f48eb6cee7','1b08f8faf424dd03ba3222ab14a076aea9ec508c7f3db6dfda44a3b1c00af4019082bcbef9742b38',0,'2021-07-01 06:28:13'),('b0d38b71656eab8a5a5dc5ff42c4ce9cf433d3e1057a7353dca664ce325158fffbad8c74d6f8342b','02dd26a5f217fa87aa090a284fb1763485430467bf0d0a3a9c204318eed5e671bdcb913b5ec84704',0,'2022-01-22 07:20:20'),('b0eec6d775f9cbbbb8ae192fbba3ba4e4c343afae0e3c99e86ff812a175b08043c579bf50aaf566f','3bb7182dd8c9416c025b2d91a22be38eeba5d26cedeabbc41a3c66828ed2c9f577cc8b8497e7b003',0,'2021-12-17 06:29:57'),('b103e9fbd6703fcdecb7a3db20a85f0b533061d6e8e89ac9aeb810fa2438782d1397f83efde3283d','aad68de54b47ab9ad034ee1174293320d1fb5d2d3977b85b9f84f59af085361a16d104195d89300c',0,'2021-07-28 07:28:41'),('b10f1c9db9adf7ce5e95b297658b4c6bfeaf1a5db199b15d29ce93472c6d29e71468c591e1323c7a','dcd712cfb5d876a125f92e3be8fdf156bd825c729946edadb57e024518c131f5886aadf1f507c29b',0,'2021-06-29 10:03:33'),('b116c75dc2e694010cd33618c13210b35d25d19aebdf1863186f67db88c16d2da866bffc51a90566','51720d17b30626157e5163ecceedef970e4df9e9000176dff71c2a7927705c038dfbda71e3c0dc0a',0,'2021-07-17 08:49:44'),('b12d288a07744579155e4286da3d6b6db466f25581de7ff16d5f002cd0b2fc8675e8865f025fe19b','a1e4351e433584e42ad4fd6e41e012742aa1ec7015a733b0f6c7eddcc0c6b441d65746ee0a482418',0,'2021-12-21 16:53:33'),('b1521c9da61aa93e7f1c1f12cad345b9173c0d26fddfffdc3476424d068f2ed997f46bb337818896','0dbfe65b2761d8930747fd8a95be47d97fd7cdcda91284539f354436ca200c28b62b82c758df7a23',0,'2021-12-03 07:05:23'),('b212fdc3a5c7b08b6f82752ac90019fea81d072efb99928647e1b32b89b39f01c30cdfd15862b448','6aee6ba05e5450450241988189d199e5222f720a8cf8f8bb1a47914a1958400ca370b1e593e7e5e5',0,'2021-09-15 09:21:42'),('b298cc5860b21b4eb0fa1d5ce82b4ea058f1f9fa445393fce37ac243ddf733205355b4283a3bd645','389cac750151c8ae11a78fd00ddb37bfe9de303646bc699ec463bfcd91b98f79e0943dab674c313b',0,'2021-06-29 08:25:37'),('b3845bd11c37c6e08204d8566966ae4f86335ccf661a411ac9bce1f388f69aa13b13cb30341a265e','8c4a8b85181ff9520a2bf41b1ec6973a972d86ff984ff13c474b7928a673b483ceecc50d758edd2a',0,'2022-01-22 16:06:32'),('b3c5576c81ad138842076af21893f0ac93e7ccaf22060eecae695cc3d2e6a8f45e29440ac85b6f5c','28118ab5f450463aef11c7d4f2bc4ec4812fb9a60346afdf70c3a6445d3f80a974e594df7970dec7',0,'2021-12-19 15:21:12'),('b3e56370ece7a349fa98fe2e0c028d865f5d429f02b291eaf6d526ce7d2b566d77f901c05442d652','3760495e21a1f14d1615eb7fe37476e5ca5aad2636686c1e062dfef6ca9a55f62c155b086a2ce260',0,'2021-06-30 07:51:56'),('b41bbbff3d8f3acb4aa3782cc6c5d56d58a0ab4830ed05f246ef70d305075ef292e4fa36d53b61fa','a29bcc0f48aa308fbd99cd2467a169cc85a9aae96f9a3c78be9a0bcf2017820cad486315c520e8d6',0,'2021-06-29 13:21:52'),('b470743e52c7ebd6cb99c037c695e8e24ee86ce71bfa2bdc1c59f3b92db9b0c8131e3b4e0bea070e','edc42cfd4b3507def027f0cec041646f59b5cce103654544ec1fb8f378347b2f15ae5ef5cec76e93',0,'2021-06-29 08:48:23'),('b47a293170a8b0ec78c4073faa5202aefe5195d0505a2aa14870fea40b1a5dc09cc8e6d965be04fc','a0c9edc6e311c6d42394e85610917e5c4096267a6e7e026db434dbd1977048625a57d99bf03b0031',0,'2021-12-15 06:45:44'),('b4c3521978387b935f4ccc3f616c5ee7d023e6c35e0fbab02722b074e66d0dae05d3884d58a1d7a0','0ba0cf1e640be5a792cd33e66ff96e3a151eac2a62882348bd041e2c161237d56bec3d15e39bf883',0,'2021-08-18 15:16:28'),('b5aa7c8218d4c5ec1bb6917becaceededa3f5752aa530385bd54b3e6c6a26b2201cf2de9d6f0a7a0','47b99dab18c55a713df2b62fcd2192fcdb0c8ece92b36416368f67c3a9f0f07ba1b91df030a099e3',0,'2021-08-28 00:49:51'),('b5e2ac56326478e05b23405b6cc762dd6a118ff4e266beca30b9146800bd1f6bcd66da12b8ad5758','c30a5246bdf970b4974761aa7516479f76b6e636699df05b3ad9df3f207d36e798a12ba497bf8374',0,'2021-12-07 07:35:50'),('b61498f1bc4fc68d2f27b842b562e9ca6b05cd2a7ca244e3d199d8f5da93a0a159a7166df637b1f4','68ec2ca3a3cf562d53a9dc15c1232dadf5fcc8efe4304831eed39ae606991d8d56e2e61a84a91eaa',0,'2021-12-15 05:18:31'),('b61e27858622de371b3b7a6bfdf2b14c223413e14712ae7512dab39b52ffa6d351ef3a552e626918','6b2754f5999ddb4a6241f192b89a231c38e277a323001678898acb4052a08196d433b1a659b0a1ad',0,'2021-12-31 09:49:16'),('b62785ef8181e7a6250b6c8de309b08675be5f66d95244c48b444a405d0d22f6a2aecc647942f811','befd4603c6b5f2065dbd1cd84115c6c99e791c95f06f53240dd2d6ec6393719740e4708af643c682',0,'2021-12-07 07:37:33'),('b6e58188fb88b66ef8278229d56aef7ffd09379c563b596c86b5db35bab53552df6ca4314d633d90','8246c46bce7776179bff97c76a50a7172c9bbd7d05491eb18fd6ea5143e437bb15a4f3fe2b4f14a8',0,'2021-12-03 07:43:37'),('b7b7959b495b127d1f49ceac88a0b4409334e18dd503f0620c79733e2062f81076788e3d96071e51','0c3059fc3ac00c3a0c82a721efa364774ee667575a609fcd9fe3eae5044b86f0146775b2b49d2940',0,'2021-08-04 08:10:57'),('b7e9c01386ebc5964d46f03d50b8f5604b6322781f2d7584e05c92fd5596566b5afdded5e1807df4','9c584e5024676908bcb6ecc91c79c13a8084ba29e4b0c2d4050f1695c892ec13317486ec73f89823',0,'2021-08-16 21:25:03'),('b7ed4201dcd90bc79fa71b181f673c492209376eaeb0e1e0d47dee329a8f1e6357ae0b7e527d367b','538656f268ea522f712b5bd5035ef7316aaa6ca07fdf41cb1921bfb6856c241a34c327e2f7813f21',0,'2021-06-29 12:17:54'),('b80fbba80c7eeb41bf09c85d376e8b66cf7b96badcf02364384e1c72b0bbeca56f43a3afbb263567','c95d9c75edd17fb16989f2eb4c2ebd8b18f3c98582b6429414c2806e25e544f4c09a5c9ea18c5a30',0,'2021-12-07 09:54:51'),('b856f978df78bf07001b7f313cbe378dae0f11f8405fcafb6858ef49b5e13cca3b8de3c9381195a2','dd8a6ccf12c11b11d2f490b45c00576da25c43286b0181a9e6edf531c610689bac796826eacc0ed3',0,'2021-07-04 14:24:01'),('b91d902fa55a728f9ad8c0cfc430252c8bc457f85fc7697421c3975976842e3bb78da1f8b814aeb7','60fb9cc81411f9621bea462ad2b3cf72d25a5a318de39ea64557090ddc1b48f3f10962b7741bbc91',0,'2021-06-19 17:31:12'),('b9598c8dc6f3c2c609ebbcb72f6858da322e13ca4efff27fc6544d0472550eba31a7bde4c3a51e8b','d2735cfd9725be2a197661b7b0d938efa8abc7a4764888fe4beb3987a0f698a9f3ea6768ca55c5f5',0,'2021-12-21 16:49:03'),('b9600978c771c5fcf4384c92937b4126d9a2d0291c8441f9c6c502ecab4029045652aced5c7f18d0','c2efb82e41ef760ded5a9fd9310863021f345b7b9474993c17cac2d66218a414789741b49168b4d3',0,'2021-12-07 10:06:16'),('b979cf71f4c93d9476ed731e8474dac7beaa87336265029531f65f3120b95256f4350294720acf73','ca3d7371654c83315d28a0b728d5189986b1798cb94d369e85916a2f805838098fecbab435978b29',0,'2021-12-18 10:26:49'),('ba136b67ee3da5db7e8dc21225509110988f81255aaa74d9314697005a5159953bcb3bdb753011c0','7aa7a09f6a198093c3c0e55573d14e8e9e2ca67ff7639201335d7ad545b95ba2d3f5497106be6edd',0,'2021-08-03 12:13:12'),('ba5d98832381f39ae79b8935ba2244a77c4d9031c16d8ca21a27814cfdd96d762bdaacc51b9afc2d','93d163e32b0ee7a8846e4229f50fa131a97f1486cd77eded825a680257c03e89bd2fa585c08a307c',0,'2021-06-29 08:49:01'),('ba9a186707159f930c8d593b407e6663eb37279b04d2c2dbc72aaf0df57972d6b8e2d2b110abfc95','63e0745da9df59b2d3e1f644455882e305e1ae54ef95144feefa5f2c4860c7166bf72877dce664a7',0,'2021-07-03 06:41:16'),('baaade65499c1f9f76c2157cb44dc4552113fcd934e33f5fcda44c5affea69c27d04c6eeecc0ee52','fe930087bbd2d54068a59bc42aa8721205d2e565ef1a268b08e38f7e8d9b993868715b6795de0f97',0,'2021-10-08 08:02:17'),('bb97e02323ffc1078e4cde547949f21d142421caf2d0e841fa1924320ac8dc7483370fd89be021b0','11bb04c111f3e84081efe027daa492d85fcaf4ad88b7fdaed209f04a1a5ebbf271e5d7494a164b8c',0,'2021-12-06 12:27:10'),('bbca87f83d6e27610335af4b585b5523d1c4f429eae644d4e762902a2f3fcdef0d3777619905caf4','70c01924484ba1b4e58f2a922d80b3b9cfddd7add6697f48845645eb4cad6caabcbb1f66a8e466dd',0,'2021-12-20 09:12:13'),('bcfa706cfe5d24ff15bee1aa557f4253161a0893ba8cb0a6d206cbd94e2dcb977e54a52d9841d986','2b6c6afbc6527d9d54e7823617ca5f5ce5bdafb548fba3480e797cfdee835d773c8dae561f01dba4',0,'2021-12-03 06:38:14'),('bd57879a29157223d6cd911dcc5c6edd98982d4828a0e343910cbc94567005dcdcf9cc4b3c221e89','0b3cd9f70f68908d63a39b2bd4ed30ec50bf5aaa8fabb0d2dc1dff90defa700e4f4305a272d5c7a4',0,'2021-07-17 08:53:58'),('bd7fada4519ec9e0254eeddd40e9eaa90b219014d04f7c003ccf20f6c72cafa4e8abd1b4e997ca3b','98822d67b25d34cc9a2985bcfd0a2f9d4b7ba983ca85cc7b6050573084a7d58bd8e3acce13918a8a',0,'2021-12-15 05:47:04'),('bdae5565bda844fd4e110ec5f2103ce5a480d13c3fd1633b9d947d14af849fa459c306c49834b4c3','69ecea47d939a9627a65b6c9fefeac910e14f7c3be1609ebb6e488f6802e649d506e9f23aa239c88',0,'2021-07-06 07:34:42'),('be1c65d82f0548ae405cb30212483589dedce15bbfa058af75442d0d1fd87f421144a594e3f8133e','8fffbaa51d1c7eea70a9109400c196b9805eac85712c8b111c7c6358cc442b5527fd2992e7315be5',0,'2021-12-08 09:22:44'),('be428f55048b4ab74388d8612db57bb154ebe0175abce67f2174f699d05c5026076c5c488bd37f36','1e7827c39358b68bdc13af1cc39cb34ca5cdc25acbfdd60c914b81f47fe43d21605d7dae7c174ac6',0,'2021-06-16 08:22:11'),('be7ad8082ef3dac6e0a18061a9fcd6272da78b9ef5152d70ddb14da988cf9e1dbb455df68dad9960','8ea6062fa9f2846ec656557f146897c9f4e0722d248cf5d1b5e1c6cd04468de182f056dac1d1f537',0,'2021-12-23 15:25:39'),('bedc89a6c1d9aa9ad6526eca0ba71d22b1af08c0f2314f7365b2dc36c4efbf6170e7506efb13b7d4','cdf308f97020566b3b1159e0710d5f43e70f791770891cbbeabe87fff7b0425fe9ad181d57350135',0,'2021-12-23 14:45:09'),('bf03e9c8023169de71f1f51cdc450ed14ccb9075c44f07e33f051a489e72b71c51eaedc15643e5f3','585322dc13c87514cdf0137f6c049f33ed9def3458705c5b7a1cfa06730fca3d2204f65cdb8cc0fa',0,'2021-07-06 10:00:02'),('bf1c5a61bce202aa1c07ee960e88205ebdbc0b6f0ee762e3e8b019fdbc74781f68adbc555ef8d951','616371d14e21254d4a1e4ddffa0c10efc6b29ac7c79026346dd298ff4f182b2755aed8cb23f5cae3',0,'2021-12-03 06:51:56'),('bf236f3561d2bb0f89421202cae0a3605ea8ae2d19a89382e271ef8ec645010837a8de16b42f82e3','da4059a378e8ffdacb4514dd97724f5a73feeecfafeda5523153e65a083e6f517cb734f828a093c9',0,'2021-12-07 08:22:39'),('bf325b4fb791bf1ce4bb42afe0f324e45e7576855334ef590f7316a8d772b543853b99315ec0219b','f89f35e54a4d7f0bfae759769e69d6aaf0e4328fbf4925628e0bfabb6dd9487eba0db4da6a52d6f1',0,'2021-12-23 07:53:07'),('bf97f742b384b8fd3d68c0d2ce8c3b0c68d8bc2b0d218a34e0ca20892cc60e868e267d5bbe97fea3','3e8e9edda3d6dbdd4eef67572d5df0a2ba744c7f5c1d85939c6e7e9bad206be95429dcc1e2826321',0,'2021-06-11 05:58:20'),('bfbf926abe8c12fd3ee6e5b611a7ec6f11a3dcd001fd030dc204b4a22a7ef378a3315ae19c6cebdb','81fde832e66bba71b7e3d20c68527256dbccac17e32f43377c8ff48fda932d7ca3f9bf05b9e00f6c',0,'2021-06-29 07:23:59'),('c0028fa710519aee8ddc1716c2e0f2456f79f8fbadbc7d69877cff497ce037aa6d082279e039e592','7e2033dc5cac57ea0f3fb772151acc61df972fda8b5bc83656ab5c965327eb6e240c6a8a350b4926',0,'2021-06-29 08:23:22'),('c09309de0f1cca79daf1c978bda3d0bb824e8f41f8305b561ce0ec714845a5eca320202b0be7e479','3ae0e4e73ce096ec1252271c338dc0fa8c887c5db0776a9c6cd6f1db95e7667c7c98f932b77863c9',0,'2021-06-29 12:18:14'),('c0a8a7e945c741111c699bafc44361a5ccd1be3f9246851d7c1e32a94994dc7e22050a734a8504ee','b6650d3da6bab0a6a07c750ad056c73139da6c28fb5a5d755b22f4b7b453f25b11ec769f3b0dbeee',0,'2021-12-15 06:28:27'),('c0b4ae567950f8c5487bfd416e127dfce5c350213484fd7102dd42f05190643d0ff7b0019c64d97a','475b02f374fec9b6e0f7716cfb41234ada63d59d019c8554a451c8ceca96393c716f03def9c5d2e1',0,'2021-06-15 08:32:18'),('c0e976606c40cb9e13ee5020ac89d217c9892f0b51c24e75cb519e96968c9cd4374e336888a424bc','180eb0032556d3b2b7055a2dc5a2b7cf00341b5ce549778e8700ca59f8b15b2782e1ee3eff02d1f0',0,'2021-09-27 13:22:06'),('c12535fee86768461ecf813b02756edc25d3002ccbc91341493c411a770691a26f53f0c8553f36c4','20420084940037ef3ab142f3f396d886a959dde49fc0b8010f7aea4890aa3663920e611e5f7ef39a',0,'2021-12-20 12:22:20'),('c16268ef042bfd2275f65b0342c470b424644640f19df07108db65bb958b275c69d3fab210b15a69','c735f56385f2ff254dfac43048e2b23436d4662fb1bfb91bd6183d6c87a74bb16c373526440318d7',0,'2021-07-02 08:22:29'),('c186f861434ef405ce0b4315889d648770b1e782451807f42c644a29b9d594cc25e0372606b27995','be105492f21b5bd33da3de98335f2a4139bfc82d4bd0185c329e9297f13de96ce306aac1e1e0529e',0,'2021-06-29 08:50:09'),('c1b7989fe6763433389ded6c0176934a9a3154122ede55557f12abbefef1ba563cac2b8f5cb7fb5c','3fa66cc935ad58cddf10e9a4c9de6d3790c40e2dfd83e9c23cbc0384c3cb9f9a9ffe8fffa85f7fb2',0,'2021-07-01 03:46:57'),('c29ccff843213e3cf5143485bae40ba11d7324c6e5902223e33f88adb037b5fc63eefb3e17da3efc','c782fa97d90228b165636d5fba806e6dd309d7c8fc05c6b865dddcfdf527c5e2e9051594799f7ac3',0,'2021-12-08 09:34:44'),('c2ad91c53d523d09da7aa549c8cf97703f50d0fff394d0a7f83eab99b104d93b90f953ded45e4772','b13b7fa800d3438186792c09b6daba0d906d60f2973c6c73cb725f2e3141f18c6bbe1b409edcb109',0,'2021-12-06 12:41:23'),('c2c700145fb9f81563d09c9e0ce9b73914f7f4d06d9964dba97b88bc8aca241c0525f93f71024ff6','eed92c8b644c08f8939a38fe2e56dd912501bc4979d82a43f1b55fee59774fb73961278eb4fb4053',0,'2021-06-29 09:17:15'),('c2cef6d6fb6cda6f6fa1a40884d690e5998c14dcf9c68eb41b4a21b4f955316070c02c6d6be0bd6a','d126d382639ccedfd75b914efe023b009d767d876000b96d3e0881aecee7c6b9fd25f4660f8a0b03',0,'2021-07-04 14:11:38'),('c2e6e44065abd515962afc307d431be1b8184bc59be6fcbe340d546968040711e635b6f1ee5c0d4e','648f1a6d34aa64e61a634c3a6c584bb0c006c91bfa46b6fc469bb17cce708423dd4c552fd7ca6bdc',0,'2021-05-11 07:52:32'),('c2fca0c33835077931963b4c2f9d2cc92e76f1c1aeb66167d9c1c47fe55f2639b48ff2109091023e','01901b49bc627467934f1c1f1609c9d86be4a3d7921d3f39bf9d618676e41b43ff831e1a18a50cbb',0,'2021-07-28 13:30:02'),('c3048eba4582a97f742e80e0942ed4a2d312a711e2eaed31a705b7b9f573701bd1d91028209a6920','4dcc2d9f9a3a3daae0a279190915122bff61ecb4db1ca15bbf541e28be06050c0ebbd74f49f202b5',0,'2021-09-09 09:16:41'),('c313fdbf50c70abcd936628483924342fca9801308c03a86032ec0151429d5644cb622af8b4585e3','36246daf016c7d36bca15ebd36ac84442da1eb15a025cb63c5233ffa1396afa59b99338b1c2cc1f0',0,'2021-08-04 08:14:20'),('c38bc70cafffdc082ec80dcc0a76629ab0995566ef764753b9aa38ca0406cc5102359a5975e2d01e','e1710ebf95f400ba95ee32eeefd34c3737c52cdf7669823997699a9510d70e5ca868789a8c0b65e6',0,'2021-09-09 02:52:14'),('c3b049d1406c84cc4502a144280fe8f9fb6d90441c64ed38e94e5ac3242a97dfb3b27347a96c8084','367b9f644f8a9b1f659665524df64a284b3256979c42e230d31decbd1b69c4190ffe15e70837b179',0,'2022-01-11 17:38:45'),('c3ec66651569a7ba7eb5205c7160337167fde822ac827f93413a3d1ff9ed7be93f28a629c04dd56f','d798aee49f93f99f3706c99da3e593cec752189b4e4867b64df33a071a7f98376437823223efcc02',0,'2021-10-15 06:14:47'),('c427fc679a249efedf94c11279af299f10db7142e96844f35de36955c45f3125c823061778981543','87e2bb773cfd2efcdd94f4f55a96b151792f0ce9b62912d8d823664edc78944f3243322b39cd75bc',0,'2021-09-28 07:53:51'),('c45049e64326eede5dc2dbc9f7f8a7086cf18c42c25432e193437b6d7f4d2b3fdc6c2e486acaeb54','79e6270289643d9e1c5ca613ff1485f1865fa025ba88e7ec88f738d291fe58866e99b69a74a17164',0,'2021-12-09 04:00:03'),('c46756ff5851d94df70de86cc673d7b70788a0cfc7f1a0fe2b35d3090c5af4f9bf751d1fd61d252d','672651335484b5d4e7294a0fcf50deca0abc2954ed4ebc269255b879b9cad16c2d43ee67b3b433da',0,'2021-11-04 03:34:15'),('c477482e08aff59a112eea95c04a8edef8804e658e55cf40f0ed4a904b68c209f20710ff54287f5f','a540ae653659660c63a4b62dd1cb9dba6c0abca8d41e1c0e772c54b2a9a0890163baf0775620cf08',0,'2021-12-03 06:39:03'),('c4d1ec4fa505904703f868d2452b42d319b0cb350ce65d92e732d3785ee4c4f8026fab3cffa313fe','74fea34c28184008c8c24d085bcd44627923a546e2963b109cabd58e764efb7e5727558ae38c1d4d',0,'2021-12-19 16:42:41'),('c5e9399365f7829660a0f8a24d764a1a5a8d7fc1670e61805c3f4d984edc99241939f88d1e2a259a','d0058f4670a4ec62f301165234f88beef5ba82fdf38230b72e392fb3a95ee5f855b2181c43c8cae5',0,'2021-06-19 08:20:05'),('c6cc858c070dd45d1abdf7ca129d60293f5cffd5ab2967ae0396d5f5555cf92eb67d465d7ced019d','d1972b333a5003016e6cbeb1f959e7b1b9c2c1ae37479fbc690379c95b00d505a245dfa80a58ec36',0,'2021-09-28 07:54:42'),('c729cd93632ea4cc23900485d22f907aff95033f259b685c58939c8bc3a32f4171ad5cfe0de75d49','5933ec47ff5a3dd1dc4559aa5c547f7e0a5fab59294af48c80098284adb01bb8f0c018bcaf933ca0',0,'2021-06-06 12:17:05'),('c73514e267f432ee0ea114e40849e2146a5e0feff62dd8e6cd6bea700a83358d82af9e65c57ec305','e667c8e06445658aee023ef6514bc5b01ba00d8d32f4578ef5967b855e065e5ecd42d8fac2242399',0,'2021-06-29 07:32:17'),('c737513bd91d8fe52b201c0168ac2909c851763e38c74ef3aaf16e5dbf81eb843eeb372aee6dc5f4','823f241bd1e5bcf76d25ea5cb82d2641f5f8d55a0e4f149ea5de8092184a957332677f95bab0206d',0,'2021-12-03 08:38:49'),('c74a4c3750f20988bf2531063e04ff25210e2ae9255776080e5cd0100349b2377991507a89bcf3eb','294f84e7af65bfd546a7342b493785788babc7e793a21111f7e0eca9f8660e69515df86637fdbb23',0,'2021-12-03 08:34:03'),('c759cedb4848604b7c3868456f381f282436512f6f359899f3738f25d220fc80272bfc02cb5da87f','1a3fa7a005d52bcd5f9f11b9ac25979e57d17104597d3486a791653bd353de49897b4b32cf71b5c9',0,'2021-09-09 03:38:53'),('c7613797f7564d9f1991eb6f929072ab1bb67dba7a4f811663a190ff2c593784e426ed9a390b1fca','1ebfc05790c2614ee2bb241a602a756b6eb025620c134ec53eec211ec76806c1b29d6938f232c7fa',0,'2021-07-28 08:39:07'),('c789cdf23408eb28ecf51038df2a1abfb5a11f01daf7f22ca37c95cdc756d700b63436cbf679a042','e06c07c1a5d1de34040a1600b8673c311d7770326fce7801df32880de3dc1d301da5074ab0440e7d',0,'2021-05-11 07:55:44'),('c7ca7bb86c71bb26445b2305be8d3e6d16330f76b9d732a8b9a02515c57ca104724507a25fa47cae','a9bc9bd3e519ee0749ee576cf5549749b656effdb4b303d5d3809baba6e1333ae5753cf7d9a2b687',0,'2021-11-13 05:36:44'),('c7f804efc69e0fddfb6f1998715c5478b8c51b64b51f95df9f9a90bf5e77f9f4e6e735faae96c9b2','8bad8b7a06044eaf9d223bba9260bcb99527aa40596bc5c0700f97dba3dade29aeff3f92878ca452',0,'2021-06-29 08:28:41'),('c80ee7f5ac544657069a3a5bea531656b70d7ef0991eeec253636980c50d0ade8eaa88131b765779','264ccffcf6a4b0f3bb17e18e3cccce1412966f08f5deb1664a28cc768c3ac960e07d94fd4d6a60fd',0,'2021-12-03 07:43:15'),('c9b82f82241f2ae549030e0dcfb26243848acd838781081d3f1f0b5112b6742d57d85d50ead48788','2a55417f7e17e87ececb77d84fc54a73d2fe6e0fb67935d811f866838e13a4b51e2ffa905125fa23',0,'2021-12-07 07:41:59'),('c9bda2549601d06e9d4b9adc0c1778a706d20a89c127e41c68dbaf4836c3ae0ca4885c63a0344c3a','5cfad589dd2d366ae28c80fe8b1b139e8f8650d310d723e8aced4f71119786db54fdbbf4cae3aa2a',0,'2021-06-30 07:32:12'),('ca2dfb4a99961bde6dd7fdc29d9d1ff8790445e71c224dbb387b2d5aa344508afca5a813834a4b4b','787819d342b5e65bc2afa6eac7abbe93503cfd83564047239f586c57cbc65fb33cd2097b11fd010f',0,'2021-12-07 07:18:47'),('ca63d1f7d6df78dfa482f5bfdd8671a5e0bc7cd3b7d32501d5e4b24b1f78e32206f5ca73c25cb9ac','c1d299e9c558b97d80075ca1bc7e045728b89535b4be5665a008da6ccbf34c1ad2edcdf3dde54ffe',0,'2021-12-07 10:14:41'),('cab3d181b0fdc95b0d0e134a0b516d69990fde5c9c4e8c28e787c8b2493822ac057d52eee9913fbb','37f48b21dddb8ff499d7dc82f392ba52e4d1e232bdfbce30d3dffa583dc29cb1da83a720760af210',0,'2021-06-29 07:27:48'),('cae6557dffec10fafbfe9038625469dd355684c64717abbc722c2df2dff7c3c7e8cedc84ff490fa2','cf6ba60be61ba104cd881123a67ea0f59654e2ceebe956be7877a8f92d31d2569ba754ce779e8fb3',0,'2021-06-11 06:31:02'),('caf37ba94ba56560f5ab9566ebb5ee6fef7e6ddc1abab9457aef26f023010164f28e4ed2d4ffa91c','9f5d3bc9ff124ef742024795aac1e2d818da4b5539863bcd6e4409613d811286f15f3d38bcbe9194',0,'2021-06-29 10:17:18'),('cb052fef29ab95451c861917711a87393e8758aedab8f23961619e9818b94e89a1a90f85d1bbc3f0','f0a2c3d3c79489fbaaef902b2e7c974cb97fad3a0bb80e3bec2b1804398e672c88bc274ed8b83611',0,'2021-06-29 15:24:20'),('cb2a76796dfb9d82ae3e69fdd67eaacbc9a45a0739dc5861fd8a2f242952d0fd7b467f59ac8e121c','9b65152783693d685e1decb5f22b02b0e930cdfc5a2ee9a827d3798e08748a28e7014d0d8cd08202',0,'2021-12-23 01:33:23'),('cb6801efb306b2a1865f2cc100485acbd89129346da35f28538b6bfecd5eea6bea33043fae99f0c3','93317e92baab89a54e393087df3560c9eb5ade4bfd84436f040a1596e58efc947a11665c66467976',0,'2021-06-11 05:57:36'),('cb87d59b605680905d515d1a75c0ca7932b057e3d6ca92ad4ea61569cd3ec9144fa7517d45bf8c14','9f99e2072ce0799fbd66bebcb6dfa6224441922f666e465f978c8255b16bf1af293b6ebde2b6a40f',0,'2021-10-05 04:05:43'),('cbbf096fd7e81fdd86294546b3f2e504deb2b8ed20d46843489f7819a674c6e857179a662b831ca4','2b3071c830e0072aa1c1d692923674adcd6f9c0b2e02450ccd7ec26848d83a7907939a2bc5042ccf',0,'2021-06-29 07:18:01'),('cc0745f2a260e5af692bfae4c0d7da55b223ade6d6eef2f70db05878952571a66c043c428332e917','e223c4abc7302d8534754fcf18eca66ae13c162cacb5e18932642ca03faa799f056e3678e0ed15d8',0,'2021-12-03 08:12:06'),('cc09919ee9ba87d5fdfb3abba8f453cae52d73dfaee236b843638725bdf065748032760526bad6dd','821a48a8aa0c49c5553ef01c42c66341a9e0b78a5f0324b0086755ddb6f16796959ca02d96b26c6d',0,'2021-12-31 06:05:44'),('cc13757878310a17db4d397ad0d5af22f135774dca6db1ff5a82f2425ba9b3724b3f6060a8a3f514','04a359ae1459b0d0f8eecd62a9f40779bf94544110e4b5de28117c389bbaf96196a2618d38bfbada',0,'2021-05-11 07:52:36'),('cc8d63017428fd5601497bc60fa83347bd98bfbacd5cb2ad68ca9c36718d82d4c12c8c2461631a01','9306868cb1e30a111fb1bb41a6bf2f58787f86dbbcbf6f6182fb603f16b43bd52f11bf74aafe8045',0,'2021-08-15 22:57:09'),('ccc621e5956f34126e21d88f3df79c7088efeb7212d6c71ec522464be00a9839ba0783fa5e508ac7','df720a67379527ec851e152075fea4ded7d79e26855f27b044b2bb5dbb9de0471d070349ed42ff80',0,'2021-08-03 09:39:29'),('cd504d1e0ce00bc22b082e0baf2ad085a87471ea89618837b48a46fb9e726c1ed219fb65fa5c21e2','7592adecdfa821a02fd40be59d5de9124022dc88de9cd7c1b58d3c93f78b0cd2fa7683cfd5428a1f',0,'2021-12-07 06:06:06'),('cd61659d7ebaec28f530a3d040b3f953252487041ce1031d10996767776bdc9edb8be6424bf0f3d3','7a063fe9a18648f61ba38d1d2983bfa45d2011389f6247c22c1f4b9f3a6fdd01d3b1d5db9954b459',0,'2021-07-06 14:26:32'),('cdc2c26c3a101fcfe9356b8d28326ad7522f994887e246f284d5eae74ee46b12176d5548b3a50481','2dfc57448b5676b5f801d47fcf043f35dae8d7f454027c11e3436603ec265a8f5877d6ec986a2844',0,'2021-12-20 08:36:45'),('cdda4b0124044797025d91d9fa8d3539089ef90887b2b73bdd896bf63afeb825604418fdbf1a5475','00d2e0bd3854e838aeaf7161dac1d76dd5a42914bb0feb3a59c6ce17b057b3c3fd4e9738fba6e64e',0,'2021-08-03 21:44:28'),('cedbf5df5b5a12f0b9d01ddb2b5a9967d9983d060b813ae237f4fc71f9458f0a637050983fa881ef','e1b7eb9b3f9e2b0322de18b1501434efc12c0bc10b6ac05357e5d243e1912c15767f26324f30a486',0,'2021-12-19 16:42:52'),('cf460206813eec24464e5494e782a86b4d5d3016236f7f26588053fdfd01c9f8a8aa22c1af15c5a6','49ac6392f7629e2ab0ff2042ff82ed6f0c412f2b50d8aa4e7411789209d642b49c1f5e0485b3d85f',0,'2021-12-07 10:41:35'),('cf4701e29b3345d3bfd313a418e854882bb141b418b06d3b6dcc6540ffe4b09505fd3979d1447deb','83b1b0e55f4e70882fa1557fe081b9a0f1bcbebafa6bca04883885a6b8316df81228fe4a1ed72bf0',0,'2021-06-10 12:40:43'),('d026a92b37cbcf91a75b0e9ab85ef11883b76e2a9937ffb3d4a329fb7366eb76dba4430dfeb6282d','2e44bf3f63ca7bf74033bf7243ae5b440a1de165b75e644bfea026a2af251115b9777422b7def6e2',0,'2021-08-16 21:25:56'),('d0511c5ac0780045314a9bd0dba5b20b7174d49fc08c47ca86e48e1f1035c8c18856574f29ee0e61','6eeecce1a1dce9c8a0f6ace59d3a2d8ff6a37d9a7632ec19584bf517ac78e9de67907ebb027291e1',0,'2021-12-21 16:53:50'),('d06d8d01895f159a4b631fd52bd1b78d02b5ae37b9af839a1020e3b9fb070552b7509a8f73e3ca16','1089e94f7fa192be9b09168d47406ef34b3e57ddcc681e27480a837a4b66263dc24593ec2578da15',0,'2021-06-29 09:17:14'),('d082a0437973ece3dd326d83cbb5830badf0e4ce112bb32f5e94d7b893576b4decfe292aae5cc408','1b77cba00557ac30833bc1cb36b9495d1cf8cdde979ca30ddf5c88708fdf14d2ea45fae92157eacd',0,'2021-06-29 07:17:31'),('d093ac437a14a636f64d348ada7d04a5f6aa93f5dcb234805cbb553efff7fc6274ad950f939eab9f','0c5572251909ed91cc2133651ee002f758adfcf45481eb335694b3ea308ae4d0ed57fe289881a0e5',0,'2021-07-04 14:05:45'),('d09839c6e842e44055ac51d48e0d2d4429b2af66263bc09a8380f703438f54cb9e4d244ffef7157a','9d63a659de4bbefa654346e31a7ee0726ef4d407cf2b72e683d38f14bdb5d794a45bc0810c1c4aaa',0,'2021-06-10 06:03:24'),('d0c586d91f23c84455f4ebfc5c6a265f354257d0b419a946bdac5a55db8ef2bbb33a5b5e43b2342e','8087e33cf95a4ce76a9b8185b4f831db24ca7b92ab894bd256f1e3d9732d7145afcfbf2151bde907',0,'2021-06-29 07:37:24'),('d0e513ad57f3cf9410e28d8ad3d663267bc5ee085db5ecf58378f374b51220de76b51a6a82742666','282a0f68b87382db90d4253520947070f22fecb434a066a36eb52321f0f73bade95a850527b721a7',0,'2021-12-20 09:07:21'),('d13c541891dcc05da833a41c522290640fbefe85530ff927ba956743295d755cf8a3b2ded568b020','2cfa8a12f0269b78238122083050b5c20c1889da101eaa1a07d8827eb3a1613c797dd8e93f70a305',0,'2021-12-03 06:32:41'),('d1725bbdb174f78a33fc832fe636a8e9f4f3cea2b0627c25ea6812902cfc4de54bc07e300329d4d3','70918600d093bb72b840534814679dfc6355a801f4983282359870372c8c5a4b3ed0af0fac2a57c8',0,'2021-09-29 09:26:09'),('d22722afa707a8ec543e92d709b76d20bd623eb987ca4d6f19e2fdd9cb0127aa338fb45c6cde1564','e97e4c08924f6d2c3de60d82e7fe284c6e1a5ccf352fdd4f88925a299b1de23c82430312ec6ba588',0,'2021-07-05 02:05:58'),('d227559606b70e1e16fe6a1d7ef02785deee3f6302ebd6ab40cd5c2733b0258905499423ac84b1ab','67bb7fe677f4c3a2a50dcbdb0f0c568d147e552709c2482cba28480c05e733ea6bd0c5283502a7e8',0,'2021-12-11 03:26:44'),('d238f6d9eee80a8215fbeb636c69808ba102abdc50ae884980a7dce6066137f825507d95c5f6053b','87584677ecbbc8e0b79d3f5ab375f3daef33b1d16de3b8b23215663c0a998e24f4effa5ff449b9a1',0,'2021-12-07 11:43:35'),('d2a5f3f305bdb3c14d4ef50ee02aaf681f34820c89384de1493babdef8fabc151c7bb938f6c1320c','e8fed81466e72f09dec79cb3a12ab89e2560af65d775a872c6ec539132919bbf9b9ab76617187871',0,'2021-10-15 06:29:35'),('d2cfebdb74161eb866ecc81362ce29e80c20a4039637e8cf2edd97f72c081741ae96137fa6d1d2f1','a16642876e07f2ae3777eebd1724ebf57946c9e155adbc8ba0e85bfdcf03b860361417bcf9de8dfe',0,'2021-06-30 07:51:20'),('d3140a2cd308a8e7075f6a1e60a031d79a634ae305732d043a1dc2da186641bb15b2a47e920d233a','bc9301927a21d43489682037cd9981724f7bf05ed6876f2209b97ee1f330dc13d03b05613191cad2',0,'2021-09-09 07:25:16'),('d314cb62c8ed103c2a0cd3f32de283587c44dfe75ab5af3b200ac406a0bd75ae735bbbf4092a83cb','7330459e0b89787b8fb3aa3534a70f11d81bcbc169158feefc6f248caf9b9524ca8fabf2c1b2ba7c',0,'2021-07-02 08:21:58'),('d3ab95e5cd58e8416124ed27a277383f8a0a06304c4b8475b6388a547479bd2e8ebc11cda2b64d38','507379753cb6d00cf6dbe6cda9fafd71da2e9428ab8bb114bf6d1ae1b4e42c2f0fd954816d699f15',0,'2021-09-27 07:54:18'),('d44812980915ebf7c61bd4b1ddae8369c92cfbe07c8589fecc43e0c3af93f59757c5a995ca47d8c2','0b069c8c770a9816bdb3cd72e9095d8a9423858b9851a0cd6a9960a6f54a6ddfe974e6a221458dee',0,'2021-12-20 09:04:09'),('d49aeba11da7f3ac59e7d4ce50b5a161001546f97f9e36b9c08b3f7122d2372f4b5bc857946751bb','43d57f5e62180bb16ccbfcb7032f79272b6133a9bc07f9c38bfd16addef7f90a9cc2299e27c22d9e',0,'2021-08-18 15:29:43'),('d4a10977fe4cd572fd6a66509685fde98f0ef579c27f6e0e5e05f94a2322e466cb4289ee91ffd4f2','c62f6caa41be3185006d6be91b9b6d771f2bdbacd276a8978757db83e86fc6d4e012f95621d27fb5',0,'2021-07-28 11:21:49'),('d4bbc96fb7d43bbf97559cf93f8efc62f8448120b27da18e6618195e278b9a36ff0dbb29db53be87','ec24b57cab2175869eab06dbdb5e08d44d3eee63018289d2dde56ccc98320deab5ec3ca85f6baeb0',0,'2021-08-04 18:04:34'),('d525b63c0c3aa07e96ec6a5ab68a3b355daf94556e76874ffb66119f39e87a2389db566ee3e6a649','9dcd3db0d54c109ae096f88f9a5b5bd1d0e32463fff576857cb6b53e37995f1e46e0a2b5b645b904',0,'2021-08-31 03:33:24'),('d5812076f052eaa13f8ed964ed9485e808dc717d5946a5ef4a575541337d5718ec8ccbc4266e7635','e6a4760fedd965b392ad4eaa1288d09ba2dcdb7622fb4f5f850bd5352aa6d13979ce4d4dc2da0e7e',0,'2021-07-28 07:15:53'),('d5d9fbeb3c9637f9c4db201eab5bf37397f5742c6b76da294960370a6b32a8576b480edac4d6d511','31d5b1d0efedfd32176718e574eba717adaf9ead838fc92ba3d302bb8f6341483ee379fb9c9f58d4',0,'2021-12-20 09:00:29'),('d62959e2809d5f4f2489ecb78d7ba94461437bb838a32416bea2ca1184709bf4a48d716a5df88949','0ecf841b4ba5daa9670476e6fe849f6083b9169feb364a9b231d10b5fbdd1a1a1d540af39d7334cd',0,'2021-09-18 06:22:51'),('d6548c6e06e70e21c00271e3cc0b8b1683f3be6cf73643cffe6735bf4d9d03971c6ddcff87bac3ce','c14fd29ff9944acbe47091c4b4c1b1e13be7cb5c09b460310bf6f93716a7e08ebf6de54a4cbe8b83',0,'2021-06-11 06:30:05'),('d75ab01609700041a97ffd5ab83918bc24d09fe535a6198dbabe5beb06446a8fda8354faefee74c6','dd50c1442995c14209a6f5e98f8a9aaf9b051498e5bb8baf0cf157b46d28fcc6083747d042b4710b',0,'2021-07-01 03:52:21'),('d7e96b25e268ca69e0d0456d3718b5bd3d9a3237873b211d76b26941f9191da116723e89111e7e80','8132182c42cad41e33981c02d347e306211088f6bf6147c3c64f13fbb1d9dc2f4f39e30059d000df',0,'2021-06-19 17:16:16'),('d83f953008ea8f8e99a7ee19f636a761b78c99d287668a032bdbaa82e26f63075417adf285d5a585','aaadf29f9c759321c8a2aaee49909e7b287632b40494efe57fde98ebe94ae7cca129b2e5d8540cfe',0,'2021-12-15 05:20:30'),('d87488596cdf852ce1f628ad6019825de7450dff5c2ed75f071cf388fd084d62edf42218cf8d8f2b','0b3ac6f8bfd6883eadadf37e866b0334fb95e3ff95a0a0cc634d3a1e761445bbf6dee82a6e1d3618',0,'2021-06-06 12:13:45'),('d92a1f514ca497f10ebdc17f3d6137631cde7e8cca5927139237aadef2c808926db7732f2dcfa42d','dcf9266b29a7d321ce6a221fa5beba68f7b88a98088bc47071afc36a97bf3f3ff91d6c80859f93b4',0,'2021-08-21 02:55:47'),('d9417f86ca4f1561969267bd5a270998033fe0c7610c274afb52902b669df46516e89c0965010aa0','a58f15e8d500ca49559fc86eca8d78a5cdd32f3cd025b09e6cfa9c0e4272a3c5a87ba333a505418f',0,'2021-06-11 06:29:54'),('d95483e4513fa27a38205c5fdb66accc6967937396e3180ed9fb0c688d778e2fc959fc9de05cf5f4','68259c1c86ccb307fbb72737e999cf882635d2ec98bdef3bb19583f318d8aa3e358a3a48b41f5d36',0,'2021-09-25 06:23:20'),('d9cecf8f1b86e8f6adecff541551bca5f28d38b9a8d2797418b57fc8db5bf3b5da14ea89997a3c50','9d4af552d6a48b8ede257602f46a14c33bdb0fb606f0b79b199006f4208f4fdb87770a0a1b33d4cb',0,'2021-09-09 03:40:17'),('d9fea98530ae659e2937c4bd607d79c6a479478895ad33a583da8469cffad022d2e69936ae77fc5e','1e04afb7e65fa98f85ba6e0d8222fdd4b452aa43ae02e26208887203995b4a800ff783f4e279102a',0,'2021-06-29 14:31:36'),('da353457ce0f5dbbaeea4afbe9185b241c7049e27714f91bbb806d71e7171603d3ba01c560441c62','ae81e09b7da936dadccdaf26d80a4b81d2e48666acfc5b324a2e2dda902d343d97990643b3eaeafe',0,'2021-06-29 08:27:20'),('da6a59417da880a9fd88cdd5231db8e1804cbe5a5f9f432969d158541202cad2f58dcce3bc36bcfe','e8a310b526471ab13f0ff90dd6d5bbb9b1f2be0413f4549eb7cfa6256c23535ce6ac99c6cd7377b0',0,'2021-06-29 10:08:37'),('da8ebafbd4f2daa9d512203adf644a592f5be7926b4e34f4223acc7a7819d615972a4912caf11aca','2633644d314ecd2c28d66067c86c6db37551d031281967195f90c198d24272403a5d524ee895db77',0,'2021-12-03 08:19:57'),('db5d37e6cca6fcfaac30b2418a039ef57f35c450ab57610864f15f8de8b05e25ac7ad05bb499d1fe','a73d54b8f9fefe1ca1b05e7c5b62f202a7c10f2292ef1fb63e9a381f189edda7ffb0f1ae8815b9f6',0,'2021-11-14 09:34:00'),('db6c49add6e5869a35cf92e2856677f1c8bf29bc6752d279c935e9affd95f2bb3a587c5b8c9fac4b','f71381b533a3d2cce6f9db92901a625d25d10e24cb1561db2160548df0f633027abe4a5a8ba73be3',0,'2021-10-06 07:00:47'),('dbce33793d259a185cbf852b4319120bc22d82b2529fdfb766f2c550d2ebe41b48e5476f09410016','0e00b32c84890a2e34a770aaf72fb589adb1a12a0646238eeb5f41efdfeaa9f0a0323035846acf4c',0,'2021-12-03 07:29:42'),('dbec377c6cf462f07fffb60a3a48ac3ad24560dfc552890c976b6aee97cff8b77152562d84aa9c47','1a5c6418c34a29e79bd87f7c30d0334e85777eb61f1fc4b5270f4a0e7427d71bef161c4bbe968845',0,'2021-11-14 09:29:40'),('dbee3b0740c8169c9dc87c47d0f93dd447baa2dbf858125a449ec6972ad89a5dcf5296ba909f7be1','f7139cebf1bf43513a55505fb18bc69c1e426b180c522d91c8db935bc729767f3223b40aaf3d7d83',0,'2021-12-08 08:59:55'),('dbf0730d0d1349ee539bdcb89723a4f2e645911ec504696ab2e83a2828a98f4bb67c4f7728d8f780','f7b29c0b08af1e41107d920473ae55c3186590626fee1c9eff11d582c444ea15bba7b26e3ebe6017',0,'2021-09-28 01:23:56'),('dc02829f0c0f4abef51c89a65dd73fed276d98c56f445e16622bc69ec06f10c0e723a9b7ecb95e84','78fdffde8e7422e1490c04d2b550ba2ed82259af6f9d0e82ad0ee30d8316782abea2f226a9bd6f6d',0,'2021-12-03 08:50:13'),('dc28e7d9cf81bef18ce34ec92a97727d39eaadc75c48686b4bf093cd7c0068502892e0a65f945f00','65d0f3174876eb0d4eae09d0438086f33cf9a1e3c1cbd45d9e0b10aa9bcf92eafd0bc66715679b9f',0,'2021-12-03 06:32:24'),('dc41496f78f6baf5dc4936f41469d632a38856755961d0a1164008d8d7de4460b7f79cf419c4d6b2','71a5cbab6db1860fe34912907cace7bf795862c8a33fe202471a6545c4076576f3bc935eb0cff5de',0,'2021-12-07 09:29:36'),('dc44cc6369944456e8425aa44abd3e941720bcedfb43b37da66d73e70506a846f8b924f14789edcc','f4d67574d4575c17d10689785205f55532837d527ac9ca1c8bce2c85f8b1884a2f64016d0887353e',0,'2021-09-09 02:45:03'),('dcb800588f5b1f218cac290b49092c54e3028bd3a6c4de71d48649c7124b6bd11f874457fd74f98c','325c3aa34a2017bd73e9262d0dd24426f18e8b671105c62b66e8fd033bd8737615965e00cc98dd63',0,'2021-12-15 06:29:58'),('dce33bef3383ccb6c2870b289f81e397389928a89ee3bd2d6a8f38b7d4e253c04fe890c4e8187d6c','e00dac8d7d1ab19d5e259770c13fb7118ad56e86a7c16ca0d9b24a2bb8f8098f2d0426d64509623f',0,'2021-12-23 01:31:05'),('dce927463de3789b8508fd5273cdd74ac0505981b9cf297d830b1117d11864b0dc7defb96ddbde65','2ee24b9e7f973881453e8c7df020c39e03a868bae3e11dd9c7c733baa82594cfa4746e60a5ccf9c7',0,'2021-12-15 06:25:12'),('dd1b238c04b3c0fc527c07df67372cf7f456f887fda05def243349883516316669c5e15315d07f09','be93c04b9bd68c486d8675e3b6777064e16cc24559fe87b8e1f7358a5acafe5999cd8fc143820525',0,'2021-12-22 04:25:38'),('dd231c92de88e40e8a73336dbc2c1b2e9656f328f166273b4e98c2cb345215443f8fc67a37d918f9','f50d5a5362c939e3d9610782be6d0a1a12fa5aef95ddc2d41fa996186531fac48f0d7eee092ab6be',0,'2021-06-10 10:41:44'),('dd252cb4f82ecd8fb6bbb7de68e107ef872e3eba0d0f39be558def6fe8d9f8154fba2c3cebe81f17','f1596a0f66349e4b05c0e02798f9840b917270f7b49d4b2d546b9e9e94dd50faeacc158fc1d35175',0,'2021-12-07 10:32:58'),('ddc8ee1593bb7f5a2fa5344b17cc8676c3a9302512775af7e93cd548f6be1053b7c8517f9acd4390','421df7861e1a21cedaf07ac99a01742b0cb18c9dfbb9443aed2297f1005f5efd08dc8bcd21a55b6e',0,'2021-06-29 08:25:02'),('dde583b7845f1008d2fef5b16bba814233c0949af4eb409f2abbc9ec169f6895880e1f89a9bc9d1a','0691a9f545f6de3bf23a6b7daf359d7866c987877a4a7b890a619c82dbfd3a7fd4a201f11662a7dc',0,'2021-07-06 15:04:00'),('de3fc97ade921f2e0d758b6d8e4acbbd3dc20282f410e5dbf300fd4c93aba5e4561fba8a74156939','e241286c040008a6302d4430e4d2020af0e7b89c123654e605d64dbb9b2e73bf960e74e31b1a8b18',0,'2021-06-19 17:26:45'),('de4cc9b1bdbd39b0c83530b00ce86ddbf60db6ce1f56bbd265b72f7ccda6e4778d3aafd3cb130c39','d0ec8b802b9fe3884caa1f3c273b6c9f056488000aad6ab272245f1fe189d305d653eb835196400c',0,'2021-12-19 16:44:18'),('de61e3514a530d63a59606b26c9f914dd1aa0c14d706c2a30f3192b4391e8923c70e85fe4e648668','d6d2f171686bc88cca86a370c277d1fe5ff742acfb732ddf86f5fb447bde29f3775b2b9e38e474ed',0,'2021-08-04 00:16:08'),('de6a8fccaedcb00fa6ceb1ca9a7345257d271157c8cfb38ab9ee0c4954c746d5af0d9b65d66974ba','a8b9754ad19105c8988ebc815e897528f57d9e827840eb837dc665fb29d6859ce025f1f461cd31d3',0,'2021-05-11 08:08:10'),('de972a4c6d6693f30ac79900edc734c16f773cb935b628672bc918b60d45a5b17a6067c57d9c6f55','60e188aed967b938734c9a3789226a19f42b1ca65b9436737f34e4b59cbb586cd8d77771f95c8b83',0,'2021-07-04 14:13:52'),('deda624ba2517aff11983fd9b0c739a90fdc216d743ce46fbe8a88e0c85975fcfd7427219f9cd260','37b98446a781a7dba02b754c800c1326bec9ffeb8436d824d8696e226ec4f5712fda2a93db7c91fa',0,'2021-06-29 07:27:06'),('df14e66ba6d1bb15980b45c7bfc7ffd7561d7e04c81687df04bec0da188ed55f337f48a18362d347','fa8fd2f60713b029b1e6cfb0f6887f84f5bb5180d8b336a6483d244c2e09323e0e7007bc2553771a',0,'2021-09-09 08:08:51'),('df81783324f09a1a3a8e119b2cfd4239820ec4687e79a631fc3e9e4f2087db442020e02207b25b24','6cc54121917b056b522fb2ca36bf42c5cbc53f4169bead303488df910e564944e81b87b0baa7ba34',0,'2021-12-19 15:21:51'),('dfbcb8061428298b4b9e516bfe5309515415da8eba2dd147b76898b34a5b9c6bba7916af3d56931b','3042ae969592943f11d4c922e4fbfa0e87425faa3ad9d0b58f4c15704126bc692ee8c77c919a432d',0,'2021-08-01 10:03:24'),('dfe7895b6537dc8ce7abe7612a7a8cacc7454c9059e6a70a7003042bee182bb1cb75e3ff65dde76e','54d20f69678ff2f7083c09e73b4f401d575a6d91270ee3046d31e919e313ec8149f25933c8621082',0,'2021-12-07 07:37:02'),('e011dd87eb9c0d08337da2e6a3c048205c0124e1592b449a88134ebf4e0e5705ac124a1c87acea9d','79b5edb35b6884bb8a250f3fc47e28de7637d7f3cb98ca255ad3d157b1f84a8863d1d8dde820238a',0,'2021-07-02 02:04:08'),('e049e3da4819610bee0021080af5859a00b11279f4a79a5b775dbc9cf76393b54d8a6c286306c0ef','5ea6b356e05850ce644601042d20e3c6d6197f995d16d5f3fd8526b5e5cf015009811775c4e816be',0,'2021-08-16 08:44:29'),('e06f471ef2a7ddc2e7954499177abac9a52b8548d1c0297e134f95de255a8114380e0397b243d282','bf5e99e89056c6ff9b666a147da931718d0f41e1df82f2bd229916d29b2096be10593eab8ff74864',0,'2022-01-07 13:40:37'),('e0b511a4ea430fecd0ba0faf3dd9ab247da0d25301dca2ffaefe1f282b000431f529e674bbffd7eb','6c63736425564a18b4d488deeb66f613e0ecd75f46b4be1bae30fe082c389682b8f271abf48a5f08',0,'2021-12-19 14:41:26'),('e0c3892d63f9e8c48b0d353a50404df75068ec3bfe953921a63474829826607a42a10d53df02deca','0638d1a1724569b39f1e2f2718311d08a028a5ea725b119df09cd65abae06153566891e00f2b48f5',0,'2021-12-18 10:00:13'),('e13542ed3b57949235ef6bc32a2319f2123934de15a3bbb8564e075d1fded5a87d002c4950540f01','25ae88ace0ef3d6dbc9e00a1ee7a5f0b9ee8493aa3dec29f5fc29ae638bac31b1414b6cdeabfe26b',0,'2021-08-18 15:51:24'),('e1397ab48b420a3da32f48df67561a872603c18cd64c8449f3f0627514934c8e67b570ef8a110f93','6cbe105df99edb1c7a98a6737f22267182f9ee579269e70e8e59b43deddfc38dfcc4cee937ab088e',0,'2022-01-20 07:31:27'),('e1d82d93b997e14054e598dd529c9f500b7297e33e5b5df94daf2107192b1d9f5a178ef09b9ed09d','8f0b04ffdca3e627bbeabbaf65a8df02ca864a38962dff4ac07ce4a1a92e4e4d00a4e5c7c2d27119',0,'2021-12-08 09:39:34'),('e1e5d54c23066211e55a1784a3791dbe4f49efa03ea83a94c4f047afb64e2bb2ebc84a349f1a4816','c2ba832583163422eb784cc2ea304bdedc1006c4778e27a942ca93fd6f9a808ce231159788849aa5',0,'2021-06-29 15:55:26'),('e1f0273dde802ec2452ed109b2911096acbcad5e47c3ee3ae658067c6296907bfd3ed5d882a900ae','9c7bdfb55bc30f6d0f797250120a560a883f63c10e4c8d1ed7b96aa729b09b0ea8dba7f50c1bc328',0,'2021-07-03 09:32:30'),('e214527323bbbffbe36e3babfbe9b2bef9a4b48404e4d3403352f1c5f357ad7bb03574b221b6ea6b','7b5647bcc30caaba232ceb3e1d20126ed3cb0505ac20f779798bc5d93d4f8a7d753c7bb4e4bc8df7',0,'2021-12-31 05:56:22'),('e217deb05d0045e1e46ec2e06a677a6f8ea8287575961ea8ae0fc968e03b0c9f265af495b12689bb','d114232a0bdff39f60fe06171dacf3652d674393f57393cb6925cab4ecc097fd1889af0b8e2f2d59',0,'2021-12-06 12:22:44'),('e21b94d19c02629bda3b7976aaed2a6b3a533f3770060c78e62cdf6b35fa9405c578fbfefbe23030','e389154d4927666dcb96095a31122ade9efe1530240c16bae42524b8ab50518dd4a6d4c9c4d355bc',0,'2021-06-29 07:59:03'),('e2419930115ecbd428c30005debedcf9bde32a46dbc28f27c5a025cea6799ce7d6860856d99bcd6e','1560801b34dda9b8cbdd6653dea2aedd6e429573e59b49dbaa6c2e8e88290d638426a6b83b65b4d2',0,'2021-12-21 17:05:46'),('e26ea787af815e5b2dd0f53be5e46d6b7924a16f97c1bb2587c532b72b5e0b1097a9d553fbe6a65c','1dcc2ef1450ad4f51fd82f9b09a263a32a9040aa294da8b5531da1ad6f621f2eb8260d18b958221c',0,'2021-07-04 12:34:09'),('e27ce4215a6ea72fb03ac0cc94337c14fd215952fb07597e7530e57a5a69ca12bb2db33aa724f294','220354e3b137516a14e49d86c6263125c35c3bf854fff462d0c71e323e982105060e42a1faf68b54',0,'2021-06-19 17:26:46'),('e2da01b19ff1b52410757ce12cd21ca0bccf488a6314d39600a4abfbfcab7f39e7e08cb88e67a1a0','bc748e9a9430f84b19f318344259d1517a21183ff5d8f717ff29a0cbd4b16cbd23c93564037a5c21',0,'2021-07-22 10:34:03'),('e303f21ace3329ea9e36777da41294b227fa198aa7b4c4f8cbaed85a25e15e0a20e81befa95da65e','44d781215802fbc075349b9d281ee7c2f1d0e581e17072cac580385fbd8d2ed14d82b33d009ed678',0,'2021-12-03 08:38:36'),('e332d17ae920ac82bb5729172e6c4cfb5252a41e90c545b455280e4ab584c934a3f130396f548922','726069735e13c996f9d1d2f9a4ba54f4819a6e2027a0cad19b464b2668eba122e27b1c11d7baa970',0,'2021-12-15 06:47:30'),('e34d92c1c97abb0c7eb65948cde0572b2812d3f2a133df370f49f131457f1bd91658351602949cfa','a472de8f3bbd9b69e44fb09eefcae74d831781a61eb7062fe746c89b74e057e9f4e75c41eb668475',0,'2021-12-22 01:00:57'),('e3eb540d991aabcb6367995bc5031050ddc58ed6625d81522bc883a3e546198ad2845609bf0af5e4','2db6e31f68c23f8ae43feb4636832742186b5e033e578e51328648eed14c5a6c90985de08551c977',0,'2021-08-03 21:25:10'),('e471354ffdc1bbb6bb001a670aa5ce3c57ba0133d05a45e7c12da684a2a9c54f68d53c4b58f61677','ac0275d67e0feed3cef9b103d62d731654bcf72bb1362f4687f45dcf6676d6ee742f721fa37e503e',0,'2021-06-06 12:58:35'),('e4788d772cd3daf6af1c9f0c9432a19094e69f13a3ddbf3b8668b2713d004bd1f35e1970d94b655f','535dab7025317107c6fa1f45acb42f35afd32f6f29e85266f409eeb29669b68bf1cc9e8c87e77ef4',0,'2021-11-14 09:42:27'),('e4d54bde9eff10c29adb2287c2a18169cbbe01393cf664983c810f8ea1330a2eb443d7c3e20a98e1','a21e18bc97fb16b964cdf9212fcb8983f8359f4fcff6926936761f322ba58503d5af7d1d751da286',0,'2021-07-04 14:35:48'),('e4f1a0728e30992ae688b4889e074d7549d6931727df6cf79b2c93ef7b4a38d4bc2217b3cff1104a','9655c211703877828acf1dfa73bdf219069c217ffef3d4a63a823e28c13f1aa18bbbd21c2848f598',0,'2021-12-14 19:21:37'),('e4fc9c6ff3476e0ba1484031ec4bec81776aa8bc9e6d913ddc6ece99365ced50cd6583c2bf5791cb','4ec72fb88fd382088a5ddfe038c286c15e64799b890857a3d6c7920bcd96a825150dec0860a6830d',0,'2021-12-07 07:50:18'),('e5ebbc9b98946246874de24192c080089407ef337ac8db2a3f704082f18265ea76da1c1ffb2db948','c9a0cbd78a0925f264c5270195364ad867b4f7845dfee3aab298f899a888e5cd5cc3f215c53f0437',0,'2021-06-29 14:46:50'),('e61206be29b25c489a39b29016b620fcf9b838cf3fb6f4f62d07d499add22e7fd1f25c1552ae4728','10bf3418d89a15cec29a48fc6df4a1efefc40f84bcf0bac268c07af68f7242233fcf89981d04eecf',0,'2021-12-03 07:31:08'),('e667883d303e92d059813cff2f571803583c41565423dc4ee5ccbf52e34e76a439d21c81417832ee','c6c9f72dbc2b4e7bd64acdc6b42fc609761cc95d13e296ad8f29e0bef66a29c4c2122147cb7e8a86',0,'2021-06-30 07:53:06'),('e6da7593435ef39c01f8bb3c8c217d3b09ff6e4cad998c50e4b9efc75b9204518cf1a1b21a4f7ce2','091880f398c788dc9ca31b0cde2336ab751cd6f6e7bd1bb3ff9883264a39c13bff5057c0e118ec7e',0,'2021-12-20 09:21:49'),('e74236bec089b289c182edc8088536f43846e1c2f141a87df5c73a4d190e60251f6712f3f884af79','dff5043d4f2b2cf526709a1a8b3dda347803ec5decfaf7052e3a6e5b06353f5068902d6978d74a9e',0,'2021-06-29 10:16:19'),('e7d13963bed382873c811060c09dcbe4f48ad385c248dddf9f3836d862f1002eff27af8d714199d4','214c579baaced54b771202dab295f9fdf6e5475df8d5b637440149f11a09b7cb96e4ebdf83231020',0,'2021-07-07 07:21:15'),('e7e3b259c94be7bf769972df3e20bebc61126246b684697ae052679082325538e4dd27eb368986c7','7b046356901728a73ff6d51737b85b0b60f08ba91e25e2ea8b5380f17c068b217e8944ae3103df57',0,'2021-12-09 06:45:49'),('e83210e338a946ab75b2f57b8183e87eb96352c2bbc48f8b8f2fb0ca829ee7359f7fe6a34c0caa22','9808b59bb3c2be812501d377bbb343b15ab41e60d386ad0c9d2bddb39966126affba7257fa0f4eb3',0,'2021-12-07 10:40:31'),('e83e95281d6f6cd9e69e6bad88acd9be97b6ebf33f540a925462d9f460e66109a8d94552816e2967','99af804a82ccabf002bc1447775fb79efbf0f208a5d974fda1e008e0eb35e70ab8aac959e80fc61e',0,'2021-08-18 15:09:37'),('e863159171b288873c62245c154ed7394d242816821dc3f4d810511799cafb32d48fef2f041d241f','552a3685eb4db639138f0c993d030d56c85d99a7cf29568c128009cd81814788878de28f4b884bbb',0,'2021-12-19 16:14:10'),('e89ddfe6b94d1df5331211e32c36eada82ca378c214eae0158534665e4dce9e063668ad6cd6c0fbd','2572379ef0d2b533efd822271f1d1571cf67eeb419cb64db9703ae450633d652910d6b8e1b0fc8a5',0,'2021-07-01 05:45:26'),('e8a4af625a9dbf8945fea2c10926c14691f604a3f08591aeebe7a46f07955969c6a4dbf776a5f706','216ae3a641e6c3ceeea900b2eac6308ccb4c27b3a4099be5ebefce07edd39aadfa81e0a5afd0543e',0,'2021-07-22 10:36:33'),('e8bdf1bcc290ca108baa5a3ba001636847a4570b3dff5127d4f3a096e73c62019c4c0bf4f0bac5ed','a8c78a2369e6dd3aacf093ccfcf5d9de75e0598a7843e1a823056159857d0d37799ca5878064e532',0,'2021-12-07 09:43:21'),('e8d608e57db0a4c5d114362c1ebb5ef86525022b2300512f74d041fa552b339741643889bf7bcb7a','c1f0f48bcfff41b7e5e99cc5708f3af860ab0827e08595a5b6bc3316019dec0e9f20a96096301ebc',0,'2021-09-28 01:57:22'),('e95514341be772e9cc0a590db5bbdc2f6bfc3805454cd1a58270f2f2c8311e331e1ccf8470c51fea','2ca23b33c03001e11c971a10686af5913b9d506bc8b9649be8e28ae02632e589efe256eaa251d58e',0,'2021-09-09 02:49:42'),('e9b2ee3e669087ed1fafe13e47111ff4bca0a4cb0df02641ef0ee0259c13d37daf110535b8400c81','e514e162217cdf23d5c9c4a88d81fc26fc4baf832b3c34d97be6d3126f79de6ea30bd5fe40187679',0,'2021-07-17 09:13:00'),('e9c237ae517b80b899851177088ce54c26bb71dcae6a214cee1617e615f53b4a79aaa3ceb277aaf2','821fdf1db151ff28e5cae4d4a461ec118b21fd58c5b12dda23ba1f9cd6f5c2c7f17e5368ffe30308',0,'2021-12-19 15:50:08'),('e9cb980c9cf8bf215588ba77e0585051d227824babcee009bd2b29659ff2958b3d4ee70971024ddf','cbfae76ee805fcab2e1362286788b7a3980c997da73e9fd6203ac05174441a78f41c02a609cb421e',0,'2021-12-06 09:55:49'),('ea4b3add337f8d42f96199c5eb5ad910045d9f182f660ea899a73b55e8c1a350f82bcf78352823c3','87fea8cd693e1cade0efac393e55072924a5267a61015de2ca97bd9968dced278f8a9cd48c2f5524',0,'2021-06-09 12:40:37'),('ea5381f3a90019f4c8eb35851b8fdd816125e30074165af842d77bbeb49bc23af8e39e61c8aa537c','a18a23d900c2bb32d27575eab8467d9869c720695f1f8601db546fce5e46c8cffd5594c4b5c78d47',0,'2021-06-09 16:02:10'),('ea81b202ec67edf38bba3d880b47a1618debeedd79466f852e91b7d7b8d055255d2866511200fe69','2020a1ea02a174a09f59a67322fa956f3fe7eb14d4457599467646bb15279cbafb8c214f690dd62a',0,'2021-12-07 10:27:24'),('eaa3db56a8c9fb9d382203ce7f303c0b85fd01273c4e7a91d1b92a3137fb515725192d359b0447be','55e338967e574b3f729c9d21ad8b6514ecc5eaad610c0af15277921cd8accccad59ec1d57707c5b6',0,'2021-12-03 07:30:42'),('eab82d1a46d36d88f8b36a05fbb5208d0b7f93505a14bb810bb49f7ea3c8e9953997f766a80e19a5','4d8c388d71c626f40ef019716d0c8155d7922afa1bb8cc8b05cbaf80d65d7bda54327501834aa351',0,'2021-06-11 10:11:12'),('eadaf5c29f32eb66b61264485901f94a0b7cb8638c6897a7933d3340403acae5155bce90c4943576','f6f6ed5ccd703845ac63e73de174a88d170603795dbd6c442328c1bb58b2a19e69ad61c601d7c671',0,'2021-12-25 12:04:48'),('eb2481038a6dbac147e1a0504ed719267057747ec309c5020ceb62527d37dd446b4c376d23aa828a','76402175b8f49f9237c48cc429df4a5406f1666aea58570d464ac85c4f03fde8b68db398dc107400',0,'2021-06-29 10:00:02'),('eb33e75f20a64c2ded1df310a90a32ec005e41708e2ce8ee5dcd2d0b9f8971b32c6a663b03db981b','bf3df97f88804cf163dbfb98ceb9a4a5d928d05bb980ea8b8282aa8da5ea704ce85cd84a5144ce4e',0,'2021-11-27 03:16:02'),('eb43fc0873e7ccc1bd2c867e59b4caa90ee7aca15f80a08d51221b844a92e829e52a12cc074c1989','6508efe6c2f94d8535ca59c45ed86ecba13741e524e3c0f31696a5932799e91392ebc1f1ccc523f8',0,'2021-06-29 15:24:47'),('ebb7f2cda46ae81ae1a7a99bd914a285bf5d157f248f192294e00bf7320b26cfd529639baeb10ce6','0281032928fa562243cf78bfdb29e08fd8a668ee88a9403deb0f1259f92979dd8e76c94701504218',0,'2021-08-20 01:05:07'),('ebcff61d951fce890663708da713791f5d065bc7830b03ad1dec841561ed47aca63df40978979662','bb993a7458b76aa6f2a1d9d998668e0174b8d8a908af4aeaa202e17ed950bff136d75473bbed62d3',0,'2021-06-19 17:15:30'),('ec8a512db063c493ee19afd3aee8142f14006cabc1dd181245e3fa2ce47fd864199118920d86fdb5','62583dd5b458b6cd003aca37658c486fab67d4a13d5c5d0dcaddd69336b97171d1b88ac66060511c',0,'2021-12-28 06:59:02'),('ecd2a562a2b562b69ddf0a42a622919e745af8293e80b185d7fdf91236940c69f7eeba6dc5fc0641','fb7ef5d9670aad0c8ce9064a8e49930b9d7ab8aefef58e34c70210d45639166fa656dae677ff202f',0,'2021-11-01 07:22:02'),('ed129cae5c2f1f781e4c5eb8baf3a78b00328820a970cc215029cb63b2a615f306e21b904b8b2a7e','580866cbbc1cb3aa455890b83dff2844814f98912b07ef2c1a2cf10e9b6fa5e2635c49de80d25c07',0,'2021-12-15 05:41:18'),('ed49ed2918f528856e87658b2c48294f61e0f773626cac620fa5ad6e1a7727157e563512aa577564','97e217534c064c681fdea7971e0507d1e5adaddaf659369a89597a464a084292d4994a331f515eef',0,'2021-10-05 10:08:27'),('ed4b44764dd3fd04d0f17508fee23877909d211c1a743acac3d6bf33c71d685516a7398694a56990','65103ad130403eec5d87668bc5527612cd9c1d73c4f1f2536653d52a787d5df9709bfdae40ed09a2',0,'2022-01-07 15:06:26'),('ed58545c09f35b3155469b7517ae27492e434b15529681334d8b284b7431dbded1f97606fe90c0ab','5e38766fd6a4aaed54979f68826e8db7416cff004723d41a7488e1a1b9a4c1f77f1d596562b6349a',0,'2021-08-03 12:35:15'),('edc5e6625aaa6bf978e439d797018cca6b5f400dc8e0160105d58b92125bc7aebff82922d56458da','2054de8e8df8d081cdf8f2e335c0e14990d5dfefca56df74b0bb40d5af8cf37f0a3bcb332ba0e1aa',0,'2021-12-20 10:46:42'),('ee81c6505fb9feabb646045b066f1fd44b9f6e325e5de82859a862cf7acc8bcf7e5e6fd5cb96cdfe','309819a70dcfbbfd53a00849650a7d20ff23ddee850b02488f9e06185c8875bf4b99646d05b4f53d',0,'2021-07-03 06:35:00'),('ee8f610078ce90a0898f05d6a3e820501812c5d08b3accfd174fe595f9b5bb50c72164183eb84127','918577bb1fbc4019b29055f69f0a0b52e8a583d5e20f0559c837a44df124e54c5a3704c2fd556ba3',0,'2021-12-07 10:31:44'),('ee9fd11bf749531b594e7c4a12ac72001c3daf6c3175412727b84d8eaf3a854eb69ac4f22a2bbe38','fdb9a5e97f77ae7e2f39076d2297177af43c65f4c2c94e3dc1dd6227256516d4b0da6acf2b10603e',0,'2022-01-07 13:30:30'),('ef39d1f3f8c5895b97aea4b110e3f2f72fd6d9d81854ea303e94c23df09ae15321f1ea47f0aa26ed','097aa88e2ffcc94daa0efd4654cd41aef1861d37254468663a8fec5d7ed65109ec0790843bbcee05',0,'2021-07-01 02:55:53'),('ef67c5eaed5e25634b0198058b528b5b1d8d6db2f486c7464c91032a9229b5f10418b93dbcc8bbb0','3303da2d7b8783f41af034e7ac06498a174c055f8f76c7660f0a8f45a8941eb92fb1158d159e27d6',0,'2021-12-23 02:30:03'),('efdeb9d243eb42562d91f541ca2cfbb08acab4b4690d1bd6c60376ec94d7b14aa90df74e14b82701','9e7a1fbf4048fa3cacc2049a75af8e4442ab2e6b5ba639eeec2b6a330c1a46bdd2650839f84166b3',0,'2021-06-30 07:30:06'),('f01ba38a71bc75f632436718fd4d3f7f7de32b56cedbac9ed03a8a501eac0e7df97efc02bcd40485','b97be15f94ba0c07cbe3122ea91f34d78c816a3dadefb872bad930524e78067923153647aa4fc4ec',0,'2021-06-29 08:50:49'),('f024a36fc012812246c961b253855f4f83870577caa5ab5725b09e0967950e827240800d8042df2b','bff70723876e7f7b16a2284d1e3eca88ec3b24bd1df0d5d1af87717eac349f2def593bc04802f300',0,'2021-12-06 12:16:21'),('f047bf376d4a56a8547f3256544989c9c1af7e44a2f2a99d94071c69375113ae25f7a82bc453ba2e','4580ca0c38f9de3adb1ddbdb7b58cd45ba717c837851f6baf57f4cfb1e918e187759b97dba0138ca',0,'2021-12-09 06:58:52'),('f0494d9d2039685f082b98d301eb8d36c6e0bb5de813a19193e0c9ec6896c0090c7c59db353f60b6','604c07b3904a16b86fc1de6b78939c028118c21ffb251fc6f83822c3b888d396eced85f5b25ef427',0,'2021-07-01 03:03:51'),('f06d291be5b57800cdc6ec05711643272bd41613b85c1f3680b15017c6224d7f6eabb7de1eed1c6c','35dde15c6176c5f717f47e7507bc8fa91b620225b996d5cc4f3b0ffc7f543dc01eedb713392589ad',0,'2021-08-03 07:33:16'),('f07a377fd8fe335e0ae2e6d4bc55da6bff57d6738e4dddcc66f31c6de4590fd11c50707581153687','395cd13bc226b7ce66dd567450182946a54662f6a2143eb308dfeb3e0004f09022b47dc62aee8997',0,'2021-10-26 09:34:05'),('f09d83821014fe7c2f3037be77d5be879445a92146abcc0bd5af9b1b1f0b2ccb03c7cbb7f11d7d72','055ce906f4c1e01b79f49e499b0c259640354a5ce52603ac496e8eb6faf6fb2c8b2c239e71fc6c03',0,'2021-06-29 10:12:39'),('f0e1c29716af6c16c384182abda709926b7d22d889ceaa037f5db1dc460b72612c7eeda24e2e5b6f','5be5b066f167de3995c04e9cce269bb6982454d5a5cb92fd5976cccb39613e104a1513d1e9eb6637',0,'2021-12-21 17:04:06'),('f16b3acb6d007e11cb8bdf3dd76e4efbdf535ad1f36e12c5d16adbcc4fbd8c10ee2f0d23b22958b6','d5335705c9d50a80cf0a44dbfdfc568d81e91a6431d5f426beb36e24a0bbde47d92ab048ece0c071',0,'2021-10-14 05:23:42'),('f16f66c6eda4f55e5245c7bd31347a9780a6019ef2b05ef6f0e6c7c0b7ce30ed03ecab50e817a599','85780ba8132f3e5e809087a19c31dbc118d9c71a373be97eb44db310fb6c7bf30eb4c57080dcf646',0,'2021-07-04 14:11:03'),('f17325d52baf2810c200933fda443ecdc4e35f7d459818e7fe54f4641e4d337a2a3f4e2ba4685911','5531dd7f7d62ed090d228c823e2902e2e44296e89b08de38cd41f6790b14ad4b28bc269c2a07366d',0,'2021-12-19 15:38:46'),('f1867c6cbd25020f40b7b6c6d98e1d4aa5c4c7c8f9bd560e53b5012d329cb578791001caf597560b','db11177ecd67d2a46906188187e51fa1e804ea8c384d13c835012b697e0b12f1689086f7d6db2d5f',0,'2021-07-01 06:31:41'),('f1bca6546b60f3294bbf8a12a6379935cd6b58f324357a470804f9cc6c5ba4da7b5fd4481180832d','ce2d65209d67c89d20a99afb84cd0efb50377df64764c31689bc523d648826bbf5529f9f5e97d54f',0,'2021-08-26 05:05:22'),('f1d4d842166c1ccf8e62eb7c40b478d6b1c921ad0445a977eea5f3336781a4b326e71a10ecb58f73','a1ecfaeb35d5bd6ff34994755334045f95d97e69adc800b864f0de8670f54a02c1aa69fff53777cd',0,'2021-06-29 08:44:25'),('f2387f3a2d14098e06fbdf203b035dfc98a89314c64b060f32b63d9ee0fb8ea037ed1bbb5e425c97','ab2a8618aa6c183b68f38183e7a302ad0a98ad4a27574a0ae86622316b9406a2096b41c681056dc0',0,'2022-01-12 04:42:39'),('f2420d8df26de88f1332966bb77c04f3eff32ba54ac3fb0e8073000b160be7e83d2204cda285a3c3','4aa69c12c8559a523ca45672971bf66443c133f0ec52de4bb34ae4d5bdfcbb59326108796f69a4d1',0,'2021-12-03 08:20:13'),('f248b11b7c9eb439149e65e2ec5e346f15bf0e3041e6aea34a305c36111b177bd37b3b3e0bffd153','56dad0690092ba57197d6867c00f12eeb1de8dd13874d8911b9f34c42da38833b535964d77c8efc0',0,'2021-12-21 16:51:33'),('f257c54971353981bcf703d178c0cacb80e7211ac06b0b59f852a855761e3a95b8068f1499fde1b3','00d6c892c70274e90b981517311d449fa43467c11e7f42262418fbc66547dd7083acaf420f7981dd',0,'2021-06-29 08:48:57'),('f262e798201dc244567f9b4b9e64a942a8f5fb50d85b317031231c646c3fbed344ea7ac8146c3fd9','1f2d96e4be9e32c379dd229ba2ddf1f32ba8899d2ed672c7ee8863f487b33e299a555fa8d8570480',0,'2021-08-01 18:13:55'),('f29aab0c05f3a7159270e5b55575ec3d625de844e20f3ea9a6b9fbc2837d5e8e478a47e51590da1b','be6d25f7a0a076ee8b9e197b84ddb073235f15005b9c08d32b2ba5fe9f9de4809a35e0c2c19bed3d',0,'2022-01-16 09:01:25'),('f307f98398eae6694d3eb68524f5371aa53559225a112a03063109c2895c05f6ae48a84ab96cc187','06737c4649229ebd3b85bd61d61518d35fa716bc3d8d12c47d98c48b7fa8144a26d10170a87ce8bf',0,'2021-12-21 16:53:37'),('f3402ce3376db211257ab1c2990d13f2024c45b43d68b860d4c8cb4fd162b373ba57d380192651e2','50d0309ba2c07b36bb43d31a7287b428aea7ae22c3a269a4e82260cfa8ae8fa72db4183eefa1aa5a',0,'2021-12-08 09:25:09'),('f3794a37a1026da46acb88c27294202f8a2d2a60696669361897cf89771fa50e398c3e50e06ebbec','37c394f0ff3a399c9c83e5edb7acdfdbb9793dc26b194522783169818005e9dbff8e79afb922a149',0,'2021-06-29 07:42:26'),('f3b76cfa737016c89b3d1d5a869fb07c2705e6c07323ff60eab0ffabc0279859f95df1b7e50f1828','6a9823a2260658271f8482a8b669f9202b4d7df300d185c12cf57478c581c8d24895fc94b81fca5b',0,'2021-12-21 13:41:33'),('f3de4da5815ef52e83d1cf61fba30f3eaa58413fbf151ad7c8b6d8cf61566410c805f64dabba6ffe','f902a732b6f9b7191c2124edd10ff290a1f313c70cd9313f24d1359863b64c28c53d6be4ad5ed504',0,'2021-06-10 10:42:57'),('f4376885b16978fc359e956e1df40be8e3f05c2dd83093de85817825192c4cb668e8ea01e53e17f8','0cccd2a740e08fb1e191030ee5e31aa0bd6c34047e20b842690adb2ddf1d46cfc61d5c1cee2d73bb',0,'2021-12-03 06:47:28'),('f478adcb3137f2aa21be5f346b2bc06768117d65140ead2b0fae493278b755cc3b784aaf39679c5f','acd778d96e520e09cf09287d991e28f0443b2d1ec451286b80408ab1999ea84b71f54ba610add098',0,'2021-07-04 14:10:14'),('f4cd51c9b78f74c3ad22568ade7b296e3a6b2cc888246b158cb0949bdecf1447e22916343f3dbf7c','e11e0bd3368c1d3cbf2a9c11663f9fe4bde7e1e9f7d052af3c3039d0b1b87b7cd05ffb930f669343',0,'2021-12-18 08:57:43'),('f4d547d87a38aa3a4db8f14ea13e28e2479ecb0cec879e2bcabe9592beeb770f24a3dfcdd75c8302','262a788f4e23ea38d2fc76a2b600d9394b916662f53de8ef9f65419546aec60a5123bad9fa8018dd',0,'2021-12-06 10:13:09'),('f51c3fbd4732b6fb9e5aa4bd3c87826e7f3c70d6ee4e49e4a0266a863038b2bcfbc71e3b674c6e17','28834c3849b90fa2bc44dabc2dfab7eeb7f3aeb907738baa4c8abd703124d383cdd5586ebe769a98',0,'2021-12-31 07:09:43'),('f53dc738fa77de1a36ff799d6d526812143787479dcba0b7a8b4ff9a48b6044d0a6da31a5b249b74','c194b65a75bf698e8876fbabe94c257d96ef9fa11ec14a71355f382fd941fec7962b20a27377722e',0,'2021-12-21 16:53:35'),('f598d472dd26387b9b53d8cb2566ccc9d18dcf08747454c671fedcc9d1ea16b67e30ae8495e950c9','213a3b5c8ab23a600202c0d49f53e491920b1e79731dc2fb319e2eb270095ad9bc51c1feb157de5c',0,'2021-12-22 09:32:36'),('f6023dd01867aacf05d0f400053911745d1382f82a3602ab0700c193c7147fa08a4816b5a199380b','a6ae0101b8b448dc02ff69bfd7bb55507ae77a72ea849e67a473c64ad15186066b8cb6656145fca8',0,'2021-06-29 07:42:56'),('f676070ae4d260e4473096d1331cb4e046fc0b617dff7d6854af3b290181d1a195fc617036f07251','328bb4729e4c71562fb915738e11582c4e0e6c1fc158c779d010848390c3b72faa375a118d2b199b',0,'2021-06-29 08:35:48'),('f6e8fb26fd4050855922c2f1a11646b9ea3f9547ae4e126ed21140b4e97aff100f52df09df24625c','21150e0a806e6384b2a2076db4b2d36fc1342e6cb436b56b0e83a900524da5f66f36784fff78565b',0,'2021-12-03 06:37:05'),('f70c0603ad6af6be36fb17109430d7fc71f30e07a9d466c54fd3a47f8146fc8d839d42a8b6f0c618','0feb75099d1ee06b3668904b24c85f73fe7dd2144926c745ba8efa196c6bf335884988e45c41b286',0,'2021-12-16 06:22:26'),('f83dfa901670d12f4b70ce517a3030e68c2d30a948713450735cb84055c9b648ee27c1b7c0a02f22','9511840fb369a5bb9149578a6e9dbfbe51496d142e0d78678b4659551403abdea89607fb609894f8',0,'2021-06-29 08:23:16'),('f83e03295b57f021aaccff9da21d4e68f16059070a7ca930a6c8d9c3919d03e210ccad8545da5b07','36501ebc8cbb4e4d0b6dcff68eba6d07f21b479cb9d279f7f5fe3d3e3facf1087d1cba1655323fe7',0,'2021-10-07 08:27:14'),('f8421f3ced3c7865be882ae48916327b7763b5f8cc631d165492ec6d5426a7a23604577f8394bf90','667096e94ca1707d25459259ecb6cb42dabf1656fcc83d6f4176b12172cb11032708cf48f05a2238',0,'2021-12-03 08:34:16'),('f850cda2f90a7d1e19d0de45d437029b67f669e362f9695ba703896b5b1217185866c9b312443469','5a4cbf4f07c10637f21aee25e717e451d330d32c93fb2c73e680c70d40e2a126308fb77f345ac3a8',0,'2021-12-21 16:48:32'),('f8753de76d20537bfcaf87db5c9692fc3e15a408bb45e30721a9885fee4043f96e42d8cf2f7414bc','ef33f46c3d6b5a027de21f11836f8837778c472dcfe91df756d453ceac370cd57dd37b035ea9fc84',0,'2021-12-20 10:59:31'),('f8c75f731eae86f7c2838c6dc5e60a5ad45b2bf8445e4832f39c86d4eb21cc40a384280dd56ceb30','bbbee91fd184ca61413375a2900aa6a1306e3256a4b10dacd18e65224dcbf3c4eec9d8a1f7b4b59d',0,'2021-12-03 06:48:00'),('f92ece2f3604b4b3327d5788b934fe29ee329f9264968b9d771931a16e9a3526b58249c4639424aa','93bb062b8e5d4bfba69fd9d0b62a1277e576c7bd187453d6295890d88baf35f78ca3fe57fffa21e4',0,'2021-06-29 08:28:12'),('f9520beab39cc88a0cc1b42c148eda793869b499007e4c614daf057ea8cd2584a729fbd4f518a5a5','7af6a33d7cc532537e8a196276cded35c0565aaf925bb7ae27efd41917bfc572cd89c49e3f399ea2',0,'2021-12-07 09:22:55'),('f995d86b4b619e43664d6b1ee530a2ae808258c3c553f77f48f021eaf6bf73c0210beb8f6872c718','bf5812166bafb4f79e4a7d8fd51f55914d0546dac08619635d2e4245115ef946e9f0e1590a921ffa',0,'2021-06-29 10:13:06'),('f9cfcb1c311eaa0c85ac506a43b92630242391cc89133bdaa1643e56e679ff35c028f04c845bcf13','d786b77535f172318a6fbdf5f8584936de0ad41f57be5583bdc913055b66b4428daa2ae1dc03c00a',0,'2021-06-29 07:51:45'),('fa003cb152d6fdd8138c847b2c35a7aa81ed03f4142cd3d923a8e3ec7279fbcdee83883379e736da','6196312326d31de34afa342aeb613ff9efcb96a1c6ccb8fd12aab3aa01799dbf5d6f329f7a86465d',0,'2021-06-11 06:43:05'),('fbc18748c9202611f1dcfcba8b6095c98a63b3641ada939500af6e4a2a3db9ae0ca7de3fed58fbc2','088249a3df21ff4e0fa392c0fce50f5d4f1cecaf9959eed8f084af469c27df426affaaaecb1f98dc',0,'2021-07-03 10:35:36'),('fbfce93e1bd91cd9fd78b52f582ec81e347bdd66f99c9f3e2de1cd1d53c260da3ebff167d0a188a1','02193c5e1bc2e5de215b1d0e2907bbb534b17549faf3af8b876ad9ef0f33bb2f25ac6d2cbc2cf5bf',0,'2021-06-29 13:34:18'),('fbfd69c09ed7b40a29f445d3cf07786ed11f010af124b349b51e3bd28dedcb6b44224f7d865546bd','5da9eb4bc967a84e6a7c490c25e678545a7b0fb2200d7fdcb238ebbc1e73c754f8ca757976d3f23f',0,'2021-12-06 09:56:07'),('fc039ecb7f2b0c0a25a63e8f409dc3c5c50d04685c4704f70e1763281433f86cc1f8943ba4ba66c2','0720160e152a438e6d872a659cbb818ebfe3b6967e2b79ea91bf28e193694d78f963694965b585e7',0,'2021-07-17 08:47:02'),('fc292944dac7963067c36478a719bced06c0875ecb8b3cc1f309a9cde9594bc85d713aac2f37e7e2','5fb70433f6ac4bbbeaae4f141a725552865b1b7ba9ba454f915c144c4a54866b4ccf0f80c65fc296',0,'2021-06-29 07:18:23'),('fc3d2e3cd1568aff09fc53dbb5afaabd10225d1c74518b6a7b4efc652e1974ec062a926ed0816a98','bc1f9cd890da04a6dfd94db7a9cc0d5468284f458b49a4141df5778961af09f44000cac7092b7fdb',0,'2021-11-14 09:42:56'),('fc8ca402b216ed3a851ea4bcfa4a3202affdd749bb8d8709548e4e19df9dcaa5fffbbe4abad31249','a14c29274392004c277a9fcd88bfd75317f3698100c07e8eaf8b696146ec247b44342ad103d69ac9',0,'2021-12-30 15:37:55'),('fc8f3b2b6ea5fb8319c91c56480f2168dedbbcbfcb8347581b959e950435c32de3edc7296fdf3838','2dc35f34b0e9856d648d6cfcc73ff99624c1e096dfa2603623f2f3bf2cd08479dd7c402791b8b2d5',0,'2021-05-11 07:52:19'),('fcbb4cc8ca0f52c820f452ec4c4b03f3a2693b2e95d91c186e4cc9cbbad745cf28d35a1d37a200b8','ab0ed4c5c4adc00ab63bf202b7331c0c483ea402931297ea5bd40bdb3a1662b1b41505873417cd1c',0,'2021-10-21 02:58:36'),('fccc8e5319b07a6dd453eaef12cf6621706ec932c0b0ef7569162bc175bf414b948554fad6190b27','d04cfc7bf64f63e60a66333fd9fd15a1a3941d797546cbfd8cfd134bb62ee6b219a6fd8e535c6430',0,'2021-12-03 07:24:34'),('fcfa829c89b7134300b230f56d24c81a9f317fdf8d588f0c15c27d220865ead3454f2f534c598544','dc64fcb3522dadf61b27efde5f2aa8a587f17d091e07feb6740d825e3806212fcf8b0df46bcc68d3',0,'2021-09-14 02:05:59'),('fd0e550443fe3bb9f747093d848b8535a965fb7b36532e5ab8e9d4b3b58b58546f92d461080515a1','781ed466acf47b2c15974f347ac4aee077a7ede4054542326c0929e10b5ab0b7dd3aeb6d7cbe3877',0,'2021-07-03 09:30:15'),('fda823fcb4c883a0b04ba1c6e1525439113bb80c57bf234b2e5438763bc0432b063f57c5b2d56a1b','40db69a292a379c3cc1fcf7ed61187518b8559b5dfeb7719e0f95004cd96d889f233dca555f5a388',0,'2021-11-14 09:45:05'),('fdeb136f044b96907d46389fc59979c9333c67ca990e78f08f65da5693320c5f3d5bdd2777c64ba2','55afcc9944889bb6e24d51a3dc33b29e59f18979b89a787ec0062016778c2d6be45371b69abbb23d',0,'2021-07-01 06:04:38'),('fe65a1ce13d3628db57051e2347c0ea40d1b9c4b5b7797e15505a08504f0353b87256e8b8c4de1fb','cbcab8f50e0a747b6d8b8b57a38b8e07dacc6a87a34875b6fa90cd72c4e1471fe67ec8b5aa7e6154',0,'2021-08-21 02:03:37'),('fed496c7081663e040e9c988c7383ec75ddda2f1f8db35c74522b6bbe8ce458f34daf08f5eea9c31','fab407e1a005387c31d37ffbd5e6927a742271b0d3de0a15c6d34f828a6c219237908e5cd2a6b5cd',0,'2021-08-18 15:25:35'),('fef46db6edca79ff745aff1e89f2401d841a45f1fc9c3de6d43ec1b76be855242ad4bd933ff6b945','a22187fecb7138d8c0c9685b525be08799d8954e58ffd6eeab99f3802459b17eeaa75334d69bb0f6',0,'2021-12-18 14:51:11'),('ff08172a4b3b42d6d514cb7bfbb2f6ea89f6e995298fb8bda1abad5c9e2cafac40dbc340a6d182bc','0a89a4080a508851b8d85f83484beaec259d24bc7be465b17f2622fc00cc9460a472d63768b651bd',0,'2021-12-06 10:14:41'),('ff09ad0d3fa8d38a09bdd99acb9bdb1345aec236daa76c30608a9cdc495efa80da5666ec66c78d71','d4f41b0bfd112e55a68397804c08642fb448bbb4a04946d928e58715b88546682fe818343a56f042',0,'2021-08-18 15:38:17'),('ff13f69a0e60a6fb0348a4946784ef3528e79f867ca3e975c1265e49520b0d50ccd86ef98edfd0b7','0873230eebde18a4c44f6a65416d7419d6a93b9a250ded8764fe2a617811eb2471bf9dcc07b17564',0,'2021-06-29 08:50:58'),('ff9293849533e9016baada68f5aba1d1f14a8883efd6311e5d1a58db1e5e44e50c2cb8b9fa005358','de1dde966586f41c2227b643c0af0aedcf19c88edfb9f14e9caf1f140dca3bdfcb69e9f5f05779d4',0,'2021-07-04 13:50:17'),('ff97b6420b34d7541b4ad12edbe47f1faa141e7db70a8cf4f57aeeb1cd7f236eeee4701a36833615','03305fc167c50669405c75acde2a648cdbb671fa4e905bcd43b2d6aca81a71342a718cd1a493bb24',0,'2021-06-29 07:21:20'),('ff9fafacf18036f8dbbc70bb1c2919c111785a483e3a530a1245a503be7ee221aafb440b6b0c6896','14c948d11045810de30e09f81c4b397e11da072b7dd6f87e9f6e50f94a71ccb5208e45f6a0dcc24c',0,'2021-12-03 07:47:21'),('ffab59275394a3d476a2b8d293dd7c0a628b1db669f96d8fada7b80e75e9e813b7d306bdf2fde1bc','33091a7d1ddba3a19a8f6b00908cc13e212b7cfeebc0d303cbd3a250929bae0005465be3c076b29f',0,'2021-06-30 09:58:19');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `project_id` bigint NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1),(2),(3);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_categories`
--

DROP TABLE IF EXISTS `project_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_categories` (
  `project_category_id` bigint NOT NULL AUTO_INCREMENT,
  `project_category` varchar(100) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`project_category_id`),
  KEY `FK_ref_project_categories_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_project_categories_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_categories`
--

LOCK TABLES `project_categories` WRITE;
/*!40000 ALTER TABLE `project_categories` DISABLE KEYS */;
INSERT INTO `project_categories` VALUES (1,'Floating',1,'2021-10-20 18:38:27','2021-12-06 06:26:15',1),(2,'Rooftop',1,'2021-10-20 18:38:42','2021-10-29 16:14:22',0),(3,'Land (Ground Mounted)',1,'2021-10-21 10:44:08','2021-10-29 16:14:22',1);
/*!40000 ALTER TABLE `project_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_contractors`
--

DROP TABLE IF EXISTS `project_contractors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_contractors` (
  `contractor_id` bigint NOT NULL AUTO_INCREMENT,
  `contractor_code` varchar(50) DEFAULT NULL,
  `contractor_name` varchar(100) DEFAULT NULL,
  `contractor_contact_person` varchar(100) DEFAULT NULL,
  `contractor_contact_number` varchar(100) DEFAULT NULL,
  `contractor_contact_email` varchar(250) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`contractor_id`),
  KEY `FK_ref_project_contractors_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_project_contractors_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_contractors`
--

LOCK TABLES `project_contractors` WRITE;
/*!40000 ALTER TABLE `project_contractors` DISABLE KEYS */;
INSERT INTO `project_contractors` VALUES (1,'imttech','imttech','imttech','imttech','wee.kwong.how@gmail.com',2,'2022-01-06 06:17:28','2022-01-06 06:17:28',1);
/*!40000 ALTER TABLE `project_contractors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_developers`
--

DROP TABLE IF EXISTS `project_developers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_developers` (
  `developer_id` bigint NOT NULL AUTO_INCREMENT,
  `developer_code` varchar(50) DEFAULT NULL,
  `developer_name` varchar(100) DEFAULT NULL,
  `developer_contact_person` varchar(100) DEFAULT NULL,
  `developer_contact_number` varchar(100) DEFAULT NULL,
  `developer_contact_email` varchar(250) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`developer_id`),
  KEY `FK_ref_project_developers_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_project_developers_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_developers`
--

LOCK TABLES `project_developers` WRITE;
/*!40000 ALTER TABLE `project_developers` DISABLE KEYS */;
INSERT INTO `project_developers` VALUES (1,'Sembcorp Singapore','Sembcorp Singapore','111','111','111@gamil.com',2,'2022-01-06 06:11:56','2022-01-06 06:11:56',1);
/*!40000 ALTER TABLE `project_developers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_document_approvals`
--

DROP TABLE IF EXISTS `project_document_approvals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_document_approvals` (
  `project_document_approval_id` bigint NOT NULL AUTO_INCREMENT,
  `project_document_id` bigint NOT NULL,
  `project_document_recurring_id` bigint DEFAULT NULL,
  `project_document_upload_id` bigint NOT NULL,
  `for_status` bigint DEFAULT NULL,
  `approval_role` bigint DEFAULT NULL,
  `approval_by` bigint DEFAULT NULL,
  `code_by_approval` tinyint DEFAULT NULL,
  `approval_comments` varchar(1000) DEFAULT NULL,
  `approval_attachments` varchar(1000) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_document_approval_id`),
  KEY `FK_ref_project_document_approvals_project_document_id_idx` (`project_document_id`),
  KEY `FK_ref_project_document_approvals_project_document_recurrin_idx` (`project_document_recurring_id`),
  KEY `FK_ref_project_document_approvals_for_status_idx` (`for_status`),
  KEY `FK_ref_project_document_approvals_created_by_idx` (`created_by`),
  KEY `FK_ref_project_document_approvals_approval_by_idx` (`approval_by`),
  KEY `FK_ref_project_document_approvals_project_document_approval_idx` (`project_document_upload_id`),
  KEY `FK_ref_project_document_approvals_approval_role_idx` (`approval_role`),
  CONSTRAINT `FK_ref_project_document_approvals_approval_by` FOREIGN KEY (`approval_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_document_approvals_approval_role` FOREIGN KEY (`approval_role`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `FK_ref_project_document_approvals_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_document_approvals_for_status` FOREIGN KEY (`for_status`) REFERENCES `status_documents` (`status_id`),
  CONSTRAINT `FK_ref_project_document_approvals_project_document_id` FOREIGN KEY (`project_document_id`) REFERENCES `project_documents` (`project_document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_project_document_approvals_project_document_recurring_id` FOREIGN KEY (`project_document_recurring_id`) REFERENCES `project_document_recurrings` (`project_document_recurring_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_project_document_approvals_project_document_upload_id` FOREIGN KEY (`project_document_upload_id`) REFERENCES `project_document_uploads` (`project_document_upload_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_document_approvals`
--

LOCK TABLES `project_document_approvals` WRITE;
/*!40000 ALTER TABLE `project_document_approvals` DISABLE KEYS */;
INSERT INTO `project_document_approvals` VALUES (1,9,NULL,3,1,8,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(2,9,NULL,3,1,9,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(3,9,NULL,3,1,10,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(4,9,NULL,3,1,15,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(5,9,NULL,3,1,14,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(6,9,NULL,3,1,16,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(7,9,NULL,3,1,18,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(8,9,NULL,3,1,19,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(9,9,NULL,3,1,17,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(10,9,NULL,3,1,3,NULL,NULL,NULL,NULL,5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(11,10,7,4,1,8,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(12,10,7,4,1,9,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(13,10,7,4,1,10,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(14,10,7,4,1,15,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(15,10,7,4,1,14,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(16,10,7,4,1,16,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(17,10,7,4,1,18,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(18,10,7,4,1,19,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(19,10,7,4,1,17,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(20,10,7,4,1,3,NULL,NULL,NULL,NULL,5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(21,11,NULL,7,1,3,2,3,'comment','21_1643015163_TH-Implementation Plan.xlsx',2,'2022-01-24 17:05:29','2022-01-24 09:06:03'),(22,11,NULL,8,1,3,2,1,NULL,NULL,2,'2022-01-24 17:06:18','2022-01-24 09:06:26'),(23,11,NULL,9,2,3,2,2,'ok but need update',NULL,2,'2022-01-24 17:06:55','2022-01-24 09:07:18'),(24,11,NULL,10,3,3,2,2,'comment',NULL,2,'2022-01-24 17:07:27','2022-01-24 09:07:41'),(25,11,NULL,11,3,3,2,1,NULL,NULL,2,'2022-01-24 17:07:55','2022-01-24 09:07:59'),(26,12,NULL,12,1,3,2,3,'comment','26_1643015306_TH-Implementation Plan.xlsx',2,'2022-01-24 17:08:15','2022-01-24 09:08:26'),(27,12,NULL,13,1,3,2,1,NULL,NULL,2,'2022-01-24 17:08:44','2022-01-24 09:08:49'),(28,12,NULL,14,2,3,2,2,'ok but better check again',NULL,2,'2022-01-24 17:09:11','2022-01-24 09:09:31'),(29,12,NULL,15,2,3,2,1,NULL,NULL,2,'2022-01-24 17:09:41','2022-01-24 09:09:45');
/*!40000 ALTER TABLE `project_document_approvals` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_approval_update` AFTER UPDATE ON `project_document_approvals` FOR EACH ROW BEGIN

	SET @project_id = null, @approval_count_check = 0, @null_check = 0;
 
   Select project_id, document_type_id, document_mandatory, asb_flag
	into @project_id, @document_type_id, @document_mandatory, @asb_flag
	from project_documents where project_document_id = OLD.project_document_id;
    
   SET @null_check = (Select count(*) from project_document_approvals where project_document_upload_id = OLD.project_document_upload_id and code_by_approval is null);
   
   SET @approval_count_check = (Select count(*) from project_document_approvals where project_document_upload_id = OLD.project_document_upload_id and code_by_approval in ('2','3','4'));
	 
	IF ( @document_type_id = 1 ) THEN
	
		IF (( OLD.for_status = '3' AND @asb_flag = 1 AND @null_check = 0 AND @approval_count_check = 0 ) OR ( OLD.for_status = '2' AND @asb_flag = 0 AND @null_check = 0 AND @approval_count_check = 0 ))THEN
		
			IF ( @document_mandatory = 1 ) THEN
	    
				SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);
						  
				SET @project_level_completed_document = @project_level_completed_document + 1;
						 
				Update projects
				SET
				project_level_completed_document = @project_level_completed_document
				Where project_id = @project_id;
			
			END IF;
			
			UPDATE project_documents
			SET 
			completed_flag = 1
			WHERE project_document_id = OLD.project_document_id;
				 
		END IF;
	
	ELSE
	
		IF ( @null_check = 0 AND @approval_count_check = 0 ) THEN
	    
	    	IF ( @document_mandatory = 1 ) THEN
	    	
				SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);
						  
				SET @project_level_completed_document = @project_level_completed_document + 1;
						 
				Update projects
				SET
				project_level_completed_document = @project_level_completed_document
				Where project_id = @project_id;
			
			END IF;			
			
			UPDATE project_documents
			SET 
			completed_flag = 1
			WHERE project_document_id = OLD.project_document_id;
			
			
			UPDATE project_document_recurrings
			SET 
			completed_flag = 1
			WHERE project_document_recurring_id = OLD.project_document_recurring_id;
				 
		END IF;
	
	END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_document_recurrings`
--

DROP TABLE IF EXISTS `project_document_recurrings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_document_recurrings` (
  `project_document_recurring_id` bigint NOT NULL AUTO_INCREMENT,
  `project_document_id` bigint NOT NULL,
  `document_recurring_date` datetime NOT NULL,
  `current_status` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `document_mandatory` tinyint(1) DEFAULT '1',
  `completed_flag` varchar(45) DEFAULT '0',
  PRIMARY KEY (`project_document_recurring_id`),
  KEY `F_ref_project_document_recurrings_created_by_idx` (`created_by`),
  KEY `FF_ref_project_document_recurrings_project_document_id_idx` (`project_document_id`),
  KEY `FK_ref_project_document_recurrings_status_id_idx` (`current_status`),
  CONSTRAINT `FF_ref_project_document_recurrings_project_document_id` FOREIGN KEY (`project_document_id`) REFERENCES `project_documents` (`project_document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_project_document_recurrings_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_document_recurrings_status_id` FOREIGN KEY (`current_status`) REFERENCES `status_documents` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_document_recurrings`
--

LOCK TABLES `project_document_recurrings` WRITE;
/*!40000 ALTER TABLE `project_document_recurrings` DISABLE KEYS */;
INSERT INTO `project_document_recurrings` VALUES (7,10,'2022-01-21 00:00:00',1,5,'2022-01-21 15:41:56','2022-01-21 15:41:56',1,'0'),(8,10,'2022-01-22 00:00:00',1,5,'2022-01-21 15:41:56','2022-01-21 15:41:56',1,'0'),(9,10,'2022-01-23 00:00:00',1,5,'2022-01-21 15:41:56','2022-01-21 15:41:56',1,'0');
/*!40000 ALTER TABLE `project_document_recurrings` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_recurring_insert` AFTER INSERT ON `project_document_recurrings` FOR EACH ROW BEGIN

	SET @project_id = null, @document_category_id = null , @document_type_id = null;
 
    Select project_id, document_category_id, document_type_id, document_mandatory
	 into @project_id, @document_category_id, @document_type_id, @document_mandatory
	 from project_documents where project_document_id = NEW.project_document_id;
 
    SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);
    
    IF @document_category_id = 1 THEN
    
		IF (@document_type_id = 2 AND NEW.document_mandatory = 1) THEN
        
			SET @project_level_total_document = @project_level_total_document + 1;
        
		END IF;
        
      Update projects
		SET
		project_level_total_document = @project_level_total_document
		Where project_id = @project_id;
         
     END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_recurrings_update` AFTER UPDATE ON `project_document_recurrings` FOR EACH ROW BEGIN

	SET @project_id = null, @document_category_id = null , @document_type_id = null;
 
   Select project_id, document_category_id, document_type_id, document_mandatory
	into @project_id, @document_category_id, @document_type_id, @document_mandatory
	from project_documents where project_document_id = NEW.project_document_id;
 
   SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);
    
   IF ( OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
        
		SET @project_level_total_document = @project_level_total_document - 1; 
        
      Update projects
		SET
		project_level_total_document = @project_level_total_document
		Where project_id = @project_id;
         
   END IF;
   
   IF ( OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
        
		SET @project_level_total_document = @project_level_total_document + 1; 
        
      Update projects
		SET
		project_level_total_document = @project_level_total_document
		Where project_id = @project_id;
         
   END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_recurring_delete` AFTER DELETE ON `project_document_recurrings` FOR EACH ROW BEGIN
 
	SET @project_id = null, @document_category_id = null , @document_type_id = null;
 
   Select project_id, document_category_id, document_type_id, document_mandatory, completed_flag
	into @project_id, @document_category_id, @document_type_id, @document_mandatory, @completed_flag
	from project_documents where project_document_id = OLD.project_document_id;
    
		IF ( OLD.document_mandatory = 1 )THEN
	 
			SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);
			  
			SET @project_level_total_document = @project_level_total_document - 1;
			
			SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);
			
			IF ( @completed_flag = 1 ) THEN
					  
				SET @project_level_completed_document = @project_level_completed_document - 1;
			
			END IF;
				 
			Update projects
			SET
			project_level_total_document = @project_level_total_document,
			project_level_completed_document = @project_level_completed_document
			Where project_id = @project_id;
		
		END IF;

  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_document_template_details`
--

DROP TABLE IF EXISTS `project_document_template_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_document_template_details` (
  `project_document_template_detail_id` bigint NOT NULL AUTO_INCREMENT,
  `project_document_template_id` bigint DEFAULT NULL,
  `project_document_template_title` varchar(100) DEFAULT NULL,
  `project_document_template_mandatory` tinyint(1) DEFAULT '1',
  `document_category_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `recurring_interval_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `req_approval_project_owner` tinyint(1) DEFAULT '0',
  `req_approval_project_manager` tinyint(1) DEFAULT '0',
  `req_approval_project_engineer` tinyint(1) DEFAULT '0',
  `req_approval_engineer` tinyint(1) DEFAULT '0',
  `req_approval_qa_qc` tinyint(1) DEFAULT '0',
  `req_approval_safety` tinyint(1) DEFAULT '0',
  `req_approval_onm` tinyint DEFAULT '0',
  `req_approval_planner` tinyint(1) DEFAULT '0',
  `req_approval_purchasing` tinyint(1) DEFAULT '0',
  `req_approval_admin` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`project_document_template_detail_id`),
  KEY `FK_ref_project_docment_template_details_created_by_idx` (`created_by`),
  KEY `FK_ref_project_document_template_details_document_type_id_idx` (`document_type_id`),
  KEY `FK_ref_project_document_template_details_project_document_t_idx` (`project_document_template_id`) /*!80000 INVISIBLE */,
  KEY `FK_ref_project_document_template_details_document_category__idx` (`document_category_id`),
  KEY `FK_ref_project_document_template_details_recurring_interval_idx` (`recurring_interval_id`),
  CONSTRAINT `FK_ref_project_document_template_details_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_document_template_details_document_category_id` FOREIGN KEY (`document_category_id`) REFERENCES `project_categories` (`project_category_id`),
  CONSTRAINT `FK_ref_project_document_template_details_document_type_id` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`document_type_id`),
  CONSTRAINT `FK_ref_project_document_template_details_recurring_interval_id` FOREIGN KEY (`recurring_interval_id`) REFERENCES `recurring_intervals` (`recurring_interval_id`),
  CONSTRAINT `FK_ref_project_document_template_details_template_id` FOREIGN KEY (`project_document_template_id`) REFERENCES `project_document_templates` (`project_document_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_document_template_details`
--

LOCK TABLES `project_document_template_details` WRITE;
/*!40000 ALTER TABLE `project_document_template_details` DISABLE KEYS */;
INSERT INTO `project_document_template_details` VALUES (1,1,'ifa-document',1,1,1,NULL,5,'2022-01-06 07:08:18','2022-01-06 15:18:38',1,1,1,1,1,1,1,1,1,1,1,NULL);
/*!40000 ALTER TABLE `project_document_template_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_template_details_insert` AFTER INSERT ON `project_document_template_details` FOR EACH ROW BEGIN
   
	Select document_details_no into @document_details_no from project_document_templates where project_document_template_id = NEW.project_document_template_id;
     
    SET @document_details_no = @document_details_no + 1;
     
    Update project_document_templates
    SET
    document_details_no = @document_details_no
    Where project_document_template_id = NEW.project_document_template_id;
        
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_template_details_delete` AFTER DELETE ON `project_document_template_details` FOR EACH ROW BEGIN
   
 	Select document_details_no into @document_details_no from project_document_templates where project_document_template_id = OLD.project_document_template_id;
     
     SET @document_details_no = @document_details_no - 1;
     
     Update project_document_templates
     SET
     document_details_no = @document_details_no
     Where project_document_template_id = OLD.project_document_template_id;
        
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_document_templates`
--

DROP TABLE IF EXISTS `project_document_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_document_templates` (
  `project_document_template_id` bigint NOT NULL AUTO_INCREMENT,
  `document_template_name` varchar(100) DEFAULT NULL,
  `document_details_no` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_document_template_id`),
  KEY `FK_ref_document_templates_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_project_document_templates_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_document_templates`
--

LOCK TABLES `project_document_templates` WRITE;
/*!40000 ALTER TABLE `project_document_templates` DISABLE KEYS */;
INSERT INTO `project_document_templates` VALUES (1,'project-document-template',1,5,'2022-01-06 07:08:05','2022-01-06 07:08:18');
/*!40000 ALTER TABLE `project_document_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_document_uploads`
--

DROP TABLE IF EXISTS `project_document_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_document_uploads` (
  `project_document_upload_id` bigint NOT NULL AUTO_INCREMENT,
  `project_document_id` bigint NOT NULL,
  `project_document_recurring_id` bigint DEFAULT NULL,
  `document_version` varchar(200) NOT NULL,
  `document_file` varchar(1000) NOT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_document_upload_id`),
  KEY `FK_ref_project_document_uploads_created_by_idx` (`created_by`),
  KEY `FK_ref_project_document_uploads_project_document_recurring__idx` (`project_document_recurring_id`),
  KEY `FK_ref_project_document_uploads_project_document_uploads_idx` (`project_document_id`),
  CONSTRAINT `FK_ref_project_document_uploads_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_document_uploads_project_document_recurring_id` FOREIGN KEY (`project_document_recurring_id`) REFERENCES `project_document_recurrings` (`project_document_recurring_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_project_document_uploads_project_document_uploads` FOREIGN KEY (`project_document_id`) REFERENCES `project_documents` (`project_document_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_document_uploads`
--

LOCK TABLES `project_document_uploads` WRITE;
/*!40000 ALTER TABLE `project_document_uploads` DISABLE KEYS */;
INSERT INTO `project_document_uploads` VALUES (1,8,NULL,'111','8_1642750466_TH-Implementation Plan.xlsx',5,'2022-01-21 15:34:26','2022-01-21 15:34:26'),(2,8,NULL,'222','8_1642750722_TH-Implementation Plan.xlsx',5,'2022-01-21 15:38:42','2022-01-21 15:38:42'),(3,9,NULL,'111','9_1642750889_TH-Implementation Plan.xlsx',5,'2022-01-21 15:41:29','2022-01-21 15:41:29'),(4,10,7,'11','10_1642750928_TH-Implementation Plan.xlsx',5,'2022-01-21 15:42:08','2022-01-21 15:42:08'),(5,8,NULL,'2333','8_1642750951_TH-Implementation Plan.xlsx',5,'2022-01-21 15:42:31','2022-01-21 15:42:31'),(6,8,NULL,'3333','8_1642751247_TH-Implementation Plan.xlsx',5,'2022-01-21 15:47:27','2022-01-21 15:47:27'),(7,11,NULL,'ifa-1','11_1643015129_TH-Implementation Plan.xlsx',2,'2022-01-24 17:05:29','2022-01-24 17:05:29'),(8,11,NULL,'ifa-2','11_1643015178_TH-Implementation Plan.xlsx',2,'2022-01-24 17:06:18','2022-01-24 17:06:18'),(9,11,NULL,'ifc-1','11_1643015215_TH-Implementation Plan.xlsx',2,'2022-01-24 17:06:55','2022-01-24 17:06:55'),(10,11,NULL,'asb-1','11_1643015247_TH-Implementation Plan.xlsx',2,'2022-01-24 17:07:27','2022-01-24 17:07:27'),(11,11,NULL,'asb-2','11_1643015275_TH-Implementation Plan.xlsx',2,'2022-01-24 17:07:55','2022-01-24 17:07:55'),(12,12,NULL,'ifa-1','12_1643015295_TH-Implementation Plan.xlsx',2,'2022-01-24 17:08:15','2022-01-24 17:08:15'),(13,12,NULL,'ifa-2','12_1643015324_TH-Implementation Plan.xlsx',2,'2022-01-24 17:08:44','2022-01-24 17:08:44'),(14,12,NULL,'ifc-1','12_1643015351_TH-Implementation Plan.xlsx',2,'2022-01-24 17:09:11','2022-01-24 17:09:11'),(15,12,NULL,'ifc-2','12_1643015381_TH-Implementation Plan.xlsx',2,'2022-01-24 17:09:41','2022-01-24 17:09:41');
/*!40000 ALTER TABLE `project_document_uploads` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_uploads_insert` AFTER INSERT ON `project_document_uploads` FOR EACH ROW BEGIN

	SELECT document_category_id, document_mandatory, project_id, uploaded_flag
	INTO @document_category_id, @document_mandatory, @project_id, @uploaded_flag
	FROM project_documents WHERE project_document_id = NEW.project_document_id;

	IF ( @document_category_id = 2 ) THEN
	
		IF ( @document_mandatory = 1 AND @uploaded_flag = 0 ) THEN 
		
			SET @project_level_completed_document = (SELECT project_level_completed_document FROM projects WHERE project_id = @project_id);
			SET @project_level_completed_document = @project_level_completed_document + 1;
			
			UPDATE projects
			SET 
			project_level_completed_document = @project_level_completed_document
			WHERE project_id = @project_id;
		
		END IF;
			
		UPDATE project_documents 
		SET 
		completed_flag = 1
		WHERE project_document_id = NEW.project_document_id;
		
	END IF;
	
	UPDATE project_documents
	SET
	uploaded_flag = 1
	WHERE project_document_id = NEW.project_document_id;
	

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_documents`
--

DROP TABLE IF EXISTS `project_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_documents` (
  `project_document_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `document_title` varchar(100) DEFAULT NULL,
  `document_mandatory` tinyint(1) DEFAULT '1',
  `assign_to_user` bigint DEFAULT NULL,
  `document_category_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `recurring_interval_id` bigint DEFAULT NULL,
  `recurring_start_date` datetime DEFAULT NULL,
  `recurring_end_date` datetime DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `req_approval_project_owner` tinyint(1) DEFAULT '0',
  `req_approval_project_manager` tinyint(1) DEFAULT '0',
  `req_approval_project_engineer` tinyint(1) DEFAULT '0',
  `req_approval_engineer` tinyint(1) DEFAULT '0',
  `req_approval_qa_qc` tinyint(1) DEFAULT '0',
  `req_approval_safety` tinyint(1) DEFAULT '0',
  `req_approval_onm` tinyint(1) DEFAULT '0',
  `req_approval_planner` tinyint(1) DEFAULT '0',
  `req_approval_purchasing` tinyint(1) DEFAULT '0',
  `req_approval_admin` tinyint(1) DEFAULT '0',
  `from_project_document_template_id` bigint DEFAULT NULL,
  `from_project_document_template_detail_id` bigint DEFAULT NULL,
  `completed_flag` tinyint DEFAULT '0',
  `uploaded_flag` tinyint DEFAULT '0',
  `asb_flag` tinyint DEFAULT '1',
  PRIMARY KEY (`project_document_id`),
  KEY `FK_ref_project_documents_created_by_idx` (`created_by`),
  KEY `FK_ref_project_documents_document_type_id_idx` (`document_type_id`),
  KEY `FK_ref_project_documents_project_id_idx` (`project_id`),
  KEY `FK_ref_project_documents_project_category_id_idx` (`document_category_id`),
  KEY `FK_ref_project_documents_recurring_interval_id_idx` (`recurring_interval_id`),
  KEY `FK_ref_project_documents_status_id_idx` (`status_id`),
  KEY `FK_ref_project_documents_assign_to_user_idx` (`assign_to_user`),
  CONSTRAINT `FK_ref_project_documents_assign_to_user` FOREIGN KEY (`assign_to_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_documents_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_documents_document_type_id` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`document_type_id`),
  CONSTRAINT `FK_ref_project_documents_project_category_id` FOREIGN KEY (`document_category_id`) REFERENCES `document_categories` (`document_category_id`),
  CONSTRAINT `FK_ref_project_documents_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`),
  CONSTRAINT `FK_ref_project_documents_recurring_interval_id` FOREIGN KEY (`recurring_interval_id`) REFERENCES `recurring_intervals` (`recurring_interval_id`),
  CONSTRAINT `FK_ref_project_documents_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_documents` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_documents`
--

LOCK TABLES `project_documents` WRITE;
/*!40000 ALTER TABLE `project_documents` DISABLE KEYS */;
INSERT INTO `project_documents` VALUES (8,1,'doc-4',1,5,2,1,NULL,NULL,NULL,NULL,5,'2022-01-21 15:33:55','2022-01-21 15:37:11',1,1,1,1,1,1,1,1,1,1,NULL,NULL,1,1,1),(9,1,'doc-5',1,5,1,1,NULL,NULL,NULL,1,5,'2022-01-21 15:41:19','2022-01-21 15:41:29',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,1),(10,1,'dco-3',1,5,1,2,1,'2022-01-21 00:00:00','2022-01-23 00:00:00',NULL,5,'2022-01-21 15:41:56','2022-01-21 15:42:08',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,1),(11,2,'ifa-document-asb-on',1,2,1,1,NULL,NULL,NULL,3,2,'2022-01-24 17:02:52','2022-01-24 17:07:59',0,0,0,0,0,0,0,0,0,1,1,1,1,1,1),(12,2,'ifa-document-asb-off',1,2,1,1,NULL,NULL,NULL,2,2,'2022-01-24 17:04:15','2022-01-24 17:09:45',0,0,0,0,0,0,0,0,0,1,NULL,NULL,1,1,0);
/*!40000 ALTER TABLE `project_documents` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_insert` AFTER INSERT ON `project_documents` FOR EACH ROW BEGIN
 
    SET @project_id = NEW.project_id;
 
    SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);
    
		IF ( NEW.document_type_id = 1 AND (NEW.document_category_id = 1 OR NEW.document_category_id = 2) AND NEW.document_mandatory = 1 )THEN
        
			SET @project_level_total_document = @project_level_total_document + 1;
	      
	      Update projects
			SET
			project_level_total_document = @project_level_total_document
			Where project_id = @project_id;
		
		END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_assign_after_insert` AFTER INSERT ON `project_documents` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
    VALUES (now(), NEW.assign_to_user, 'PROJECT DOCUMENT', NEW.project_id, NEW.project_document_id, NEW.document_title, 'projectDocumentEditFrProject');
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_update` AFTER UPDATE ON `project_documents` FOR EACH ROW BEGIN
 
   SET @project_id = NEW.project_id;
 
   SELECT project_level_total_document
   INTO @project_level_total_document
	FROM projects WHERE project_id = @project_id;
	
	# ALL COMPLETED WONT ALLOW UPDATE no need to worry about status = 3 for IFA-ONE-TIME, IFI = COMPLETED, IFA-RECURRING = COMPLETED
	# ANY CHANGE OF DOCUMENT CATEGORY OR TYPE need to DELETE all uploads
    
   # IFI to IFI and IFA-ONE-TIME to IFA-ONE-TIME
   IF ( (OLD.document_category_id = NEW.document_category_id) AND (OLD.document_type_id = NEW.document_type_id) AND OLD.document_type_id = 1) THEN
   	
   	IF ( OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document + 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
   	
   	IF ( OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document - 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
    
   END IF;
   
   # IFI to IFA-ONE-TIME
   IF ( (OLD.document_category_id = 2 AND NEW.document_category_id = 1) AND (OLD.document_type_id = NEW.document_type_id) AND OLD.document_type_id = 1) THEN
   	
   	IF ( OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document + 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
   	
   	IF ( OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document - 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
   	
   	DELETE FROM project_document_uploads WHERE project_document_id = OLD.project_document_id;
    
   END IF;
   
   # IFA-ONE-TIME to IFI
   IF ( (OLD.document_category_id = 1 AND NEW.document_category_id = 2) AND (OLD.document_type_id = NEW.document_type_id) AND OLD.document_type_id = 1) THEN
   	
   	IF ( OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document + 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
   	
   	IF ( OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document - 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
   	
   	DELETE FROM project_document_uploads WHERE project_document_id = OLD.project_document_id;
    
   END IF;
   
   # IFA-RECURRING to IFA-RECURRING and DATA START & END DATE DIFFERENT HANDLE by during recuring update trigger
  
   # IFA-RECURRING & IFI to IFA
   
   IF ( OLD.document_type_id = 2 AND NEW.document_type_id = 1 ) THEN
   
   	DELETE FROM project_document_uploads WHERE project_document_id = OLD.project_document_id;
   	
   	IF ( NEW.document_mandatory = 1 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document + 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
    
   END IF;
   
    # IFA-ONE-TIME & IFI to IFA-RECURRING
   IF ( OLD.document_type_id = 1 AND NEW.document_type_id = 2 ) THEN
   
   	DELETE FROM project_document_uploads WHERE project_document_id = OLD.project_document_id;
   	
   	IF ( OLD.document_mandatory = 1 ) THEN
   	
   		SET @project_level_total_document = @project_level_total_document - 1;
   		
   		UPDATE projects
			SET
			project_level_total_document = @project_level_total_document
			WHERE project_id = @project_id;
   	
   	END IF;
    
   END IF;

         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_document_assign_after_update` AFTER UPDATE ON `project_documents` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
		VALUES (now(), NEW.assign_to_user, 'PROJECT DOCUMENT', NEW.project_id, NEW.project_document_id, NEW.document_title, 'projectDocumentEditFrProject');
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_documents_deleted`
--

DROP TABLE IF EXISTS `project_documents_deleted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_documents_deleted` (
  `project_document_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `document_title` varchar(100) DEFAULT NULL,
  `document_mandatory` tinyint(1) DEFAULT '1',
  `assign_to_user` bigint DEFAULT NULL,
  `document_category_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `recurring_interval_id` bigint DEFAULT NULL,
  `recurring_start_date` datetime DEFAULT NULL,
  `recurring_end_date` datetime DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `req_approval_project_owner` tinyint(1) DEFAULT '0',
  `req_approval_project_manager` tinyint(1) DEFAULT '0',
  `req_approval_project_engineer` tinyint(1) DEFAULT '0',
  `req_approval_engineer` tinyint(1) DEFAULT '0',
  `req_approval_qa_qc` tinyint(1) DEFAULT '0',
  `req_approval_safety` tinyint(1) DEFAULT '0',
  `req_approval_onm` tinyint(1) DEFAULT '0',
  `req_approval_planner` tinyint(1) DEFAULT '0',
  `req_approval_purchasing` tinyint(1) DEFAULT '0',
  `req_approval_admin` tinyint(1) DEFAULT '0',
  `from_project_document_template_id` bigint DEFAULT NULL,
  `from_project_document_template_detail_id` bigint DEFAULT NULL,
  `completed_flag` tinyint DEFAULT '0',
  `deleted_by` bigint DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_document_id`),
  KEY `FK_ref_project_documents_created_by_idx` (`created_by`),
  KEY `FK_ref_project_documents_document_type_id_idx` (`document_type_id`),
  KEY `FK_ref_project_documents_project_id_idx` (`project_id`),
  KEY `FK_ref_project_documents_project_category_id_idx` (`document_category_id`),
  KEY `FK_ref_project_documents_recurring_interval_id_idx` (`recurring_interval_id`),
  KEY `FK_ref_project_documents_status_id_idx` (`status_id`),
  KEY `FK_ref_project_documents_assign_to_user_idx` (`assign_to_user`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_documents_deleted`
--

LOCK TABLES `project_documents_deleted` WRITE;
/*!40000 ALTER TABLE `project_documents_deleted` DISABLE KEYS */;
INSERT INTO `project_documents_deleted` VALUES (1,1,'project-document-1',1,5,1,1,NULL,NULL,NULL,1,5,'2022-01-18 00:08:40','2022-01-17 16:10:24',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-18 00:17:34'),(2,1,'project-document-2',1,5,2,1,NULL,NULL,NULL,NULL,5,'2022-01-18 00:09:14','2022-01-17 16:10:28',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-18 00:16:29'),(3,1,'project-document-3',1,5,1,1,NULL,NULL,NULL,1,5,'2022-01-18 00:10:47','2022-01-18 00:10:47',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-18 00:15:36'),(4,1,'project-document-4',1,5,1,2,1,'2022-01-18 00:00:00','2022-01-20 00:00:00',NULL,5,'2022-01-18 00:11:10','2022-01-18 00:11:10',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-18 00:15:10'),(5,1,'doc-1',1,5,1,1,NULL,NULL,NULL,1,5,'2022-01-21 15:14:08','2022-01-21 15:14:08',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,2,'2022-01-21 15:24:28'),(6,1,'doc-2',1,5,2,1,NULL,NULL,NULL,NULL,5,'2022-01-21 15:17:10','2022-01-21 15:17:10',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,2,'2022-01-21 15:25:09'),(7,1,'doc-3',1,5,1,2,1,'2022-01-21 00:00:00','2022-01-23 00:00:00',NULL,5,'2022-01-21 15:17:28','2022-01-21 15:17:28',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,2,'2022-01-21 15:25:26');
/*!40000 ALTER TABLE `project_documents_deleted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_groups`
--

DROP TABLE IF EXISTS `project_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_groups` (
  `group_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `group_code` varchar(50) DEFAULT NULL,
  `group_name` varchar(300) DEFAULT NULL,
  `group_engineer` bigint DEFAULT NULL,
  `group_information` varchar(300) DEFAULT NULL,
  `group_location` varchar(300) DEFAULT NULL,
  `group_pv_capacity` decimal(12,6) DEFAULT '0.000000',
  `group_total_installation` int DEFAULT '0',
  `group_total_task` int DEFAULT '0',
  `group_completed_task` int DEFAULT '0',
  `group_total_document` int DEFAULT '0',
  `group_completed_document` int DEFAULT '0',
  `group_progress` int DEFAULT '0',
  `group_total_capacity` decimal(12,6) DEFAULT '0.000000',
  `group_completed_capacity` decimal(12,6) DEFAULT '0.000000',
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_id`,`created_by`),
  KEY `FK_ref_sem_groups_created_by_idx` (`created_by`),
  KEY `FK_ref_sem_groups_project_id_idx` (`project_id`),
  KEY `FK_ref_groups_status_id_idx` (`status_id`),
  KEY `FK_ref_groups_project_engineer_idx` (`group_engineer`),
  CONSTRAINT `FK_ref_groups_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_groups_project_engineer` FOREIGN KEY (`group_engineer`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_groups_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`),
  CONSTRAINT `FK_ref_groups_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_groups` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_groups`
--

LOCK TABLES `project_groups` WRITE;
/*!40000 ALTER TABLE `project_groups` DISABLE KEYS */;
INSERT INTO `project_groups` VALUES (1,1,NULL,'p1-group-1',5,'p1-group-1',NULL,0.100000,0,1,1,6,5,0,10.001000,0.000000,1,5,'2022-01-15 14:21:57','2022-01-21 15:47:46');
/*!40000 ALTER TABLE `project_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_group_assign_after_insert` AFTER INSERT ON `project_groups` FOR EACH ROW BEGIN

	# GROUP ENGINEER ASSIGN
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, assign_to_id, assign_to_title, assign_link ) 
    VALUES (now(), NEW.group_engineer, 'PROJECT GROUP', NEW.project_id, NEW.group_id, NEW.group_id, NEW.group_name, 'projectGroupDashboard');
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_group_assign_after_update` AFTER UPDATE ON `project_groups` FOR EACH ROW BEGIN

	# GROUP ENGINEER ASSIGN
    IF ( OLD.group_engineer != NEW.group_engineer ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, assign_to_id, assign_to_title, assign_link ) 
		VALUES (now(), NEW.group_engineer, 'PROJECT GROUP', NEW.project_id, NEW.group_id, NEW.group_id, NEW.group_name, 'projectGroupDashboard');
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_itasks`
--

DROP TABLE IF EXISTS `project_itasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_itasks` (
  `task_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `task_title` varchar(100) DEFAULT NULL,
  `assign_to_user` bigint DEFAULT NULL,
  `task_description` varchar(300) DEFAULT NULL,
  `task_remarks` varchar(1000) DEFAULT NULL,
  `task_progress` int DEFAULT '0',
  `status_id` bigint DEFAULT NULL,
  `task_est_start_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `task_est_end_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `task_start_date` datetime DEFAULT NULL,
  `task_end_date` datetime DEFAULT NULL,
  `upload_attachment` varchar(900) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from_project_task_template_id` bigint DEFAULT NULL,
  `from_project_task_template_detail_id` bigint DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY `FK_ref_project_itasks_created_by_idx` (`created_by`),
  KEY `FK_ref_project_itasks_status_id_idx` (`status_id`),
  KEY `FK_ref_project_itasks_project_id_idx` (`project_id`),
  KEY `FK_ref_project_itasks_assign_to_user_idx` (`assign_to_user`),
  CONSTRAINT `FK_ref_project_itasks_assign_to_user` FOREIGN KEY (`assign_to_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_itasks_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_itasks_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`),
  CONSTRAINT `FK_ref_project_itasks_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_tasks` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_itasks`
--

LOCK TABLES `project_itasks` WRITE;
/*!40000 ALTER TABLE `project_itasks` DISABLE KEYS */;
INSERT INTO `project_itasks` VALUES (1,1,'project-task-1',5,NULL,NULL,100,4,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 13:40:55','2022-01-16 05:41:43',NULL,NULL),(5,2,'task-1',2,'111',NULL,0,1,'2022-01-24 00:00:00','2022-01-24 00:00:00',NULL,NULL,NULL,2,'2022-01-24 17:02:52','2022-01-24 17:02:52',1,1);
/*!40000 ALTER TABLE `project_itasks` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_itasks_insert` AFTER INSERT ON `project_itasks` FOR EACH ROW BEGIN
 
    SET @project_id = NEW.project_id;
 
    SET @project_level_total_task = (Select project_level_total_task from projects where project_id = @project_id);
      
    SET @project_level_total_task = @project_level_total_task + 1;
     
 	Update projects
 	SET
 	project_level_total_task = @project_level_total_task
 	Where project_id = @project_id;
 	
 	IF ( NEW.status_id = '4' ) THEN
 		 
 		SET @project_level_completed_task = (Select project_level_completed_task from projects where project_id = @project_id);
 		  
 		SET @project_level_completed_task = @project_level_completed_task + 1;
 		 
 		Update projects
 		SET
 		project_level_completed_task = @project_level_completed_task
 		Where project_id = @project_id;
     
     END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_task_assign_after_insert` AFTER INSERT ON `project_itasks` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE TASK TO
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
    VALUES (now(), NEW.assign_to_user, 'PROJECT TASK', NEW.project_id, NEW.task_id, NEW.task_title, 'projectTaskEditFrProject');
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_itasks_update` AFTER UPDATE ON `project_itasks` FOR EACH ROW BEGIN

	SET @project_id = NEW.project_id;
 
 	IF ( NEW.status_id = '4' ) THEN
 		 
 		SET @project_level_completed_task = (Select project_level_completed_task from projects where project_id = @project_id);
 		  
 		SET @project_level_completed_task = @project_level_completed_task + 1;
 		 
 		Update projects
 		SET
 		project_level_completed_task = @project_level_completed_task
 		Where project_id = @project_id;
     
     END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_task_assign_after_update` AFTER UPDATE ON `project_itasks` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE TASK TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
		VALUES (now(), NEW.assign_to_user, 'PROJECT TASK', NEW.project_id, NEW.task_id, NEW.task_title, 'projectTaskEditFrProject');
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_itasks_deleted`
--

DROP TABLE IF EXISTS `project_itasks_deleted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_itasks_deleted` (
  `task_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `task_title` varchar(100) DEFAULT NULL,
  `assign_to_user` bigint DEFAULT NULL,
  `task_description` varchar(300) DEFAULT NULL,
  `task_remarks` varchar(1000) DEFAULT NULL,
  `task_progress` int DEFAULT '0',
  `status_id` bigint DEFAULT NULL,
  `task_est_start_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `task_est_end_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `task_start_date` datetime DEFAULT NULL,
  `task_end_date` datetime DEFAULT NULL,
  `upload_attachment` varchar(900) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from_project_task_template_id` bigint DEFAULT NULL,
  `from_project_task_template_detail_id` bigint DEFAULT NULL,
  `deleted_by` bigint DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`task_id`),
  KEY `FK_ref_project_itasks_created_by_idx` (`created_by`),
  KEY `FK_ref_project_itasks_status_id_idx` (`status_id`),
  KEY `FK_ref_project_itasks_project_id_idx` (`project_id`),
  KEY `FK_ref_project_itasks_assign_to_user_idx` (`assign_to_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_itasks_deleted`
--

LOCK TABLES `project_itasks_deleted` WRITE;
/*!40000 ALTER TABLE `project_itasks_deleted` DISABLE KEYS */;
INSERT INTO `project_itasks_deleted` VALUES (2,1,'project-task-2',5,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 13:41:14','2022-01-16 13:41:14',NULL,NULL,5,'2022-01-16 13:54:39'),(3,1,'project-task-3',5,NULL,NULL,0,2,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 13:41:59','2022-01-16 13:41:59',NULL,NULL,5,'2022-01-16 13:56:32'),(4,1,'project-task-4',5,NULL,NULL,0,1,'2022-01-16 00:00:00','2022-01-16 00:00:00',NULL,NULL,NULL,5,'2022-01-16 13:56:48','2022-01-16 13:56:48',NULL,NULL,5,'2022-01-16 13:56:57');
/*!40000 ALTER TABLE `project_itasks_deleted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_milestones`
--

DROP TABLE IF EXISTS `project_milestones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_milestones` (
  `milestone_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `milestone_code` varchar(50) DEFAULT NULL,
  `milestone_info` varchar(100) DEFAULT NULL,
  `milestone_progress` int DEFAULT '0',
  `milestone_sequence` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `from_milestone_template_id` bigint DEFAULT NULL,
  `from_milestone_template_detail_id` bigint DEFAULT NULL,
  `milestone_task_total` int DEFAULT '0',
  `milestone_task_completed` int DEFAULT '0',
  `milestone_document_total` int DEFAULT '0',
  `milestone_document_completed` int DEFAULT '0',
  PRIMARY KEY (`milestone_id`),
  KEY `FK_ref_milestone_created_by_idx` (`created_by`),
  KEY `FK_ref_project_milestone_project_id_idx` (`project_id`),
  CONSTRAINT `FK_ref_project_milestone_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_milestone_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_milestones`
--

LOCK TABLES `project_milestones` WRITE;
/*!40000 ALTER TABLE `project_milestones` DISABLE KEYS */;
INSERT INTO `project_milestones` VALUES (1,1,'design','design',0,1,5,'2022-01-15 14:29:10','2022-01-19 21:08:11',1,NULL,NULL,1,1,2,2),(2,1,'Installation','Installation',0,2,5,'2022-01-15 14:29:27','2022-01-21 15:47:46',1,NULL,NULL,0,0,4,3);
/*!40000 ALTER TABLE `project_milestones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_scurves`
--

DROP TABLE IF EXISTS `project_scurves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_scurves` (
  `project_scurve_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `month` int DEFAULT NULL,
  `expected_total_task` int DEFAULT '0',
  `actual_total_task` int DEFAULT '0',
  `expected_total_document` int DEFAULT '0',
  `actual_total_document` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_scurve_id`),
  KEY `FK_ref_project_scurves_project_id_idx` (`project_id`),
  CONSTRAINT `FK_ref_project_scurves_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_scurves`
--

LOCK TABLES `project_scurves` WRITE;
/*!40000 ALTER TABLE `project_scurves` DISABLE KEYS */;
INSERT INTO `project_scurves` VALUES (1,1,'2022',1,1,0,0,0,'2022-01-15 14:41:37','2022-01-16 13:20:49');
/*!40000 ALTER TABLE `project_scurves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_site_contractors`
--

DROP TABLE IF EXISTS `project_site_contractors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_site_contractors` (
  `site_contractor_id` bigint NOT NULL AUTO_INCREMENT,
  `site_id` bigint DEFAULT NULL,
  `contractor_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`site_contractor_id`),
  KEY `FK_ref_project_site_contractors_created_by_idx` (`created_by`),
  KEY `FK_ref_project_site_contractors_site_id_idx` (`site_id`),
  KEY `FK_ref_project_site_contractors_contractors_idx` (`contractor_id`),
  CONSTRAINT `FK_ref_project_site_contractors_contractors` FOREIGN KEY (`contractor_id`) REFERENCES `project_contractors` (`contractor_id`),
  CONSTRAINT `FK_ref_project_site_contractors_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_project_site_contractors_site_id` FOREIGN KEY (`site_id`) REFERENCES `project_sites` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_site_contractors`
--

LOCK TABLES `project_site_contractors` WRITE;
/*!40000 ALTER TABLE `project_site_contractors` DISABLE KEYS */;
INSERT INTO `project_site_contractors` VALUES (1,1,1,5,'2022-01-15 14:41:07','2022-01-15 14:41:07',1);
/*!40000 ALTER TABLE `project_site_contractors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_sites`
--

DROP TABLE IF EXISTS `project_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_sites` (
  `site_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  `group_id` bigint DEFAULT NULL,
  `site_name` varchar(300) DEFAULT NULL,
  `site_postal_code` varchar(10) DEFAULT NULL,
  `site_address` varchar(200) DEFAULT NULL,
  `site_lead_contact` varchar(50) DEFAULT NULL,
  `site_total_installation` int DEFAULT '0',
  `site_total_task` int DEFAULT '0',
  `site_completed_task` int DEFAULT '0',
  `site_total_document` int DEFAULT '0',
  `site_completed_document` int DEFAULT '0',
  `site_progress` int DEFAULT '0',
  `site_module_quantity` decimal(12,4) DEFAULT '0.0000',
  `site_module_capacity` decimal(12,4) DEFAULT '0.0000',
  `site_total_capacity` decimal(12,4) DEFAULT '0.0000',
  `site_pr_capacity` int DEFAULT '0',
  `site_type_of_tension` varchar(100) DEFAULT NULL,
  `site_approved_load` varchar(100) DEFAULT NULL,
  `site_mssl_account_number` varchar(100) DEFAULT NULL,
  `site_switchroom_tie` varchar(100) DEFAULT NULL,
  `site_plant_name` varchar(200) DEFAULT NULL,
  `site_folder_in_sftp_server` varchar(600) DEFAULT NULL,
  `site_monitoring_application` varchar(100) DEFAULT NULL,
  `site_inverter_type` varchar(50) DEFAULT NULL,
  `site_inverter_qty` int DEFAULT '1',
  `site_total_inverter` int DEFAULT NULL,
  `site_logger_type` varchar(100) DEFAULT NULL,
  `site_total_data_logger` varchar(100) DEFAULT NULL,
  `site_start_date` datetime DEFAULT NULL,
  `site_end_date` datetime DEFAULT NULL,
  `site_contractor_start_date` datetime DEFAULT NULL,
  `site_contractor_end_date` datetime DEFAULT NULL,
  `site_elu_end_date` datetime DEFAULT NULL,
  `site_el_expiry_date` datetime DEFAULT NULL,
  `site_target_turn_on_date` datetime DEFAULT NULL,
  `site_elu_license_number` varchar(100) DEFAULT NULL,
  `site_elu_status` varchar(100) DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `site_completed_capacity` decimal(12,6) DEFAULT '0.000000',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_template_id` bigint DEFAULT NULL,
  `document_template_id` bigint DEFAULT NULL,
  PRIMARY KEY (`site_id`),
  KEY `FK_ref_sem_sites_created_by_idx` (`created_by`),
  KEY `FK_ref_sem_sites_project_id_idx` (`project_id`),
  KEY `FK_ref_sem_sites_group_id_idx` (`group_id`),
  KEY `FK_ref_sites_status_id_idx` (`status_id`),
  CONSTRAINT `FK_ref_sites_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_sites_group_id` FOREIGN KEY (`group_id`) REFERENCES `project_groups` (`group_id`),
  CONSTRAINT `FK_ref_sites_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`),
  CONSTRAINT `FK_ref_sites_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_sites` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_sites`
--

LOCK TABLES `project_sites` WRITE;
/*!40000 ALTER TABLE `project_sites` DISABLE KEYS */;
INSERT INTO `project_sites` VALUES (1,1,1,'p1-g1-site-1','53300','Kuala Lumpur',NULL,0,1,1,6,5,1,100.0000,100.0000,10.0000,1,'None','1','MSSL',NULL,NULL,NULL,'SolarMon','111',1,1,'HwaWei','1','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','EL',NULL,1,0.000000,5,'2022-01-15 14:26:55','2022-01-21 15:47:46',NULL,NULL),(2,1,1,'p1-g1-site-2','53300','Kuala Lumpur',NULL,0,0,0,0,0,1,1.0000,1.0000,0.0010,1,'None','1','MSSL',NULL,NULL,NULL,'SolarMon','1',1,1,'HuaWei','1','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','EL',NULL,1,0.000000,5,'2022-01-15 16:46:12','2022-01-15 16:46:12',NULL,NULL);
/*!40000 ALTER TABLE `project_sites` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_sites_insert_before` BEFORE INSERT ON `project_sites` FOR EACH ROW BEGIN
     
	IF ( NEW.status_id in (5,6,7)) THEN
    
		SET NEW.site_completed_capacity = NEW.site_total_capacity;
        
	ELSE
    
		SET NEW.site_completed_capacity = 0;
       
    END IF;
         
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_sites_insert` AFTER INSERT ON `project_sites` FOR EACH ROW BEGIN
     
	SET @group_id = NEW.group_id;
	
	SET @project_id = (Select project_id from project_groups where group_id = @group_id);
    
    SET @group_total_capacity = (Select sum(site_total_capacity) from project_sites where group_id = @group_id);
	SET @group_completed_capacity = (Select sum(site_completed_capacity) from project_sites where group_id = @group_id);
    
    SET @project_total_capacity = (Select sum(site_total_capacity) from project_sites where project_id = @project_id);
	SET @project_completed_capacity = (Select sum(site_completed_capacity) from project_sites where project_id = @project_id);
    
	UPDATE `project_groups`
	SET
	`group_completed_capacity` = @group_completed_capacity,
    `group_total_capacity` = @group_total_capacity
	WHERE `group_id` = @group_id;
        
	UPDATE `projects`
	SET
	`project_completed_capacity` = @project_completed_capacity,
	`project_total_capacity` = @project_total_capacity
	WHERE `project_id` = @project_id;
         
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_sites_update_before` BEFORE UPDATE ON `project_sites` FOR EACH ROW BEGIN
     
	IF ( NEW.status_id in (5,6,7)) THEN
    
		SET NEW.site_completed_capacity = NEW.site_total_capacity;
        
	ELSE
    
		SET NEW.site_completed_capacity = 0;
       
    END IF;
         
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_sites_update` AFTER UPDATE ON `project_sites` FOR EACH ROW BEGIN
     
	SET @group_id = NEW.group_id;
	
	SET @project_id = (Select project_id from project_groups where group_id = @group_id);
    
    SET @group_total_capacity = (Select sum(site_total_capacity) from project_sites where group_id = @group_id);
	SET @group_completed_capacity = (Select sum(site_completed_capacity) from project_sites where group_id = @group_id);
    
    SET @project_total_capacity = (Select sum(site_total_capacity) from project_sites where project_id = @project_id);
	SET @project_completed_capacity = (Select sum(site_completed_capacity) from project_sites where project_id = @project_id);
    
	UPDATE `project_groups`
	SET
	`group_completed_capacity` = @group_completed_capacity,
    `group_total_capacity` = @group_total_capacity
	WHERE `group_id` = @group_id;
        
	UPDATE `projects`
	SET
	`project_completed_capacity` = @project_completed_capacity,
	`project_total_capacity` = @project_total_capacity
	WHERE `project_id` = @project_id;
         
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_task_template_details`
--

DROP TABLE IF EXISTS `project_task_template_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_task_template_details` (
  `project_task_template_detail_id` bigint NOT NULL AUTO_INCREMENT,
  `project_task_template_id` bigint DEFAULT NULL,
  `project_task_title` varchar(100) DEFAULT NULL,
  `project_task_description` varchar(300) DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`project_task_template_detail_id`),
  KEY `FK_ref_pro_task_template_details_created_by_idx` (`created_by`),
  KEY `FK_ref_pro_task_template_details_task_template_id_idx` (`project_task_template_id`),
  KEY `FK_ref_pro_task_template_details_status_id_idx` (`status_id`),
  CONSTRAINT `FK_ref_pro_task_template_details_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_pro_task_template_details_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_tasks` (`status_id`),
  CONSTRAINT `FK_ref_pro_task_template_details_task_template_id` FOREIGN KEY (`project_task_template_id`) REFERENCES `project_task_templates` (`project_task_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task_template_details`
--

LOCK TABLES `project_task_template_details` WRITE;
/*!40000 ALTER TABLE `project_task_template_details` DISABLE KEYS */;
INSERT INTO `project_task_template_details` VALUES (1,1,'task-1','111',1,5,'2022-01-06 07:15:37','2022-01-06 07:15:37',1,NULL);
/*!40000 ALTER TABLE `project_task_template_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_task_template_details_insert` AFTER INSERT ON `project_task_template_details` FOR EACH ROW BEGIN
   
	Select task_details_no into @task_details_no from project_task_templates where project_task_template_id = NEW.project_task_template_id;
     
    SET @task_details_no = @task_details_no + 1;
     
    Update project_task_templates
    SET
    task_details_no = @task_details_no
    Where project_task_template_id = NEW.project_task_template_id;
        
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_task_template_details_delete` AFTER DELETE ON `project_task_template_details` FOR EACH ROW BEGIN
   
 	Select task_details_no into @task_details_no from project_task_templates where project_task_template_id = OLD.project_task_template_id;
     
     SET @task_details_no = @task_details_no - 1;
     
     Update project_task_templates
     SET
     task_details_no = @task_details_no
     Where project_task_template_id = OLD.project_task_template_id;
        
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_task_templates`
--

DROP TABLE IF EXISTS `project_task_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_task_templates` (
  `project_task_template_id` bigint NOT NULL AUTO_INCREMENT,
  `task_template_name` varchar(100) DEFAULT NULL,
  `task_details_no` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_task_template_id`),
  KEY `FK_ref_project_task_templates_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_project_task_templates_created_by_id` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task_templates`
--

LOCK TABLES `project_task_templates` WRITE;
/*!40000 ALTER TABLE `project_task_templates` DISABLE KEYS */;
INSERT INTO `project_task_templates` VALUES (1,'project-task-template',1,5,'2022-01-06 07:15:29','2022-01-06 07:15:37');
/*!40000 ALTER TABLE `project_task_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_types`
--

DROP TABLE IF EXISTS `project_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_types` (
  `project_type_id` bigint NOT NULL AUTO_INCREMENT,
  `project_type` varchar(100) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`project_type_id`),
  KEY `FK_ref_project_types_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_project_types_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_types`
--

LOCK TABLES `project_types` WRITE;
/*!40000 ALTER TABLE `project_types` DISABLE KEYS */;
INSERT INTO `project_types` VALUES (1,'C&I',1,'2021-10-15 12:13:40','2021-10-29 16:15:11',0),(2,'SN2',1,'2021-10-15 17:33:02','2021-10-29 16:15:12',1),(3,'SN3',1,'2021-10-20 11:01:37','2021-10-29 16:15:12',1),(4,'SN5',1,'2021-10-29 16:15:33','2021-10-29 16:15:33',1),(5,'SR2',1,'2021-10-29 16:16:15','2021-10-29 08:16:29',0),(6,'SL3',1,'2021-10-29 16:16:21','2021-10-29 16:16:21',1);
/*!40000 ALTER TABLE `project_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `projects` (
  `project_id` bigint NOT NULL AUTO_INCREMENT,
  `project_code` varchar(50) DEFAULT NULL,
  `project_name` varchar(300) DEFAULT NULL,
  `project_country` varchar(100) DEFAULT NULL,
  `project_manager` bigint DEFAULT NULL,
  `project_engineer` bigint DEFAULT NULL,
  `project_safety` bigint DEFAULT NULL,
  `project_type_id` bigint DEFAULT NULL,
  `project_category_id` bigint DEFAULT NULL,
  `project_pv_capacity` decimal(12,6) DEFAULT '0.000000',
  `project_total_installation` int DEFAULT '0',
  `project_total_task` int DEFAULT '0',
  `project_completed_task` int DEFAULT '0',
  `project_total_document` int DEFAULT '0',
  `project_completed_document` int DEFAULT '0',
  `project_progress` int DEFAULT '0',
  `project_total_capacity` decimal(12,6) DEFAULT '0.000000',
  `project_completed_capacity` decimal(12,6) DEFAULT '0.000000',
  `project_start_date` datetime DEFAULT NULL,
  `project_end_date` datetime DEFAULT NULL,
  `project_ppa_start_date` datetime DEFAULT NULL,
  `project_ppa_contractual_cod_date` datetime DEFAULT NULL,
  `developer_id` bigint DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `project_task_template_id` bigint DEFAULT NULL,
  `project_document_template_id` bigint DEFAULT NULL,
  `project_level_total_task` int DEFAULT '0',
  `project_level_completed_task` int DEFAULT '0',
  `project_level_total_document` int DEFAULT '0',
  `project_level_completed_document` int DEFAULT '0',
  PRIMARY KEY (`project_id`),
  KEY `FK_ref_sem_projects_created_by_idx` (`created_by`),
  KEY `FK_ref_projecys_developer_id_idx` (`developer_id`),
  KEY `FK_ref_projects_status_id_idx` (`status_id`),
  KEY `FK_ref_projects_project_manager_idx` (`project_manager`),
  KEY `FK_ref_projects_type_id_idx` (`project_type_id`),
  KEY `FK_ref_projects_category_id_idx` (`project_category_id`),
  CONSTRAINT `FK_ref_projects_category_id` FOREIGN KEY (`project_category_id`) REFERENCES `project_categories` (`project_category_id`),
  CONSTRAINT `FK_ref_projects_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_projects_developer_id` FOREIGN KEY (`developer_id`) REFERENCES `project_developers` (`developer_id`),
  CONSTRAINT `FK_ref_projects_project_manager` FOREIGN KEY (`project_manager`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_projects_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_projects` (`status_id`),
  CONSTRAINT `FK_ref_projects_type_id` FOREIGN KEY (`project_type_id`) REFERENCES `project_types` (`project_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,NULL,'project-1','Malaysia',5,3,6,1,1,10.000000,0,1,1,6,5,8,10.001000,0.000000,'2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00','2022-01-15 00:00:00',1,1,5,'2022-01-15 14:21:38','2022-01-21 15:47:46',NULL,NULL,1,1,5,3),(2,NULL,'project-2','Malaysia',5,3,6,1,1,0.000000,0,0,0,0,0,0,0.000000,0.000000,'2022-01-24 00:00:00','2022-01-24 00:00:00','2022-01-24 00:00:00','2022-01-24 00:00:00',1,1,2,'2022-01-24 17:02:52','2022-01-24 17:09:45',1,1,1,0,2,2);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `projects_insert` AFTER INSERT ON `projects` FOR EACH ROW BEGIN

/*
SET @group_engineer = (select id from users where role_id = 10 order by RAND() limit 1);

INSERT INTO `project_groups`
(`project_id`,
`group_code`,
`group_name`,
`group_engineer`,
`group_information`,
`group_location`,
`group_total_installation`,
`group_total_task`,
`group_total_document`,
`group_progress`,
`status_id`,
`created_by`)
VALUES
(
NEW.project_id,
CONCAT(NEW.project_code, '-group'),
CONCAT(NEW.project_name,'-group'),
@group_engineer,
'',
'',
0,
0,
0,
0,
1,
NEW.created_by);

INSERT INTO `project_milestones`
(`project_id`,
`milestone_code`,
`milestone_info`,
`milestone_progress`,
`milestone_sequence`,
`active_status`
)
VALUES
(NEW.project_id,
'DEFAULT',
'Default Milestone',
0,
1,
1
);
*/
        
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_assign_after_insert` AFTER INSERT ON `projects` FOR EACH ROW BEGIN

	# ENGINEER ASSIGN
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
    VALUES (now(), NEW.project_engineer, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');
    
    # SAFETY ASSIGN
    INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
    VALUES (now(), NEW.project_safety, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `project_assign_after_update` AFTER UPDATE ON `projects` FOR EACH ROW BEGIN

	#ENGINEER ASSIGN
    IF ( OLD.project_engineer != NEW.project_engineer ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
		VALUES (now(), NEW.project_engineer, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');
    END IF;
    
    # SAFETY ASSIGN
	IF ( OLD.project_safety != NEW.project_safety ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link ) 
		VALUES (now(), NEW.project_safety, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');
    END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `recurring_intervals`
--

DROP TABLE IF EXISTS `recurring_intervals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recurring_intervals` (
  `recurring_interval_id` bigint NOT NULL AUTO_INCREMENT,
  `document_recurring_interval` varchar(50) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`recurring_interval_id`),
  KEY `FK_ref_document_recurring_intervals_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_document_recurring_intervals_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recurring_intervals`
--

LOCK TABLES `recurring_intervals` WRITE;
/*!40000 ALTER TABLE `recurring_intervals` DISABLE KEYS */;
INSERT INTO `recurring_intervals` VALUES (1,'DAILY',1,'2021-11-15 15:52:44','2021-11-15 15:52:44'),(2,'WEEKLY',1,'2021-11-15 15:52:44','2021-11-15 15:52:44'),(3,'MONTHLY',1,'2021-11-15 15:52:44','2021-11-15 15:52:44');
/*!40000 ALTER TABLE `recurring_intervals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_accesses`
--

DROP TABLE IF EXISTS `role_accesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_accesses` (
  `role_access_id` bigint NOT NULL AUTO_INCREMENT,
  `module_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `role_id` bigint DEFAULT NULL,
  `access_listing` tinyint(1) DEFAULT '1',
  `access_create` tinyint(1) DEFAULT '1',
  `access_show` tinyint(1) DEFAULT '1',
  `access_edit` tinyint(1) DEFAULT '1',
  `access_delete` tinyint(1) DEFAULT '1',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_access_id`),
  KEY `FK_ref_role_accesses_created_by` (`created_by`),
  KEY `FK_ref_role_accesses_module_code` (`module_code`),
  KEY `FK_ref_role_accesses_role_id` (`role_id`),
  CONSTRAINT `FK_ref_role_accesses_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_ref_role_accesses_module_code` FOREIGN KEY (`module_code`) REFERENCES `modules` (`module_code`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_role_accesses_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=905 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_accesses`
--

LOCK TABLES `role_accesses` WRITE;
/*!40000 ALTER TABLE `role_accesses` DISABLE KEYS */;
INSERT INTO `role_accesses` VALUES (1,'USER',1,1,1,1,1,0,1,'2021-06-08 22:39:29','2021-12-06 14:16:52'),(2,'ROLE',1,1,1,1,1,0,1,'2021-06-08 22:42:45','2021-06-10 16:10:55'),(3,'MODULE',1,1,1,1,1,0,1,'2021-06-09 00:07:10','2021-06-10 16:50:29'),(4,'ROLE_ACCESS',1,1,1,1,1,0,1,'2021-06-09 00:08:35','2021-06-09 01:04:34'),(5,'SUBSCRIPTION',1,1,1,1,1,0,1,'2021-06-09 00:34:18','2021-06-13 21:35:50'),(6,'CUSTOMER',1,1,1,1,1,0,1,'2021-06-09 00:34:36','2021-06-14 00:54:49'),(7,'CONFIGTYPE',1,1,1,1,1,0,1,'2021-06-13 22:05:52','2021-06-15 20:27:23'),(8,'COMPANY',1,1,1,1,1,0,1,'2021-06-13 23:02:32','2021-06-15 20:27:23'),(9,'DEVELOPER',1,1,1,1,1,1,1,'2021-06-14 00:23:33','2021-12-06 13:56:16'),(10,'STATUS',1,1,1,1,1,0,1,'2021-06-14 16:48:47','2021-06-15 20:27:23'),(11,'PROJECT',1,1,1,1,1,1,1,'2021-06-14 17:46:09','2021-12-06 18:34:29'),(12,'CLUSTER',1,1,1,1,1,0,1,'2021-06-15 16:44:41','2021-06-15 20:27:23'),(13,'BUILDING',1,1,1,1,1,0,1,'2021-06-15 17:09:25','2021-06-15 20:27:23'),(14,'DEPARTMENT',1,1,1,1,1,0,1,'2021-06-15 17:33:40','2021-06-15 20:27:23'),(15,'FLOOR',1,1,1,1,1,0,1,'2021-06-15 17:59:50','2021-06-15 20:27:23'),(16,'UNIT',1,1,1,1,1,0,1,'2021-06-15 20:25:01','2021-06-15 20:27:23'),(18,'TASK_MILESTONE_MODE',1,1,1,1,1,0,1,'2021-06-15 21:20:12','2021-06-15 21:22:49'),(19,'TASK_STATUS',1,1,1,1,1,1,1,'2021-06-15 21:48:38','2021-06-15 22:30:34'),(20,'TASK_CATEGORY',1,1,1,1,1,1,1,'2021-06-17 18:32:08','2021-06-17 18:32:18'),(21,'TASK_PRIORITY',1,1,1,1,1,1,1,'2021-06-17 19:30:57','2021-06-18 16:22:24'),(22,'TASK_ATTACHMENT_STATUS',1,1,1,1,1,1,1,'2021-06-18 16:21:20','2021-06-18 16:22:39'),(23,'TASK',1,1,1,1,1,1,1,'2021-06-18 17:47:10','2021-12-07 17:34:39'),(24,'MILESTONE_TEMPLATE',1,1,1,1,1,1,1,'2021-07-21 17:36:29','2021-12-05 18:15:45'),(25,'MILESTONE_TEMPLATE_DETAIL',1,1,1,1,1,1,1,'2021-07-21 20:09:27','2021-12-05 18:34:06'),(26,'DOCUMENT_TEMPLATE',1,1,1,1,1,1,1,'2021-07-21 17:36:29','2021-12-05 20:27:03'),(27,'DOCUMENT_TEMPLATE_DETAIL',1,1,1,1,1,1,1,'2021-07-21 20:09:27','2021-12-05 20:27:03'),(29,'PROJECT_TYPE',1,1,1,1,1,1,1,'2021-10-15 11:55:37','2021-12-06 14:22:18'),(30,'PROJECT_CATEGORY',1,1,1,1,1,1,1,'2021-10-15 11:55:37','2021-12-06 14:26:01'),(31,'PROJECT_DOCUMENT_TEMPLATE',1,1,1,1,1,1,1,'2021-10-21 17:23:51','2021-12-06 14:17:36'),(32,'PROJECT_TASK_TEMPLATE',1,1,1,1,1,1,1,'2021-10-22 18:22:58','2021-12-05 20:45:42'),(33,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',1,1,1,1,1,1,1,'2021-10-23 16:34:28','2021-12-05 20:37:53'),(34,'PROJECT_TASK_TEMPLATE_DETAIL',1,1,1,1,1,1,1,'2021-10-24 18:15:54','2021-12-05 20:45:42'),(35,'TASK_TEMPLATE',1,1,1,1,1,1,1,'2021-10-26 10:35:16','2021-12-05 20:20:03'),(36,'TASK_TEMPLATE_DETAIL',1,1,1,1,1,1,1,'2021-10-26 10:35:16','2021-12-05 20:20:03'),(37,'PROJECT_GROUP',1,1,1,1,1,1,1,'2021-11-03 11:33:16','2021-12-07 17:08:39'),(39,'PROJECT_SITE',1,1,1,1,1,1,1,'2021-11-05 17:46:15','2021-12-07 17:43:26'),(40,'PROJECT_TASK',1,1,1,1,1,1,1,'2021-11-08 11:19:26','2021-12-06 17:37:20'),(41,'PROJECT_MILESTONE',1,1,1,1,1,1,1,'2021-11-09 16:04:38','2021-12-02 16:51:03'),(42,'PROJECT_DOCUMENT',1,1,1,1,1,1,1,'2021-11-17 15:12:31','2021-12-06 18:00:14'),(43,'PROJECT_DOCUMENT_UPLOADS',1,1,1,1,1,1,1,'2021-11-18 17:34:33','2021-12-06 17:59:56'),(44,'PROJECT_DOCUMENT_APPROVAL',1,1,1,1,1,1,1,'2021-11-20 17:17:32','2021-11-20 17:17:32'),(45,'PROJECT_DOCUMENT_RECURRING',1,1,1,1,1,1,1,'2021-11-21 17:39:24','2021-12-06 17:54:45'),(46,'SITE_DOCUMENT',1,1,1,1,1,1,1,'2021-11-23 17:58:27','2021-12-07 17:38:18'),(47,'SITE_DOCUMENT_RECURRING',1,1,1,1,1,1,1,'2021-11-24 16:47:22','2021-12-07 17:38:42'),(48,'SITE_DOCUMENT_UPLOADS',1,1,1,1,1,1,1,'2021-11-24 16:47:22','2021-12-07 17:39:13'),(49,'SITE_DOCUMENT_APPROVAL',1,1,1,1,1,1,1,'2021-11-24 16:47:22','2021-12-06 18:13:55'),(51,'CONTRACTOR',1,1,1,1,1,1,1,'2021-12-02 16:21:59','2021-12-06 14:04:14'),(52,'SITE_CONTRACTOR',1,1,1,1,1,1,1,'2021-12-06 19:34:38','2021-12-07 17:32:16'),(242,'USER',9,0,0,0,0,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:44'),(243,'ROLE',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(244,'MODULE',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(245,'ROLE_ACCESS',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(246,'SUBSCRIPTION',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(247,'CUSTOMER',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(248,'CONFIGTYPE',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(249,'COMPANY',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(250,'DEVELOPER',9,1,0,1,0,1,1,'2021-12-14 14:31:03','2021-12-15 09:43:13'),(251,'STATUS',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(252,'PROJECT',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(253,'CLUSTER',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(254,'BUILDING',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(255,'DEPARTMENT',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(256,'FLOOR',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(257,'UNIT',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(258,'TASK_MILESTONE_MODE',9,1,1,1,1,0,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(259,'TASK_STATUS',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(260,'TASK_CATEGORY',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(261,'TASK_PRIORITY',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(262,'TASK_ATTACHMENT_STATUS',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(263,'TASK',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(264,'MILESTONE_TEMPLATE',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(265,'MILESTONE_TEMPLATE_DETAIL',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(266,'DOCUMENT_TEMPLATE',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(267,'DOCUMENT_TEMPLATE_DETAIL',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(268,'PROJECT_TYPE',9,1,0,1,0,1,1,'2021-12-14 14:31:03','2021-12-15 09:42:48'),(269,'PROJECT_CATEGORY',9,1,0,1,0,1,1,'2021-12-14 14:31:03','2021-12-15 09:42:48'),(270,'PROJECT_DOCUMENT_TEMPLATE',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(271,'PROJECT_TASK_TEMPLATE',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(272,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(273,'PROJECT_TASK_TEMPLATE_DETAIL',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(274,'TASK_TEMPLATE',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(275,'TASK_TEMPLATE_DETAIL',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(276,'PROJECT_GROUP',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(277,'PROJECT_SITE',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(278,'PROJECT_TASK',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(279,'PROJECT_MILESTONE',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(280,'PROJECT_DOCUMENT',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(281,'PROJECT_DOCUMENT_UPLOADS',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(282,'PROJECT_DOCUMENT_APPROVAL',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(283,'PROJECT_DOCUMENT_RECURRING',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(284,'SITE_DOCUMENT',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(285,'SITE_DOCUMENT_RECURRING',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(286,'SITE_DOCUMENT_UPLOADS',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(287,'SITE_DOCUMENT_APPROVAL',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(288,'CONTRACTOR',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-18 23:11:48'),(289,'SITE_CONTRACTOR',9,1,1,1,1,1,1,'2021-12-14 14:31:03','2021-12-14 14:31:03'),(305,'USER',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:41:22'),(306,'ROLE',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:38:33'),(307,'MODULE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(308,'ROLE_ACCESS',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:38:33'),(309,'SUBSCRIPTION',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(310,'CUSTOMER',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(311,'CONFIGTYPE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(312,'COMPANY',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(313,'DEVELOPER',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(314,'STATUS',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(315,'PROJECT',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-19 00:14:05'),(316,'CLUSTER',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(317,'BUILDING',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(318,'DEPARTMENT',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(319,'FLOOR',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(320,'UNIT',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:38:33'),(321,'TASK_MILESTONE_MODE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(322,'TASK_STATUS',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(323,'TASK_CATEGORY',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(324,'TASK_PRIORITY',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(325,'TASK_ATTACHMENT_STATUS',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(326,'TASK',12,1,0,1,1,0,1,'2021-12-14 14:32:20','2021-12-14 14:38:33'),(327,'MILESTONE_TEMPLATE',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-19 00:15:48'),(328,'MILESTONE_TEMPLATE_DETAIL',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(329,'DOCUMENT_TEMPLATE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(330,'DOCUMENT_TEMPLATE_DETAIL',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(331,'PROJECT_TYPE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(332,'PROJECT_CATEGORY',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(333,'PROJECT_DOCUMENT_TEMPLATE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(334,'PROJECT_TASK_TEMPLATE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(335,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(336,'PROJECT_TASK_TEMPLATE_DETAIL',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(337,'TASK_TEMPLATE',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(338,'TASK_TEMPLATE_DETAIL',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(339,'PROJECT_GROUP',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-19 00:14:05'),(340,'PROJECT_SITE',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-19 00:14:05'),(341,'PROJECT_TASK',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(342,'PROJECT_MILESTONE',12,1,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-19 00:14:05'),(343,'PROJECT_DOCUMENT',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(344,'PROJECT_DOCUMENT_UPLOADS',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(345,'PROJECT_DOCUMENT_APPROVAL',12,1,0,1,0,0,1,'2021-12-14 14:32:20','2022-01-06 06:48:14'),(346,'PROJECT_DOCUMENT_RECURRING',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(347,'SITE_DOCUMENT',12,1,0,1,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:38:33'),(348,'SITE_DOCUMENT_RECURRING',12,1,0,1,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:38:33'),(349,'SITE_DOCUMENT_UPLOADS',12,1,0,1,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:38:33'),(350,'SITE_DOCUMENT_APPROVAL',12,1,0,1,0,0,1,'2021-12-14 14:32:20','2022-01-06 06:48:14'),(351,'CONTRACTOR',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(352,'SITE_CONTRACTOR',12,0,0,0,0,0,1,'2021-12-14 14:32:20','2021-12-14 14:37:14'),(353,'USER',8,0,0,0,0,0,1,'2021-12-15 14:52:14','2021-12-15 14:52:14'),(354,'ROLE',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(355,'MODULE',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(356,'ROLE_ACCESS',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(357,'SUBSCRIPTION',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(358,'CUSTOMER',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(359,'CONFIGTYPE',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(360,'COMPANY',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(361,'DEVELOPER',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:52:14'),(362,'STATUS',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(363,'PROJECT',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:53:12'),(364,'CLUSTER',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(365,'BUILDING',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(366,'DEPARTMENT',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(367,'FLOOR',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(368,'UNIT',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(369,'TASK_MILESTONE_MODE',8,1,0,1,0,0,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(370,'TASK_STATUS',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(371,'TASK_CATEGORY',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(372,'TASK_PRIORITY',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(373,'TASK_ATTACHMENT_STATUS',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:15:59'),(374,'TASK',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:57:19'),(375,'MILESTONE_TEMPLATE',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-19 00:02:34'),(376,'MILESTONE_TEMPLATE_DETAIL',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-19 00:02:34'),(377,'DOCUMENT_TEMPLATE',8,1,0,0,0,1,1,'2021-12-15 14:52:14','2021-12-19 00:02:34'),(378,'DOCUMENT_TEMPLATE_DETAIL',8,1,0,0,0,1,1,'2021-12-15 14:52:14','2021-12-19 00:02:34'),(379,'PROJECT_TYPE',8,0,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:55:02'),(380,'PROJECT_CATEGORY',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:52:14'),(381,'PROJECT_DOCUMENT_TEMPLATE',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(382,'PROJECT_TASK_TEMPLATE',8,1,0,0,0,1,1,'2021-12-15 14:52:14','2021-12-19 00:02:34'),(383,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(384,'PROJECT_TASK_TEMPLATE_DETAIL',8,1,0,0,0,1,1,'2021-12-15 14:52:14','2021-12-19 00:02:34'),(385,'TASK_TEMPLATE',8,0,0,0,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:56:21'),(386,'TASK_TEMPLATE_DETAIL',8,0,0,0,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:56:21'),(387,'PROJECT_GROUP',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:07:50'),(388,'PROJECT_SITE',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:07:50'),(389,'PROJECT_TASK',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:06:16'),(390,'PROJECT_MILESTONE',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 14:53:34'),(391,'PROJECT_DOCUMENT',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:06:16'),(392,'PROJECT_DOCUMENT_UPLOADS',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(393,'PROJECT_DOCUMENT_APPROVAL',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:38'),(394,'PROJECT_DOCUMENT_RECURRING',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(395,'SITE_DOCUMENT',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(396,'SITE_DOCUMENT_RECURRING',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(397,'SITE_DOCUMENT_UPLOADS',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(398,'SITE_DOCUMENT_APPROVAL',8,1,1,1,1,1,1,'2021-12-15 14:52:14','2021-12-19 00:04:39'),(399,'CONTRACTOR',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-18 23:11:34'),(400,'SITE_CONTRACTOR',8,1,0,1,0,1,1,'2021-12-15 14:52:14','2021-12-15 15:08:23'),(416,'USER',14,0,0,0,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(417,'ROLE',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(418,'MODULE',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(419,'ROLE_ACCESS',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(420,'SUBSCRIPTION',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(421,'CUSTOMER',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(422,'CONFIGTYPE',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(423,'COMPANY',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(424,'DEVELOPER',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(425,'STATUS',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(426,'PROJECT',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(427,'CLUSTER',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(428,'BUILDING',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(429,'DEPARTMENT',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(430,'FLOOR',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(431,'UNIT',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(432,'TASK_MILESTONE_MODE',14,1,0,1,0,0,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(433,'TASK_STATUS',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(434,'TASK_CATEGORY',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(435,'TASK_PRIORITY',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(436,'TASK_ATTACHMENT_STATUS',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(437,'TASK',14,1,1,1,1,1,1,'2021-12-15 15:17:49','2021-12-15 15:19:53'),(438,'MILESTONE_TEMPLATE',14,0,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(439,'MILESTONE_TEMPLATE_DETAIL',14,0,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(440,'DOCUMENT_TEMPLATE',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(441,'DOCUMENT_TEMPLATE_DETAIL',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(442,'PROJECT_TYPE',14,0,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(443,'PROJECT_CATEGORY',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(444,'PROJECT_DOCUMENT_TEMPLATE',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(445,'PROJECT_TASK_TEMPLATE',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(446,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(447,'PROJECT_TASK_TEMPLATE_DETAIL',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(448,'TASK_TEMPLATE',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(449,'TASK_TEMPLATE_DETAIL',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(450,'PROJECT_GROUP',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(451,'PROJECT_SITE',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(452,'PROJECT_TASK',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(453,'PROJECT_MILESTONE',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(454,'PROJECT_DOCUMENT',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(455,'PROJECT_DOCUMENT_UPLOADS',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(456,'PROJECT_DOCUMENT_APPROVAL',14,0,0,0,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(457,'PROJECT_DOCUMENT_RECURRING',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(458,'SITE_DOCUMENT',14,1,1,1,1,1,1,'2021-12-15 15:17:49','2021-12-15 15:19:53'),(459,'SITE_DOCUMENT_RECURRING',14,1,1,1,1,1,1,'2021-12-15 15:17:49','2021-12-15 15:19:53'),(460,'SITE_DOCUMENT_UPLOADS',14,1,1,1,1,1,1,'2021-12-15 15:17:49','2021-12-15 15:19:54'),(461,'SITE_DOCUMENT_APPROVAL',14,1,1,1,1,1,1,'2021-12-15 15:17:49','2021-12-15 15:19:53'),(462,'CONTRACTOR',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(463,'SITE_CONTRACTOR',14,1,0,1,0,1,1,'2021-12-15 15:17:49','2021-12-15 15:17:49'),(479,'USER',16,0,0,0,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(480,'ROLE',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(481,'MODULE',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(482,'ROLE_ACCESS',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(483,'SUBSCRIPTION',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(484,'CUSTOMER',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(485,'CONFIGTYPE',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(486,'COMPANY',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(487,'DEVELOPER',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(488,'STATUS',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(489,'PROJECT',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(490,'CLUSTER',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(491,'BUILDING',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(492,'DEPARTMENT',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(493,'FLOOR',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(494,'UNIT',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(495,'TASK_MILESTONE_MODE',16,1,0,1,0,0,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(496,'TASK_STATUS',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(497,'TASK_CATEGORY',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(498,'TASK_PRIORITY',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(499,'TASK_ATTACHMENT_STATUS',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(500,'TASK',16,1,1,1,1,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(501,'MILESTONE_TEMPLATE',16,0,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(502,'MILESTONE_TEMPLATE_DETAIL',16,0,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(503,'DOCUMENT_TEMPLATE',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(504,'DOCUMENT_TEMPLATE_DETAIL',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(505,'PROJECT_TYPE',16,0,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(506,'PROJECT_CATEGORY',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(507,'PROJECT_DOCUMENT_TEMPLATE',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(508,'PROJECT_TASK_TEMPLATE',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(509,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(510,'PROJECT_TASK_TEMPLATE_DETAIL',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(511,'TASK_TEMPLATE',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(512,'TASK_TEMPLATE_DETAIL',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(513,'PROJECT_GROUP',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(514,'PROJECT_SITE',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(515,'PROJECT_TASK',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(516,'PROJECT_MILESTONE',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(517,'PROJECT_DOCUMENT',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(518,'PROJECT_DOCUMENT_UPLOADS',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(519,'PROJECT_DOCUMENT_APPROVAL',16,0,0,0,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(520,'PROJECT_DOCUMENT_RECURRING',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(521,'SITE_DOCUMENT',16,1,1,1,1,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(522,'SITE_DOCUMENT_RECURRING',16,1,1,1,1,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(523,'SITE_DOCUMENT_UPLOADS',16,1,1,1,1,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(524,'SITE_DOCUMENT_APPROVAL',16,1,1,1,1,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(525,'CONTRACTOR',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(526,'SITE_CONTRACTOR',16,1,0,1,0,1,1,'2021-12-15 15:20:14','2021-12-15 15:20:14'),(542,'USER',18,0,0,0,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(543,'ROLE',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(544,'MODULE',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(545,'ROLE_ACCESS',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(546,'SUBSCRIPTION',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(547,'CUSTOMER',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(548,'CONFIGTYPE',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(549,'COMPANY',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(550,'DEVELOPER',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(551,'STATUS',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(552,'PROJECT',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(553,'CLUSTER',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(554,'BUILDING',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(555,'DEPARTMENT',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(556,'FLOOR',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(557,'UNIT',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(558,'TASK_MILESTONE_MODE',18,1,0,1,0,0,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(559,'TASK_STATUS',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(560,'TASK_CATEGORY',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(561,'TASK_PRIORITY',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(562,'TASK_ATTACHMENT_STATUS',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(563,'TASK',18,1,1,1,1,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(564,'MILESTONE_TEMPLATE',18,0,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(565,'MILESTONE_TEMPLATE_DETAIL',18,0,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(566,'DOCUMENT_TEMPLATE',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(567,'DOCUMENT_TEMPLATE_DETAIL',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(568,'PROJECT_TYPE',18,0,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(569,'PROJECT_CATEGORY',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(570,'PROJECT_DOCUMENT_TEMPLATE',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(571,'PROJECT_TASK_TEMPLATE',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(572,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(573,'PROJECT_TASK_TEMPLATE_DETAIL',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(574,'TASK_TEMPLATE',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(575,'TASK_TEMPLATE_DETAIL',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(576,'PROJECT_GROUP',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(577,'PROJECT_SITE',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(578,'PROJECT_TASK',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(579,'PROJECT_MILESTONE',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(580,'PROJECT_DOCUMENT',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(581,'PROJECT_DOCUMENT_UPLOADS',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(582,'PROJECT_DOCUMENT_APPROVAL',18,0,0,0,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(583,'PROJECT_DOCUMENT_RECURRING',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(584,'SITE_DOCUMENT',18,1,1,1,1,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(585,'SITE_DOCUMENT_RECURRING',18,1,1,1,1,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(586,'SITE_DOCUMENT_UPLOADS',18,1,1,1,1,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(587,'SITE_DOCUMENT_APPROVAL',18,1,1,1,1,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(588,'CONTRACTOR',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(589,'SITE_CONTRACTOR',18,1,0,1,0,1,1,'2021-12-15 15:20:33','2021-12-15 15:20:33'),(605,'USER',19,0,0,0,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(606,'ROLE',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(607,'MODULE',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(608,'ROLE_ACCESS',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(609,'SUBSCRIPTION',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(610,'CUSTOMER',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(611,'CONFIGTYPE',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(612,'COMPANY',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(613,'DEVELOPER',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(614,'STATUS',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(615,'PROJECT',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(616,'CLUSTER',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(617,'BUILDING',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(618,'DEPARTMENT',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(619,'FLOOR',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(620,'UNIT',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(621,'TASK_MILESTONE_MODE',19,1,0,1,0,0,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(622,'TASK_STATUS',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(623,'TASK_CATEGORY',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(624,'TASK_PRIORITY',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(625,'TASK_ATTACHMENT_STATUS',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(626,'TASK',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(627,'MILESTONE_TEMPLATE',19,0,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(628,'MILESTONE_TEMPLATE_DETAIL',19,0,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(629,'DOCUMENT_TEMPLATE',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(630,'DOCUMENT_TEMPLATE_DETAIL',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(631,'PROJECT_TYPE',19,0,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(632,'PROJECT_CATEGORY',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(633,'PROJECT_DOCUMENT_TEMPLATE',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(634,'PROJECT_TASK_TEMPLATE',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(635,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(636,'PROJECT_TASK_TEMPLATE_DETAIL',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(637,'TASK_TEMPLATE',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(638,'TASK_TEMPLATE_DETAIL',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(639,'PROJECT_GROUP',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(640,'PROJECT_SITE',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(641,'PROJECT_TASK',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(642,'PROJECT_MILESTONE',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(643,'PROJECT_DOCUMENT',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(644,'PROJECT_DOCUMENT_UPLOADS',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(645,'PROJECT_DOCUMENT_APPROVAL',19,0,0,0,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(646,'PROJECT_DOCUMENT_RECURRING',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(647,'SITE_DOCUMENT',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(648,'SITE_DOCUMENT_RECURRING',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(649,'SITE_DOCUMENT_UPLOADS',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(650,'SITE_DOCUMENT_APPROVAL',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(651,'CONTRACTOR',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(652,'SITE_CONTRACTOR',19,1,0,1,0,1,1,'2021-12-15 15:20:55','2021-12-15 15:20:55'),(668,'USER',17,0,0,0,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(669,'ROLE',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(670,'MODULE',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(671,'ROLE_ACCESS',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(672,'SUBSCRIPTION',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(673,'CUSTOMER',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(674,'CONFIGTYPE',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(675,'COMPANY',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(676,'DEVELOPER',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(677,'STATUS',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(678,'PROJECT',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(679,'CLUSTER',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(680,'BUILDING',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(681,'DEPARTMENT',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(682,'FLOOR',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(683,'UNIT',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(684,'TASK_MILESTONE_MODE',17,1,0,1,0,0,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(685,'TASK_STATUS',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(686,'TASK_CATEGORY',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(687,'TASK_PRIORITY',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(688,'TASK_ATTACHMENT_STATUS',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(689,'TASK',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(690,'MILESTONE_TEMPLATE',17,0,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(691,'MILESTONE_TEMPLATE_DETAIL',17,0,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(692,'DOCUMENT_TEMPLATE',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(693,'DOCUMENT_TEMPLATE_DETAIL',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(694,'PROJECT_TYPE',17,0,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(695,'PROJECT_CATEGORY',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(696,'PROJECT_DOCUMENT_TEMPLATE',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(697,'PROJECT_TASK_TEMPLATE',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(698,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(699,'PROJECT_TASK_TEMPLATE_DETAIL',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(700,'TASK_TEMPLATE',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(701,'TASK_TEMPLATE_DETAIL',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(702,'PROJECT_GROUP',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(703,'PROJECT_SITE',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(704,'PROJECT_TASK',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(705,'PROJECT_MILESTONE',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(706,'PROJECT_DOCUMENT',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(707,'PROJECT_DOCUMENT_UPLOADS',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(708,'PROJECT_DOCUMENT_APPROVAL',17,0,0,0,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(709,'PROJECT_DOCUMENT_RECURRING',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(710,'SITE_DOCUMENT',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(711,'SITE_DOCUMENT_RECURRING',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(712,'SITE_DOCUMENT_UPLOADS',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(713,'SITE_DOCUMENT_APPROVAL',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(714,'CONTRACTOR',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(715,'SITE_CONTRACTOR',17,1,0,1,0,1,1,'2021-12-15 15:21:12','2021-12-15 15:21:12'),(731,'USER',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(732,'ROLE',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(733,'MODULE',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(734,'ROLE_ACCESS',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(735,'SUBSCRIPTION',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(736,'CUSTOMER',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(737,'CONFIGTYPE',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(738,'COMPANY',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(739,'DEVELOPER',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(740,'STATUS',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(741,'PROJECT',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(742,'CLUSTER',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(743,'BUILDING',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(744,'DEPARTMENT',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(745,'FLOOR',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(746,'UNIT',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(747,'TASK_MILESTONE_MODE',3,1,1,1,1,0,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(748,'TASK_STATUS',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(749,'TASK_CATEGORY',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(750,'TASK_PRIORITY',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(751,'TASK_ATTACHMENT_STATUS',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(752,'TASK',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(753,'MILESTONE_TEMPLATE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(754,'MILESTONE_TEMPLATE_DETAIL',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(755,'DOCUMENT_TEMPLATE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(756,'DOCUMENT_TEMPLATE_DETAIL',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(757,'PROJECT_TYPE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(758,'PROJECT_CATEGORY',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(759,'PROJECT_DOCUMENT_TEMPLATE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(760,'PROJECT_TASK_TEMPLATE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(761,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(762,'PROJECT_TASK_TEMPLATE_DETAIL',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(763,'TASK_TEMPLATE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(764,'TASK_TEMPLATE_DETAIL',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(765,'PROJECT_GROUP',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(766,'PROJECT_SITE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(767,'PROJECT_TASK',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(768,'PROJECT_MILESTONE',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(769,'PROJECT_DOCUMENT',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(770,'PROJECT_DOCUMENT_UPLOADS',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(771,'PROJECT_DOCUMENT_APPROVAL',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(772,'PROJECT_DOCUMENT_RECURRING',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(773,'SITE_DOCUMENT',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(774,'SITE_DOCUMENT_RECURRING',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(775,'SITE_DOCUMENT_UPLOADS',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(776,'SITE_DOCUMENT_APPROVAL',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(777,'CONTRACTOR',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(778,'SITE_CONTRACTOR',3,1,1,1,1,1,1,'2021-12-15 15:22:14','2021-12-15 15:22:14'),(794,'USER',10,1,0,1,0,0,1,'2021-12-16 14:52:54','2021-12-16 14:55:35'),(795,'ROLE',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(796,'MODULE',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(797,'ROLE_ACCESS',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(798,'SUBSCRIPTION',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(799,'CUSTOMER',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(800,'CONFIGTYPE',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(801,'COMPANY',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(802,'DEVELOPER',10,1,0,1,0,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(803,'STATUS',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(804,'PROJECT',10,1,0,1,0,1,1,'2021-12-16 14:52:54','2021-12-16 14:55:51'),(805,'CLUSTER',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(806,'BUILDING',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(807,'DEPARTMENT',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(808,'FLOOR',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(809,'UNIT',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(810,'TASK_MILESTONE_MODE',10,1,1,1,1,0,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(811,'TASK_STATUS',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(812,'TASK_CATEGORY',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(813,'TASK_PRIORITY',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(814,'TASK_ATTACHMENT_STATUS',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(815,'TASK',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(816,'MILESTONE_TEMPLATE',10,1,0,1,0,1,1,'2021-12-16 14:52:54','2021-12-16 14:55:35'),(817,'MILESTONE_TEMPLATE_DETAIL',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(818,'DOCUMENT_TEMPLATE',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(819,'DOCUMENT_TEMPLATE_DETAIL',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(820,'PROJECT_TYPE',10,1,0,1,0,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(821,'PROJECT_CATEGORY',10,1,0,1,0,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(822,'PROJECT_DOCUMENT_TEMPLATE',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(823,'PROJECT_TASK_TEMPLATE',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(824,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(825,'PROJECT_TASK_TEMPLATE_DETAIL',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(826,'TASK_TEMPLATE',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(827,'TASK_TEMPLATE_DETAIL',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(828,'PROJECT_GROUP',10,1,0,1,0,1,1,'2021-12-16 14:52:54','2021-12-16 14:55:35'),(829,'PROJECT_SITE',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(830,'PROJECT_TASK',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(831,'PROJECT_MILESTONE',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(832,'PROJECT_DOCUMENT',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(833,'PROJECT_DOCUMENT_UPLOADS',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(834,'PROJECT_DOCUMENT_APPROVAL',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(835,'PROJECT_DOCUMENT_RECURRING',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(836,'SITE_DOCUMENT',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(837,'SITE_DOCUMENT_RECURRING',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(838,'SITE_DOCUMENT_UPLOADS',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(839,'SITE_DOCUMENT_APPROVAL',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(840,'CONTRACTOR',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:55:35'),(841,'SITE_CONTRACTOR',10,1,1,1,1,1,1,'2021-12-16 14:52:54','2021-12-16 14:52:54'),(857,'USER',15,1,0,1,0,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(858,'ROLE',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(859,'MODULE',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(860,'ROLE_ACCESS',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(861,'SUBSCRIPTION',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(862,'CUSTOMER',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(863,'CONFIGTYPE',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(864,'COMPANY',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(865,'DEVELOPER',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(866,'STATUS',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(867,'PROJECT',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(868,'CLUSTER',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(869,'BUILDING',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(870,'DEPARTMENT',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(871,'FLOOR',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(872,'UNIT',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(873,'TASK_MILESTONE_MODE',15,1,1,1,1,0,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(874,'TASK_STATUS',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(875,'TASK_CATEGORY',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(876,'TASK_PRIORITY',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(877,'TASK_ATTACHMENT_STATUS',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(878,'TASK',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(879,'MILESTONE_TEMPLATE',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(880,'MILESTONE_TEMPLATE_DETAIL',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-20 22:17:18'),(881,'DOCUMENT_TEMPLATE',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(882,'DOCUMENT_TEMPLATE_DETAIL',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(883,'PROJECT_TYPE',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(884,'PROJECT_CATEGORY',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(885,'PROJECT_DOCUMENT_TEMPLATE',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(886,'PROJECT_TASK_TEMPLATE',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(887,'PROJECT_DOCUMENT_TEMPLATE_DETAIL',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(888,'PROJECT_TASK_TEMPLATE_DETAIL',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(889,'TASK_TEMPLATE',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(890,'TASK_TEMPLATE_DETAIL',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(891,'PROJECT_GROUP',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(892,'PROJECT_SITE',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:45'),(893,'PROJECT_TASK',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(894,'PROJECT_MILESTONE',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(895,'PROJECT_DOCUMENT',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(896,'PROJECT_DOCUMENT_UPLOADS',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(897,'PROJECT_DOCUMENT_APPROVAL',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(898,'PROJECT_DOCUMENT_RECURRING',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(899,'SITE_DOCUMENT',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(900,'SITE_DOCUMENT_RECURRING',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(901,'SITE_DOCUMENT_UPLOADS',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(902,'SITE_DOCUMENT_APPROVAL',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15'),(903,'CONTRACTOR',15,1,0,1,0,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:44'),(904,'SITE_CONTRACTOR',15,1,1,1,1,1,1,'2021-12-16 14:56:15','2021-12-16 14:56:15');
/*!40000 ALTER TABLE `role_accesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`),
  KEY `FK_ref_roles_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_roles_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Super Admin',1,'2021-06-05 21:14:31','2021-12-19 18:43:29'),(3,'Admin',1,'2021-06-05 21:14:31','2021-06-05 21:14:31'),(8,'Project Owner',1,'2021-07-29 19:29:32','2021-07-29 19:29:32'),(9,'Project Manager',1,'2021-07-29 19:29:32','2021-07-29 19:29:32'),(10,'Project Engineer',1,'2021-07-29 19:29:32','2021-07-29 19:29:32'),(11,'Project Lead ',1,'2021-07-29 19:29:32','2021-07-29 19:29:32'),(12,'Contractor',1,'2021-07-29 19:29:32','2021-07-29 19:29:32'),(13,'Project Member',1,'2021-07-29 19:29:32','2021-07-29 19:29:32'),(14,'Quality Assurance',1,'2021-10-12 14:17:51','2021-10-12 14:17:51'),(15,'Engineer',1,'2021-11-03 16:59:36','2021-11-03 16:59:36'),(16,'Safety',1,'2021-11-03 16:59:36','2021-11-03 16:59:36'),(17,'Purchasing',1,'2021-11-03 16:59:36','2021-11-03 16:59:36'),(18,'O&M',1,'2021-11-03 17:00:03','2021-11-03 17:00:03'),(19,'Planner',1,'2021-11-03 17:00:24','2021-11-03 17:00:24');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_document_approvals`
--

DROP TABLE IF EXISTS `site_document_approvals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_document_approvals` (
  `site_document_approval_id` bigint NOT NULL AUTO_INCREMENT,
  `site_document_id` bigint NOT NULL,
  `site_document_recurring_id` bigint DEFAULT NULL,
  `site_document_upload_id` bigint NOT NULL,
  `for_status` bigint DEFAULT NULL,
  `approval_role` bigint DEFAULT NULL,
  `approval_by` bigint DEFAULT NULL,
  `code_by_approval` tinyint DEFAULT NULL,
  `approval_comments` varchar(1000) DEFAULT NULL,
  `approval_attachments` varchar(1000) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`site_document_approval_id`),
  KEY `FK_ref_site_document_approvals_site_document_id_idx` (`site_document_id`),
  KEY `FK_ref_site_document_approvals_site_document_recurrin_idx` (`site_document_recurring_id`),
  KEY `FK_ref_site_document_approvals_for_status_idx` (`for_status`),
  KEY `FK_ref_site_document_approvals_created_by_idx` (`created_by`),
  KEY `FK_ref_site_document_approvals_approval_by_idx` (`approval_by`),
  KEY `FK_ref_site_document_approvals_site_document_approval_idx` (`site_document_upload_id`),
  KEY `FK_ref_site_document_approvals_approval_role_idx` (`approval_role`),
  CONSTRAINT `FK_ref_site_document_approvals_approval_by` FOREIGN KEY (`approval_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_site_document_approvals_approval_role` FOREIGN KEY (`approval_role`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `FK_ref_site_document_approvals_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_site_document_approvals_for_status` FOREIGN KEY (`for_status`) REFERENCES `status_documents` (`status_id`),
  CONSTRAINT `FK_ref_site_document_approvals_site_document_id` FOREIGN KEY (`site_document_id`) REFERENCES `site_documents` (`site_document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_site_document_approvals_site_document_recurring_id` FOREIGN KEY (`site_document_recurring_id`) REFERENCES `site_document_recurrings` (`site_document_recurring_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_site_document_approvals_site_document_upload_id` FOREIGN KEY (`site_document_upload_id`) REFERENCES `site_document_uploads` (`site_document_upload_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_document_approvals`
--

LOCK TABLES `site_document_approvals` WRITE;
/*!40000 ALTER TABLE `site_document_approvals` DISABLE KEYS */;
INSERT INTO `site_document_approvals` VALUES (7,10,NULL,6,1,9,5,1,NULL,NULL,5,'2022-01-19 16:16:55','2022-01-19 08:16:58'),(8,9,19,8,1,9,NULL,NULL,NULL,NULL,5,'2022-01-19 16:18:03','2022-01-19 16:18:03');
/*!40000 ALTER TABLE `site_document_approvals` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_approval_update` AFTER UPDATE ON `site_document_approvals` FOR EACH ROW BEGIN

	SET @site_id = null, @approval_count_check = 0, @null_check = 0, @milestone_id = null;
	
	SELECT site_id, milestone_id, document_type_id, document_mandatory
	INTO @site_id, @milestone_id, @document_type_id, @document_mandatory
	FROM site_documents where site_document_id = OLD.site_document_id;
    
   SET @null_check = (Select count(*) from site_document_approvals where site_document_upload_id = OLD.site_document_upload_id and code_by_approval is null);
   
   SET @approval_count_check = (Select count(*) from site_document_approvals where site_document_upload_id = OLD.site_document_upload_id and code_by_approval in ('2','3','4'));
	 
	IF ( @document_type_id = 1 ) THEN
	
		IF ( OLD.for_status = '3' AND @null_check = 0 AND @approval_count_check = 0 ) THEN
		
			IF ( @document_mandatory = 1 ) THEN
	    
				SET @site_completed_document = (Select site_completed_document from project_sites where project_id = @site_id);
						  
				SET @site_completed_document = @site_completed_document + 1;
				
				SET @project_id = null, @group_id = null;
	            
				Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
	            
				SET @group_completed_document = (Select group_completed_document from project_groups where group_id = @group_id);
	            
				SET @group_completed_document = @group_completed_document + 1;
	            
				SET @project_completed_document = (Select project_completed_document from projects where project_id = @project_id);
	            
				SET @project_completed_document = @project_completed_document + 1;
				
				SET @milestone_document_completed = (Select milestone_document_completed from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
	            
				SET @milestone_document_completed = @milestone_document_completed + 1;
						 
				Update project_sites
				SET
				site_completed_document = @site_completed_document
				Where project_id = @project_id;
	            
				Update project_groups
				SET
				group_completed_document = @group_completed_document
				Where group_id = @group_id;
	            
				Update projects
				SET
				project_completed_document = @project_completed_document
				Where project_id = @project_id;
	            
				Update project_milestones
				SET
				milestone_document_completed = @milestone_document_completed
				Where project_id = @project_id and milestone_id = @milestone_id;
			
			END IF;
			
			UPDATE site_documents
			SET 
			completed_flag = 1
			WHERE site_document_id = OLD.site_document_id;
				 
		END IF;
	
	ELSE
	
		IF ( @null_check = 0 AND @approval_count_check = 0 ) THEN
	    
	    	IF ( @document_mandatory = 1 ) THEN
	    	
				SET @site_completed_document = (Select site_completed_document from project_sites where project_id = @site_id);
				
				Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
	            
				SET @group_completed_document = (Select group_completed_document from project_groups where group_id = @group_id);
				
				SET @project_completed_document = (Select project_completed_document from projects where project_id = @project_id);
				
				SET @milestone_document_completed = (Select milestone_document_completed from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
						  
				SET @site_completed_document = @site_completed_document + 1;
				
				SET @group_completed_document = @group_completed_document + 1;
	            
				SET @project_completed_document = @project_completed_document + 1;
	      	      
				SET @milestone_document_completed = @milestone_document_completed + 1;
						 
				Update project_sites
				SET
				site_completed_document = @site_completed_document
				Where project_id = @project_id;
	            
				Update project_groups
				SET
				group_completed_document = @group_completed_document
				Where group_id = @group_id;
	            
				Update projects
				SET
				project_completed_document = @project_completed_document
				Where project_id = @project_id;
	            
				Update project_milestones
				SET
				milestone_document_completed = @milestone_document_completed
				Where project_id = @project_id and milestone_id = @milestone_id;
			
			END IF;			
			
			UPDATE site_documents
			SET 
			completed_flag = 1
			WHERE site_document_id = OLD.site_document_id;
			
			UPDATE site_document_recurrings
			SET 
			completed_flag = 1
			WHERE site_document_recurring_id = OLD.site_document_recurring_id;
				 
		END IF;
	
	END IF;
	/*
	SET @site_id = null, @approval_count_check = 0, @null_check = 0, @milestone_id = null;
 
   Select site_id, milestone_id, document_type_id, document_mandatory
	into @site_id, @milestone_id, @document_type_id, @document_mandatory
	from site_documents where site_document_id = OLD.site_document_id;
    
   SET @null_check = (Select count(*) from site_document_approvals where site_document_upload_id = OLD.site_document_upload_id and code_by_approval is null);
    
   SET @approval_count_check = (Select count(*) from site_document_approvals where site_document_upload_id = OLD.site_document_upload_id and code_by_approval in ('2','3','4'));
    
   IF ( @document_type_id = 1 ) THEN
	 
		IF ( OLD.for_status = '3' AND @null_check = 0 AND @approval_count_check = 0 AND @document_mandatory = 1 ) THEN
	    
			SET @site_completed_document = (Select site_completed_document from project_sites where site_id = @site_id);
					  
			SET @site_completed_document = @site_completed_document + 1;
					 
			Update project_sites
			SET
			site_completed_document = @site_completed_document
			Where site_id = @site_id;
	        
	      SET @project_id = null, @group_id = null;
	            
			Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
	            
			SET @group_completed_document = (Select group_completed_document from project_groups where group_id = @group_id);
	            
			SET @group_completed_document = @group_completed_document + 1;
	            
			SET @project_completed_document = (Select project_completed_document from projects where project_id = @project_id);
	            
			SET @project_completed_document = @project_completed_document + 1;
	            
			Update project_groups
			SET
			group_completed_document = @group_completed_document
			Where group_id = @group_id;
	            
			Update projects
			SET
			project_completed_document = @project_completed_document
			Where project_id = @project_id;
	            
			SET @milestone_document_completed = (Select milestone_document_completed from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
	            
			SET @milestone_document_completed = @milestone_document_completed + 1;
	            
			Update project_milestones
			SET
			milestone_document_completed = @milestone_document_completed
			Where project_id = @project_id and milestone_id = @milestone_id;
				 
		END IF;
		
	ELSE
	
		IF ( @null_check = 0 AND @approval_count_check = 0 AND @document_mandatory = 1 ) THEN
	    
			SET @site_completed_document = (Select site_completed_document from project_sites where site_id = @site_id);
					  
			SET @site_completed_document = @site_completed_document + 1;
					 
			Update project_sites
			SET
			site_completed_document = @site_completed_document
			Where site_id = @site_id;
	        
	      SET @project_id = null, @group_id = null;
	            
			Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
	            
			SET @group_completed_document = (Select group_completed_document from project_groups where group_id = @group_id);
	            
			SET @group_completed_document = @group_completed_document + 1;
	            
			SET @project_completed_document = (Select project_completed_document from projects where project_id = @project_id);
	            
			SET @project_completed_document = @project_completed_document + 1;
	            
			Update project_groups
			SET
			group_completed_document = @group_completed_document
			Where group_id = @group_id;
	            
			Update projects
			SET
			project_completed_document = @project_completed_document
			Where project_id = @project_id;
	            
			SET @milestone_document_completed = (Select milestone_document_completed from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
	            
			SET @milestone_document_completed = @milestone_document_completed + 1;
	            
			Update project_milestones
			SET
			milestone_document_completed = @milestone_document_completed
			Where project_id = @project_id and milestone_id = @milestone_id;
				 
		END IF;
	
	END IF;
	*/
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `site_document_recurrings`
--

DROP TABLE IF EXISTS `site_document_recurrings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_document_recurrings` (
  `site_document_recurring_id` bigint NOT NULL AUTO_INCREMENT,
  `site_document_id` bigint NOT NULL,
  `document_recurring_date` datetime NOT NULL,
  `milestone_id` bigint DEFAULT NULL,
  `current_status` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `document_mandatory` tinyint(1) DEFAULT '1',
  `completed_flag` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`site_document_recurring_id`),
  KEY `F_ref_site_document_recurrings_created_by_idx` (`created_by`),
  KEY `FF_ref_site_document_recurrings_site_document_id_idx` (`site_document_id`),
  KEY `FK_ref_site_document_recurrings_status_id_idx` (`current_status`),
  CONSTRAINT `FF_ref_site_document_recurrings_site_document_id` FOREIGN KEY (`site_document_id`) REFERENCES `site_documents` (`site_document_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_site_document_recurrings_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_site_document_recurrings_status_id` FOREIGN KEY (`current_status`) REFERENCES `status_documents` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_document_recurrings`
--

LOCK TABLES `site_document_recurrings` WRITE;
/*!40000 ALTER TABLE `site_document_recurrings` DISABLE KEYS */;
INSERT INTO `site_document_recurrings` VALUES (19,9,'2022-01-19 00:00:00',2,1,5,'2022-01-19 15:37:43','2022-01-19 15:37:43',1,0),(20,9,'2022-01-20 00:00:00',2,1,5,'2022-01-19 15:37:43','2022-01-19 15:37:43',1,0),(21,9,'2022-01-21 00:00:00',2,1,5,'2022-01-19 15:37:43','2022-01-19 15:37:43',1,0);
/*!40000 ALTER TABLE `site_document_recurrings` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_recurring_insert` AFTER INSERT ON `site_document_recurrings` FOR EACH ROW BEGIN

	SET @site_id = null, @document_category_id = null , @document_type_id = null, @milestone_id = null;
 
    Select site_id, document_category_id, document_type_id
    into @site_id, @document_category_id, @document_type_id
    from site_documents where site_document_id = NEW.site_document_id;
 
    SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);

    
	   IF (NEW.document_mandatory = 1) THEN
        
         Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
            
         SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);
         
         SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
         
         SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);
         
			SET @site_total_document = @site_total_document + 1;   
         SET @group_total_document = @group_total_document + 1;
         SET @project_total_document = @project_total_document + 1;
         SET @milestone_document_total = @milestone_document_total + 1;
         
         Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;
            
         Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;
            
         Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;
			
			Update project_milestones
         SET
         milestone_document_total = @milestone_document_total
         Where project_id = @project_id and milestone_id = NEW.milestone_id;
        
		END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_recurring_update` AFTER UPDATE ON `site_document_recurrings` FOR EACH ROW BEGIN
 
   Select site_id, document_category_id, document_type_id, milestone_id
   into @site_id, @document_category_id, @document_type_id, @milestone_id
   from site_documents where site_document_id = NEW.site_document_id;
	
   SELECT project_id, group_id, site_total_document INTO @project_id, @group_id, @site_total_document FROM project_sites WHERE site_id = @site_id;
 
   SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
   SET @group_total_document = (SELECT group_total_document FROM project_groups WHERE group_id = @group_id);
   SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
   
   IF ( (OLD.milestone_id != NEW.milestone_id) AND OLD.document_mandatory = NEW.document_mandatory ) THEN
   	
   	SET @old_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = OLD.milestone_id);
   		
   	SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = NEW.milestone_id);
   	
   	UPDATE project_milestones
		SET
		milestone_document_total = @old_milestone_document_total - 1
		WHERE milestone_id = OLD.milestone_id;
			
		UPDATE project_milestones
		SET
		milestone_document_total = @new_milestone_document_total + 1
		WHERE milestone_id = NEW.milestone_id;
   	
   END IF;

   IF ( ((OLD.milestone_id = NEW.milestone_id) OR (OLD.milestone_id != NEW.milestone_id)) AND OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
        
		SET @site_total_document = @site_total_document - 1;   
		SET @project_total_document = @project_total_document - 1; 
		SET @group_total_document = @group_total_document - 1; 

		Update project_sites SET site_total_document = @site_total_document  Where site_id = @site_id;
        
		Update project_groups SET group_total_document = @group_total_document Where group_id = @group_id;
		
    	Update projects SET project_total_document = @project_total_document Where project_id = @project_id;
    
    	SET @old_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = OLD.milestone_id);
   		
   	UPDATE project_milestones
		SET
		milestone_document_total = @old_milestone_document_total - 1
		WHERE milestone_id = OLD.milestone_id;
    	
   END IF;
   
   IF ( ((OLD.milestone_id = NEW.milestone_id) OR (OLD.milestone_id != NEW.milestone_id)) AND OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
      
		SET @site_total_document = @site_total_document + 1;   
		SET @project_total_document = @project_total_document + 1; 
		SET @group_total_document = @group_total_document + 1; 
		
		Update project_sites SET site_total_document = @site_total_document  Where site_id = @site_id;
        
		Update project_groups SET group_total_document = @group_total_document Where group_id = @group_id;
		
    	Update projects SET project_total_document = @project_total_document Where project_id = @project_id;
    
    	SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = NEW.milestone_id);
			
		UPDATE project_milestones
		SET
		milestone_document_total = @new_milestone_document_total + 1
		WHERE milestone_id = NEW.milestone_id;
	
   END IF;
   
 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_recurring_delete` AFTER DELETE ON `site_document_recurrings` FOR EACH ROW BEGIN

	SET @site_id = null, @document_category_id = null , @document_type_id = null, @milestone_id = null;
 
    Select site_id, document_category_id, document_type_id
    into @site_id, @document_category_id, @document_type_id
    from site_documents where site_document_id = OLD.site_document_id;
 
    SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);

    
	   IF (OLD.document_mandatory = 1) THEN
        
         Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
            
         SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);
         
         SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
         
         SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = OLD.milestone_id);
         
			SET @site_total_document = @site_total_document - 1;   
         SET @group_total_document = @group_total_document - 1;
         SET @project_total_document = @project_total_document - 1;
         SET @milestone_document_total = @milestone_document_total - 1;
         
         Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;
            
         Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;
            
         Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;
			
			Update project_milestones
         SET
         milestone_document_total = @milestone_document_total
         Where project_id = @project_id and milestone_id = OLD.milestone_id;
        
		END IF;
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `site_document_uploads`
--

DROP TABLE IF EXISTS `site_document_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_document_uploads` (
  `site_document_upload_id` bigint NOT NULL AUTO_INCREMENT,
  `site_document_id` bigint NOT NULL,
  `site_document_recurring_id` bigint DEFAULT NULL,
  `document_version` varchar(200) NOT NULL,
  `document_file` varchar(1000) NOT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`site_document_upload_id`),
  KEY `FK_ref_site_document_uploads_created_by_idx` (`created_by`),
  KEY `FK_ref_site_document_uploads_site_document_recurring__idx` (`site_document_recurring_id`),
  KEY `FK_ref_site_document_uploads_site_document_uploads_idx` (`site_document_id`),
  CONSTRAINT `FK_ref_site_document_uploads_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_site_document_uploads_site_document_recurring_id` FOREIGN KEY (`site_document_recurring_id`) REFERENCES `site_document_recurrings` (`site_document_recurring_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ref_site_document_uploads_site_document_uploads` FOREIGN KEY (`site_document_id`) REFERENCES `site_documents` (`site_document_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_document_uploads`
--

LOCK TABLES `site_document_uploads` WRITE;
/*!40000 ALTER TABLE `site_document_uploads` DISABLE KEYS */;
INSERT INTO `site_document_uploads` VALUES (6,10,NULL,'111','10_1642580215_TH-Implementation Plan.xlsx',5,'2022-01-19 16:16:55','2022-01-19 16:16:55'),(7,8,NULL,'22','8_1642580265_TH-Implementation Plan.xlsx',5,'2022-01-19 16:17:45','2022-01-19 16:17:45'),(8,9,19,'111','9_1642580283_TH-Implementation Plan.xlsx',5,'2022-01-19 16:18:03','2022-01-19 16:18:03'),(9,20,NULL,'111','20_1642751103_TH-Implementation Plan.xlsx',5,'2022-01-21 15:45:03','2022-01-21 15:45:03'),(10,20,NULL,'222','20_1642751113_TH-Implementation Plan.xlsx',5,'2022-01-21 15:45:13','2022-01-21 15:45:13'),(11,20,NULL,'33','20_1642751266_TH-Implementation Plan.xlsx',5,'2022-01-21 15:47:46','2022-01-21 15:47:46'),(15,20,NULL,'444','20_1642751361_TH-Implementation Plan.xlsx',5,'2022-01-21 15:49:21','2022-01-21 15:49:21'),(16,20,NULL,'555','20_1642751386_TH-Implementation Plan.xlsx',5,'2022-01-21 15:49:46','2022-01-21 15:49:46');
/*!40000 ALTER TABLE `site_document_uploads` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_uploads_insert` AFTER INSERT ON `site_document_uploads` FOR EACH ROW BEGIN

	SELECT document_category_id, document_mandatory, site_id, milestone_id, uploaded_flag
	INTO @document_category_id, @document_mandatory, @site_id, @milestone_id, @uploaded_flag
	FROM site_documents WHERE site_document_id = NEW.site_document_id;

	IF ( @document_category_id = 2 ) THEN
	
		IF ( @document_mandatory = 1 AND @uploaded_flag = 0  ) THEN 
		
			SELECT group_id, project_id INTO @group_id,@project_id FROM project_sites WHERE site_id = @site_id;
			SET @project_completed_document = (SELECT project_completed_document FROM projects WHERE project_id = @project_id);
			SET @group_completed_document = (SELECT group_completed_document FROM project_groups WHERE group_id = @group_id);
			SET @site_completed_document = (SELECT site_completed_document FROM project_sites WHERE site_id = @site_id);
			SET @milestone_document_completed = (Select milestone_document_completed from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
			
			SET @site_completed_document = @site_completed_document + 1;
			SET @group_completed_document = @group_completed_document + 1;
			SET @project_completed_document = @project_completed_document + 1;
			SET @milestone_document_completed = @milestone_document_completed + 1;
			
			UPDATE project_sites
			SET 
			site_completed_document = @site_completed_document
			WHERE site_id = @site_id;
			
			UPDATE project_groups
			SET 
			group_completed_document = @group_completed_document
			WHERE group_id = @group_id;
			
			UPDATE projects
			SET 
			project_completed_document = @project_completed_document
			WHERE project_id = @project_id;
			
			UPDATE project_milestones
			SET 
			milestone_document_completed = @milestone_document_completed
			WHERE project_id = @project_id and milestone_id = @milestone_id;
		
		END IF;
			
		UPDATE site_documents 
		SET 
		completed_flag = 1
		WHERE site_document_id = NEW.site_document_id;
		
	END IF;
	
	UPDATE site_documents
	SET 
	uploaded_flag = 1
	WHERE site_document_id = NEW.site_document_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `site_documents`
--

DROP TABLE IF EXISTS `site_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_documents` (
  `site_document_id` bigint NOT NULL AUTO_INCREMENT,
  `site_id` bigint DEFAULT NULL,
  `document_title` varchar(100) DEFAULT NULL,
  `document_mandatory` tinyint(1) DEFAULT '1',
  `contractor_id` bigint DEFAULT NULL,
  `assign_to_user` bigint DEFAULT NULL,
  `milestone_id` bigint DEFAULT NULL,
  `document_category_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `recurring_interval_id` bigint DEFAULT NULL,
  `recurring_start_date` datetime DEFAULT NULL,
  `recurring_end_date` datetime DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `req_approval_project_owner` tinyint(1) DEFAULT '0',
  `req_approval_project_manager` tinyint(1) DEFAULT '0',
  `req_approval_project_engineer` tinyint(1) DEFAULT '0',
  `req_approval_engineer` tinyint(1) DEFAULT '0',
  `req_approval_qa_qc` tinyint(1) DEFAULT '0',
  `req_approval_safety` tinyint(1) DEFAULT '0',
  `req_approval_onm` tinyint(1) DEFAULT '0',
  `req_approval_planner` tinyint(1) DEFAULT '0',
  `req_approval_purchasing` tinyint(1) DEFAULT '0',
  `req_approval_admin` tinyint(1) DEFAULT '0',
  `from_site_document_template_id` bigint DEFAULT NULL,
  `from_site_document_template_detail_id` bigint DEFAULT NULL,
  `completed_flag` tinyint(1) DEFAULT '0',
  `uploaded_flag` tinyint DEFAULT '0',
  PRIMARY KEY (`site_document_id`),
  KEY `FK_ref_site_documents_created_by_idx` (`created_by`),
  KEY `FK_ref_site_documents_document_type_id_idx` (`document_type_id`),
  KEY `FK_ref_site_documents_project_category_id_idx` (`document_category_id`),
  KEY `FK_ref_site_documents_recurring_interval_id_idx` (`recurring_interval_id`),
  KEY `FK_ref_site_documents_status_id_idx` (`status_id`),
  KEY `FK_ref_site_documents_assign_to_user_idx` (`assign_to_user`),
  KEY `FK_ref_site_documents_milestone_id_idx` (`milestone_id`),
  KEY `FK_ref_site_documents_site_id_idx` (`site_id`),
  KEY `FK_ref_site_documents_contractor_id_idx` (`contractor_id`),
  CONSTRAINT `FK_ref_site_documents_assign_to_user` FOREIGN KEY (`assign_to_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_site_documents_contractor_id` FOREIGN KEY (`contractor_id`) REFERENCES `project_contractors` (`contractor_id`),
  CONSTRAINT `FK_ref_site_documents_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_site_documents_document_type_id` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`document_type_id`),
  CONSTRAINT `FK_ref_site_documents_milestone_id` FOREIGN KEY (`milestone_id`) REFERENCES `project_milestones` (`milestone_id`),
  CONSTRAINT `FK_ref_site_documents_project_category_id` FOREIGN KEY (`document_category_id`) REFERENCES `document_categories` (`document_category_id`),
  CONSTRAINT `FK_ref_site_documents_recurring_interval_id` FOREIGN KEY (`recurring_interval_id`) REFERENCES `recurring_intervals` (`recurring_interval_id`),
  CONSTRAINT `FK_ref_site_documents_site_id` FOREIGN KEY (`site_id`) REFERENCES `project_sites` (`site_id`),
  CONSTRAINT `FK_ref_site_documents_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_documents` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_documents`
--

LOCK TABLES `site_documents` WRITE;
/*!40000 ALTER TABLE `site_documents` DISABLE KEYS */;
INSERT INTO `site_documents` VALUES (8,1,'document-2',1,NULL,5,1,2,1,NULL,NULL,NULL,NULL,5,'2022-01-19 15:36:47','2022-01-19 16:17:45',1,1,1,1,1,1,1,1,1,1,NULL,NULL,1,1),(9,1,'document-3',1,NULL,5,2,1,2,1,'2022-01-19 00:00:00','2022-01-21 00:00:00',NULL,5,'2022-01-19 15:37:43','2022-01-19 16:18:03',0,1,0,0,0,0,0,0,0,0,NULL,NULL,0,1),(10,1,'document-4',1,NULL,5,1,1,1,NULL,NULL,NULL,2,5,'2022-01-19 16:14:23','2022-01-19 08:16:58',0,1,0,0,0,0,0,0,0,0,NULL,NULL,0,1),(20,1,'doc-1',1,NULL,5,2,2,1,NULL,NULL,NULL,NULL,5,'2022-01-21 15:44:53','2022-01-21 15:45:03',1,1,1,1,1,1,1,1,1,1,NULL,NULL,1,1);
/*!40000 ALTER TABLE `site_documents` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_insert` AFTER INSERT ON `site_documents` FOR EACH ROW BEGIN

   SET @site_id = NEW.site_id;
    
	IF ( NEW.document_type_id = 1 AND (NEW.document_category_id = 1 OR NEW.document_category_id = 2) AND NEW.document_mandatory = 1 )THEN
	
		Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
		
		SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);
		
		SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);  
		
		SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
		
		SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);
         
		SET @site_total_document = @site_total_document + 1;   
      SET @group_total_document = @group_total_document + 1;
      SET @project_total_document = @project_total_document + 1;
      SET @milestone_document_total = @milestone_document_total + 1;
      
      Update project_sites
		SET
		site_total_document = @site_total_document
		Where site_id = @site_id;
            
      Update project_groups
		SET
		group_total_document = @group_total_document
		Where group_id = @group_id;
            
      Update projects
		SET
		project_total_document = @project_total_document
		Where project_id = @project_id;
            
      Update project_milestones
      SET
      milestone_document_total = @milestone_document_total
      Where project_id = @project_id and milestone_id = NEW.milestone_id;
		
	END IF;
         

 	/*
    SET @site_id = NEW.site_id;
 
    SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);
    
    IF NEW.document_category_id = 1 THEN
    
		IF (NEW.document_type_id = 1 AND NEW.document_mandatory = 1) THEN
        
			SET @site_total_document = @site_total_document + 1;
            
			Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;
            
            SET @project_id = null, @group_id = null;
            
            Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
            
            SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);
            
            SET @group_total_document = @group_total_document + 1;
            
            SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
            
            SET @project_total_document = @project_total_document + 1;
            
            Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;
            
            Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;
            
            SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);
            
            SET @milestone_document_total = @milestone_document_total + 1;
            
            Update project_milestones
            SET
            milestone_document_total = @milestone_document_total
            Where project_id = @project_id and milestone_id = NEW.milestone_id;
        
		END IF;
        
     END IF;
     */
         
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_assign_after_insert` AFTER INSERT ON `site_documents` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
    Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;
    
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link ) 
    VALUES (now(), NEW.assign_to_user, 'SITE DOCUMENT', @project_id, @group_id, NEW.site_id, NEW.site_document_id, NEW.document_title, 'siteDocumentEdit');
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_documents_update_milestone` AFTER UPDATE ON `site_documents` FOR EACH ROW BEGIN

	# IFA-ONE-TIME to IFA-ONE-TIME / IFI to IFI
	IF ( (OLD.document_category_id = NEW.document_category_id) AND OLD.document_type_id = 1 ) THEN
	
		IF ( (OLD.milestone_id != NEW.milestone_id) AND OLD.document_mandatory = NEW.document_mandatory ) THEN
   	
   		SET @old_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = OLD.milestone_id);
   		
   		SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = NEW.milestone_id);
   	
   		UPDATE project_milestones
			SET
			milestone_document_total = @old_milestone_document_total - 1
			WHERE milestone_id = OLD.milestone_id;
			
			UPDATE project_milestones
			SET
			milestone_document_total = @new_milestone_document_total + 1
			WHERE milestone_id = NEW.milestone_id;
   	
   	END IF;
   	
   	IF ( ((OLD.milestone_id = NEW.milestone_id) OR (OLD.milestone_id != NEW.milestone_id)) AND OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = NEW.milestone_id);
			
			UPDATE project_milestones
			SET
			milestone_document_total = @new_milestone_document_total + 1
			WHERE milestone_id = NEW.milestone_id;
   	
   	END IF;
   	
   	
   	IF ( ((OLD.milestone_id = NEW.milestone_id) OR (OLD.milestone_id != NEW.milestone_id)) AND OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @old_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = OLD.milestone_id);
   		
   		UPDATE project_milestones
			SET
			milestone_document_total = @old_milestone_document_total - 1
			WHERE milestone_id = OLD.milestone_id;
   	
   	END IF;
   
   END IF;
   
   # IFI to IFA-ONE-TIME / IFA-ONE-TIME to IFI
   IF ( ((OLD.document_category_id = 2 AND NEW.document_category_id = 1) OR (OLD.document_category_id = 1 AND NEW.document_category_id = 2)) AND (OLD.document_type_id = NEW.document_type_id) AND OLD.document_type_id = 1) THEN
   
   	IF ( (OLD.milestone_id != NEW.milestone_id) AND OLD.document_mandatory = NEW.document_mandatory ) THEN
   	
   		SET @old_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = OLD.milestone_id);
   		
   		SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = NEW.milestone_id);
   	
   		UPDATE project_milestones
			SET
			milestone_document_total = @old_milestone_document_total - 1
			WHERE milestone_id = OLD.milestone_id;
			
			UPDATE project_milestones
			SET
			milestone_document_total = @new_milestone_document_total + 1
			WHERE milestone_id = NEW.milestone_id;
   	
   	END IF;
   	
   	IF ( ((OLD.milestone_id = NEW.milestone_id) OR (OLD.milestone_id != NEW.milestone_id)) AND OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = NEW.milestone_id);
			
			UPDATE project_milestones
			SET
			milestone_document_total = @new_milestone_document_total + 1
			WHERE milestone_id = NEW.milestone_id;
   	
   	END IF;
   	
   	
   	IF ( ((OLD.milestone_id = NEW.milestone_id) OR (OLD.milestone_id != NEW.milestone_id)) AND OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @old_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = OLD.milestone_id);
   		
   		UPDATE project_milestones
			SET
			milestone_document_total = @old_milestone_document_total - 1
			WHERE milestone_id = OLD.milestone_id;
   	
   	END IF;
   
   END IF;
   
    # IFA-RECURRING & IFI to IFA
   
   IF ( OLD.document_type_id = 2 AND NEW.document_type_id = 1 ) THEN
   	
   	IF ( NEW.document_mandatory = 1 ) THEN
   	
   		SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = NEW.milestone_id);
			
			UPDATE project_milestones
			SET
			milestone_document_total = @new_milestone_document_total + 1
			WHERE milestone_id = NEW.milestone_id;
   	
   	END IF;
    
   END IF;
   
    # IFA-ONE-TIME / IFI to IFA-RECURRING
   
   IF ( OLD.document_type_id = 1 AND NEW.document_type_id = 2  ) THEN
   	
   	IF ( OLD.document_mandatory = 1 ) THEN
   	
   		SET @new_milestone_document_total = (SELECT milestone_document_total FROM project_milestones WHERE milestone_id = OLD.milestone_id);
			
			UPDATE project_milestones
			SET
			milestone_document_total = @new_milestone_document_total - 1
			WHERE milestone_id = OLD.milestone_id;
   	
   	END IF;
    
   END IF;
   

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_documents_update` AFTER UPDATE ON `site_documents` FOR EACH ROW BEGIN

	SET @site_id = NEW.site_id;
 
   SELECT site_total_document, group_id, project_id
   INTO @site_total_document, @group_id, @project_id
	FROM project_sites WHERE site_id = @site_id;
	
	SELECT group_total_document INTO @group_total_document FROM project_groups WHERE group_id = @group_id;
	SELECT project_total_document INTO @project_total_document FROM projects WHERE project_id = @project_id;
	
	# ALL COMPLETED WONT ALLOW UPDATE no need to worry about status = 3 for IFA-ONE-TIME, IFI = COMPLETED, IFA-RECURRING = COMPLETED
	# ANY CHANGE OF DOCUMENT CATEGORY OR TYPE need to DELETE all uploads
    
   # IFI to IFI and IFA-ONE-TIME to IFA-ONE-TIME
   IF ( (OLD.document_category_id = NEW.document_category_id) AND (OLD.document_type_id = NEW.document_type_id) AND OLD.document_type_id = 1) THEN
   	
   	IF ( OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @site_total_document = @site_total_document + 1;
   		SET @group_total_document = @group_total_document + 1;
   		SET @project_total_document = @project_total_document + 1;
   	
   	END IF;
   	
   	IF ( OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @site_total_document = @site_total_document - 1;
   		SET @group_total_document = @group_total_document - 1;
   		SET @project_total_document = @project_total_document - 1;
   		
   	END IF;
   	
   	UPDATE project_sites SET site_total_document = @site_total_document WHERE site_id = @site_id;
   	UPDATE project_groups SET group_total_document = @group_total_document WHERE group_id = @group_id;
  		UPDATE projects SET project_total_document = @project_total_document WHERE project_id = @project_id;	
    
   END IF;
   
   

   # IFI to IFA-ONE-TIME
   IF ( (OLD.document_category_id = 2 AND NEW.document_category_id = 1) AND (OLD.document_type_id = NEW.document_type_id) AND OLD.document_type_id = 1) THEN
   	
   	IF ( OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @site_total_document = @site_total_document + 1;
   		SET @group_total_document = @group_total_document + 1;
   		SET @project_total_document = @project_total_document + 1;
   	
   	END IF;
   	
   	IF ( OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @site_total_document = @site_total_document - 1;
   		SET @group_total_document = @group_total_document - 1;
   		SET @project_total_document = @project_total_document - 1;
   		
   	END IF;
   	
   	UPDATE project_sites SET site_total_document = @site_total_document WHERE site_id = @site_id;
   	UPDATE project_groups SET group_total_document = @group_total_document WHERE group_id = @group_id;
  		UPDATE projects SET project_total_document = @project_total_document WHERE project_id = @project_id;	
   	
   	DELETE FROM site_document_uploads WHERE site_document_id = OLD.site_document_id;
    
   END IF;
   
   
   # IFA-ONE-TIME to IFI
   IF ( (OLD.document_category_id = 1 AND NEW.document_category_id = 2) AND (OLD.document_type_id = NEW.document_type_id) AND OLD.document_type_id = 1) THEN
   	
   	IF ( OLD.document_mandatory = 0 AND NEW.document_mandatory = 1 ) THEN
   	
   		SET @site_total_document = @site_total_document + 1;
   		SET @group_total_document = @group_total_document + 1;
   		SET @project_total_document = @project_total_document + 1;
   		
   	END IF;
   	
   	IF ( OLD.document_mandatory = 1 AND NEW.document_mandatory = 0 ) THEN
   	
   		SET @site_total_document = @site_total_document - 1;
   		SET @group_total_document = @group_total_document - 1;
   		SET @project_total_document = @project_total_document - 1;
   	
   	END IF;
   	
   	UPDATE project_sites SET site_total_document = @site_total_document WHERE site_id = @site_id;
   	UPDATE project_groups SET group_total_document = @group_total_document WHERE group_id = @group_id;
  		UPDATE projects SET project_total_document = @project_total_document WHERE project_id = @project_id;	
   	
   	DELETE FROM site_document_uploads WHERE site_document_id = OLD.site_document_id;
    
   END IF;
   
   
   # IFA-RECURRING & IFI to IFA
   
   IF ( OLD.document_type_id = 2 AND NEW.document_type_id = 1 ) THEN
   
   	DELETE FROM site_document_uploads WHERE site_document_id = OLD.site_document_id;
   	
   	IF ( NEW.document_mandatory = 1 ) THEN
   	
   		SET @site_total_document = @site_total_document + 1;
   		SET @group_total_document = @group_total_document + 1;
   		SET @project_total_document = @project_total_document + 1;
   		
   		UPDATE project_sites SET site_total_document = @site_total_document WHERE site_id = @site_id;
   		UPDATE project_groups SET group_total_document = @group_total_document WHERE group_id = @group_id;
  			UPDATE projects SET project_total_document = @project_total_document WHERE project_id = @project_id;	
   	
   	END IF;
    
   END IF;
   
   # IFA-IFA & IFA-RECURRING
   
   IF ( OLD.document_type_id = 1 AND NEW.document_type_id = 2 ) THEN
   
   	DELETE FROM site_document_uploads WHERE site_document_id = OLD.site_document_id;
   	
   	IF ( OLD.document_mandatory = 1 ) THEN
   	
   		SET @site_total_document = @site_total_document - 1;
   		SET @group_total_document = @group_total_document - 1;
   		SET @project_total_document = @project_total_document - 1;
   		
   		UPDATE project_sites SET site_total_document = @site_total_document WHERE site_id = @site_id;
   		UPDATE project_groups SET group_total_document = @group_total_document WHERE group_id = @group_id;
  			UPDATE projects SET project_total_document = @project_total_document WHERE project_id = @project_id;	
   	
   	END IF;
    
   END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `site_document_assign_after_update` AFTER UPDATE ON `site_documents` FOR EACH ROW BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;
		
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link ) 
		VALUES (now(), NEW.assign_to_user, 'SITE DOCUMENT', @project_id, @group_id, NEW.site_id, NEW.site_document_id, NEW.document_title, 'siteDocumentEdit');
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `site_documents_deleted`
--

DROP TABLE IF EXISTS `site_documents_deleted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_documents_deleted` (
  `site_document_id` bigint NOT NULL AUTO_INCREMENT,
  `site_id` bigint DEFAULT NULL,
  `document_title` varchar(100) DEFAULT NULL,
  `document_mandatory` tinyint(1) DEFAULT '1',
  `contractor_id` bigint DEFAULT NULL,
  `assign_to_user` bigint DEFAULT NULL,
  `milestone_id` bigint DEFAULT NULL,
  `document_category_id` bigint DEFAULT NULL,
  `document_type_id` bigint DEFAULT NULL,
  `recurring_interval_id` bigint DEFAULT NULL,
  `recurring_start_date` datetime DEFAULT NULL,
  `recurring_end_date` datetime DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `req_approval_project_owner` tinyint(1) DEFAULT '0',
  `req_approval_project_manager` tinyint(1) DEFAULT '0',
  `req_approval_project_engineer` tinyint(1) DEFAULT '0',
  `req_approval_engineer` tinyint(1) DEFAULT '0',
  `req_approval_qa_qc` tinyint(1) DEFAULT '0',
  `req_approval_safety` tinyint(1) DEFAULT '0',
  `req_approval_onm` tinyint(1) DEFAULT '0',
  `req_approval_planner` tinyint(1) DEFAULT '0',
  `req_approval_purchasing` tinyint(1) DEFAULT '0',
  `req_approval_admin` tinyint(1) DEFAULT '0',
  `from_site_document_template_id` bigint DEFAULT NULL,
  `from_site_document_template_detail_id` bigint DEFAULT NULL,
  `completed_flag` tinyint(1) DEFAULT '0',
  `deleted_by` bigint DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`site_document_id`),
  KEY `FK_ref_site_documents_created_by_idx` (`created_by`),
  KEY `FK_ref_site_documents_document_type_id_idx` (`document_type_id`),
  KEY `FK_ref_site_documents_project_category_id_idx` (`document_category_id`),
  KEY `FK_ref_site_documents_recurring_interval_id_idx` (`recurring_interval_id`),
  KEY `FK_ref_site_documents_status_id_idx` (`status_id`),
  KEY `FK_ref_site_documents_assign_to_user_idx` (`assign_to_user`),
  KEY `FK_ref_site_documents_milestone_id_idx` (`milestone_id`),
  KEY `FK_ref_site_documents_site_id_idx` (`site_id`),
  KEY `FK_ref_site_documents_contractor_id_idx` (`contractor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_documents_deleted`
--

LOCK TABLES `site_documents_deleted` WRITE;
/*!40000 ALTER TABLE `site_documents_deleted` DISABLE KEYS */;
INSERT INTO `site_documents_deleted` VALUES (1,1,'document-1',1,NULL,5,1,1,1,NULL,NULL,NULL,1,5,'2022-01-17 16:29:07','2022-01-17 09:18:27',0,1,0,0,0,0,0,0,0,0,NULL,NULL,0,1,'2022-01-17 17:19:21'),(2,1,'document-2',1,NULL,5,2,2,1,NULL,NULL,NULL,NULL,5,'2022-01-17 16:52:45','2022-01-17 16:52:45',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-17 17:10:20'),(3,1,'document-3',1,NULL,5,2,1,2,1,'2022-01-17 00:00:00','2022-01-22 00:00:00',NULL,5,'2022-01-17 17:20:45','2022-01-17 17:20:45',1,1,1,1,0,1,1,1,1,1,NULL,NULL,0,1,'2022-01-17 17:21:03'),(4,1,'document-4',1,NULL,5,1,1,2,1,'2022-01-17 00:00:00','2022-01-20 00:00:00',NULL,5,'2022-01-17 17:23:00','2022-01-17 17:23:00',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-17 17:24:44'),(5,1,'document-1',1,NULL,5,1,1,2,1,'2022-01-17 00:00:00','2022-01-20 00:00:00',NULL,5,'2022-01-17 23:42:02','2022-01-17 23:42:02',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-17 23:42:15'),(6,1,'document-1',1,NULL,5,2,1,2,1,'2022-01-17 00:00:00','2022-01-20 00:00:00',NULL,5,'2022-01-17 23:43:42','2022-01-17 23:43:42',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,1,'2022-01-17 23:44:00'),(7,1,'document-1',1,NULL,5,1,1,1,NULL,NULL,NULL,2,5,'2022-01-19 15:36:22','2022-01-19 16:17:23',0,1,0,0,0,0,0,0,0,0,NULL,NULL,0,5,'2022-01-19 18:20:20'),(11,1,'document-5',1,NULL,5,2,2,1,NULL,NULL,NULL,NULL,5,'2022-01-19 16:14:47','2022-01-19 16:14:47',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 18:21:08'),(12,1,'document-6',1,NULL,5,1,1,2,1,'2022-01-19 00:00:00','2022-01-21 00:00:00',NULL,5,'2022-01-19 16:15:15','2022-01-19 16:15:15',0,1,0,0,0,0,0,0,0,0,NULL,NULL,0,5,'2022-01-19 18:20:50'),(13,1,'document-7',1,NULL,5,1,1,2,1,'2022-01-19 00:00:00','2022-01-26 00:00:00',NULL,5,'2022-01-19 18:21:58','2022-01-19 10:22:36',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 18:22:56'),(14,1,'n-doc-1',1,NULL,5,1,2,1,NULL,NULL,NULL,NULL,5,'2022-01-19 21:02:43','2022-01-19 21:02:43',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 21:08:11'),(15,1,'n-doc-2',1,NULL,5,2,1,1,NULL,NULL,NULL,1,5,'2022-01-19 21:03:01','2022-01-19 21:03:01',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 21:08:16'),(16,1,'n-doc-3',1,NULL,5,2,1,2,1,'2022-01-19 00:00:00','2022-01-21 00:00:00',NULL,5,'2022-01-19 21:04:15','2022-01-19 21:04:15',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 21:08:21'),(17,1,'a-doc-1',1,NULL,5,2,2,1,NULL,NULL,NULL,NULL,5,'2022-01-19 21:13:25','2022-01-19 13:13:47',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 21:16:41'),(18,1,'a-doc-2',1,NULL,5,2,1,1,NULL,NULL,NULL,1,5,'2022-01-19 21:14:02','2022-01-19 21:14:02',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 21:16:50'),(19,1,'a-doc-3',1,NULL,5,2,1,2,1,'2022-01-19 00:00:00','2022-01-21 00:00:00',NULL,5,'2022-01-19 21:14:25','2022-01-19 21:14:25',1,1,1,1,1,1,1,1,1,1,NULL,NULL,0,5,'2022-01-19 21:16:55');
/*!40000 ALTER TABLE `site_documents_deleted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_documents`
--

DROP TABLE IF EXISTS `status_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_documents` (
  `status_id` bigint NOT NULL AUTO_INCREMENT,
  `status_code` varchar(50) DEFAULT NULL,
  `status_info` varchar(100) DEFAULT NULL,
  `status_sequence` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`status_id`),
  KEY `FK_ref_status_documents_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_status_documents_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_documents`
--

LOCK TABLES `status_documents` WRITE;
/*!40000 ALTER TABLE `status_documents` DISABLE KEYS */;
INSERT INTO `status_documents` VALUES (1,'IFA Status','IFA Status',1,1,'2021-09-07 15:16:11','2021-11-15 15:57:46',1),(2,'IFC Status','IFC Status',2,1,'2021-09-07 15:16:11','2021-11-15 15:57:46',1),(3,'ASB Status','ASB Status',3,1,'2021-09-07 15:16:11','2021-11-17 16:35:25',1);
/*!40000 ALTER TABLE `status_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_groups`
--

DROP TABLE IF EXISTS `status_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_groups` (
  `status_id` bigint NOT NULL AUTO_INCREMENT,
  `status_code` varchar(50) DEFAULT NULL,
  `status_info` varchar(100) DEFAULT NULL,
  `status_sequence` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`status_id`),
  KEY `FK_ref_status_group_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_status_group_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_groups`
--

LOCK TABLES `status_groups` WRITE;
/*!40000 ALTER TABLE `status_groups` DISABLE KEYS */;
INSERT INTO `status_groups` VALUES (1,'Pending','Pending Status for group',1,1,'2021-08-03 08:30:46','2021-08-15 23:58:16',1),(2,'Work-In-Progress','Work In Progress Status for group',2,1,'2021-08-03 08:38:28','2021-08-15 23:58:16',1),(3,'On-Hold','On-Hold Status for group',3,1,'2021-08-03 08:38:28','2021-08-15 23:58:16',1),(4,'Delayed','Delayed Status for group',4,1,'2021-08-03 08:38:28','2021-08-15 23:58:16',1),(5,'Completed','Reviewed Status for group',5,1,'2021-08-03 08:38:28','2021-08-15 23:58:16',1),(6,'Reviewed','Reviewed Status for group',6,1,'2021-08-03 08:38:28','2021-08-15 23:58:16',1),(7,'Approved','Approved Status for group',7,1,'2021-08-03 08:38:28','2021-08-15 23:58:16',1);
/*!40000 ALTER TABLE `status_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_projects`
--

DROP TABLE IF EXISTS `status_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_projects` (
  `status_id` bigint NOT NULL AUTO_INCREMENT,
  `status_code` varchar(50) DEFAULT NULL,
  `status_info` varchar(100) DEFAULT NULL,
  `status_sequence` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`status_id`),
  KEY `FK_ref_sem_status_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_sem_status_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_projects`
--

LOCK TABLES `status_projects` WRITE;
/*!40000 ALTER TABLE `status_projects` DISABLE KEYS */;
INSERT INTO `status_projects` VALUES (1,'Pending','Pending Status for project',1,1,'2021-08-03 08:30:46','2021-11-15 15:59:58',1),(2,'Work-In-Progress','Work In Progress Status for project',2,1,'2021-08-03 08:38:28','2021-11-15 15:59:58',1),(3,'On-Hold','On-Hold Status for project',3,1,'2021-08-03 08:38:28','2021-11-15 15:59:58',1),(4,'Delayed','Delayed Status for project',4,1,'2021-08-03 08:38:28','2021-08-03 08:38:28',1),(5,'Completed','Reviewed Status for project',5,1,'2021-08-03 08:38:28','2021-08-03 12:32:42',1),(6,'Reviewed','Reviewed Status for project',6,1,'2021-08-03 08:38:28','2021-08-03 12:32:42',1),(7,'Approved','Approved Status for project',7,1,'2021-08-03 08:38:28','2021-08-03 08:38:28',1);
/*!40000 ALTER TABLE `status_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_sites`
--

DROP TABLE IF EXISTS `status_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_sites` (
  `status_id` bigint NOT NULL AUTO_INCREMENT,
  `status_code` varchar(50) DEFAULT NULL,
  `status_info` varchar(100) DEFAULT NULL,
  `status_sequence` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`status_id`),
  KEY `FK_ref_status_sites_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_status_sites_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_sites`
--

LOCK TABLES `status_sites` WRITE;
/*!40000 ALTER TABLE `status_sites` DISABLE KEYS */;
INSERT INTO `status_sites` VALUES (1,'Pending','Pending Status for Site',1,1,'2021-08-03 08:30:46','2021-08-15 23:57:40',1),(2,'Work-In-Progress','Work In Progress Status for Site',2,1,'2021-08-03 08:38:28','2021-08-15 23:57:40',1),(3,'On-Hold','On-Hold Status for Site',3,1,'2021-08-03 08:38:28','2021-08-15 23:57:40',1),(4,'Delayed','Delayed Status for Site',4,1,'2021-08-03 08:38:28','2021-08-15 23:57:40',1),(5,'Completed','Completed Status for Site',5,1,'2021-08-03 08:38:28','2021-09-24 13:12:21',1),(6,'Reviewed','Reviewed Status for Site',6,1,'2021-08-03 08:38:28','2021-08-15 23:57:40',1),(7,'Approved','Approved Status for Site',7,1,'2021-08-03 08:38:28','2021-08-15 23:57:40',1);
/*!40000 ALTER TABLE `status_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_tasks`
--

DROP TABLE IF EXISTS `status_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_tasks` (
  `status_id` bigint NOT NULL AUTO_INCREMENT,
  `status_code` varchar(50) DEFAULT NULL,
  `status_info` varchar(100) DEFAULT NULL,
  `status_sequence` int DEFAULT '0',
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`status_id`),
  KEY `FK_ref_status_tasks_created_by_idx` (`created_by`),
  CONSTRAINT `FK_ref_status_tasks_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_tasks`
--

LOCK TABLES `status_tasks` WRITE;
/*!40000 ALTER TABLE `status_tasks` DISABLE KEYS */;
INSERT INTO `status_tasks` VALUES (1,'Pending','Pending Status for Task',0,NULL,'2021-08-27 17:11:26','2021-08-27 17:11:26',1),(2,'Work In Progress','Work In Progress Status for Task',1,NULL,'2021-08-27 17:11:26','2021-08-27 17:11:26',1),(3,'Completed','Completed Status for Task',2,NULL,'2021-08-27 17:13:21','2021-08-27 17:13:21',1),(4,'Approved','Approved Status for Task',3,NULL,'2021-08-27 17:13:21','2021-08-27 17:13:21',1),(5,'On-Hold','On-Hold Status for Task',4,NULL,'2021-08-27 17:13:21','2021-08-27 17:13:21',1),(6,'Re-Schedule','Re-Schedule Status for Task',5,NULL,'2021-08-27 17:13:21','2021-08-27 17:13:21',1),(7,'Delayed','Delayed Status for Task',6,NULL,'2021-08-27 17:13:21','2021-08-27 17:13:21',1);
/*!40000 ALTER TABLE `status_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_template_details`
--

DROP TABLE IF EXISTS `task_template_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task_template_details` (
  `task_template_detail_id` bigint NOT NULL AUTO_INCREMENT,
  `task_template_id` bigint DEFAULT NULL,
  `task_template_title` varchar(100) DEFAULT NULL,
  `task_description` varchar(300) DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  `milestone_template_detail_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`task_template_detail_id`),
  KEY `FK_ref_task_template_details_created_by_idx` (`created_by`),
  KEY `FK_ref_task_template_details_task_template_id_idx` (`task_template_id`),
  KEY `FK_ref_task_template_details_milestone_template_detail__idx` (`milestone_template_detail_id`),
  KEY `FK_ref_task_template_details_status_id_idx` (`status_id`),
  CONSTRAINT `FK_ref_task_template_details_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_task_template_details_milestone_template_detail_id` FOREIGN KEY (`milestone_template_detail_id`) REFERENCES `milestone_template_details` (`milestone_template_detail_id`),
  CONSTRAINT `FK_ref_task_template_details_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_tasks` (`status_id`),
  CONSTRAINT `FK_ref_task_template_details_task_template_id` FOREIGN KEY (`task_template_id`) REFERENCES `task_templates` (`task_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_template_details`
--

LOCK TABLES `task_template_details` WRITE;
/*!40000 ALTER TABLE `task_template_details` DISABLE KEYS */;
INSERT INTO `task_template_details` VALUES (1,1,'task-1','task-1',1,1,5,'2022-01-06 06:50:18','2022-01-06 06:50:18',1,NULL);
/*!40000 ALTER TABLE `task_template_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `task_template_details_insert` AFTER INSERT ON `task_template_details` FOR EACH ROW BEGIN
  
	Select task_details_no into @task_details_no from task_templates where task_template_id = NEW.task_template_id;
    
    SET @task_details_no = @task_details_no + 1;
    
    Update task_templates
    SET
    task_details_no = @task_details_no
    Where task_template_id = NEW.task_template_id;
       
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`web_db`@`%`*/ /*!50003 TRIGGER `task_template_details_delete` AFTER DELETE ON `task_template_details` FOR EACH ROW BEGIN
  
	Select task_details_no into @task_details_no from task_templates where task_template_id = OLD.task_template_id;
    
    SET @task_details_no = @task_details_no - 1;
    
    Update task_templates
    SET
    task_details_no = @task_details_no
    Where task_template_id = OLD.task_template_id;
       
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `task_templates`
--

DROP TABLE IF EXISTS `task_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task_templates` (
  `task_template_id` bigint NOT NULL AUTO_INCREMENT,
  `task_template_name` varchar(100) DEFAULT NULL,
  `task_details_no` int DEFAULT '0',
  `milestone_template_id` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`task_template_id`),
  KEY `FK_ref_task_templates_created_by_idx` (`created_by`),
  KEY `FK_ref_task_templates_milestone_template_id_idx` (`milestone_template_id`),
  CONSTRAINT `FK_ref_task_templates_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_task_templates_milestone_template_id` FOREIGN KEY (`milestone_template_id`) REFERENCES `milestone_templates` (`milestone_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_templates`
--

LOCK TABLES `task_templates` WRITE;
/*!40000 ALTER TABLE `task_templates` DISABLE KEYS */;
INSERT INTO `task_templates` VALUES (1,'site-task-template-1',1,1,5,'2022-01-06 06:50:02','2022-01-06 06:50:18');
/*!40000 ALTER TABLE `task_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tasks` (
  `task_id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY `FKProjeect_idx` (`project_id`),
  CONSTRAINT `FKProjeect` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,1);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `customer_id` bigint DEFAULT NULL,
  `role_id` bigint DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contractor_id` bigint DEFAULT NULL,
  `belongs_to_account` bigint DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active_status` tinyint(1) DEFAULT '1',
  `deleted_status` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `two_factor_secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `two_factor_confirmed` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `FK_ref_users_created_by_idx` (`created_by`),
  KEY `FK_ref_users_role_id` (`role_id`),
  KEY `FK_ref_users_role_id_idx` (`contractor_id`),
  CONSTRAINT `FK_ref_users_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_ref_users_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,3,'ken.wee@imttech.co','ken.wee@imttech.co',NULL,'$2y$10$UzqYZi7ltwVt60zcOv1waOuN/mmrsnh/WWi2LSOQM3wmeYGAvtmGu',NULL,NULL,1,1,'2021-05-09 23:16:45','2022-01-06 21:41:47',1,0,NULL,'eyJpdiI6ImVuWlJ1OHZUa1ptcndRQ2k5UklXR1E9PSIsInZhbHVlIjoiajB5RWhHN045NzdiTkVlTHM1YURkb21hME5QNFVmRER1MGJ4SUZYUXA3Yz0iLCJtYWMiOiJhOTVhZTc0MTYzMTExODI4N2Q5ODE3MDBkMjZiOWJjNGQyMDVkOTNiMmRjMDdmNjU0MDI2MWJjMWU0MmExNTc1IiwidGFnIjoiIn0=','eyJpdiI6IkhRejdkTlJhV2xyV004NWE2cFloWEE9PSIsInZhbHVlIjoia2UyeFIwOEdkc1p0MEpLMEJqRC9KSXNCRXlQcmNqUUNUaXlFU1ZIbDJNS1k4U1Jqbm1tejh4cHZKdWtUZU5ocGdFdVVuTEg1cVlkUkxIWGpQdGhqZGJPV1EwYnluTGRJRDFlVU9IRVFmN0NuT3VoQkdMQjVhL05zMi9STS9PVnhKVXBnVWEwdkFBRFpSeVF6Y3R6WFJZS0N0V1packlRZDRwSDhxdUFmMUJJYkgwVlp6MUdTZUJPb0g4LzNGWmpiaFRYT0Y1Sm1SS09mTUcvcVF3dGtTQ0c3UkJlVVBkd29VR2R3UWczV1FVSUZyWnBBUkVGNkRzWVBzOXJVaXpMQ21rK1pBMmhISjJnVFdSOGdHR21QQmc9PSIsIm1hYyI6IjJhNzY3YWY0ODEwN2M3NjZmNDY4ZGIxNWIyMGNjNDViODY4ODZiODIyNjQ4MTgzMjZiOGYyODE2OWQzMmI5N2EiLCJ0YWciOiIifQ==',1),(2,NULL,3,'admin@imttech.co','admin@imttech.co',NULL,'$2y$10$737DBEMPZYJkB2HJzoAbKervt1xeMNxyalLwRQ2/rKb0OL0EuKVzq',NULL,NULL,NULL,1,'2021-12-18 15:21:45','2022-01-06 21:42:04',1,0,NULL,NULL,NULL,NULL),(3,NULL,15,'user001@imttech.co','user001@imttech.co',NULL,'$2y$10$2nEfF6Bf39RYmR6oxDNru.n4aZxMO6HDIxtoc1..a496XThQ7KnT6',NULL,NULL,NULL,1,'2022-01-06 13:33:28','2022-01-06 21:50:29',1,0,NULL,NULL,NULL,NULL),(4,NULL,10,'project.engineer@imttech.co','project.engineer@imttech.co',NULL,'$2y$10$UqQSI0zykisCn00.KadAqO99oVsRoPbd1jtxLP7XvrE570rykuo3K',NULL,NULL,NULL,2,'2022-01-06 14:08:14','2022-01-06 14:08:14',1,0,NULL,NULL,NULL,NULL),(5,NULL,9,'project.manager@imttech.co','project.manager@imttech.co',NULL,'$2y$10$p1L1.HSCCXz0KqSdDg6yQOuPMsn33Qd/LCZBLJ64mKnBsvE0lwdmi',NULL,NULL,NULL,2,'2022-01-06 14:08:43','2022-01-06 14:08:43',1,0,NULL,NULL,NULL,NULL),(6,NULL,16,'safety@imttech.co','safety@imttech.co',NULL,'$2y$10$mwWFG1nGvKN1JcyJwnWgZOt5JTaimuEyXpUIzYg4J.a3qVFeP2F5C',NULL,NULL,NULL,2,'2022-01-06 14:10:35','2022-01-06 14:10:35',1,0,NULL,NULL,NULL,NULL),(7,NULL,12,'contractor.01@imttech.co','contractor.01@imttech.co',NULL,'$2y$10$6wU9fKJEWwLXduoCD.sVZO8xynyXAfjDBowccM/qRPL2ifdojgd9q',NULL,1,NULL,2,'2022-01-06 14:45:37','2022-01-06 14:45:37',1,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_consolidated_daily_document_overdue`
--

DROP TABLE IF EXISTS `v_consolidated_daily_document_overdue`;
/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_document_overdue`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_consolidated_daily_document_overdue` AS SELECT 
 1 AS `for_date`,
 1 AS `total_overdue`,
 1 AS `total_overdue_below_30`,
 1 AS `total_overdue_30_to_60`,
 1 AS `total_overdue_60_to_90`,
 1 AS `total_overdue_above_90`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_consolidated_daily_document_summary`
--

DROP TABLE IF EXISTS `v_consolidated_daily_document_summary`;
/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_document_summary`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_consolidated_daily_document_summary` AS SELECT 
 1 AS `for_date`,
 1 AS `document_total`,
 1 AS `document_completed`,
 1 AS `document_pending`,
 1 AS `document_overdue`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_consolidated_daily_overdue_projects`
--

DROP TABLE IF EXISTS `v_consolidated_daily_overdue_projects`;
/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_overdue_projects`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_consolidated_daily_overdue_projects` AS SELECT 
 1 AS `for_date`,
 1 AS `total_site`,
 1 AS `total_overdue_below_30`,
 1 AS `total_overdue_30_to_60`,
 1 AS `total_overdue_60_to_90`,
 1 AS `total_overdue_above_90`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_consolidated_daily_overdue_turn_on`
--

DROP TABLE IF EXISTS `v_consolidated_daily_overdue_turn_on`;
/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_overdue_turn_on`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_consolidated_daily_overdue_turn_on` AS SELECT 
 1 AS `for_date`,
 1 AS `mwp_total_overdue`,
 1 AS `mwp_below_1`,
 1 AS `mwp_1_to_5`,
 1 AS `mwp_5_to_10`,
 1 AS `mwp_above_10`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_consolidated_daily_project_turn_on`
--

DROP TABLE IF EXISTS `v_consolidated_daily_project_turn_on`;
/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_project_turn_on`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_consolidated_daily_project_turn_on` AS SELECT 
 1 AS `for_date`,
 1 AS `project_total`,
 1 AS `pending_total`,
 1 AS `completed_total`,
 1 AS `overdue_total`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_daily_project_milestones`
--

DROP TABLE IF EXISTS `v_daily_project_milestones`;
/*!50001 DROP VIEW IF EXISTS `v_daily_project_milestones`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_daily_project_milestones` AS SELECT 
 1 AS `for_date`,
 1 AS `milestone_code`,
 1 AS `milestone_task_total`,
 1 AS `milestone_task_pending`,
 1 AS `milestone_task_completed`,
 1 AS `milestone_document_pending`,
 1 AS `milestone_document_total`,
 1 AS `milestone_document_completed`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_project_document_recurring_with_total_upload`
--

DROP TABLE IF EXISTS `v_project_document_recurring_with_total_upload`;
/*!50001 DROP VIEW IF EXISTS `v_project_document_recurring_with_total_upload`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_project_document_recurring_with_total_upload` AS SELECT 
 1 AS `project_document_recurring_id`,
 1 AS `project_document_id`,
 1 AS `document_recurring_date`,
 1 AS `current_status`,
 1 AS `created_by`,
 1 AS `created_at`,
 1 AS `updated_at`,
 1 AS `document_mandatory`,
 1 AS `completed_flag`,
 1 AS `total_upload`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_project_group_site_contractors`
--

DROP TABLE IF EXISTS `v_project_group_site_contractors`;
/*!50001 DROP VIEW IF EXISTS `v_project_group_site_contractors`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_project_group_site_contractors` AS SELECT 
 1 AS `site_contractor_id`,
 1 AS `site_id`,
 1 AS `contractor_id`,
 1 AS `created_by`,
 1 AS `created_at`,
 1 AS `updated_at`,
 1 AS `active_status`,
 1 AS `contractor_code`,
 1 AS `site_name`,
 1 AS `group_id`,
 1 AS `group_name`,
 1 AS `project_id`,
 1 AS `project_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_project_group_site_document`
--

DROP TABLE IF EXISTS `v_project_group_site_document`;
/*!50001 DROP VIEW IF EXISTS `v_project_group_site_document`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_project_group_site_document` AS SELECT 
 1 AS `project_name`,
 1 AS `group_id`,
 1 AS `group_name`,
 1 AS `site_name`,
 1 AS `site_document_id`,
 1 AS `project_id`,
 1 AS `site_id`,
 1 AS `document_title`,
 1 AS `document_mandatory`,
 1 AS `document_type_id`,
 1 AS `document_category_id`,
 1 AS `assign_to_user`,
 1 AS `milestone_id`,
 1 AS `status_id`,
 1 AS `created_by`,
 1 AS `created_at`,
 1 AS `updated_at`,
 1 AS `from_site_document_template_id`,
 1 AS `from_site_document_template_detail_id`,
 1 AS `uploaded_flag`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_project_group_site_task`
--

DROP TABLE IF EXISTS `v_project_group_site_task`;
/*!50001 DROP VIEW IF EXISTS `v_project_group_site_task`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_project_group_site_task` AS SELECT 
 1 AS `project_id`,
 1 AS `project_name`,
 1 AS `group_id`,
 1 AS `group_name`,
 1 AS `site_id`,
 1 AS `site_name`,
 1 AS `task_id`,
 1 AS `task_code`,
 1 AS `task_title`,
 1 AS `assign_to_user`,
 1 AS `milestone_id`,
 1 AS `contractor_id`,
 1 AS `task_description`,
 1 AS `task_remarks`,
 1 AS `task_progress`,
 1 AS `status_id`,
 1 AS `task_est_start_date`,
 1 AS `task_est_end_date`,
 1 AS `task_start_date`,
 1 AS `task_end_date`,
 1 AS `created_by`,
 1 AS `created_at`,
 1 AS `updated_at`,
 1 AS `from_task_template_id`,
 1 AS `from_task_template_detail_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_project_milestone_progress`
--

DROP TABLE IF EXISTS `v_project_milestone_progress`;
/*!50001 DROP VIEW IF EXISTS `v_project_milestone_progress`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_project_milestone_progress` AS SELECT 
 1 AS `milestone_id`,
 1 AS `project_id`,
 1 AS `milestone_code`,
 1 AS `milestone_info`,
 1 AS `milestone_sequence`,
 1 AS `created_by`,
 1 AS `created_at`,
 1 AS `updated_at`,
 1 AS `active_status`,
 1 AS `milestone_progress`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_project_s_curves_display`
--

DROP TABLE IF EXISTS `v_project_s_curves_display`;
/*!50001 DROP VIEW IF EXISTS `v_project_s_curves_display`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_project_s_curves_display` AS SELECT 
 1 AS `project_id`,
 1 AS `month_of`,
 1 AS `expected_total_task`,
 1 AS `actual_total_task`,
 1 AS `expected_total_document`,
 1 AS `actual_total_document`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'semcorp_db_01'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `daily_report_generate` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`web_db`@`%`*/ /*!50106 EVENT `daily_report_generate` ON SCHEDULE EVERY 1 DAY STARTS '2020-01-01 16:01:00' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
 
	CALL sp_make_top_overdue(now());
	CALL sp_make_daily_project_milestone_report(now());
	CALL sp_make_daily_document_summary(now());
	CALL sp_make_project_overdue_report(now());
	CALL sp_make_project_turn_on(now());
    
END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'semcorp_db_01'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_daily_assign` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_daily_assign`(
assign_date_x date
)
BEGIN

	select da.daily_assign_id, da.assign_date, da.assign_to_user_id, u.name, u.email, 
	da.assign_module_code, p.project_name, g.group_name, s.site_name,
	da.assign_to_title, CONCAT(da.assign_link,'/',da.assign_to_id) as assign_link
	from daily_assigns as da inner join users as u
	on u.id = da.assign_to_user_id 	left join projects as p
	on p.project_id = da.project_id left join project_groups as g
	on g.group_id = da.group_id left join project_sites as s
	on s.site_id = da.site_id where date_format(da.assign_date, '%Y-%m-%e')  = assign_date_x and completed_flag = 0 order by da.assign_to_id ASC;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_document_overdue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_document_overdue`(
project_type_id_x bigint,
input_date_x date
)
BEGIN

	SET @check_count = 0;
    
    IF ( project_type_id_x = 0 ) THEN    
		SET @check_count = (Select count(*) from v_consolidated_daily_document_overdue where for_date = input_date_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'total_overdue', 0 as 'total_overdue_below_30', 0 as 'total_overdue_30_to_60', 0 as 'total_overdue_60_to_90', 0 as 'total_overdue_above_90';
        ELSE
			Select * from v_consolidated_daily_document_overdue where for_date = input_date_x;
        END IF;
	ELSE
		SET @check_count = (Select count(*) from daily_document_overdue where for_date = input_date_x and project_type_id = project_type_id_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'total_overdue', 0 as 'total_overdue_below_30', 0 as 'total_overdue_30_to_60', 0 as 'total_overdue_60_to_90', 0 as 'total_overdue_above_90';
        ELSE
			Select for_date, (total_overdue_below_30 + total_overdue_30_to_60 + total_overdue_60_to_90 + total_overdue_above_90) as 'total_overdue',
            total_overdue_below_30, total_overdue_30_to_60, total_overdue_60_to_90, total_overdue_above_90
			from daily_document_overdue where for_date = input_date_x and project_type_id = project_type_id_x;
        END IF;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_document_summary` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_document_summary`(
project_type_id_x bigint,
input_date_x date
)
BEGIN

	SET @check_count = 0;
    
    IF ( project_type_id_x = 0 ) THEN    
		SET @check_count = (Select count(*) from v_consolidated_daily_document_summary where for_date = input_date_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'document_total', 0 as 'document_completed', 0 as 'document_pending', 0 as 'document_overdue';
        ELSE
			Select * from v_consolidated_daily_document_summary where for_date = input_date_x;
        END IF;
	ELSE
		SET @check_count = (Select count(*) from daily_document_summary where for_date = input_date_x and project_type_id = project_type_id_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'document_total', 0 as 'document_completed', 0 as 'document_pending', 0 as 'document_overdue';
        ELSE
			Select for_date, document_total, document_completed, document_pending, document_overdue
			from daily_document_summary where for_date = input_date_x and project_type_id = project_type_id_x;
        END IF;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_overdue_project` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_overdue_project`(
project_type_id_x bigint,
input_date_x date
)
BEGIN

	SET @check_count = 0;
    
    IF ( project_type_id_x = 0 ) THEN    
		SET @check_count = (Select count(*) from v_consolidated_daily_overdue_projects where for_date = input_date_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'total_site', 0 as 'total_overdue_below_30', 0 as 'total_overdue_30_to_60', 0 as 'total_overdue_60_to_90', 0 as 'total_overdue_above_90';
        ELSE
			Select * from v_consolidated_daily_overdue_projects where for_date = input_date_x;
        END IF;
	ELSE
		SET @check_count = (Select count(*) from daily_overdue_projects where for_date = input_date_x and project_type_id = project_type_id_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'total_site', 0 as 'total_overdue_below_30', 0 as 'total_overdue_30_to_60', 0 as 'total_overdue_60_to_90', 0 as 'total_overdue_above_90';
        ELSE
			Select for_date, total_site, total_overdue_below_30, total_overdue_30_to_60, total_overdue_60_to_90, total_overdue_above_90
			from daily_overdue_projects where for_date = input_date_x and project_type_id = project_type_id_x;
        END IF;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_overdue_turn_on` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_overdue_turn_on`(
project_type_id_x bigint,
input_date_x date
)
BEGIN

	SET @check_count = 0;
    
    IF ( project_type_id_x = 0 ) THEN    
		SET @check_count = (Select count(*) from v_consolidated_daily_overdue_turn_on where for_date = input_date_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'mwp_total_overdue', 0 as 'mwp_below_1', 0 as 'mwp_1_to_5', 0 as 'mwp_5_to_10', 0 as 'mwp_above_10';
        ELSE
			Select * from v_consolidated_daily_overdue_turn_on where for_date = input_date_x;
        END IF;
	ELSE
		SET @check_count = (Select count(*) from daily_overdue_turn_on where for_date = input_date_x and project_type_id = project_type_id_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'mwp_total_overdue', 0 as 'mwp_below_1', 0 as 'mwp_1_to_5', 0 as 'mwp_5_to_10', 0 as 'mwp_above_10';
        ELSE
			Select for_date, mwp_total_overdue, mwp_below_1, mwp_1_to_5, mwp_5_to_10, mwp_above_10
			from daily_overdue_turn_on where for_date = input_date_x and project_type_id = project_type_id_x;
        END IF;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_project_milestone_turn_on` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_project_milestone_turn_on`(
project_type_id_x varchar(100),
input_date_x date
)
BEGIN
	## EMPTY CURSOR template
	DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_milestone_code VARCHAR(100);
    DECLARE v_milestone_total BIGINT;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
	SELECT milestone_code, (milestone_task_completed + milestone_document_completed) as 'milestone_total' 
    FROM daily_project_milestones where project_type_id = project_type_id_x and for_date = input_date_x and milestone_code not like 'turn%';

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    SET @pending_milestone_total = (SELECT ((sum(milestone_task_total) + sum(milestone_document_total)) - (sum(milestone_task_completed) + sum(milestone_document_completed))) 
	FROM daily_project_milestones where project_type_id = project_type_id_x and for_date = input_date_x);

	SET @turn_on_milestone_total = (SELECT (milestone_task_completed + milestone_document_completed) 
    FROM daily_project_milestones where project_type_id = project_type_id_x and for_date = input_date_x and milestone_code like 'turn%');
    
    DROP TEMPORARY TABLE IF EXISTS project_summary_milestone;
    CREATE TEMPORARY TABLE `project_summary_milestone` (`milestone_code` VARCHAR(100) , `milestone_total` INT );
    
    INSERT INTO project_summary_milestone (milestone_code, milestone_total) VALUES ('Pending', @pending_milestone_total);
    INSERT INTO project_summary_milestone (milestone_code, milestone_total) VALUES ('Turn-On', @turn_on_milestone_total);
    
	OPEN x_cursor;
	get_x: LOOP
	FETCH x_cursor INTO v_milestone_code, v_milestone_total;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		INSERT INTO project_summary_milestone (milestone_code, milestone_total) VALUES (v_milestone_code, v_milestone_total);
    
	END LOOP get_x;
 
	CLOSE x_cursor;
    
    Select * from project_summary_milestone;
	DROP TEMPORARY TABLE IF EXISTS project_summary_milestone;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_project_turn_on` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_project_turn_on`(
project_type_id_x bigint,
input_date_x date
)
BEGIN

	SET @check_count = 0;
    
    IF ( project_type_id_x = 0 ) THEN    
		SET @check_count = (Select count(*) from v_consolidated_daily_project_turn_on where for_date = input_date_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'project_total', 0 as 'pending_total', 0 as 'completed_total', 0 as 'overdue_total';
        ELSE
			Select * from v_consolidated_daily_project_turn_on where for_date = input_date_x;
        END IF;
	ELSE
		SET @check_count = (Select count(*) from daily_overdue_turn_on where for_date = input_date_x and project_type_id = project_type_id_x);
        IF ( @check_count = 0 ) THEN
			Select input_date_x as 'for_date', 0 as 'project_total', 0 as 'pending_total', 0 as 'completed_total', 0 as 'overdue_total';
        ELSE
			Select for_date, project_total, pending_total, completed_total, overdue_total
			from daily_project_turn_on where for_date = input_date_x and project_type_id = project_type_id_x;
        END IF;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_top_overdue_document` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_top_overdue_document`(
project_type_id_x bigint,
input_date_x date
)
BEGIN

	IF ( project_type_id_x = 0 ) THEN
		Select for_date, site_id, total_overdue_document from daily_top_overdue_document
		where for_date = input_date_x order by total_overdue_document DESC limit 5;
	ELSE
		Select for_date, site_id, total_overdue_document from daily_top_overdue_document
		where project_type_id = project_type_id_x and for_date = input_date_x order by total_overdue_document DESC limit 5;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_collect_top_overdue_project` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_top_overdue_project`(
project_type_id_x bigint,
input_date_x date
)
BEGIN

	IF ( project_type_id_x = 0 ) THEN
		Select for_date, site_id, overdue_mwp from daily_top_overdue_project
		where for_date = input_date_x order by overdue_mwp DESC limit 5;
	ELSE
		Select for_date, site_id, overdue_mwp from daily_top_overdue_project
		where project_type_id = project_type_id_x and for_date = input_date_x order by overdue_mwp DESC limit 5;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_create_role_access_for_all_module` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_create_role_access_for_all_module`(
IN FROM_ROLE bigint,
IN TO_ROLE bigint
)
BEGIN

INSERT INTO role_accesses (module_code, role_id, access_listing, access_create, access_show, access_edit, access_delete, created_by)
Select module_code, TO_ROLE, access_listing, access_create, access_show, access_edit, access_delete, created_by from role_accesses where role_id = FROM_ROLE;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_project_document` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_delete_project_document`(
project_document_id_x bigint,
delete_user_id_x bigint
)
BEGIN

	SET @count_x = 0;
    
    SET @count_x = (Select count(project_document_id) from project_documents where project_document_id = project_document_id_x);
    
    IF ( @count_x = 1 ) THEN
 
		SET @project_id = NULL;
        SET @document_type_id = NULL;
        SET @document_category_id = NULL;
        SET @document_mandatory = NULL;
        
        Select project_id, document_type_id, document_category_id, document_mandatory into
        @project_id, @document_type_id, @document_category_id, @document_mandatory 
        from project_documents where project_document_id = project_document_id_x;
	 
		SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);
		
		IF ( @document_type_id = 1 AND (@document_category_id = 1 OR @document_category_id = 2) AND @document_mandatory = 1 ) THEN
			
			SET @project_level_total_document = @project_level_total_document - 1;
				  
			Update projects
			SET
			project_level_total_document = @project_level_total_document
			Where project_id = @project_id;
			
		END IF;
        
        INSERT INTO `project_documents_deleted`
		(`project_document_id`,`project_id`,`document_title`,`document_mandatory`,`assign_to_user`,
		`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,
		`req_approval_qa_qc`,`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,
		`req_approval_purchasing`,`req_approval_admin`,`from_project_document_template_id`,
		`from_project_document_template_detail_id`,`completed_flag`,`deleted_by`)
		Select `project_document_id`,`project_id`,`document_title`,`document_mandatory`,`assign_to_user`,
		`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,
		`req_approval_qa_qc`,`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,
		`req_approval_purchasing`,`req_approval_admin`,`from_project_document_template_id`,
		`from_project_document_template_detail_id`,`completed_flag`, delete_user_id_x
        From project_documents where project_document_id = project_document_id_x;
        
		delete from project_document_approvals where project_document_id = project_document_id_x;
        delete from project_document_uploads where project_document_id = project_document_id_x;
        delete from project_document_recurrings where project_document_id = project_document_id_x;
        delete from project_documents where project_document_id = project_document_id_x;
		
    END IF;
    
    Select "Completed";
         
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_project_task` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_delete_project_task`(
task_id_x bigint,
delete_user_id_x bigint
)
BEGIN
	
   SET @count_x = 0;
   
   SET @count_x = (Select count(task_id) from project_itasks where task_id = task_id_x);
   
   IF ( @count_x = 1 ) THEN

		SET @project_id = NULL;
        SET @project_id = (Select project_id from project_itasks where task_id = task_id_x);
	 
		SET @project_level_total_task = (Select project_level_total_task from projects where project_id = @project_id);
		  
		SET @project_level_total_task = @project_level_total_task - 1;
		 
		Update projects
		SET
		project_level_total_task = @project_level_total_task
		Where project_id = @project_id;
        
        INSERT INTO `project_itasks_deleted`
		(`task_id`, `project_id`, `task_title`, `assign_to_user`, `task_description`,
		`task_remarks`, `task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`, `created_at`, `updated_at`,
		`from_project_task_template_id`, `from_project_task_template_detail_id`, `deleted_by`)
        Select `task_id`, `project_id`, `task_title`, `assign_to_user`, `task_description`,
		`task_remarks`, `task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`, `created_at`, `updated_at`,
		`from_project_task_template_id`, `from_project_task_template_detail_id`, delete_user_id_x from project_itasks where task_id = task_id_x;
        
        DELETE FROM project_itasks where task_id = task_id_x;

	END IF;
    
	Select "Completed";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_site_document` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_delete_site_document`(
site_document_id_x bigint,
delete_user_id_x bigint
)
BEGIN

	SET @count_x = 0;
    
    SET @count_x = (Select count(site_document_id) from site_documents where site_document_id = site_document_id_x);
    
    IF ( @count_x = 1) THEN

		SET @document_type_id = NULL;
		SET @document_category_id = NULL;
		SET @document_mandatory = NULL;
		SET @milestone_id = NULL;
		SET @site_id = NULL;
		
		Select site_id, milestone_id, document_type_id, document_category_id, document_mandatory into 
		@site_id, @milestone_id, @document_type_id, @document_category_id, @document_mandatory from site_documents where site_document_id = site_document_id_x;
	   
		IF ( @document_type_id = 1 AND (@document_category_id = 1 OR @document_category_id = 2) AND @document_mandatory = 1 ) THEN
		
			Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
			
			SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);
			
			SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);  
			
			SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
			
			SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
			 
			SET @site_total_document = @site_total_document - 1;   
			SET @group_total_document = @group_total_document - 1;
			SET @project_total_document = @project_total_document - 1;
			SET @milestone_document_total = @milestone_document_total - 1;
		  
			Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;
				
			Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;
				
			Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;
				
			Update project_milestones
			SET
			milestone_document_total = @milestone_document_total
			Where project_id = @project_id and milestone_id = @milestone_id;
			
		END IF;
        
        INSERT INTO `site_documents_deleted`
		(`site_document_id`,`site_id`,`document_title`,`document_mandatory`,`contractor_id`,`assign_to_user`,
		`milestone_id`,`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,`req_approval_qa_qc`,
		`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,`req_approval_purchasing`,`req_approval_admin`,
		`from_site_document_template_id`,`from_site_document_template_detail_id`,`completed_flag`,`deleted_by`)
        Select `site_document_id`,`site_id`,`document_title`,`document_mandatory`,`contractor_id`,`assign_to_user`,
		`milestone_id`,`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,`req_approval_qa_qc`,
		`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,`req_approval_purchasing`,`req_approval_admin`,
		`from_site_document_template_id`,`from_site_document_template_detail_id`,`completed_flag`, delete_user_id_x
        from site_documents where site_document_id = site_document_id_x;
        
        delete from site_document_approvals where site_document_id = site_document_id_x;
        delete from site_document_uploads where site_document_id = site_document_id_x;
        delete from site_document_recurrings where site_document_id = site_document_id_x;
        delete from site_documents where site_document_id = site_document_id_x;
        
    END IF;

Select "Completed";

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_site_task` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_delete_site_task`(
task_id_x bigint,
delete_user_id_x bigint
)
BEGIN

   SET @count_x = 0;
   
   SET @count_x = (Select count(task_id) from itasks where task_id = task_id_x);
   
   IF ( @count_x = 1 ) THEN

	SET @site_id = NULL;
	SET @milestone_id = NULL;
	SET @task_est_start_date = NULL;
	SET @task_est_end_date = NULL;
	SET @task_start_date = NULL;
	SET @task_end_date = NULL;
   
	Select site_id, milestone_id, task_est_start_date, task_est_end_date, task_start_date, task_end_date into 
	@site_id, @milestone_id, @task_est_start_date, @task_est_end_date, @task_start_date, @task_end_date from 
	itasks where task_id = task_id_x;
   
	SET @group_id = (Select group_id from project_sites where site_id = @site_id);
	SET @project_id = (Select project_id from project_groups where group_id = @group_id);
    
	SET @site_total_task = (Select site_total_task from project_sites where site_id = @site_id);
	SET @group_total_task = (Select group_total_task from project_groups where group_id = @group_id);
	SET @project_total_task = (Select project_total_task from projects where project_id = @project_id);
     
	SET @site_total_task = @site_total_task - 1;
	SET @group_total_task = @group_total_task - 1;
	SET @project_total_task = @project_total_task - 1;
    
	Update project_sites
	SET
	site_total_task = @site_total_task
	Where site_id = @site_id;
     
	Update project_groups
	SET
	group_total_task = @group_total_task
	Where group_id = @group_id;
    
	Update projects
	SET
	project_total_task = @project_total_task
	Where project_id = @project_id;
	
	SET @milestone_task_total = (Select milestone_task_total from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
            
	SET @milestone_task_total = @milestone_task_total - 1;
            
	Update project_milestones
	SET
	milestone_task_total = @milestone_task_total
	Where project_id = @project_id and milestone_id = @milestone_id;

/*
	IF ( NEW.status_id = '4' ) THEN
		 
		SET @site_completed_task = (Select site_completed_task from project_sites where site_id = @site_id);
		SET @group_completed_task = (Select group_completed_task from project_groups where group_id = @group_id);
		SET @project_completed_task = (Select project_completed_task from projects where project_id = @project_id);
		  
		SET @site_completed_task = @site_completed_task + 1;
		SET @group_completed_task = @group_completed_task + 1;
		SET @project_completed_task = @project_completed_task + 1;
		 
		Update project_sites
		SET
		site_completed_task = @site_completed_task
		Where site_id = @site_id;
		  
		Update project_groups
		SET
		group_completed_task = @group_completed_task
		Where group_id = @group_id;
		 
		Update projects
		SET
		project_completed_task = @project_completed_task
		Where project_id = @project_id;
		
		SET @milestone_task_completed = (Select milestone_task_completed from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
            
	   SET @milestone_task_completed = @milestone_task_completed + 1;
	            
	   Update project_milestones
	   SET
	   milestone_task_completed = @milestone_task_completed
	   Where project_id = @project_id and milestone_id = @milestone_id;
	   
	   SET NEW.calculated_flag = 1;
	   
	    
   END IF;
*/
   ## Update Project S-Curve EST
	SET @project_scurve_id = NULL;
   
   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task 
   FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(@task_est_end_date) AND `year` = YEAR(@task_est_end_date);
   
   IF (@project_scurve_id IS NOT NULL) THEN
   
   	SET @expected_total_task = @expected_total_task - 1;
   	
   	UPDATE project_scurves
   	SET
   	expected_total_task = @expected_total_task
   	WHERE project_scurve_id = @project_scurve_id;
   
   END IF; 
   
   ## Update Project S-Curve Actual
   SET @project_scurve_id = NULL;
   
   IF ( @task_end_date IS NOT NULL ) THEN
   
	   SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task 
	   FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(@task_end_date) AND `year` = YEAR(@task_end_date);
	   
	   IF (@project_scurve_id IS NOT NULL) THEN
	   
	   	SET @actual_total_task = @actual_total_task - 1;
	   	
	   	UPDATE project_scurves
	   	SET
	   	actual_total_task = @actual_total_task
	   	WHERE project_scurve_id = @project_scurve_id;
	   
	   END IF; 
       
	END IF;
    
        INSERT INTO `itasks_deleted`
		(`task_id`, `site_id`, `task_code`, `task_title`, `assign_to_user`, 
		`milestone_id`, `contractor_id`, `task_description`, `task_remarks`,
		`task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`,
		`created_at`, `updated_at`, `from_task_template_id`, `from_task_template_detail_id`,
		`calculated_flag`, `deleted_by`)
		Select `task_id`, `site_id`, `task_code`, `task_title`, `assign_to_user`, 
		`milestone_id`, `contractor_id`, `task_description`, `task_remarks`,
		`task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`,
		`created_at`, `updated_at`, `from_task_template_id`, `from_task_template_detail_id`,
		`calculated_flag`, delete_user_id_x from itasks where task_id = task_id_x;

		delete from itasks where task_id = task_id_x;
	
END IF;

Select "Completed";

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_empty_cursor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_empty_cursor`(

)
BEGIN
	## EMPTY CURSOR template
	DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_id bigint;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
	Select id from users;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
	
    
	END LOOP get_x;
 
	CLOSE x_cursor;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_team_member` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_get_team_member`(
project_idx bigint
)
BEGIN

	Select * from users where id in 
    (
    Select project_manager from projects where project_id = project_idx
    union all
    Select project_engineer from projects where project_id = project_idx
    union all
    Select project_safety from projects where project_id = project_idx
    union all
    select group_engineer from project_groups where project_id = project_idx
    ) order by role_id, `name` ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_user_list_from_project` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_get_user_list_from_project`(
project_idx bigint,
myself_idx bigint
)
BEGIN

	Select * from users where id = myself_idx 
    or role_id in (8, 11, 13, 14, 17, 18, 19) 
    or id in 
    (
    Select project_manager from projects where project_id = project_idx
    union all
    Select project_engineer from projects where project_id = project_idx
    union all
    Select project_safety from projects where project_id = project_idx
    ) order by role_id, `name` ASC;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_user_list_from_project_with_contractor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_get_user_list_from_project_with_contractor`(
project_idx bigint,
myself_idx bigint,
contractor_idx bigint
)
BEGIN

	Select * from users where id = myself_idx 
    -- or role_id in (8, 11, 13, 14, 17, 18, 19) 
    or contractor_id = contractor_idx
    order by role_id, `name` ASC;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_import_document_from_template` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_document_from_template`(
document_template_id_x bigint,
created_by_x bigint,
site_id_x bigint
)
BEGIN

    SET @site_code = null;
    SET @group_id = null;
    SET @project_id = null;
    SET @task_template_code = null;
    SET @milestone_template_id = null;
    
    SET @document_template_code = (Select document_template_name from document_templates where document_template_id = document_template_id_x);
    SET @milestone_template_id = (Select milestone_template_id from document_templates where document_template_id = document_template_id_x);
    SET @site_code = (Select site_name from project_sites where site_id = site_id_x);
    SET @group_id = (Select group_id from project_sites where site_id = site_id_x);
    SET @project_id = (Select project_id from project_groups where group_id = @group_id);
    
    -- Select document_template_id_x, site_id_x, @milestone_template_id, site_id_x, @group_id, @project_id;
   
    CALL sp_import_milestone_into_project(@project_id, @milestone_template_id, created_by_x);
    CALL sp_import_document_into_site(site_id_x, document_template_id_x, created_by_x, @project_id);
    
	Select @document_template_code as 'document_template_code', @site_code as 'site_code';
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_import_document_into_site` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_document_into_site`(
site_id_x bigint,
document_template_id_x bigint,
created_by_x bigint,
project_id_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_document_template_id bigint;
    DECLARE v_document_template_detail_id bigint;
    DECLARE v_document_template_title varchar(100);
	DECLARE v_document_template_mandatory tinyint;
    DECLARE v_document_category_id bigint;
    DECLARE v_document_type_id bigint;
    DECLARE v_recurring_interval_id bigint;
    DECLARE v_req_approval_project_owner tinyint;
	DECLARE v_req_approval_project_manager tinyint;
    DECLARE v_req_approval_project_engineer tinyint;
    DECLARE v_req_approval_engineer tinyint;
    DECLARE v_req_approval_qa_qc tinyint;
    DECLARE v_req_approval_safety tinyint;
	DECLARE v_req_approval_onm tinyint;
    DECLARE v_req_approval_planner tinyint;
    DECLARE v_req_approval_purchasing tinyint;
    DECLARE v_req_approval_admin tinyint;
    DECLARE v_milestone_template_detail_id bigint;
    DECLARE v_milestone_id bigint;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
	Select 
	document_template_id, document_template_detail_id, document_template_title, document_template_mandatory, document_category_id, 
	document_type_id, recurring_interval_id, req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer,
	req_approval_engineer, req_approval_qa_qc, req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin,
    milestone_template_detail_id, milestone_id from 
	document_template_details
	INNER JOIN project_milestones
    ON project_milestones.from_milestone_template_detail_id = document_template_details.milestone_template_detail_id
    WHERE document_template_id = document_template_id_x and project_milestones.project_id = project_id_x and document_template_details.active_status = 1 and deleted_at is null;
	 
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    
    SET @site_start_date = (Select site_start_date from project_sites where site_id = site_id_x);
    SET @site_end_date = (Select site_end_date from project_sites where site_id = site_id_x);
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_document_template_id, v_document_template_detail_id, v_document_template_title, v_document_template_mandatory, 
    v_document_category_id, v_document_type_id, v_recurring_interval_id, v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer,
	v_req_approval_engineer, v_req_approval_qa_qc, v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin,
    v_milestone_template_detail_id, v_milestone_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
    SET @check_available = 0;
  
    SET @check_available = (Select Count(site_document_id) From site_documents where from_site_document_template_detail_id = v_document_template_detail_id and site_id = site_id_x);
    
	IF @check_available = 0 THEN
    
		IF v_document_type_id = 1 THEN
        
			INSERT INTO site_documents
			(site_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id, status_id, created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin, 
            from_site_document_template_id, from_site_document_template_detail_id, milestone_id)
			VALUES
			(site_id_x, v_document_template_title, v_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id, 1, created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc, 
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_document_template_id, v_document_template_detail_id, v_milestone_id);
        
        END IF;
        
        IF v_document_type_id = 2 THEN
        
			INSERT INTO site_documents
			(site_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id, created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin, 
            from_site_document_template_id, from_site_document_template_detail_id, 
            recurring_interval_id, recurring_start_date, recurring_end_date, milestone_id)
			VALUES
			(site_id_x, v_document_template_title, v_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id,  created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc, 
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_document_template_id, v_document_template_detail_id,
            v_recurring_interval_id, @site_start_date, @site_end_date, v_milestone_id);
            
            SET @last_site_document_id = LAST_INSERT_ID();
            
			SET x = @site_start_date;
				
			loop_date:  LOOP
				IF  x > @site_end_date THEN 
					LEAVE  loop_date;
				END  IF;
                
                INSERT INTO site_document_recurrings
                (site_document_id, document_recurring_date, current_status, created_by, document_mandatory, milestone_id)
                VALUES
                (@last_site_document_id, x, 1, created_by_x, v_document_template_mandatory, v_milestone_id );
                
                IF v_recurring_interval_id = 1 THEN
					SET x = DATE_ADD(x, INTERVAL 1 DAY);
                END IF;
                
                IF v_recurring_interval_id = 2 THEN
					SET x = DATE_ADD(x, INTERVAL 1 WEEK);
                END IF;
                
                IF v_recurring_interval_id = 3 THEN
					SET x = DATE_ADD(x, INTERVAL 1 MONTH);
                END IF;
				
			END LOOP;
        
        END IF;

    END IF;
    
    -- Select @site_start_date, @site_end_date, @check_available, v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;
    
	END LOOP get_x;
 
	CLOSE x_cursor;
    
    UPDATE site_documents
	SET 
	status_id = null
	WHERE document_category_id = 2;
    
    Select 'OK';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_import_milestone_into_project` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_milestone_into_project`(
project_id_x bigint,
milestone_template_id_x bigint,
created_by_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_milestone_template_detail_code varchar(50);
    DECLARE v_milestone_template_title varchar(100);
    DECLARE v_milestone_template_sequence int;
    DECLARE v_milestone_template_detail_id bigint;
	 
	DECLARE x_cursor CURSOR FOR 
	SELECT  milestone_template_title, milestone_template_sequence, milestone_template_detail_id  
    FROM milestone_template_details where milestone_template_id = milestone_template_id_x and active_status = 1 and deleted_at is null;
	 
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO  v_milestone_template_title, v_milestone_template_sequence, v_milestone_template_detail_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		SET @trim_milestone_code = trim( replace( replace( lower( v_milestone_template_title ), ' ', ''), '-', '') );
        
        SET @project_milestone_code = null;
        
        SET @project_milestone_code = (Select milestone_code from project_milestones where trim(replace( replace(lower(milestone_code), ' ', ''), '-', '')) = @trim_milestone_code and project_id = project_id_x);
        
        IF ( @project_milestone_code is null ) THEN
            
            INSERT INTO project_milestones 
            ( project_id, milestone_code, milestone_info, milestone_sequence, from_milestone_template_id, from_milestone_template_detail_id, created_by ) 
            VALUES
            ( project_id_x, v_milestone_template_title, v_milestone_template_title, v_milestone_template_sequence, milestone_template_id_x, v_milestone_template_detail_id, created_by_x );
		
        ELSE
        
			UPDATE project_milestones SET
            from_milestone_template_id = milestone_template_id_x,
            from_milestone_template_detail_id = v_milestone_template_detail_id
            WHERE milestone_code = @project_milestone_code and project_id = project_id_x;
			
        END IF;

	END LOOP get_x;
 
	CLOSE x_cursor;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_import_project_document_into_project` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_project_document_into_project`(
	IN `project_document_template_idx` bigint,
	IN `created_by_x` bigint,
	IN `project_id_x` bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_project_document_template_id bigint;
    DECLARE v_project_document_template_detail_id bigint;
    DECLARE v_project_document_template_title varchar(100);
	DECLARE v_project_document_template_mandatory tinyint;
    DECLARE v_document_category_id bigint;
    DECLARE v_document_type_id bigint;
    DECLARE v_recurring_interval_id bigint;
    DECLARE v_req_approval_project_owner tinyint;
	DECLARE v_req_approval_project_manager tinyint;
    DECLARE v_req_approval_project_engineer tinyint;
    DECLARE v_req_approval_engineer tinyint;
    DECLARE v_req_approval_qa_qc tinyint;
    DECLARE v_req_approval_safety tinyint;
	DECLARE v_req_approval_onm tinyint;
    DECLARE v_req_approval_planner tinyint;
    DECLARE v_req_approval_purchasing tinyint;
    DECLARE v_req_approval_admin tinyint;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
	Select 
	project_document_template_id, project_document_template_detail_id, project_document_template_title, project_document_template_mandatory, document_category_id, 
	document_type_id, recurring_interval_id, req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer,
	req_approval_engineer, req_approval_qa_qc, req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin from 
	project_document_template_details
	where project_document_template_id = project_document_template_idx and active_status = 1 and deleted_at is null;
	 
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    
    SET @project_start_date = (Select project_start_date from projects where project_id = project_id_x);
    SET @project_end_date = (Select project_end_date from projects where project_id = project_id_x);
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_project_document_template_id, v_project_document_template_detail_id, v_project_document_template_title, v_project_document_template_mandatory, 
    v_document_category_id, v_document_type_id, v_recurring_interval_id, v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer,
	v_req_approval_engineer, v_req_approval_qa_qc, v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
    SET @check_available = 0;
  
    SET @check_available = (Select Count(project_document_id) From project_documents where from_project_document_template_detail_id = v_project_document_template_detail_id and project_id = project_id_x);
    
	IF @check_available = 0 THEN
    
		IF v_document_type_id = 1 THEN
        
			INSERT INTO project_documents
			(project_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id, status_id, created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin, 
            from_project_document_template_id, from_project_document_template_detail_id)
			VALUES
			(project_id_x, v_project_document_template_title, v_project_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id, 1, created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc, 
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_project_document_template_id, v_project_document_template_detail_id);
        
        END IF;
        
        IF v_document_type_id = 2 OR v_document_type_id = 3 THEN
        
			INSERT INTO project_documents
			(project_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id,  created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin, 
            from_project_document_template_id, from_project_document_template_detail_id, 
            recurring_interval_id, recurring_start_date, recurring_end_date)
			VALUES
			(project_id_x, v_project_document_template_title, v_project_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id, created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc, 
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_project_document_template_id, v_project_document_template_detail_id,
            v_recurring_interval_id, @project_start_date, @project_end_date);
            
            SET @last_project_document_id = LAST_INSERT_ID();
            
			SET x = @project_start_date;
				
			loop_date:  LOOP
				IF  x > @project_end_date THEN 
					LEAVE  loop_date;
				END  IF;
                
                INSERT INTO project_document_recurrings
                (project_document_id, document_recurring_date, current_status, created_by, document_mandatory )
                VALUES
                (@last_project_document_id, x, 1, created_by_x, v_project_document_template_mandatory);
                
                IF v_recurring_interval_id = 1 THEN
					SET x = DATE_ADD(x, INTERVAL 1 DAY);
                END IF;
                
                IF v_recurring_interval_id = 2 THEN
					SET x = DATE_ADD(x, INTERVAL 1 WEEK);
                END IF;
                
                IF v_recurring_interval_id = 3 THEN
					SET x = DATE_ADD(x, INTERVAL 1 MONTH);
                END IF;
				
			END LOOP;
        
        END IF;

    END IF;
    
    -- Select @project_start_date, @project_end_date, @check_available, v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;
    
	END LOOP get_x;
 
	CLOSE x_cursor;
	
	UPDATE project_documents
	SET 
	status_id = null
	WHERE document_category_id = 2;
    
    Select 'OK';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_import_project_task_into_project` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_project_task_into_project`(
project_task_template_idx bigint,
created_by_x bigint,
project_id_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_project_task_template_detail_id bigint;
    DECLARE v_project_task_template_id bigint;
    DECLARE v_project_task_title varchar(100);
	DECLARE v_project_task_description varchar(300);
    DECLARE v_status_id bigint;
    
	DECLARE x_cursor CURSOR FOR 
	Select project_task_template_detail_id, project_task_template_id, project_task_title, project_task_description, status_id  from 
	project_task_template_details 
	where project_task_template_id = project_task_template_idx and active_status = 1 and deleted_at is null;
	 
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    
    SET @project_start_date = (Select project_start_date from projects where project_id = project_id_x);
    SET @project_end_date = (Select project_end_date from projects where project_id = project_id_x);
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
    SET @check_available = 0;
  
    SET @check_available = (Select Count(task_id) From project_itasks where from_project_task_template_detail_id = v_project_task_template_detail_id and project_id = project_id_x);
    
	IF @check_available = 0 THEN
    
		INSERT INTO project_itasks 
        (task_title, task_description, status_id, task_est_start_date, task_est_end_date, created_by, from_project_task_template_id, from_project_task_template_detail_id, project_id, assign_to_user) 
        VALUES 
        (v_project_task_title, v_project_task_description, v_status_id, @project_start_date, @project_end_date, created_by_x, v_project_task_template_id, v_project_task_template_detail_id, project_id_x, created_by_x);
    
    END IF;
    
    -- Select @project_start_date, @project_end_date, @check_available, v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;
    
	END LOOP get_x;
 
	CLOSE x_cursor;
    
    Select 'OK';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_import_task_from_template` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_task_from_template`(
task_template_id_x bigint,
created_by_x bigint,
site_id_x bigint
)
BEGIN

    SET @site_code = null;
    SET @group_id = null;
    SET @project_id = null;
    SET @task_template_code = null;
    SET @milestone_template_id = null;
    
    SET @task_template_code = (Select task_template_name from task_templates where task_template_id = task_template_id_x);
    SET @milestone_template_id = (Select milestone_template_id from task_templates where task_template_id = task_template_id_x);
    SET @site_code = (Select site_name from project_sites where site_id = site_id_x);
    SET @group_id = (Select group_id from project_sites where site_id = site_id_x);
    SET @project_id = (Select project_id from project_groups where group_id = @group_id);
    
    -- Select task_template_id_x, site_id_x, @milestone_template_id, site_id_x, @group_id, @project_id;
   
    
    CALL sp_import_milestone_into_project(@project_id, @milestone_template_id, created_by_x);
    CALL sp_import_task_into_site(site_id_x, task_template_id_x, created_by_x, @project_id);
    
     Select @task_template_code as 'task_template_code', @site_code as 'site_code';
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_import_task_into_site` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_task_into_site`(
site_id_x bigint,
task_template_id_x bigint,
created_by_x bigint,
project_id_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_task_template_detail_id bigint;
    DECLARE v_task_template_code varchar(50);
    DECLARE v_task_template_title varchar(100);
	DECLARE v_task_description varchar(300);
    DECLARE v_status_id bigint;
    DECLARE v_milestone_template_detail_id bigint;
    DECLARE v_milestone_id bigint;
    DECLARE v_milestone_code varchar(50);
    
	DECLARE x_cursor CURSOR FOR 
	SELECT task_template_detail_id,  task_template_title, task_description, status_id, milestone_template_detail_id, milestone_id, milestone_code
    FROM task_template_details 
    INNER JOIN project_milestones
    ON project_milestones.from_milestone_template_detail_id = task_template_details.milestone_template_detail_id
    WHERE task_template_id = task_template_id_x and project_milestones.project_id = project_id_x and task_template_details.active_status = 1 and deleted_at is null;
	 
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    
    SET @site_start_date = (Select site_start_date from project_sites where site_id = site_id_x);
    SET @site_end_date = (Select site_end_date from project_sites where site_id = site_id_x);
    
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_task_template_detail_id,  v_task_template_title, v_task_description, v_status_id, v_milestone_template_detail_id, v_milestone_id, v_milestone_code;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
    SET @check_available = 0;
    SET @contractor_id = null;
    
    SET @check_available = (Select Count(task_id) From itasks where from_task_template_detail_id = v_task_template_detail_id and site_id = site_id_x);
    
    IF ( @check_available = 0 ) THEN
    
		SET @contractor_id = (Select contractor_id from project_site_contractors where site_id = site_id_x order by created_at DESC limit 1);
         
		select site_id_x, v_task_template_title, v_milestone_id, v_task_description, v_status_id, created_by_x, task_template_id_x, v_task_template_detail_id, @contractor_id, created_by_x,
        @site_start_date, @site_end_date;
    
		INSERT INTO itasks
        (site_id, task_title, milestone_id, task_description, status_id, created_by, from_task_template_id, from_task_template_detail_id, contractor_id, assign_to_user,
        task_est_start_date, task_est_end_date)
        VALUES
        (site_id_x, v_task_template_title, v_milestone_id, v_task_description, v_status_id, created_by_x, task_template_id_x, v_task_template_detail_id, @contractor_id, created_by_x,
        @site_start_date, @site_end_date);
        
    END IF;
    
    -- Select v_task_template_detail_id, v_task_template_code, v_task_template_title, v_task_description, v_status_id, v_milestone_template_detail_id, v_milestone_id, v_milestone_code;
    
	END LOOP get_x;
 
	CLOSE x_cursor;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_daily_document_overdue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_daily_document_overdue`(
inputDate_x date,
site_end_date_x date,
project_type_id_x bigint
)
BEGIN

	#Sub Function for sp_make_overdue_document_total_one_time
	SET @over_days = (SELECT DATEDIFF(now(), site_end_date_x));
    
    SET @CheckExist_count = 0;
    SET @daily_document_overdue_id = null;
    SET @CheckExist_count = (Select Count(*) from daily_document_overdue where for_date = inputDate_x and project_type_id = project_type_id_x);
    
    IF ( @CheckExist_count = 0 ) THEN
        INSERT INTO daily_document_overdue
        (for_date, project_type_id)
        VALUES
        (inputDate_x, project_type_id_x);
    
        SET @daily_document_overdue_id = (SELECT LAST_INSERT_ID());
    END IF;
    
    IF ( @daily_document_overdue_id is null ) THEN
        SET @daily_document_overdue_id = (Select daily_document_overdue_id
        from daily_document_overdue where for_date = inputDate_x and project_type_id = project_type_id_x);
    END IF;
        
    Select total_overdue_below_30, total_overdue_30_to_60, total_overdue_60_to_90, total_overdue_above_90 
    into @total_overdue_below_30, @total_overdue_30_to_60, @total_overdue_60_to_90, @total_overdue_above_90
    from daily_document_overdue where daily_document_overdue_id = @daily_document_overdue_id;
    
    IF ( @over_days > 0 AND @over_days < 30 ) THEN
        SET @total_overdue_below_30 = @total_overdue_below_30 + 1;
    ELSEIF ( @over_days >= 30 AND @over_days <= 60 ) THEN
        SET @total_overdue_30_to_60 = @total_overdue_30_to_60 + 1;
    ELSEIF ( @over_days > 60 AND @over_days <= 90 ) THEN
        SET @total_overdue_60_to_90 = @total_overdue_60_to_90 + 1;
    ELSEIF ( @over_days > 90 ) THEN
        SET @total_overdue_above_90 = @total_overdue_above_90 + 1;
    END IF;
    
    UPDATE daily_document_overdue
    SET 
    total_overdue_below_30 = @total_overdue_below_30, 
    total_overdue_30_to_60 = @total_overdue_30_to_60, 
    total_overdue_60_to_90 = @total_overdue_60_to_90, 
    total_overdue_above_90 = @total_overdue_above_90
    WHERE daily_document_overdue_id = @daily_document_overdue_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_daily_document_summary` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_daily_document_summary`(
inputDate_x date
)
BEGIN

	## Function for DONUT NO 3 & 6
	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_project_type_id BIGINT;
    DECLARE v_milestone_document_total INT;
    DECLARE v_milestone_document_completed INT;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
    Select p.project_type_id, sum(milestone_document_total), sum(milestone_document_completed)
	from project_milestones as pm
	Inner Join projects as p
	On p.project_id = pm.project_id
	group by p.project_type_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_project_type_id, v_milestone_document_total, v_milestone_document_completed;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		SET @CheckExist_count = 0;
        SET @total_overdue = 0;
        CALL sp_make_overdue_document_total_one_time(inputDate_x, v_project_type_id,@total_overdue);
		
		SET @CheckExist_count = (Select Count(*) from daily_document_summary where for_date = inputDate_x and project_type_id = v_project_type_id);
		
		IF ( @CheckExist_count = 0 ) THEN
		
			INSERT INTO daily_document_summary
			(for_date, project_type_id, document_total, document_completed, document_pending, document_overdue)
			VALUES
			(inputDate_x, v_project_type_id, v_milestone_document_total, v_milestone_document_completed, (v_milestone_document_total - (v_milestone_document_completed + @total_overdue)), @total_overdue);
			
		ELSE
        
			SET @total_pending = 0;
            SET @total_pending = (v_milestone_document_total - (v_milestone_document_completed + @total_overdue));
   
			UPDATE daily_document_summary
			SET 
			document_total = v_milestone_document_total, 
			document_completed = v_milestone_document_completed,
			document_pending = @total_pending,
            document_overdue = @total_overdue
			Where for_date = inputDate_x and project_type_id = v_project_type_id;
	  
		END IF;
    
	END LOOP get_x;
 
	CLOSE x_cursor;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_daily_project_milestone_report` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_daily_project_milestone_report`(
inputDate_x date
)
BEGIN

	## Function 1st donut charts
    
	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_project_type_id BIGINT;
    DECLARE v_milestone_code VARCHAR(50);
    DECLARE v_milestone_task_total INT;
    DECLARE v_milestone_task_completed INT;
    DECLARE v_milestone_document_total INT;
    DECLARE v_milestone_document_completed INT;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
    Select p.project_type_id, pm.milestone_code, sum(milestone_task_total), sum(milestone_task_completed), sum(milestone_document_total), sum(milestone_document_completed)
	from project_milestones as pm
	Inner Join projects as p
	On p.project_id = pm.project_id
	group by p.project_type_id, pm.milestone_code;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_project_type_id, v_milestone_code, v_milestone_task_total, v_milestone_task_completed, v_milestone_document_total, v_milestone_document_completed;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
    SET @CheckExist_count = 0;
    
    SET @CheckExist_count = (Select Count(*) from daily_project_milestones where for_date = inputDate_x and project_type_id = v_project_type_id 
    and milestone_code = v_milestone_code);
    
    IF ( @CheckExist_count = 0 ) THEN
    
		INSERT INTO daily_project_milestones
		(for_date, project_type_id, milestone_code, milestone_task_total, milestone_task_completed, milestone_document_total, milestone_document_completed)
		VALUES
		(inputDate_x, v_project_type_id, v_milestone_code, v_milestone_task_total, v_milestone_task_completed, v_milestone_document_total, v_milestone_document_completed);
        
	ELSE
    
		UPDATE daily_project_milestones
        SET 
        milestone_task_total = v_milestone_task_total, 
        milestone_task_completed = v_milestone_task_completed, 
        milestone_document_total = v_milestone_document_total, 
        milestone_document_completed = v_milestone_document_completed
        Where for_date = inputDate_x and project_type_id = v_project_type_id and milestone_code = v_milestone_code;
  
	END IF;
    
	END LOOP get_x;
 
	CLOSE x_cursor;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_overdue_document_total` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_overdue_document_total`(
project_type_id_x BIGINT,
OUT total_overdue int
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_site_document_id BIGINT;
    DECLARE v_document_mandatory TINYINT;
    DECLARE v_document_category_id BIGINT;
    DECLARE v_document_type_id BIGINT;
	DECLARE v_status_id BIGINT;
    DECLARE v_site_id BIGINT;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
    Select site_document_id, document_mandatory, document_category_id, document_type_id, status_id, site_id from site_documents where site_id in (
    Select site_id from projects as p
	Inner join project_sites as s on p.project_id = s.project_id where p.project_type_id = project_type_id_x);

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
    SET total_overdue = 0;
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_site_document_id, v_document_mandatory, v_document_category_id, v_document_type_id, v_status_id, v_site_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
        SET @site_end_date = (Select site_end_date from project_sites where site_id = v_site_id);
    
		IF ( v_document_category_id = 2 ) THEN
        
            SET @upload_count = (Select Count(*) from site_document_uploads where site_document_id = v_site_document_id);
            
            IF (( NOW() > @site_end_date ) AND @upload_count = 0) THEN
            
				 SET total_overdue = total_overdue + 1;
				
            END IF;
        
        END IF;
        
		IF ( v_document_category_id = 1 AND v_document_type_id = 1  ) THEN
        
            SET @upload_count = (Select Count(*) from site_document_uploads where site_document_id = v_site_document_id);
            
            IF (( NOW() > @site_end_date ) AND @upload_count >= 0 AND v_status_id < 3) THEN
            
				 SET total_overdue = total_overdue + 1;
                
            END IF;
            
            IF (( NOW() > @site_end_date ) AND v_status_id = 3) THEN
            
				 SET @approval_zero_check_count_null = 0;
                 SET @approval_zero_check_count_not_approved = 0;
                 SET @last_site_document_upload_id = null;
                 SET @last_site_document_upload_id = (Select site_document_upload_id from site_document_uploads where site_document_id = v_site_document_id order by created_at DESC limit 1);
                 
                 IF ( @last_site_document_upload_id is null ) THEN
                 
					SET total_overdue = total_overdue + 1;
                 
                 ELSE
                 
					 SET @approval_zero_check_count_null = (Select count(*) from site_document_approvals where site_document_upload_id = @last_site_document_upload_id and code_by_approval is null);
					 SET @approval_zero_check_count_not_approved = (Select count(*) from site_document_approvals where site_document_upload_id = @last_site_document_upload_id and code_by_approval in (2,3,4));
					 
					 IF (@approval_zero_check_count_null > 0 and @approval_zero_check_count_not_approved > 0) THEN
					 
						SET total_overdue = total_overdue + 1;
					 
					 END IF;
                     
                 END IF;
                
            END IF;

		END IF;
        
	END LOOP get_x;
 
	CLOSE x_cursor;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_overdue_document_total_not_use` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_overdue_document_total_not_use`(
project_type_id_x BIGINT,
OUT total_overdue int
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_site_document_id BIGINT;
    DECLARE v_document_mandatory TINYINT;
    DECLARE v_document_category_id BIGINT;
    DECLARE v_document_type_id BIGINT;
	DECLARE v_status_id BIGINT;
    DECLARE v_site_id BIGINT;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
    Select site_document_id, document_mandatory, document_category_id, document_type_id, status_id, site_id from site_documents where site_id in (
    Select site_id from projects as p
	Inner join project_sites as s on p.project_id = s.project_id where p.project_type_id = project_type_id_x);

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
    SET total_overdue = 0;
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_site_document_id, v_document_mandatory, v_document_category_id, v_document_type_id, v_status_id, v_site_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
        SET @site_end_date = (Select site_end_date from project_sites where site_id = v_site_id);
    
		IF ( v_document_category_id = 2 ) THEN
        
            SET @upload_count = (Select Count(*) from site_document_uploads where site_document_id = v_site_document_id);
            
            IF (( NOW() > @site_end_date ) AND @upload_count = 0) THEN
            
				 SET total_overdue = total_overdue + 1;
				
            END IF;
        
        END IF;
        
		IF ( v_document_category_id = 1 AND v_document_type_id = 1  ) THEN
        
            SET @upload_count = (Select Count(*) from site_document_uploads where site_document_id = v_site_document_id);
            
            IF (( NOW() > @site_end_date ) AND @upload_count >= 0 AND v_status_id < 3) THEN
            
				 SET total_overdue = total_overdue + 1;
                
            END IF;
            
            IF (( NOW() > @site_end_date ) AND v_status_id = 3) THEN
            
				 SET @approval_zero_check_count_null = 0;
                 SET @approval_zero_check_count_not_approved = 0;
                 SET @last_site_document_upload_id = null;
                 SET @last_site_document_upload_id = (Select site_document_upload_id from site_document_uploads where site_document_id = v_site_document_id order by created_at DESC limit 1);
                 
                 IF ( @last_site_document_upload_id is null ) THEN
                 
					SET total_overdue = total_overdue + 1;
                 
                 ELSE
                 
					 SET @approval_zero_check_count_null = (Select count(*) from site_document_approvals where site_document_upload_id = @last_site_document_upload_id and code_by_approval is null);
					 SET @approval_zero_check_count_not_approved = (Select count(*) from site_document_approvals where site_document_upload_id = @last_site_document_upload_id and code_by_approval in (2,3,4));
					 
					 IF (@approval_zero_check_count_null > 0 and @approval_zero_check_count_not_approved > 0) THEN
					 
						SET total_overdue = total_overdue + 1;
					 
					 END IF;
                     
                 END IF;
                
            END IF;

		END IF;
        
	END LOOP get_x;
 
	CLOSE x_cursor;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_overdue_document_total_one_time` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_overdue_document_total_one_time`(
inputDate_x date,
project_type_id_x BIGINT,
OUT total_overdue int
)
BEGIN

	## Sub Function for sp_make_daily_document_summary
	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_site_document_id BIGINT;
    DECLARE v_document_mandatory TINYINT;
    DECLARE v_document_category_id BIGINT;
    DECLARE v_document_type_id BIGINT;
	DECLARE v_status_id BIGINT;
    DECLARE v_site_id BIGINT;
    
    DECLARE x INT;
    
	DECLARE x_cursor CURSOR FOR 
    Select site_document_id, document_mandatory, document_category_id, document_type_id, status_id, site_id from site_documents where site_id in (
    Select site_id from projects as p Inner join project_sites as s on p.project_id = s.project_id where p.project_type_id = project_type_id_x);

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
    SET total_overdue = 0;
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_site_document_id, v_document_mandatory, v_document_category_id, v_document_type_id, v_status_id, v_site_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		SET @overdue_flag = 0;
        SET @over_days = 0;
    
        SET @site_end_date = (Select site_end_date from project_sites where site_id = v_site_id);
        
        IF ( v_document_category_id = 1 AND v_document_type_id = 2 and v_document_mandatory = 1) THEN
			IF ( inputDate_x > @site_end_date ) THEN
				SET @recurring_overdue_count = 0;
                SET @recurring_overdue_count = (Select count(*) from site_document_recurrings where completed_flag = 0 and site_document_id = v_site_document_id);
                SET total_overdue = total_overdue + @recurring_overdue_count;
				SET x = 1;
				loop_date:  LOOP
				IF  x > @recurring_overdue_count THEN 
					LEAVE  loop_date;
				END  IF;
					CALL sp_make_daily_document_overdue(inputDate_x, @site_end_date, project_type_id_x);
                    SET x = x + 1;
				END LOOP;
            END IF;
        END IF;
    
		IF ( v_document_type_id = 1 AND v_document_mandatory = 1 ) THEN
			IF ( v_document_category_id = 2 ) THEN
				SET @upload_count = (Select Count(*) from site_document_uploads where site_document_id = v_site_document_id);
				
				IF (( inputDate_x > @site_end_date ) AND @upload_count = 0) THEN
					 SET total_overdue = total_overdue + 1;
					 SET @overdue_flag = 1;
				END IF;
			END IF;
			
			IF ( v_document_category_id = 1 AND v_document_type_id = 1  ) THEN
				SET @upload_count = (Select Count(*) from site_document_uploads where site_document_id = v_site_document_id);
				
				IF (( inputDate_x > @site_end_date ) AND @upload_count >= 0 AND v_status_id < 3) THEN
					 SET total_overdue = total_overdue + 1;
					 SET @overdue_flag = 1;
				END IF;
				
				IF (( inputDate_x > @site_end_date ) AND v_status_id = 3) THEN
					 SET @approval_zero_check_count_null = 0;
					 SET @approval_zero_check_count_not_approved = 0;
					 SET @last_site_document_upload_id = null;
					 SET @last_site_document_upload_id = (Select site_document_upload_id from site_document_uploads where site_document_id = v_site_document_id order by created_at DESC limit 1);
					 
					 IF ( @last_site_document_upload_id is null ) THEN
						SET total_overdue = total_overdue + 1;
						SET @overdue_flag = 1;
					 ELSE
						 SET @approval_zero_check_count_null = (Select count(*) from site_document_approvals where site_document_upload_id = @last_site_document_upload_id and code_by_approval is null);
						 SET @approval_zero_check_count_not_approved = (Select count(*) from site_document_approvals where site_document_upload_id = @last_site_document_upload_id and code_by_approval in (2,3,4));
						 
						 IF (@approval_zero_check_count_null > 0 and @approval_zero_check_count_not_approved > 0) THEN
							SET total_overdue = total_overdue + 1;
							SET @overdue_flag = 1;
						 END IF;
					 END IF;
				END IF;
			END IF;
			
			IF (  @overdue_flag = 1 ) THEN 
				CALL sp_make_daily_document_overdue(inputDate_x, @site_end_date, project_type_id_x);
			END IF;
        END IF;
        
    END LOOP get_x;
 
	CLOSE x_cursor;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_project_overdue_report` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_project_overdue_report`(
input_date_x date
)
BEGIN
	## Function for 4th & Maybe 5th Donut by project_type
	DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_project_type_id BIGINT;
    
	DECLARE x_cursor CURSOR FOR 
	select project_type_id from project_types order by project_type_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
	OPEN x_cursor;
	get_x: LOOP
	 
	FETCH x_cursor INTO v_project_type_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		CALL sp_make_project_overdue_report_by_project_type(input_date_x,v_project_type_id);
    
	END LOOP get_x;

	CLOSE x_cursor;

    delete from daily_overdue_projects where 
    total_site = 0 and total_overdue_below_30 = 0 and total_overdue_30_to_60 = 0 
    and total_overdue_60_to_90 = 0
    and total_overdue_above_90 = 0 and for_date = input_date_x;
    
	delete from daily_overdue_turn_on where mwp_total_overdue = 0 and mwp_below_1 = 0 and mwp_1_to_5 = 0 and mwp_5_to_10 = 0
    and mwp_above_10 = 0 and for_date = input_date_x;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_project_overdue_report_by_project_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_project_overdue_report_by_project_type`(
inputDate_x date,
project_type_id_x BIGINT
)
BEGIN

	## sub function for sp_make_project_overdue_report
	DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_site_end_date DATE;
    DECLARE v_site_id BIGINT;
    DECLARE v_site_overdue_mwp DECIMAL(12,6);
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
	select ps.site_end_date , ps.site_id, (ps.site_total_capacity / 1000) as site_overdue_mwp  from projects as p
	inner join project_sites as ps
	on ps.project_id = p.project_id 
	where project_type_id = project_type_id_x
	and ps.status_id not in (5,6,7) and site_end_date < inputDate_x;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
    SET @check_daily_overdue_projects = 0;
    SET @check_daily_overdue_turn_on = 0;
    SET @total_site = 0;
	SET @over_days = 0;
	SET @total_overdue_below_30 = 0;
	SET @total_overdue_30_to_60 = 0;
	SET @total_overdue_60_to_90 = 0;
	SET @total_overdue_above_90 = 0;
	SET @mwp_below_1 = 0, @mwp_1_to_5 = 0, @mwp_5_to_10 = 0, @mwp_above_10 = 0, @mwp_total_overdue = 0;
    
    SET @check_daily_overdue_projects = (Select count(*) from daily_overdue_projects where for_date = inputDate_x and project_type_id = project_type_id_x);
    SET @check_daily_overdue_turn_on = (Select count(*) from daily_overdue_turn_on where for_date = inputDate_x and project_type_id = project_type_id_x);
    
    IF ( @check_daily_overdue_projects = 0 ) THEN
		INSERT INTO `daily_overdue_projects` (`for_date`, `project_type_id`) VALUES (inputDate_x, project_type_id_x );
    END IF;
    
    IF ( @check_daily_overdue_turn_on = 0 ) THEN
		INSERT INTO `daily_overdue_turn_on` (`for_date`, `project_type_id`) VALUES (inputDate_x, project_type_id_x );
    END IF;
    
	OPEN x_cursor;
	 
	get_x: LOOP
	 
	FETCH x_cursor INTO v_site_end_date, v_site_id, v_site_overdue_mwp;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		-- Select inputDate_x, project_type_id_x, v_site_end_date, v_site_id, v_site_overdue_mwp;
        SET @over_days = 0;
        SET @over_days = (SELECT DATEDIFF(now(), v_site_end_date));
    
        IF ( @over_days > 0 AND @over_days < 30 ) THEN
			SET @total_overdue_below_30 = @total_overdue_below_30 + 1;
            SET @total_site = @total_site + 1;
		ELSEIF ( @over_days >= 30 AND @over_days <= 60 ) THEN
			SET @total_overdue_30_to_60 = @total_overdue_30_to_60 + 1;
            SET @total_site = @total_site + 1;
		ELSEIF ( @over_days > 60 AND @over_days <= 90 ) THEN
			SET @total_overdue_60_to_90 = @total_overdue_60_to_90 + 1;
            SET @total_site = @total_site + 1;
		ELSEIF ( @over_days > 90 ) THEN
			SET @total_overdue_above_90 = @total_overdue_above_90 + 1;
            SET @total_site = @total_site + 1;
		END IF;
        
        IF ( @over_days > 0 ) THEN
        
			IF ( v_site_overdue_mwp < 1 ) THEN
				SET @mwp_below_1 = @mwp_below_1 + 1;
                SET @mwp_total_overdue = @mwp_total_overdue + v_site_overdue_mwp;
			END IF;
            
            IF ( v_site_overdue_mwp >= 1 AND v_site_overdue_mwp <= 5 ) THEN 
				SET @mwp_1_to_5 = @mwp_1_to_5 + 1;
                SET @mwp_total_overdue = @mwp_total_overdue + v_site_overdue_mwp;
			END IF;
            
            IF ( v_site_overdue_mwp > 5 AND v_site_overdue_mwp <= 10 ) THEN
				SET @mwp_5_to_10 = @mwp_5_to_10 + 1;
                SET @mwp_total_overdue = @mwp_total_overdue + v_site_overdue_mwp;
            END IF;
            
            IF ( v_site_overdue_mwp > 10 ) THEN
				SET @mwp_above_10 = @mwp_above_10 + 1;
                SET @mwp_total_overdue = @mwp_total_overdue + v_site_overdue_mwp;
			END IF;
        
        END IF;
    
	END LOOP get_x;
 
	CLOSE x_cursor;
    
    UPDATE daily_overdue_projects
    SET 
    `for_date` = inputDate_x,
    `project_type_id` = project_type_id_x, 
    `total_site` = @total_site, 
    `total_overdue_below_30` = @total_overdue_below_30,
    `total_overdue_30_to_60` = @total_overdue_30_to_60,
    `total_overdue_60_to_90` = @total_overdue_60_to_90, 
    `total_overdue_above_90` = @total_overdue_above_90
    WHERE for_date = inputDate_x and project_type_id = project_type_id_x;
    
	UPDATE daily_overdue_turn_on
    SET
    `for_date` = inputDate_x, 
    `project_type_id` = project_type_id_x,
    `mwp_below_1` = @mwp_below_1, 
    `mwp_1_to_5` = @mwp_1_to_5, 
    `mwp_5_to_10` = @mwp_5_to_10,
    `mwp_above_10` = @mwp_above_10,
    `mwp_total_overdue` = @mwp_total_overdue
    WHERE for_date = inputDate_x and project_type_id = project_type_id_x;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_project_turn_on` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_project_turn_on`(
input_date_x date
)
BEGIN
	## Function for 2nd donut
	DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_project_type_id BIGINT;
    
	DECLARE x_cursor CURSOR FOR 
	select project_type_id from project_types order by project_type_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
	OPEN x_cursor;
	get_x: LOOP
	 
	FETCH x_cursor INTO v_project_type_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		CALL sp_make_project_turn_on_by_project_type(input_date_x,v_project_type_id);
    
	END LOOP get_x;

	CLOSE x_cursor;

/*
    delete from daily_project_turn_on where 
    pending_total = 0 and completed_total = 0 and overdue_total = 0 
	and for_date = input_date_x;*/
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_project_turn_on_by_project_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_project_turn_on_by_project_type`(
inputDate_x date,
project_type_id_x BIGINT
)
BEGIN

	## sub function for sp_make_project_overdue_report
    SET @project_total = 0;
    SET @completed_total = 0;
    SET @overdue_total = 0;
    SET @pending_total = 0;
    SET @check_exists = 0;
    
	select sum(ps.site_total_capacity / 1000) into @project_total from projects as p
	inner join project_sites as ps on ps.project_id = p.project_id 	where project_type_id = project_type_id_x;
    
    select sum(ps.site_total_capacity / 1000) into @completed_total  from projects as p
	inner join project_sites as ps on ps.project_id = p.project_id where project_type_id = project_type_id_x
	and ps.status_id in (5,6,7);
    
    select sum(ps.site_total_capacity / 1000) into @overdue_total  from projects as p
	inner join project_sites as ps on ps.project_id = p.project_id where project_type_id = project_type_id_x
	and ps.status_id not in (5,6,7) and site_end_date < inputDate_x;
    
    IF ( @project_total is null ) THEN
		SET @project_total = 0;
    END IF;
    
    IF ( @completed_total is null ) THEN
		SET @completed_total = 0;
    END IF;
    
	IF ( @overdue_total is null ) THEN
		SET @overdue_total = 0;
    END IF;
    
    SET @pending_total = @project_total - (@completed_total + @overdue_total);
    
    IF ( @project_total > 0 ) THEN
    
		SET @check_exists = (Select count(*) from daily_project_turn_on where for_date = inputDate_x and project_type_id = project_type_id_x);
    
		IF ( @check_exists = 0 ) THEN
			INSERT INTO `daily_project_turn_on` (`for_date`, `project_type_id`) VALUES (inputDate_x, project_type_id_x);
        END IF;
        
        UPDATE `daily_project_turn_on`
		SET
		`for_date` = inputDate_x,
		`project_type_id` = project_type_id_x,
		`project_total` = @project_total,
		`pending_total` = @pending_total,
		`completed_total` = @completed_total,
		`overdue_total` = @overdue_total
		WHERE for_date = inputDate_x and project_type_id = project_type_id_x;

    END IF;

    
    -- Select @project_total, @pending_total, @completed_total, @overdue_total;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_top_overdue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_top_overdue`(
inputDate_x date
)
BEGIN
	## Function for TOP 5 Project
	DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_project_type_id BIGINT;
    
	DECLARE x_cursor CURSOR FOR 
	select project_type_id from project_types order by project_type_id;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
	OPEN x_cursor;
	get_x: LOOP
	 
	FETCH x_cursor INTO v_project_type_id;
	 
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		CALL sp_make_top_overdue_project_by_project_type(inputDate_x,v_project_type_id);
        CALL sp_make_top_overdue_document_by_project_type(inputDate_x,v_project_type_id);
    
	END LOOP get_x;

	CLOSE x_cursor;

	delete from daily_top_overdue_document where total_overdue_document = 0;
    delete from daily_top_overdue_project where overdue_mwp = 0;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_top_overdue_document_by_project_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_top_overdue_document_by_project_type`(
inputDate_x date,
project_type_id_x BIGINT
)
BEGIN
## sub function for top overdue
	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_site_document_id BIGINT;
    DECLARE v_document_mandatory TINYINT;
    DECLARE v_document_category_id BIGINT;
    DECLARE v_document_type_id BIGINT;
	DECLARE v_status_id BIGINT;
    DECLARE v_site_id BIGINT;
    DECLARE v_completed_flag TINYINT;
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
	Select site_document_id, document_mandatory, document_category_id, document_type_id, status_id, site_id, completed_flag from site_documents where site_id in (
    Select site_id from projects as p Inner join project_sites as s on p.project_id = s.project_id where p.project_type_id = project_type_id_x) order by site_id;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
    SET @top_5 = 1;
    
	OPEN x_cursor;
	get_x: LOOP	 
	FETCH x_cursor INTO v_site_document_id, v_document_mandatory, v_document_category_id, v_document_type_id, v_status_id, v_site_id, v_completed_flag;
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
		SET @check_count = 0;
        SET @site_end_date = (Select site_end_date from project_sites where site_id = v_site_id);
        SET @check_count = (Select count(*) from daily_top_overdue_document where for_date = inputDate_x and project_type_id = project_type_id_x and site_id = v_site_id);
        
        IF ( @check_count = 0 ) THEN
			INSERT INTO daily_top_overdue_document (for_date, project_type_id, site_id) VALUES (inputDate_x, project_type_id_x, v_site_id);
        END IF;
        
		IF ( v_document_category_id = 1 AND v_document_type_id = 2 and v_document_mandatory = 1 ) THEN
			IF ( inputDate_x > @site_end_date ) THEN
				SET @total_overdue = 0;
                SET @total_overdue = (Select count(*) from site_document_recurrings where completed_flag = 0 and site_document_id = v_site_document_id);
                SET @total_overdue_document = (Select total_overdue_document from daily_top_overdue_document where for_date = inputDate_x and project_type_id = project_type_id_x and site_id = v_site_id);
                SET @total_overdue_document = @total_overdue_document + @total_overdue;
                UPDATE daily_top_overdue_document SET total_overdue_document = @total_overdue_document where for_date = inputDate_x and project_type_id = project_type_id_x and site_id = v_site_id;
			END IF;
		END IF;
        
        IF ( (v_document_category_id = 2 OR v_document_category_id = 1) AND v_document_type_id = 1 and v_document_mandatory = 1 ) THEN
			IF (( inputDate_x > @site_end_date ) AND v_completed_flag = 0) THEN
				SET @total_overdue_document = (Select total_overdue_document from daily_top_overdue_document where for_date = inputDate_x and project_type_id = project_type_id_x and site_id = v_site_id);
                SET @total_overdue_document = @total_overdue_document + 1;
                UPDATE daily_top_overdue_document SET total_overdue_document = @total_overdue_document where for_date = inputDate_x and project_type_id = project_type_id_x and site_id = v_site_id;
            END IF;
        END IF;
    
	END LOOP get_x;
 
	CLOSE x_cursor;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_make_top_overdue_project_by_project_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_make_top_overdue_project_by_project_type`(
inputDate_x date,
project_type_id_x BIGINT
)
BEGIN

	## sub function for top overdue
	DECLARE v_finished INTEGER DEFAULT 0;
    DECLARE v_site_end_date DATE;
    DECLARE v_site_id BIGINT;
    DECLARE v_site_overdue_mwp DECIMAL(12,6);
    
    DECLARE x datetime;
    
	DECLARE x_cursor CURSOR FOR 
	select ps.site_end_date , ps.site_id, (ps.site_total_capacity / 1000) as site_overdue_mwp  from projects as p
	inner join project_sites as ps
	on ps.project_id = p.project_id 
	where project_type_id = project_type_id_x
	and ps.status_id not in (5,6,7) and site_end_date < inputDate_x order by site_overdue_mwp DESC;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;    
    
    SET @top_5 = 1;
    
	OPEN x_cursor;
	get_x: LOOP	 
	FETCH x_cursor INTO v_site_end_date, v_site_id, v_site_overdue_mwp;
	IF v_finished = 1 THEN 
	LEAVE get_x;
	END IF;
    
        SET @over_days = 0;
        SET @over_days = (SELECT DATEDIFF(now(), v_site_end_date));
        
        IF ( @over_days > 0 ) THEN
			
            IF ( @top_5 <= 5 ) THEN
				SET @check_count = 0;
				SET @check_count = (Select count(*) from daily_top_overdue_project WHERE for_date = inputDate_x and project_type_id = project_type_id_x and site_id = v_site_id);
				
				IF ( @check_count = 0 ) THEN
					INSERT INTO daily_top_overdue_project (for_date, project_type_id, site_id) VALUES (inputDate_x, project_type_id_x, v_site_id);
				END IF;
                
                UPDATE daily_top_overdue_project SET `overdue_mwp` = v_site_overdue_mwp WHERE for_date = inputDate_x and project_type_id = project_type_id_x and site_id = v_site_id;
			END IF;
            
            SET @top_5 = @top_5 + 1;

        END IF;
    
	END LOOP get_x;
 
	CLOSE x_cursor;
  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_consolidated_daily_document_overdue`
--

/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_document_overdue`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_consolidated_daily_document_overdue` AS select `daily_document_overdue`.`for_date` AS `for_date`,(((sum(`daily_document_overdue`.`total_overdue_below_30`) + sum(`daily_document_overdue`.`total_overdue_30_to_60`)) + sum(`daily_document_overdue`.`total_overdue_60_to_90`)) + sum(`daily_document_overdue`.`total_overdue_above_90`)) AS `total_overdue`,sum(`daily_document_overdue`.`total_overdue_below_30`) AS `total_overdue_below_30`,sum(`daily_document_overdue`.`total_overdue_30_to_60`) AS `total_overdue_30_to_60`,sum(`daily_document_overdue`.`total_overdue_60_to_90`) AS `total_overdue_60_to_90`,sum(`daily_document_overdue`.`total_overdue_above_90`) AS `total_overdue_above_90` from `daily_document_overdue` group by `daily_document_overdue`.`for_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_consolidated_daily_document_summary`
--

/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_document_summary`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_consolidated_daily_document_summary` AS select `daily_document_summary`.`for_date` AS `for_date`,sum(`daily_document_summary`.`document_total`) AS `document_total`,sum(`daily_document_summary`.`document_completed`) AS `document_completed`,sum(`daily_document_summary`.`document_pending`) AS `document_pending`,sum(`daily_document_summary`.`document_overdue`) AS `document_overdue` from `daily_document_summary` group by `daily_document_summary`.`for_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_consolidated_daily_overdue_projects`
--

/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_overdue_projects`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_consolidated_daily_overdue_projects` AS select `daily_overdue_projects`.`for_date` AS `for_date`,sum(`daily_overdue_projects`.`total_site`) AS `total_site`,sum(`daily_overdue_projects`.`total_overdue_below_30`) AS `total_overdue_below_30`,sum(`daily_overdue_projects`.`total_overdue_30_to_60`) AS `total_overdue_30_to_60`,sum(`daily_overdue_projects`.`total_overdue_60_to_90`) AS `total_overdue_60_to_90`,sum(`daily_overdue_projects`.`total_overdue_above_90`) AS `total_overdue_above_90` from `daily_overdue_projects` group by `daily_overdue_projects`.`for_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_consolidated_daily_overdue_turn_on`
--

/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_overdue_turn_on`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_consolidated_daily_overdue_turn_on` AS select `daily_overdue_turn_on`.`for_date` AS `for_date`,sum(`daily_overdue_turn_on`.`mwp_total_overdue`) AS `mwp_total_overdue`,sum(`daily_overdue_turn_on`.`mwp_below_1`) AS `mwp_below_1`,sum(`daily_overdue_turn_on`.`mwp_1_to_5`) AS `mwp_1_to_5`,sum(`daily_overdue_turn_on`.`mwp_5_to_10`) AS `mwp_5_to_10`,sum(`daily_overdue_turn_on`.`mwp_above_10`) AS `mwp_above_10` from `daily_overdue_turn_on` group by `daily_overdue_turn_on`.`for_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_consolidated_daily_project_turn_on`
--

/*!50001 DROP VIEW IF EXISTS `v_consolidated_daily_project_turn_on`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_consolidated_daily_project_turn_on` AS select `daily_project_turn_on`.`for_date` AS `for_date`,sum(`daily_project_turn_on`.`project_total`) AS `project_total`,sum(`daily_project_turn_on`.`pending_total`) AS `pending_total`,sum(`daily_project_turn_on`.`completed_total`) AS `completed_total`,sum(`daily_project_turn_on`.`overdue_total`) AS `overdue_total` from `daily_project_turn_on` group by `daily_project_turn_on`.`for_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_daily_project_milestones`
--

/*!50001 DROP VIEW IF EXISTS `v_daily_project_milestones`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_daily_project_milestones` AS select `daily_project_milestones`.`for_date` AS `for_date`,`daily_project_milestones`.`milestone_code` AS `milestone_code`,`daily_project_milestones`.`milestone_task_total` AS `milestone_task_total`,(`daily_project_milestones`.`milestone_task_total` - `daily_project_milestones`.`milestone_task_completed`) AS `milestone_task_pending`,`daily_project_milestones`.`milestone_task_completed` AS `milestone_task_completed`,(`daily_project_milestones`.`milestone_task_completed` - `daily_project_milestones`.`milestone_document_total`) AS `milestone_document_pending`,`daily_project_milestones`.`milestone_document_total` AS `milestone_document_total`,`daily_project_milestones`.`milestone_document_completed` AS `milestone_document_completed` from `daily_project_milestones` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_project_document_recurring_with_total_upload`
--

/*!50001 DROP VIEW IF EXISTS `v_project_document_recurring_with_total_upload`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_project_document_recurring_with_total_upload` AS select `dr`.`project_document_recurring_id` AS `project_document_recurring_id`,`dr`.`project_document_id` AS `project_document_id`,`dr`.`document_recurring_date` AS `document_recurring_date`,`dr`.`current_status` AS `current_status`,`dr`.`created_by` AS `created_by`,`dr`.`created_at` AS `created_at`,`dr`.`updated_at` AS `updated_at`,`dr`.`document_mandatory` AS `document_mandatory`,`dr`.`completed_flag` AS `completed_flag`,count(`du`.`project_document_upload_id`) AS `total_upload` from (`project_document_recurrings` `dr` left join `project_document_uploads` `du` on((`du`.`project_document_recurring_id` = `dr`.`project_document_recurring_id`))) group by `dr`.`project_document_recurring_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_project_group_site_contractors`
--

/*!50001 DROP VIEW IF EXISTS `v_project_group_site_contractors`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_project_group_site_contractors` AS select `sc`.`site_contractor_id` AS `site_contractor_id`,`sc`.`site_id` AS `site_id`,`sc`.`contractor_id` AS `contractor_id`,`sc`.`created_by` AS `created_by`,`sc`.`created_at` AS `created_at`,`sc`.`updated_at` AS `updated_at`,`sc`.`active_status` AS `active_status`,`pc`.`contractor_code` AS `contractor_code`,`ps`.`site_name` AS `site_name`,`pg`.`group_id` AS `group_id`,`pg`.`group_name` AS `group_name`,`p`.`project_id` AS `project_id`,`p`.`project_name` AS `project_name` from ((((`project_site_contractors` `sc` join `project_contractors` `pc` on((`pc`.`contractor_id` = `sc`.`contractor_id`))) join `project_sites` `ps` on((`ps`.`site_id` = `sc`.`site_id`))) join `project_groups` `pg` on((`pg`.`group_id` = `ps`.`group_id`))) join `projects` `p` on((`p`.`project_id` = `pg`.`project_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_project_group_site_document`
--

/*!50001 DROP VIEW IF EXISTS `v_project_group_site_document`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_project_group_site_document` AS select `p`.`project_name` AS `project_name`,`pg`.`group_id` AS `group_id`,`pg`.`group_name` AS `group_name`,`ps`.`site_name` AS `site_name`,`d`.`site_document_id` AS `site_document_id`,`p`.`project_id` AS `project_id`,`d`.`site_id` AS `site_id`,`d`.`document_title` AS `document_title`,`d`.`document_mandatory` AS `document_mandatory`,`d`.`document_type_id` AS `document_type_id`,`d`.`document_category_id` AS `document_category_id`,`d`.`assign_to_user` AS `assign_to_user`,`d`.`milestone_id` AS `milestone_id`,`d`.`status_id` AS `status_id`,`d`.`created_by` AS `created_by`,`d`.`created_at` AS `created_at`,`d`.`updated_at` AS `updated_at`,`d`.`from_site_document_template_id` AS `from_site_document_template_id`,`d`.`from_site_document_template_detail_id` AS `from_site_document_template_detail_id`,`d`.`uploaded_flag` AS `uploaded_flag` from (((`site_documents` `d` join `project_sites` `ps` on((`ps`.`site_id` = `d`.`site_id`))) join `project_groups` `pg` on((`pg`.`group_id` = `ps`.`group_id`))) join `projects` `p` on((`p`.`project_id` = `pg`.`project_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_project_group_site_task`
--

/*!50001 DROP VIEW IF EXISTS `v_project_group_site_task`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_project_group_site_task` AS select `p`.`project_id` AS `project_id`,`p`.`project_name` AS `project_name`,`pg`.`group_id` AS `group_id`,`pg`.`group_name` AS `group_name`,`ps`.`site_id` AS `site_id`,`ps`.`site_name` AS `site_name`,`t`.`task_id` AS `task_id`,`t`.`task_code` AS `task_code`,`t`.`task_title` AS `task_title`,`t`.`assign_to_user` AS `assign_to_user`,`t`.`milestone_id` AS `milestone_id`,`t`.`contractor_id` AS `contractor_id`,`t`.`task_description` AS `task_description`,`t`.`task_remarks` AS `task_remarks`,`t`.`task_progress` AS `task_progress`,`t`.`status_id` AS `status_id`,`t`.`task_est_start_date` AS `task_est_start_date`,`t`.`task_est_end_date` AS `task_est_end_date`,`t`.`task_start_date` AS `task_start_date`,`t`.`task_end_date` AS `task_end_date`,`t`.`created_by` AS `created_by`,`t`.`created_at` AS `created_at`,`t`.`updated_at` AS `updated_at`,`t`.`from_task_template_id` AS `from_task_template_id`,`t`.`from_task_template_detail_id` AS `from_task_template_detail_id` from (((`itasks` `t` join `project_sites` `ps` on((`ps`.`site_id` = `t`.`site_id`))) join `project_groups` `pg` on((`pg`.`group_id` = `ps`.`group_id`))) join `projects` `p` on((`p`.`project_id` = `pg`.`project_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_project_milestone_progress`
--

/*!50001 DROP VIEW IF EXISTS `v_project_milestone_progress`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_project_milestone_progress` AS select `project_milestones`.`milestone_id` AS `milestone_id`,`project_milestones`.`project_id` AS `project_id`,`project_milestones`.`milestone_code` AS `milestone_code`,`project_milestones`.`milestone_info` AS `milestone_info`,`project_milestones`.`milestone_sequence` AS `milestone_sequence`,`project_milestones`.`created_by` AS `created_by`,`project_milestones`.`created_at` AS `created_at`,`project_milestones`.`updated_at` AS `updated_at`,`project_milestones`.`active_status` AS `active_status`,if(((((`project_milestones`.`milestone_task_completed` + `project_milestones`.`milestone_document_completed`) * 100) / (`project_milestones`.`milestone_task_total` + `project_milestones`.`milestone_document_total`)) is null),0,floor((((`project_milestones`.`milestone_task_completed` + `project_milestones`.`milestone_document_completed`) * 100) / (`project_milestones`.`milestone_task_total` + `project_milestones`.`milestone_document_total`)))) AS `milestone_progress` from `project_milestones` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_project_s_curves_display`
--

/*!50001 DROP VIEW IF EXISTS `v_project_s_curves_display`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`web_db`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_project_s_curves_display` AS select `project_scurves`.`project_id` AS `project_id`,concat(convert(substr(monthname(concat(`project_scurves`.`year`,'-',`project_scurves`.`month`,'-01')),1,3) using utf8mb4),'-',substr(`project_scurves`.`year`,3,2)) AS `month_of`,`project_scurves`.`expected_total_task` AS `expected_total_task`,`project_scurves`.`actual_total_task` AS `actual_total_task`,`project_scurves`.`expected_total_document` AS `expected_total_document`,`project_scurves`.`actual_total_document` AS `actual_total_document` from `project_scurves` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-25 10:46:46
