@extends('layouts.app')

@section('content')
<style>
    .done {
        text-decoration: line-through;
    }
</style>

<title>To Do App</title>

<body>
    <main id="app">
        <h3>@{{ title }}</h3>
        <form @submit.prevent="addTodo">
            <div>
                <label>New To Do List</label>
            </div>
            <br>
            <div>
                <input v-model="newTodo" type="text" name="newTodo" id="newTodo">
            </div>
            <br>
            <div>
                <button type="submit" name="submitButton">Add To Do</button>
            </div>
            <div>
                <button @click="allDone" type="button" name="allDone">All Done</button>
            </div>
            <div>
                <button @click="allRemove" type="button" name="allRemove">All Remove</button>
            </div>
            <ul>
                <li v-for="todo in todos">
                    <input v-model="todo.done" type="checkbox">
                    {{-- <input type="checkbox" :checked="{ todo.done }"> --}}
                    <span :class="{ done: todo.done }">@{{ todo.title }}</span>
                    <button @click="removeTodo(todo)" type="button" name="remove">Remove</button>
                </li>
            </ul>
        </form>
    </main>
</body>

<script src="{{ url('vue/vue_app.js') }}"></script>

@endsection

