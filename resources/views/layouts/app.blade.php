<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.header')
    </head>

    @include('layouts.script')

    @yield('content')

    @include('layouts.footer')

</html>

