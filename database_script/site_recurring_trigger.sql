DROP TRIGGER IF EXISTS site_document_recurring_insert;
DELIMITER $$

CREATE TRIGGER site_document_recurring_insert
    AFTER INSERT
    ON site_document_recurrings FOR EACH ROW

BEGIN

	SET @site_id = null, @document_category_id = null , @document_type_id = null, @milestone_id = null;

    Select site_id, document_category_id, document_type_id, milestone_id
    into @site_id, @document_category_id, @document_type_id, @milestone_id
    from site_documents where site_document_id = NEW.site_document_id;

    SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);

    IF @document_category_id = 1 THEN

		IF @document_type_id = 2 THEN

			SET @site_total_document = @site_total_document + 1;

			Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;

			SET @project_id = null, @group_id = null;

            Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;

            SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);

            SET @group_total_document = @group_total_document + 1;

            SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);

            SET @project_total_document = @project_total_document + 1;

            Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;

            Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;

            SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = @milestone_id);

            SET @milestone_document_total = @milestone_document_total + 1;

            Update project_milestones
            SET
            milestone_document_total = @milestone_document_total
            Where project_id = @project_id and milestone_id = @milestone_id;

		END IF;



     END IF;

  END
$$ DELIMITER ;


DROP TRIGGER IF EXISTS site_document_recurring_delete;
DELIMITER $$

CREATE TRIGGER site_document_recurring_delete
    AFTER DELETE
    ON site_document_recurrings FOR EACH ROW

BEGIN

	SET @site_id = null, @document_category_id = null , @document_type_id = null, @milestone = null;

    Select site_id, document_category_id, document_type_id, milestone_id
    into @site_id, @document_category_id, @document_type_id, @milestone_id
    from site_documents where site_document_id = OLD.site_document_id;

    IF @document_category_id = 1 THEN

		IF @document_type_id = 2 THEN

			SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);

			SET @site_total_document = @site_total_document - 1;

			Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;

            SET @project_id = null, @group_id = null;

            Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;

            SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);

            SET @group_total_document = @group_total_document - 1;

            SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);

            SET @project_total_document = @project_total_document - 1;

            Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;

            Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;

			SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = @milestone_id);

            SET @milestone_document_total = @milestone_document_total - 1;

            Update project_milestones
            SET
            milestone_document_total = @milestone_document_total
            Where project_id = @project_id and milestone_id = @milestone_id;

		END IF;

    END IF;

  END
$$ DELIMITER ;


DROP TRIGGER IF EXISTS site_document_approval_update;
DELIMITER $$

CREATE TRIGGER site_document_approval_update
    AFTER UPDATE
    ON site_document_approvals FOR EACH ROW

BEGIN

	SET @site_id = null, @approval_count_check = 0, @null_check = 0, @milestone_id = null;

    Select site_id, milestone_id into @site_id, @milestone_id from site_documents where site_document_id = OLD.site_document_id;

    SET @null_check = (Select count(*) from site_document_approvals where site_document_upload_id = OLD.site_document_upload_id and code_by_approval is null);

    SET @approval_count_check = (Select count(*) from site_document_approvals where site_document_upload_id = OLD.site_document_upload_id and code_by_approval in ('2','3','4'));

	IF ( OLD.for_status = '3' AND @null_check = 0 AND @approval_count_check = 0 ) THEN

		SET @site_completed_document = (Select site_completed_document from project_sites where site_id = @site_id);

		SET @site_completed_document = @site_completed_document + 1;

		Update project_sites
		SET
		site_completed_document = @site_completed_document
		Where site_id = @site_id;

        SET @project_id = null, @group_id = null;

		Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;

		SET @group_completed_document = (Select group_completed_document from project_groups where group_id = @group_id);

		SET @group_completed_document = @group_completed_document + 1;

		SET @project_completed_document = (Select project_completed_document from projects where project_id = @project_id);

		SET @project_completed_document = @project_completed_document + 1;

		Update project_groups
		SET
		group_completed_document = @group_completed_document
		Where group_id = @group_id;

		Update projects
		SET
		project_completed_document = @project_completed_document
		Where project_id = @project_id;

		SET @milestone_document_completed = (Select milestone_document_completed from project_milestones where project_id = @project_id and milestone_id = @milestone_id);

		SET @milestone_document_completed = @milestone_document_completed + 1;

		Update project_milestones
		SET
		milestone_document_completed = @milestone_document_completed
		Where project_id = @project_id and milestone_id = @milestone_id;

	END IF;

  END
$$ DELIMITER ;

