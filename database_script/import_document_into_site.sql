CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_document_into_site`(
site_id_x bigint,
document_template_id_x bigint,
created_by_x bigint,
project_id_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_document_template_id bigint;
    DECLARE v_document_template_detail_id bigint;
    DECLARE v_document_template_title varchar(100);
	DECLARE v_document_template_mandatory tinyint;
    DECLARE v_document_category_id bigint;
    DECLARE v_document_type_id bigint;
    DECLARE v_recurring_interval_id bigint;
    DECLARE v_req_approval_project_owner tinyint;
	DECLARE v_req_approval_project_manager tinyint;
    DECLARE v_req_approval_project_engineer tinyint;
    DECLARE v_req_approval_engineer tinyint;
    DECLARE v_req_approval_qa_qc tinyint;
    DECLARE v_req_approval_safety tinyint;
	DECLARE v_req_approval_onm tinyint;
    DECLARE v_req_approval_planner tinyint;
    DECLARE v_req_approval_purchasing tinyint;
    DECLARE v_req_approval_admin tinyint;
    DECLARE v_milestone_template_detail_id bigint;
    DECLARE v_milestone_id bigint;

    DECLARE x datetime;

	DECLARE x_cursor CURSOR FOR
	Select
	document_template_id, document_template_detail_id, document_template_title, document_template_mandatory, document_category_id,
	document_type_id, recurring_interval_id, req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer,
	req_approval_engineer, req_approval_qa_qc, req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin,
    milestone_template_detail_id, milestone_id from
	document_template_details
	INNER JOIN project_milestones
    ON project_milestones.from_milestone_template_detail_id = document_template_details.milestone_template_detail_id
    WHERE document_template_id = document_template_id_x and project_milestones.project_id = project_id_x;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;

    SET @site_start_date = (Select site_start_date from project_sites where site_id = site_id_x);
    SET @site_end_date = (Select site_end_date from project_sites where site_id = site_id_x);

	OPEN x_cursor;

	get_x: LOOP

	FETCH x_cursor INTO v_document_template_id, v_document_template_detail_id, v_document_template_title, v_document_template_mandatory,
    v_document_category_id, v_document_type_id, v_recurring_interval_id, v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer,
	v_req_approval_engineer, v_req_approval_qa_qc, v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin,
    v_milestone_template_detail_id, v_milestone_id;

	IF v_finished = 1 THEN
	LEAVE get_x;
	END IF;

    SET @check_available = 0;

    SET @check_available = (Select Count(site_document_id) From site_documents where from_site_document_template_detail_id = v_document_template_detail_id and site_id = site_id_x);

	IF @check_available = 0 THEN

		IF v_document_type_id = 1 THEN

			INSERT INTO site_documents
			(site_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id, status_id, created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin,
            from_site_document_template_id, from_site_document_template_detail_id, milestone_id)
			VALUES
			(site_id_x, v_document_template_title, v_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id, 1, created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc,
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_document_template_id, v_document_template_detail_id, v_milestone_id);

        END IF;

        IF v_document_type_id = 2 THEN

			INSERT INTO site_documents
			(site_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id, created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin,
            from_site_document_template_id, from_site_document_template_detail_id,
            recurring_interval_id, recurring_start_date, recurring_end_date, milestone_id)
			VALUES
			(site_id_x, v_document_template_title, v_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id,  created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc,
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_document_template_id, v_document_template_detail_id,
            v_recurring_interval_id, @site_start_date, @site_end_date, v_milestone_id);

            SET @last_site_document_id = LAST_INSERT_ID();

			SET x = @site_start_date;

			loop_date:  LOOP
				IF  x > @site_end_date THEN
					LEAVE  loop_date;
				END  IF;

                INSERT INTO site_document_recurrings
                (site_document_id, document_recurring_date, current_status, created_by)
                VALUES
                (@last_site_document_id, x, 1, created_by_x);

                IF v_recurring_interval_id = 1 THEN
					SET x = DATE_ADD(x, INTERVAL 1 DAY);
                END IF;

                IF v_recurring_interval_id = 2 THEN
					SET x = DATE_ADD(x, INTERVAL 1 WEEK);
                END IF;

                IF v_recurring_interval_id = 3 THEN
					SET x = DATE_ADD(x, INTERVAL 1 MONTH);
                END IF;

			END LOOP;

        END IF;

    END IF;

    -- Select @site_start_date, @site_end_date, @check_available, v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;

	END LOOP get_x;

	CLOSE x_cursor;

    Select 'OK';

END
