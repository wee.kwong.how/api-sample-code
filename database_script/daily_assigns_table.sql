CREATE TABLE `daily_assigns` (
  `daily_assign_id` bigint NOT NULL AUTO_INCREMENT,
  `assign_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `assign_to_user_id` bigint DEFAULT NULL,
  `assign_module_code` varchar(100) DEFAULT NULL,
  `project_id` bigint DEFAULT NULL,
  `group_id` bigint DEFAULT NULL,
  `site_id` bigint DEFAULT NULL,
  `assign_to_id` bigint DEFAULT NULL,
  `assign_to_title` varchar(300) DEFAULT NULL,
  `assign_link` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `completed_flag` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`daily_assign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
