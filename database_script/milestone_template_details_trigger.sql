DROP TRIGGER IF EXISTS milestone_template_details_insert;
DELIMITER $$

CREATE TRIGGER milestone_template_details_insert
    AFTER INSERT
    ON milestone_template_details FOR EACH ROW

BEGIN

	Select milestone_details_no into @milestone_details_no from milestone_templates where milestone_template_id = NEW.milestone_template_id;

    SET @milestone_details_no = @milestone_details_no + 1;

    Update milestone_templates
    SET
    milestone_details_no = @milestone_details_no
    Where milestone_template_id = NEW.milestone_template_id;

END$$

DELIMITER ;

DROP TRIGGER IF EXISTS milestone_template_details_delete;
DELIMITER $$

CREATE TRIGGER milestone_template_details_delete
    AFTER DELETE
    ON milestone_template_details FOR EACH ROW

BEGIN

	Select milestone_details_no into @milestone_details_no from milestone_templates where milestone_template_id = OLD.milestone_template_id;

    SET @milestone_details_no = @milestone_details_no - 1;

    Update milestone_templates
    SET
    milestone_details_no = @milestone_details_no
    Where milestone_template_id = OLD.milestone_template_id;

END$$

DELIMITER ;
