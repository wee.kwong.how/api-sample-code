CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_project_task_into_project`(
project_task_template_idx bigint,
created_by_x bigint,
project_id_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_project_task_template_detail_id bigint;
    DECLARE v_project_task_template_id bigint;
    DECLARE v_project_task_title varchar(100);
	DECLARE v_project_task_description varchar(300);
    DECLARE v_status_id bigint;

	DECLARE x_cursor CURSOR FOR
	Select project_task_template_detail_id, project_task_template_id, project_task_title, project_task_description, status_id  from
	project_task_template_details
	where project_task_template_id = project_task_template_idx and active_status = 1;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;

    SET @project_start_date = (Select project_start_date from projects where project_id = project_id_x);
    SET @project_end_date = (Select project_end_date from projects where project_id = project_id_x);

	OPEN x_cursor;

	get_x: LOOP

	FETCH x_cursor INTO v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;

	IF v_finished = 1 THEN
	LEAVE get_x;
	END IF;

    SET @check_available = 0;

    SET @check_available = (Select Count(task_id) From project_itasks where from_project_task_template_detail_id = v_project_task_template_detail_id and project_id = project_id_x);

	IF @check_available = 0 THEN

		INSERT INTO project_itasks
        (task_title, task_description, status_id, task_est_start_date, task_est_end_date, created_by, from_project_task_template_id, from_project_task_template_detail_id, project_id, assign_to_user)
        VALUES
        (v_project_task_title, v_project_task_description, v_status_id, @project_start_date, @project_end_date, created_by_x, v_project_task_template_id, v_project_task_template_detail_id, project_id_x, created_by_x);

    END IF;

    -- Select @project_start_date, @project_end_date, @check_available, v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;

	END LOOP get_x;

	CLOSE x_cursor;

    Select 'OK';

END
