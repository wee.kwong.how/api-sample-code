CREATE PROCEDURE `sp_delete_project_document`(
project_document_id_x bigint,
delete_user_id_x bigint
)
BEGIN

	SET @count_x = 0;
    
    SET @count_x = (Select count(project_document_id) from project_documents where project_document_id = project_document_id_x);
    
    IF ( @count_x = 1 ) THEN
 
		SET @project_id = NULL;
        SET @document_type_id = NULL;
        SET @document_category_id = NULL;
        SET @document_mandatory = NULL;
        
        Select project_id, document_type_id, document_category_id, document_mandatory into
        @project_id, @document_type_id, @document_category_id, @document_mandatory 
        from project_documents where project_document_id = project_document_id_x;
	 
		SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);
		
		IF ( @document_type_id = 1 AND (@document_category_id = 1 OR @document_category_id = 2) AND @document_mandatory = 1 ) THEN
			
			SET @project_level_total_document = @project_level_total_document - 1;
				  
			Update projects
			SET
			project_level_total_document = @project_level_total_document
			Where project_id = @project_id;
			
		END IF;
        
        INSERT INTO `project_documents_deleted`
		(`project_document_id`,`project_id`,`document_title`,`document_mandatory`,`assign_to_user`,
		`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,
		`req_approval_qa_qc`,`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,
		`req_approval_purchasing`,`req_approval_admin`,`from_project_document_template_id`,
		`from_project_document_template_detail_id`,`completed_flag`,`deleted_by`)
		Select `project_document_id`,`project_id`,`document_title`,`document_mandatory`,`assign_to_user`,
		`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,
		`req_approval_qa_qc`,`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,
		`req_approval_purchasing`,`req_approval_admin`,`from_project_document_template_id`,
		`from_project_document_template_detail_id`,`completed_flag`, delete_user_id_x
        From project_documents where project_document_id = project_document_id_x;
        
		delete from project_document_approvals where project_document_id = project_document_id_x;
        delete from project_document_uploads where project_document_id = project_document_id_x;
        delete from project_document_recurrings where project_document_id = project_document_id_x;
        delete from project_documents where project_document_id = project_document_id_x;
		
    END IF;
    
    Select "Completed";
         
END