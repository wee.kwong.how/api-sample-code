DROP TRIGGER IF EXISTS project_document_template_details_insert;
DELIMITER $$

CREATE TRIGGER project_document_template_details_insert
    AFTER INSERT
    ON project_document_template_details FOR EACH ROW

BEGIN

	Select document_details_no into @document_details_no from project_document_templates where project_document_template_id = NEW.project_document_template_id;

    SET @document_details_no = @document_details_no + 1;

    Update project_document_templates
    SET
    document_details_no = @document_details_no
    Where project_document_template_id = NEW.project_document_template_id;

END
$$ DELIMITER ;

DROP TRIGGER IF EXISTS project_document_template_details_delete;
DELIMITER $$

CREATE TRIGGER project_document_template_details_delete
    AFTER DELETE
    ON project_document_template_details FOR EACH ROW

BEGIN

 	Select document_details_no into @document_details_no from project_document_templates where project_document_template_id = OLD.project_document_template_id;

     SET @document_details_no = @document_details_no - 1;

     Update project_document_templates
     SET
     document_details_no = @document_details_no
     Where project_document_template_id = OLD.project_document_template_id;

END
$$ DELIMITER ;
