DROP TRIGGER IF EXISTS document_template_details_insert;
DELIMITER $$

CREATE TRIGGER document_template_details_insert
    AFTER INSERT
    ON document_template_details FOR EACH ROW

BEGIN

	Select document_details_no into @document_details_no from document_templates where document_template_id = NEW.document_template_id;

    SET @document_details_no = @document_details_no + 1;

    Update document_templates
    SET
    document_details_no = @document_details_no
    Where document_template_id = NEW.document_template_id;

END$$

DELIMITER ;

DROP TRIGGER IF EXISTS document_template_details_delete;
DELIMITER $$

CREATE TRIGGER document_template_details_delete
    AFTER DELETE
    ON document_template_details FOR EACH ROW

BEGIN

	Select document_details_no into @document_details_no from document_templates where document_template_id = OLD.document_template_id;

    SET @document_details_no = @document_details_no - 1;

    Update document_templates
    SET
    document_details_no = @document_details_no
    Where document_template_id = OLD.document_template_id;

END$$

DELIMITER ;
