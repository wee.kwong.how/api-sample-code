DROP TRIGGER IF EXISTS project_task_template_details_insert;
DELIMITER $$

CREATE TRIGGER project_task_template_details_insert
    AFTER INSERT
    ON project_task_template_details FOR EACH ROW

BEGIN

	Select task_details_no into @task_details_no from project_task_templates where project_task_template_id = NEW.project_task_template_id;

    SET @task_details_no = @task_details_no + 1;

    Update project_task_templates
    SET
    task_details_no = @task_details_no
    Where project_task_template_id = NEW.project_task_template_id;

END
$$ DELIMITER ;

DROP TRIGGER IF EXISTS project_task_template_details_delete;
DELIMITER $$

CREATE TRIGGER project_task_template_details_delete
    AFTER DELETE
    ON project_task_template_details FOR EACH ROW

BEGIN

 	Select task_details_no into @task_details_no from project_task_templates where project_task_template_id = OLD.project_task_template_id;

     SET @task_details_no = @task_details_no - 1;

     Update project_task_templates
     SET
     task_details_no = @task_details_no
     Where project_task_template_id = OLD.project_task_template_id;

END
$$ DELIMITER ;
