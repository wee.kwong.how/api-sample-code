DROP TRIGGER IF EXISTS project_document_recurring_insert;
DELIMITER $$

CREATE TRIGGER project_document_recurring_insert
    AFTER INSERT
    ON project_document_recurrings FOR EACH ROW

BEGIN

	SET @project_id = null, @document_category_id = null , @document_type_id = null;

    Select project_id, document_category_id, document_type_id into @project_id, @document_category_id, @document_type_id from project_documents where project_document_id = NEW.project_document_id;

    SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);

    IF @document_category_id = 1 THEN

		IF @document_type_id = 2 THEN

			SET @project_level_total_document = @project_level_total_document + 1;

		END IF;

        Update projects
		SET
		project_level_total_document = @project_level_total_document
		Where project_id = @project_id;

     END IF;

  END
$$ DELIMITER ;


DROP TRIGGER IF EXISTS project_document_recurring_delete;
DELIMITER $$

CREATE TRIGGER project_document_recurring_delete
    AFTER DELETE
    ON project_document_recurrings FOR EACH ROW

BEGIN

	SET @project_id = null, @document_category_id = null , @document_type_id = null;

    Select project_id, document_category_id, document_type_id into @project_id, @document_category_id, @document_type_id from project_documents where project_document_id = OLD.project_document_id;

    IF @document_category_id = 1 THEN

		IF @document_type_id = 2 THEN

			SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);

			SET @project_level_total_document = @project_level_total_document - 1;

			SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);

			SET @project_level_completed_document = @project_level_completed_document - 1;

			Update projects
			SET
			project_level_total_document = @project_level_total_document,
			project_level_completed_document = @project_level_completed_document
			Where project_id = @project_id;

		END IF;

    END IF;

  END
$$ DELIMITER ;



-- DROP TRIGGER IF EXISTS project_document_recurring_update;
-- DELIMITER $$

-- CREATE TRIGGER project_document_recurring_update
--     AFTER UPDATE
--     ON project_document_recurrings FOR EACH ROW
--
-- BEGIN

-- 	IF NEW.document_category_id = 1 THEN
--
-- 		IF NEW.document_type_id = 1 THEN
--
-- 			IF ( NEW.current_status = '3' ) THEN
--
-- 				SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);
--
-- 				SET @project_level_completed_document = @project_level_completed_document + 1;
--
-- 				Update projects
-- 				SET
-- 				project_level_completed_document = @project_level_completed_document
-- 				Where project_id = @project_id;
--
-- 			 END IF;
--
-- 		 END IF;
--
--      END IF;
--
--   END
-- $$ DELIMITER ;
