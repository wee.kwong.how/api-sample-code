# Assign to User After Create Project

DROP TRIGGER IF EXISTS project_assign_after_insert;
DELIMITER $$

CREATE TRIGGER project_assign_after_insert
    AFTER INSERT
    ON projects FOR EACH ROW
BEGIN

	# ENGINEER ASSIGN
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
    VALUES (now(), NEW.project_engineer, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');

    # SAFETY ASSIGN
    INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
    VALUES (now(), NEW.project_safety, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');

END$$
DELIMITER ;

# Assign to User After Update Project

DROP TRIGGER IF EXISTS project_assign_after_update;
DELIMITER $$

CREATE TRIGGER project_assign_after_update
    AFTER UPDATE
    ON projects FOR EACH ROW
BEGIN

	#ENGINEER ASSIGN
    IF ( OLD.project_engineer != NEW.project_engineer ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
		VALUES (now(), NEW.project_engineer, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');
    END IF;

    # SAFETY ASSIGN
	IF ( OLD.project_safety != NEW.project_safety ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
		VALUES (now(), NEW.project_safety, 'PROJECT', NEW.project_id, NEW.project_id, NEW.project_name, 'projectDashboard');
    END IF;

END$$
DELIMITER ;

######################################################

# Assign to User After Create Project Group

DROP TRIGGER IF EXISTS project_group_assign_after_insert;
DELIMITER $$

CREATE TRIGGER project_group_assign_after_insert
    AFTER INSERT
    ON project_groups FOR EACH ROW
BEGIN

	# GROUP ENGINEER ASSIGN
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, assign_to_id, assign_to_title, assign_link )
    VALUES (now(), NEW.group_engineer, 'PROJECT GROUP', NEW.project_id, NEW.group_id, NEW.group_id, NEW.group_name, 'projectGroupDashboard');

END$$
DELIMITER ;

# Assign to User After UPDATE Project Group

DROP TRIGGER IF EXISTS project_group_assign_after_update;
DELIMITER $$

CREATE TRIGGER project_group_assign_after_update
    AFTER UPDATE
    ON project_groups FOR EACH ROW
BEGIN

	# GROUP ENGINEER ASSIGN
    IF ( OLD.group_engineer != NEW.group_engineer ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, assign_to_id, assign_to_title, assign_link )
		VALUES (now(), NEW.group_engineer, 'PROJECT GROUP', NEW.project_id, NEW.group_id, NEW.group_id, NEW.group_name, 'projectGroupDashboard');
    END IF;

END$$
DELIMITER ;

###############################

# Assign to User After CREATE Project Document

DROP TRIGGER IF EXISTS project_document_assign_after_insert;
DELIMITER $$

CREATE TRIGGER project_document_assign_after_insert
    AFTER INSERT
    ON project_documents FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
    VALUES (now(), NEW.assign_to_user, 'PROJECT DOCUMENT', NEW.project_id, NEW.project_document_id, NEW.document_title, 'projectDocumentEditFrProject');

END$$
DELIMITER ;

# Assign to User After UPDATE Project Document

DROP TRIGGER IF EXISTS project_document_assign_after_update;
DELIMITER $$

CREATE TRIGGER project_document_assign_after_update
    AFTER UPDATE
    ON project_documents FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
		VALUES (now(), NEW.assign_to_user, 'PROJECT DOCUMENT', NEW.project_id, NEW.project_document_id, NEW.document_title, 'projectDocumentEditFrProject');
    END IF;

END$$
DELIMITER ;

#################################################################


# Assign to User After CREATE Project TASK

DROP TRIGGER IF EXISTS project_task_assign_after_insert;
DELIMITER $$

CREATE TRIGGER project_task_assign_after_insert
    AFTER INSERT
    ON project_itasks FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE TASK TO
	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
    VALUES (now(), NEW.assign_to_user, 'PROJECT TASK', NEW.project_id, NEW.task_id, NEW.task_title, 'projectTaskEditFrProject');

END$$
DELIMITER ;

# Assign to User After CREATE Project TASK

DROP TRIGGER IF EXISTS project_task_assign_after_update;
DELIMITER $$

CREATE TRIGGER project_task_assign_after_update
    AFTER UPDATE
    ON project_itasks FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE TASK TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, assign_to_id, assign_to_title, assign_link )
		VALUES (now(), NEW.assign_to_user, 'PROJECT TASK', NEW.project_id, NEW.task_id, NEW.task_title, 'projectTaskEditFrProject');
    END IF;

END$$
DELIMITER ;

##################################################


# Assign to User After CREATE SITE Document

DROP TRIGGER IF EXISTS site_document_assign_after_insert;
DELIMITER $$

CREATE TRIGGER site_document_assign_after_insert
    AFTER INSERT
    ON site_documents FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
    Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;

	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link )
    VALUES (now(), NEW.assign_to_user, 'SITE DOCUMENT', @project_id, @group_id, NEW.site_id, NEW.site_document_id, NEW.document_title, 'siteDocumentEdit');

END$$
DELIMITER ;


# Assign to User After UPDATE SITE Document

DROP TRIGGER IF EXISTS site_document_assign_after_update;
DELIMITER $$

CREATE TRIGGER site_document_assign_after_update
    AFTER UPDATE
    ON site_documents FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE DOCUMENT TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;

		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link )
		VALUES (now(), NEW.assign_to_user, 'SITE DOCUMENT', @project_id, @group_id, NEW.site_id, NEW.site_document_id, NEW.document_title, 'siteDocumentEdit');
    END IF;

END$$
DELIMITER ;


##############################################


# Assign to User After CREATE SITE TASK

DROP TRIGGER IF EXISTS site_task_assign_after_insert;
DELIMITER $$

CREATE TRIGGER site_task_assign_after_insert
    AFTER INSERT
    ON itasks FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE TASK TO
    Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;

	INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link )
    VALUES (now(), NEW.assign_to_user, 'SITE TASK', @project_id, @group_id, NEW.site_id, NEW.task_id, NEW.task_title, 'taskEdit');

END$$
DELIMITER ;

# Assign to User After UDPATE SITE TASK

DROP TRIGGER IF EXISTS site_task_assign_after_update;
DELIMITER $$

CREATE TRIGGER site_task_assign_after_update
    AFTER UPDATE
    ON itasks FOR EACH ROW
BEGIN

	# ANYONE ASSIGN THE TASK TO
    IF ( OLD.assign_to_user != NEW.assign_to_user ) THEN
		Select project_id, group_id into @project_id, @group_id from project_sites where site_id = NEW.site_id;

		INSERT INTO daily_assigns ( assign_date, assign_to_user_id, assign_module_code, project_id, group_id, site_id, assign_to_id, assign_to_title, assign_link )
		VALUES (now(), NEW.assign_to_user, 'SITE TASK', @project_id, @group_id, NEW.site_id, NEW.task_id, NEW.task_title, 'taskEdit');
    END IF;

END$$
DELIMITER ;




























