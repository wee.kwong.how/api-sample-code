CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_project_document_into_project`(
project_document_template_idx bigint,
created_by_x bigint,
project_id_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_project_document_template_id bigint;
    DECLARE v_project_document_template_detail_id bigint;
    DECLARE v_project_document_template_title varchar(100);
	DECLARE v_project_document_template_mandatory tinyint;
    DECLARE v_document_category_id bigint;
    DECLARE v_document_type_id bigint;
    DECLARE v_recurring_interval_id bigint;
    DECLARE v_req_approval_project_owner tinyint;
	DECLARE v_req_approval_project_manager tinyint;
    DECLARE v_req_approval_project_engineer tinyint;
    DECLARE v_req_approval_engineer tinyint;
    DECLARE v_req_approval_qa_qc tinyint;
    DECLARE v_req_approval_safety tinyint;
	DECLARE v_req_approval_onm tinyint;
    DECLARE v_req_approval_planner tinyint;
    DECLARE v_req_approval_purchasing tinyint;
    DECLARE v_req_approval_admin tinyint;

    DECLARE x datetime;

	DECLARE x_cursor CURSOR FOR
	Select
	project_document_template_id, project_document_template_detail_id, project_document_template_title, project_document_template_mandatory, document_category_id,
	document_type_id, recurring_interval_id, req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer,
	req_approval_engineer, req_approval_qa_qc, req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin from
	project_document_template_details
	where project_document_template_id = project_document_template_idx and active_status = 1;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;

    SET @project_start_date = (Select project_start_date from projects where project_id = project_id_x);
    SET @project_end_date = (Select project_end_date from projects where project_id = project_id_x);

	OPEN x_cursor;

	get_x: LOOP

	FETCH x_cursor INTO v_project_document_template_id, v_project_document_template_detail_id, v_project_document_template_title, v_project_document_template_mandatory,
    v_document_category_id, v_document_type_id, v_recurring_interval_id, v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer,
	v_req_approval_engineer, v_req_approval_qa_qc, v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin;

	IF v_finished = 1 THEN
	LEAVE get_x;
	END IF;

    SET @check_available = 0;

    SET @check_available = (Select Count(project_document_id) From project_documents where from_project_document_template_detail_id = v_project_document_template_detail_id and project_id = project_id_x);

	IF @check_available = 0 THEN

		IF v_document_type_id = 1 THEN

			INSERT INTO project_documents
			(project_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id, status_id, created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin,
            from_project_document_template_id, from_project_document_template_detail_id)
			VALUES
			(project_id_x, v_project_document_template_title, v_project_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id, 1, created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc,
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_project_document_template_id, v_project_document_template_detail_id);

        END IF;

        IF v_document_type_id = 2 OR v_document_type_id = 3 THEN

			INSERT INTO project_documents
			(project_id, document_title, document_mandatory, assign_to_user, document_category_id, document_type_id,  created_by,
            req_approval_project_owner, req_approval_project_manager, req_approval_project_engineer, req_approval_engineer, req_approval_qa_qc,
            req_approval_safety, req_approval_onm, req_approval_planner, req_approval_purchasing, req_approval_admin,
            from_project_document_template_id, from_project_document_template_detail_id,
            recurring_interval_id, recurring_start_date, recurring_end_date)
			VALUES
			(project_id_x, v_project_document_template_title, v_project_document_template_mandatory, created_by_x, v_document_category_id, v_document_type_id, created_by_x,
            v_req_approval_project_owner, v_req_approval_project_manager, v_req_approval_project_engineer, v_req_approval_engineer, v_req_approval_qa_qc,
			v_req_approval_safety, v_req_approval_onm, v_req_approval_planner, v_req_approval_purchasing, v_req_approval_admin, v_project_document_template_id, v_project_document_template_detail_id,
            v_recurring_interval_id, @project_start_date, @project_end_date);

            SET @last_project_document_id = LAST_INSERT_ID();

			SET x = @project_start_date;

			loop_date:  LOOP
				IF  x > @project_end_date THEN
					LEAVE  loop_date;
				END  IF;

                INSERT INTO project_document_recurrings
                (project_document_id, document_recurring_date, current_status, created_by)
                VALUES
                (@last_project_document_id, x, 1, created_by_x);

                IF v_recurring_interval_id = 1 THEN
					SET x = DATE_ADD(x, INTERVAL 1 DAY);
                END IF;

                IF v_recurring_interval_id = 2 THEN
					SET x = DATE_ADD(x, INTERVAL 1 WEEK);
                END IF;

                IF v_recurring_interval_id = 3 THEN
					SET x = DATE_ADD(x, INTERVAL 1 MONTH);
                END IF;

			END LOOP;

        END IF;

    END IF;

    -- Select @project_start_date, @project_end_date, @check_available, v_project_task_template_detail_id, v_project_task_template_id, v_project_task_title, v_project_task_description, v_status_id;

	END LOOP get_x;

	CLOSE x_cursor;

    Select 'OK';

END
