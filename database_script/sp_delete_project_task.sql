CREATE PROCEDURE `sp_delete_project_task`(
task_id_x bigint,
delete_user_id_x bigint
)
BEGIN
	
   SET @count_x = 0;
   
   SET @count_x = (Select count(task_id) from project_itasks where task_id = task_id_x);
   
   IF ( @count_x = 1 ) THEN

		SET @project_id = NULL;
        SET @project_id = (Select project_id from project_itasks where task_id = task_id_x);
	 
		SET @project_level_total_task = (Select project_level_total_task from projects where project_id = @project_id);
		  
		SET @project_level_total_task = @project_level_total_task - 1;
		 
		Update projects
		SET
		project_level_total_task = @project_level_total_task
		Where project_id = @project_id;
        
        INSERT INTO `project_itasks_deleted`
		(`task_id`, `project_id`, `task_title`, `assign_to_user`, `task_description`,
		`task_remarks`, `task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`, `created_at`, `updated_at`,
		`from_project_task_template_id`, `from_project_task_template_detail_id`, `deleted_by`)
        Select `task_id`, `project_id`, `task_title`, `assign_to_user`, `task_description`,
		`task_remarks`, `task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`, `created_at`, `updated_at`,
		`from_project_task_template_id`, `from_project_task_template_detail_id`, delete_user_id_x from project_itasks where task_id = task_id_x;
        
        DELETE FROM project_itasks where task_id = task_id_x;

	END IF;
    
	Select "Completed";
END