DROP TRIGGER IF EXISTS site_document_insert;
DELIMITER $$

CREATE TRIGGER site_document_insert
    AFTER INSERT
    ON site_documents FOR EACH ROW

BEGIN

    SET @site_id = NEW.site_id;

    SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);

    IF NEW.document_category_id = 1 THEN

		IF NEW.document_type_id = 1 THEN

			SET @site_total_document = @site_total_document + 1;

			Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;

            SET @project_id = null, @group_id = null;

            Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;

            SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);

            SET @group_total_document = @group_total_document + 1;

            SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);

            SET @project_total_document = @project_total_document + 1;

            Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;

            Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;

            SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);

            SET @milestone_document_total = @milestone_document_total + 1;

            Update project_milestones
            SET
            milestone_document_total = @milestone_document_total
            Where project_id = @project_id and milestone_id = NEW.milestone_id;

		END IF;

     END IF;

  END
$$ DELIMITER ;


DROP TRIGGER IF EXISTS site_document_delete;
DELIMITER $$

CREATE TRIGGER site_document_delete
    AFTER DELETE
    ON site_documents FOR EACH ROW

BEGIN

    SET @site_id = OLD.site_id;

    IF OLD.document_category_id = 1 THEN

		IF OLD.document_type_id = 1 THEN

			SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);

			SET @site_total_document = @site_total_document - 1;

			Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;

			SET @project_id = null, @group_id = null;

            Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;

            SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);

            SET @group_total_document = @group_total_document - 1;

            SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);

            SET @project_total_document = @project_total_document - 1;

            Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;

            Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;

			SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = OLD.milestone_id);

            SET @milestone_document_total = @milestone_document_total - 1;

            Update project_milestones
            SET
            milestone_document_total = @milestone_document_total
            Where project_id = @project_id and milestone_id = OLD.milestone_id;

		END IF;

    END IF;

  END
$$ DELIMITER ;

