CREATE PROCEDURE `sp_delete_site_document`(
site_document_id_x bigint,
delete_user_id_x bigint
)
BEGIN

	SET @count_x = 0;
    
    SET @count_x = (Select count(site_document_id) from site_documents where site_document_id = site_document_id_x);
    
    IF ( @count_x = 1) THEN

		SET @document_type_id = NULL;
		SET @document_category_id = NULL;
		SET @document_mandatory = NULL;
		SET @milestone_id = NULL;
		SET @site_id = NULL;
		
		Select site_id, milestone_id, document_type_id, document_category_id, document_mandatory into 
		@site_id, @milestone_id, @document_type_id, @document_category_id, @document_mandatory from site_documents where site_document_id = site_document_id_x;
	   
		IF ( @document_type_id = 1 AND (@document_category_id = 1 OR @document_category_id = 2) AND @document_mandatory = 1 )THEN
		
			Select project_id, group_id into @project_id, @group_id from project_sites where site_id = @site_id;
			
			SET @site_total_document = (Select site_total_document from project_sites where site_id = @site_id);
			
			SET @group_total_document = (Select group_total_document from project_groups where group_id = @group_id);  
			
			SET @project_total_document = (Select project_total_document from projects where project_id = @project_id);
			
			SET @milestone_document_total = (Select milestone_document_total from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
			 
			SET @site_total_document = @site_total_document - 1;   
			SET @group_total_document = @group_total_document - 1;
			SET @project_total_document = @project_total_document - 1;
			SET @milestone_document_total = @milestone_document_total - 1;
		  
			Update project_sites
			SET
			site_total_document = @site_total_document
			Where site_id = @site_id;
				
			Update project_groups
			SET
			group_total_document = @group_total_document
			Where group_id = @group_id;
				
			Update projects
			SET
			project_total_document = @project_total_document
			Where project_id = @project_id;
				
			Update project_milestones
			SET
			milestone_document_total = @milestone_document_total
			Where project_id = @project_id and milestone_id = @milestone_id;
			
		END IF;
        
        INSERT INTO `site_documents_deleted`
		(`site_document_id`,`site_id`,`document_title`,`document_mandatory`,`contractor_id`,`assign_to_user`,
		`milestone_id`,`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,`req_approval_qa_qc`,
		`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,`req_approval_purchasing`,`req_approval_admin`,
		`from_site_document_template_id`,`from_site_document_template_detail_id`,`completed_flag`,`deleted_by`)
        Select `site_document_id`,`site_id`,`document_title`,`document_mandatory`,`contractor_id`,`assign_to_user`,
		`milestone_id`,`document_category_id`,`document_type_id`,`recurring_interval_id`,`recurring_start_date`,
		`recurring_end_date`,`status_id`,`created_by`,`created_at`,`updated_at`,`req_approval_project_owner`,
		`req_approval_project_manager`,`req_approval_project_engineer`,`req_approval_engineer`,`req_approval_qa_qc`,
		`req_approval_safety`,`req_approval_onm`,`req_approval_planner`,`req_approval_purchasing`,`req_approval_admin`,
		`from_site_document_template_id`,`from_site_document_template_detail_id`,`completed_flag`, delete_user_id_x
        from site_documents where site_document_id = site_document_id_x;
        
        delete from site_document_approvals where site_document_id = site_document_id_x;
        delete from site_document_uploads where site_document_id = site_document_id_x;
        delete from site_document_recurrings where site_document_id = site_document_id_x;
        delete from site_documents where site_document_id = site_document_id_x;
        
    END IF;

Select "Completed";

END