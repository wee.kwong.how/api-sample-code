CREATE PROCEDURE `sp_delete_site_task`(
task_id_x bigint,
delete_user_id_x bigint
)
BEGIN

   SET @count_x = 0;
   
   SET @count_x = (Select count(task_id) from itasks where task_id = task_id_x);
   
   IF ( @count_x = 1 ) THEN

	SET @site_id = NULL;
	SET @milestone_id = NULL;
	SET @task_est_start_date = NULL;
	SET @task_est_end_date = NULL;
	SET @task_start_date = NULL;
	SET @task_end_date = NULL;
   
	Select site_id, milestone_id, task_est_start_date, task_est_end_date, task_start_date, task_end_date into 
	@site_id, @milestone_id, @task_est_start_date, @task_est_end_date, @task_start_date, @task_end_date from 
	itasks where task_id = task_id_x;
   
	SET @group_id = (Select group_id from project_sites where site_id = @site_id);
	SET @project_id = (Select project_id from project_groups where group_id = @group_id);
    
	SET @site_total_task = (Select site_total_task from project_sites where site_id = @site_id);
	SET @group_total_task = (Select group_total_task from project_groups where group_id = @group_id);
	SET @project_total_task = (Select project_total_task from projects where project_id = @project_id);
     
	SET @site_total_task = @site_total_task - 1;
	SET @group_total_task = @group_total_task - 1;
	SET @project_total_task = @project_total_task - 1;
    
	Update project_sites
	SET
	site_total_task = @site_total_task
	Where site_id = @site_id;
     
	Update project_groups
	SET
	group_total_task = @group_total_task
	Where group_id = @group_id;
    
	Update projects
	SET
	project_total_task = @project_total_task
	Where project_id = @project_id;
	
	SET @milestone_task_total = (Select milestone_task_total from project_milestones where project_id = @project_id and milestone_id = @milestone_id);
            
	SET @milestone_task_total = @milestone_task_total - 1;
            
	Update project_milestones
	SET
	milestone_task_total = @milestone_task_total
	Where project_id = @project_id and milestone_id = @milestone_id;


   ## Update Project S-Curve EST
	SET @project_scurve_id = NULL;
   
   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task 
   FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(@task_est_end_date) AND `year` = YEAR(@task_est_end_date);
   
   IF (@project_scurve_id IS NOT NULL) THEN
   
   	SET @expected_total_task = @expected_total_task - 1;
   	
   	UPDATE project_scurves
   	SET
   	expected_total_task = @expected_total_task
   	WHERE project_scurve_id = @project_scurve_id;
   
   END IF; 
   
   ## Update Project S-Curve Actual
   SET @project_scurve_id = NULL;
   
   IF ( @task_end_date IS NOT NULL ) THEN
   
	   SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task 
	   FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(@task_end_date) AND `year` = YEAR(@task_end_date);
	   
	   IF (@project_scurve_id IS NOT NULL) THEN
	   
	   	SET @actual_total_task = @actual_total_task - 1;
	   	
	   	UPDATE project_scurves
	   	SET
	   	actual_total_task = @actual_total_task
	   	WHERE project_scurve_id = @project_scurve_id;
	   
	   END IF; 
       
	END IF;
    
        INSERT INTO `itasks_deleted`
		(`task_id`, `site_id`, `task_code`, `task_title`, `assign_to_user`, 
		`milestone_id`, `contractor_id`, `task_description`, `task_remarks`,
		`task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`,
		`created_at`, `updated_at`, `from_task_template_id`, `from_task_template_detail_id`,
		`calculated_flag`, `deleted_by`)
		Select `task_id`, `site_id`, `task_code`, `task_title`, `assign_to_user`, 
		`milestone_id`, `contractor_id`, `task_description`, `task_remarks`,
		`task_progress`, `status_id`, `task_est_start_date`, `task_est_end_date`,
		`task_start_date`, `task_end_date`, `upload_attachment`, `created_by`,
		`created_at`, `updated_at`, `from_task_template_id`, `from_task_template_detail_id`,
		`calculated_flag`, delete_user_id_x from itasks where task_id = task_id_x;

		delete from itasks where task_id = task_id_x;
	
END IF;

Select "Completed";

END