CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_import_task_into_site`(
site_id_x bigint,
task_template_id_x bigint,
created_by_x bigint,
project_id_x bigint
)
BEGIN

	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_task_template_detail_id bigint;
    DECLARE v_task_template_code varchar(50);
    DECLARE v_task_template_title varchar(100);
	DECLARE v_task_description varchar(300);
    DECLARE v_status_id bigint;
    DECLARE v_milestone_template_detail_id bigint;
    DECLARE v_milestone_id bigint;
    DECLARE v_milestone_code varchar(50);

	DECLARE x_cursor CURSOR FOR
	SELECT task_template_detail_id,  task_template_title, task_description, status_id, milestone_template_detail_id, milestone_id, milestone_code
    FROM task_template_details
    INNER JOIN project_milestones
    ON project_milestones.from_milestone_template_detail_id = task_template_details.milestone_template_detail_id
    WHERE task_template_id = task_template_id_x and project_milestones.project_id = project_id_x;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;

    SET @site_start_date = (Select site_start_date from project_sites where site_id = site_id_x);
    SET @site_end_date = (Select site_end_date from project_sites where site_id = site_id_x);

	OPEN x_cursor;

	get_x: LOOP

	FETCH x_cursor INTO v_task_template_detail_id,  v_task_template_title, v_task_description, v_status_id, v_milestone_template_detail_id, v_milestone_id, v_milestone_code;

	IF v_finished = 1 THEN
	LEAVE get_x;
	END IF;

    SET @check_available = 0;
    SET @contractor_id = null;

    SET @check_available = (Select Count(task_id) From itasks where from_task_template_detail_id = v_task_template_detail_id and site_id = site_id_x);

    IF ( @check_available = 0 ) THEN

		SET @contractor_id = (Select contractor_id from project_site_contractors where site_id = site_id_x order by created_at DESC limit 1);

		INSERT INTO itasks
        (site_id, task_code, task_title, milestone_id, task_description, status_id, created_by, from_task_template_id, from_task_template_detail_id, contractor_id, assign_to_user,
        task_est_start_date, task_est_end_date)
        VALUES
        (site_id_x, v_task_template_code, v_task_template_title, v_milestone_id, v_task_description, v_status_id, created_by_x, task_template_id_x, v_task_template_detail_id, @contractor_id, created_by_x,
        @site_start_date, @site_end_date);

    END IF;

    -- Select v_task_template_detail_id, v_task_template_code, v_task_template_title, v_task_description, v_status_id, v_milestone_template_detail_id, v_milestone_id, v_milestone_code;

	END LOOP get_x;

	CLOSE x_cursor;

END
