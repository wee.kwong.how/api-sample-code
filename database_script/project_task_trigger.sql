DROP TRIGGER IF EXISTS project_itasks_insert;
DELIMITER $$

CREATE TRIGGER project_itasks_insert
    AFTER INSERT
    ON project_itasks FOR EACH ROW

BEGIN

    SET @project_id = NEW.project_id;

    SET @project_level_total_task = (Select project_level_total_task from projects where project_id = @project_id);

    SET @project_level_total_task = @project_level_total_task + 1;

 	Update projects
 	SET
 	project_level_total_task = @project_level_total_task
 	Where project_id = @project_id;

 	IF ( NEW.status_id = '3' ) THEN

 		SET @project_level_completed_task = (Select project_level_completed_task from projects where project_id = @project_id);

 		SET @project_level_completed_task = @project_level_completed_task + 1;

 		Update projects
 		SET
 		project_level_completed_task = @project_level_completed_task
 		Where project_id = @project_id;

     END IF;

  END
$$ DELIMITER ;

DROP TRIGGER IF EXISTS project_itasks_update;
DELIMITER $$

CREATE TRIGGER project_itasks_update
    AFTER UPDATE
    ON project_itasks FOR EACH ROW

BEGIN

 	IF ( NEW.status_id = '3' ) THEN

 		SET @project_level_completed_task = (Select project_level_completed_task from projects where project_id = @project_id);

 		SET @project_level_completed_task = @project_level_completed_task + 1;

 		Update projects
 		SET
 		project_level_completed_task = @project_level_completed_task
 		Where project_id = @project_id;

     END IF;

  END
$$ DELIMITER ;

DROP TRIGGER IF EXISTS project_itasks_delete;
DELIMITER $$

CREATE TRIGGER project_itasks_delete
    AFTER DELETE
    ON project_itasks FOR EACH ROW

BEGIN

    SET @project_id = OLD.project_id;

    SET @project_level_total_task = (Select project_level_total_task from projects where project_id = @project_id);

    SET @project_level_total_task = @project_level_total_task - 1;

	SET @project_level_completed_task = (Select project_level_completed_task from projects where project_id = @project_id);

	SET @project_level_completed_task = @project_level_completed_task - 1;

	Update projects
	SET
	project_level_total_task = @project_level_total_task,
    project_level_completed_task = @project_level_completed_task
	Where project_id = @project_id;

  END
$$ DELIMITER ;

