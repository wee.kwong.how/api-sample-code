CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_collect_daily_assign`(
assign_date_x date
)
BEGIN

	select da.daily_assign_id, da.assign_date, da.assign_to_user_id, u.name, u.email,
	da.assign_module_code, p.project_name, g.group_name, s.site_name,
	da.assign_to_title, CONCAT(da.assign_link,'/',da.assign_to_id) as assign_link
	from daily_assigns as da inner join users as u
	on u.id = da.assign_to_user_id 	left join projects as p
	on p.project_id = da.project_id left join project_groups as g
	on g.group_id = da.group_id left join project_sites as s
	on s.site_id = da.site_id where date_format(da.assign_date, '%Y-%m-%e')  = assign_date_x and completed_flag = 0 order by da.assign_to_id ASC;

END
