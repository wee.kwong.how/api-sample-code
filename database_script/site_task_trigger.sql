BEGIN

	SET @group_id = (Select group_id from project_sites where site_id = NEW.site_id);
   SET @project_id = (Select project_id from project_groups where group_id = @group_id);

   SET @site_total_task = (Select site_total_task from project_sites where site_id = NEW.site_id);
   SET @group_total_task = (Select group_total_task from project_groups where group_id = @group_id);
   SET @project_total_task = (Select project_total_task from projects where project_id = @project_id);

	SET @site_total_task = @site_total_task + 1;
	SET @group_total_task = @group_total_task + 1;
   SET @project_total_task = @project_total_task + 1;

	Update project_sites
	SET
	site_total_task = @site_total_task
	Where site_id = NEW.site_id;

	Update project_groups
	SET
	group_total_task = @group_total_task
	Where group_id = @group_id;

	Update projects
	SET
	project_total_task = @project_total_task
	Where project_id = @project_id;

	SET @milestone_task_total = (Select milestone_task_total from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);

   SET @milestone_task_total = @milestone_task_total + 1;

   Update project_milestones
   SET
   milestone_task_total = @milestone_task_total
   Where project_id = @project_id and milestone_id = NEW.milestone_id;

	IF ( NEW.status_id = '3' ) THEN

		SET @site_completed_task = (Select site_completed_task from project_sites where site_id = NEW.site_id);
		SET @group_completed_task = (Select group_completed_task from project_groups where group_id = @group_id);
		SET @project_completed_task = (Select project_completed_task from projects where project_id = @project_id);

		SET @site_completed_task = @site_completed_task + 1;
		SET @group_completed_task = @group_completed_task + 1;
		SET @project_completed_task = @project_completed_task + 1;

		Update project_sites
		SET
		site_completed_task = @site_completed_task
		Where site_id = NEW.site_id;

		Update project_groups
		SET
		group_completed_task = @group_completed_task
		Where group_id = @group_id;

		Update projects
		SET
		project_completed_task = @project_completed_task
		Where project_id = @project_id;

		SET @milestone_task_completed = (Select milestone_task_completed from project_milestones where project_id = @project_id and milestone_id = NEW.milestone_id);

	   SET @milestone_task_completed = @milestone_task_completed + 1;

	   Update project_milestones
	   SET
	   milestone_task_completed = @milestone_task_completed
	   Where project_id = @project_id and milestone_id = NEW.milestone_id;

   END IF;

   ## Update Project S-Curve EST
   SET @project_scurve_id = NULL;

   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task
	FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_est_end_date) AND `year` = YEAR(NEW.task_est_end_date);

   IF (@project_scurve_id IS NOT NULL) THEN

   	SET @expected_total_task = @expected_total_task + 1;

   	UPDATE project_scurves
   	SET
   	expected_total_task = @expected_total_task
   	WHERE project_scurve_id = @project_scurve_id;

   ELSE

   	INSERT INTO project_scurves
   	(project_id, `year`, `month`, expected_total_task )
   	VALUES
   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);

   END IF;

   ## Update Project S-Curve Actual
   SET @project_scurve_id = NULL;

   IF ( NEW.task_end_date IS NOT NULL ) THEN
	   SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task
		FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_end_date) AND `year` = YEAR(NEW.task_end_date);

	   IF (@project_scurve_id IS NOT NULL) THEN

	   	SET @actual_total_task = @actual_total_task + 1;

	   	UPDATE project_scurves
	   	SET
	   	actual_total_task = @actual_total_task
	   	WHERE project_scurve_id = @project_scurve_id;

	   ELSE

	   	INSERT INTO project_scurves
	   	(project_id, `year`, `month`, actual_total_task )
	   	VALUES
	   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);

	   END IF;
	END IF;

 END

 BEGIN

	IF ( NEW.status_id = '3' ) THEN

		SET @group_id = (Select group_id from project_sites where site_id = NEW.site_id);
		SET @project_id = (Select project_id from project_groups where group_id = @group_id);

		SET @site_completed_task = (Select site_completed_task from project_sites where site_id = NEW.site_id);
		SET @group_completed_task = (Select group_completed_task from project_groups where group_id = @group_id);
		SET @project_completed_task = (Select project_completed_task from projects where project_id = @project_id);

		SET @site_completed_task = @site_completed_task + 1;
		SET @group_completed_task = @group_completed_task + 1;
		SET @project_completed_task = @project_completed_task + 1;

		Update project_sites
		SET
		site_completed_task = @site_completed_task
		Where site_id = NEW.site_id;

		Update project_groups
		SET
		group_completed_task = @group_completed_task
		Where group_id = @group_id;

		Update projects
		SET
		project_completed_task = @project_completed_task
		Where project_id = @project_id;

		SET @milestone_task_completed = (Select milestone_task_completed from project_milestones where project_id = @project_id and milestone_id = OLD.milestone_id);

	   SET @milestone_task_completed = @milestone_task_completed + 1;

	   Update project_milestones
	   SET
	   milestone_task_completed = @milestone_task_completed
	   Where project_id = @project_id and milestone_id = OLD.milestone_id;

    END IF;

   ## Update Project S-Curve EST
   IF (OLD.task_est_end_date <> NEW.task_est_end_date) THEN

	   #Remove OlD One
	   SET @project_scurve_id = NULL;

	   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task
		FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(OLD.task_est_end_date) AND `year` = YEAR(OLD.task_est_end_date);

	   IF (@project_scurve_id IS NOT NULL) THEN

	   	SET @expected_total_task = @expected_total_task - 1;

	   	UPDATE project_scurves
	   	SET
	   	expected_total_task = @expected_total_task
	   	WHERE project_scurve_id = @project_scurve_id;

	   END IF;

	   #Add New One
	   SET @project_scurve_id = NULL;

	   SELECT project_scurve_id, expected_total_task INTO @project_scurve_id, @expected_total_task
		FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_est_end_date) AND `year` = YEAR(NEW.task_est_end_date);

	   IF (@project_scurve_id IS NOT NULL) THEN

	   	SET @expected_total_task = @expected_total_task + 1;

	   	UPDATE project_scurves
	   	SET
	   	expected_total_task = @expected_total_task
	   	WHERE project_scurve_id = @project_scurve_id;

	   ELSE

	   	INSERT INTO project_scurves
	   	(project_id, `year`, `month`, expected_total_task )
	   	VALUES
	   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);

	   END IF;

   END IF;

   ## Update Project S-Curve Actual

   IF ((OLD.task_end_date <> NEW.task_end_date) and NEW.task_end_date is not NULL )THEN

	   # Remove OLD one
	   SET @project_scurve_id = NULL;

		SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task
		FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(OLD.task_end_date) AND `year` = YEAR(OLD.task_end_date);

	   IF (@project_scurve_id IS NOT NULL) THEN

	   	SET @actual_total_task = @actual_total_task - 1;

	   	UPDATE project_scurves
	   	SET
	   	actual_total_task = @actual_total_task
	   	WHERE project_scurve_id = @project_scurve_id;

	   END IF;

	   #Add New One
	   SET @project_scurve_id = NULL;

	   IF ( NEW.task_end_date IS NOT NULL ) THEN
			SELECT project_scurve_id, actual_total_task INTO @project_scurve_id, @actual_total_task
			FROM project_scurves WHERE project_id = @project_id AND `month` = MONTH(NEW.task_end_date) AND `year` = YEAR(NEW.task_end_date);

		   IF (@project_scurve_id IS NOT NULL) THEN

		   	SET @actual_total_task = @actual_total_task + 1;

		   	UPDATE project_scurves
		   	SET
		   	actual_total_task = @actual_total_task
		   	WHERE project_scurve_id = @project_scurve_id;

		   ELSE

		   	INSERT INTO project_scurves
		   	(project_id, `year`, `month`, actual_total_task )
		   	VALUES
		   	(@project_id, YEAR(NEW.task_est_end_date), MONTH(NEW.task_est_end_date), 1);

		   END IF;
	   END IF;

   END IF;

END

BEGIN

	SET @group_id = (Select group_id from project_sites where site_id = OLD.site_id);
   SET @project_id = (Select project_id from project_groups where group_id = @group_id);

   SET @site_total_task = (Select site_total_task from project_sites where site_id = OLD.site_id);
   SET @group_total_task = (Select group_total_task from project_groups where group_id = @group_id);
   SET @project_total_task = (Select project_total_task from projects where project_id = @project_id);

	SET @site_total_task = @site_total_task - 1;
	SET @group_total_task = @group_total_task - 1;
   SET @project_total_task = @project_total_task - 1;

	Update project_sites
	SET
	site_total_task = @site_total_task
	Where site_id = OLD.site_id;

	Update project_groups
	SET
	group_total_task = @group_total_task
	Where group_id = @group_id;

	Update projects
	SET
	project_total_task = @project_total_task
	Where project_id = @project_id;

	SET @milestone_task_total = (Select milestone_task_total from project_milestones where project_id = @project_id and milestone_id = OLD.milestone_id);

   SET @milestone_task_total = @milestone_task_total - 1;

   Update project_milestones
   SET
   milestone_task_total = @milestone_task_total
   Where project_id = @project_id and milestone_id = OLD.milestone_id;

 END
