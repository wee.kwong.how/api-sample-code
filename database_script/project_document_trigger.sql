DROP TRIGGER IF EXISTS project_document_insert;
DELIMITER $$

CREATE TRIGGER project_document_insert
    AFTER INSERT
    ON project_documents FOR EACH ROW

BEGIN

    SET @project_id = NEW.project_id;

    SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);

    IF NEW.document_category_id = 1 THEN

		IF NEW.document_type_id = 1 THEN

			SET @project_level_total_document = @project_level_total_document + 1;

		END IF;

        Update projects
		SET
		project_level_total_document = @project_level_total_document
		Where project_id = @project_id;

        IF ( NEW.status_id = '3' ) THEN

			SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);

			SET @project_level_completed_document = @project_level_completed_document + 1;

			Update projects
			SET
			project_level_completed_document = @project_level_completed_document
			Where project_id = @project_id;

		 END IF;

     END IF;

  END
$$ DELIMITER ;

DROP TRIGGER IF EXISTS project_document_update;
DELIMITER $$

CREATE TRIGGER project_document_update
    AFTER UPDATE
    ON project_documents FOR EACH ROW

BEGIN

	IF NEW.document_category_id = 1 THEN

		IF NEW.document_type_id = 1 THEN

			IF ( NEW.status_id = '3' ) THEN

				SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);

				SET @project_level_completed_document = @project_level_completed_document + 1;

				Update projects
				SET
				project_level_completed_document = @project_level_completed_document
				Where project_id = @project_id;

			 END IF;

		 END IF;

     END IF;

  END
$$ DELIMITER ;

DROP TRIGGER IF EXISTS project_document_delete;
DELIMITER $$

CREATE TRIGGER project_document_delete
    AFTER DELETE
    ON project_documents FOR EACH ROW

BEGIN

    SET @project_id = OLD.project_id;

    IF OLD.document_category_id = 1 THEN

		IF OLD.document_type_id = 1 THEN

			SET @project_level_total_document = (Select project_level_total_document from projects where project_id = @project_id);

			SET @project_level_total_document = @project_level_total_document - 1;

			SET @project_level_completed_document = (Select project_level_completed_document from projects where project_id = @project_id);

			SET @project_level_completed_document = @project_level_completed_document - 1;

			Update projects
			SET
			project_level_total_document = @project_level_total_document,
			project_level_completed_document = @project_level_completed_document
			Where project_id = @project_id;

		END IF;

    END IF;

  END
$$ DELIMITER ;

