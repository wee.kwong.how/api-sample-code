<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    public function handle($request, Closure $next)
    {

       $domains = ['http://localhost:8080', 'http://localhost:9090', 'http://128.199.90.140:8080'];
       if (isset($request->server()['HTTP_ORIGIN'])) {
           $origin = $request->server()['HTTP_ORIGIN'];
           if (in_array($origin, $domains)) {
               //header('Access-Control-Allow-Origin: ' );
               header('Access-Control-Allow-Origin: ' . $origin);
               header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization, X-Requested-With, Accept');
               header('Access-Control-Allow-Methods: *');
           }
       }

        return $next($request);
    }
}
