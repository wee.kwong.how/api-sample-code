<?php

namespace App\Http\Controllers\SaaS\Task\Task;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Unit;
use App\Models\SaaS\Floor;
use App\Models\SaaS\Department;
use App\Models\SaaS\Building;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Project;
use App\Models\SaaS\Company;
use App\Models\SaaS\Task;
use Auth;
use App\Http\Resources\SaaS\TaskResource;
use Illuminate\Validation\Rule;
use DB;

class TaskController extends APIController
{
    protected $moduleCode = "TASK";
    protected $primaryKey = 'task_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0   => 'belongs_to_account',
            1   => 'depend_on_task',
            2   => 'company_id',
            3   => 'project_id',
            4   => 'cluster_id',
            5   => 'building_id',
            6   => 'department_id',
            7   => 'floor_id',
            8   => 'unit_id',
            9   => 'assign_by',
            10  => 'assign_to',
            11  => 'verified_by',
            12  => 'approved_by',
            13  => 'final_approved_by',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->has($this->uniqueCombo[1]) ? request()->input($this->uniqueCombo[1]) : NULL,
            $this->uniqueCombo[2] => request()->has($this->uniqueCombo[2]) ? request()->input($this->uniqueCombo[2]) : NULL,
            $this->uniqueCombo[3] => request()->has($this->uniqueCombo[3]) ? request()->input($this->uniqueCombo[3]) : NULL,
            $this->uniqueCombo[4] => request()->has($this->uniqueCombo[4]) ? request()->input($this->uniqueCombo[4]) : NULL,
            $this->uniqueCombo[5] => request()->has($this->uniqueCombo[5]) ? request()->input($this->uniqueCombo[5]) : NULL,
            $this->uniqueCombo[6] => request()->has($this->uniqueCombo[6]) ? request()->input($this->uniqueCombo[6]) : NULL,
            $this->uniqueCombo[7] => request()->has($this->uniqueCombo[7]) ? request()->input($this->uniqueCombo[7]) : NULL,
            $this->uniqueCombo[8] => request()->has($this->uniqueCombo[8]) ? request()->input($this->uniqueCombo[8]) : NULL,
            $this->uniqueCombo[9] => request()->has($this->uniqueCombo[9]) ? request()->input($this->uniqueCombo[9]) : NULL,
            $this->uniqueCombo[10] => request()->has($this->uniqueCombo[10]) ? request()->input($this->uniqueCombo[10]) : NULL,
            $this->uniqueCombo[11] => request()->has($this->uniqueCombo[11]) ? request()->input($this->uniqueCombo[11]) : NULL,
            $this->uniqueCombo[12] => request()->has($this->uniqueCombo[12]) ? request()->input($this->uniqueCombo[12]) : NULL,
            $this->uniqueCombo[13] => request()->has($this->uniqueCombo[13]) ? request()->input($this->uniqueCombo[13]) : NULL,
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return TaskResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Task::orderBy('created_at', 'DESC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Task::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $validationCheckTask = "YES";
        $validationCheckCompany = "YES";
        $validationCheckProject = "YES";
        $validationCheckCluster = "YES";
        $validationCheckBuilding = "YES";
        $validationCheckDepartment = "YES";
        $validationCheckFloor = "YES";
        $validationCheckUnit = "YES";
        $validationCheckFloorAndDepartment = "YES";
        $validationCheckAssignByAndAssignTo = "NO";

        $validationCheckAssignBy = "YES";
        $validationCheckAssignTo = "YES";

        $validationVerifiedBy = "YES";
        $validationApprovedBy = "YES";
        $validationFinalApprovedBy = "YES";

        if ((($uniqueColumnRequest[$this->uniqueCombo[7]] != NULL AND $uniqueColumnRequest[$this->uniqueCombo[8]] != NULL) AND $uniqueColumnRequest[$this->uniqueCombo[6]] != NULL ) 
           OR ($uniqueColumnRequest[$this->uniqueCombo[6]] != NULL AND $uniqueColumnRequest[$this->uniqueCombo[7]] != NULL ))
        {
            $validationCheckFloorAndDepartment = "NO";
        }

        if (( $uniqueColumnRequest[$this->uniqueCombo[9]] != NULL AND $uniqueColumnRequest[$this->uniqueCombo[10]] != NULL ) OR 
           ( $uniqueColumnRequest[$this->uniqueCombo[9]] == NULL AND $uniqueColumnRequest[$this->uniqueCombo[10]] == NULL ))
        {
            $validationCheckAssignByAndAssignTo = "YES";
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[1]] != NULL )
        {
            $keyCount = Task::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('task_id', $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->count();

            $validationCheckTask = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[2]] != NULL )
        {
            $keyCount = Company::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->count();

            $validationCheckCompany = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[3]] != NULL )
        {
            $keyCount = Project::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->where($this->uniqueCombo[3], $uniqueColumnRequest[$this->uniqueCombo[3]])
            ->count();

            $validationCheckCluster = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[4]] != NULL )
        {
            $keyCount = Cluster::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where($this->uniqueCombo[3], $uniqueColumnRequest[$this->uniqueCombo[3]])
            ->where($this->uniqueCombo[4], $uniqueColumnRequest[$this->uniqueCombo[4]])
            ->count();

            $validationCheckProject = $keyCount >= 1 ? "YES" : "NO" ;
        }
        
        if ( $uniqueColumnRequest[$this->uniqueCombo[5]] != NULL )
        {
            $keyCount = Building::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where($this->uniqueCombo[4], $uniqueColumnRequest[$this->uniqueCombo[4]])
            ->where($this->uniqueCombo[5], $uniqueColumnRequest[$this->uniqueCombo[5]])
            ->count();

            $validationCheckBuilding = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[6]] != NULL )
        {
            $keyCount = Department::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where($this->uniqueCombo[5], $uniqueColumnRequest[$this->uniqueCombo[5]])
            ->where($this->uniqueCombo[6], $uniqueColumnRequest[$this->uniqueCombo[6]])
            ->count();

            $validationCheckDepartment = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[7]] != NULL )
        {
            $keyCount = Floor::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where($this->uniqueCombo[5], $uniqueColumnRequest[$this->uniqueCombo[5]])
            ->where($this->uniqueCombo[7], $uniqueColumnRequest[$this->uniqueCombo[7]])
            ->count();

            $validationCheckFloor = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[8]] != NULL )
        {
            $keyCount = Unit::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->where($this->uniqueCombo[3], $uniqueColumnRequest[$this->uniqueCombo[3]])
            ->where($this->uniqueCombo[4], $uniqueColumnRequest[$this->uniqueCombo[4]])
            ->where($this->uniqueCombo[5], $uniqueColumnRequest[$this->uniqueCombo[5]])
            ->where($this->uniqueCombo[7], $uniqueColumnRequest[$this->uniqueCombo[7]])
            ->where($this->uniqueCombo[8], $uniqueColumnRequest[$this->uniqueCombo[8]])
            ->count();

            $validationCheckUnit = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[9]] != NULL AND $uniqueColumnRequest[$this->uniqueCombo[10]] != NULL )
        {
            # Assign By
            $keyCount = User::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('id', $uniqueColumnRequest[$this->uniqueCombo[9]])
            ->count();
            # require to add the user with role where role_id in (Approval, Verifier, Preparer)
            $validationCheckAssignBy = $keyCount >= 1 ? "YES" : "NO" ;

            # Assign To
            $keyCount = User::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('id', $uniqueColumnRequest[$this->uniqueCombo[10]])
            ->count();
            # require to add the user with role where role_id in (Approval, Verifier, Preparer, Checker)
            $validationCheckAssignTo = $keyCount >= 1 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[11]] != NULL )
        {
            # Assign Verified By
            $keyCount = User::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('id', $uniqueColumnRequest[$this->uniqueCombo[11]])
            ->count();
            # require to add the user with role where role_id in (Approval,Verifier)
            $validationVerifiedBy = ( $keyCount >= 1 AND ( $uniqueColumnRequest[$this->uniqueCombo[9]] != NULL AND $uniqueColumnRequest[$this->uniqueCombo[10]] != NULL )) ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[12]] != NULL )
        {
            # Assign Approved By
            $keyCount = User::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('id', $uniqueColumnRequest[$this->uniqueCombo[12]])
            ->count();
            # require to add the user with role where role_id in (Approval)
            $validationApprovedBy = ( $keyCount >= 1 AND ( $validationVerifiedBy == "YES" ) AND ( $uniqueColumnRequest[$this->uniqueCombo[11]] != NULL )) ? "YES" : "NO" ;
        }
        
        if ( $uniqueColumnRequest[$this->uniqueCombo[13]] != NULL )
        {
            # Assign Final Approved By
            $keyCount = User::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('id', $uniqueColumnRequest[$this->uniqueCombo[13]])
            ->count();
            # require to add the user with role where role_id in (Approval)
            $validationFinalApprovedBy = ( $keyCount >= 1 AND ( $validationVerifiedBy == "YES" ) AND ( $uniqueColumnRequest[$this->uniqueCombo[11]] != NULL ) AND ( $uniqueColumnRequest[$this->uniqueCombo[12]] != NULL )) ? "YES" : "NO" ;
        }

        $validationArray = array
        ( 
            $validationCheckTask, $validationCheckCompany, $validationCheckProject, 
            $validationCheckCluster, $validationCheckBuilding, $validationCheckDepartment,
            $validationCheckFloor, $validationCheckUnit, $validationCheckFloorAndDepartment,
            $validationCheckAssignByAndAssignTo, $validationCheckAssignBy, $validationCheckAssignTo,
            $validationVerifiedBy, $validationApprovedBy, $validationFinalApprovedBy
        );

        $validationCheck = (array_search("NO",$validationArray,true)) === FALSE ? "YES" : "NO";
    
        return $validationCheck;
    }

    protected function additionalUpdateRequirementCheck($idOrCode, $uniqueColumnRequest)
    {
        $validationCheckAssignByAndTo = "YES";
        $validationVerifiedBy = "YES";
        $validationApprovedBy = "YES";
        $validationFinalApprovedBy = "YES";

        if ( $uniqueColumnRequest[$this->uniqueCombo[9]] == NULL AND $uniqueColumnRequest[$this->uniqueCombo[10]] == NULL )
        {
            # Assign By & Assign To
            $keyCount = Task::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('task_id', $idOrCode)
            ->orwhereNotNull($this->uniqueCombo[11])
            ->orwhereNotNull($this->uniqueCombo[12])
            ->orwhereNotNull($this->uniqueCombo[12])
            ->count();

            $validationCheckAssignByAndTo = $keyCount == 0 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[11]] == NULL )
        {
            # Verified By
            $keyCount = Task::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('task_id', $idOrCode)
            ->whereNotNull($this->uniqueCombo[9])
            ->whereNotNull($this->uniqueCombo[10])
            ->count();

            $validationVerifiedBy = $keyCount == 0 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[12]] == NULL )
        {
            # Approved By
            $keyCount = Task::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('task_id', $idOrCode)
            ->whereNotNull($this->uniqueCombo[9])
            ->whereNotNull($this->uniqueCombo[10])
            ->whereNotNull($this->uniqueCombo[11])
            ->count();

            $validationApprovedBy = $keyCount == 0 ? "YES" : "NO" ;
        }

        if ( $uniqueColumnRequest[$this->uniqueCombo[13]] == NULL )
        {
            # Final Approved By
            $keyCount = Task::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
            ->where('task_id', $idOrCode)
            ->whereNotNull($this->uniqueCombo[9])
            ->whereNotNull($this->uniqueCombo[10])
            ->whereNotNull($this->uniqueCombo[11])
            ->whereNotNull($this->uniqueCombo[12])
            ->count();

            $validationApprovedBy = $keyCount == 0 ? "YES" : "NO" ;
        }


        $validationArray = array
        ( 
            $validationVerifiedBy, $validationCheckAssignByAndTo,
            $validationApprovedBy, $validationFinalApprovedBy
        );
 
        $validationCheck = (array_search("NO",$validationArray,true)) === FALSE ? "YES" : "NO";

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return collect([  ]);
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            if ($this->additionalUpdateRequirementCheck($idOrCode, $uniqueColumnRequest) == "YES")
            {
                $collectionCount = collect([  ]);
            }
            else
            {
                $collectionCount = collect([ 0 ]);
            }
        }
        else
        {
            $collectionCount =  collect([ 0 ]);
        }

        if ( $idOrCode == $uniqueColumnRequest[$this->uniqueCombo[1]] )
        {
            $collectionCount =  collect([ 0 ]);
        }

        return $collectionCount;
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Task::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Task::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Task::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'depend_on_task'            => 'nullable|exists:itasks,task_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'task_category_id'	        => 'required|exists:itask_categories,task_category_id',
            'task_title'	            => 'required|min:5|max:200',
            'task_milestone_mode'	    => 'required|exists:itask_milestone_modes,task_milestone_mode',
            'task_milestone'            => 'nullable|numeric|between:0,100',
            'task_description'	        => 'nullable|min:5|max:1000',
            'task_priority_id'	        => 'required|exists:itask_priorities,task_priority_id',
            'task_status_id'	        => 'required|exists:itask_status,task_status_id',
            'task_est_start_date'       => 'required|date',
            'task_est_end_date'		    => 'required|date|after_or_equal:task_est_start_date',
            'task_start_date'           => 'nullable|date',
            'task_end_date'		        => 'nullable|date|after_or_equal:task_start_date',
            'task_remarks'	            => 'nullable|min:5|max:1000',
            'company_id'                => 'nullable|exists:companies,company_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'project_id'                => 'nullable|exists:projects,project_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'cluster_id'                => 'nullable|exists:clusters,cluster_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'building_id'               => 'nullable|exists:buildings,building_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'department_id'             => 'nullable|exists:departments,department_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'floor_id'                  => 'nullable|exists:floors,floor_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'unit_id'                   => 'nullable|exists:units,unit_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'assign_by'                 => 'nullable|exists:users,id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'assign_to'                 => 'nullable|exists:users,id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'verified_by'               => 'nullable|exists:users,id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'approved_by'               => 'nullable|exists:users,id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'final_approved_by'         => 'nullable|exists:users,id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'depend_on_task'            => 'nullable|exists:itasks,task_id',
            'task_category_id'	        => 'required|exists:itask_categories,task_category_id',
            'task_title'	            => 'required|min:5|max:200',
            'task_milestone_mode'	    => 'required|exists:itask_milestone_modes,task_milestone_mode',
            'task_milestone'            => 'nullable|numeric|between:0,100',
            'task_description'	        => 'nullable|min:5|max:1000',
            'task_priority_id'	        => 'required|exists:itask_priorities,task_priority_id',
            'task_status_id'	        => 'required|exists:itask_status,task_status_id',
            'task_est_start_date'       => 'required|date',
            'task_est_end_date'		    => 'required|date|after_or_equal:task_est_start_date',
            'task_start_date'           => 'nullable|date',
            'task_end_date'		        => 'nullable|date|after_or_equal:task_start_date',
            'task_remarks'	            => 'nullable|min:5|max:1000',
            'company_id'                => 'nullable|exists:companies,company_id',
            'project_id'                => 'nullable|exists:projects,project_id',
            'cluster_id'                => 'nullable|exists:clusters,cluster_id',
            'building_id'               => 'nullable|exists:buildings,building_id',
            'department_id'             => 'nullable|exists:departments,department_id',
            'floor_id'                  => 'nullable|exists:floors,floor_id',
            'unit_id'                   => 'nullable|exists:units,unit_id',
            'assign_by'                 => 'nullable|exists:users,id',
            'assign_to'                 => 'nullable|exists:users,id',
            'verified_by'               => 'nullable|exists:users,id',
            'approved_by'               => 'nullable|exists:users,id',
            'final_approved_by'         => 'nullable|exists:users,id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "depend_on_task"                => $request->depend_on_task,
            "task_category_id"              => $request->task_category_id,
            "task_title"                    => $request->task_title,
            "task_milestone_mode"           => $request->task_milestone_mode,
            "task_milestone"                => $request->task_milestone,
            "task_description"              => $request->task_description,
            "task_priority_id"              => $request->task_priority_id,
            "task_status_id"                => $request->task_status_id,

            "task_est_start_date"           => $request->task_est_start_date,
            "task_est_end_date"             => $request->task_est_end_date,
            "task_start_date"               => $request->task_start_date,
            "task_end_date"                 => $request->task_end_date,

            "task_remarks"                  => $request->task_remarks,

            "company_id"                    => $request->company_id,
            "project_id"                    => $request->project_id,
            "cluster_id"                    => $request->cluster_id,
            "building_id"                   => $request->building_id,
            "department_id"                 => $request->department_id,
            "floor_id"                      => $request->floor_id,
            "unit_id"                       => $request->unit_id,

            "assign_by"                     => $request->assign_by,
            "assign_to"                     => $request->assign_to,
            "verified_by"                   => $request->verified_by,
            "approved_by"                   => $request->approved_by,
            "final_approved_by"             => $request->final_approved_by,
        ];
    }

}

