<?php

namespace App\Http\Controllers\SaaS\Task\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
// use App\Models\SaaS\Task;
use App\Models\SaaS\TaskStatus;
use Auth;
use App\Http\Resources\SaaS\TaskStatusResource;
use Illuminate\Validation\Rule;

class TaskStatusController extends APIController
{
    protected $moduleCode = "TASK_STATUS";
    protected $primaryKey = 'task_status_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'belongs_to_account',
            1 => 'task_status_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return TaskStatusResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return TaskStatus::orderBy('created_at', 'DESC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return TaskStatus::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return TaskStatus::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return TaskStatus::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        // $keyCount = Task::where($this->primaryKey, $idOrCode)->count();

        // $validationCheck = $keyCount == 0 ? "YES" : "NO" ;

        // return $validationCheck;
    }

    protected function insertNewRecord($newData)
    {
        return TaskStatus::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        TaskStatus::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        TaskStatus::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_status_name'                  => 'required|min:1|max:100|unique:itask_status,task_status_name',
            'task_status_milestone_percentage'  => 'required|numeric|between:0,100',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_status_name'                  => 'required|min:1|max:100',
            'task_status_milestone_percentage'  => 'required|between:0,100',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "task_status_name"                  => $request->task_status_name,
            "task_status_milestone_percentage"  => $request->task_status_milestone_percentage,
            "active_status"                     => $request->active_status,
        ];
    }
}
