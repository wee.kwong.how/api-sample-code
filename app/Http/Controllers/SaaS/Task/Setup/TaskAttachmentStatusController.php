<?php

namespace App\Http\Controllers\SaaS\Task\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
// use App\Models\SaaS\Task;
use App\Models\SaaS\TaskAttachmentStatus;
use Auth;
use App\Http\Resources\SaaS\TaskAttachmentStatusResource;
use Illuminate\Validation\Rule;

class TaskAttachmentStatusController extends APIController
{
    protected $moduleCode = "TASK_ATTACHMENT_STATUS";
    protected $primaryKey = 'task_attach_status_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'belongs_to_account',
            1 => 'task_attach_status_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return TaskAttachmentStatusResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return TaskAttachmentStatus::orderBy('created_at', 'DESC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return TaskAttachmentStatus::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return TaskAttachmentStatus::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return TaskAttachmentStatus::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        // $keyCount = Task::where($this->primaryKey, $idOrCode)->count();

        // $validationCheck = $keyCount == 0 ? "YES" : "NO" ;

        // return $validationCheck;
        return $validatationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return TaskAttachmentStatus::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        TaskAttachmentStatus::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        TaskAttachmentStatus::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_attach_status_name'           => 'required|min:1|max:100|unique:itask_attachment_status,task_attach_status_name',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_attach_status_name'           => 'required|min:1|max:100',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "task_attach_status_name"           => $request->task_attach_status_name,
            "active_status"                     => $request->active_status,
        ];
    }
}
