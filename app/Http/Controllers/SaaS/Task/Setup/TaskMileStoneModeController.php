<?php

namespace App\Http\Controllers\SaaS\Task\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\TaskMileStoneMode;
use Auth;
use App\Http\Resources\SaaS\TaskMileStoneModeResource;
use Illuminate\Validation\Rule;

class TaskMileStoneModeController extends APIController
{
    protected $moduleCode = "TASK_MILESTONE_MODE";
    protected $primaryKey = 'task_milestone_mode';
    protected $uniqueColumnName = "task_milestone_mode";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest;

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest = request()->input($this->uniqueColumnName);
    }

    protected function makeResoucesCollection($data)
    {
        return TaskMileStoneModeResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return TaskMileStoneMode::orderBy($this->primaryKey, 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return TaskMileStoneMode::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return TaskMileStoneMode::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest)->get();
    }

    protected function insertNewRecord($newData)
    {
        TaskMileStoneMode::insert($newData);
        return $newData['task_milestone_mode'];
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        TaskMileStoneMode::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_milestone_mode'	    => 'required|min:3|max:100|unique:itask_milestone_modes,task_milestone_mode',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'task_milestone_mode'			=> 'required|min:3|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "task_milestone_mode"           => $request->task_milestone_mode,
        ];
    }
}
