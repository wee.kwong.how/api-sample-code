<?php

namespace App\Http\Controllers\SaaS\Customer;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\Customer;
use App\Models\SaaS\SubscriptionAccount;
use Auth;
use App\Http\Resources\SaaS\CustomerResource;
use App\Http\Resources\SaaS\SubscriptionAccountResource;

class SubscriptionAccountController extends APIController
{
    protected $moduleCode = "SUBSCRIPTION";
    protected $primaryKey = 'subscription_account_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'customer_id',
            1 => 'config_type_id',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            'customer_id' => request()->input($this->uniqueCombo[0]),
            'config_type_id' => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return SubscriptionAccountResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return SubscriptionAccount::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return SubscriptionAccount::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return SubscriptionAccount::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return SubscriptionAccount::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function insertNewRecord($newData)
    {
        return SubscriptionAccount::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        SubscriptionAccount::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'customer_id'	    => 'required|exists:customers,customer_id',
            'config_type_id'	=> 'required|exists:config_types,config_type_id',
            'start_date'        => 'required|date',
            'end_date'		    => 'required|date|after_or_equal:start_date',
            'active_status'		=> 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'customer_id'	    => 'required|exists:customers,customer_id',
            'config_type_id'	=> 'required|exists:config_types,config_type_id',
            'start_date'        => 'required|date',
            'end_date'		    => 'required|date|after_or_equal:start_date',
            'active_status'		=> 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "customer_id"               => $request->customer_id,
            "config_type_id"            => $request->config_type_id,
            "start_date"                => $request->start_date,
            "end_date"                  => $request->end_date,
            "active_status"             => $request->active_status,
        ];
    }

    public function showByCustomer($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'show' );

        if ( $validationCheck == "YES" )
        {
            $subscriptionAccount = SubscriptionAccount::where('customer_id', $idOrCode)->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($subscriptionAccount), $this->successReadMessageResponse);
        }

        return response($this->response, $this->status);
    }
}
