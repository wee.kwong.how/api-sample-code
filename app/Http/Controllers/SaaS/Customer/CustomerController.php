<?php

namespace App\Http\Controllers\SaaS\Customer;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\Customer;
use Auth;
use App\Http\Resources\SaaS\CustomerResource;

class CustomerController extends APIController
{
    protected $moduleCode = "CUSTOMER";
    protected $primaryKey = 'customer_id';
    protected $uniqueColumnName = "customer_email";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest;

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest = request()->input($this->uniqueColumnName);
    }

    protected function makeResoucesCollection($data)
    {
        return CustomerResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Customer::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Customer::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Customer::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest)->get();
    }

    protected function insertNewRecord($newData)
    {
        return Customer::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Customer::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'customer_name'			=> 'required|min:3|max:300',
            'contact_number'	    => 'required|min:3|max:16',
            'customer_email'		=> 'required|email|min:8|max:300|unique:customers,customer_email',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'customer_name'			=> 'required|min:3|max:300',
            'contact_number'	    => 'required|min:3|max:16',
            'customer_email'		=> 'required|email|min:8|max:300',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "module_code"           => $request->module_code,
            "module_name"           => $request->module_name,
            "module_information"    => $request->module_information,
        ];
    }
}
