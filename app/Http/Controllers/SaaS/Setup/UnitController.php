<?php

namespace App\Http\Controllers\SaaS\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Unit;
use App\Models\SaaS\Floor;
use Auth;
use App\Http\Resources\SaaS\UnitResource;
use Illuminate\Validation\Rule;

class UnitController extends APIController
{
    protected $moduleCode = "UNIT";
    protected $primaryKey = 'unit_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'belongs_to_account',
            1 => 'floor_id',
            2 => 'unit_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
            $this->uniqueCombo[2] => request()->input($this->uniqueCombo[2]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return UnitResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Unit::orderBy('created_at', 'DESC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Unit::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Floor::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return Unit::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return Unit::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Unit::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Unit::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Unit::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'floor_id'                  => 'required|exists:floors,floor_id',
            'unit_code'	                => 'required|min:1|max:50',
            'unit_name'	                => 'required|min:1|max:100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'floor_id'                  => 'required|exists:floors,floor_id',
            'unit_code'	                => 'required|min:1|max:50',
            'unit_name'	                => 'required|min:1|max:100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        $floor  = Floor::where("floor_id", $request->floor_id)->first();

        return
        [
            "company_id"            => $floor->_building->_cluster->_project->company_id,
            "project_id"            => $floor->_building->_cluster->project_id,
            "cluster_id"            => $floor->_building->cluster_id,
            "building_id"           => $floor->building_id,
            "floor_id"              => $request->floor_id,
            "unit_code"             => $request->unit_code,
            "unit_name"             => $request->unit_name,
            "active_status"         => $request->active_status,
        ];
    }
}
