<?php

namespace App\Http\Controllers\SaaS\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SaaS\MilestoneTemplate;
use App\Models\SaaS\MilestoneTemplateDetail;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SaaS\MilestoneTemplateDetailResource;
use Illuminate\Validation\Rule;

class MilestoneTemplateDetailController extends APIController
{
    protected $moduleCode = "MILESTONE_TEMPLATE_DETAIL";
    protected $primaryKey = 'milestone_template_detail_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'belongs_to_account',
            1 => 'milestone_template_id',
            2 => 'milestone_template_detail_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
            $this->uniqueCombo[2] => request()->input($this->uniqueCombo[2]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return MilestoneTemplateDetailResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return MilestoneTemplateDetail::orderBy('milestone_template_id', 'ASC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return MilestoneTemplateDetail::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = MilestoneTemplate::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return MilestoneTemplateDetail::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return MilestoneTemplateDetail::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return MilestoneTemplateDetail::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        MilestoneTemplateDetail::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        MilestoneTemplateDetail::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'milestone_template_id'             => 'required|exists:milestone_templates,milestone_template_id',
            'milestone_template_detail_code'	=> 'required|min:1|max:50',
            'milestone_template_info'	        => 'required|min:1|max:100',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'milestone_template_id'             => 'required|exists:milestone_templates,milestone_template_id',
            'milestone_template_detail_code'	=> 'required|min:1|max:50',
            'milestone_template_info'	        => 'required|min:1|max:100',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "milestone_template_id"           => $request->milestone_template_id,
            "milestone_template_detail_code"  => $request->milestone_template_detail_code,
            "milestone_template_info"         => $request->milestone_template_info,
            "active_status"                   => $request->active_status,
        ];
    }

    public function showByTemplate($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'show' );

        if ( $validationCheck == "YES" )
        {
            $milestoneTemplateDetail = MilestoneTemplateDetail::where('milestone_template_id', $idOrCode)->get();

            if ( $milestoneTemplateDetail->count() >= 1 )
            {
                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($milestoneTemplateDetail), $this->successReadMessageResponse, $milestoneTemplateDetail[0]->_milestone_template->milestone_template_code );
            }
            else
            {
                $milestoneTemplate = MilestoneTemplate::where('milestone_template_id', $idOrCode)->first();

                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($milestoneTemplateDetail), $this->successReadMessageResponse, $milestoneTemplate->milestone_template_code );
            }

        }

        return response($this->response, $this->status);
    }

}
