<?php

namespace App\Http\Controllers\SaaS\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SaaS\MilestoneTemplate;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SaaS\MilestoneTemplateResource;
use Illuminate\Validation\Rule;
use DB;

class MilestoneTemplateController extends APIController
{
    protected $moduleCode = "MILESTONE_TEMPLATE";
    protected $primaryKey = 'milestone_template_id';
    protected $uniqueColumnName = "milestone_template_code";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'belongs_to_account',
            1 => 'milestone_template_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return MilestoneTemplateResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return MilestoneTemplate::orderBy('created_at', 'DESC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return MilestoneTemplate::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    // protected function checkKeyBelongToAccount($uniqueColumnRequest)
    // {
    //     $keyCount = MilestoneTemplate::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
    //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
    //     ->count();

    //     $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

    //     return $validationCheck;
    // }

    // protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    // {
    //      DB::enableQueryLog();

    //     return MilestoneTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
    //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
    //     ->get();
    //      dd(DB::getQueryLog());
    // }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return MilestoneTemplate::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return MilestoneTemplate::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        MilestoneTemplate::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        MilestoneTemplate::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'milestone_template_code'	=> 'required|min:1|max:50|unique:milestone_templates,milestone_template_code',
            'milestone_template_name'	=> 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'milestone_template_id'     => 'required|exists:milestone_templates,milestone_template_id',
            'milestone_template_code'	=> 'required|min:1|max:50',
            'milestone_template_name'	=> 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "milestone_template_id"           => $request->milestone_template_id,
            "milestone_template_code"         => $request->milestone_template_code,
            "milestone_template_name"         => $request->milestone_template_name,
        ];
    }

}
