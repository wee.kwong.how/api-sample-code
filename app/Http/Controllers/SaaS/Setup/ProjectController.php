<?php

namespace App\Http\Controllers\SaaS\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Company;
use App\Models\SaaS\Developer;
use App\Models\SaaS\Project;
use Auth;
use App\Http\Resources\SaaS\ProjectResource;
use Illuminate\Validation\Rule;

class ProjectController extends APIController
{
    protected $moduleCode = "PROJECT";
    protected $primaryKey = 'project_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'belongs_to_account',
            1 => 'company_id',
            2 => 'project_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
            $this->uniqueCombo[2] => request()->input($this->uniqueCombo[2]),
            'developer_id'        => $request->developer_id,
            'project_manager'     => $request->project_manager,
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Project::orderBy('created_at', 'DESC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Project::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Company::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        $keyCount = Developer::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
        ->where('developer_id', $uniqueColumnRequest['developer_id'])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        $keyCount = User::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
        ->where('id', $uniqueColumnRequest['project_manager'])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return Project::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return Project::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Project::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Project::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Project::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'company_id'                => 'required|exists:companies,company_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'project_code'	            => 'required|min:1|max:50',
            'project_name'	            => 'required|min:1|max:100',
            'project_manager'	        => 'required|exists:users,id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'developer_id'              => 'required|exists:developers,developer_id,belongs_to_account,'.$this->loginUserInfo['belongs_to_account'],
            'project_start_date'        => 'required|date',
            'project_end_date'		    => 'required|date|after_or_equal:project_start_date',
            'project_status'            => 'required|exists:status,status_code',
            'project_progress'          => 'required|numeric|between:0,100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'company_id'                => 'required|exists:companies,company_id',
            'project_code'	            => 'required|min:1|max:50',
            'project_name'	            => 'required|min:1|max:100',
            'project_manager'	        => 'required|exists:users,id',
            'developer_id'              => 'required|exists:developers,developer_id',
            'project_start_date'        => 'required|date',
            'project_end_date'		    => 'required|date|after_or_equal:project_start_date',
            'project_status'            => 'required|exists:status,status_code',
            'project_progress'          => 'required|numeric|between:0,100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "company_id"            => $request->company_id,
            "project_code"          => $request->project_code,
            "project_name"          => $request->project_name,
            "project_manager"       => $request->project_manager,
            "developer_id"          => $request->developer_id,
            "project_start_date"    => $request->project_start_date,
            "project_end_date"      => $request->project_end_date,
            "project_status"        => $request->project_status,
            "project_progress"      => $request->project_progress,
            "active_status"         => $request->active_status,
        ];
    }
}
