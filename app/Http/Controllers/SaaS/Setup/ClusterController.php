<?php

namespace App\Http\Controllers\SaaS\Setup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Project;
use App\Models\SaaS\Cluster;
use Auth;
use App\Http\Resources\SaaS\ClusterResource;
use Illuminate\Validation\Rule;

class ClusterController extends APIController
{
    protected $moduleCode = "CLUSTER";
    protected $primaryKey = 'cluster_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'belongs_to_account',
            1 => 'project_id',
            2 => 'cluster_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => $this->loginUserInfo['belongs_to_account'],
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
            $this->uniqueCombo[2] => request()->input($this->uniqueCombo[2]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ClusterResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Cluster::orderBy('created_at', 'DESC')
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Cluster::where($this->primaryKey, $idOrCode)
        ->where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Project::where($this->uniqueCombo[0], $this->loginUserInfo['belongs_to_account'])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return Cluster::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return Cluster::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Cluster::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Cluster::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Cluster::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'project_id'                => 'required|exists:projects,project_id',
            'cluster_code'	            => 'required|min:1|max:50',
            'cluster_name'	            => 'required|min:1|max:100',
            'cluster_start_date'        => 'required|date',
            'cluster_end_date'		    => 'required|date|after_or_equal:cluster_start_date',
            'cluster_status'            => 'required|exists:status,status_code',
            'cluster_progress'          => 'required|numeric|between:0,100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_id'                => 'required|exists:projects,project_id',
            'cluster_code'	            => 'required|min:1|max:50',
            'cluster_name'	            => 'required|min:1|max:100',
            'cluster_start_date'        => 'required|date',
            'cluster_end_date'		    => 'required|date|after_or_equal:cluster_start_date',
            'cluster_status'            => 'required|exists:status,status_code',
            'cluster_progress'          => 'required|numeric|between:0,100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "belongs_to_account"    => $this->loginUserInfo['belongs_to_account'],
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_id"            => $request->project_id,
            "cluster_code"          => $request->cluster_code,
            "cluster_name"          => $request->cluster_name,
            "cluster_start_date"    => $request->cluster_start_date,
            "cluster_end_date"      => $request->cluster_end_date,
            "cluster_status"        => $request->cluster_status,
            "cluster_progress"      => $request->cluster_progress,
            "active_status"         => $request->active_status,
        ];
    }
}
