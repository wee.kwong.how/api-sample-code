<?php

namespace App\Http\Controllers\SaaS\Lookup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\ConfigType;
use Auth;
use App\Http\Resources\SaaS\ConfigTypeResource;
use Illuminate\Validation\Rule;

class ConfigTypeController extends APIController
{
    protected $moduleCode = "CONFIGTYPE";
    protected $primaryKey = 'config_type_id';
    protected $uniqueColumnName = "config_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest;

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest = request()->input($this->uniqueColumnName);
    }

    protected function makeResoucesCollection($data)
    {
        return ConfigTypeResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ConfigType::orderBy($this->uniqueColumnName, 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ConfigType::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ConfigType::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest)->get();
    }

    protected function insertNewRecord($newData)
    {
        return ConfigType::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        ConfigType::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'config_name'			=> 'required|min:4|max:100|unique:config_types,config_name',
            'config_information'	=> 'required|min:1|max:1000',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'config_name'			=> 'required|min:4|max:100',
            'config_information'	=> 'required|min:1|max:1000',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "config_name"           => $request->config_name,
            "config_information"    => $request->config_information,
        ];
    }
}
