<?php

namespace App\Http\Controllers\SaaS\Lookup;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\Status;
use Auth;
use App\Http\Resources\SaaS\StatusResource;
use Illuminate\Validation\Rule;

class StatusController extends APIController
{
    protected $moduleCode = "STATUS";
    protected $primaryKey = 'status_code';
    protected $uniqueColumnName = "status_code";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest;

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest = request()->input($this->uniqueColumnName);
    }

    protected function makeResoucesCollection($data)
    {
        return StatusResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Status::orderBy($this->primaryKey, 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Status::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Status::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest)->get();
    }

    protected function insertNewRecord($newData)
    {
        Status::insert($newData);
        return $newData['status_code'];
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Status::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'status_code'			=> 'required|min:3|max:50|unique:status,status_code',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'status_code'			=> 'required|min:3|max:50',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "status_code"           => $request->status_code,
        ];
    }
}
