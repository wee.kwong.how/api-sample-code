<?php

namespace App\Http\Controllers\SaaS\UserAccess;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\Module;
use Auth;
use App\Http\Resources\SaaS\ModuleResource;
use Illuminate\Validation\Rule;

class ModuleController extends APIController
{
    protected $moduleCode = "MODULE";
    protected $primaryKey = 'module_code';
    protected $uniqueColumnName = "module_code";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest;

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest = request()->input($this->uniqueColumnName);
    }

    protected function makeResoucesCollection($data)
    {
        return ModuleResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Module::orderBy($this->primaryKey, 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Module::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Module::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest)->get();
    }

    protected function insertNewRecord($newData)
    {
        Module::insert($newData);
        return $newData['module_code'];
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Module::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'module_code'			=> 'required|min:3|max:50|unique:modules,module_code',
            'module_name'			=> 'required|min:3|max:100',
            'module_information'	=> 'nullable|min:3|max:1000',
        ];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'module_code'			=> 'required|min:3|max:50',
            'module_name'			=> 'required|min:3|max:100',
            'module_information'	=> 'nullable|min:3|max:1000',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "module_code"           => $request->module_code,
            "module_name"           => $request->module_name,
            "module_information"    => $request->module_information,
        ];
    }
}
