<?php

namespace App\Http\Controllers\SaaS\UserAccess;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\Role;
use App\Models\SaaS\RoleAccess;
use Auth;
use App\Http\Resources\SaaS\RoleAccessResource;

class RoleAccessControllerx extends APIController
{
    protected $moduleCode = "ROLE_ACCESS";
    protected $primaryKey = 'role_access_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'module_code',
            1 => 'role_id',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            'module_code' => request()->input($this->uniqueCombo[0]),
            'role_id' => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return RoleAccessResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return RoleAccess::orderBy($this->primaryKey, 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return RoleAccess::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return RoleAccess::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return RoleAccess::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])->get();
    }

    protected function insertNewRecord($newData)
    {
        return RoleAccess::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        RoleAccess::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'module_code'	    => 'required|exists:modules,module_code',
            'role_id'	        => 'required|exists:roles,role_id',
            'access_listing'    => 'required|min:1|max:1|boolean',
            'access_create'		=> 'required|min:1|max:1|boolean',
            'access_show'		=> 'required|min:1|max:1|boolean',
            'access_edit'		=> 'required|min:1|max:1|boolean',
            'access_delete'		=> 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'module_code'	    => 'required|exists:modules,module_code',
            'role_id'	        => 'required|exists:roles,role_id',
            'access_listing'    => 'required|min:1|max:1|boolean',
            'access_create'		=> 'required|min:1|max:1|boolean',
            'access_show'		=> 'required|min:1|max:1|boolean',
            'access_edit'		=> 'required|min:1|max:1|boolean',
            'access_delete'		=> 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "module_code"       => $request->module_code,
            "role_id"           => $request->role_id,
            "access_listing"    => $request->access_listing,
            "access_create"     => $request->access_create,
            "access_show"       => $request->access_show,
            "access_edit"       => $request->access_edit,
            "access_delete"     => $request->access_delete,
        ];
    }

    public function showByRole($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'show' );

        if ( $validationCheck == "YES" )
        {
            $roleAccess = RoleAccess::where('role_id', $idOrCode)->get();

            if ( $roleAccess->count() == 0 )
            {
                $role_count = Role::where('role_name', $idOrCode)->count();

                if ( $role_count > 0 )
                {
                    $role = Role::where('role_name', $idOrCode)->first();
                    $roleAccess = RoleAccess::where('role_id', $role->role_id)->get();
                }
            }

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($roleAccess), $this->successReadMessageResponse);
        }

        return response($this->response, $this->status);
    }

}
