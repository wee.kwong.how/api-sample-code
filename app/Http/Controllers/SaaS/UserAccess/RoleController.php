<?php

namespace App\Http\Controllers\SaaS\UserAccess;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SaaS\Role;
use Auth;
use App\Http\Resources\SaaS\RoleResource;
use Illuminate\Validation\Rule;

class RoleController extends APIController
{
    protected $moduleCode = "ROLE";
    protected $primaryKey = 'role_id';
    protected $uniqueColumnName = "role_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest;

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest = request()->input($this->uniqueColumnName);
    }

    protected function makeResoucesCollection($data)
    {
        return RoleResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Role::orderBy($this->uniqueColumnName, 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Role::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Role::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest)->get();
    }

    protected function insertNewRecord($newData)
    {
        return Role::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Role::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'role_name'			=> 'required|min:4|max:100|unique:roles,role_name',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'role_name'			=> 'required|min:4|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"        => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "role_name"     => $request->role_name,
        ];
    }
}
