<?php

namespace App\Http\Controllers\SemCorp\Setup\ProjectCategory;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectCategory;
use App\Models\SemCorp\AuditLogSetup;
use Auth;
use App\Http\Resources\SemCorp\ProjectCategoryResource;
use Illuminate\Validation\Rule;
use DB;

class ProjectCategoryController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_CATEGORY";
    protected $primaryKey = 'project_category_id';
    protected $uniqueColumnName = "project_category";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $tableName = 'project_categories';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_category',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectCategoryResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectCategory::orderBy('project_category', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectCategory::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectCategory::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectCategory::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogSetup::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectCategory::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_category_id   = $request->project_category_id;
        $destinationData->project_category      = $request->project_category;
        $destinationData->active_status         = $request->active_status;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, $this->tableName);
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectCategory::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_category'	        => 'required|min:1|max:100|unique:project_categories,project_category',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_category_id'       => 'required|exists:project_categories,project_category_id',
            'project_category'	        => 'required|min:1|max:100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_category_id"       => $request->project_category_id,
            "project_category"          => $request->project_category,
            "active_status"             => $request->active_status,
        ];
    }
}
