<?php

namespace App\Http\Controllers\SemCorp\Setup\ProjectType;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectType;
use App\Models\SemCorp\AuditLogSetup;
use Auth;
use App\Http\Resources\SemCorp\ProjectTypeResource;
use Illuminate\Validation\Rule;
use DB;

class ProjectTypeController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_TYPE";
    protected $primaryKey = 'project_type_id';
    protected $uniqueColumnName = "project_type";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $tableName = 'project_types';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_type',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectTypeResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectType::orderBy('project_type', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectType::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectType::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectType::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogSetup::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectType::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_type_id       = $request->project_type_id;
        $destinationData->project_type          = $request->project_type;
        $destinationData->active_status         = $request->active_status;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, $this->tableName);
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectType::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_type'	            => 'required|min:1|max:100|unique:project_types,project_type',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_type_id'           => 'required|exists:project_types,project_type_id',
            'project_type'	            => 'required|min:1|max:100',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_type_id"           => $request->project_type_id,
            "project_type"              => $request->project_type,
            "active_status"             => $request->active_status,
        ];
    }
}
