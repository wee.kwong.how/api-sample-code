<?php

namespace App\Http\Controllers\SemCorp\Setup\Role;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\SemCorp\Role;
use App\Models\SemCorp\AuditLogSetup;
use Auth;
use App\Http\Resources\SemCorp\RoleListResource;
use Illuminate\Validation\Rule;
use DB;

class RoleController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "ROLE";
    protected $primaryKey = 'role_id';
    protected $uniqueColumnName = "role_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $tableName = 'roles';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'role_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return RoleListResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Role::orderBy('role_name', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Role::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Role::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Role::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogSetup::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = Role::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->role_id       = $request->id;
        $destinationData->role_name     = $request->name;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, $this->tableName);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Role::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'name'             => 'required|unique:roles,role_name'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'name'           => 'required'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "id"        => $request->role_id,
            "name"      => $request->role_name
        ];
    }
}
