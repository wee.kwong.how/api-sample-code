<?php

namespace App\Http\Controllers\SemCorp\Setup\Contractor;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\SemCorp\Contractor;
use App\Models\SemCorp\SiteContractor;
use Auth;
use App\Http\Resources\SemCorp\ContractorResource;
use Illuminate\Validation\Rule;
use DB;

class ContractorController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "CONTRACTOR";
    protected $primaryKey = 'contractor_id';
    protected $uniqueColumnName = "contractor_code";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'contractor_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ContractorResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Contractor::orderBy('contractor_code', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Contractor::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Contractor::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Contractor::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Contractor::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Contractor::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'contractor_code'	            => 'required|min:1|max:50|unique:project_contractors,contractor_code',
            'contractor_name'	            => 'required|min:1|max:100',
            'contractor_contact_person'	    => 'required|min:1|max:100',
            'contractor_contact_number'	    => 'required|min:1|max:100',
            'contractor_contact_email'	    => 'required|email|min:1|max:250',
            'active_status'	                => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'contractor_id'                 => 'required|exists:project_contractors,contractor_id',
            'contractor_code'	            => 'required|min:1|max:50',
            'contractor_name'	            => 'required|min:1|max:100',
            'contractor_contact_person'	    => 'required|min:1|max:100',
            'contractor_contact_number'	    => 'required|min:1|max:100',
            'contractor_contact_email'	    => 'required|email|min:1|max:250',
            'active_status'	                => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "contractor_id"                 => $request->contractor_id,
            "contractor_code"               => $request->contractor_code,
            "contractor_name"               => $request->contractor_name,
            "contractor_contact_person"     => $request->contractor_contact_person,
            "contractor_contact_number"     => $request->contractor_contact_number,
            "contractor_contact_email"      => $request->contractor_contact_email,
            "active_status"                 => $request->active_status,
        ];
    }

    public function showContractorNotInSiteContractor($idOrCode)
    {
        $siteContractor = SiteContractor::where('site_id', $idOrCode)->Orderby('created_at', 'ASC')->pluck('contractor_id');

        //dd($siteContractor->contractor_id);

        $data = Contractor::whereNotIn('contractor_id', $siteContractor)->Orderby('created_at', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

}
