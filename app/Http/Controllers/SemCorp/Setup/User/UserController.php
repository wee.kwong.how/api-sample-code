<?php

namespace App\Http\Controllers\SemCorp\Setup\User;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\AuditLogSetup;
use Auth;
use App\Http\Resources\UserResource;
use Illuminate\Validation\Rule;
use DB;

class UserController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "USER";
    protected $primaryKey = 'id';
    protected $uniqueColumnName = "email";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $tableName = 'users';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'email',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return UserResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return User::orderBy('name', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return User::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return User::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return User::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogSetup::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = User::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->id            = $request->id;
        $destinationData->email         = $request->email;
        if ( $request->password != null )
        {
            $destinationData->password      = bcrypt($request->password);
        }
        $destinationData->name          = $request->name;
        $destinationData->customer_id   = $request->customer_id;
        $destinationData->role_id       = $request->role_id;
        $destinationData->contractor_id = $request->contractor_id;
        $destinationData->active_status = $request->active_status;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, $this->tableName);
    }

    protected function softDeleteRecord($idOrCode)
    {
        User::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'email'                 => 'required|email|unique:users,email',
            'name'                  => 'required',
            'password'              => [
                'required',
                'string',
                'min:8',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'role_id'	            => 'required',
            'active_status'	        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'name'           => 'required',
            'role_id'        => 'required',
            'password'              => [
                'nullable',
                'string',
                'min:8',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'active_status'	 => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        if ($request->contractor_id == 'x')
        {
            $request->contractor_id = null;
        }

        return
        [
            "id"           => $request->id,
            "email"          => $request->email,
            "name"              => $request->name,
            "password"          => bcrypt($request->password),
            "customer_id"       => $request->customer_id,
            "role_id"           => $request->role_id,
            "contractor_id"     => $request->contractor_id,
            "active_status"    => $request->active_status,
        ];
        
    }
}
