<?php

namespace App\Http\Controllers\SemCorp\Setup\Developer;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\SemCorp\Developer;
use Auth;
use App\Http\Resources\SemCorp\DeveloperResource;
use Illuminate\Validation\Rule;
use DB;

class DeveloperController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "DEVELOPER";
    protected $primaryKey = 'developer_id';
    protected $uniqueColumnName = "developer_code";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'developer_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DeveloperResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Developer::orderBy('developer_code', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Developer::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Developer::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Developer::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Developer::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Developer::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'developer_code'	        => 'required|min:1|max:50|unique:project_developers,developer_code',
            'developer_name'	        => 'required|min:1|max:100',
            'developer_contact_person'	=> 'required|min:1|max:100',
            'developer_contact_number'	=> 'required|min:1|max:100',
            'developer_contact_email'	=> 'required|email|min:1|max:250',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'developer_id'              => 'required|exists:project_developers,developer_id',
            'developer_code'	        => 'required|min:1|max:50',
            'developer_name'	        => 'required|min:1|max:100',
            'developer_contact_person'	=> 'required|min:1|max:100',
            'developer_contact_number'	=> 'required|min:1|max:100',
            'developer_contact_email'	=> 'required|email|min:1|max:250',
            'active_status'	            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "developer_id"           => $request->developer_id,
            "developer_code"         => $request->developer_code,
            "developer_name"         => $request->developer_name,
            "developer_contact_person"  => $request->developer_contact_person,
            "developer_contact_number"  => $request->developer_contact_number,
            "developer_contact_email"   => $request->developer_contact_email,
            "active_status"             => $request->active_status,
        ];
    }

}
