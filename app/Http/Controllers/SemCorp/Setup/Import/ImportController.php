<?php

namespace App\Http\Controllers\SemCorp\Setup\Import;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Validation\Rule;
use App\Models\SemCorp\ImportReturn;
use App\Models\SemCorp\SiteContractor;
use DB;

class ImportController extends SEMCorp_APIController
{

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->status = config('response.status.success');
	}

    public function importToSite(Request $request)
    {
        $this->validationStoreRequest($request);

        $check_site_contractor_count = 0;

        $check_site_contractor_count = SiteContractor::where('site_id', $request->site_id)->get();

        if ($check_site_contractor_count->count() == 0)
        {
            $this->status = config('response.status.error');
            $this->response =
            [
                'message' => config('response.message.error.invalid_data'),
                'errors' =>
                [
                    'site_contractor' => 'No Site Contractor Available',
                ]
            ];
        }
        else
        {
            $this->callDBtoImport($request);
        }

        return response($this->response, $this->status);
    }

    protected function callDBtoImport($request)
    {
        $task_template = 'YES';
        $document_template = 'YES';
        $model = new ImportReturn();
        $model2 = new ImportReturn();

        if ($request->has('task_template_id'))
        {
            $returnData = $model->hydrate(DB::select(" CALL `sp_import_task_from_template`( " . $request->task_template_id . " , " . $request->site_id . " , " . $this->loginUserInfo['created_by'] . " ) "));

            $this->response =
            [
                'side_code' => $returnData[0]->site_code,
                'task_template_code' => $returnData[0]->task_template_code,
            ];
        }
        else
        {
            $task_template = 'NO';
        }

        if ($request->has('document_template_id') && $request->has('task_template_id'))
        {
            $returnDataDoc = $model2->hydrate(DB::select(" CALL `sp_import_document_from_template`( " . $request->document_template_id . " , " . $request->site_id . " , " . $this->loginUserInfo['created_by'] . " ) "));

            $this->response +=
            [
                'side_code' => $returnDataDoc[0]->site_code,
                'document_template_code' => $returnDataDoc[0]->document_template_code,
            ];
        }
        else if ($request->has('document_template_id') )
        {
            $returnDataDoc = $model2->hydrate(DB::select(" CALL `sp_import_document_from_template`( " . $request->document_template_id . " , " . $request->site_id . " , " . $this->loginUserInfo['created_by'] . " ) "));

            // dd($returnDataDoc);

            $this->response =
            [
                'side_code' => $returnDataDoc[0]->site_code,
                'document_template_code' => $returnDataDoc[0]->document_template_code,
            ];
        }

        if ( $task_template == 'NO' && $document_template == 'NO' )
        {
            $this->status = config('response.status.error');
            $this->response =
            [
                'message' => config('response.message.error.invalid_data'),
                'errors' =>
                [
                    'Error Listing'
                ]
            ];
        }
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_id'                => 'required|exists:projects,project_id',
            'group_id'                  => 'required|exists:project_groups,group_id',
            'site_id'                   => 'required|exists:project_sites,site_id',
            'task_template_id'          => 'nullable|exists:task_templates,task_template_id',
            'document_template_id'      => 'nullable|exists:document_templates,document_template_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

}
