<?php

namespace App\Http\Controllers\SemCorp\Task;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\TaskTemplate;
use App\Models\SemCorp\Task;
use App\Models\SemCorp\AuditLogTask;
use App\Models\SemCorp\ProjectSite;
use App\Models\SemCorp\View\ProjectGroupSiteTaskView;
use Auth;
use App\Http\Resources\SemCorp\TaskResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\SiteTaskChangeStatusMail;
use Illuminate\Support\Facades\Storage;
use App\Models\SemCorp\Dashboard\DailyOverDueProject;

class TaskController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "TASK";
    protected $primaryKey = 'task_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Task Title cannot be repeat in within the same Task & Milestone.";

    protected $backend_path = '/site_task_attachments/';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'milestone_id',
            1 => 'site_id',
            2 => 'task_title',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
            $this->uniqueCombo[2] => request()->input($this->uniqueCombo[2]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return TaskResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Task::orderBy('task_title', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Task::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = TaskTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        //dd($validationCheck);

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return Task::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        ->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Task::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $newId = Task::insertGetId($newData);

        $task_data = Task::where('task_id', $newId)->first();

        if (in_array($task_data->status_id, [3,4,5,6,7]))
        {
            $this->siteTaskSendEmailChangeStatus($newId);
        }

        return $newId;
    }

    public function siteTaskSendEmailChangeStatus($task_id)
    {
        $task_data = Task::where('task_id', $task_id)->first();

        $site_data = ProjectSite::where('site_id', $task_data->site_id)->first();

        $project_engineer = $this->EmailUsers->getProjectEngineer($site_data->group_id);

        if ( $project_engineer !== null )
        {
            $project_engineer->notify( new SiteTaskChangeStatusMail($task_data, $project_engineer->name));
        }
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTask::insert($changes);
    }

    public function updateTaskRecord(Request $request, $idOrCode)
    {
        $this->uniqueColumnRequest($request);

        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'edit' );

        if ( $validationCheck == "YES" )
        {
            if ( $this->uniqueOrNotSetting == "YES" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueColumnName);
            }
            else if ( $this->uniqueOrNotSetting == "COMBO" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueCombo);
            }
            else
            {
                $codeOrIdCheck = $this->validateCodeOrId($idOrCode, $this->primaryKey);
            }

            if ( $codeOrIdCheck == "YES" )
            {
                $this->validationUpdateRequest($request);
                $this->updateExistingRecord($request, $idOrCode);
            }
        }

        return response($this->response, $this->status);
    }

    public function downloadDocument($idOrCode)
    {
        $task_detail =  Task::where('task_id', $idOrCode)->first();

        $path = storage_path($this->backend_path.$task_detail->upload_attachment);

        return response()->download($path);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = Task::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->task_title            = $request->task_title;
        $destinationData->assign_to_user        = $request->assign_to_user;
        $destinationData->task_description      = $request->task_description;
        $destinationData->task_remarks          = $request->task_remarks;
        $destinationData->milestone_id          = $request->milestone_id;
        $destinationData->contractor_id         = $request->contractor_id;
        $destinationData->task_progress         = $request->task_progress;
        $destinationData->status_id             = $request->status_id;
        $destinationData->task_est_start_date   = $request->task_est_start_date;
        $destinationData->task_est_end_date     = $request->task_est_end_date;
        $destinationData->task_start_date       = $request->task_start_date;
        $destinationData->task_end_date         = $request->task_end_date;
        
        if ($request->hasFile('upload_attachment')) 
        {
            $image = $request->file('upload_attachment');

            $imgName = $request->task_id . '_' . time() . '_' . $image->getClientOriginalName();

            $location = storage_path($this->backend_path); //move file to ftp file

            Storage::disk('sftp')->putFileAs($this->backend_path, $image, $imgName);

            $destinationData->upload_attachment      = $imgName;
         }

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'itasks');

        if (array_key_exists('status_id', $changed))
        {
            if (in_array($changed['status_id'], [3,4,5,6,7]))
            {
                $this->siteTaskSendEmailChangeStatus($original['task_id']);
            }
        }

        # SEND EMAIL TO PROJECT MANAGER & PROJECT ENGINEER
        # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
        # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
        # 3. Get $this->EmailUsers->getProjetManager($project_id)
        # 4. Get Group ID $group_id = $this->EmailUsers->getGroupIDfromSiteID($idOrCode)
        # 5. Get $this->EmailUsers->getProjectEngineer($group_id)
    }

    protected function softDeleteRecord($idOrCode)
    {
        $model = new DailyOverDueProject();

        $model->hydrate(DB::select(" CALL `sp_delete_site_task`( '" . $idOrCode . "' , '" . $this->loginUserInfo['created_by'] . "' ) "));
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_id'                       => 'required|exists:project_sites,site_id',
            'task_title'	                => 'required|min:1|max:100',
            'assign_to_user'                => 'required|exists:users,id',
            'milestone_id'                  => 'required|exists:project_milestones,milestone_id',
            'contractor_id'                 => 'nullable|exists:project_contractors,contractor_id',
            'task_description'	            => 'nullable|min:1|max:300',
            'task_remarks'	                => 'nullable|min:1|max:1000',
            'task_progress'	                => 'required|integer|gte:0|lte:100',
            'status_id'                     => 'required|exists:status_tasks,status_id',
            'task_est_start_date'           => 'required|date',
            'task_est_end_date'             => 'required|date|after_or_equal:task_est_start_date',
            'task_start_date'               => 'nullable|date',
            'task_end_date'                 => 'nullable|date|after_or_equal:task_start_date',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'task_id'                       => 'required|exists:itasks,task_id',
            'site_id'                       => 'required|exists:project_sites,site_id',
            'task_title'	                => 'required|min:1|max:100',
            'assign_to_user'                => 'required|exists:users,id',
            'milestone_id'                  => 'required|exists:project_milestones,milestone_id',
            'contractor_id'                 => 'nullable|exists:project_contractors,contractor_id',
            'task_description'	            => 'nullable|min:1|max:300',
            'task_remarks'	                => 'nullable|min:1|max:1000',
            'task_progress'	                => 'required|integer|gte:0|lte:100',
            'status_id'                     => 'required|exists:status_tasks,status_id',
            'task_est_start_date'           => 'required|date',
            'task_est_end_date'             => 'required|date|after_or_equal:task_est_start_date',
            'task_start_date'               => 'nullable|date',
            'task_end_date'                 => 'nullable|date|after_or_equal:task_start_date',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "task_id"                   => $request->task_id,
            "site_id"                   => $request->site_id,
            "task_title"                => $request->task_title,
            "assign_to_user"            => $request->assign_to_user,
            "milestone_id"              => $request->milestone_id,
            "contractor_id"             => $request->contractor_id,
            "task_description"          => $request->task_description,
            "task_remarks"              => $request->task_remarks,
            "task_progress"             => $request->task_progress,
            "status_id"                 => $request->status_id,
            "task_est_start_date"       => $request->task_est_start_date,
            "task_est_end_date"         => $request->task_est_end_date,
            "task_start_date"           => $request->task_start_date,
            "task_end_date"             => $request->task_end_date,
        ];
    }

    public function showTaskBySite($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = Task::where('site_id', $idOrCode)->Orderby('task_id', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showTaskByGroup($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data   = ProjectGroupSiteTaskView::where('group_id', $idOrCode)->Orderby('task_id', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showTaskProject($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data   = ProjectGroupSiteTaskView::where('project_id', $idOrCode)->Orderby('task_id', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showTaskByUser($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = Task::where('assign_to_user', $idOrCode)->Orderby('task_id', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }


}
