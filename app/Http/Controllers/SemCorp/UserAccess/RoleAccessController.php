<?php

namespace App\Http\Controllers\SemCorp\UserAccess;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\User;
use App\Models\SemCorp\Role;
use App\Models\SemCorp\RoleAccess;
use Auth;
use App\Http\Resources\SemCorp\RoleAccessResource;
use App\Http\Resources\SemCorp\RoleAccessDisplayResource;

class RoleAccessController extends SEMCorp_APIController
{
    protected $moduleCode = "ROLE_ACCESS";

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    protected function makeResoucesCollection($data)
    {
        return RoleAccessResource::Collection($data);
    }

    public function showByRole($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'show' );

        if ( $validationCheck == "YES" )
        {
            $roleAccess = RoleAccess::where('role_id', $idOrCode)->get();

            if ( $roleAccess->count() == 0 )
            {
                $role_count = Role::where('role_name', $idOrCode)->count();

                if ( $role_count > 0 )
                {
                    $role = Role::where('role_name', $idOrCode)->first();
                    $roleAccess = RoleAccess::where('role_id', $role->role_id)->get();
                }
            }

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($roleAccess), $this->successReadMessageResponse);
        }

        return response($this->response, $this->status);
    }


    public function userRoleForEachModule()
    {
        $setup_modules =  ['DEVELOPER', 'CONTRACTOR', 'USER', 'PROJECT_TYPE', 'PROJECT_CATEGORY'];

        $template_modules =
        [
            'MILESTONE_TEMPLATE', 'MILESTONE_TEMPLATE_DETAIL',
            'TASK_TEMPLATE', 'TASK_TEMPLATE_DETAIL',
            'DOCUMENT_TEMPLATE', 'DOCUMENT_TEMPLATE_DETAIL',
            'PROJECT_TASK_TEMPLATE', 'PROJECT_TASK_TEMPLATE_DETAIL',
            'PROJECT_DOCUMENT_TEMPLATE', 'PROJECT_DOCUMENT_TEMPLATE_DETAIL',
        ];

        $project_modules = [ 'PROJECT', 'PROJECT_MILESTONE', 'PROJECT_GROUP', 'PROJECT_SITE', 'SITE_CONTRACTOR'];

        $site_transaction_modules = [ 'TASK', 'SITE_DOCUMENT', 'SITE_DOCUMENT_UPLOADS', 'SITE_DOCUMENT_RECURRING', 'SITE_DOCUMENT_APPROVAL'];

        $project_transaction_modules = [ 'PROJECT_TASK', 'PROJECT_DOCUMENT', 'PROJECT_DOCUMENT_UPLOADS', 'PROJECT_DOCUMENT_RECURRING', 'PROJECT_DOCUMENT_APPROVAL'];

        $setup_modules_data = RoleAccess::whereIn('module_code', $setup_modules)
        ->OrderBy('module_code', 'ASC')->where('role_id', $this->loginUserInfo['role'])->get();

        $template_module_data = RoleAccess::whereIn('module_code', $template_modules)
        ->OrderBy('module_code', 'ASC')->where('role_id', $this->loginUserInfo['role'])->get();

        $project_module_data = RoleAccess::whereIn('module_code', $project_modules)
        ->OrderBy('module_code', 'ASC')->where('role_id', $this->loginUserInfo['role'])->get();

        $site_transaction_module_data = RoleAccess::whereIn('module_code', $site_transaction_modules)
        ->OrderBy('module_code', 'ASC')->where('role_id', $this->loginUserInfo['role'])->get();

        $project_transaction_module_data = RoleAccess::whereIn('module_code', $project_transaction_modules)
        ->OrderBy('module_code', 'ASC')->where('role_id', $this->loginUserInfo['role'])->get();

        return
        [
            'data' =>
            [
                'setup_modules'                 => RoleAccessDisplayResource::Collection($setup_modules_data),
                'template_modules'              => RoleAccessDisplayResource::Collection($template_module_data),
                'project_modules'               => RoleAccessDisplayResource::Collection($project_module_data),
                'site_transaction_modules'      => RoleAccessDisplayResource::Collection($site_transaction_module_data),
                'project_transaction_modules'   => RoleAccessDisplayResource::Collection($project_transaction_module_data),
            ],
        ];
    }

    public function projectAccess()
    {
        $project_modules = [ 'PROJECT', 'PROJECT_MILESTONE', 'PROJECT_GROUP', 'PROJECT_SITE'];

        $project_module_data = RoleAccess::whereIn('module_code', $project_modules)
        ->OrderBy('module_code', 'ASC')->where('role_id', $this->loginUserInfo['role'])->get();

        return
        [
            'data' =>
            [
                'project_modules' => RoleAccessDisplayResource::Collection($project_module_data),
            ],
        ];

    }

    public function siteAccess()
    {
        $project_modules = [ 'TASK' ];

        $project_module_data = RoleAccess::whereIn('module_code', $project_modules)
        ->OrderBy('module_code', 'ASC')->where('role_id', $this->loginUserInfo['role'])->get();

        return
        [
            'data' =>
            [
                'project_modules' => RoleAccessDisplayResource::Collection($project_module_data),
            ],
        ];

    }

}
