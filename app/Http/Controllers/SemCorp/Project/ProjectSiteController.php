<?php

namespace App\Http\Controllers\SemCorp\Project;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\ProjectSite;
use App\Models\SemCorp\ProjectDetail;
use App\Models\SemCorp\AuditLogProject;
use App\Models\SemCorp\MilestoneTemplateDetail;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SemCorp\ProjectSiteResource;
use Illuminate\Validation\Rule;
use App\Models\SemCorp\ImportReturn;
use DB;
use App\Notifications\SiteChangeStatusMail;

class ProjectSiteController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_SITE";
    protected $primaryKey = 'site_id';
    protected $uniqueColumnName = "site_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $uploadRequest;

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'site_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectSiteResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectSite::orderBy('site_id', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectSite::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectSite::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $newId = ProjectSite::insertGetId($newData);

        if ( $this->uploadRequest->document_template_id != null )
        {
            $this->importSiteDocumentFromTemplate($newId);
        }

        if ( $this->uploadRequest->task_template_id != null )
        {
            $this->importSiteTaskFromTemplate($newId);
        }

        $site_data = ProjectSite::where('site_id', $newId)->first();

        if (in_array($site_data->status_id, [4,5,6,7]))
        {
            $this->siteSendEmailChangeStatus($newId);
        }

        return $newId;

        # SEND EMAIL HERE
        # You can get project Owner using $this->EmailUsers->getProjectOwner();
        # You can get the project Manager by using $newData->project_id send to $this->EmailUsers->getProjetManager($newData->project_id)
        # You can get Project Engineer using $newData->group_id send to $this->EmailUsers->getProjectEngineer($newData->group_id)
    }

    public function siteSendEmailChangeStatus($site_id)
    {
        $site_data = ProjectSite::where('site_id', $site_id)->first();

        $project_manager = $this->EmailUsers->getProjetManager($site_data->project_id);

        $project_engineer = $this->EmailUsers->getProjectEngineer($site_data->group_id);

        if ( $project_manager !== null )
        {
            $project_manager->notify( new SiteChangeStatusMail($site_data, $project_manager->name));
        }

        if ( $project_engineer !== null )
        {
            $project_engineer->notify( new SiteChangeStatusMail($site_data, $project_engineer->name));
        }

        // $user = User::where('id', 1)->first();

        // $user->notify( new GroupChangeStatusMail($site_data, $user->name));

        //dd($site_data);
        //dd($user);
    }

    protected function importSiteDocumentFromTemplate($site_id)
    {
        $model = new ImportReturn();

        $returnData = $model->hydrate(DB::select(" CALL `sp_import_document_from_template`( " . $this->uploadRequest->document_template_id . " , " . $this->loginUserInfo['created_by'] . " , " . $site_id . " ) "));

    }

    protected function importSiteTaskFromTemplate($site_id)
    {
        $model = new ImportReturn();

        $returnData = $model->hydrate(DB::select(" CALL `sp_import_task_from_template`( " . $this->uploadRequest->task_template_id . " , " . $this->loginUserInfo['created_by'] . " , " . $site_id . " ) "));

    }

    protected function insertAuditLog($changes)
    {
        AuditLogProject::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectSite::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->site_name                         = $request->site_name;
        $destinationData->site_postal_code                  = $request->site_postal_code;
        $destinationData->site_address                      = $request->site_address;
        $destinationData->site_progress                     = $request->site_progress;

        $destinationData->site_module_quantity              = $request->site_module_quantity;
        $destinationData->site_module_capacity              = $request->site_module_capacity;
        $destinationData->site_total_capacity               = $request->site_total_capacity;

        $destinationData->site_pr_capacity                  = $request->site_pr_capacity;
        $destinationData->site_type_of_tension              = $request->site_type_of_tension;
        $destinationData->site_approved_load                = $request->site_approved_load;
        $destinationData->site_mssl_account_number          = $request->site_mssl_account_number;

        $destinationData->site_plant_name                   = $request->site_plant_name;
        $destinationData->site_folder_in_sftp_server        = $request->site_folder_in_sftp_server;
        $destinationData->site_monitoring_application       = $request->site_monitoring_application;

        $destinationData->site_inverter_type                = $request->site_inverter_type;
        $destinationData->site_inverter_qty                 = $request->site_inverter_qty;
        $destinationData->site_total_inverter               = $request->site_total_inverter;

        $destinationData->site_logger_type                  = $request->site_logger_type;
        $destinationData->site_total_data_logger            = $request->site_total_data_logger;
        $destinationData->site_start_date                   = $request->site_start_date;

        $destinationData->site_target_turn_on_date          = $request->site_target_turn_on_date;
        $destinationData->site_end_date                     = $request->site_end_date;
        $destinationData->site_contractor_start_date        = $request->site_contractor_start_date;

        $destinationData->site_contractor_end_date          = $request->site_contractor_end_date;
        $destinationData->site_elu_end_date                 = $request->site_elu_end_date;
        $destinationData->site_el_expiry_date               = $request->site_el_expiry_date;

        $destinationData->site_elu_license_number           = $request->site_elu_license_number;
        $destinationData->site_elu_status                   = $request->site_elu_status;
        $destinationData->status_id                         = $request->status_id;
        $destinationData->document_template_id              = $request->document_template_id;
        $destinationData->task_template_id                  = $request->task_template_id;

        if ( ($original['document_template_id'] == null) AND ($request->document_template_id != null ) )
        {
            $this->importSiteDocumentFromTemplate($original['site_id']);
        }

        if ( ($original['task_template_id'] == null) AND ($request->task_template_id != null ) )
        {
            $this->importSiteTaskFromTemplate($original['site_id']);
        }

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_sites');

        if (array_key_exists('status_id', $changed))
        {
            if (in_array($changed['status_id'], [4,5,6,7]))
            {
                $this->siteSendEmailChangeStatus($original['site_id']);
            }
        }


        # SEND EMAIL HERE
        # You can get project Owner using $this->EmailUsers->getProjectOwner();
        # You can get the project Manager by using $request->project_id send to $this->EmailUsers->getProjetManager($request->project_id)
        # You can get Project Engineer using $request->group_id send to $this->EmailUsers->getProjectEngineer($request->group_id)
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectSite::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_name'	                    => 'required|min:1|max:300|unique:project_sites,site_name',
            'project_id'                    => 'required|min:1|max:50|exists:projects,project_id',
            'group_id'                      => 'required|min:1|max:50|exists:project_groups,group_id',
            'site_postal_code'	            => 'required|min:3|max:10',
            'site_address'	                => 'required|min:1|max:200',
            'site_progress'                 => 'required|integer|gt:-1',
            'site_module_quantity'	        => 'required|numeric|gt:-1',
            'site_module_capacity'	        => 'required|numeric|gt:-1',
            'site_total_capacity'	        => 'required|numeric|gt:-1',
            'site_pr_capacity'              => 'required|integer|gt:-1',
            'site_type_of_tension'	        => 'required|min:1|max:100',
            'site_approved_load'	        => 'required|min:1|max:100',
            'site_mssl_account_number'	    => 'required|min:1|max:100',
            'site_plant_name'	            => 'nullable|min:1|max:200',
            'site_folder_in_sftp_server'	=> 'nullable|min:1|max:600',
            'site_monitoring_application'	=> 'required|min:1|max:100',
            'site_inverter_type'            => 'required|min:1|max:50',
            'site_inverter_qty'             => 'nullable|integer|gt:-1',
            'site_total_inverter'           => 'nullable|integer|gt:-1',
            'site_total_data_logger'	    => 'nullable|min:1|max:100',
            'site_logger_type'              => 'required|min:1|max:100',
            'site_start_date'	            => 'required|date',
            'site_target_turn_on_date'	    => 'required|date',
            'site_end_date'                 => 'required|date|after_or_equal:site_start_date',
            'site_contractor_start_date'	=> 'required|date',
            'site_contractor_end_date'      => 'required|date|after_or_equal:site_contractor_start_date',
            'site_elu_end_date'	            => 'nullable|date',
            'site_el_expiry_date'	        => 'nullable|date',
            'site_elu_license_number'	    => 'required|min:1|max:100',
            'site_elu_status'	            => 'nullable|min:1|max:100',
            'status_id'                     => 'required|exists:status_groups,status_id',

            'task_template_id'              => 'nullable|exists:task_templates,task_template_id',
            'document_template_id'          => 'nullable|exists:document_templates,document_template_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_id'                       => 'required|min:1|max:50|exists:project_sites,site_id',
            'project_id'                    => 'required|min:1|max:50|exists:projects,project_id',
            'group_id'                      => 'required|min:1|max:50|exists:project_groups,group_id',
            'site_name'	                    => 'required|min:1|max:300',
            'site_postal_code'	            => 'required|min:3|max:10',
            'site_address'	                => 'required|min:1|max:200',
            'site_progress'                 => 'required|integer|gt:-1',
            'site_module_quantity'	        => 'required|numeric',
            'site_module_capacity'	        => 'required|numeric',
            'site_total_capacity'	        => 'required|numeric',
            'site_pr_capacity'              => 'required|integer|gt:-1',
            'site_type_of_tension'	        => 'required|min:1|max:100',
            'site_approved_load'	        => 'required|min:1|max:100',
            'site_mssl_account_number'	    => 'required|min:1|max:100',
            'site_plant_name'	            => 'nullable|min:1|max:200',
            'site_folder_in_sftp_server'	=> 'nullable|min:1|max:600',
            'site_monitoring_application'	=> 'required|min:1|max:100',
            'site_inverter_type'            => 'required|min:1|max:50',
            'site_inverter_qty'             => 'nullable|integer|gt:-1',
            'site_total_inverter'           => 'nullable|integer|gt:-1',
            'site_total_data_logger'	    => 'nullable|min:1|max:100',
            'site_logger_type'              => 'required|min:1|max:100',
            'site_start_date'	            => 'required|date',
            'site_target_turn_on_date'	    => 'required|date',
            'site_end_date'                 => 'required|date|after_or_equal:site_start_date',
            'site_contractor_start_date'	=> 'required|date',
            'site_contractor_end_date'      => 'required|date|after_or_equal:site_contractor_start_date',
            'site_elu_end_date'	            => 'nullable|date',
            'site_el_expiry_date'	        => 'nullable|date',
            'site_elu_license_number'	    => 'required|min:1|max:100',
            'site_elu_status'	            => 'nullable|min:1|max:100',
            'status_id'                     => 'required|exists:status_groups,status_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        $this->uploadRequest = $request;

        return
        [
            "site_id"                       => $request->site_id,
            "project_id"                    => $request->project_id,
            "group_id"                      => $request->group_id,
            "site_name"                     => $request->site_name,
            "site_postal_code"              => $request->site_postal_code,
            "site_address"                  => $request->site_address,
            "site_progress"                 => $request->site_progress,
            "site_module_quantity"          => $request->site_module_quantity,
            "site_module_capacity"          => $request->site_module_capacity,
            "site_total_capacity"           => $request->site_total_capacity,
            "site_pr_capacity"              => $request->site_pr_capacity,
            "site_type_of_tension"          => $request->site_type_of_tension,
            "site_approved_load"            => $request->site_approved_load,
            "site_mssl_account_number"      => $request->site_mssl_account_number,
            "site_plant_name"               => $request->site_plant_name,
            "site_folder_in_sftp_server"    => $request->site_folder_in_sftp_server,
            "site_monitoring_application"   => $request->site_monitoring_application,
            "site_inverter_type"            => $request->site_inverter_type,
            "site_inverter_qty"             => $request->site_inverter_qty,
            "site_total_inverter"           => $request->site_total_inverter,
            "site_logger_type"              => $request->site_logger_type,
            "site_total_data_logger"        => $request->site_total_data_logger,
            "site_start_date"               => $request->site_start_date,
            "site_target_turn_on_date"      => $request->site_target_turn_on_date,
            "site_end_date"                 => $request->site_end_date,
            "site_contractor_start_date"    => $request->site_contractor_start_date,
            "site_contractor_end_date"      => $request->site_contractor_end_date,
            "site_elu_end_date"             => $request->site_elu_end_date,
            "site_el_expiry_date"           => $request->site_el_expiry_date,
            "site_elu_license_number"       => $request->site_elu_license_number,
            "site_elu_status"               => $request->site_elu_status,
            "status_id"                     => $request->status_id,

            "task_template_id"              => $request->task_template_id,
            "document_template_id"          => $request->document_template_id,
        ];
    }

    public function showSiteByProject($idOrCode)
    {
        $data = ProjectSite::where('project_id', $idOrCode)->Orderby('site_name', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }


    public function showSiteByGroup($idOrCode)
    {
        $data = ProjectSite::where('group_id', $idOrCode)->Orderby('site_name', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

}
