<?php

namespace App\Http\Controllers\SemCorp\Project;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\ProjectMilestone;
use App\Models\SemCorp\AuditLogProject;
use App\Models\SemCorp\View\ProjectMilestoneView;
use Auth;
use App\Http\Resources\SemCorp\BuildingResource;
use App\Http\Resources\SemCorp\ProjectMilestoneResource;
use App\Http\Resources\SemCorp\ProjectMilestoneResourceProgress;
use Illuminate\Validation\Rule;
use DB;

class ProjectMilestoneController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_MILESTONE";
    protected $primaryKey = 'milestone_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Project Milestone cannot be repeat in within the same Project.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_id',
            1 => 'milestone_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectMilestoneResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectMilestoneView::orderBy('milestone_sequence', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectMilestone::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Project::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return ProjectMilestone::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return ProjectMilestone::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectMilestone::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogProject::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        // ProjectMilestone::where($this->primaryKey, $idOrCode)->update($updateData);
        $destinationData = ProjectMilestone::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->milestone_code        = $request->milestone_code;
        $destinationData->project_id            = $request->project_id;
        $destinationData->milestone_info        = $request->milestone_info;
        $destinationData->milestone_sequence    = $request->milestone_sequence;
        $destinationData->active_status         = $request->active_status;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_milestones');
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectMilestone::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'milestone_code'	                        => 'required|min:1|max:50',
			'project_id'                                => 'required|exists:projects,project_id',
            'milestone_info'	                        => 'required|min:1|max:100',
            'milestone_sequence'                        => 'required|integer|gt:0',
            'active_status'	                            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'milestone_id'                              => 'required|exists:project_milestones,milestone_id',
            'milestone_code'	                        => 'required|min:1|max:50',
			'project_id'                                => 'required|exists:projects,project_id',
            'milestone_info'	                        => 'required|min:1|max:100',
            'milestone_sequence'                        => 'required|integer|gt:0',
            'active_status'	                            => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "milestone_id"                          => $request->milestone_id,
            "project_id"                            => $request->project_id,
            "milestone_code"                        => $request->milestone_code,
            "milestone_info"                        => $request->milestone_info,
            "milestone_sequence"                    => $request->milestone_sequence,
            "active_status"                         => $request->active_status,
        ];
    }

    public function showMilestoneByProject($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectMilestoneView::where('project_id', $idOrCode)->where('active_status', '1')->Orderby('milestone_sequence', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }

        return response($this->response, $this->status);
    }

    # This function is to ready.
    public function showMilestoneByGroup($idOrCode)
    {
        $data = ProjectMilestoneView::where('project_id', $idOrCode)->where('active_status', '1')->Orderby('milestone_sequence', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

}
