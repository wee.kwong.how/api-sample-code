<?php

namespace App\Http\Controllers\SemCorp\Project;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\ProjectSCurve;
use App\Models\SemCorp\ProjectDetail;
use App\Models\SemCorp\AuditLogProject;
use App\Models\SemCorp\ImportReturn;
use App\Models\SemCorp\MilestoneTemplateDetail;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SemCorp\ProjectResource;
use App\Http\Resources\SemCorp\ProjectSCurveResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\ProjectChangeStatusMail;
use Notification;

class ProjectController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT";
    protected $primaryKey = 'project_id';
    protected $uniqueColumnName = "project_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $uploadRequest;

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_name',
        ];
	}

    public function testGetOwner()
    {
        #### SAMPLE for you to use Email User Role Classes
        # url to test http://{{server_url}}/project/project.test_get_owner
        ## getProjectOwner

        return $this->EmailUsers->getProjectOwner();
    }

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Project::orderBy('project_id', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Project::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return Project::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $newId = Project::insertGetId($newData);

        if ( $this->uploadRequest->project_document_template_id != null )
        {
            $this->importProjectDocumentFromTemplate($newId);
        }

        if ( $this->uploadRequest->project_task_template_id != null )
        {
            $this->importProjectTaskFromTemplate($newId);
        }

        $project_data = Project::where('project_id', $newId)->first();

        if (in_array($project_data->status_id, [4,5,6,7]))
        {
            $this->projectSendEmailChangeStatus($newId);
        }

        # IF new status = COMPLETED / APPROVED / ON-HOLD / RE-SCHEDULE / DELAYED send email to project Owner.
        # Send email to Engineer and Safety
        # You can get project Owner using $this->EmailUsers->getProjectOwner();


        return $newId;
    }

    public function projectSendEmailChangeStatus($project_id)
    {
        $project_data = Project::where('project_id', $project_id)->first();

        $project_owner_datas = $this->EmailUsers->getProjectOwner();
        // $engineer_data =  $this->EmailUsers->getEngineerForProject($project_id);
        // $safety_data =  $this->EmailUsers->getSafetyForProject($project_id);

        $display_data =
        [
            'project_data'          => $project_data,
            'project_owner_datas'   => $project_owner_datas,
            // 'engineer_data'         => $engineer_data,
            // 'safety_data'           => $safety_data,
        ];

        if ( $project_owner_datas !== null )
        {
            foreach($project_owner_datas as $project_owner)
            {
                $project_owner->notify( new ProjectChangeStatusMail($project_data, $project_owner->name));

            }
        }

        // $ken = User::where('id', 1)->first();

        // $ken->notify( new ProjectChangeStatusMail($project_data, $project_owner_datas[0]->name));
        //Notification::send($ken, new ProjectChangeStatusMail($project_data));

    }


    protected function importProjectDocumentFromTemplate($project_id)
    {
        $model = new ImportReturn();

        $returnData = $model->hydrate(DB::select(" CALL `sp_import_project_document_into_project`( " . $this->uploadRequest->project_document_template_id . " , " . $this->loginUserInfo['created_by'] . " , " . $project_id . " ) "));

    }

    protected function importProjectTaskFromTemplate($project_id)
    {
        $model = new ImportReturn();

        $returnData = $model->hydrate(DB::select(" CALL `sp_import_project_task_into_project`( " . $this->uploadRequest->project_task_template_id . " , " . $this->loginUserInfo['created_by'] . " , " . $project_id . " ) "));

    }

    protected function insertAuditLog($changes)
    {
        AuditLogProject::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = Project::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_name                      = $request->project_name;
        $destinationData->project_country                   = $request->project_country;
        $destinationData->project_manager                   = $request->project_manager;
        $destinationData->project_engineer                  = $request->project_engineer;
        $destinationData->project_safety                    = $request->project_safety;
        $destinationData->project_type_id                   = $request->project_type_id;
        $destinationData->project_pv_capacity               = $request->project_pv_capacity;
        $destinationData->project_category_id               = $request->project_category_id;
        $destinationData->developer_id                      = $request->developer_id;
        $destinationData->status_id                         = $request->status_id;
        $destinationData->project_start_date                = $request->project_start_date;
        $destinationData->project_end_date                  = $request->project_end_date;
        $destinationData->project_ppa_start_date            = $request->project_ppa_start_date;
        $destinationData->project_ppa_contractual_cod_date  = $request->project_ppa_contractual_cod_date;
        $destinationData->project_document_template_id      = $request->project_document_template_id;
        $destinationData->project_task_template_id          = $request->project_task_template_id;

        if ( ($original['project_document_template_id'] == null) AND ($request->project_document_template_id != null ) )
        {
            $this->importProjectDocumentFromTemplate($original['project_id']);
        }

        if ( ($original['project_task_template_id'] == null) AND ($request->project_task_template_id != null ) )
        {
            $this->importProjectTaskFromTemplate($original['project_id']);
        }

        $changed = $destinationData->getChanges();
        
        $destinationData->save();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'projects');

        if (array_key_exists('status_id', $changed))
        {
            if (in_array($changed['status_id'], [4,5,6,7]))
            {
                $this->projectSendEmailChangeStatus($original['project_id']);
            }
        }

        # IF new status = COMPLETED / APPROVED / ON-HOLD / RE-SCHEDULE / DELAYED send email to project Owner.
        # Send email to Engineer and Safety is got changes. (Make use of the Original and Changes)
        # You can get project Engineer using $request->project_engineer
        # You can get project Safety using $request->project_safety

    }

    protected function softDeleteRecord($idOrCode)
    {
        Project::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_name'	            => 'required|min:1|max:300|unique:projects,project_name',
            'project_country'	        => 'required|min:1|max:100',
            'project_manager'           => 'required|exists:users,id',
            'project_engineer'          => 'required|exists:users,id',
            'project_safety'            => 'required|exists:users,id',
            'project_type_id'           => 'required|exists:project_types,project_type_id',
            'project_pv_capacity'       => 'required|numeric',
            'project_category_id'       => 'required|exists:project_categories,project_category_id',
            'developer_id'              => 'required|exists:project_developers,developer_id',
            'status_id'                 => 'required|exists:status_projects,status_id',
            'project_progress'          => 'required|integer|gt:-1',
            'project_start_date'        => 'required|date',
            'project_end_date'          => 'required|date|after_or_equal:project_start_date',
            'project_ppa_start_date'    => 'required|date',

            'project_ppa_contractual_cod_date'      => 'required|date',

            'project_task_template_id'              => 'nullable|exists:project_task_templates,project_task_template_id',
            'project_document_template_id'          => 'nullable|exists:project_document_templates,project_document_template_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_id'                => 'required|min:1|max:50|exists:projects,project_id',
            'project_name'	            => 'required|min:1|max:300',
            'project_country'	        => 'required|min:1|max:100',
            'project_manager'           => 'required|exists:users,id',
            'project_engineer'          => 'required|exists:users,id',
            'project_safety'            => 'required|exists:users,id',
            'project_type_id'           => 'required|exists:project_types,project_type_id',
            'project_pv_capacity'       => 'required|numeric',
            'project_category_id'       => 'required|exists:project_categories,project_category_id',
            'developer_id'              => 'required|exists:project_developers,developer_id',
            'status_id'                 => 'required|exists:status_projects,status_id',
            'project_progress'          => 'required|integer|gt:-1',
            'project_start_date'        => 'required|date',
            'project_end_date'          => 'required|date|after_or_equal:project_start_date',
            'project_ppa_start_date'    => 'required|date',
            'project_ppa_contractual_cod_date'  => 'required|date',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        $this->uploadRequest = $request;

        return
        [
            "project_id"                => $request->project_id,
            "project_name"              => $request->project_name,
            "project_country"           => $request->project_country,
            "project_manager"           => $request->project_manager,
            "project_engineer"          => $request->project_engineer,
            "project_safety"            => $request->project_safety,
            "project_type_id"           => $request->project_type_id,
            "project_pv_capacity"       => $request->project_pv_capacity,
            "project_category_id"       => $request->project_category_id,
            "developer_id"              => $request->developer_id,
            "status_id"                 => $request->status_id,
            "project_progress"          => $request->project_progress,
            "project_start_date"        => $request->project_start_date,
            "project_end_date"          => $request->project_end_date,
            "project_ppa_start_date"    => $request->project_ppa_start_date,
            "project_ppa_contractual_cod_date"    => $request->project_ppa_contractual_cod_date,

            "project_task_template_id"      => $request->project_task_template_id,
            "project_document_template_id"  => $request->project_document_template_id,
        ];
    }

    public function documentDetailCount($idOrCode)
    {
        $countDetails = ProjectDetail::where('project_id', $idOrCode)->get();

        return $countDetails->count();
    }

    public function milestoneDetailCount($idOrCode)
    {
        $ProjectDetail = Project::where('project_id', $idOrCode)->first();

        $countDetails = MilestoneTemplateDetail::where('milestone_template_id', $ProjectDetail->milestone_template_id)->get();

        return $countDetails->count();
    }

    public function projectSCurve($project_id)
    {
        // $year = date('Y');

        // $months_name = [ 1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec' ];

        $ProjectSCurves = ProjectSCurve::where('project_id', $project_id)->get();

        $data_labels = [];
        $task_results_est = [];
        $task_results_act = [];
        $doc_results_est = [];
        $doc_results_act = [];

        foreach($ProjectSCurves as $monthly_data)
        {
            array_push($data_labels, $monthly_data->month_of);
            array_push($task_results_est, $monthly_data->expected_total_task);
            array_push($task_results_act, $monthly_data->actual_total_task);
            array_push($doc_results_est, $monthly_data->expected_total_document);
            array_push($doc_results_act, $monthly_data->actual_total_document);
        }

        $data =
        [
            'labels'                => $data_labels,
            'task_results_est'      => $task_results_est,
            'task_results_act'      => $task_results_act,
            'doc_results_est'       => $doc_results_est,
            'doc_results_act'       => $doc_results_act,
        ];

        $this->response = $this->apiResponse->makeResponseData($data, $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function consolidated($project_id)
    {
        $year = date('Y');

        $data =
        [
            "series" => [ 61, 72, 84, 80, 216, 56 ],
            "label" => [ "Design", "Hoisting", "Installation", "Testing & Commissioning", "Turn-On", "Overdue"],
        ];

        $this->response = $this->apiResponse->makeResponseData($data, $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

}
