<?php

namespace App\Http\Controllers\SemCorp\Project;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\ProjectGroup;
use App\Models\SemCorp\ProjectDetail;
use App\Models\SemCorp\AuditLogProject;
use App\Models\SemCorp\MilestoneTemplateDetail;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SemCorp\ProjectGroupResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\GroupChangeStatusMail;

class ProjectGroupController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_GROUP";
    protected $primaryKey = 'group_id';
    protected $uniqueColumnName = "group_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'group_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectGroupResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectGroup::orderBy('group_id', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectGroup::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectGroup::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $newId = ProjectGroup::insertGetId($newData);

        # SEND EMAIL HERE
        # You can get project Owner using $this->EmailUsers->getProjectOwner();
        # You can get the project Manager by using $newData->project_id send to $this->EmailUsers->getProjetManager($newData->project_id)

        $group_data = ProjectGroup::where('group_id', $newId)->first();

        if (in_array($group_data->status_id, [4,5,6,7]))
        {
            $this->groupSendEmailChangeStatus($newId);
        }

        return $newId;
    }

    public function groupSendEmailChangeStatus($group_id)
    {
        $group_data = ProjectGroup::where('group_id', $group_id)->first();

        $project_owner_datas = $this->EmailUsers->getProjectOwner();

        $project_manager = $this->EmailUsers->getProjetManager($group_data->project_id);

        if ( $project_owner_datas !== null )
        {
            foreach($project_owner_datas as $project_owner)
            {
                $project_owner->notify( new GroupChangeStatusMail($group_data, $project_owner->name));
            }
        }

        if ( $project_manager !== null )
        {
            $project_manager->notify( new GroupChangeStatusMail($group_data, $project_manager->name));
        }

        // $user = User::where('id', 1)->first();

        // $user->notify( new GroupChangeStatusMail($group_data, $user->name));

        //dd($group_data);
        //dd($user);
    }



    protected function insertAuditLog($changes)
    {
        AuditLogProject::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectGroup::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_id                   = $request->project_id;
        $destinationData->group_name                   = $request->group_name;
        $destinationData->group_engineer               = $request->group_engineer;
        $destinationData->group_information            = $request->group_information;
        $destinationData->group_pv_capacity            = $request->group_pv_capacity;
        $destinationData->group_progress               = $request->group_progress;
        $destinationData->status_id                    = $request->status_id;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_groups');

        if (array_key_exists('status_id', $changed))
        {
            if (in_array($changed['status_id'], [4,5,6,7]))
            {
                $this->groupSendEmailChangeStatus($original['group_id']);
            }
        }

        # SEND EMAIL HERE
        # You can get project Owner using $this->EmailUsers->getProjectOwner();
        # You can get the project Manager by using $request->project_id send to $this->EmailUsers->getProjetManager($request->project_id)
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectGroup::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'group_name'	            => 'required|min:1|max:300|unique:project_groups,group_name',
            'project_id'                => 'required|min:1|max:50|exists:projects,project_id',
            'group_engineer'            => 'required|exists:users,id',
            'group_information'	        => 'required|min:1|max:300',
            'group_pv_capacity'         => 'required|numeric',
            'group_progress'            => 'required|integer|gt:-1',
            'status_id'                 => 'required|exists:status_groups,status_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'group_id'                  => 'required|min:1|max:50|exists:project_groups,group_id',
            'project_id'                => 'required|min:1|max:50|exists:projects,project_id',
            'group_name'	            => 'required|min:1|max:300',
            'group_engineer'            => 'required|exists:users,id',
            'group_information'	        => 'required|min:1|max:300',
            'group_pv_capacity'         => 'required|numeric',
            'group_progress'            => 'required|integer|gt:-1',
            'status_id'                 => 'required|exists:status_groups,status_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "group_id"                => $request->group_id,
            "project_id"              => $request->project_id,
            "group_name"              => $request->group_name,
            "group_engineer"          => $request->group_engineer,
            "group_information"       => $request->group_information,
            "group_pv_capacity"       => $request->group_pv_capacity,
            "group_progress"          => $request->group_progress,
            "status_id"               => $request->status_id,
        ];
    }

    public function showGroupByProject($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectGroup::where('project_id', $idOrCode)->Orderby('group_name', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }
}
