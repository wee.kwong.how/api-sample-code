<?php

namespace App\Http\Controllers\SemCorp\Project;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\ProjectSite;
use App\Models\SemCorp\SiteContractor;
use Auth;
use App\Http\Resources\SemCorp\BuildingResource;
use App\Http\Resources\SemCorp\SiteContractorResource;
use Illuminate\Validation\Rule;
use DB;
use App\Models\SemCorp\View\ProjectGroupSiteContractorView;

class SiteContractorController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "SITE_CONTRACTOR";
    protected $primaryKey = 'site_contractor_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'site_id',
            1 => 'contractor_id',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return SiteContractorResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return SiteContractor::orderBy('site_contractor_id', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return SiteContractor::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = ProjectSite::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        //dd($validationCheck);

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return SiteContractor::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return SiteContractor::where($this->primaryKey, '<>', $idOrCode)
        //     ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        return SiteContractor::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return SiteContractor::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        SiteContractor::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        SiteContractor::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'site_id'                            => 'required|exists:project_sites,site_id',
            'contractor_id'	                     => 'required|exists:project_contractors,contractor_id',
            'active_status'	                     => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_contractor_id'                 => 'required|exists:project_site_contractors,site_contractor_id',
            'site_id'                            => 'required|exists:project_sites,site_id',
            'contractor_id'	                     => 'required|exists:project_contractors,contractor_id',
            'active_status'	                     => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "site_id"                    => $request->site_id,
            "contractor_id"              => $request->contractor_id,
            "active_status"              => $request->active_status,
        ];
    }

    public function showSiteContractorBySite($idOrCode)
    {
        $data = SiteContractor::where('site_id', $idOrCode)->Orderby('created_at', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showSiteContractorByGroup($idOrCode)
    {
        $data   = ProjectGroupSiteContractorView::where('group_id', $idOrCode)->Orderby('group_name', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showSiteContractorByProject($idOrCode)
    {
        $data   = ProjectGroupSiteContractorView::where('project_id', $idOrCode)->Orderby('site_name', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }



}
