<?php

namespace App\Http\Controllers\SemCorp\Lookup;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Validation\Rule;
use App\Models\SemCorp\Role;
use App\Http\Resources\SemCorp\RoleListResource;
use DB;

class RoleListController extends SEMCorp_APIController
{
    public function __construct()
	{
        $this->initialParameterSetup();
	}

    // protected function uniqueColumnRequest($request)
    // {

    // }

    protected function makeResoucesCollection($data)
    {
        return RoleListResource::Collection($data);
    }

    public function roleListALL()
    {
        $data = Role::OrderBy('role_name', 'ASC')->get();
        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }


}
