<?php

namespace App\Http\Controllers\SemCorp\Lookup;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\Models\SemCorp\ProjectStatus;
use Auth;
use App\Http\Resources\SemCorp\ProjectStatusResource;
use Illuminate\Validation\Rule;
use DB;

class ProjectStatusController extends SEMCorp_APIController
{
    protected $moduleCode = "PROJECT_STATUS";
    protected $primaryKey = 'status_id';
    protected $uniqueColumnName = "status_code";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'status_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectStatusResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectStatus::orderBy('status_sequence', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectStatus::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        #DB::enableQueryLog();
        return ProjectStatus::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
        #dd(DB::getQueryLog());
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectStatus::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        ProjectStatus::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectStatus::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'status_code'	    => 'required|min:1|max:50|unique:status_projects,status_code',
            'status_info'	    => 'required|min:1|max:100',
            'status_sequence'   => 'required|integer|gt:0',
            'active_status'	    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'status_id'         => 'required|min:1|max:50|exists:status_projects,status_id',
            'status_code'	    => 'required|min:1|max:50',
            'status_info'	    => 'required|min:1|max:100',
            'status_sequence'   => 'required|integer|gt:0',
            'active_status'	    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "status_id"             => $request->status_id,
            "status_code"           => $request->status_code,
            "status_info"           => $request->status_info,
            "status_sequence"       => $request->status_sequence,
            "active_status"         => $request->active_status,
        ];
    }

}
