<?php

namespace App\Http\Controllers\SemCorp\Lookup;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\Models\SemCorp\DocumentCategory;
use Auth;
use App\Http\Resources\SemCorp\DocumentCategoryResource;
use Illuminate\Validation\Rule;
use DB;

class DocumentCategoryController extends SEMCorp_APIController
{
    protected $moduleCode = "DOCUMENT_CATEGORY";
    protected $primaryKey = 'document_category_id';
    protected $uniqueColumnName = "document_category";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'document_category',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DocumentCategoryResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return DocumentCategory::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return DocumentCategory::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return DocumentCategory::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return DocumentCategory::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        DocumentCategory::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        DocumentCategory::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_category'	    => 'required|min:1|max:50|unique:document_categories,document_category',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'document_category_id'      => 'required|exists:milestone_templates,document_category_id',
            'document_category'	        => 'required|min:1|max:50',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_category_id"      => $request->document_category_id,
            "document_category"         => $request->document_category,
        ];
    }

}
