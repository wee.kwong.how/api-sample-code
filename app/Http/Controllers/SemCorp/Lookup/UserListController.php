<?php

namespace App\Http\Controllers\SemCorp\Lookup;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\SemCorp\Procedure\ProjectUserListWithContractor;
use App\Http\Resources\SemCorp\UserListResource;
use DB;

class UserListController extends SEMCorp_APIController
{
    protected $userRole =
    [
        'Project_Owner'         => 8,
        'Project_Manager'       => 9,
        'Project_Engineer'      => 10,
        'Project_Lead'          => 11,
        'Contractor'            => 12,
        'Project_Member'        => 13,
        'Quality_Assurance'     => 14,
        'Engineer'              => 15,
        'Safety'                => 16,
        'Purchasing'            => 17,
        'O&M'                   => 18,
        'Planner'               => 19,
    ];

    public function __construct()
	{
        $this->initialParameterSetup();
	}

    // protected function uniqueColumnRequest($request)
    // {

    // }

    protected function makeResoucesCollection($data)
    {
        return UserListResource::Collection($data);
    }

    public function userList($userType)
    {
        $data = User::where('role_id', $this->userRole[$userType])->where('active_status', '1')->get();
        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function userListProjectGroup()
    {
        $data = User::whereIn('role_id', [9,10])->where('active_status', '1')->get();
        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function userListALL()
    {
        $data = User::OrderBy('role_id', 'ASC')->OrderBy('name', 'ASC')->get();
        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function projectUserListWithContractor($project_id, $contractor_id)
    {
        $model = new ProjectUserListWithContractor();

        $data = $model->hydrate(DB::select(" CALL `sp_get_user_list_from_project_with_contractor`( " . $project_id . " , " . $this->loginUserInfo['created_by'] . " , " . $contractor_id .") "));

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function projectUserList($project_id)
    {
        $model = new ProjectUserListWithContractor();

        $data = $model->hydrate(DB::select(" CALL `sp_get_user_list_from_project`( " . $project_id . " , " . $this->loginUserInfo['created_by'] . ") "));

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }


    public function getProjectTeamMember($project_id)
    {
        $model = new ProjectUserListWithContractor();

        $data = $model->hydrate(DB::select(" CALL `sp_get_team_member`( " . $project_id . " ) "));

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }


}
