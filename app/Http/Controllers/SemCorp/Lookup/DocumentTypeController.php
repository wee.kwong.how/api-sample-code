<?php

namespace App\Http\Controllers\SemCorp\Lookup;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\Models\SemCorp\DocumentType;
use Auth;
use App\Http\Resources\SemCorp\DocumentTypeResource;
use Illuminate\Validation\Rule;
use DB;

class DocumentTypeController extends SEMCorp_APIController
{
    protected $moduleCode = "DOCUMENT_TYPE";
    protected $primaryKey = 'document_type_id';
    protected $uniqueColumnName = "document_type_code";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'document_type_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DocumentTypeResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return DocumentType::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return DocumentType::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return DocumentType::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return DocumentType::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        DocumentType::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        DocumentType::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_type_code'	=> 'required|min:1|max:50|unique:milestone_templates,document_type_code',
            'document_type_info'	=> 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'document_type_id'      => 'required|exists:milestone_templates,document_type_id',
            'document_type_code'	=> 'required|min:1|max:50',
            'document_type_info'	=> 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_type_id"           => $request->document_type_id,
            "document_type_code"         => $request->document_type_code,
            "document_type_info"         => $request->document_type_info,
        ];
    }

}
