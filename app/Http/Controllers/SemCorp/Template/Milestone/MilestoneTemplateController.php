<?php

namespace App\Http\Controllers\SemCorp\Template\Milestone;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\MilestoneTemplate;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SemCorp\MilestoneTemplateResource;
use Illuminate\Validation\Rule;
use DB;

class MilestoneTemplateController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "MILESTONE_TEMPLATE";
    protected $primaryKey = 'milestone_template_id';
    protected $uniqueColumnName = "milestone_template_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'milestone_template_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return MilestoneTemplateResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return MilestoneTemplate::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return MilestoneTemplate::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return MilestoneTemplate::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return MilestoneTemplate::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = MilestoneTemplate::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->milestone_template_name = $request->milestone_template_name;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'milestone_templates');
    }

    protected function softDeleteRecord($idOrCode)
    {
        MilestoneTemplate::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'milestone_template_name'	=> 'required|min:1|max:100|unique:milestone_templates,milestone_template_name',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'milestone_template_id'     => 'required|exists:milestone_templates,milestone_template_id',
            'milestone_template_name'	=> 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "milestone_template_id"           => $request->milestone_template_id,
            "milestone_template_name"         => $request->milestone_template_name,
        ];
    }

}
