<?php

namespace App\Http\Controllers\SemCorp\Template\Milestone;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\MilestoneTemplate;
use App\Models\SemCorp\MilestoneTemplateDetail;

use App\Models\SemCorp\TaskTemplateDetail;
use App\Models\SemCorp\DocumentTemplateDetail;

use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SemCorp\BuildingResource;
use App\Http\Resources\SemCorp\MilestoneTemplateDetailResource;
use Illuminate\Validation\Rule;
use DB;

class MilestoneTemplateDetailController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "MILESTONE_TEMPLATE_DETAIL";
    protected $primaryKey = 'milestone_template_detail_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Milestone Details Name cannot be repeat in the Same Template.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'milestone_template_id',
            1 => 'milestone_template_title',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return MilestoneTemplateDetailResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return MilestoneTemplateDetail::orderBy('milestone_template_id', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return MilestoneTemplateDetail::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = MilestoneTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return MilestoneTemplateDetail::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return MilestoneTemplateDetail::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        $TaskTemplateDetailCount = TaskTemplateDetail::where($this->primaryKey, $idOrCode)->count();
        $DocumentTemplateDetailCount = DocumentTemplateDetail::where($this->primaryKey, $idOrCode)->count();

        if ( ( $TaskTemplateDetailCount == 0 ) AND ( $DocumentTemplateDetailCount == 0 ) )
        {
            return $validationCheck = "YES";
        }
        else
        {
            return $validationCheck = "NO";
        }
    }

    protected function insertNewRecord($newData)
    {
        return MilestoneTemplateDetail::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = MilestoneTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->milestone_template_id         = $request->milestone_template_id;
        $destinationData->milestone_template_title      = $request->milestone_template_title;
        $destinationData->milestone_template_sequence   = $request->milestone_template_sequence;
        $destinationData->active_status                 = $request->active_status;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'milestone_template_details');
    }

    protected function softDeleteRecord($idOrCode)
    {
        $MilestoneTemplateDetailData = MilestoneTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $MilestoneTemplateData = MilestoneTemplate::where('milestone_template_id', $MilestoneTemplateDetailData->milestone_template_id)->first();

        ## CHECK IF THE MILESTONE EXISTS in the SITE TASK & DOCUMENT.

        MilestoneTemplateDetail::where($this->primaryKey, $idOrCode)->delete();
        
        $MilestoneTemplateData->milestone_details_no = $MilestoneTemplateData->milestone_details_no - 1;

        $MilestoneTemplateData->save();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'milestone_template_id'             => 'required|exists:milestone_templates,milestone_template_id',
            'milestone_template_title'	        => 'required|min:1|max:100',
            'milestone_template_sequence'       => 'required|integer|gt:0',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'milestone_template_id'             => 'required|exists:milestone_templates,milestone_template_id',
            'milestone_template_title'	        => 'required|min:1|max:100',
            'milestone_template_sequence'       => 'required|integer|gt:0',
            'active_status'	                    => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "milestone_template_id"           => $request->milestone_template_id,
            "milestone_template_title"        => $request->milestone_template_title,
            "milestone_template_sequence"     => $request->milestone_template_sequence,
            "active_status"                   => $request->active_status,
        ];
    }

    public function showByTemplate($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $milestoneTemplateDetail = MilestoneTemplateDetail::where('milestone_template_id', $idOrCode)->get();

            if ( $milestoneTemplateDetail->count() >= 1 )
            {
                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($milestoneTemplateDetail), $this->successReadMessageResponse, $milestoneTemplateDetail[0]->_milestone_template->milestone_template_name );
            }
            else
            {
                $milestoneTemplate = MilestoneTemplate::where('milestone_template_id', $idOrCode)->first();

                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($milestoneTemplateDetail), $this->successReadMessageResponse, $milestoneTemplate->milestone_template_name );
            }
        }

        return response($this->response, $this->status);
    }

}
