<?php

namespace App\Http\Controllers\SemCorp\Template\ProjectDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\ProjectDocumentTemplate;
use App\Models\SemCorp\ProjectDocumentTemplateDetail;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SemCorp\BuildingResource;
use App\Http\Resources\SemCorp\ProjectDocumentTemplateDetailResource;
use Illuminate\Validation\Rule;
use DB;

class ProjectDocumentTemplateDetailController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_DOCUMENT_TEMPLATE_DETAIL";
    protected $primaryKey = 'project_document_template_detail_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Project Document Title Code cannot be repeat in the Same Template.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_document_template_id',
            1 => 'project_document_template_title',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectDocumentTemplateDetailResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectDocumentTemplateDetail::orderBy('project_document_template_title', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectDocumentTemplateDetail::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = ProjectDocumentTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return ProjectDocumentTemplateDetail::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return ProjectDocumentTemplateDetail::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectDocumentTemplateDetail::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectDocumentTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_document_template_id          = $request->project_document_template_id;
        $destinationData->project_document_template_title       = $request->project_document_template_title;
        $destinationData->project_document_template_mandatory   = $request->project_document_template_mandatory;
        $destinationData->document_type_id                      = $request->document_type_id;
        $destinationData->document_category_id                  = $request->document_category_id;
        $destinationData->recurring_interval_id                 = $request->recurring_interval_id;
        $destinationData->active_status                         = $request->active_status;
        $destinationData->req_approval_project_owner            = $request->req_approval_project_owner;
        $destinationData->req_approval_project_manager          = $request->req_approval_project_manager;
        $destinationData->req_approval_project_engineer         = $request->req_approval_project_engineer;
        $destinationData->req_approval_engineer                 = $request->req_approval_engineer;
        $destinationData->req_approval_qa_qc                    = $request->req_approval_qa_qc;
        $destinationData->req_approval_onm                      = $request->req_approval_onm;
        $destinationData->req_approval_safety                   = $request->req_approval_safety;
        $destinationData->req_approval_planner                  = $request->req_approval_planner;
        $destinationData->req_approval_purchasing               = $request->req_approval_purchasing;
        $destinationData->req_approval_admin                    = $request->req_approval_admin;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_document_template_details');
    }

    protected function softDeleteRecord($idOrCode)
    {
        $ProjectDocumentTemplateDetail = ProjectDocumentTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $ProjectDocumentTemplate = ProjectDocumentTemplate::where('project_document_template_id', $ProjectDocumentTemplateDetail->project_document_template_id)->first();

        ProjectDocumentTemplateDetail::where($this->primaryKey, $idOrCode)->delete();

        $ProjectDocumentTemplate->document_details_no = $ProjectDocumentTemplate->document_details_no - 1;

        $ProjectDocumentTemplate->save();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'project_document_template_id'              => 'required|exists:project_document_templates,project_document_template_id',
            'project_document_template_title'	        => 'required|min:1|max:100',
            'project_document_template_mandatory'	    => 'required|min:1|max:1|boolean',
            'document_category_id'                      => 'required|exists:document_categories,document_category_id',
            'document_type_id'                          => 'required|exists:document_types,document_type_id',
            'recurring_interval_id'                     => 'nullable|exists:recurring_intervals,recurring_interval_id',
            'active_status'	                            => 'required|min:1|max:1|boolean',
            'req_approval_project_owner'                => 'required|min:1|max:1|boolean',
            'req_approval_project_manager'              => 'required|min:1|max:1|boolean',
            'req_approval_project_engineer'             => 'required|min:1|max:1|boolean',
            'req_approval_engineer'                     => 'required|min:1|max:1|boolean',
            'req_approval_qa_qc'                        => 'required|min:1|max:1|boolean',
            'req_approval_onm'                          => 'required|min:1|max:1|boolean',
            'req_approval_safety'                       => 'required|min:1|max:1|boolean',
            'req_approval_planner'                      => 'required|min:1|max:1|boolean',
            'req_approval_purchasing'                   => 'required|min:1|max:1|boolean',
            'req_approval_admin'                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_document_template_id'              => 'required|exists:project_document_templates,project_document_template_id',
            'project_document_template_title'	        => 'required|min:1|max:100',
            'project_document_template_mandatory'	    => 'required|min:1|max:1|boolean',
            'document_category_id'                      => 'required|exists:document_categories,document_category_id',
            'document_type_id'                          => 'required|exists:document_types,document_type_id',
            'recurring_interval_id'                     => 'nullable|exists:recurring_intervals,recurring_interval_id',
            'active_status'	                            => 'required|min:1|max:1|boolean',
            'req_approval_project_owner'                => 'required|min:1|max:1|boolean',
            'req_approval_project_manager'              => 'required|min:1|max:1|boolean',
            'req_approval_project_engineer'             => 'required|min:1|max:1|boolean',
            'req_approval_engineer'                     => 'required|min:1|max:1|boolean',
            'req_approval_qa_qc'                        => 'required|min:1|max:1|boolean',
            'req_approval_onm'                          => 'required|min:1|max:1|boolean',
            'req_approval_safety'                       => 'required|min:1|max:1|boolean',
            'req_approval_planner'                      => 'required|min:1|max:1|boolean',
            'req_approval_purchasing'                   => 'required|min:1|max:1|boolean',
            'req_approval_admin'                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_document_template_id"          => $request->project_document_template_id,
            "project_document_template_title"       => $request->project_document_template_title,
            "project_document_template_mandatory"   => $request->project_document_template_mandatory,
            "document_category_id"                  => $request->document_category_id,
            "document_type_id"                      => $request->document_type_id,
            "recurring_interval_id"                 => $request->recurring_interval_id,
            "active_status"                         => $request->active_status,
            "req_approval_project_owner"            => $request->req_approval_project_owner,
            "req_approval_project_manager"          => $request->req_approval_project_manager,
            "req_approval_project_engineer"         => $request->req_approval_project_engineer,
            "req_approval_engineer"                 => $request->req_approval_engineer,
            "req_approval_qa_qc"                    => $request->req_approval_qa_qc,
            "req_approval_onm"                      => $request->req_approval_onm,
            "req_approval_safety"                   => $request->req_approval_safety,
            "req_approval_planner"                  => $request->req_approval_planner,
            "req_approval_purchasing"               => $request->req_approval_purchasing,
            "req_approval_admin"                    => $request->req_approval_admin,
        ];
    }

    public function showByTemplate($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $DocumentTemplateDetail = ProjectDocumentTemplateDetail::where('project_document_template_id', $idOrCode)->get();

            if ( $DocumentTemplateDetail->count() >= 1 )
            {
                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($DocumentTemplateDetail), $this->successReadMessageResponse, $DocumentTemplateDetail[0]->_project_document_template->document_template_name );
            }
            else
            {
                $DocumentTemplate = ProjectDocumentTemplate::where('project_document_template_id', $idOrCode)->first();

                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($DocumentTemplateDetail), $this->successReadMessageResponse, $DocumentTemplate->document_template_name );
            }
        }


        return response($this->response, $this->status);
    }

}
