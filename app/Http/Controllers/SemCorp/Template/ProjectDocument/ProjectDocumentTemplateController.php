<?php

namespace App\Http\Controllers\SemCorp\Template\ProjectDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\ProjectDocumentTemplate;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SemCorp\ProjectDocumentTemplateResource;
use Illuminate\Validation\Rule;
use DB;

class ProjectDocumentTemplateController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_DOCUMENT_TEMPLATE";
    protected $primaryKey = 'project_document_template_id';
    protected $uniqueColumnName = "document_template_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'document_template_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectDocumentTemplateResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectDocumentTemplate::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectDocumentTemplate::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectDocumentTemplate::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectDocumentTemplate::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectDocumentTemplate::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_document_template_id  = $request->project_document_template_id;
        $destinationData->document_template_name        = $request->document_template_name;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_document_templates');
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectDocumentTemplate::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_template_name'	=> 'required|min:1|max:100|unique:project_document_templates,document_template_name',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'project_document_template_id'      => 'required|exists:project_document_templates,project_document_template_id',
            'document_template_name'	        => 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_document_template_id"   => $request->project_document_template_id,
            "document_template_name"         => $request->document_template_name,
        ];
    }

    public function documentDetailCount($idOrCode)
    {
        $countDetails = ProjectDocumentTemplateDetail::where('project_document_template_id', $idOrCode)->get();

        return $countDetails->count();
    }

    public function milestoneDetailCount($idOrCode)
    {
        $ProjectDocumentTemplateDetail = ProjectDocumentTemplate::where('project_document_template_id', $idOrCode)->first();

        $countDetails = MilestoneTemplateDetail::where('milestone_template_id', $ProjectDocumentTemplateDetail->milestone_template_id)->get();

        return $countDetails->count();
    }

}
