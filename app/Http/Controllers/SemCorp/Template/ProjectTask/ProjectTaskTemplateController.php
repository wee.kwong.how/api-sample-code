<?php

namespace App\Http\Controllers\SemCorp\Template\ProjectTask;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectTaskTemplate;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SemCorp\ProjectTaskTemplateResource;
use Illuminate\Validation\Rule;
use DB;

class ProjectTaskTemplateController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_TASK_TEMPLATE";
    protected $primaryKey = 'project_task_template_id';
    protected $uniqueColumnName = "task_template_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'task_template_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectTaskTemplateResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectTaskTemplate::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectTaskTemplate::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectTaskTemplate::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectTaskTemplate::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectTaskTemplate::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_task_template_id  = $request->project_task_template_id;
        $destinationData->task_template_name        = $request->task_template_name;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_task_templates');
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectTaskTemplate::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'task_template_name'	    => 'required|min:1|max:100|unique:project_task_templates,task_template_name',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'project_task_template_id'  => 'required|exists:project_task_templates,project_task_template_id',
            'task_template_name'	    => 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_task_template_id"   => $request->project_task_template_id,
            "task_template_name"         => $request->task_template_name,
        ];
    }

    public function TaskDetailCount($idOrCode)
    {
        $countDetails = TaskTemplateDetail::where('project_task_template_id', $idOrCode)->get();

        return $countDetails->count();
    }

    public function milestoneDetailCount($idOrCode)
    {
        $TaskTemplateDetail = TaskTemplate::where('project_task_template_id', $idOrCode)->first();

        $countDetails = MilestoneTemplateDetail::where('milestone_template_id', $TaskTemplateDetail->milestone_template_id)->get();

        return $countDetails->count();
    }

}
