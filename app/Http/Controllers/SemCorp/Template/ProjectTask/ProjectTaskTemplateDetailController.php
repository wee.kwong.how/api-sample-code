<?php

namespace App\Http\Controllers\SemCorp\Template\ProjectTask;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\ProjectTaskTemplate;
use App\Models\SemCorp\ProjectTaskTemplateDetail;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SemCorp\BuildingResource;
use App\Http\Resources\SemCorp\ProjectTaskTemplateDetailResource;
use Illuminate\Validation\Rule;
use DB;

class ProjectTaskTemplateDetailController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_TASK_TEMPLATE_DETAIL";
    protected $primaryKey = 'project_task_template_detail_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Project Task Title Code cannot be repeat in the Same Template.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_task_template_id',
            1 => 'project_task_title',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectTaskTemplateDetailResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectTaskTemplateDetail::orderBy('project_task_title', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectTaskTemplateDetail::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = ProjectTaskTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return ProjectTaskTemplateDetail::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return ProjectTaskTemplateDetail::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectTaskTemplateDetail::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectTaskTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->project_task_template_id      = $request->project_task_template_id;
        $destinationData->project_task_title            = $request->project_task_title;
        $destinationData->project_task_description      = $request->project_task_description;
        $destinationData->status_id                     = $request->status_id;
        $destinationData->active_status                 = $request->active_status;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_task_template_details');
    }

    protected function softDeleteRecord($idOrCode)
    {
        
        $ProjectTaskTemplateDetailData = ProjectTaskTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $ProjectTaskTemplateData = ProjectTaskTemplate::where('project_task_template_id', $ProjectTaskTemplateDetailData->project_task_template_id)->first();

        ProjectTaskTemplateDetail::where($this->primaryKey, $idOrCode)->delete();

        $ProjectTaskTemplateData->task_details_no = $ProjectTaskTemplateData->task_details_no - 1;

        $ProjectTaskTemplateData->save();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_task_template_id'              => 'required|exists:project_task_templates,project_task_template_id',
            'project_task_title'	                => 'required|min:1|max:100',
            'project_task_description'              => 'required|min:1|max:300',
            'status_id'                             => 'required|exists:status_tasks,status_id',
            'active_status'	                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_task_template_id'              => 'required|exists:project_task_templates,project_task_template_id',
            'project_task_title'	                => 'required|min:1|max:100',
            'project_task_description'              => 'required|min:1|max:300',
            'status_id'                             => 'required|exists:status_tasks,status_id',
            'active_status'	                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_task_template_id"          => $request->project_task_template_id,
            "project_task_title"                => $request->project_task_title,
            "project_task_description"          => $request->project_task_description,
            "status_id"                         => $request->status_id,
            "active_status"                     => $request->active_status,
        ];
    }

    public function showByTemplate($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $TaskTemplateDetail = ProjectTaskTemplateDetail::where('project_task_template_id', $idOrCode)->get();

            if ( $TaskTemplateDetail->count() >= 1 )
            {
                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($TaskTemplateDetail), $this->successReadMessageResponse, $TaskTemplateDetail[0]->_project_task_template->task_template_name );
            }
            else
            {
                $TaskTemplate = ProjectTaskTemplate::where('project_task_template_id', $idOrCode)->first();

                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($TaskTemplateDetail), $this->successReadMessageResponse, $TaskTemplate->task_template_name );
            }
        }

        return response($this->response, $this->status);
    }

}
