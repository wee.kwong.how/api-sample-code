<?php

namespace App\Http\Controllers\SemCorp\Template\Task;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\TaskTemplate;
use App\Models\SemCorp\TaskTemplateDetail;
use App\Models\SemCorp\MilestoneTemplateDetail;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SemCorp\TaskTemplateResource;
use Illuminate\Validation\Rule;
use DB;

class TaskTemplateController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "TASK_TEMPLATE";
    protected $primaryKey = 'task_template_id';
    protected $uniqueColumnName = "task_template_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'task_template_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return TaskTemplateResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return TaskTemplate::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return TaskTemplate::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return TaskTemplate::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return TaskTemplate::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = TaskTemplate::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->task_template_id          = $request->task_template_id;
        $destinationData->task_template_name        = $request->task_template_name;
        $destinationData->milestone_template_id     = $request->milestone_template_id;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'task_templates');
    }

    protected function softDeleteRecord($idOrCode)
    {
        TaskTemplate::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'task_template_name'	    => 'required|min:1|max:100|unique:task_templates,task_template_name',
            'milestone_template_id'     => 'required|exists:milestone_templates,milestone_template_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_template_id'          => 'required|exists:task_templates,task_template_id',
            'milestone_template_id'     => 'required|exists:milestone_templates,milestone_template_id',
            'task_template_name'	    => 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "task_template_id"           => $request->task_template_id,
            "task_template_name"         => $request->task_template_name,
            "milestone_template_id"      => $request->milestone_template_id,
        ];
    }

    public function TaskDetailCount($idOrCode)
    {
        $countDetails = TaskTemplateDetail::where('task_template_id', $idOrCode)->get();

        return $countDetails->count();
    }

    public function milestoneDetailCount($idOrCode)
    {
        $TaskTemplateDetail = TaskTemplate::where('task_template_id', $idOrCode)->first();

        $countDetails = MilestoneTemplateDetail::where('milestone_template_id', $TaskTemplateDetail->milestone_template_id)->get();

        return $countDetails->count();
    }

}
