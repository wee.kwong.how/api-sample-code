<?php

namespace App\Http\Controllers\SemCorp\Template\Task;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\TaskTemplate;
use App\Models\SemCorp\TaskTemplateDetail;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SemCorp\BuildingResource;
use App\Http\Resources\SemCorp\TaskTemplateDetailResource;
use Illuminate\Validation\Rule;
use DB;

class TaskTemplateDetailController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "TASK_TEMPLATE_DETAIL";
    protected $primaryKey = 'task_template_detail_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Task Title Details cannot be repeat in the Same Template.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'task_template_id',
            1 => 'task_template_title',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return TaskTemplateDetailResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return TaskTemplateDetail::orderBy('task_template_id', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return TaskTemplateDetail::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = TaskTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return TaskTemplateDetail::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return TaskTemplateDetail::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return TaskTemplateDetail::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = TaskTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->task_template_detail_id       = $request->task_template_detail_id;
        $destinationData->task_template_title           = $request->task_template_title;
        $destinationData->task_description              = $request->task_description;
        $destinationData->status_id                     = $request->status_id;
        $destinationData->milestone_template_detail_id  = $request->milestone_template_detail_id;
        $destinationData->active_status                 = $request->active_status;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'task_template_details');
    }

    protected function softDeleteRecord($idOrCode)
    {
        $TaskTemplateDetailData = TaskTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $TaskTemplateData = TaskTemplate::where('task_template_id', $TaskTemplateDetailData->task_template_id)->first();

        TaskTemplateDetail::where($this->primaryKey, $idOrCode)->delete();

        $TaskTemplateData->task_details_no = $TaskTemplateData->task_details_no - 1;

        $TaskTemplateData->save();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_template_id'                      => 'required|exists:task_templates,task_template_id',
            'task_template_title'	                => 'required|min:1|max:100',
            'task_description'                      => 'required|min:1|max:300',
            'status_id'                             => 'required|exists:status_tasks,status_id',
            'milestone_template_detail_id'          => 'required|exists:milestone_template_details,milestone_template_detail_id',
            'active_status'	                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'task_template_id'                      => 'required|exists:task_templates,task_template_id',
            'task_template_title'	                => 'required|min:1|max:100',
            'task_description'                      => 'required|min:1|max:300',
            'status_id'                             => 'required|exists:status_tasks,status_id',
            'milestone_template_detail_id'          => 'required|exists:milestone_template_details,milestone_template_detail_id',
            'active_status'	                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "task_template_id"                  => $request->task_template_id,
            "task_template_title"               => $request->task_template_title,
            "task_description"                  => $request->task_description,
            "status_id"                         => $request->status_id,
            "milestone_template_detail_id"      => $request->milestone_template_detail_id,
            "active_status"                     => $request->active_status,
        ];
    }

    public function showByTemplate($idOrCode)
    {
        $TaskTemplateDetail = TaskTemplateDetail::where('task_template_id', $idOrCode)->get();

        if ( $TaskTemplateDetail->count() >= 1 )
        {
            $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($TaskTemplateDetail), $this->successReadMessageResponse, $TaskTemplateDetail[0]->_task_template->task_template_name );
        }
        else
        {
            $TaskTemplate = TaskTemplate::where('task_template_id', $idOrCode)->first();

            $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($TaskTemplateDetail), $this->successReadMessageResponse, $TaskTemplate->task_template_name );
        }

        return response($this->response, $this->status);
    }

}
