<?php

namespace App\Http\Controllers\SemCorp\Template\Document;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\DocumentTemplate;
use App\Models\SemCorp\DocumentTemplateDetail;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SemCorp\BuildingResource;
use App\Http\Resources\SemCorp\DocumentTemplateDetailResource;
use Illuminate\Validation\Rule;
use DB;

class DocumentTemplateDetailController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "DOCUMENT_TEMPLATE_DETAIL";
    protected $primaryKey = 'document_template_detail_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Document Title cannot be repeat in the Same Template.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'document_template_id',
            1 => 'document_template_title',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DocumentTemplateDetailResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return DocumentTemplateDetail::orderBy('document_template_title', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return DocumentTemplateDetail::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = DocumentTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return DocumentTemplateDetail::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        {
            return DocumentTemplateDetail::where($this->primaryKey, '<>', $idOrCode)
            ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
            ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
            ->get();
        }
        else
        {
            return collect([ 0 ]);
        }
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return DocumentTemplateDetail::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = DocumentTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->document_template_id                  = $request->document_template_id;
        $destinationData->document_template_title               = $request->document_template_title;
        $destinationData->document_template_mandatory           = $request->document_template_mandatory;
        $destinationData->document_type_id                      = $request->document_type_id;
        $destinationData->document_category_id                  = $request->document_category_id;
        $destinationData->recurring_interval_id                 = $request->recurring_interval_id;
        $destinationData->milestone_template_detail_id          = $request->milestone_template_detail_id;
        $destinationData->active_status                         = $request->active_status;
        $destinationData->req_approval_project_owner            = $request->req_approval_project_owner;
        $destinationData->req_approval_project_manager          = $request->req_approval_project_manager;
        $destinationData->req_approval_project_engineer         = $request->req_approval_project_engineer;
        $destinationData->req_approval_engineer                 = $request->req_approval_engineer;
        $destinationData->req_approval_qa_qc                    = $request->req_approval_qa_qc;
        $destinationData->req_approval_onm                      = $request->req_approval_onm;
        $destinationData->req_approval_safety                   = $request->req_approval_safety;
        $destinationData->req_approval_planner                  = $request->req_approval_planner;
        $destinationData->req_approval_purchasing               = $request->req_approval_purchasing;
        $destinationData->req_approval_admin                    = $request->req_approval_admin;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'document_template_details');
    }

    protected function softDeleteRecord($idOrCode)
    {

        $DocumentTemplateDetailData = DocumentTemplateDetail::where($this->primaryKey, $idOrCode)->first();

        $DocumentTemplateData = DocumentTemplate::where('document_template_id', $DocumentTemplateDetailData->document_template_id)->first();

        DocumentTemplateDetail::where($this->primaryKey, $idOrCode)->delete();

        $DocumentTemplateData->document_details_no = $DocumentTemplateData->document_details_no - 1;

        $DocumentTemplateData->save();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'document_template_id'                      => 'required|exists:document_templates,document_template_id',
            'document_template_title'	                => 'required|min:1|max:100',
            'document_template_mandatory'	            => 'required|min:1|max:1|boolean',
            'document_type_id'                          => 'required|exists:document_types,document_type_id',
            'document_category_id'                      => 'required|exists:document_categories,document_category_id',
            'recurring_interval_id'                     => 'nullable|exists:recurring_intervals,recurring_interval_id',
            'milestone_template_detail_id'              => 'required|exists:milestone_template_details,milestone_template_detail_id',
            'active_status'	                            => 'required|min:1|max:1|boolean',
            'req_approval_project_owner'                => 'required|min:1|max:1|boolean',
            'req_approval_project_manager'              => 'required|min:1|max:1|boolean',
            'req_approval_project_engineer'             => 'required|min:1|max:1|boolean',
            'req_approval_engineer'                     => 'required|min:1|max:1|boolean',
            'req_approval_qa_qc'                        => 'required|min:1|max:1|boolean',
            'req_approval_onm'                          => 'required|min:1|max:1|boolean',
            'req_approval_safety'                       => 'required|min:1|max:1|boolean',
            'req_approval_planner'                      => 'required|min:1|max:1|boolean',
            'req_approval_purchasing'                   => 'required|min:1|max:1|boolean',
            'req_approval_admin'                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'document_template_id'                      => 'required|exists:document_templates,document_template_id',
            'document_template_title'	                => 'required|min:1|max:100',
            'document_template_mandatory'	            => 'required|min:1|max:1|boolean',
            'document_type_id'                          => 'required|exists:document_types,document_type_id',
            'document_category_id'                      => 'required|exists:document_categories,document_category_id',
            'recurring_interval_id'                     => 'nullable|exists:recurring_intervals,recurring_interval_id',
            'milestone_template_detail_id'              => 'required|exists:milestone_template_details,milestone_template_detail_id',
            'active_status'	                            => 'required|min:1|max:1|boolean',
            'req_approval_project_owner'                => 'required|min:1|max:1|boolean',
            'req_approval_project_manager'              => 'required|min:1|max:1|boolean',
            'req_approval_project_engineer'             => 'required|min:1|max:1|boolean',
            'req_approval_engineer'                     => 'required|min:1|max:1|boolean',
            'req_approval_qa_qc'                        => 'required|min:1|max:1|boolean',
            'req_approval_onm'                          => 'required|min:1|max:1|boolean',
            'req_approval_safety'                       => 'required|min:1|max:1|boolean',
            'req_approval_planner'                      => 'required|min:1|max:1|boolean',
            'req_approval_purchasing'                   => 'required|min:1|max:1|boolean',
            'req_approval_admin'                        => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_template_id"                  => $request->document_template_id,
            "document_template_title"               => $request->document_template_title,
            "document_template_mandatory"           => $request->document_template_mandatory,
            "document_type_id"                      => $request->document_type_id,
            "document_category_id"                  => $request->document_category_id,
            "recurring_interval_id"                 => $request->recurring_interval_id,
            "milestone_template_detail_id"          => $request->milestone_template_detail_id,
            "active_status"                         => $request->active_status,
            "req_approval_project_owner"            => $request->req_approval_project_owner,
            "req_approval_project_manager"          => $request->req_approval_project_manager,
            "req_approval_project_engineer"         => $request->req_approval_project_engineer,
            "req_approval_engineer"                 => $request->req_approval_engineer,
            "req_approval_qa_qc"                    => $request->req_approval_qa_qc,
            "req_approval_onm"                      => $request->req_approval_onm,
            "req_approval_safety"                   => $request->req_approval_safety,
            "req_approval_planner"                  => $request->req_approval_planner,
            "req_approval_purchasing"               => $request->req_approval_purchasing,
            "req_approval_admin"                    => $request->req_approval_admin,
        ];
    }

    public function showByTemplate($idOrCode)
    {

        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $DocumentTemplateDetail = DocumentTemplateDetail::where('document_template_id', $idOrCode)->get();

            if ( $DocumentTemplateDetail->count() >= 1 )
            {
                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($DocumentTemplateDetail), $this->successReadMessageResponse, $DocumentTemplateDetail[0]->_document_template->document_template_name );
            }
            else
            {
                $DocumentTemplate = DocumentTemplate::where('document_template_id', $idOrCode)->first();

                $this->response = $this->apiResponse->makeResponseDataTitle($this->makeResoucesCollection($DocumentTemplateDetail), $this->successReadMessageResponse, $DocumentTemplate->document_template_name );
            }
        }

        return response($this->response, $this->status);
    }

}
