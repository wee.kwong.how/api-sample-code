<?php

namespace App\Http\Controllers\SemCorp\Template\Document;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SaaS\Cluster;
use App\Models\SaaS\Building;
use App\Models\SemCorp\DocumentTemplate;
use App\Models\SemCorp\DocumentTemplateDetail;
use App\Models\SemCorp\MilestoneTemplateDetail;
use App\Models\SemCorp\AuditLogTemplate;
use Auth;
use App\Http\Resources\SaaS\BuildingResource;
use App\Http\Resources\SemCorp\DocumentTemplateResource;
use Illuminate\Validation\Rule;
use DB;

class DocumentTemplateController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "DOCUMENT_TEMPLATE";
    protected $primaryKey = 'document_template_id';
    protected $uniqueColumnName = "document_template_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'document_template_name',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DocumentTemplateResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return DocumentTemplate::orderBy('created_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return DocumentTemplate::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return DocumentTemplate::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return DocumentTemplate::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTemplate::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = DocumentTemplate::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->document_template_id      = $request->document_template_id;
        $destinationData->milestone_template_id     = $request->milestone_template_id;
        $destinationData->document_template_name    = $request->document_template_name;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'document_templates');
    }

    protected function softDeleteRecord($idOrCode)
    {
        DocumentTemplate::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_template_name'	=> 'required|min:1|max:100|unique:document_templates,document_template_name',
            'milestone_template_id'     => 'required|exists:milestone_templates,milestone_template_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
			'document_template_id'      => 'required|exists:document_templates,document_template_id',
            'milestone_template_id'     => 'required|exists:milestone_templates,milestone_template_id',
            'document_template_name'	=> 'required|min:1|max:100',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_template_id"           => $request->document_template_id,
            "document_template_name"         => $request->document_template_name,
            "milestone_template_id"          => $request->milestone_template_id,
        ];
    }

    public function documentDetailCount($idOrCode)
    {
        $countDetails = DocumentTemplateDetail::where('document_template_id', $idOrCode)->get();

        return $countDetails->count();
    }

    public function milestoneDetailCount($idOrCode)
    {
        $DocumentTemplateDetail = DocumentTemplate::where('document_template_id', $idOrCode)->first();

        $countDetails = MilestoneTemplateDetail::where('milestone_template_id', $DocumentTemplateDetail->milestone_template_id)->get();

        return $countDetails->count();
    }

}
