<?php

namespace App\Http\Controllers\SemCorp\Document;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\DocumentTemplate;
use App\Models\SemCorp\Document;
use App\Models\SemCorp\View\ProjectGroupSiteDocumentView;
use Auth;
use App\Http\Resources\SemCorp\DocumentResource;
use Illuminate\Validation\Rule;
use DB;

class DocumentController extends SEMCorp_APIController
{
    protected $moduleCode = "DOCUMENT";
    protected $primaryKey = 'document_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'milestone_id',
            1 => 'site_id',
            2 => 'document_classification_code',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
            $this->uniqueCombo[2] => request()->input($this->uniqueCombo[2]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DocumentResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return Document::orderBy('updated_at', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return Document::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = DocumentTemplate::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        //dd($validationCheck);

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        ->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->primaryKey, '<>', $idOrCode)
        //     ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        return Document::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return Document::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        Document::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        Document::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_id'                        => 'required|exists:projects,project_id',
            'site_id'                           => 'required|exists:project_sites,site_id',
            'document_classification_code'	    => 'required|min:1|max:50',
            'document_information'	            => 'required|min:1|max:100',
            'document_mandatory'		        => 'required|min:1|max:1|boolean',
            'document_type_id'                  => 'required|exists:document_types,document_type_id',
            'milestone_id'                      => 'required|exists:project_milestones,milestone_id',
            'status_id'                         => 'required|exists:status_documents,status_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_id'                       => 'required|exists:documents,document_id',
            'project_id'                        => 'required|exists:projects,project_id',
            'site_id'                           => 'required|exists:project_sites,site_id',
            'document_classification_code'	    => 'required|min:1|max:50',
            'document_information'	            => 'required|min:1|max:100',
            'document_mandatory'		        => 'required|min:1|max:1|boolean',
            'document_type_id'                  => 'required|exists:document_types,document_type_id',
            'milestone_id'                      => 'required|exists:project_milestones,milestone_id',
            'status_id'                         => 'required|exists:status_documents,status_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_id"                       => $request->document_id,
            "project_id"                        => $request->project_id,
            "site_id"                           => $request->site_id,
            "document_classification_code"      => $request->document_classification_code,
            "document_information"              => $request->document_information,
            "document_mandatory"                => $request->document_mandatory,
            "document_type_id"                  => $request->document_type_id,
            "milestone_id"                      => $request->milestone_id,
            "status_id"                         => $request->status_id,
        ];
    }

    public function showDocumentBySite($idOrCode)
    {
        $data = Document::where('site_id', $idOrCode)->Orderby('updated_at', 'DESC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showDocumentByGroup($idOrCode)
    {
        $data   = ProjectGroupSiteDocumentView::where('group_id', $idOrCode)->Orderby('updated_at', 'DESC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showDocumentProject($idOrCode)
    {
        $data   = ProjectGroupSiteDocumentView::where('project_id', $idOrCode)->Orderby('updated_at', 'DESC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }


}
