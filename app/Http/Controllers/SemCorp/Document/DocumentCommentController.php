<?php

namespace App\Http\Controllers\SemCorp\Document;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\DocumentTemplate;
use App\Models\SemCorp\DocumentComment;
use App\Models\SemCorp\View\ProjectGroupSiteDocumentView;
use Auth;
use App\Http\Resources\SemCorp\DocumentCommentResource;
use Illuminate\Validation\Rule;
use DB;

class DocumentCommentController extends SEMCorp_APIController
{
    protected $moduleCode = "DOCUMENT_COMMENT";
    protected $primaryKey = 'document_comment_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "NO";
    protected $uniqueColumnRequest = [];

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [

        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [

        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DocumentCommentResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return DocumentComment::orderBy('document_comment_sequence', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return DocumentComment::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        //dd($validationCheck);

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        // return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        // ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        // ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        // ->get();

        return collect([ 0 ]);
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->primaryKey, '<>', $idOrCode)
        //     ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        // return Document::where($this->primaryKey, '<>', $idOrCode)
        // ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        // ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        // ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        // ->get();
        return collect([ 0 ]);
        
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return DocumentComment::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        DocumentComment::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        DocumentComment::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_detail_id'                => 'required|exists:document_details,document_detail_id',
            'document_comment'	                => 'required|min:1|max:1000',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_comment_id'               => 'required|exists:document_comments,document_comment_id',
            'document_detail_id'                => 'required|exists:document_details,document_detail_id',
            'document_comment'	                => 'required|min:1|max:1000',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_comment_id"               => $request->document_comment_id,
            "document_detail_id"                => $request->document_detail_id,
            "document_comment"                  => $request->document_comment,
        ];
    }

    public function showDocumentCommentByDocumentDetail($idOrCode)
    {
        $data = DocumentComment::where('document_detail_id', $idOrCode)->Orderby('document_comment_sequence', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showDocumentByGroup($idOrCode)
    {
        $data   = ProjectGroupSiteDocumentView::where('group_id', $idOrCode)->Orderby('document_id', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showDocumentProject($idOrCode)
    {
        $data   = ProjectGroupSiteDocumentView::where('project_id', $idOrCode)->Orderby('document_id', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }


}
