<?php

namespace App\Http\Controllers\SemCorp\Document;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\Document;
use App\Models\SemCorp\DocumentDetail;
use App\Models\SemCorp\View\ProjectGroupSiteDocumentView;
use Auth;
use App\Http\Resources\SemCorp\DocumentDetailResource;
use Illuminate\Validation\Rule;
use DB;
use File;

class DocumentDetailsController extends SEMCorp_APIController
{
    protected $moduleCode = "DOCUMENT_DETAILS";
    protected $primaryKey = 'document_detail_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "NO";
    protected $uniqueColumnRequest = [];

    protected $backend_path = '/upload_file/';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [

        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [

        ];
    }

    protected function makeResoucesCollection($data)
    {
        return DocumentDetailResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return DocumentDetail::orderBy('document_version', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return DocumentDetail::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        //dd($validationCheck);

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        // return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        // ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        // ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        // ->get();

        return collect([ 0 ]);
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->primaryKey, '<>', $idOrCode)
        //     ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        return Document::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return DocumentDetail::insertGetId($newData);
    }

    protected function UpdateRecord($updateData, $idOrCode)
    {
        DocumentDetail::where($this->primaryKey, $idOrCode)->update($updateData);
    }

    protected function softDeleteRecord($idOrCode)
    {
        DocumentDetail::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_id'                       => 'required|exists:documents,document_id',
            // 'site_id'                           => 'required|exists:project_sites,site_id',
            'document_information'	            => 'required|min:1|max:300',
            'document_file'                     => 'nullable|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'document_id'                       => 'required|exists:documents,document_id',
            // 'site_id'                           => 'required|exists:project_sites,site_id',
            'document_information'	            => 'required|min:1|max:300',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        if ($request->hasFile('document_file')) {

            $image = $request->file('document_file');

            $imgName = $request->document_id . '_' . time() . '_' . $image->getClientOriginalName();

            $location = storage_path($this->backend_path); //move file to ftp file

            $image->move($location, $imgName);

         }

        $additionalDataSet =
        [
            "document_file"         => $imgName,
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_id"                       => $request->document_id,
            "document_detail_id"                => $request->document_detail_id,
            "document_information"              => $request->document_information,
        ];
    }

    public function store(Request $request)
    {
        $this->uniqueColumnRequest($request);

        $this->validationStoreRequest($request);

        if ( $this->uniqueOrNotSetting == "COMBO" )
        {
            if ( $this->getUniqueComboInsertCheck($this->uniqueColumnRequest)->count() == 0 )
            {
                $this->createNewRecord($request, $this->loginUserInfo['created_by']);
            }
            else
            {
                $this->response = $this->apiResponse->setColumnNotUniqueCombo();
                $this->status = config('response.status.error');
            }
        }
        else
        {
            $this->createNewRecord($request, $this->loginUserInfo['created_by']);

            $document_data =
            [
                "status_id" => $request->status_id,
            ];

            Document::where('document_id', $request->document_id)->update($document_data);
        }

        return response($this->response, $this->status);
    }

    public function showDocumentDetailByDocument($idOrCode)
    {
        $data = DocumentDetail::where('document_id', $idOrCode)->Orderby('document_version', 'DESC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showDocumentByGroup($idOrCode)
    {
        $data   = ProjectGroupSiteDocumentView::where('group_id', $idOrCode)->Orderby('document_id', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function showDocumentProject($idOrCode)
    {
        $data   = ProjectGroupSiteDocumentView::where('project_id', $idOrCode)->Orderby('document_id', 'ASC')->get();

        $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        return response($this->response, $this->status);
    }

    public function downloadDocument($document_detail_id)
    {
        $document_detail =  DocumentDetail::where('document_detail_id', $document_detail_id)->first();

        $path = storage_path('/upload_file/'.$document_detail->document_file);

        return response()->download($path);
    }


}
