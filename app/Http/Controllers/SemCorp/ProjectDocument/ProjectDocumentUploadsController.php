<?php

namespace App\Http\Controllers\SemCorp\ProjectDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectDocumentTemplate;
use App\Models\SemCorp\ProjectDocument;
use App\Models\SemCorp\ProjectDocumentUpload;
use App\Models\SemCorp\ProjectDocumentRecurring;
use App\Models\SemCorp\ProjectDocumentApproval;
use App\Models\SemCorp\check_table;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\AuditLogDocument;
use App\Models\SemCorp\View\ProjectGroupSiteTaskView;
use Auth;
use App\Http\Resources\SemCorp\ProjectDocumentUploadResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\DocumentUploadMail;
use Illuminate\Support\Facades\Storage;

class ProjectDocumentUploadsController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_DOCUMENT_UPLOADS";
    protected $primaryKey = 'project_document_upload_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "NO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Upload Document Version cannot be repeat in within the same Project Document.";
    protected $uploadRequest;

    protected $backend_path = '/project_document_uploads/';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [

        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [

        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectDocumentUploadResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectDocumentUpload::orderBy('project_document_upload_id', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectDocumentUpload::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Project::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return ProjectDocumentUpload::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectDocumentUpload::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $idOrCode = ProjectDocumentUpload::insertGetId($newData);
        $this->createDocumentApprovals($this->uploadRequest, $idOrCode);
        return $idOrCode;
    }

    protected function insertAuditLog($changes)
    {
        AuditLogDocument::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectDocumentUpload::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->document_title                = $request->document_title;
        $destinationData->document_mandatory            = $request->document_mandatory;
        $destinationData->assign_to_user                = $request->assign_to_user;
        $destinationData->document_category_id          = $request->document_category_id;
        $destinationData->document_type_id              = $request->document_type_id;
        $destinationData->recurring_interval_id         = $request->recurring_interval_id;
        $destinationData->recurring_start_date          = $request->recurring_start_date;
        $destinationData->recurring_end_date            = $request->recurring_end_date;
        $destinationData->status_id                     = $request->status_id;
        $destinationData->req_approval_project_owner    = $request->req_approval_project_owner;
        $destinationData->req_approval_project_manager  = $request->req_approval_project_manager;
        $destinationData->req_approval_project_engineer = $request->req_approval_project_engineer;
        $destinationData->req_approval_engineer         = $request->req_approval_engineer;
        $destinationData->req_approval_qa_qc            = $request->req_approval_qa_qc;
        $destinationData->req_approval_safety           = $request->req_approval_safety;
        $destinationData->req_approval_onm              = $request->req_approval_onm;
        $destinationData->req_approval_planner          = $request->req_approval_planner;
        $destinationData->req_approval_purchasing       = $request->req_approval_purchasing;
        $destinationData->req_approval_admin            = $request->req_approval_admin;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_documents');
    }

    protected function softDeleteRecord($idOrCode)
    {
        ProjectDocumentUpload::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_document_recurring_id'     => 'nullable|exists:project_document_recurrings,project_document_recurring_id',
            'project_document_id'               => 'required|exists:project_documents,project_document_id',
            'document_version'	                => 'required|min:1|max:200',
            'document_file'                     => 'required|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_document_recurring_id'     => 'nullable|exists:documents,project_document_recurring_id',
            'project_document_id'               => 'required|exists:project_documents,project_document_id',
            'document_version'	                => 'required|min:1|max:200',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $this->uploadRequest = $request;

        if ($request->hasFile('document_file')) {

            $image = $request->file('document_file');

            $imgName = $request->project_document_id . '_' . time() . '_' . $image->getClientOriginalName();

            $location = storage_path($this->backend_path); //move file to ftp file

            //$image->move($location, $imgName);

            Storage::disk('sftp')->putFileAs($this->backend_path, $image, $imgName);

         }

        $additionalDataSet =
        [
            "document_file"         => $imgName,
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    public function store(Request $request)
    {
        $this->uniqueColumnRequest($request);

        $this->validationStoreRequest($request);

        if ( $this->uniqueOrNotSetting == "COMBO" )
        {
            if ( $this->getUniqueComboInsertCheck($this->uniqueColumnRequest)->count() == 0 )
            {
                $this->createNewRecord($request, $this->loginUserInfo['created_by']);
            }
            else
            {
                $this->response = $this->apiResponse->setColumnNotUniqueCombo();
                $this->status = config('response.status.error');
            }
        }
        else
        {
            $this->createNewRecord($request, $this->loginUserInfo['created_by']);
        }

        return response($this->response, $this->status);
    }

    protected function createDocumentApprovals($request, $idOrCode)
    {
        if ( $request->project_document_recurring_id != null )
        {
            $data = ProjectDocumentRecurring::where('project_document_recurring_id', $request->project_document_recurring_id)->first();
            $this->requireProjectOwnerApproval($data->_project_document->req_approval_project_owner, $data->current_status, $request, $idOrCode);
            $this->requireProjectManagerApproval($data->_project_document->req_approval_project_manager, $data->current_status, $request, $idOrCode);
            $this->requireProjectEngineerApproval($data->_project_document->req_approval_project_engineer, $data->current_status, $request, $idOrCode);
            $this->requireEngineerApproval($data->_project_document->req_approval_engineer, $data->current_status, $request, $idOrCode);
            $this->requireQAQCApproval($data->_project_document->req_approval_qa_qc, $data->current_status, $request, $idOrCode);
            $this->requireSafetyApproval($data->_project_document->req_approval_safety, $data->current_status, $request, $idOrCode);
            $this->requireONMApproval($data->_project_document->req_approval_onm, $data->current_status, $request, $idOrCode);
            $this->requirePlannerApproval($data->_project_document->req_approval_planner, $data->current_status, $request, $idOrCode);
            $this->requirePurchasingApproval($data->_project_document->req_approval_purchasing, $data->current_status, $request, $idOrCode);
            $this->requireAdminApproval($data->_project_document->req_approval_admin, $data->current_status, $request, $idOrCode);

            $this->projectDocumentUploadMail($data->_project_document->project_document_id, $idOrCode);
        }
        else
        {
            $data = ProjectDocument::where('project_document_id', $request->project_document_id)->first();
            if ($data->document_category_id == 1)
            {
                $this->requireProjectOwnerApproval($data->req_approval_project_owner, $data->status_id, $request, $idOrCode);
                $this->requireProjectManagerApproval($data->req_approval_project_manager, $data->status_id, $request, $idOrCode);
                $this->requireProjectEngineerApproval($data->req_approval_project_engineer, $data->status_id, $request, $idOrCode);
                $this->requireEngineerApproval($data->req_approval_engineer, $data->status_id, $request, $idOrCode);
                $this->requireQAQCApproval($data->req_approval_qa_qc, $data->status_id, $request, $idOrCode);
                $this->requireSafetyApproval($data->req_approval_safety, $data->status_id, $request, $idOrCode);
                $this->requireONMApproval($data->req_approval_onm, $data->status_id, $request, $idOrCode);
                $this->requirePlannerApproval($data->req_approval_planner, $data->status_id, $request, $idOrCode);
                $this->requirePurchasingApproval($data->req_approval_purchasing, $data->status_id, $request, $idOrCode);
                $this->requireAdminApproval($data->req_approval_admin, $data->status_id, $request, $idOrCode);
            }

            $this->projectDocumentUploadMail($data->project_document_id, $idOrCode);

            # if ($data->document_category_id == 1 && $data->document_type == 1) .... THIS IS FOR IFI DOCUMENT
            # SEND EMAIL to PROJECT MANAGER for Upload Notification
            # 1. Get project ID $project_id = $this->EmailUsers->getProjectIDFrDocumentUpload($request->project_document_id)
            # 2. Get $this->EmailUsers->getProjetManager($project_id)
        }
    }

    public function projectDocumentUploadMail($project_document_id, $project_document_upload_id)
    {
        $document_data = ProjectDocument::where('project_document_id', $project_document_id)->first();

        #IFA RECURRING Document
        if ( $document_data->document_type_id == 2 )
        {
            $project_id = $document_data->project_id;

            $ProjectDocumentUpload = ProjectDocumentUpload::where('project_document_upload_id', $project_document_upload_id)->first();

            $document_data = ProjectDocumentRecurring::where('project_document_recurring_id', $ProjectDocumentUpload->project_document_recurring_id)->first();

            $this->reqApprovalMailRecurring($project_document_upload_id, $document_data, $project_id);

        }

        #IFI Document
        if ( $document_data->document_category_id == 2 && $document_data->document_type_id == 1 )
        {
            $project_manager = $this->EmailUsers->getProjetManager($document_data->project_id);

            if ( $project_manager !== null )
            {
                $project_manager->notify( new DocumentUploadMail($document_data, $project_manager->name));
            }
        }

        #IFA ONE-TIME Document
        if ( $document_data->document_category_id == 1 )
        {
           $this->reqApprovalMailRecurring($project_document_upload_id, $document_data, $document_data->project_id);
        }


    }

    protected function reqApprovalMailRecurring($project_document_upload_id, $document_data, $project_id)
    {
        $ProjectDocumentApprovals = ProjectDocumentApproval::where('project_document_upload_id', $project_document_upload_id)->get();

        if ( $ProjectDocumentApprovals !== null )
        {
            foreach($ProjectDocumentApprovals as $ProjectDocumentApproval)
            {
                #Project Owner
                if ( $ProjectDocumentApproval->approval_role == 8 )
                {
                    $project_owner_datas = $this->EmailUsers->getProjectOwner();

                    if ( $project_owner_datas !== null )
                    {
                        foreach($project_owner_datas as $project_owner)
                        {
                            $project_owner->notify( new DocumentUploadMail($document_data, $project_owner->name));

                        }
                    }
                }

                #Project Manager
                if ( $ProjectDocumentApproval->approval_role == 9 )
                {
                    $project_manager = $this->EmailUsers->getProjetManager($project_id);

                    if ( $project_manager !== null )
                    {
                        $project_manager->notify( new DocumentUploadMail($document_data, $project_manager->name));
                    }
                }

                #Project Engineer
                if ( $ProjectDocumentApproval->approval_role == 10 )
                {
                    $project_engineer_datas = $this->EmailUsers->getUserListFromRoleAll(10);

                    if ( $project_engineer_datas !== null )
                    {
                        foreach($project_engineer_datas as $project_engineer)
                        {
                            $project_engineer->notify( new DocumentUploadMail($document_data, $project_engineer->name));

                        }
                    }
                }

                # Engineer
                if ( $ProjectDocumentApproval->approval_role == 15 )
                {
                    $engineer = $this->EmailUsers->getEngineerForProject($project_id);

                    if ( $engineer !== null )
                    {
                        $engineer->notify( new DocumentUploadMail($document_data, $engineer->name));
                    }
                }

                # QAQC 14
                if ( $ProjectDocumentApproval->approval_role == 14 )
                {
                    $qaqc_datas = $this->EmailUsers->getUserListFromRoleAll(14);

                    if ( $qaqc_datas !== null )
                    {
                        foreach($qaqc_datas as $qaqc)
                        {
                            $qaqc->notify( new DocumentUploadMail($document_data, $qaqc->name));

                        }
                    }
                }

                #Safety
                if ( $ProjectDocumentApproval->approval_role == 16 )
                {
                    $safety = $this->EmailUsers->getSafetyForProject($project_id);

                    if ( $safety !== null )
                    {
                        $safety->notify( new DocumentUploadMail($document_data, $safety->name));
                    }
                }

                # ONM 14
                if ( $ProjectDocumentApproval->approval_role == 18 )
                {
                    $onm_datas = $this->EmailUsers->getUserListFromRoleAll(18);

                    if ( $onm_datas !== null )
                    {
                        foreach($onm_datas as $onm)
                        {
                            $onm->notify( new DocumentUploadMail($document_data, $onm->name));

                        }
                    }
                }

                # Planner 19
                if ( $ProjectDocumentApproval->approval_role == 19 )
                {
                    $planner_datas = $this->EmailUsers->getUserListFromRoleAll(19);

                    if ( $planner_datas !== null )
                    {
                        foreach($planner_datas as $planner)
                        {
                            $planner->notify( new DocumentUploadMail($document_data, $planner->name));

                        }
                    }
                }

                # Purchasing 17
                if ( $ProjectDocumentApproval->approval_role == 17 )
                {
                    $purchasing_datas = $this->EmailUsers->getUserListFromRoleAll(17);

                    if ( $purchasing_datas !== null )
                    {
                        foreach($purchasing_datas as $purchasing)
                        {
                            $purchasing->notify( new DocumentUploadMail($document_data, $purchasing->name));

                        }
                    }
                }

                # Admin 3
                if ( $ProjectDocumentApproval->approval_role == 3 )
                {
                    $admin_datas = $this->EmailUsers->getUserListFromRoleAll(3);

                    if ( $admin_datas !== null )
                    {
                        foreach($admin_datas as $admin)
                        {
                            $admin->notify( new DocumentUploadMail($document_data, $admin->name));

                        }
                    }
                }

            }
        }
    }

    protected function codeOneCheck($request, $status_id, $role_id)
    {
        $code_one = 0;

        if ( $request->project_document_recurring_id != null )
        {
            $code_one = ProjectDocumentApproval::Where('project_document_recurring_id', $request->project_document_recurring_id)
            ->Where('approval_role', $role_id)->Where('code_by_approval', 1)->count();
        }
        else
        {
            $code_one = ProjectDocumentApproval::Where('project_document_id', $request->project_document_id)
            ->Where('approval_role', $role_id)->Where('code_by_approval', 1)->Where('for_status', $status_id)->count();
        }

        return $code_one;
    }

    protected function requireProjectOwnerApproval($req_approval_project_owner, $status_id, $request, $idOrCode)
    {
        $req_approval_project_owner_code_one = $this->codeOneCheck($request, $status_id, 8);

        if ( $req_approval_project_owner == '1' && $req_approval_project_owner_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 8);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT OWNER to Request for Approval using  $this->EmailUsers->getProjectOwner()
        }
    }

    protected function requireProjectManagerApproval($req_approval_project_manager, $status_id, $request, $idOrCode)
    {
        $req_approval_project_manager_code_one = $this->codeOneCheck($request, $status_id, 9);

        if ($req_approval_project_manager == '1' && $req_approval_project_manager_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 9);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT MANAGER to Request for Approval
            # 1. Get project ID $project_id = $this->EmailUsers->getProjectIDFrDocumentUpload($request->project_document_id)
            # 2. Get $this->EmailUsers->getProjetManager($project_id)
        }
    }

    protected function requireProjectEngineerApproval($req_approval_project_engineer, $status_id, $request, $idOrCode)
    {
        $req_approval_project_engineer_code_one = $this->codeOneCheck($request, $status_id, 10);

        if ( $req_approval_project_engineer == '1' && $req_approval_project_engineer_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 10);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT ENGINEEER to Request for Approval
            # $this->EmailUsers->getUserListFromRole(10)
        }
    }

    protected function requireEngineerApproval($req_approval_engineer, $status_id, $request, $idOrCode)
    {
        $req_approval_engineer_code_one = $this->codeOneCheck($request, $status_id, 15);

        if ( $req_approval_engineer == '1' && $req_approval_engineer_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 15);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT ENGINEEER to Request for Approval
            # $this->EmailUsers->getUserListFromRole(15)
        }
    }

    protected function requireQAQCApproval($req_approval_qa_qc, $status_id, $request, $idOrCode)
    {
        $req_approval_qa_qc_code_one = $this->codeOneCheck($request, $status_id, 14);

        if ( $req_approval_qa_qc == '1' && $req_approval_qa_qc_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 14);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT QA&QC to Request for Approval
            # $this->EmailUsers->getUserListFromRole(14)
        }
    }

    protected function requireSafetyApproval($req_approval_safety, $status_id, $request, $idOrCode)
    {
        $req_approval_safety_code_one = $this->codeOneCheck($request, $status_id, 16);

        if ( $req_approval_safety == '1' && $req_approval_safety_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 16);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to SAFETY to Request for Approval
            # 1. Get project ID $project_id = $this->EmailUsers->getProjectIDFrDocumentUpload($request->project_document_id)
            # 2. Get $this->EmailUsers->getSafetyForProject($project_id)
        }
    }

    protected function requireONMApproval($req_approval_onm, $status_id, $request, $idOrCode)
    {
        $req_approval_onm_code_one = $this->codeOneCheck($request, $status_id, 18);

        if ( $req_approval_onm == '1' && $req_approval_onm_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 18);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT ONM to Request for Approval
            # $this->EmailUsers->getUserListFromRole(18)
        }
    }

    protected function requirePlannerApproval($req_approval_planner, $status_id, $request, $idOrCode)
    {
        $req_approval_planner_code_one = $this->codeOneCheck($request, $status_id, 19);

        if ( $req_approval_planner == '1' && $req_approval_planner_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 19);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT Planner to Request for Approval
            # $this->EmailUsers->getUserListFromRole(19)
        }
    }

    protected function requirePurchasingApproval($req_approval_purchasing, $status_id, $request, $idOrCode)
    {
        $req_approval_purchasing_code_one = $this->codeOneCheck($request, $status_id, 17);

        if ( $req_approval_purchasing == '1' && $req_approval_purchasing_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 17);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT QA&QC to Request for Approval
            # $this->EmailUsers->getUserListFromRole(17)
        }
    }

    protected function requireAdminApproval($req_approval_admin, $status_id, $request, $idOrCode)
    {
        $req_approval_admin_code_one = $this->codeOneCheck($request, $status_id, 3);

        if ( $req_approval_admin == '1' && $req_approval_admin_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 3);

            ProjectDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT QA&QC to Request for Approval
            # $this->EmailUsers->getUserListFromRole(17)
        }
    }

    protected function makeApprovalDataSet($status_id, $request, $idOrCode, $role_id)
    {
        return
        [
            "project_document_upload_id"    => $idOrCode,
            "project_document_id"           => $request->project_document_id,
            "project_document_recurring_id" => $request->project_document_recurring_id,
            "for_status"                    => $status_id,
            "approval_role"                 => $role_id,
            "created_by"                    => $this->loginUserInfo['created_by']
        ];
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "project_document_upload_id"        => $request->project_document_upload_id,
            "project_document_id"               => $request->project_document_id,
            "project_document_recurring_id"     => $request->project_document_recurring_id,
            "document_version"                  => $request->document_version,
        ];
    }

    public function showUploadsByDocument($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectDocumentUpload::where('project_document_id', $idOrCode)->Orderby('created_at', 'DESC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showUploadsByDocumentRecurring($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectDocumentUpload::where('project_document_recurring_id', $idOrCode)->Orderby('created_at', 'DESC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function downloadDocument($idOrCode)
    {
        $document_detail =  ProjectDocumentUpload::where('project_document_upload_id', $idOrCode)->first();

        $path = storage_path($this->backend_path.$document_detail->document_file);

        return response()->download($path);
    }

    public function checkAllowUpload($idOrCode, $document_type)
    {
        $returnYesOrNo = "YES";

        if ( $document_type == "ONE-TIME" )
        {
            $countUpload = ProjectDocumentUpload::where('project_document_id', $idOrCode)->count();

            if ( $countUpload == 0 )
            {
                $returnYesOrNo = "YES";
            }
            else
            {
                $returnYesOrNo = "NO";

                $count = ProjectDocumentApproval::where('project_document_id', $idOrCode)
                ->whereNull('code_by_approval')->count();

                if ( $count == 0 )
                {
                    $returnYesOrNo = "YES";

                    $projectDocument = ProjectDocument::where('project_document_id', $idOrCode)->first();

                    if ( $projectDocument->asb_flag == 1 )
                    {
                        $last_project_document_upload = ProjectDocumentApproval::where('project_document_id', $idOrCode)->where('for_status', '3')->orderBy('project_document_upload_id', 'DESC')->first();
                    }
                    else
                    {
                        $last_project_document_upload = ProjectDocumentApproval::where('project_document_id', $idOrCode)->where('for_status', '2')->orderBy('project_document_upload_id', 'DESC')->first();
                    }

                    if ($last_project_document_upload != NULL)
                    {
                        $returnYesOrNo = "NO";

                        $countXY = ProjectDocumentApproval::where('project_document_upload_id', $last_project_document_upload->project_document_upload_id)
                        ->whereNull('code_by_approval')->count();

                        if ( $countXY == 0 )
                        {
                            $returnYesOrNo = "YES";

                            $checkRevisionRejectCountXY = ProjectDocumentApproval::where('project_document_upload_id', $last_project_document_upload->project_document_upload_id)
                            ->whereNOTIn('code_by_approval', ['1'])->count();

                            if ($checkRevisionRejectCountXY == 0)
                            {
                                $returnYesOrNo = "NO";
                            }
                            else
                            {
                                $returnYesOrNo = "YES";
                            }
                        }
                    }
                    else
                    {
                        $returnYesOrNo = "YES";
                    }
                }
                else
                {
                    $returnYesOrNo = "NO";
                }
            }
        }

        if ( $document_type == "RECURRING" )
        {
            $countUpload = ProjectDocumentUpload::where('project_document_recurring_id', $idOrCode)->count();

            if ( $countUpload == 0 )
            {
                $returnYesOrNo = "YES";
            }
            else
            {
                $returnYesOrNo = "NO";

                $count = ProjectDocumentApproval::where('project_document_recurring_id', $idOrCode)
                ->whereNull('code_by_approval')->count();

                if ( $count == 0 )
                {
                    $returnYesOrNo = "YES";

                    $last_project_document_upload = ProjectDocumentApproval::where('project_document_recurring_id', $idOrCode)->orderBy('project_document_upload_id', 'DESC')->first();

                    if ($last_project_document_upload != NULL)
                    {
                        $returnYesOrNo = "NO";

                        $countX = ProjectDocumentApproval::where('project_document_upload_id', $last_project_document_upload->project_document_upload_id)
                        ->whereNull('code_by_approval')->count();

                        if ( $countX == 0 )
                        {
                            $returnYesOrNo = "YES";

                            $checkRevisionRejectCountX = ProjectDocumentApproval::where('project_document_upload_id', $last_project_document_upload->project_document_upload_id)
                            ->whereNOTIn('code_by_approval', ['1'])->count();

                            if ($checkRevisionRejectCountX == 0)
                            {
                                $returnYesOrNo = "NO";
                            }
                            else
                            {
                                $returnYesOrNo = "YES";
                            }
                        }
                    }
                    else
                    {
                        $returnYesOrNo = "YES";
                    }
                }
                else
                {
                    $returnYesOrNo = "NO";
                }
            }
        }

        return $returnYesOrNo;

    }

}
