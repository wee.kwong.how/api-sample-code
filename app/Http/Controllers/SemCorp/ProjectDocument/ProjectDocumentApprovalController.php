<?php

namespace App\Http\Controllers\SemCorp\ProjectDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\DocumentTemplate;
use App\Models\SemCorp\ProjectDocument;
use App\Models\SemCorp\ProjectDocumentApproval;
use App\Models\SemCorp\ProjectDocumentRecurring;
use App\Models\SemCorp\View\ProjectGroupSiteDocumentView;
use App\Models\SemCorp\AuditLogDocument;
use Auth;
use App\Http\Resources\SemCorp\ProjectDocumentApprovalResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\DocumentChangeStatusMail;
use Illuminate\Support\Facades\Storage;

class ProjectDocumentApprovalController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_DOCUMENT_APPROVAL";
    protected $primaryKey = 'project_document_approval_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "NO";
    protected $uniqueColumnRequest = [];

    protected $backend_path = '/project_approval_attachments/';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [

        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [

        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectDocumentApprovalResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectDocumentApproval::orderBy('project_document_approval_id', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectDocumentApproval::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;



        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return collect([ 0 ]);
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return collect([ 0 ]);

    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return ProjectDocumentApproval::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogDocument::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectDocumentApproval::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        if ($request->hasFile('approval_attachments'))
        {
            $image = $request->file('approval_attachments');

            $imgName = $request->project_document_approval_id . '_' . time() . '_' . $image->getClientOriginalName();

            //$location = storage_path($this->backend_path); //move file to ftp file

            //$image->move($location, $imgName);
            Storage::disk('sftp')->putFileAs($this->backend_path, $image, $imgName);

            $destinationData->approval_attachments          = $imgName;
        }

        $destinationData->code_by_approval              = $request->code_by_approval;
        $destinationData->approval_comments             = $request->approval_comments;
        $destinationData->approval_by                   = $this->loginUserInfo['created_by'];

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_document_approvals');

        $this->checkApprovalStatus($destinationData);

        # SEND EMAIL HERE to ASSIGNEE to using $this->EmailUsers->getAssignUserFrProjectDocApproval($request->project_document_approval_id)
        # SEND EMAIL TO PROJECT MANAGER
        # 1. Get project ID $project_id = $this->EmailUsers->getProjectIDFrDocumentUpload($request->project_document_id)
        # 2. Get $this->EmailUsers->getProjetManager($project_id)
    }

    protected function checkApprovalStatus($ProjectDocumentApprovalData)
    {
        $checkApprovalCount = ProjectDocumentApproval::where('project_document_upload_id', $ProjectDocumentApprovalData->project_document_upload_id)
        ->whereNull('code_by_approval')->count();

        if ( $checkApprovalCount == 0 )
        {
            $checkRevisionRejectCount = ProjectDocumentApproval::where('project_document_upload_id', $ProjectDocumentApprovalData->project_document_upload_id)
            ->whereIn('code_by_approval', ['3', '4'])->count();

            if ($checkRevisionRejectCount == 0 )
            {
                $this->updateDocumentStatus($ProjectDocumentApprovalData);
            }
        }
    }

    protected function updateDocumentStatus($ProjectDocumentApprovalData)
    {
        if ( $ProjectDocumentApprovalData->project_document_recurring_id == NULL  )
        {
            $projectDocumentData = ProjectDocument::where('project_document_id', $ProjectDocumentApprovalData->project_document_id)->first();

            if ( $projectDocumentData->asb_flag == 1 )
            {
                if ( $projectDocumentData->status_id < 3 )
                {
                    $this->updateStatus($projectDocumentData, $ProjectDocumentApprovalData);
                }
            }
            else
            {
                if ( $projectDocumentData->status_id < 2 )
                {
                    $this->updateStatus($projectDocumentData, $ProjectDocumentApprovalData);
                }
            }
        }

    }

    public function updateStatus($projectDocumentData, $ProjectDocumentApprovalData)
    {
        $new_status = $projectDocumentData->status_id + 1;
    
        $data = [ "status_id" => $new_status, ];

        ProjectDocument::Where('project_document_id', $ProjectDocumentApprovalData->project_document_id)->update($data);

        $this->projectDocumentSendEmailChangeStatus($ProjectDocumentApprovalData->project_document_id);

        # SEND EMAIL TO PROJECT MANAGER
        # 1. Get project ID $project_id = $this->EmailUsers->getProjectIDFrDocumentUpload($request->project_document_id)
        # 2. Get $this->EmailUsers->getProjetManager($project_id)
    }

    public function projectDocumentSendEmailChangeStatus($project_document_id)
    {
        $document_data = ProjectDocument::where('project_document_id', $project_document_id)->first();

        $project_manager = $this->EmailUsers->getProjetManager($document_data->project_id);

        if ( $project_manager !== null )
        {
            $project_manager->notify( new DocumentChangeStatusMail($document_data, $project_manager->name));
        }

        // $user = User::where('id', 1)->first();

        // $user->notify( new GroupChangeStatusMail($document_data, $user->name));

        //dd($document_data);
        //dd($user);
    }


    protected function softDeleteRecord($idOrCode)
    {
        ProjectDocumentApproval::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_document_approval_id'      => 'required|exists:project_document_approvals,project_document_approval_id',
            'code_by_approval'                  => 'required|integer',
            'approval_comments'	                => 'nullable|min:1|max:1000',
            'approval_attachments'              => 'nullable|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_document_approval_id'      => 'required|exists:project_document_approvals,project_document_approval_id',
            'code_by_approval'                  => 'required|integer',
            'approval_comments'	                => 'nullable|min:1|max:1000',
            'approval_attachments'              => 'nullable|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {

        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    public function store(Request $request)
    {
        $this->uniqueColumnRequest($request);

        $idOrCode = $request->project_document_approval_id;

        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'edit' );

        if ( $validationCheck == "YES" )
        {
            if ( $this->uniqueOrNotSetting == "YES" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueColumnName);
            }
            else if ( $this->uniqueOrNotSetting == "COMBO" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueCombo);
            }
            else
            {
                $codeOrIdCheck = $this->validateCodeOrId($idOrCode, $this->primaryKey);
            }

            if ( $codeOrIdCheck == "YES" )
            {
                $this->validationUpdateRequest($request);
                $this->updateExistingRecord($request, $idOrCode);
            }
        }

        return response($this->response, $this->status);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_comment_id"               => $request->document_comment_id,
            "document_detail_id"                => $request->document_detail_id,
            "document_comment"                  => $request->document_comment,
        ];
    }

    public function showApprovalByDocument($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectDocumentApproval::where('project_document_upload_id', $idOrCode)->Orderby('project_document_approval_id', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function downloadDocument($idOrCode)
    {
        $document_detail =  ProjectDocumentApproval::where('project_document_approval_id', $idOrCode)->first();

        $path = storage_path($this->backend_path.$document_detail->approval_attachments);

        return response()->download($path);
    }

}
