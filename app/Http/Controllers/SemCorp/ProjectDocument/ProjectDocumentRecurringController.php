<?php

namespace App\Http\Controllers\SemCorp\ProjectDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectDocumentTemplate;
use App\Models\SemCorp\ProjectDocument;
use App\Models\SemCorp\ProjectDocumentRecurring;
use App\Models\SemCorp\ProjectDocumentApproval;
use App\Models\SemCorp\ProjectDocumentUpload;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\AuditLogDocument;
use App\Models\SemCorp\View\ProjectGroupSiteTaskView;
use App\Models\SemCorp\View\ProjectDocumentRecurringView;
use Auth;
use App\Http\Resources\SemCorp\ProjectDocumentResource;
use App\Http\Resources\SemCorp\ProjectDocumentRecurringResource;
use Illuminate\Validation\Rule;
use DB;
use DateTime;

class ProjectDocumentRecurringController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_DOCUMENT_RECURRING";
    protected $primaryKey = 'project_document_recurring_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Project Document Recurring Date cannot be repeat in within the same Project Document.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_document_id',
            1 => 'document_recurring_date',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectDocumentRecurringResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectDocumentRecurring::orderBy('document_recurring_date', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectDocumentRecurring::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Project::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return ProjectDocumentRecurring::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectDocumentRecurring::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $idOrCode = ProjectDocumentRecurring::insertGetId($newData);

        return $idOrCode;
    }

    protected function insertAuditLog($changes)
    {
        AuditLogDocument::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = ProjectDocumentRecurring::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->document_recurring_date                = $request->document_recurring_date;
        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_document_recurrings');
    }


    protected function softDeleteRecord($idOrCode)
    {
        ProjectDocumentRecurring::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_document_id'               => 'required|exists:project_documents,project_document_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_document_id'               => 'required|exists:project_documents,project_document_id',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [

        ];
    }

    public function showRecurringByDocument($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectDocumentRecurringView::where('project_document_id', $idOrCode)->Orderby('document_recurring_date', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

}
