<?php

namespace App\Http\Controllers\SemCorp\ProjectTask;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectTaskTemplate;
use App\Models\SemCorp\ProjectTask;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\AuditLogTask;
use App\Models\SemCorp\View\ProjectGroupSiteTaskView;
use Auth;
use App\Http\Resources\SemCorp\ProjectTaskResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\TaskChangeStatusMail;
use Illuminate\Support\Facades\Storage;
use App\Models\SemCorp\Dashboard\DailyOverDueProject;

class ProjectTaskController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT_TASK";
    protected $primaryKey = 'task_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Project Task Title cannot be repeat in within the same Project.";

    protected $backend_path = '/project_task_attachments/';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'project_id',
            1 => 'task_title',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return ProjectTaskResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return ProjectTask::orderBy('task_title', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return ProjectTask::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Project::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return ProjectTask::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return ProjectTask::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $newId = ProjectTask::insertGetId($newData);

        $task_data = ProjectTask::where('task_id', $newId)->first();

        if (in_array($task_data->status_id, [3,4,5,6,7]))
        {
            $this->projectTaskSendEmailChangeStatus($newId);
        }

        return $newId;
    }

    public function projectTaskSendEmailChangeStatus($task_id)
    {
        $task_data = ProjectTask::where('task_id', $task_id)->first();

        $project_manager = $this->EmailUsers->getProjetManager($task_data->project_id);

        if ( $project_manager !== null )
        {
            $project_manager->notify( new TaskChangeStatusMail($task_data, $project_manager->name));
        }

        // $user = User::where('id', 1)->first();

        // $user->notify( new GroupChangeStatusMail($task_data, $user->name));

        //dd($task_data);
        //dd($user);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogTask::insert($changes);
    }

    public function updateTaskRecord(Request $request, $idOrCode)
    {
        $this->uniqueColumnRequest($request);

        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'edit' );

        if ( $validationCheck == "YES" )
        {
            if ( $this->uniqueOrNotSetting == "YES" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueColumnName);
            }
            else if ( $this->uniqueOrNotSetting == "COMBO" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueCombo);
            }
            else
            {
                $codeOrIdCheck = $this->validateCodeOrId($idOrCode, $this->primaryKey);
            }

            if ( $codeOrIdCheck == "YES" )
            {
                $this->validationUpdateRequest($request);
                $this->updateExistingRecord($request, $idOrCode);
            }
        }

        return response($this->response, $this->status);
    }

    public function downloadDocument($idOrCode)
    {
        $task_detail =  ProjectTask::where('task_id', $idOrCode)->first();

        $path = storage_path($this->backend_path.$task_detail->upload_attachment);

        return response()->download($path);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        // ProjectTask::where($this->primaryKey, $idOrCode)->update($updateData);
        $destinationData = ProjectTask::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->task_title            = $request->task_title;
        $destinationData->assign_to_user        = $request->assign_to_user;
        $destinationData->task_description      = $request->task_description;
        $destinationData->task_remarks          = $request->task_remarks;
        $destinationData->task_progress         = $request->task_progress;
        $destinationData->status_id             = $request->status_id;
        $destinationData->task_est_start_date   = $request->task_est_start_date;
        $destinationData->task_est_end_date     = $request->task_est_end_date;
        $destinationData->task_start_date       = $request->task_start_date;
        $destinationData->task_end_date         = $request->task_end_date;

        if ($request->hasFile('upload_attachment')) 
        {
            $image = $request->file('upload_attachment');

            $imgName = $request->task_id . '_' . time() . '_' . $image->getClientOriginalName();

            $location = storage_path($this->backend_path); //move file to ftp file

            Storage::disk('sftp')->putFileAs($this->backend_path, $image, $imgName);

            $destinationData->upload_attachment      = $imgName;
         }

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'project_itasks');

        if (array_key_exists('status_id', $changed))
        {
            if (in_array($changed['status_id'], [4,5,6,7]))
            {
                $this->projectTaskSendEmailChangeStatus($original['task_id']);
            }
        }

    
        # SEND EMAIL TO PROJECT MANAGER
        # 1. Get project ID $project_id = $this->EmailUsers->getProjectIDFrDocumentUpload($request->project_document_id)
        # 2. Get $this->EmailUsers->getProjetManager($project_id)
    }

    protected function softDeleteRecord($idOrCode)
    {
        $model = new DailyOverDueProject();

        $model->hydrate(DB::select(" CALL `sp_delete_project_task`( '" . $idOrCode . "' , '" . $this->loginUserInfo['created_by'] . "' ) "));
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'project_id'                    => 'required|exists:projects,project_id',
            'task_title'	                => 'required|min:1|max:100',
            'assign_to_user'                => 'required|exists:users,id',
            'task_description'	            => 'nullable|min:1|max:300',
            'task_remarks'	                => 'nullable|min:1|max:1000',
            'task_progress'	                => 'required|integer|gte:-1|lte:100',
            'status_id'                     => 'required|exists:status_tasks,status_id',
            'task_est_start_date'           => 'required|date',
            'task_est_end_date'             => 'required|date|after_or_equal:task_est_start_date',
            'task_start_date'               => 'nullable|date',
            'task_end_date'                 => 'nullable|date|after_or_equal:task_start_date',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'task_id'                       => 'required|exists:project_itasks,task_id',
            'project_id'                    => 'required|exists:projects,project_id',
            'task_title'	                => 'required|min:1|max:100',
            'assign_to_user'                => 'required|exists:users,id',
            'task_description'	            => 'nullable|min:1|max:300',
            'task_remarks'	                => 'nullable|min:1|max:1000',
            'task_progress'	                => 'required|integer|gte:-1|lte:100',
            'status_id'                     => 'required|exists:status_tasks,status_id',
            'task_est_start_date'           => 'required|date',
            'task_est_end_date'             => 'required|date|after_or_equal:task_est_start_date',
            'task_start_date'               => 'nullable|date',
            'task_end_date'                 => 'nullable|date|after_or_equal:task_start_date',
            'upload_attachment'             => 'nullable|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip,txt|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "task_id"                   => $request->task_id,
            "project_id"                => $request->project_id,
            "task_title"                => $request->task_title,
            "assign_to_user"            => $request->assign_to_user,
            "task_description"          => $request->task_description,
            "task_remarks"              => $request->task_remarks,
            "task_progress"             => $request->task_progress,
            "status_id"                 => $request->status_id,
            "task_est_start_date"       => $request->task_est_start_date,
            "task_est_end_date"         => $request->task_est_end_date,
            "task_start_date"           => $request->task_start_date,
            "task_end_date"             => $request->task_end_date,
        ];
    }

    public function showTaskByProject($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectTask::where('project_id', $idOrCode)->Orderby('task_title', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

}
