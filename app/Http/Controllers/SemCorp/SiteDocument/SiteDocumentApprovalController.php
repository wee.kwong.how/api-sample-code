<?php

namespace App\Http\Controllers\SemCorp\SiteDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectSite;
use App\Models\SemCorp\DocumentTemplate;
use App\Models\SemCorp\SiteDocument;
use App\Models\SemCorp\SiteDocumentApproval;
use App\Models\SemCorp\SiteDocumentRecurring;
use App\Models\SemCorp\View\ProjectGroupSiteDocumentView;
use App\Models\SemCorp\AuditLogDocument;
use Auth;
use App\Http\Resources\SemCorp\SiteDocumentApprovalResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\SiteDocumentChangeStatusMail;
use Illuminate\Support\Facades\Storage;

class SiteDocumentApprovalController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "SITE_DOCUMENT_APPROVAL";
    protected $primaryKey = 'site_document_approval_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "NO";
    protected $uniqueColumnRequest = [];

    protected $backend_path = '/site_approval_attachments/';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [

        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [

        ];
    }

    protected function makeResoucesCollection($data)
    {
        return SiteDocumentApprovalResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return SiteDocumentApproval::orderBy('site_document_approval_id', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return SiteDocumentApproval::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;



        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        // return Document::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        // ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        // ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        // ->get();

        return collect([ 0 ]);
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        // if ( $this->checkKeyBelongToAccount($uniqueColumnRequest) == "YES" )
        // {
        //     return Document::where($this->primaryKey, '<>', $idOrCode)
        //     ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        //     ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        //     ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        //     ->get();
        // }
        // else
        // {
        //     return collect([ 0 ]);
        // }

        // return Document::where($this->primaryKey, '<>', $idOrCode)
        // ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        // ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        // ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        // ->get();
        return collect([ 0 ]);

    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        return SiteDocumentApproval::insertGetId($newData);
    }

    protected function insertAuditLog($changes)
    {
        AuditLogDocument::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = SiteDocumentApproval::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        if ($request->hasFile('approval_attachments'))
        {
            $image = $request->file('approval_attachments');

            $imgName = $request->site_document_approval_id . '_' . time() . '_' . $image->getClientOriginalName();

            // $location = storage_path($this->backend_path); //move file to ftp file

            // $image->move($location, $imgName);
            Storage::disk('sftp')->putFileAs($this->backend_path, $image, $imgName);
            
            $destinationData->approval_attachments          = $imgName;
        }

        $destinationData->code_by_approval              = $request->code_by_approval;
        $destinationData->approval_comments             = $request->approval_comments;
        $destinationData->approval_by                   = $this->loginUserInfo['created_by'];

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'site_document_approvals');

        $this->checkApprovalStatus($destinationData);

        # SEND EMAIL HERE to ASSIGNEE to using $this->EmailUsers->getAssignUserFrSiteDocApproval($site_document_approval_id)
        # SEND EMAIL TO PROJECT MANAGER & PROJECT ENGINEER
        # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
        # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
        # 3. Get $this->EmailUsers->getProjetManager($project_id)
        # 4. Get Group ID $group_id = $this->EmailUsers->getGroupIDfromSiteID($idOrCode)
        # 5. Get $this->EmailUsers->getProjectEngineer($group_id)
    }

    protected function checkApprovalStatus($SiteDocumentApprovalData)
    {
        $checkApprovalCount = SiteDocumentApproval::where('site_document_upload_id', $SiteDocumentApprovalData->site_document_upload_id)
        ->whereNull('code_by_approval')->count();

        if ( $checkApprovalCount == 0 )
        {
            $checkRevisionRejectCount = SiteDocumentApproval::where('site_document_upload_id', $SiteDocumentApprovalData->site_document_upload_id)
            ->whereIn('code_by_approval', ['3', '4'])->count();

            if ($checkRevisionRejectCount == 0 )
            {
                $this->updateDocumentStatus($SiteDocumentApprovalData);
            }
        }
    }

    protected function updateDocumentStatus($SiteDocumentApprovalData)
    {
        if ( $SiteDocumentApprovalData->site_document_recurring_id == NULL  )
        {
            $SiteDocumentData = SiteDocument::where('site_document_id', $SiteDocumentApprovalData->site_document_id)->first();

            if ( $SiteDocumentData->asb_flag == 1 )
            {
                if ( $SiteDocumentData->status_id < 3 )
                {
                    $this->updateStatus($SiteDocumentData, $SiteDocumentApprovalData);
                }
            }
            else
            {
                if ( $SiteDocumentData->status_id < 2 )
                {
                    $this->updateStatus($SiteDocumentData, $SiteDocumentApprovalData);
                }
            }
        }
    }

    public function updateStatus($SiteDocumentData, $SiteDocumentApprovalData)
    {
        $new_status = $SiteDocumentData->status_id + 1;

        $data = [ "status_id" => $new_status, ];

        SiteDocument::Where('site_document_id', $SiteDocumentApprovalData->site_document_id)->update($data);

        $this->siteDocumentSendEmailChangeStatus($SiteDocumentApprovalData->site_document_id);

        # SEND EMAIL TO PROJECT MANAGER & PROJECT ENGINEER
        # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
        # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
        # 3. Get $this->EmailUsers->getProjetManager($project_id)
        # 4. Get Group ID $group_id = $this->EmailUsers->getGroupIDfromSiteID($idOrCode)
        # 5. Get $this->EmailUsers->getProjectEngineer($group_id)
    }

    public function siteDocumentSendEmailChangeStatus($site_document_id)
    {
        $document_data = SiteDocument::where('site_document_id', $site_document_id)->first();

        $site_data = ProjectSite::where('site_id', $document_data->site_id)->first();

        $project_engineer = $this->EmailUsers->getProjectEngineer($site_data->group_id);

        if ( $project_engineer !== null )
        {
            $project_engineer->notify( new SiteDocumentChangeStatusMail($document_data, $project_engineer->name));
        }

        // $user = User::where('id', 1)->first();

        // $user->notify( new GroupChangeStatusMail($document_data, $user->name));

        //dd($document_data);
        //dd($user);
    }


    protected function softDeleteRecord($idOrCode)
    {
        SiteDocumentApproval::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_document_approval_id'      => 'required|exists:site_document_approvals,site_document_approval_id',
            'code_by_approval'                  => 'required|integer',
            'approval_comments'	                => 'nullable|min:1|max:1000',
            'approval_attachments'              => 'nullable|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_document_approval_id'      => 'required|exists:site_document_approvals,site_document_approval_id',
            'code_by_approval'                  => 'required|integer',
            'approval_comments'	                => 'nullable|min:1|max:1000',
            'approval_attachments'              => 'nullable|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {

        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    public function store(Request $request)
    {
        $this->uniqueColumnRequest($request);

        $idOrCode = $request->site_document_approval_id;

        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'edit' );

        if ( $validationCheck == "YES" )
        {
            if ( $this->uniqueOrNotSetting == "YES" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueColumnName);
            }
            else if ( $this->uniqueOrNotSetting == "COMBO" )
            {
                $codeOrIdCheck = $this->validateCodeOrIdWithUniqueColumn($idOrCode, $this->uniqueColumnRequest, $this->primaryKey, $this->uniqueCombo);
            }
            else
            {
                $codeOrIdCheck = $this->validateCodeOrId($idOrCode, $this->primaryKey);
            }

            if ( $codeOrIdCheck == "YES" )
            {
                $this->validationUpdateRequest($request);
                $this->updateExistingRecord($request, $idOrCode);
            }
        }

        return response($this->response, $this->status);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "document_comment_id"               => $request->document_comment_id,
            "document_detail_id"                => $request->document_detail_id,
            "document_comment"                  => $request->document_comment,
        ];
    }

    public function showApprovalByDocument($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = SiteDocumentApproval::where('site_document_upload_id', $idOrCode)->Orderby('site_document_approval_id', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function downloadDocument($idOrCode)
    {
        $document_detail =  SiteDocumentApproval::where('site_document_approval_id', $idOrCode)->first();

        $path = storage_path($this->backend_path.$document_detail->approval_attachments);

        return response()->download($path);
    }

}
