<?php

namespace App\Http\Controllers\SemCorp\SiteDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\SiteDocumentTemplate;
use App\Models\SemCorp\SiteDocument;
use App\Models\SemCorp\SiteDocumentUpload;
use App\Models\SemCorp\SiteDocumentRecurring;
use App\Models\SemCorp\SiteDocumentApproval;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\ProjectSite;
use App\Models\SemCorp\AuditLogDocument;
use App\Models\SemCorp\View\ProjectGroupSiteTaskView;
use Auth;
use App\Http\Resources\SemCorp\SiteDocumentUploadResource;
use Illuminate\Validation\Rule;
use DB;
use App\Notifications\SiteDocumentUploadMail;
use Illuminate\Support\Facades\Storage;

class SiteDocumentUploadsController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "SITE_DOCUMENT_UPLOADS";
    protected $primaryKey = 'site_document_upload_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "NO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Upload Document Version cannot be repeat in within the same Site Document.";
    protected $uploadRequest;

    protected $backend_path = '/site_document_uploads/';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [

        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [

        ];
    }

    protected function makeResoucesCollection($data)
    {
        return SiteDocumentUploadResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return SiteDocumentUpload::orderBy('site_document_upload_id', 'DESC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return SiteDocumentUpload::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = Project::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return SiteDocumentUpload::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return SiteDocumentUpload::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $idOrCode = SiteDocumentUpload::insertGetId($newData);

        $this->createDocumentApprovals($this->uploadRequest, $idOrCode);

        return $idOrCode;
    }

    protected function insertAuditLog($changes)
    {
        AuditLogDocument::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = SiteDocumentUpload::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->document_title                = $request->document_title;
        $destinationData->document_mandatory            = $request->document_mandatory;
        $destinationData->assign_to_user                = $request->assign_to_user;
        $destinationData->document_category_id          = $request->document_category_id;
        $destinationData->document_type_id              = $request->document_type_id;
        $destinationData->recurring_interval_id         = $request->recurring_interval_id;
        $destinationData->recurring_start_date          = $request->recurring_start_date;
        $destinationData->recurring_end_date            = $request->recurring_end_date;
        $destinationData->status_id                     = $request->status_id;
        $destinationData->req_approval_project_owner    = $request->req_approval_project_owner;
        $destinationData->req_approval_project_manager  = $request->req_approval_project_manager;
        $destinationData->req_approval_project_engineer = $request->req_approval_project_engineer;
        $destinationData->req_approval_engineer         = $request->req_approval_engineer;
        $destinationData->req_approval_qa_qc            = $request->req_approval_qa_qc;
        $destinationData->req_approval_safety           = $request->req_approval_safety;
        $destinationData->req_approval_onm              = $request->req_approval_onm;
        $destinationData->req_approval_planner          = $request->req_approval_planner;
        $destinationData->req_approval_purchasing       = $request->req_approval_purchasing;
        $destinationData->req_approval_admin            = $request->req_approval_admin;

        $destinationData->save();

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'site_documents');
    }

    protected function softDeleteRecord($idOrCode)
    {
        SiteDocumentUpload::where($this->primaryKey, $idOrCode)->delete();
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_document_recurring_id'        => 'nullable|exists:site_document_recurrings,site_document_recurring_id',
            'site_document_id'                  => 'required|exists:site_documents,site_document_id',
            'document_version'	                => 'required|min:1|max:200',
            'document_file'                     => 'required|mimes:jpeg,jpg,png,gif,pdf,dwg,doc,docx,xlsx,ppt,pptx,zip|max:716800'
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_document_recurring_id'        => 'nullable|exists:documents,site_document_recurring_id',
            'site_document_id'                  => 'required|exists:site_documents,site_document_id',
            'document_version'	                => 'required|min:1|max:200',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $this->uploadRequest = $request;

        if ($request->hasFile('document_file')) {

            $image = $request->file('document_file');

            $imgName = $request->site_document_id . '_' . time() . '_' . $image->getClientOriginalName();

            // $location = storage_path($this->backend_path); //move file to ftp file

            // $image->move($location, $imgName);

            Storage::disk('sftp')->putFileAs($this->backend_path, $image, $imgName);

         }

        $additionalDataSet =
        [
            "document_file"         => $imgName,
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    public function store(Request $request)
    {
        $this->uniqueColumnRequest($request);

        $this->validationStoreRequest($request);

        if ( $this->uniqueOrNotSetting == "COMBO" )
        {
            if ( $this->getUniqueComboInsertCheck($this->uniqueColumnRequest)->count() == 0 )
            {
                $this->createNewRecord($request, $this->loginUserInfo['created_by']);
            }
            else
            {
                $this->response = $this->apiResponse->setColumnNotUniqueCombo();
                $this->status = config('response.status.error');
            }
        }
        else
        {
            $this->createNewRecord($request, $this->loginUserInfo['created_by']);
        }

        return response($this->response, $this->status);
    }

    protected function createDocumentApprovals($request, $idOrCode)
    {
        if ( $request->site_document_recurring_id != null )
        {
            $data = SiteDocumentRecurring::where('site_document_recurring_id', $request->site_document_recurring_id)->first();
            $this->requireProjectOwnerApproval($data->_site_document->req_approval_project_owner, $data->current_status, $request, $idOrCode);
            $this->requireProjectManagerApproval($data->_site_document->req_approval_project_manager, $data->current_status, $request, $idOrCode);
            $this->requireProjectEngineerApproval($data->_site_document->req_approval_project_engineer, $data->current_status, $request, $idOrCode);
            $this->requireEngineerApproval($data->_site_document->req_approval_engineer, $data->current_status, $request, $idOrCode);
            $this->requireQAQCApproval($data->_site_document->req_approval_qa_qc, $data->current_status, $request, $idOrCode);
            $this->requireSafetyApproval($data->_site_document->req_approval_safety, $data->current_status, $request, $idOrCode);
            $this->requireONMApproval($data->_site_document->req_approval_onm, $data->current_status, $request, $idOrCode);
            $this->requirePlannerApproval($data->_site_document->req_approval_planner, $data->current_status, $request, $idOrCode);
            $this->requirePurchasingApproval($data->_site_document->req_approval_purchasing, $data->current_status, $request, $idOrCode);
            $this->requireAdminApproval($data->_site_document->req_approval_admin, $data->current_status, $request, $idOrCode);

            $this->siteDocumentUploadMail($data->_site_document->site_document_id, $idOrCode);
        }
        else
        {
            $data = SiteDocument::where('site_document_id', $request->site_document_id)->first();
            if ($data->document_category_id == 1)
            {
                $this->requireProjectOwnerApproval($data->req_approval_project_owner, $data->status_id, $request, $idOrCode);
                $this->requireProjectManagerApproval($data->req_approval_project_manager, $data->status_id, $request, $idOrCode);
                $this->requireProjectEngineerApproval($data->req_approval_project_engineer, $data->status_id, $request, $idOrCode);
                $this->requireEngineerApproval($data->req_approval_engineer, $data->status_id, $request, $idOrCode);
                $this->requireQAQCApproval($data->req_approval_qa_qc, $data->status_id, $request, $idOrCode);
                $this->requireSafetyApproval($data->req_approval_safety, $data->status_id, $request, $idOrCode);
                $this->requireONMApproval($data->req_approval_onm, $data->status_id, $request, $idOrCode);
                $this->requirePlannerApproval($data->req_approval_planner, $data->status_id, $request, $idOrCode);
                $this->requirePurchasingApproval($data->req_approval_purchasing, $data->status_id, $request, $idOrCode);
                $this->requireAdminApproval($data->req_approval_admin, $data->status_id, $request, $idOrCode);
            }

            $this->siteDocumentUploadMail($data->site_document_id, $idOrCode);
            # if ($data->document_category_id == 1 && $data->document_type == 1) .... THIS IS FOR IFI DOCUMENT
            # SEND EMAIL to PROJECT MANAGER & PROJECT ENGINEER for Upload Notification
            # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
            # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
            # 3. Get $this->EmailUsers->getProjetManager($project_id)
            # 4. Get Group ID $group_id = $this->EmailUsers->getGroupIDfromSiteID($idOrCode)
            # 5. Get $this->EmailUsers->getProjectEngineer($group_id)
        }

    }

    public function siteDocumentUploadMail($site_document_id, $site_document_upload_id)
    {
        $document_data = SiteDocument::where('site_document_id', $site_document_id)->first();

        #IFA RECURRING Document
        if ( $document_data->document_type_id == 2 )
        {
            $site_data = ProjectSite::where('site_id', $document_data->site_id)->first();

            $SiteDocumentUpload = SiteDocumentUpload::where('site_document_upload_id', $site_document_upload_id)->first();

            $document_data = SiteDocumentRecurring::where('site_document_recurring_id', $SiteDocumentUpload->site_document_recurring_id)->first();

            $this->reqApprovalMailRecurring($site_document_upload_id, $document_data, $site_data->project_id, $site_data->group_id);

        }

        #IFI Document
        if ( $document_data->document_category_id == 2 && $document_data->document_type_id == 1 )
        {
            $site_data = ProjectSite::where('site_id', $document_data->site_id)->first();

            $project_manager = $this->EmailUsers->getProjetManager($site_data->project_id);

            if ( $project_manager !== null )
            {
                $project_manager->notify( new SiteDocumentUploadMail($document_data, $project_manager->name));
            }

            $project_engineer = $this->EmailUsers->getProjectEngineer($site_data->group_id);

            if ( $project_engineer !== null )
            {
                $project_engineer->notify( new SiteDocumentUploadMail($site_data, $project_engineer->name));
            }

        }

        #IFA ONE-TIME Document
        if ( $document_data->document_category_id == 1 )
        {
            $site_data = ProjectSite::where('site_id', $document_data->site_id)->first();

            $this->reqApprovalMailRecurring($site_document_upload_id, $document_data, $site_data->project_id, $site_data->group_id);
        }


    }

    protected function reqApprovalMailRecurring($site_document_upload_id, $document_data, $project_id, $group_id)
    {
        $SiteDocumentApprovals = SiteDocumentApproval::where('site_document_upload_id', $site_document_upload_id)->get();

        if ( $SiteDocumentApprovals !== null )
        {
            foreach($SiteDocumentApprovals as $SiteDocumentApproval)
            {
                #Project Owner
                if ( $SiteDocumentApproval->approval_role == 8 )
                {
                    $project_owner_datas = $this->EmailUsers->getProjectOwner();

                    if ( $project_owner_datas !== null )
                    {
                        foreach($project_owner_datas as $project_owner)
                        {
                            $project_owner->notify( new SiteDocumentUploadMail($document_data, $project_owner->name));

                        }
                    }
                }

                #Project Manager
                if ( $SiteDocumentApproval->approval_role == 9 )
                {
                    $project_manager = $this->EmailUsers->getProjetManager($project_id);

                    if ( $project_manager !== null )
                    {
                        $project_manager->notify( new SiteDocumentUploadMail($document_data, $project_manager->name));
                    }
                }

                #Project Engineer
                if ( $SiteDocumentApproval->approval_role == 10 )
                {
                    $project_engineer = $this->EmailUsers->getProjectEngineer($group_id);

                    if ( $project_engineer !== null )
                    {
                        $project_engineer->notify( new SiteDocumentUploadMail($document_data, $project_engineer->name));
                    }
                }

                # Engineer
                if ( $SiteDocumentApproval->approval_role == 15 )
                {
                    $engineer = $this->EmailUsers->getEngineerForProject($project_id);

                    if ( $engineer !== null )
                    {
                        $engineer->notify( new SiteDocumentUploadMail($document_data, $engineer->name));
                    }
                }

                # QAQC 14
                if ( $SiteDocumentApproval->approval_role == 14 )
                {
                    $qaqc_datas = $this->EmailUsers->getUserListFromRoleAll(14);

                    if ( $qaqc_datas !== null )
                    {
                        foreach($qaqc_datas as $qaqc)
                        {
                            $qaqc->notify( new SiteDocumentUploadMail($document_data, $qaqc->name));

                        }
                    }
                }

                #Safety
                if ( $SiteDocumentApproval->approval_role == 16 )
                {
                    $safety = $this->EmailUsers->getSafetyForProject($project_id);

                    if ( $safety !== null )
                    {
                        $safety->notify( new SiteDocumentUploadMail($document_data, $safety->name));
                    }
                }

                # ONM 14
                if ( $SiteDocumentApproval->approval_role == 18 )
                {
                    $onm_datas = $this->EmailUsers->getUserListFromRoleAll(18);

                    if ( $onm_datas !== null )
                    {
                        foreach($onm_datas as $onm)
                        {
                            $onm->notify( new SiteDocumentUploadMail($document_data, $onm->name));

                        }
                    }
                }

                # Planner 19
                if ( $SiteDocumentApproval->approval_role == 19 )
                {
                    $planner_datas = $this->EmailUsers->getUserListFromRoleAll(19);

                    if ( $planner_datas !== null )
                    {
                        foreach($planner_datas as $planner)
                        {
                            $planner->notify( new SiteDocumentUploadMail($document_data, $planner->name));

                        }
                    }
                }

                # Purchasing 17
                if ( $SiteDocumentApproval->approval_role == 17 )
                {
                    $purchasing_datas = $this->EmailUsers->getUserListFromRoleAll(17);

                    if ( $purchasing_datas !== null )
                    {
                        foreach($purchasing_datas as $purchasing)
                        {
                            $purchasing->notify( new SiteDocumentUploadMail($document_data, $purchasing->name));

                        }
                    }
                }

                # Admin 3
                if ( $SiteDocumentApproval->approval_role == 3 )
                {
                    $admin_datas = $this->EmailUsers->getUserListFromRoleAll(3);

                    if ( $admin_datas !== null )
                    {
                        foreach($admin_datas as $admin)
                        {
                            $admin->notify( new SiteDocumentUploadMail($document_data, $admin->name));

                        }
                    }
                }

            }
        }
    }

    protected function codeOneCheck($request, $status_id, $role_id)
    {
        $code_one = 0;

        if ( $request->site_document_recurring_id != null )
        {
            $code_one = SiteDocumentApproval::Where('site_document_recurring_id', $request->site_document_recurring_id)
            ->Where('approval_role', $role_id)->Where('code_by_approval', 1)->count();
        }
        else
        {
            $code_one = SiteDocumentApproval::Where('site_document_id', $request->site_document_id)
            ->Where('approval_role', $role_id)->Where('code_by_approval', 1)->Where('for_status', $status_id)->count();
        }

        return $code_one;
    }

    protected function requireProjectOwnerApproval($req_approval_project_owner, $status_id, $request, $idOrCode)
    {
        $req_approval_project_owner_code_one = $this->codeOneCheck($request, $status_id, 8);

        if ( $req_approval_project_owner == '1' && $req_approval_project_owner_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 8);

            SiteDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT OWNER to Request for Approval using  $this->EmailUsers->getProjectOwner()
        }
    }

    protected function requireProjectManagerApproval($req_approval_project_manager, $status_id, $request, $idOrCode)
    {
        $req_approval_project_manager_code_one = $this->codeOneCheck($request, $status_id, 9);

        if ($req_approval_project_manager == '1' && $req_approval_project_manager_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 9);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT Manager to Request for Approval using
            # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
            # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
            # 3. Get $this->EmailUsers->getProjetManager($project_id)
        }
    }

    protected function requireProjectEngineerApproval($req_approval_project_engineer, $status_id, $request, $idOrCode)
    {
        $req_approval_project_engineer_code_one = $this->codeOneCheck($request, $status_id, 10);

        if ( $req_approval_project_engineer == '1' && $req_approval_project_engineer_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 10);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT Engineer to Request for Approval using
            # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
            # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
            # 3. Get $this->EmailUsers->getProjetManager($project_id)
            # 4. Get Group ID $group_id = $this->EmailUsers->getGroupIDfromSiteID($idOrCode)
            # 5. Get $this->EmailUsers->getProjectEngineer($group_id)
        }
    }

    protected function requireEngineerApproval($req_approval_engineer, $status_id, $request, $idOrCode)
    {
        $req_approval_engineer_code_one = $this->codeOneCheck($request, $status_id, 15);

        if ( $req_approval_engineer == '1' && $req_approval_engineer_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 15);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT Engineer to Request for Approval using
            # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
            # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
            # 3. Get $this->EmailUsers->getEngineerForProject($project_id)
        }
    }

    protected function requireQAQCApproval($req_approval_qa_qc, $status_id, $request, $idOrCode)
    {
        $req_approval_qa_qc_code_one = $this->codeOneCheck($request, $status_id, 14);

        if ( $req_approval_qa_qc == '1' && $req_approval_qa_qc_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 14);

            SiteDocumentApproval::insert($data);

            # SEND EMAIL to PROJECT QA&QC to Request for Approval
            # $this->EmailUsers->getUserListFromRole(14)
        }
    }

    protected function requireSafetyApproval($req_approval_safety, $status_id, $request, $idOrCode)
    {
        $req_approval_safety_code_one = $this->codeOneCheck($request, $status_id, 16);

        if ( $req_approval_safety == '1' && $req_approval_safety_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 16);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT Engineer to Request for Approval using
            # 1. Get Site ID $site_id = $this->EmailUsers->getGroupIDFrDocumentUpload($request->site_document_id)
            # 2. Get Project ID $project_id = $this->EmailUsers->getProjectIDfromSiteID($site_id)
            # 3. Get $this->EmailUsers->getSafetyForProject($project_id)
        }
    }

    protected function requireONMApproval($req_approval_onm, $status_id, $request, $idOrCode)
    {
        $req_approval_onm_code_one = $this->codeOneCheck($request, $status_id, 18);

        if ( $req_approval_onm == '1' && $req_approval_onm_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 18);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT ONM to Request for Approval
            # $this->EmailUsers->getUserListFromRole(18)
        }
    }

    protected function requirePlannerApproval($req_approval_planner, $status_id, $request, $idOrCode)
    {
        $req_approval_planner_code_one = $this->codeOneCheck($request, $status_id, 19);

        if ( $req_approval_planner == '1' && $req_approval_planner_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 19);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT Planner to Request for Approval
            # $this->EmailUsers->getUserListFromRole(19)
        }
    }

    protected function requirePurchasingApproval($req_approval_purchasing, $status_id, $request, $idOrCode)
    {
        $req_approval_purchasing_code_one = $this->codeOneCheck($request, $status_id, 17);

        if ( $req_approval_purchasing == '1' && $req_approval_purchasing_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 17);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT QA&QC to Request for Approval
            # $this->EmailUsers->getUserListFromRole(17)
        }
    }

    protected function requireAdminApproval($req_approval_admin, $status_id, $request, $idOrCode)
    {
        $req_approval_admin_code_one = $this->codeOneCheck($request, $status_id, 17);

        if ( $req_approval_admin == '1' && $req_approval_admin_code_one == 0 )
        {
            $data = $this->makeApprovalDataSet($status_id, $request, $idOrCode, 3);

            SiteDocumentApproval::insert($data);
            # SEND EMAIL to PROJECT QA&QC to Request for Approval
            # $this->EmailUsers->getUserListFromRole(17)
        }
    }

    protected function makeApprovalDataSet($status_id, $request, $idOrCode, $role_id)
    {
        return
        [
            "site_document_upload_id"       => $idOrCode,
            "site_document_id"              => $request->site_document_id,
            "site_document_recurring_id"    => $request->site_document_recurring_id,
            "for_status"                    => $status_id,
            "approval_role"                 => $role_id,
            "created_by"                    => $this->loginUserInfo['created_by']
        ];
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "site_document_upload_id"        => $request->site_document_upload_id,
            "site_document_id"               => $request->site_document_id,
            "site_document_recurring_id"     => $request->site_document_recurring_id,
            "document_version"                  => $request->document_version,
        ];
    }

    public function showUploadsByDocument($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = SiteDocumentUpload::where('site_document_id', $idOrCode)->Orderby('created_at', 'DESC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showUploadsByDocumentRecurring($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = SiteDocumentUpload::where('site_document_recurring_id', $idOrCode)->Orderby('created_at', 'DESC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function downloadDocument($idOrCode)
    {
        $document_detail =  SiteDocumentUpload::where('site_document_upload_id', $idOrCode)->first();

        $path = storage_path($this->backend_path.$document_detail->document_file);

        return response()->download($path);
    }

    public function checkAllowUpload($idOrCode, $document_type)
    {
        $returnYesOrNo = "YES";

        if ( $document_type == "ONE-TIME" )
        {
            $countUpload = SiteDocumentUpload::where('site_document_id', $idOrCode)->count();

            if ( $countUpload == 0 )
            {
                $returnYesOrNo = "YES";
            }
            else
            {
                $returnYesOrNo = "NO";

                $count = SiteDocumentApproval::where('site_document_id', $idOrCode)
                ->whereNull('code_by_approval')->count();

                if ( $count == 0 )
                {
                    $returnYesOrNo = "YES";

                    $siteDocument = SiteDocument::where('site_document_id', $idOrCode)->first();

                    if ( $siteDocument->asb_flag == 1 )
                    {
                        $last_site_document_upload = SiteDocumentApproval::where('site_document_id', $idOrCode)->where('for_status', '3')->orderBy('site_document_upload_id', 'DESC')->first();
                    }
                    else
                    {
                        $last_site_document_upload = SiteDocumentApproval::where('site_document_id', $idOrCode)->where('for_status', '2')->orderBy('site_document_upload_id', 'DESC')->first();
                    }

                    if ($last_site_document_upload != NULL)
                    {
                        $returnYesOrNo = "NO";

                        $countXY = SiteDocumentApproval::where('site_document_upload_id', $last_site_document_upload->site_document_upload_id)
                        ->whereNull('code_by_approval')->count();

                        if ( $countXY == 0 )
                        {
                            $returnYesOrNo = "YES";

                            $checkRevisionRejectCountXY = SiteDocumentApproval::where('site_document_upload_id', $last_site_document_upload->site_document_upload_id)
                            ->whereNOTIn('code_by_approval', ['1'])->count();

                            if ($checkRevisionRejectCountXY == 0)
                            {
                                $returnYesOrNo = "NO";
                            }
                            else
                            {
                                $returnYesOrNo = "YES";
                            }
                        }
                    }
                    else
                    {
                        $returnYesOrNo = "YES";
                    }
                }
                else
                {
                    $returnYesOrNo = "NO";
                }
            }
        }

        if ( $document_type == "RECURRING" )
        {
            $countUpload = SiteDocumentUpload::where('site_document_recurring_id', $idOrCode)->count();

            if ( $countUpload == 0 )
            {
                $returnYesOrNo = "YES";
            }
            else
            {
                $returnYesOrNo = "NO";

                $count = SiteDocumentApproval::where('site_document_recurring_id', $idOrCode)
                ->whereNull('code_by_approval')->count();

                if ( $count == 0 )
                {
                    $returnYesOrNo = "YES";

                    $last_site_document_upload = SiteDocumentApproval::where('site_document_recurring_id', $idOrCode)->orderBy('site_document_upload_id', 'DESC')->first();

                    if ($last_site_document_upload != NULL)
                    {
                        $returnYesOrNo = "NO";

                        $countX = SiteDocumentApproval::where('site_document_upload_id', $last_site_document_upload->site_document_upload_id)
                        ->whereNull('code_by_approval')->count();

                        if ( $countX == 0 )
                        {
                            $returnYesOrNo = "YES";

                            $checkRevisionRejectCountX = SiteDocumentApproval::where('site_document_upload_id', $last_site_document_upload->site_document_upload_id)
                            ->whereNOTIn('code_by_approval', ['1'])->count();

                            if ($checkRevisionRejectCountX == 0)
                            {
                                $returnYesOrNo = "NO";
                            }
                            else
                            {
                                $returnYesOrNo = "YES";
                            }
                        }
                    }
                    else
                    {
                        $returnYesOrNo = "YES";
                    }
                }
                else
                {
                    $returnYesOrNo = "NO";
                }
            }
        }

        return $returnYesOrNo;

    }

}
