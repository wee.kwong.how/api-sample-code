<?php

namespace App\Http\Controllers\SemCorp\SiteDocument;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\ProjectDocumentTemplate;
use App\Models\SemCorp\SiteDocument;
use App\Models\SemCorp\SiteDocumentRecurring;
use App\Models\SemCorp\ProjectDocumentApproval;
use App\Models\SemCorp\ProjectDocumentUpload;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\AuditLogDocument;
use App\Models\SemCorp\View\ProjectGroupSiteDocumentView;
use Auth;
use App\Http\Resources\SemCorp\SiteDocumentResource;
use App\Http\Resources\SemCorp\SiteDocumentRecurringResource;
use Illuminate\Validation\Rule;
use DB;
use DateTime;
use App\Models\SemCorp\Dashboard\DailyOverDueProject;

class SiteDocumentController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "SITE_DOCUMENT";
    protected $primaryKey = 'site_document_id';
    protected $uniqueColumnName = "";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "COMBO";
    protected $uniqueColumnRequest = [];
    protected $uniqueComboMessage = "The Site Document Title cannot be repeat in within the same Site & Milestone. Please use a different title / Select a different Milestone.";

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'site_id',
            1 => 'document_title',
            2 => 'milestone_id',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
            $this->uniqueCombo[1] => request()->input($this->uniqueCombo[1]),
            $this->uniqueCombo[2] => request()->input($this->uniqueCombo[2]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return SiteDocumentResource::Collection($data);
    }

    protected function getListingRecord()
    {
        return SiteDocument::orderBy('document_title', 'ASC')->get();
    }

    protected function getSingleRecord($idOrCode)
    {
        return SiteDocument::where($this->primaryKey, $idOrCode)->get();
    }

    protected function checkKeyBelongToAccount($uniqueColumnRequest)
    {
        $keyCount = project_sitesite::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->count();

        $validationCheck = $keyCount >= 1 ? "YES" : "NO" ;

        return $validationCheck;
    }

    protected function getUniqueComboInsertCheck($uniqueColumnRequest)
    {
        return SiteDocument::where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        ->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return SiteDocument::where($this->primaryKey, '<>', $idOrCode)
        ->where($this->uniqueCombo[0], $uniqueColumnRequest[$this->uniqueCombo[0]])
        ->where($this->uniqueCombo[1], $uniqueColumnRequest[$this->uniqueCombo[1]])
        ->where($this->uniqueCombo[2], $uniqueColumnRequest[$this->uniqueCombo[2]])
        ->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertNewRecord($newData)
    {
        $idOrCode = SiteDocument::insertGetId($newData);

        $this->checkDocumentRecurring($idOrCode);

        return $idOrCode;
    }

    protected function insertAuditLog($changes)
    {
        AuditLogDocument::insert($changes);
    }

    protected function UpdateRecord($updateData, $idOrCode, $request)
    {
        $destinationData = SiteDocument::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->document_title                = $request->document_title;
        $destinationData->document_mandatory            = $request->document_mandatory;
        $destinationData->milestone_id                  = $request->milestone_id;
        $destinationData->contractor_id                 = $request->contractor_id;
        $destinationData->assign_to_user                = $request->assign_to_user;
        $destinationData->document_category_id          = $request->document_category_id;
        $destinationData->document_type_id              = $request->document_type_id;
        $destinationData->recurring_interval_id         = $request->recurring_interval_id;
        $destinationData->recurring_start_date          = $request->recurring_start_date;
        $destinationData->recurring_end_date            = $request->recurring_end_date;
        $destinationData->status_id                     = $request->status_id;
        $destinationData->req_approval_project_owner    = $request->req_approval_project_owner;
        $destinationData->req_approval_project_manager  = $request->req_approval_project_manager;
        $destinationData->req_approval_project_engineer = $request->req_approval_project_engineer;
        $destinationData->req_approval_engineer         = $request->req_approval_engineer;
        $destinationData->req_approval_qa_qc            = $request->req_approval_qa_qc;
        $destinationData->req_approval_safety           = $request->req_approval_safety;
        $destinationData->req_approval_onm              = $request->req_approval_onm;
        $destinationData->req_approval_planner          = $request->req_approval_planner;
        $destinationData->req_approval_purchasing       = $request->req_approval_purchasing;
        $destinationData->req_approval_admin            = $request->req_approval_admin;
        $destinationData->asb_flag                      = $request->asb_flag;

        $destinationData->save();

        if ( $destinationData->document_type_id == 2 )
        {
            $destinationData->status_id                     = null;
            $destinationData->save();

            $this->checkDocumentRecurring($idOrCode);
        }

        if ( $destinationData->document_type_id == 1 )
        {
            if ( $destinationData->document_category_id == 1 )
            {
                $destinationData->status_id                     = 1;
            }
            else
            {
                $destinationData->status_id                     = null;
            }
            $destinationData->save();
            SiteDocumentRecurring::where($this->primaryKey, $idOrCode)->delete();
        }

        $changed = $destinationData->getChanges();

        $this->makeAuditTrailLog($original, $changed, $idOrCode, 'site_documents');
    }

    protected function checkDocumentRecurring($idOrCode)
    {
        $siteDocumentData = SiteDocument::where('site_document_id', $idOrCode)->first();

        if ( $siteDocumentData->document_type_id == 2 )
        {
            $this->createRecurring($siteDocumentData);
        }
    }

    protected function createRecurring($siteDocumentData)
    {
        if ( $siteDocumentData->recurring_interval_id == 1 )
        {
            $recurringInterval = '+1 day';
        }
        if ( $siteDocumentData->recurring_interval_id == 2 )
        {
            $recurringInterval = '+1 week';
        }
        if ( $siteDocumentData->recurring_interval_id == 3 )
        {
            $recurringInterval = '+1 month';
        }

        $begin = new DateTime( $siteDocumentData->recurring_start_date );
        $end   = new DateTime( $siteDocumentData->recurring_end_date );

        $all_interval_array = [];

        for($i = $begin; $i <= $end; $i->modify($recurringInterval))
        {
            array_push($all_interval_array, $i->format('Y-m-d'));

            $checkDataExistCount = SiteDocumentRecurring::where('document_recurring_date', $i)
            ->where('site_document_id', $siteDocumentData->site_document_id)->count();

            if ( $checkDataExistCount == 0 )
            {
                $data =
                [
                    'site_document_id'          => $siteDocumentData->site_document_id,
                    'document_recurring_date'   => $i,
                    'current_status'            => 1,
                    'created_by'                => $this->loginUserInfo['created_by'],
                    'document_mandatory'        => $siteDocumentData->document_mandatory,
                    'milestone_id'              => $siteDocumentData->milestone_id,
                ];

                SiteDocumentRecurring::insert($data);
            }
            else
            {
                $unchange = SiteDocumentRecurring::where('document_recurring_date', $i)
                ->where('site_document_id', $siteDocumentData->site_document_id)->first();

                $data_mandatory =
                [
                    'milestone_id'              => $siteDocumentData->milestone_id,
                    'document_mandatory'        => $siteDocumentData->document_mandatory,
                ];

                SiteDocumentRecurring::where('site_document_recurring_id', $unchange->site_document_recurring_id)->update($data_mandatory);
            }

        }

        SiteDocumentRecurring::where('site_document_id', $siteDocumentData->site_document_id)
        ->whereNotIn('document_recurring_date', array_values($all_interval_array))->delete();


    }

    protected function softDeleteRecord($idOrCode)
    {
        $model = new DailyOverDueProject();

        $model->hydrate(DB::select(" CALL `sp_delete_site_document`( '" . $idOrCode . "' , '" . $this->loginUserInfo['created_by'] . "' ) "));
    }

    protected function validationStoreRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_id'                           => 'required|exists:project_sites,site_id',
            'document_title'	                => 'required|min:1|max:100',
            'document_mandatory'		        => 'required|min:1|max:1|boolean',
            'contractor_id'                     => 'nullable|exists:project_contractors,contractor_id',
            'assign_to_user'                    => 'required|exists:users,id',
            'milestone_id'                      => 'required|exists:project_milestones,milestone_id',
            'document_category_id'              => 'required|exists:document_categories,document_category_id',
            'document_type_id'                  => 'required|exists:document_types,document_type_id',
            'recurring_interval_id'             => 'nullable|exists:recurring_intervals,recurring_interval_id',
            'recurring_start_date'              => 'nullable|date',
            'recurring_end_date'                => 'nullable|date|after_or_equal:recurring_start_date',
            'status_id'                         => 'nullable|exists:status_documents,status_id',
            'req_approval_project_owner'        => 'required|min:1|max:1|boolean',
            'req_approval_project_manager'      => 'required|min:1|max:1|boolean',
            'req_approval_project_engineer'     => 'required|min:1|max:1|boolean',
            'req_approval_engineer'             => 'required|min:1|max:1|boolean',
            'req_approval_qa_qc'                => 'required|min:1|max:1|boolean',
            'req_approval_safety'               => 'required|min:1|max:1|boolean',
            'req_approval_planner'              => 'required|min:1|max:1|boolean',
            'req_approval_purchasing'           => 'required|min:1|max:1|boolean',
            'req_approval_admin'                => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'site_document_id'                  => 'required|exists:site_documents,site_document_id',
            'site_id'                           => 'required|exists:project_sites,site_id',
            'document_title'	                => 'required|min:1|max:100',
            'document_mandatory'		        => 'required|min:1|max:1|boolean',
            'contractor_id'                     => 'nullable|exists:project_contractors,contractor_id',
            'assign_to_user'                    => 'required|exists:users,id',
            'milestone_id'                      => 'required|exists:project_milestones,milestone_id',
            'document_category_id'              => 'required|exists:document_categories,document_category_id',
            'document_type_id'                  => 'required|exists:document_types,document_type_id',
            'recurring_interval_id'             => 'nullable|exists:recurring_intervals,recurring_interval_id',
            'recurring_start_date'              => 'nullable|date',
            'recurring_end_date'                => 'nullable|date|after_or_equal:recurring_start_date',
            'status_id'                         => 'nullable|exists:status_documents,status_id',
            'req_approval_project_owner'        => 'required|min:1|max:1|boolean',
            'req_approval_project_manager'      => 'required|min:1|max:1|boolean',
            'req_approval_project_engineer'     => 'required|min:1|max:1|boolean',
            'req_approval_engineer'             => 'required|min:1|max:1|boolean',
            'req_approval_qa_qc'                => 'required|min:1|max:1|boolean',
            'req_approval_safety'               => 'required|min:1|max:1|boolean',
            'req_approval_planner'              => 'required|min:1|max:1|boolean',
            'req_approval_purchasing'           => 'required|min:1|max:1|boolean',
            'req_approval_admin'                => 'required|min:1|max:1|boolean',
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeNewRecordDataSet($request, $created_by)
    {
        $additionalDataSet =
        [
            "created_by"            => $created_by,
        ];

        return array_merge($this->standardDataSet($request), $additionalDataSet);
    }

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "site_document_id"                      => $request->site_document_id,
            "site_id"                               => $request->site_id,
            "document_title"                        => $request->document_title,
            "contractor_id"                         => $request->contractor_id,
            "assign_to_user"                        => $request->assign_to_user,
            "document_mandatory"                    => $request->document_mandatory,
            "milestone_id"                          => $request->milestone_id,
            "document_category_id"                  => $request->document_category_id,
            "document_type_id"                      => $request->document_type_id,
            "recurring_interval_id"                 => $request->recurring_interval_id,
            "recurring_start_date"                  => $request->recurring_start_date,
            "recurring_end_date"                    => $request->recurring_end_date,
            "status_id"                             => $request->status_id,
            "req_approval_project_owner"            => $request->req_approval_project_owner,
            "req_approval_project_manager"          => $request->req_approval_project_manager,
            "req_approval_project_engineer"         => $request->req_approval_project_engineer,
            "req_approval_engineer"                 => $request->req_approval_engineer,
            "req_approval_qa_qc"                    => $request->req_approval_qa_qc,
            "req_approval_safety"                   => $request->req_approval_safety,
            "req_approval_onm"                      => $request->req_approval_onm,
            "req_approval_planner"                  => $request->req_approval_planner,
            "req_approval_purchasing"               => $request->req_approval_purchasing,
            "req_approval_admin"                    => $request->req_approval_admin,
            "asb_flag"                              => $request->asb_flag,
        ];
    }

    public function showDocumentbySite($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = ProjectGroupSiteDocumentView::where('site_id', $idOrCode)->Orderby('document_title', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showDocumentbyProject($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data   = ProjectGroupSiteDocumentView::where('project_id', $idOrCode)->Orderby('document_title', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showDocumentbyGroup($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data   = ProjectGroupSiteDocumentView::where('group_id', $idOrCode)->Orderby('document_title', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function showDocumentbyUser($idOrCode)
    {
        $validationCheck = $this->checkForUserAccess( $this->loginUserInfo['role'], $this->moduleCode, 'listing' );

        if ( $validationCheck == "YES" )
        {
            $data = SiteDocument::where('assign_to_user', $idOrCode)->Orderby('document_title', 'ASC')->get();

            $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successReadMessageResponse);
        }
        return response($this->response, $this->status);
    }

    public function checkAllowChangeASBFlag($idOrCode)
    {
        $returnYesOrNo = "YES";

        $siteDocumentData = SiteDocument::where('site_document_id', $idOrCode)->first();

        if ( $siteDocumentData->completed_flag == 1 )
        {
            $returnYesOrNo = "NO";
        }
        else
        {
            if ( $siteDocumentData->asb_flag == 1 )
            {
                if ( $siteDocumentData->status_id == 3 )
                {
                    $returnYesOrNo = "NO";
                }
                else
                {
                    $returnYesOrNo = "YES";
                }
            }
            else
            {
                if ( $siteDocumentData->status_id == 1 OR $siteDocumentData->status_id == 2 )
                {
                    $returnYesOrNo = "YES";
                }
                else
                {
                    $returnYesOrNo = "NO";
                }
            }
        }

        return $returnYesOrNo;
    }

}
