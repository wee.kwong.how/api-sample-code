<?php

namespace App\Http\Controllers\SemCorp\Dashboard;

use App\Http\Controllers\SEMCorp_API_RoleController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\Dashboard\DailyOverDueProject;
use App\Models\SemCorp\Dashboard\DailyOverDueProjectReturn;
use App\Models\SemCorp\Dashboard\ConsolidatedDailyOverDueProject;
use App\Models\SemCorp\Dashboard\ProjectSummaryMilestones;
use App\Http\Resources\SemCorp\Dashboard\ProjectOverdueResource;
use App\Http\Resources\SemCorp\Dashboard\OverdueTurnOnResource;
use App\Http\Resources\SemCorp\Dashboard\ProjectTurnOnResource;
use App\Http\Resources\SemCorp\Dashboard\DocumentSummaryResource;
use App\Http\Resources\SemCorp\Dashboard\DocumentOverdueResource;
use App\Http\Resources\SemCorp\Dashboard\TopOverdueProjectResource;
use App\Http\Resources\SemCorp\Dashboard\TopOverdueDocumentResource;
use Auth;
use Illuminate\Validation\Rule;
use DB;

class ConsolidatedDashboardController extends SEMCorp_API_RoleController
{
    protected $moduleCode = "PROJECT";
    protected $primaryKey = 'project_id';
    protected $uniqueColumnName = "project_name";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $uploadRequest;

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
        ];
	}

    public function topOverdueDocument($project_type_id, $date)
    {
        $model = new DailyOverDueProjectReturn();

        $data = $model->hydrate(DB::select(" CALL `sp_collect_top_overdue_document`( " . $project_type_id . " , '" . $date . "' ) "));

        $this->response = $this->apiResponse->makeResponseData(TopOverdueDocumentResource::Collection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function topOverdueProject($project_type_id, $date)
    {
        $model = new DailyOverDueProjectReturn();

        $data = $model->hydrate(DB::select(" CALL `sp_collect_top_overdue_project`( " . $project_type_id . " , '" . $date . "' ) "));

        $this->response = $this->apiResponse->makeResponseData(TopOverdueProjectResource::Collection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function projectSummary($project_type_id, $date)
    {
        $series = [];
        $label = [];
        $model = new DailyOverDueProjectReturn();

        $datas = $model->hydrate(DB::select(" CALL `sp_collect_project_milestone_turn_on`( " . $project_type_id . " , '" . $date . "' ) "));

        foreach($datas as $one_data)
        {
            array_push($series, intval($one_data->milestone_total));
            array_push($label, $one_data->milestone_code);
        }

        $data =
        [
            "series" => $series,
            "label" => $label,
        ];

        $this->response = $this->apiResponse->makeResponseData($data, $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function overDueProject($project_type_id, $date)
    {
        $model = new DailyOverDueProjectReturn();

        $data = $model->hydrate(DB::select(" CALL `sp_collect_overdue_project`( " . $project_type_id . " , '" . $date . "' ) "));

        $this->response = $this->apiResponse->makeResponseData(ProjectOverdueResource::Collection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function overDueTurnOn($project_type_id, $date)
    {
        $model = new DailyOverDueProjectReturn();

        $data = $model->hydrate(DB::select(" CALL `sp_collect_overdue_turn_on`( " . $project_type_id . " , '" . $date . "' ) "));

        $this->response = $this->apiResponse->makeResponseData(OverdueTurnOnResource::Collection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function projectTurnOn($project_type_id, $date)
    {
        $model = new DailyOverDueProjectReturn();

        $data = $model->hydrate(DB::select(" CALL `sp_collect_project_turn_on`( " . $project_type_id . " , '" . $date . "' ) "));

        $this->response = $this->apiResponse->makeResponseData(ProjectTurnOnResource::Collection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function documentSummary($project_type_id, $date)
    {
        $model = new DailyOverDueProjectReturn();

        $data = $model->hydrate(DB::select(" CALL `sp_collect_document_summary`( " . $project_type_id . " , '" . $date . "' ) "));

        $this->response = $this->apiResponse->makeResponseData(DocumentSummaryResource::Collection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }

    public function documentOverdue($project_type_id, $date)
    {
        $model = new DailyOverDueProjectReturn();

        $data = $model->hydrate(DB::select(" CALL `sp_collect_document_overdue`( " . $project_type_id . " , '" . $date . "' ) "));

        $this->response = $this->apiResponse->makeResponseData(DocumentOverdueResource::Collection($data), $this->successReadMessageResponse);

        return response($this->response, $this->status);
    }
}
