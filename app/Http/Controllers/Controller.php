<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Auth;
// use App\Models\SaaS\RoleAccess;
// use App\Http\Classes\CustomResponseMessage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public $status;
    // public $response = [];
    // public $successReadMesssageResponse;
    // public $successInsertMessageResponse;
    // public $successUpdateMessageResponse;
    // public $successDeleteMessageResponse;
    // public $roleCreator;
    // public $loginUserInfo;
    // public $created_by;
    // public $validationCheckationRulesNew;
    // public $apiResponse;

    // protected function initialParameterSetup()
    // {
    //     $this->status = config('response.status.success');
    //     $this->successReadMessageResponse = config('response.message.success.read');
    //     $this->successInsertMessageResponse = config('response.message.success.insert');
    //     $this->successUpdateMessageResponse = config('response.message.success.update');
    //     $this->successDeleteMessageResponse = config('response.message.success.delete');

    //     $this->roleCreator = config('user_access.roles.role_creator');

    //     $this->middleware(function ($request, $next) {

    //         $this->loginUserInfo =
	// 	    [
	// 		  "created_by"		        => Auth::user()->id,
    //           "name"		            => Auth::user()->name,
    //           "role"		            => Auth::user()->role_id,
	// 	    ];

    //         return $next($request);
    //     });

    //     $this->apiResponse = new CustomResponseMessage();
    // }

    // protected function checkForUserAccess( $loginUserRole, $moduleCode, $accessType )
    // {
    //     $accessColumn = config("user_access.access_type.".$accessType);

    //     $roleAccessCount = RoleAccess::where('role_id', $loginUserRole)->where('module_code', $moduleCode)
    //                        ->where($accessColumn, 1)->count();

    //     if ( $roleAccessCount == 1 )
    //     {
    //         $validationCheck = "YES";
    //     }
    //     else
    //     {
    //         $this->response = $this->apiResponse->setNoAccess();
    //         $this->status = config('response.status.no_access');
    //         $validationCheck = "NO";
    //     }

    //     return $validationCheck;
    // }

    // protected function validateCodeOrIdWithUniqueColumn($idOrCode, $uniqueColumnName)
    // {
    //     $recordCheck = $this->getSingleRecord($idOrCode);

    //     if ( $recordCheck->count() == 0 )
    //     {
    //         $this->response = $this->apiResponse->setNoIdOrCodeFound();
    //         $this->status = config('response.status.error');
    //         $validationCheck = 'NO';
    //     }
    //     else
    //     {
    //         $uniqueCheck = $this->getUnqiueColumnCheck($idOrCode, $uniqueColumnName);

    //         if ( $uniqueCheck->count() == 0 )
    //         {
    //             $validationCheck = 'YES';
    //         }
    //         else
    //         {
    //             $this->response = $this->apiResponse->setColumnNotUnique();
    //             $this->status = config('response.status.error');
    //             $validationCheck =  'NO';
    //         }
    //     }

    //     return $validationCheck;
    // }

    // protected function validateCodeOrId($idOrCode)
    // {
    //     $recordCheck = $this->getSingleRecord($idOrCode);

    //     if ( $recordCheck->count() == 0 )
    //     {
    //         $this->response = $this->apiResponse->setNoIdOrCodeFound();
    //         $this->status = config('response.status.error');
    //         $validationCheck = 'NO';
    //     }
    //     else
    //     {
    //         $validationCheck = 'YES';
    //     }

    //     return $validationCheck;
    // }

    // protected function createNewRecord($request, $created_by)
    // {
    //     $newData = $this->makeNewRecordDataSet($request, $created_by);

    //     $idOrCode = $this->insertNewRecord($newData);

    //     $data = $this->getSingleRecord($idOrCode);

    //     $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successInsertMessageResponse);
    // }

    // protected function updateExistingRecord($request, $idOrCode)
    // {
    //     $updateData = $this->makeUpdateRecordDataSet($request);

    //     $this->UpdateRecord($updateData, $idOrCode);

    //     $data = $this->getSingleRecord($idOrCode);

    //     $this->response = $this->apiResponse->makeResponseData($this->makeResoucesCollection($data), $this->successUpdateMessageResponse);
    // }

    // /** Check for similar deleted data */
    // protected function check_for_deleted_customer($customer_email)
    // {
    //     $check_email = Customer::where('deleted_status', 1)->where('customer_email', $customer_email)->get();

    //     if ( $check_email->count() > 0 )
    //     {
    //         $this->response =
    //         [
    //             'message' => config('response.message.error.invalid_data'),
    //             'errors' =>
    //             [
    //                 'customer_email' => [
    //                     "The customer email belong to a deleted customer in database. To use please enabled it back instead of create a new one."
    //                 ]
    //             ]
    //         ];

    //         $this->status = config('response.status.error');

    //         return $valid = "NO";
    //     }
    //     else
    //     {
    //         return $valid = "YES";
    //     }
    // }

    // /** Delete function for any module that can allow to delete */
    // public function delete_customer($customer_id)
    // {
    //     $login_user_info 	= $this->login_user_info();

    //     $valid = $this->check_for_user_access( $login_user_info, $this->module_code, 'delete');

    //     if ( $valid == "YES" )
    //     {
    //         $customer = $this->get_single_customer($customer_id);

    //         if ( $customer->count() == 0 )
    //         {
    //             $this->response =
    //             [
    //                 'message' => config('response.message.error.invalid_data'),
    //                 'errors' =>
    //                 [
    //                     'customer_id' => [
    //                         "The customer id doesn't not exist in database or has been deleted."
    //                     ]
    //                 ]
    //             ];
    //             $this->status = config('response.status.error');
    //         }
    //         else
    //         {
    //             $customer_data =
    //             [
    //                 "deleted_status"            => True,
    //                 "deleted_date"              => NOW(),
    //             ];

    //             Customer::where('customer_id', $customer_id)->update($customer_data);

    //             /** In future there will be lots of table that need to be updated to deleted status here
    //              *  Few that I can think of :
    //              *  1. User.
    //              *  2. Subscription Account.
    //              *  3. All Checklist Table.
    //              *  4. All Form Table.
    //              *  5. All Customer Form Table.
    //              *  6. All Task Table.
    //              *  7. All the Duty Roster Table.
    //              *
    //              *  We can do this later if nessarry.
    //              */

    //             $data_response = CustomerResource::Collection($customer);
    //             $this->response =
    //             [
    //                 'data' => $data_response,
    //                 'message' => $this->delete_message_response
    //             ];
    //         }
    //     }

    //     return response($this->response, $this->status);
    // }
}
