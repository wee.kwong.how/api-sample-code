<?php

namespace App\Http\Controllers;

use App\Http\Controllers\SEMCorp_APIController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SemCorp\AuditLogSetup;
use Auth;
use App\Http\Resources\UserResource;
use Illuminate\Validation\Rule;
use DB;

class MyProfileController extends SEMCorp_APIController
{

    protected $moduleCode = "USER";
    protected $primaryKey = 'id';
    protected $uniqueColumnName = "email";
    protected $uniqueCombo = [];
    protected $uniqueOrNotSetting = "YES";
    protected $uniqueColumnRequest = [];
    protected $tableName = 'users';

    public function __construct()
	{
        $this->initialParameterSetup();

        $this->uniqueCombo =
        [
            0 => 'email',
        ];
	}

    protected function uniqueColumnRequest($request)
    {
        $this->uniqueColumnRequest =
        [
            $this->uniqueCombo[0] => request()->input($this->uniqueCombo[0]),
        ];
    }

    protected function makeResoucesCollection($data)
    {
        return UserResource::Collection($data);
    }

    protected function getSingleRecord($idOrCode)
    {
        return User::where($this->primaryKey, $idOrCode)->get();
    }

    protected function getUnqiueColumnCheck($idOrCode, $uniqueColumnRequest)
    {
        return User::where($this->primaryKey, '<>', $idOrCode)->where($this->uniqueColumnName, $uniqueColumnRequest[$this->uniqueCombo[0]])->get();
    }

    protected function getChildTableCheck($idOrCode)
    {
        return $validationCheck = "YES";
    }

    protected function insertAuditLog($changes)
    {
        AuditLogSetup::insert($changes);
    }

    protected function UpdateRecord($request, $idOrCode)
    {
        $destinationData = User::where($this->primaryKey, $idOrCode)->first();

        $original = $destinationData->getOriginal();

        $destinationData->password      = bcrypt($request['password']);
        $destinationData->name          = $request['name'];

        $destinationData->save();

        $changed = $destinationData->getChanges();

        //$this->makeAuditTrailLog($original, $changed, $idOrCode, $this->tableName);
    }

    protected function validationUpdateRequest($request)
	{
		$this->validationCheckationRulesNew =
		[
            'name'           => 'required',
            'password'              => [
                'required',
                'string',
                'min:8',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
		];

		$this->validate($request, $this->validationCheckationRulesNew);
	}

    protected function makeUpdateRecordDataSet($request)
    {
        return $this->standardDataSet($request);
    }

    protected function standardDataSet($request)
    {
        return
        [
            "name"              => $request->name,
            "password"          => $request->password
        ];
    }
}
