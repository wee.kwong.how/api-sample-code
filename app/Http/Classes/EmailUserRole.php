<?php

namespace App\Http\Classes;
use App\Models\SemCorp\Role;
use App\Models\User;
use App\Models\SemCorp\Project;
use App\Models\SemCorp\ProjectGroup;
use App\Models\SemCorp\ProjectSite;
use App\Models\SemCorp\Procedure\DailyAssignInfo;
use App\Models\SemCorp\ProjectDocument;
use App\Models\SemCorp\ProjectDocumentApproval;
use App\Models\SemCorp\SiteDocument;
use App\Models\SemCorp\SiteDocumentApproval;
use App\Models\SemCorp\ProjectTask;
use App\Models\SemCorp\Task;

class EmailUserRole
{

    protected $roles =
    [
        'cookie'            => 1,
        'admin'             => 3,
        'project_owner'     => 8,
        'project_manager'   => 9,
        'project_engineer'  => 10,
        'project_lead'      => 11,
        'contractor'        => 12,
        'project_member'    => 13,
        'quality_assurance' => 14,
        'engineer'          => 15,
        'safety'            => 16,
        'purchasing'        => 17,
        'onm'               => 18,
        'planner'           => 19,
    ];

    ## All Below function is For Project & Group & Site Controller Use
    public function getProjectOwner()
    {
        # Return Multiple User
        return User::where('role_id', $this->roles['project_owner'])->get();

    }

    public function getEngineerForProject($project_id)
    {
        # Return only 1 User
        $idOrCode = Project::where('project_id', $project_id)->pluck('project_engineer');

        return $this->getUserInformationSingle($idOrCode);
    }

    public function getSafetyForProject($project_id)
    {
        # Return only 1 User
        $idOrCode = Project::where('project_id', $project_id)->pluck('project_safety');

        return $this->getUserInformationSingle($idOrCode);
    }

    public function getProjetManager($project_id)
    {
        # Return only 1 User user in group
        $idOrCode =  Project::where('project_id', $project_id)->pluck('project_manager');

        return $this->getUserInformationSingle($idOrCode);
    }

    public function getProjectEngineer($group_id)
    {
        # Return only 1 User
        $idOrCode =  ProjectGroup::where('group_id', $group_id)->pluck('group_engineer');

        return $this->getUserInformationSingle($idOrCode);
    }

    public function getAssignUserFrProjectDocApproval($project_document_approval_id)
    {
        $idOrCode = ProjectDocumentApproval::where('project_document_approval_id', $project_document_approval_id)->pluck('assign_to_user');

        return $this->getUserInformation($idOrCode);
    }

    public function getAssignUserFrSiteDocApproval($site_document_approval_id)
    {
        $idOrCode = SiteDocumentApproval::where('site_document_approval_id', $site_document_approval_id)->pluck('assign_to_user');

        return $this->getUserInformation($idOrCode);
    }

    public function getProjectIDfromSiteID($idOrCode)
    {
        return ProjectSite::where('site_id', $idOrCode)->pluck('project_id');
    }

    public function getGroupIDfromSiteID($idOrCode)
    {
        return ProjectSite::where('site_id', $idOrCode)->pluck('group_id');
    }

    # Get List of daily assign to ready send by email with all the inforamtion on together (NO NEED join or user model join required). Please put in "http://{{server_link}}/assign_to_link" for LINK button
    # Please refer this following files /var/www/api-semcorp/database_script/daily_assigns_table.sql and /var/www/api-semcorp/database_script/sp_collect_daily_assign.sql

    public function getDailyAssign($date)
    {
        $model = new DailyOverDueProjectReturn();

        return $model->hydrate(DB::select(" CALL `sp_collect_daily_assign`( '" . $date . "' ) "));
    }


    public function getProjectIDFrDocumentUpload($project_document_id)
    {
        $idOrCode = ProjectDocument::where('project_document_id', $project_document_id)->pluck('project_id');

        return $idOrCode;
    }

    public function getProjectIDFrTask($task_id)
    {
        $idOrCode = ProjectTask::where('task_id', $task_id)->pluck('project_id');

        return $idOrCode;
    }

    public function getSiteIDFrTask($task_id)
    {
        $idOrCode = Task::where('task_id', $task_id)->pluck('site_id');

        return $idOrCode;
    }

    public function getSiteIDFrDocumentUpload($site_document_id)
    {
        $idOrCode = SiteDocument::where('site_document_id', $site_document_id)->pluck('site_id');

        return $idOrCode;
    }

    public function getUserInformationSingle($idOrCode)
    {
        return User::where('id', $idOrCode)->first();
    }

    public function getUserInformationAll($idOrCode)
    {
        return User::whereAIn('id', $idOrCode)->get();
    }

    public function getUserbyProject($role, $project_id)
    {
        return User::where('role_id', $role)->where('project_id', $project_id)->pluck('id');
    }

    public function getUserListFromRole($role_id)
    {
        return User::where('role_id', $role_id)->first();
    }

    public function getUserListFromRoleAll($role_id)
    {
        return User::where('role_id', $role_id)->get();
    }


}
