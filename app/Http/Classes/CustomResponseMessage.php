<?php

namespace App\Http\Classes;

class CustomResponseMessage
{
    public function setNoAccess()
    {
        return
        [
            'message' => config('response.message.no_access'),
        ];
    }

    public function setForeignKeyInChildTable($primaryKey)
    {
        $responseString = "You are not allow to delete this data the Key being USE in 1 or more CHILD Table. (e.g Site Task / Document Template Details).";

        return $this->makeErrorResponseData($primaryKey, $responseString);
    }

    public function setNoIdOrCodeFound($primaryKey)
    {
        $responseString = "The Primary Key ".str_replace('_', ' ', $primaryKey)." doesn't not exist in database or Belongs To Another Account.";

        return $this->makeErrorResponseData($primaryKey, $responseString);
    }

    public function setColumnNotUnique($uniqueColumnName)
    {
        $responseString = "The ".str_replace('_', ' ', $uniqueColumnName)." already exists database with different ID/Code/Name.";

        return $this->makeErrorResponseData($uniqueColumnName, $responseString);
    }

    public function setColumnNotUniqueCombo($responseString)
    {
        //$responseString = "The Combination Unique Foreign Key already exists database with different ID or Code (or Belongs To Another Account).";

        return $this->makeErrorResponseData('Unique_Foreign_Key', $responseString);
    }

    public function makeResponseData($dataResponse, $displayMessage)
    {
        return
        [
            'data' => $dataResponse,
            'message' => $displayMessage,
        ];
    }

    public function makeResponseDataTitle($dataResponse, $displayMessage, $dataTitle)
    {
        return
        [
            'data' => $dataResponse,
            'message' => $displayMessage,
            'dataTitle' => $dataTitle,
        ];
    }

    protected function makeErrorResponseData($responseColumn, $responseString)
    {
        return
        [
            'message' => config('response.message.error.invalid_data'),
            'errors' =>
            [
                $responseColumn => [
                    $responseString
                ]
            ]
        ];
    }

}
