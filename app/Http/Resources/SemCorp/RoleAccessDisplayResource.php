<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class RoleAccessDisplayResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'module_code'       => $this->module_code,
            'role'              => $this->role_id,
            'access_listing'    => $this->when($this->access_listing == 1, 'YES', 'NO'),
            'access_create'     => $this->when($this->access_create == 1, 'YES', 'NO'),
            'access_show'       => $this->when($this->access_show == 1, 'YES', 'NO'),
            'access_edit'       => $this->when($this->access_edit == 1, 'YES', 'NO'),
            'access_delete'     => $this->when($this->access_delete == 1, 'YES', 'NO'),
        ];
    }
}
