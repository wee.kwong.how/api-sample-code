<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectTypeResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_type_id'           => $this->project_type_id,
            'project_type'              => $this->project_type,
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('d-M-Y'),
            'updated_at'                => $this->updated_at->format('d-M-Y'),
            'active_status'             => $this->active_status,
            'active_status_display'     => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
