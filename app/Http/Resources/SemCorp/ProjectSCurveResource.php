<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectSCurveResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'results'                   => intval($this->results),
        ];
    }
}
