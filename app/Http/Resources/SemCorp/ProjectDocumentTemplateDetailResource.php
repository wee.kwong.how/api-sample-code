<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectDocumentTemplateDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_document_template_detail_id'       => $this->project_document_template_detail_id,
            'project_document_template'                 =>
            [
                'project_document_template_id'          => $this->_project_document_template->project_document_template_id,
                'document_template_name'                => $this->_project_document_template->document_template_name,
            ],
            'project_document_template_title'           => $this->project_document_template_title,
            'project_document_template_mandatory'       => $this->when($this->project_document_template_mandatory == 1, 'YES', 'NO'),
            'document_category'                         =>
            [
                'document_category_id'                  => $this->_document_category->document_category_id,
                'document_category'                     => $this->_document_category->document_category,
            ],
            'document_type'                             =>
            [
                'document_type_id'                      => $this->_document_type->document_type_id,
                'document_type_code'                    => $this->_document_type->document_type_code,
                'document_type_info'                    => $this->_document_type->document_type_info,
            ],
            'project_document_recurring'                =>
            [
                'recurring_interval_id'                 => optional($this->_recurring_interval)->recurring_interval_id,
                'document_recurring_interval'           => optional($this->_recurring_interval)->document_recurring_interval,
            ],
            'created_by'                                => new CreatedByResource($this->_created_by),
            'created_at'                                => $this->created_at->format('d-M-Y'),
            'updated_at'                                => $this->updated_at->format('d-M-Y'),
            'active_status'                             => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
            'req_approval_project_owner'                => $this->req_approval_project_owner,
            'req_approval_project_manager'              => $this->req_approval_project_manager,
            'req_approval_project_engineer'             => $this->req_approval_project_engineer,
            'req_approval_engineer'                     => $this->req_approval_engineer,
            'req_approval_qa_qc'                        => $this->req_approval_qa_qc,
            'req_approval_onm'                          => $this->req_approval_onm,
            'req_approval_safety'                       => $this->req_approval_safety,
            'req_approval_planner'                      => $this->req_approval_planner,
            'req_approval_purchasing'                   => $this->req_approval_purchasing,
            'req_approval_admin'                        => $this->req_approval_admin,
        ];
    }
}
