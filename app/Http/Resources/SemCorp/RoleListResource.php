<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class RoleListResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'id'                   => $this->role_id,
            'name'                 => $this->role_name,
            'created_by'           => new CreatedByResource($this->_created_by),
            'created_at'           => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'           => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
