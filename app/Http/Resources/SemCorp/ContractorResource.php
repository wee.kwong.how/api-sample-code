<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ContractorResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'contractor_id'                  => $this->contractor_id,
            'contractor_code'                => $this->contractor_code,
            'contractor_name'                => $this->contractor_name,
            'contractor_contact_person'      => $this->contractor_contact_person,
            'contractor_contact_number'      => $this->contractor_contact_number,
            'contractor_contact_email'       => $this->contractor_contact_email,
            'created_by'                     => new CreatedByResource($this->_created_by),
            'created_at'                     => $this->created_at->format('d-M-Y'),
            'updated_at'                     => $this->updated_at->format('d-M-Y'),
            'active_status'                  => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
