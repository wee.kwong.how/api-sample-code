<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class SiteDocumentResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'site_document_id'                  => $this->site_document_id,
            'site'                              =>
            [
                'site_id'                       => $this->_site->site_id,
                'site_name'                     => $this->_site->site_name,
                'group'                         =>
                [
                    'group_id'                  => $this->_site->_group->group_id,
                    'group_name'                => $this->_site->_group->group_name,
                    'group_engineer'            =>
                    [
                        'id'                    => $this->_site->_group->_group_engineer->id,
                        'name'                  => $this->_site->_group->_group_engineer->name,
                        'email'                 => $this->_site->_group->_group_engineer->email,
                    ],
                ],
                'project'                       =>
                [
                    'project_id'                => $this->_site->_group->_project->project_id,
                    'project_name'              => $this->_site->_group->_project->project_name,
                ],
            ],
            'document_title'                    => $this->document_title,
            'document_mandatory'                => $this->document_mandatory,
            'show_document_mandatory'           => $this->when($this->document_mandatory == 1, 'YES', 'NO'),
            'contractor'                        =>
            [
                'contractor_id'                 => optional($this->_contractor)->contractor_id,
                'contractor_code'               => optional($this->_contractor)->contractor_code,
                'contractor_name'               => optional($this->_contractor)->contractor_name,
            ],
            'assign_to_user'                    =>
            [
                'id'                            => optional($this->_assign_to_user)->id,
                'name'                          => optional($this->_assign_to_user)->name,
                'email'                         => optional($this->_assign_to_user)->email,
            ],
            'milestone'                         =>
            [
                'milestone_id'                  => $this->_milestone->milestone_id,
                'milestone_code'                => $this->_milestone->milestone_code,
                'milestone_info'                => $this->_milestone->milestone_info,
                'milestone_progress'            => $this->_milestone->milestone_progress,
            ],

            'document_category'                 =>
            [
                'document_category_id'          => $this->_document_category->document_category_id,
                'document_category'             => $this->_document_category->document_category,
            ],
            'document_type'                     =>
            [
                'document_type_id'              => $this->_document_type->document_type_id,
                'document_type_code'            => $this->_document_type->document_type_code,
                'document_type_info'            => $this->_document_type->document_type_info,
            ],
            'site_document_recurring'        =>
            [
                'recurring_interval_id'         => optional($this->_recurring_interval)->recurring_interval_id,
                'document_recurring_interval'   => optional($this->_recurring_interval)->document_recurring_interval,
            ],
            'show_recurring_start_date'         => optional($this->recurring_start_date)->format('d-M-Y'),
            'show_recurring_end_date'           => optional($this->recurring_end_date)->format('d-M-Y'),
            'recurring_start_date'              => optional($this->recurring_start_date)->format('Y-m-d'),
            'recurring_end_date'                => optional($this->recurring_end_date)->format('Y-m-d'),
            'document_status'                   =>
            [
                'status_id'                     => optional($this->_document_status)->status_id,
                'status_code'                   => optional($this->_document_status)->status_code,
                'status_info'                   => optional($this->_document_status)->status_info,
                'status_sequence'               => optional($this->_document_status)->status_sequence,
            ],
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
            'req_approval_project_owner'        => $this->req_approval_project_owner,
            'req_approval_project_manager'      => $this->req_approval_project_manager,
            'req_approval_project_engineer'     => $this->req_approval_project_engineer,
            'req_approval_engineer'             => $this->req_approval_engineer,
            'req_approval_qa_qc'                => $this->req_approval_qa_qc,
            'req_approval_safety'               => $this->req_approval_safety,
            'req_approval_onm'                  => $this->req_approval_onm,
            'req_approval_planner'              => $this->req_approval_planner,
            'req_approval_purchasing'           => $this->req_approval_purchasing,
            'req_approval_admin'                => $this->req_approval_admin,
            'completed_flag'                    => $this->completed_flag,
            'uploaded_flag'                     => $this->when($this->uploaded_flag == 1, 'YES', 'NO'),
            'asb_flag'                          => $this->asb_flag,
            'asb_flag_display'                  => $this->when($this->asb_flag == 1, 'YES', 'NO'),
        ];
    }
}
