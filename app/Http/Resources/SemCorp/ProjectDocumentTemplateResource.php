<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectDocumentTemplateResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_document_template_id'  => $this->project_document_template_id,
            'document_template_name'        => $this->document_template_name,
            'document_details_no'           => $this->document_details_no,
            'created_by'                    => new CreatedByResource($this->_created_by),
            'created_at'                    => $this->created_at->format('d-M-Y'),
            'updated_at'                    => $this->updated_at->format('d-M-Y'),
        ];
    }
}
