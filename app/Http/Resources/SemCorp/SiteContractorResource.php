<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class SiteContractorResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'site_contractor_id'            => $this->site_contractor_id,
            'project'                       =>
            [
                'project_id'                => $this->_site->_group->_project->project_id,
                'project_code'              => $this->_site->_group->_project->project_code,
                'project_name'              => $this->_site->_group->_project->project_name,
            ],
            'group'                         =>
            [
                'group_id'                  => $this->_site->_group->group_id,
                'group_code'                => $this->_site->_group->group_code,
                'group_name'                => $this->_site->_group->group_name,
            ],
            'site'                          =>
            [
                'site_id'                   => $this->_site->site_id,
                'site_code'                 => $this->_site->site_code,
                'site_name'                 => $this->_site->site_name,
            ],
            'contractor'                    =>
            [
                'contractor_id'             => $this->_contractor->contractor_id,
                'contractor_code'           => $this->_contractor->contractor_code,
                'contractor_name'           => $this->_contractor->contractor_name,
            ],
            'created_by'                    => new CreatedByResource($this->_created_by),
            'created_at'                    => $this->created_at->format('d-M-Y'),
            'updated_at'                    => $this->updated_at->format('d-M-Y'),
            'active_status'                 => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
