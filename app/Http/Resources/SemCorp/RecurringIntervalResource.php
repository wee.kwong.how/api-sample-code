<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class RecurringIntervalResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'recurring_interval_id'             => $this->recurring_interval_id,
            'document_recurring_interval'       => $this->document_recurring_interval,
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
        ];
    }
}
