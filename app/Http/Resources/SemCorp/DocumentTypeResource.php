<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class DocumentTypeResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'document_type_id'      => $this->document_type_id,
            'document_type_code'    => $this->document_type_code,
            'document_type_info'    => $this->document_type_info,
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('d-M-Y'),
            'updated_at'            => $this->updated_at->format('d-M-Y'),
        ];
    }
}
