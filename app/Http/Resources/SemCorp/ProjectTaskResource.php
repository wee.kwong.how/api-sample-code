<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectTaskResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'task_id'                           => $this->task_id,
            'project'                           =>
            [
                'project_id'            => $this->_project->project_id,
                'project_name'          => $this->_project->project_name,
                'project_manager'       =>
                [
                    'id'                => $this->_project->_project_manager->id,
                    'name'              => $this->_project->_project_manager->name,
                    'email'             => $this->_project->_project_manager->email,
                ],
            ],
            'task_title'                        => $this->task_title,
            'assign_to_user'                    =>
            [
                'id'                    => optional($this->_assign_to_user)->id,
                'name'                  => optional($this->_assign_to_user)->name,
                'email'                 => optional($this->_assign_to_user)->email,
            ],
            'task_description'                  => $this->when($this->task_description == null, null, $this->task_description),
            'task_remarks'                      => $this->when($this->task_remarks == null, null, $this->task_remarks),
            //'task_progress'                     => intval($this->task_progress),
            'task_progress'                     => $this->task_progress,
            'task_est_start_date'               => $this->task_est_start_date->format('Y-m-d'),
            'task_est_end_date'                 => $this->task_est_end_date->format('Y-m-d'),
            'task_start_date'                   => optional($this->task_start_date)->format('Y-m-d'),
            'task_end_date'                     => optional($this->task_end_date)->format('Y-m-d'),
            'upload_attachment'                 => $this->when($this->upload_attachment == null, null, $this->upload_attachment),
            'status'                            =>
            [
                'status_id'             => $this->_task_status->status_id,
                'status_code'           => $this->_task_status->status_code,
                'status_info'           => $this->_task_status->status_info,
            ],
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
        ];
    }
}
