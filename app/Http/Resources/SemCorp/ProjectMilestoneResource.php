<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectMilestoneResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'milestone_id'                      => $this->milestone_id,
            'project'                           =>
            [
                'project_id'        => $this->_project->project_id,
                'project_code'      => $this->_project->project_code,
                'project_name'      => $this->_project->project_name,
                'project_manager'   =>
                [
                    'id'            => $this->_project->_project_manager->id,
                    'name'          => $this->_project->_project_manager->name,
                    'email'         => $this->_project->_project_manager->email,
                ],
            ],
            'milestone_code'                    => $this->milestone_code,
            'milestone_info'                    => $this->milestone_info,
            'milestone_progress'                => $this->milestone_progress,
            'milestone_sequence'                => $this->milestone_sequence,
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
            'active_status'                     => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
