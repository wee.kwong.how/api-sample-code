<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectDocumentUploadResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_document_upload_id'            => $this->project_document_upload_id,
            'project_document'                      =>
            [
                'project_document_id'               => $this->_project_document->project_document_id,
                'document_title'                    => $this->_project_document->document_title,
                'document_mandatory'                => $this->_project_document->document_mandatory,
                'project'                           =>
                [
                    'project_id'                    => $this->_project_document->_project->project_id,
                    'project_name'                  => $this->_project_document->_project->project_name,
                    'project_manager'               =>
                    [
                        'id'                        => $this->_project_document->_project->_project_manager->id,
                        'name'                      => $this->_project_document->_project->_project_manager->name,
                        'email'                     => $this->_project_document->_project->_project_manager->email,
                    ],
                ],
                'document_category'                 =>
                [
                    'document_category_id'          => $this->_project_document->_document_category->document_category_id,
                    'document_category'             => $this->_project_document->_document_category->document_category,
                ],
            ],
            'project_document_recurring'            =>
            [
                'project_document_recurring_id'     => $this->project_document_recurring_id,
                //'document_recurring_date'           => optional($this->_project_document_recurring)->document_recurring_date->format('d-M-Y'),
                // 'current_status'                    =>
                // [
                //     'status_id'                     => optional($this->_project_document_recurring->_document_status)->status_id,
                //     // 'status_code'                   => optional($this->_project_document_recurring->_document_status->status_code,
                //     // 'status_info'                   => optional($this->_project_document_recurring->_document_status->status_info,
                //     // 'status_sequence'               => optional($this->_project_document_recurring->_document_status->status_sequence,
                // ],
            ],
            'document_version'                      => $this->document_version,
            'document_file'                         => $this->document_file,
            'created_by'                            => new CreatedByResource($this->_created_by),
            'upload_time'                           => $this->created_at->format('d-M-Y H:i:s'),
            'created_at'                            => $this->created_at->format('d-M-Y'),
            'updated_at'                            => $this->updated_at->format('d-M-Y'),
        ];
    }
}
