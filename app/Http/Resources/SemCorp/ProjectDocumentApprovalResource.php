<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectDocumentApprovalResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_document_approval_id'          => $this->project_document_approval_id,
            'project_document'                      =>
            [
                'project_document_id'               => $this->_project_document->project_document_id,
                'document_title'                    => $this->_project_document->document_title,
                'document_mandatory'                => $this->_project_document->document_mandatory,
                'project'                           =>
                [
                    'project_id'                    => $this->_project_document->_project->project_id,
                    'project_name'                  => $this->_project_document->_project->project_name,
                    'project_manager'               =>
                    [
                        'id'                        => $this->_project_document->_project->_project_manager->id,
                        'name'                      => $this->_project_document->_project->_project_manager->name,
                        'email'                     => $this->_project_document->_project->_project_manager->email,
                    ],
                ],
                'document_category'                 =>
                [
                    'document_category_id'          => $this->_project_document->_document_category->document_category_id,
                    'document_category'             => $this->_project_document->_document_category->document_category,
                ],
            ],
            'approval_role'                         =>
            [
                'role_id'                           => $this->_approval_role->role_id,
                'role_name'                         => $this->_approval_role->role_name,
            ],
            'project_document_recurring'            =>
            [
                'project_document_recurring_id'     => $this->project_document_recurring_id,
                // 'document_recurring_date'           => optional($this->_project_document_recurring)->document_recurring_date,
                // 'current_status'                    =>
                // [
                //     'status_id'                     => $this->_project_document_recurring->_document_status->status_id,
                //     'status_code'                   => $this->_project_document_recurring->_document_status->status_code,
                //     'status_info'                   => $this->_project_document_recurring->_document_status->status_info,
                //     'status_sequence'               => $this->_project_document_recurring->_document_status->status_sequence,
                // ],
            ],
            'project_document_upload'               =>
            [
                'project_document_upload_id'            => $this->_project_document_upload->project_document_upload_id,
                'document_version'                      => $this->_project_document_upload->document_version,
                'document_file'                         => $this->_project_document_upload->document_file,
            ],
            'for_status'                            =>
            [
                'status_id'                     => $this->_document_status->status_id,
                'status_code'                   => $this->_document_status->status_code,
                'status_info'                   => $this->_document_status->status_info,
                'status_sequence'               => $this->_document_status->status_sequence,
            ],
            'approval_by'                           =>
            [
                'id'            => optional($this->_approval_by)->id,
                'name'          => optional($this->_approval_by)->name
            ],
            'code_by_approval'                      => $this->code_by_approval,
            'approval_comments'                     => $this->approval_comments,
            'approval_attachments'                  => $this->approval_attachments,
            'created_by'                            => new CreatedByResource($this->_created_by),
            'created_at'                            => $this->created_at->format('d-M-Y'),
            'updated_at'                            => $this->updated_at->format('d-M-Y'),
        ];
    }
}
