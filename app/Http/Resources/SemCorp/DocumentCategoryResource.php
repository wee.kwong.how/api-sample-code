<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class DocumentCategoryResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'document_category_id'  => $this->document_category_id,
            'document_category'     => $this->document_category,
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('d-M-Y'),
            'updated_at'            => $this->updated_at->format('d-M-Y'),
        ];
    }
}
