<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectTaskTemplateResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_task_template_id'  => $this->project_task_template_id,
            'task_template_name'        => $this->task_template_name,
            'task_details_no'           => $this->task_details_no,
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('d-M-Y'),
            'updated_at'                => $this->updated_at->format('d-M-Y'),
        ];
    }
}
