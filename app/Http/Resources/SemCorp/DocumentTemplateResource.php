<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class DocumentTemplateResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'document_template_id'      => $this->document_template_id,
            'document_template_name'    => $this->document_template_name,
            'document_details_no'       => $this->document_details_no,
            'milestone_template'        =>
            [
                'milestone_template_id'     => $this->_milestone_template->milestone_template_id,
                'milestone_template_name'   => $this->_milestone_template->milestone_template_name,
            ],
            'milestone_template_name'   => $this->_milestone_template->milestone_template_name,
            'created_by'                => $this->_created_by->email,
            'created_at'                => $this->created_at->format('d-M-Y'),
            'updated_at'                => $this->updated_at->format('d-M-Y'),
        ];
    }
}
