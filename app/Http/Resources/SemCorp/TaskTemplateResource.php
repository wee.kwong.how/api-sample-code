<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class TaskTemplateResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'task_template_id'      => $this->task_template_id,
            'task_template_name'    => $this->task_template_name,
            'task_details_no'       => $this->task_details_no,
            'milestone_template'        =>
            [
                'milestone_template_id'     => $this->_milestone_template->milestone_template_id,
                'milestone_template_name'   => $this->_milestone_template->milestone_template_name,
            ],
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('d-M-Y'),
            'updated_at'                => $this->updated_at->format('d-M-Y'),
        ];
    }
}
