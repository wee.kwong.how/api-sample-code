<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class DeveloperResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'developer_id'                  => $this->developer_id,
            'developer_code'                => $this->developer_code,
            'developer_name'                => $this->developer_name,
            'developer_contact_person'      => $this->developer_contact_person,
            'developer_contact_number'      => $this->developer_contact_number,
            'developer_contact_email'       => $this->developer_contact_email,
            'created_by'                    => new CreatedByResource($this->_created_by),
            'created_at'                    => $this->created_at->format('d-M-Y'),
            'updated_at'                    => $this->updated_at->format('d-M-Y'),
            'active_status'                 => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
