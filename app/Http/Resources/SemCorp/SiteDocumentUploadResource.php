<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class SiteDocumentUploadResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'site_document_upload_id'               => $this->site_document_upload_id,
            'site_document'                         =>
            [
                'site_document_id'                  => $this->_site_document->site_document_id,
                'document_title'                    => $this->_site_document->document_title,
                'document_mandatory'                => $this->_site_document->document_mandatory,
                'site'                              =>
                [
                    'site_id'                       => $this->_site_document->_site->site_id,
                    'site_name'                     => $this->_site_document->_site->site_name,
                    'group'                         =>
                    [
                        'group_id'                  => $this->_site_document->_site->_group->group_id,
                        'group_name'                => $this->_site_document->_site->_group->group_name,
                        'group_engineer'            =>
                        [
                            'id'                    => $this->_site_document->_site->_group->_group_engineer->id,
                            'name'                  => $this->_site_document->_site->_group->_group_engineer->name,
                            'email'                 => $this->_site_document->_site->_group->_group_engineer->email,
                        ],
                    ],
                ],
                'document_category'                 =>
                [
                    'document_category_id'          => $this->_site_document->_document_category->document_category_id,
                    'document_category'             => $this->_site_document->_document_category->document_category,
                ],
            ],
            'site_document_recurring'               =>
            [
                'site_document_recurring_id'        => $this->site_document_recurring_id,
                //'document_recurring_date'           => optional($this->_site_document_recurring)->document_recurring_date->format('d-M-Y'),
                // 'current_status'                    =>
                // [
                //     'status_id'                     => optional($this->_site_document_recurring->_document_status)->status_id,
                //     // 'status_code'                   => optional($this->_site_document_recurring->_document_status->status_code,
                //     // 'status_info'                   => optional($this->_site_document_recurring->_document_status->status_info,
                //     // 'status_sequence'               => optional($this->_site_document_recurring->_document_status->status_sequence,
                // ],
            ],
            'document_version'                      => $this->document_version,
            'document_file'                         => $this->document_file,
            'created_by'                            => new CreatedByResource($this->_created_by),
            'upload_time'                           => $this->created_at->format('d-M-Y H:i:s'),
            'created_at'                            => $this->created_at->format('d-M-Y'),
            'updated_at'                            => $this->updated_at->format('d-M-Y'),
        ];
    }
}
