<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class TaskResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'task_id'                           => $this->task_id,
            'site_id'                           => $this->site_id,
            'project'                           =>
            [
                'project_id'            => $this->_site->_group->_project->project_id,
                'project_name'          => $this->_site->_group->_project->project_name,
                'project_manager'       =>
                [
                    'id'                => $this->_site->_group->_project->_project_manager->id,
                    'name'              => $this->_site->_group->_project->_project_manager->name,
                    'email'             => $this->_site->_group->_project->_project_manager->email,
                ],
            ],
            'group'                             =>
            [
                'group_id'              => $this->_site->_group->group_id,
                'group_name'            => $this->_site->_group->group_name,
                'group_engineer'        =>
                [
                    'id'                => $this->_site->_group->_group_engineer->id,
                    'name'              => $this->_site->_group->_group_engineer->name,
                    'email'             => $this->_site->_group->_group_engineer->email,
                ],
            ],
            'site'                              =>
            [
                'site_id'               => $this->_site->site_id,
                'site_name'             => $this->_site->site_name,

            ],
            'task_title'                        => $this->task_title,
            'milestone'                         =>
            [
                'milestone_id'          => $this->_milestone->milestone_id,
                'milestone_code'        => $this->_milestone->milestone_code,
                'milestone_info'        => $this->_milestone->milestone_info,
                'milestone_progress'    => $this->_milestone->milestone_progress,
            ],
            'assign_to_user'                    =>
            [
                'id'                    => optional($this->_assign_to_user)->id,
                'name'                  => optional($this->_assign_to_user)->name,
                'email'                 => optional($this->_assign_to_user)->email,
            ],
            'contractor'                        =>
            [
                'contractor_id'         => optional($this->_contractor)->contractor_id,
                'contractor_code'       => optional($this->_contractor)->contractor_code,
                'contractor_name'       => optional($this->_contractor)->contractor_name,
            ],
            'task_description'                  => $this->when($this->task_description == null, null, $this->task_description),
            'task_remarks'                      => $this->when($this->task_remarks == null, null, $this->task_remarks),
            'task_progress'                     => intval($this->task_progress),
            'task_est_start_date'               => $this->task_est_start_date->format('Y-m-d'),
            'task_est_end_date'                 => $this->task_est_end_date->format('Y-m-d'),
            'task_start_date'                   => optional($this->task_start_date)->format('Y-m-d'),
            'task_end_date'                     => optional($this->task_end_date)->format('Y-m-d'),
            'status'                            =>
            [
                'status_id'             => $this->_task_status->status_id,
                'status_code'           => $this->_task_status->status_code,
                'status_info'           => $this->_task_status->status_info,
            ],
            'upload_attachment'                 => $this->when($this->upload_attachment == null, null, $this->upload_attachment),
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
        ];
    }
}
