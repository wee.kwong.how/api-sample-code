<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class TaskStatusResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'status_id'             => $this->status_id,
            'status_code'           => $this->status_code,
            'status_info'           => $this->status_info,
            'status_sequence'       => $this->status_sequence,
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('d-M-Y'),
            'updated_at'            => $this->updated_at->format('d-M-Y'),
            'active_status'         => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
