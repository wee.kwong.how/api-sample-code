<?php

namespace App\Http\Resources\SemCorp;
use Illuminate\Http\Resources\Json\JsonResource;

class CreatedByResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'id'        => $this->id,
            'name'      => $this->name,
            'email'     => $this->email,
        ];
    }
}
