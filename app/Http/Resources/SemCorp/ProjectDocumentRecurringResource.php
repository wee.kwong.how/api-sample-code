<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectDocumentRecurringResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_document_recurring_id' => $this->project_document_recurring_id,
            'project_document'              =>
            [
                'project_document_id'               => $this->_project_document->project_document_id,
                'document_title'                    => $this->_project_document->document_title,
                'assign_to_user'                    => optional($this->_project_document)->assign_to_user,
                'document_category'                 =>
                [
                    'document_category_id'          => $this->_project_document->_document_category->document_category_id,
                    'document_category'             => $this->_project_document->_document_category->document_category,
                ],
                'document_type'                     =>
                [
                    'document_type_id'              => $this->_project_document->_document_type->document_type_id,
                    'document_type_code'            => $this->_project_document->_document_type->document_type_code,
                    'document_type_info'            => $this->_project_document->_document_type->document_type_info,
                ],
                'project_document_recurring'        =>
                [
                    'recurring_interval_id'         => optional($this->_project_document->_recurring_interval)->recurring_interval_id,
                    'document_recurring_interval'   => optional($this->_project_document->_recurring_interval)->document_recurring_interval,
                ],
                'show_recurring_start_date'         => optional($this->_project_document->recurring_start_date)->format('d-M-Y'),
                'show_recurring_end_date'           => optional($this->_project_document->recurring_end_date)->format('d-M-Y'),
                'recurring_start_date'              => optional($this->_project_document->recurring_start_date)->format('Y-m-d'),
                'recurring_end_date'                => optional($this->_project_document->recurring_end_date)->format('Y-m-d'),
                'req_approval_project_owner'        => $this->_project_document->req_approval_project_owner,
                'req_approval_project_manager'      => $this->_project_document->req_approval_project_manager,
                'req_approval_project_engineer'     => $this->_project_document->req_approval_project_engineer,
                'req_approval_engineer'             => $this->_project_document->req_approval_engineer,
                'req_approval_qa_qc'                => $this->_project_document->req_approval_qa_qc,
                'req_approval_safety'               => $this->_project_document->req_approval_safety,
                'req_approval_onm'                  => $this->_project_document->req_approval_onm,
                'req_approval_planner'              => $this->_project_document->req_approval_planner,
                'req_approval_purchasing'           => $this->_project_document->req_approval_purchasing,
                'req_approval_admin'                => $this->_project_document->req_approval_admin,
                'completed_flag'                    => $this->_project_document->completed_flag,
            ],
            'show_document_recurring_date'  => $this->document_recurring_date->format('d-M-Y'),
            'recurring_end_date'            => $this->document_recurring_date->format('Y-m-d'),
            'total_upload'                  => $this->total_upload,
            'current_status'                =>
            [
                'status_id'                 => $this->_document_status->status_id,
                'status_code'               => $this->_document_status->status_code,
                'status_info'               => $this->_document_status->status_info,
                'status_sequence'           => $this->_document_status->status_sequence,
            ],
            'created_by'                    => new CreatedByResource($this->_created_by),
            'created_at'                    => $this->created_at->format('d-M-Y'),
            'updated_at'                    => $this->updated_at->format('d-M-Y'),
        ];
    }
}
