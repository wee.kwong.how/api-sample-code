<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class MilestoneTemplateDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'milestone_template_detail_id'      => $this->milestone_template_detail_id,
            'milestone_template'                =>
            [
                'milestone_template_id'     => $this->milestone_template_id,
                'milestone_template_name'   => $this->_milestone_template->milestone_template_name,
            ],
            'milestone_template_title'          => $this->milestone_template_title,
            'milestone_template_sequence'       => $this->milestone_template_sequence,
            'created_by'                        => $this->_created_by->email,
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
            'active_status'                     => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
