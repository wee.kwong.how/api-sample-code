<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_id'                        => $this->project_id,
            'project_name'                      => $this->project_name,
            'project_manager'                   =>
            [
                'id'        => $this->_project_manager->id,
                'name'      => $this->_project_manager->name,
                'email'     => $this->_project_manager->email,
            ],
            'project_engineer'                   =>
            [
                'id'        => $this->_project_engineer->id,
                'name'      => $this->_project_engineer->name,
                'email'     => $this->_project_engineer->email,
            ],
            'project_safety'                   =>
            [
                'id'        => $this->_project_safety->id,
                'name'      => $this->_project_safety->name,
                'email'     => $this->_project_safety->email,
            ],
            'project_country'                   => $this->project_country,
            'project_total_installation'        => $this->project_total_installation,
            'project_pv_capacity'               => number_format($this->project_pv_capacity, 2),
            'project_total_capacity'            => number_format($this->project_total_capacity, 2),
            'project_completed_capacity'        => number_format($this->project_completed_capacity, 2),
            'project_total_task'                => $this->project_total_task,
            'project_completed_task'            => $this->project_completed_task,
            'project_total_document'            => $this->project_total_document,
            'project_completed_document'        => $this->project_completed_document,
            'project_progress'                  => intval($this->project_progress),
            'project_start_date'                => $this->project_start_date->format('Y-m-d'),
            'project_end_date'                  => $this->project_end_date->format('Y-m-d'),
            'project_ppa_start_date'            => $this->project_ppa_start_date->format('Y-m-d'),
            'project_ppa_contractual_cod_date'  => $this->project_ppa_contractual_cod_date->format('Y-m-d'),
            'project_type'                      =>
            [
                'project_type_id'               => $this->_project_type->project_type_id,
                'project_type'                  => $this->_project_type->project_type,
            ],
            'project_category'                  =>
            [
                'project_category_id'           => $this->_project_category->project_category_id,
                'project_category'              => $this->_project_category->project_category,
            ],
            'developer'                         =>
            [
                'developer_id'      => $this->_developer->developer_id,
                'developer_code'    => $this->_developer->developer_code,
                'developer_name'    => $this->_developer->developer_name,
            ],
            'status'                            =>
            [
                'status_id'         => $this->_project_status->status_id,
                'status_code'       => $this->_project_status->status_code,
                'status_info'       => $this->_project_status->status_info
            ],
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
            'project_level_total_task'          => $this->project_level_total_task,
            'project_level_completed_task'      => $this->project_level_completed_task,
            'project_level_total_document'      => $this->project_level_total_document,
            'project_level_completed_document'  => $this->project_level_completed_document,
            'project_task_template_id'          => $this->project_task_template_id,
            'project_document_template_id'      => $this->project_document_template_id,
        ];
    }
}
