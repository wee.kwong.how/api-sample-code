<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class MilestoneTemplateResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'milestone_template_id'     => $this->milestone_template_id,
            'milestone_template_name'   => $this->milestone_template_name,
            'milestone_details_no'      => $this->milestone_details_no,
            'created_by'                => $this->_created_by->email,
            'created_at'                => $this->created_at->format('d-M-Y'),
            'updated_at'                => $this->updated_at->format('d-M-Y'),
        ];
    }
}
