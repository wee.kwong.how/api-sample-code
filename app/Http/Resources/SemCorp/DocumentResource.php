<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class DocumentResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'document_id'                       => $this->document_id,
            'project'                           =>
            [
                'project_id'            => $this->_site->_group->_project->project_id,
                'project_code'          => $this->_site->_group->_project->project_code,
                'project_name'          => $this->_site->_group->_project->project_name,
                'project_manager'       =>
                [
                    'id'                => $this->_site->_group->_project->_project_manager->id,
                    'name'              => $this->_site->_group->_project->_project_manager->name,
                    'email'             => $this->_site->_group->_project->_project_manager->email,
                ],
            ],
            'group'                             =>
            [
                'group_id'              => $this->_site->_group->group_id,
                'group_code'            => $this->_site->_group->group_code,
                'group_name'            => $this->_site->_group->group_name,
                'group_engineer'        =>
                [
                    'id'                => $this->_site->_group->_group_engineer->id,
                    'name'              => $this->_site->_group->_group_engineer->name,
                    'email'             => $this->_site->_group->_group_engineer->email,
                ],
            ],
            'site'                              =>
            [
                'site_id'               => $this->_site->site_id,
                'site_code'             => $this->_site->site_code,
                'site_name'             => $this->_site->site_name,

            ],
            'document_classification_code'      => $this->document_classification_code,
            'document_information'              => $this->document_information,
            'document_mandatory'                => $this->when($this->document_mandatory == 1, 'YES', 'NO'),
            'document_version'                  => $this->document_version,
            'document_type'                     =>
            [
                'document_type_id'      => $this->_document_type->document_type_id,
                'document_type_code'    => $this->_document_type->document_type_code,
                'document_type_info'    => $this->_document_type->document_type_info,
            ],
            'milestone'                         =>
            [
                'milestone_id'          => $this->_milestone->milestone_id,
                'milestone_code'        => $this->_milestone->milestone_code,
                'milestone_info'        => $this->_milestone->milestone_info,
                'milestone_progress'    => $this->_milestone->milestone_progress,
            ],
            'status'                            =>
            [
                'status_id'             => $this->_document_status->status_id,
                'status_code'           => $this->_document_status->status_code,
                'status_info'           => $this->_document_status->status_info,
            ],
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
        ];
    }
}
