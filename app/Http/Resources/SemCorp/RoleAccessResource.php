<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleAccessResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'role_access_id'    => $this->role_access_id,
            'module'            => $this->_module->module_code,
            'role'              =>
            [
                'role_id'       => $this->_role->role_id,
                'role_name'     => $this->_role->role_name,
            ],
            'access_listing'    => $this->access_listing,
            'access_create'     => $this->access_create,
            'access_show'       => $this->access_show,
            'access_edit'       => $this->access_edit,
            'access_delete'     => $this->access_delete,
            'created_by'        => new CreatedByResource($this->_created_by),
            'created_at'        => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'        => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
