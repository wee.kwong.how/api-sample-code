<?php

namespace App\Http\Resources\SemCorp\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentSummaryResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'document_total'                 => intval($this->document_total),
            'document_pending'               => intval($this->document_pending),
            'document_completed'             => intval($this->document_completed),
            'document_overdue'               => intval($this->document_overdue),
        ];
    }
}
