<?php

namespace App\Http\Resources\SemCorp\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectOverdueResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'total_site'                    => intval($this->total_site),
            'total_overdue_below_30'        => intval($this->total_overdue_below_30),
            'total_overdue_30_to_60'        => intval($this->total_overdue_30_to_60),
            'total_overdue_60_to_90'        => intval($this->total_overdue_60_to_90),
            'total_overdue_above_90'        => intval($this->total_overdue_above_90),
        ];
    }
}
