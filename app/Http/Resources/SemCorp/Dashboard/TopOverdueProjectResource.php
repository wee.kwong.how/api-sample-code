<?php

namespace App\Http\Resources\SemCorp\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class TopOverdueProjectResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'site_name'               => optional($this->_site)->site_name,
            'overdue_mwp'             => intval($this->overdue_mwp),
        ];
    }
}
