<?php

namespace App\Http\Resources\SemCorp\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentOverdueResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'total_overdue'                 => intval($this->total_overdue),
            'total_overdue_below_30'        => intval($this->total_overdue_below_30),
            'total_overdue_30_to_60'        => intval($this->total_overdue_30_to_60),
            'total_overdue_60_to_90'        => intval($this->total_overdue_60_to_90),
            'total_overdue_above_90'        => intval($this->total_overdue_above_90),
        ];
    }
}
