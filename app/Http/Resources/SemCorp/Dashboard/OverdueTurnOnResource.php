<?php

namespace App\Http\Resources\SemCorp\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class OverdueTurnOnResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'mwp_total_overdue'             => intval($this->mwp_total_overdue),
            'mwp_below_1'                   => intval($this->mwp_below_1),
            'mwp_1_to_5'                    => intval($this->mwp_1_to_5),
            'mwp_5_to_10'                   => intval($this->mwp_5_to_10),
            'mwp_above_10'                  => intval($this->mwp_above_10),
        ];
    }
}
