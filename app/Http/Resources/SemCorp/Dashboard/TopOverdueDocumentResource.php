<?php

namespace App\Http\Resources\SemCorp\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class TopOverdueDocumentResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'site_name'               => optional($this->_site)->site_name,
            'total_overdue_document'  => intval($this->total_overdue_document),
        ];
    }
}
