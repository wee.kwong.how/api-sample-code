<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;
use storage;

class DocumentDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'document_detail_id'                => $this->document_detail_id,
            'document'                          =>
            [
                'document_id'                       => $this->_document->document_id,
                'document_classification_code'      => $this->_document->document_classification_code,
                'document_information'              => $this->_document->document_information,
                'document_mandatory'                => $this->when($this->_document->document_mandatory == 1, 'YES', 'NO'),
                'document_version'                  => $this->_document->document_version,
                'document_type'                     =>
                [
                    'document_type_id'      => $this->_document->_document_type->document_type_id,
                    'document_type_code'    => $this->_document->_document_type->document_type_code,
                    'document_type_info'    => $this->_document->_document_type->document_type_info,
                ],
                'milestone'                         =>
                [
                    'milestone_id'          => $this->_document->_milestone->milestone_id,
                    'milestone_code'        => $this->_document->_milestone->milestone_code,
                ],
                'status'                            =>
                [
                    'status_id'             => $this->_document->_document_status->status_id,
                    'status_code'           => $this->_document->_document_status->status_code,
                    'status_info'           => $this->_document->_document_status->status_info,
                ],
            ],
            'document_information'                  => $this->document_information,
            'document_version'                      => $this->document_version,
            'document_file'                         => $this->document_file,
            'document_comment_no'                   => $this->document_comment_no,
            'created_by'                        => new CreatedByResource($this->_created_by),
            'upload_time'                       => $this->created_at->format('d-M-Y H:i:s'),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
        ];
    }
}
