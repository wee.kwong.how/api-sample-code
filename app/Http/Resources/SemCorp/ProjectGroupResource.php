<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectGroupResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'group_id'                          => $this->group_id,
            'project'                           =>
            [
                'project_id'        => $this->_project->project_id,
                'project_code'      => $this->_project->project_code,
                'project_name'      => $this->_project->project_name,
                'project_manager'   =>
                [
                    'id'            => $this->_project->_project_manager->id,
                    'name'          => $this->_project->_project_manager->name,
                    'email'         => $this->_project->_project_manager->email,
                ],
            ],
            'group_code'                        => $this->group_code,
            'group_name'                        => $this->group_name,
            'group_information'                 => $this->group_information,
            'group_location'                    => $this->group_location,
            'group_pv_capacity'                 => number_format($this->group_pv_capacity, 2),
            'group_engineer'                    =>
            [
                'id'                => $this->_group_engineer->id,
                'name'              => $this->_group_engineer->name,
                'email'             => $this->_group_engineer->email,
            ],
            'group_total_installation'          => $this->group_total_installation,
            'group_total_task'                  => $this->group_total_task,
            'group_completed_task'              => $this->group_completed_task,
            'group_total_document'              => $this->group_total_document,
            'group_completed_document'          => $this->group_completed_document,
            'group_total_capacity'              => number_format($this->group_total_capacity, 2),
            'group_completed_capacity'          => number_format($this->group_completed_capacity, 2),
            'group_progress'                    => intval($this->group_progress),
            'status'                            =>
            [
                'status_id'         => $this->_group_status->status_id,
                'status_code'       => $this->_group_status->status_code,
                'status_info'       => $this->_group_status->status_info
            ],
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
        ];
    }
}
