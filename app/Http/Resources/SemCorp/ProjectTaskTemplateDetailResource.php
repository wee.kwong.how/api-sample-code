<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectTaskTemplateDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_task_template_detail_id'       => $this->project_task_template_detail_id,
            'project_task_template'                 =>
            [
                'project_task_template_id'          => $this->_project_task_template->project_task_template_id,
                'task_template_name'                => $this->_project_task_template->task_template_name,
            ],
            'project_task_title'                    => $this->project_task_title,
            'project_task_description'              => $this->project_task_description,
            'status'                                =>
            [
                'status_id'                         => $this->_task_status->status_id,
                'status_code'                       => $this->_task_status->status_code,

            ],
            'created_by'                            => new CreatedByResource($this->_created_by),
            'created_at'                            => $this->created_at->format('d-M-Y'),
            'updated_at'                            => $this->updated_at->format('d-M-Y'),
            'active_status'                         => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
