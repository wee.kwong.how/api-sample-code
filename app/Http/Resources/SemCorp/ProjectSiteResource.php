<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class ProjectSiteResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'site_id'                           => $this->site_id,
            'project'                           =>
            [
                'project_id'            => $this->_project->project_id,
                'project_name'          => $this->_project->project_name,
                'project_manager'       =>
                [
                    'id'                => $this->_project->_project_manager->id,
                    'name'              => $this->_project->_project_manager->name,
                    'email'             => $this->_project->_project_manager->email,
                ],
            ],
            'group'                             =>
            [
                'group_id'              => $this->_group->group_id,
                'group_name'            => $this->_group->group_name,
                'group_engineer'        =>
                [
                    'id'                => $this->_group->_group_engineer->id,
                    'name'              => $this->_group->_group_engineer->name,
                    'email'             => $this->_group->_group_engineer->email,
                ],
            ],
            'site_name'                         => $this->site_name,
            'site_postal_code'                  => $this->site_postal_code,
            'site_address'                      => $this->site_address,
            'site_total_installation'           => $this->site_total_installation,
            'site_total_task'                   => $this->site_total_task,
            'site_total_document'               => $this->site_total_document,
            'site_completed_document'           => $this->site_completed_document,
            'site_completed_task'               => $this->site_completed_task,
            'site_progress'                     => intval($this->site_progress),
            'site_module_quantity'              => $this->site_module_quantity,
            'site_module_capacity'              => $this->site_module_capacity,
            'site_total_capacity'               => $this->site_total_capacity,
            'site_completed_capacity'           => $this->site_completed_capacity,
            'site_pr_capacity'                  => $this->site_pr_capacity,
            'site_type_of_tension'              => $this->site_type_of_tension,
            'site_approved_load'                => $this->site_approved_load,
            'site_mssl_account_number'          => $this->site_mssl_account_number,
            'site_plant_name'                   => $this->site_plant_name,
            'site_folder_in_sftp_server'        => $this->site_folder_in_sftp_server,
            'site_monitoring_application'       => $this->site_monitoring_application,
            'site_inverter_type'                => $this->site_inverter_type,
            'site_inverter_qty'                 => $this->site_inverter_qty,
            'site_total_inverter'               => $this->site_total_inverter,
            'site_logger_type'                  => $this->site_logger_type,
            'site_total_data_logger'            => $this->site_total_data_logger,
            'site_start_date'                   => $this->site_start_date->format('Y-m-d'),
            'site_end_date'                     => $this->site_end_date->format('Y-m-d'),
            'est_turn_on_date'                  => optional($this->site_end_date)->format('d-M-Y'),
            'site_contractor_start_date'        => $this->site_contractor_start_date->format('Y-m-d'),
            'site_contractor_end_date'          => $this->site_contractor_end_date->format('Y-m-d'),
            'site_elu_end_date'                 => $this->site_elu_end_date->format('Y-m-d'),
            'site_el_expiry_date'               => $this->site_el_expiry_date->format('Y-m-d'),
            'site_target_turn_on_date'          => optional($this->site_target_turn_on_date)->format('Y-m-d'),
            'site_elu_license_number'           => $this->site_elu_license_number,
            'site_elu_status'                   => $this->site_elu_status,
            'status'                            =>
            [
                'status_id'             => $this->_group_status->status_id,
                'status_code'           => $this->_group_status->status_code,
                'status_info'           => $this->_group_status->status_info,
            ],
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
            'document_template_id'              => $this->document_template_id,
            'task_template_id'                  => $this->task_template_id,
        ];
    }
}
