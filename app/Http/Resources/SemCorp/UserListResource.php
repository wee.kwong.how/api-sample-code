<?php

namespace App\Http\Resources\SemCorp;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SemCorp\CreatedByResource;

class UserListResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'id'                        => $this->id,
            'role_id'                   => optional($this->_role)->role_id,
            'role_name'                 => optional($this->_role)->role_name,
            'contractor'                =>
            [
                'contractor_id'         => optional($this->_contractor)->contractor_id,
                'contractor_name'       => optional($this->_contractor)->contractor_name,
            ],
            'active_status'             => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
            'name'                      => $this->name,
            'email'                     => $this->email,
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'                => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
