<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class ClusterResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'cluster_id'            => $this->cluster_id,
            'project'            =>
            [
                'project_id'        => $this->_project->project_id,
                'project_code'      => $this->_project->project_code,
                'project_name'      => $this->_project->project_name,
                'project_manager'   => new CreatedByResource($this->_project->_project_manager),
            ],
            'cluster_code'          => $this->cluster_code,
            'cluster_name'          => $this->cluster_name,
            'cluster_start_date'    => $this->cluster_start_date->format('Y-m-d H:i:s'),
            'cluster_end_date'      => $this->cluster_end_date->format('Y-m-d H:i:s'),
            'cluster_status'        => $this->cluster_status,
            'cluster_progress'      => $this->cluster_progress,
            'belongs_to_account'    => new BelongsToResource($this->_belong_to_account),
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'            => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'         => $this->active_status,
        ];
    }
}
