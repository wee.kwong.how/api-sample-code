<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionAccountResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'subscription_account_id'   => $this->subscription_account_id,
            'customer'               =>
            [
                'customer_id' => $this->_customer->customer_id,
                'customer_name' => $this->_customer->customer_name,
                'customer_email' => $this->_customer->customer_email,
            ],
            // 'package_id'             => $this->package_id,
            'config_type'               => new ConfigTypeResource($this->_config_type),
            'start_date'                => $this->start_date->format('Y-m-d H:i:s'),
            'end_date'                  => $this->end_date->format('Y-m-d H:i:s'),
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'                => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'             => $this->active_status,
        ];
    }
}
