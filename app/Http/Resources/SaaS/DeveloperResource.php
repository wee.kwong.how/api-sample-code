<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class DeveloperResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'developer_id'              => $this->developer_id,
            'customer'                  => new CustomerRefResource($this->_customer),
            'developer_company_name'    => $this->developer_company_name,
            'developer_contact_person'  => $this->developer_contact_person,
            'belongs_to_account'        => new BelongsToResource($this->_belong_to_account),
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'                => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'             => $this->active_status,
        ];
    }
}
