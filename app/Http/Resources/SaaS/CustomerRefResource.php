<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerRefResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'customer_id'           => $this->customer_id,
            'customer_name'         => $this->customer_name,
            'customer_email'        => $this->customer_email,
        ];
    }
}
