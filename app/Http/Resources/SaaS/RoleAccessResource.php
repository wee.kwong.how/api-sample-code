<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleAccessResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'role_access_id'    => $this->role_access_id,
            'module_code'       => new ModuleRefResource($this->_module),
            'role'              => new RoleRefResource($this->_role),
            'access_listing'    => $this->access_listing,
            'access_create'     => $this->access_create,
            'access_show'       => $this->access_show,
            'access_edit'       => $this->access_edit,
            'access_delete'     => $this->access_delete,
            'created_by'        => new CreatedByResource($this->_created_by),
            'created_at'        => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'        => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
