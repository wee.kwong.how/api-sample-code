<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class UnitResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'unit_id'               => $this->unit_id,
            'floor'                 =>
            [
                'floor_id'          => $this->_floor->floor_id,
                'floor_code'        => $this->_floor->floor_code,
                'floor_name'        => $this->_floor->floor_name,
            ],
            'building'              =>
            [
                'building_id'       => $this->_building->building_id,
                'building_code'     => $this->_building->building_code,
                'building_name'     => $this->_building->building_name,
            ],
            'cluster'               =>
            [
                'cluster_id'        => $this->_cluster->cluster_id,
                'cluster_code'      => $this->_cluster->cluster_code,
                'cluster_name'      => $this->_cluster->cluster_name,
            ],
            'project'               =>
            [
                'project_id'        => $this->_project->project_id,
                'project_code'      => $this->_project->project_code,
                'project_name'      => $this->_project->project_name,
            ],
            'company'               =>
            [
                'company_id'        => $this->_company->company_id,
                'company_code'      => $this->_company->company_code,
                'company_name'      => $this->_company->company_name,
            ],
            'unit_code'             => $this->unit_code,
            'unit_name'             => $this->unit_name,
            'belongs_to_account'    => new BelongsToResource($this->_belong_to_account),
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'            => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'         => $this->active_status,
        ];
    }
}
