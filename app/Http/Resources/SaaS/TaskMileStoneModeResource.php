<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class TaskMileStoneModeResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'task_milestone_mode'       => $this->task_milestone_mode,
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'                => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
