<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class FloorResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'floor_id'              => $this->floor_id,
            'building'              =>
            [
                'building_id'       => $this->_building->building_id,
                'building_code'     => $this->_building->building_code,
                'building_name'     => $this->_building->building_name,
            ],
            'floor_code'            => $this->floor_code,
            'floor_name'            => $this->floor_name,
            'belongs_to_account'    => new BelongsToResource($this->_belong_to_account),
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'            => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'         => $this->active_status,
        ];
    }
}
