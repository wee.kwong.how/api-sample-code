<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class ModuleResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'module_code'           => $this->module_code,
            'module_name'           => $this->module_name,
            'module_information'    => $this->module_information,
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'            => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
