<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class ProjectResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'project_id'            => $this->project_id,
            'company'            =>
            [
                'company_id'        => $this->_company->company_id,
                'company_code'      => $this->_company->company_code,
                'company_name'      => $this->_company->company_name,
                'customer'          =>
                [
                    'customer_id'       => $this->_company->_customer->customer_id,
                    'customer_name'     => $this->_company->_customer->customer_name,
                ],
            ],
            'project_code'          => $this->project_code,
            'project_name'          => $this->project_name,
            'project_manager'       => 
            [
                'id'                => optional($this->_project_manager)->id,
                'name'              => optional($this->_project_manager)->name,
                'email'             => optional($this->_project_manager)->email,
            ],
            'developer'             =>
            [
                'developer_id'              => $this->_developer->developer_id,
                'developer_company_name'    => $this->_developer->developer_company_name,
            ],
            'project_start_date'    => $this->project_start_date->format('Y-m-d H:i:s'),
            'project_end_date'      => $this->project_end_date->format('Y-m-d H:i:s'),
            'project_status'        => $this->project_status,
            'project_progress'      => $this->project_progress,
            'belongs_to_account'    => new BelongsToResource($this->_belong_to_account),
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'            => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'         => $this->active_status,
        ];
    }
}
