<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class ModuleRefResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'module_code'           => $this->module_code,
            'module_name'           => $this->module_name,
        ];
    }
}
