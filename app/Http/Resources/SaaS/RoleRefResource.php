<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleRefResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'role_id'       => $this->role_id,
            'role_name'     => $this->role_name,
        ];
    }
}
