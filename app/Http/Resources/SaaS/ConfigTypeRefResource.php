<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class ConfigTypeRefResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'config_type_id'        => $this->config_type_id,
            'config_name'           => $this->config_name,
            'config_information'    => $this->config_information,
        ];
    }
}
