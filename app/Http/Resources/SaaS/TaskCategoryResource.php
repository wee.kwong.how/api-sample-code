<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class TaskCategoryResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'task_category_id'                  => $this->task_category_id,
            'task_category_name'                => $this->task_category_name,
            'belongs_to_account'                => new BelongsToResource($this->_belong_to_account),
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'                        => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'                     => $this->active_status,
        ];
    }
}
