<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class TaskResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'task_id'                           => $this->task_id,
            'depend_on_task'                    => 
            [
                'task_id'                       => optional($this->_depend_on_task)->task_id,
                'task_title'                    => optional($this->_depend_on_task)->task_title,
            ],
            'task_category'                     => 
            [
                'task_category_id'              => $this->_task_category->task_category_id,
                'task_category_name'            => $this->_task_category->task_category_name,
            ],
            'task_title'                        => $this->task_title,
            'task_milestone_mode'               => $this->task_milestone_mode,
            'task_milestone'                    => $this->task_milestone,
            'task_description'                  => $this->task_description,
            'task_priority'                     =>
            [
                'task_priority_id'              => $this->_task_priority->task_priority_id,
                'task_priority_name'            => $this->_task_priority->task_priority_name,
            ],
            'task_status'                       =>
            [
                'task_status_id'                => $this->_task_status->task_status_id,
                'task_status_name'              => $this->_task_status->task_status_name,
            ],
            'task_est_start_date'               => optional($this->task_est_start_date)->format('Y-m-d H:i:s'),
            'task_est_end_date'                 => optional($this->task_est_end_date)->format('Y-m-d H:i:s'),
            'task_start_date'                   => optional($this->task_start_date)->format('Y-m-d H:i:s'),
            'task_end_date'                     => optional($this->task_end_date)->format('Y-m-d H:i:s'),
            'task_remarks'                      => $this->task_remarks,
            'company'                           => 
            [
                'company_id'                    => optional($this->_company)->company_id,
                'company_name'                  => optional($this->_company)->company_name,
            ],
            'project'                           => 
            [
                'project_id'                    => optional($this->_project)->project_id,
                'project_name'                  => optional($this->_project)->project_name,
            ],
            'cluster'                           => 
            [
                'cluster_id'                    => optional($this->_cluster)->cluster_id,
                'cluster_name'                  => optional($this->_cluster)->cluster_name,
            ],
            'building'                          => 
            [
                'building_id'                   => optional($this->_building)->building_id,
                'building_name'                 => optional($this->_building)->building_name,
            ],
            'department'                        => 
            [
                'department_id'                 => optional($this->_department)->department_id,
                'department_name'               => optional($this->_department)->department_name,
            ],
            'floor'                             => 
            [
                'floor_id'                      => optional($this->_floor)->floor_id,
                'floor_name'                    => optional($this->_floor)->floor_name,
            ],
            'unit'                              =>
            [
                'unit_id'                       => optional($this->_unit)->unit_id,
                'unit_name'                     => optional($this->_unit)->unit_name,
            ],
            'assign_by'                         =>
            [
                'id'                            => optional($this->_assign_by)->id,
                'name'                          => optional($this->_assign_by)->name,
                'email'                         => optional($this->_assign_by)->email,
            ],
            'assign_to'                         =>
            [
                'id'                            => optional($this->_assign_to)->id,
                'name'                          => optional($this->_assign_to)->name,
                'email'                         => optional($this->_assign_to)->email,
            ],
            'verified_by'                       =>
            [
                'id'                            => optional($this->_verified_by)->id,
                'name'                          => optional($this->_verified_by)->name,
                'email'                         => optional($this->_verified_by)->email,
            ],
            'approved_by'                       =>
            [
                'id'                            => optional($this->_approved_by)->id,
                'name'                          => optional($this->_approved_by)->name,
                'email'                         => optional($this->_approved_by)->email,
            ],
            'final_approved_by'                 =>
            [
                'id'                            => optional($this->_final_approved_by)->id,
                'name'                          => optional($this->_final_approved_by)->name,
                'email'                         => optional($this->_final_approved_by)->email,
            ],
            'belongs_to_account'                => new BelongsToResource($this->_belong_to_account),
            'created_by'                        => new CreatedByResource($this->_created_by),
            'created_at'                        => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'                        => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
