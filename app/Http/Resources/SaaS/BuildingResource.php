<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class BuildingResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'building_id'            => $this->building_id,
            'cluster'            =>
            [
                'cluster_id'        => $this->_cluster->cluster_id,
                'cluster_code'      => $this->_cluster->cluster_code,
                'cluster_name'      => $this->_cluster->cluster_name,
            ],
            'building_code'         => $this->building_code,
            'building_name'         => $this->building_name,
            'belongs_to_account'    => new BelongsToResource($this->_belong_to_account),
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'            => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'         => $this->active_status,
        ];
    }
}
