<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;

class ConfigTypeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'config_type_id'        => $this->config_type_id,
            'config_name'           => $this->config_name,
            'config_information'    => $this->config_information,
            'created_by'            => new CreatedByResource($this->_created_by),
            'created_at'            => $this->created_at,
            'updated_at'            => $this->updated_at,
        ];
    }
}
