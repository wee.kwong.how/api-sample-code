<?php

namespace App\Http\Resources\SaaS;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CreatedByResource;

class MilestoneTemplateDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'milestone_template_detail_id'      => $this->milestone_template_detail_id,
            'milestone_template'                =>
            [
                'milestone_template_id'     => $this->milestone_template_id,
                'milestone_template_code'   => $this->_milestone_template->milestone_template_code,
            ],
            'milestone_template_detail_code'    => $this->milestone_template_detail_code,
            'milestone_template_info'           => $this->milestone_template_info,
            'belongs_to_account'                => $this->belongs_to_account,
            'created_by'                        => $this->_created_by->email,
            'created_at'                        => $this->created_at->format('d-M-Y'),
            'updated_at'                        => $this->updated_at->format('d-M-Y'),
            'active_status'                     => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
