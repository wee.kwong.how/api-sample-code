<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SaaS\CustomerRefResource;
use App\Http\Resources\SaaS\RoleRefResource;
use App\Http\Resources\SaaS\CreatedByResource;
use App\Http\Resources\SemCorp\ContractorResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'id'                        => $this->id,
            'name'                        => $this->name,
            'customer'                  => new CustomerRefResource($this->_customer),
            'role'                      => new RoleRefResource($this->_role),
            'contractor'                => new ContractorResource($this->_contractor),
            'email'                     => $this->email,
            'created_by'                => new CreatedByResource($this->_created_by),
            'created_at'                => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at'                => $this->updated_at->format('Y-m-d H:i:s'),
            'active_status'             => $this->active_status,
            'active_status_display'     => $this->when($this->active_status == 1, 'ACTIVE', 'SUSPENDED'),
        ];
    }
}
