<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use App\Models\SemCorp\Dashboard\DailyOverDueProjectReturn;
use App\Models\User;
use App\Models\SemCorp\Dashboard\DailyAssigns;

use App\Notifications\DailyAssignEmail;

class SendDailyAssignEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily-assign-email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily assignment email, input must be in y-m-d format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model = new DailyOverDueProjectReturn();

	\Log::info('daily-assign-email:send:log');

        $list = $model->hydrate(DB::select(" CALL `sp_collect_daily_assign`() "))->groupBy('assign_to_user_id');

        if ( count($list) ) {

	    \Log::info('daily-assign-email:send:list '.json_encode( $list->toArray() ) );

            foreach ($list as $key => $value) {

                $user = User::find($key);

                $user->notify(new DailyAssignEmail($value));
               // var_dump($value->toArray());

                foreach ($value as $abc)
                {
                    DailyAssigns::where('daily_assign_id', $abc['daily_assign_id'])->update(['completed_flag' => 1]);
                }
            }


            //DailyAssigns::where(, 'like', $yesterday . '%')->update(['completed_flag' => 1]);

	    \Log::info('daily-assign-email:send:done');

        }else{
	    \Log::info('daily-assign-email:send:empty');
	}
    }

}
