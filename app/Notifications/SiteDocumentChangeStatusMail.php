<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SiteDocumentChangeStatusMail extends Notification implements ShouldQueue
{
    use Queueable;
    protected $data;
    protected $send_to;

    /**
     * Create a new notification instance.
     *
     * @return void
    */

    public function __construct($data, $send_to)
    {
        $this->data = $data;
        $this->send_to = $send_to;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line('Thank you for using our application!');

        return (new MailMessage)->subject('MOMENS NOTIFICATION : Site IFA One-Time Document Status Change')->view('EmailTemplate.SiteDocumentChangeStatusMail',
        [
            'data' => $this->data,
            'send_to' => $this->send_to,
        ]);
        // return (new MailMessage)->view('EmailTemplate.ProjectChangeStatus')
        // ->with('data', $this->data);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
