<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";

    protected $fillable =
    [
        'project_id',
        'company_id',
        'project_code',
        'project_name',
        'project_manager',
        'developer_id',
        'project_start_date',
        'project_end_date',
        'project_status',
        'project_progress',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ 'project_start_date', 'project_end_date', ];

    public function _company()
    {
        return $this->belongsTo('App\Models\SaaS\Company', 'company_id', 'company_id');
        //                                                  foreign      primary
    }

    public function _project_manager()
    {
        return $this->belongsTo('App\Models\User', 'project_manager', 'id');
        //                                          foreign            primary
    }

    public function _developer()
    {
        return $this->belongsTo('App\Models\SaaS\Developer', 'developer_id', 'developer_id');
        //                                                      foreign      primary
    }

    public function _project_status()
    {
        return $this->belongsTo('App\Models\SaaS\Status', 'project_status', 'status_code');
        //                                                  foreign          primary
    }

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*

drop table if exists projects;

create table projects
(
   project_id           bigint not null auto_increment,
   company_id           bigint,
   project_code         varchar(50),
   project_name         varchar(100),
   project_manager      bigint,
   developer_id         bigint,
   project_start_date   datetime,
   project_end_date     datetime,
   project_status       varchar(50),
   project_progress     int default 0,
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   active_status        tinyint(1) default 1,
   primary key (project_id)
);

alter table projects add constraint FK_ref_projects_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table projects add constraint FK_ref_projects_company_id foreign key (company_id)
      references companies (company_id) on delete restrict on update restrict;

alter table projects add constraint FK_ref_projects_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

alter table projects add constraint FK_ref_projects_developer_id foreign key (developer_id)
      references developers (developer_id) on delete restrict on update restrict;

alter table projects add constraint FK_ref_projects_project_manager foreign key (project_manager)
      references users (id) on delete restrict on update restrict;

alter table projects add constraint FK_ref_projects_project_status foreign key (project_status)
      references status (status_code) on delete restrict on update cascade;

*/
