<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = "departments";

    protected $fillable =
    [
        'department_id',
        'building_id',
        'department_code',
        'department_name',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _building()
    {
        return $this->belongsTo('App\Models\SaaS\Building', 'building_id', 'building_id');
        //                                                      foreign      primary
    }

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists departments;

create table departments
(
   department_id        bigint not null auto_increment,
   building_id          bigint,
   department_code      varchar(50),
   department_name      varchar(100),
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   active_status        tinyint(1) default 1,
   primary key (department_id)
);

alter table departments add constraint FK_ref_departments_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table departments add constraint FK_ref_departments_building_id foreign key (building_id)
      references buildings (building_id) on delete restrict on update restrict;

alter table departments add constraint FK_ref_departments_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

*/
