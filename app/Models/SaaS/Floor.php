<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $table = "floors";

    protected $fillable =
    [
        'floor_id',
        'building_id',
        'floor_code',
        'floor_name',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _building()
    {
        return $this->belongsTo('App\Models\SaaS\Building', 'building_id', 'building_id');
        //                                                      foreign      primary
    }

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists floors;

create table floors
(
   floor_id             bigint not null auto_increment,
   building_id          bigint,
   floor_code           varchar(50),
   floor_name           varchar(100),
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   active_status        tinyint(1) default 1,
   primary key (floor_id)
);

alter table floors add constraint FK_Reference_15 foreign key (building_id)
      references buildings (building_id) on delete restrict on update restrict;

alter table floors add constraint FK_ref_floors_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table floors add constraint FK_ref_floors_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

*/
