<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = "modules";

    protected $fillable =
    [
        'module_code',
        'module_name',
        'module_information',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign       primary
    }
}

/*
drop table if exists modules;


create table modules
(
   module_code          varchar(50) not null,
   module_name          varchar(100),
   module_information   varchar(1000),
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (module_code)
);

alter table modules add constraint FK_ref_modules_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;


*/
