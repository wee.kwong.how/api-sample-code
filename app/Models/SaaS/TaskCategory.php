<?php

namespace App\Models\SaaS;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class TaskCategory extends Model
{
    use SoftDeletes;

    protected $table = "itask_categories";

    protected $fillable =
    [
        'task_category_id',
        'task_category_name',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists itask_categories;


create table itask_categories
(
   task_category_id     bigint not null auto_increment,
   task_category_name   varchar(100),
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   deleted_at           datetime,
   active_status        tinyint(1) default 1,
   primary key (task_category_id)
);

alter table itask_categories add constraint FK_ref_itask_categories_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table itask_categories add constraint FK_ref_itask_categories_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;
*/
