<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "status";

    protected $fillable =
    [
        'status_code',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }
}

/*
drop table if exists status;

create table status
(
   status_code          varchar(50) not null,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (status_code)
);

alter table status add constraint FK_ref_status_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

*/
