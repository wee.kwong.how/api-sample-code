<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class TaskMileStoneMode extends Model
{
    protected $table = "itask_milestone_modes";

    protected $fillable =
    [
        'task_milestone_mode',
        'created_by',
        'created_at',
        'updated_at',
    ];

    protected $dates = [ ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists itask_milestone_modes;

create table itask_milestone_modes
(
   task_milestone_mode  varchar(50) not null,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (task_milestone_mode)
);

alter table itask_milestone_modes comment 'This Mode is mainly for Milestone mode which can be configur';

alter table itask_milestone_modes add constraint FK_ref_task_milestone_modes_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

*/
