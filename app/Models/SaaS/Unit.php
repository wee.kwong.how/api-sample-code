<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = "units";

    protected $fillable =
    [
        'unit_id',
        'floor_id',
        'building_id',
        'cluster_id',
        'project_id',
        'company_id',
        'unit_code',
        'unit_name',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _floor()
    {
        return $this->belongsTo('App\Models\SaaS\Floor', 'floor_id', 'floor_id');
        //                                                foreign      primary
    }

    public function _building()
    {
        return $this->belongsTo('App\Models\SaaS\Building', 'building_id', 'building_id');
        //                                                      foreign      primary
    }

    public function _cluster()
    {
        return $this->belongsTo('App\Models\SaaS\Cluster', 'cluster_id', 'cluster_id');
        //                                                      foreign      primary
    }

    public function _project()
    {
        return $this->belongsTo('App\Models\SaaS\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _company()
    {
        return $this->belongsTo('App\Models\SaaS\Company', 'company_id', 'company_id');
        //                                                      foreign      primary
    }

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists units;

create table units
(
   unit_id              bigint not null auto_increment,
   floor_id             bigint,
   building_id          bigint,
   cluster_id           bigint,
   project_id           bigint,
   company_id           bigint,
   unit_code            varchar(50),
   unit_name            varchar(100),
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   active_status        tinyint(1) default 1,
   primary key (unit_id)
);

alter table units add constraint FK_ref_units_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table units add constraint FK_ref_units_building_id foreign key (building_id)
      references buildings (building_id) on delete restrict on update restrict;

alter table units add constraint FK_ref_units_cluster_id foreign key (cluster_id)
      references clusters (cluster_id) on delete restrict on update restrict;

alter table units add constraint FK_ref_units_company_id foreign key (company_id)
      references companies (company_id) on delete restrict on update restrict;

alter table units add constraint FK_ref_units_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

alter table units add constraint FK_ref_units_floor_id foreign key (floor_id)
      references floors (floor_id) on delete restrict on update restrict;

alter table units add constraint FK_ref_units_project_id foreign key (project_id)
      references projects (project_id) on delete restrict on update restrict;

*/
