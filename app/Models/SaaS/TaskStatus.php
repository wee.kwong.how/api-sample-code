<?php

namespace App\Models\SaaS;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    use SoftDeletes;

    protected $table = "itask_status";

    protected $fillable =
    [
        'task_status_id',
        'task_status_name',
        'task_status_milestone_percentage',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists itask_status;

create table itask_status
(
   task_status_id       bigint not null auto_increment,
   task_status_name     varchar(100),
   task_status_milestone_percentage int default 0,
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   deleted_at           datetime,
   active_status        tinyint(1) default 1,
   primary key (task_status_id)
);

alter table itask_status comment 'Status

- Not Started
- Pending
- WI';

alter table itask_status add constraint FK_ref_itask_status_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table itask_status add constraint FK_ref_itask_status_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;


*/
