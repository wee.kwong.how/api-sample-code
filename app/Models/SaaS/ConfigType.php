<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class ConfigType extends Model
{
    protected $table = "config_types";

    protected $fillable =
    [
        'config_type_id',
        'config_name',
        'config_information',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }
}

/*
drop table if exists config_types;

create table config_types
(
   config_type_id       bigint not null auto_increment,
   config_name          varchar(100),
   config_information   varchar(1000),
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (config_type_id)
);

alter table config_types add constraint FK_ref_config_type_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;
*/
