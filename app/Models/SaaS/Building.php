<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = "buildings";

    protected $fillable =
    [
        'building_id',
        'cluster_id',
        'building_code',
        'building_name',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _cluster()
    {
        return $this->belongsTo('App\Models\SaaS\Cluster', 'cluster_id', 'cluster_id');
        //                                                  foreign      primary
    }

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists buildings;

create table buildings
(
   building_id          bigint not null auto_increment,
   cluster_id           bigint,
   building_code        varchar(50) not null,
   building_name        varchar(100),
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   active_status        tinyint(1) default 1,
   primary key (building_id)
);

alter table buildings add constraint FK_ref_buildings_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table buildings add constraint FK_ref_buildings_cluster_id foreign key (cluster_id)
      references clusters (cluster_id) on delete restrict on update restrict;

alter table buildings add constraint FK_ref_buildings_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

*/
