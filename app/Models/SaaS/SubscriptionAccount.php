<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class SubscriptionAccount extends Model
{
    protected $table = "subscription_accounts";

    protected $fillable =
    [
        'subscription_account_id',
        'customer_id',
        // 'package_id',
        'config_type_id',
        'start_date',
        'end_date',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = ['start_date','end_date', ];

    public function _customer()
    {
        return $this->belongsTo('App\Models\SaaS\Customer', 'customer_id', 'customer_id');
        //                                          foreign      primary
    }

    public function _config_type()
    {
        return $this->belongsTo('App\Models\SaaS\ConfigType', 'config_type_id', 'config_type_id');
        //                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*

drop table if exists subscription_accounts;

create table subscription_accounts
(
   subscription_account_id bigint not null auto_increment,
   customer_id          bigint,
   package_id           bigint,
   config_type_id       bigint,
   start_date           datetime default CURRENT_TIMESTAMP,
   end_date             datetime,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   active_status        tinyint(1) default 1,
   primary key (subscription_account_id)
);

alter table subscription_accounts add constraint FK_ref_subscription_accounts_config_type_id foreign key (config_type_id)
      references config_types (config_type_id) on delete restrict on update restrict;

alter table subscription_accounts add constraint FK_ref_subscription_accounts_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

alter table subscription_accounts add constraint FK_ref_subscription_accounts_customer_id foreign key (customer_id)
      references customers (customer_id) on delete restrict on update restrict;


*/
