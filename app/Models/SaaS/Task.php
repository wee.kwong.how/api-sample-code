<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use SoftDeletes;

    protected $table = "itasks";

    protected $fillable =
    [
        'task_id',
        'depend_on_task',
        'task_category_id',
        'task_title',
        'task_milestone_mode',
        'task_milestone',
        'task_description',
        'task_priority_id',
        'task_status_id',
        'task_est_start_date',
        'task_est_end_date',
        'task_start_date',
        'task_end_date',
        'task_remarks',
        'company_id',
        'project_id',
        'cluster_id',
        'building_id',
        'department_id',
        'floor_id',
        'unit_id',
        'assign_by',
        'assign_to',
        'verified_by',
        'approved_by',
        'final_approved_by',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = [ 'task_est_start_date', 'task_est_end_date', 'task_start_date', 'task_end_date', ];

    public function _depend_on_task()
    {
        return $this->belongsTo('App\Models\SaaS\Task', 'depend_on_task', 'task_id');
        //                                                foreign             primary
    }

    public function _task_category()
    {
        return $this->belongsTo('App\Models\SaaS\TaskCategory', 'task_category_id', 'task_category_id');
        //                                                        foreign             primary
    }

    public function _task_milestone_mode()
    {
        return $this->belongsTo('App\Models\SaaS\TaskMileStoneMode', 'task_milestone_mode', 'task_milestone_mode');
        //                                                        foreign             primary
    }

    public function _task_priority()
    {
        return $this->belongsTo('App\Models\SaaS\TaskPriority', 'task_priority_id', 'task_priority_id');
        //                                                        foreign             primary
    }

    public function _task_status()
    {
        return $this->belongsTo('App\Models\SaaS\TaskStatus', 'task_status_id', 'task_status_id');
        //                                                        foreign             primary
    }

    public function _company()
    {
        return $this->belongsTo('App\Models\SaaS\Company', 'company_id', 'company_id');
        //                                                      foreign      primary
    }

    public function _project()
    {
        return $this->belongsTo('App\Models\SaaS\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _cluster()
    {
        return $this->belongsTo('App\Models\SaaS\Cluster', 'cluster_id', 'cluster_id');
        //                                                      foreign      primary
    }
   
    public function _building()
    {
        return $this->belongsTo('App\Models\SaaS\Building', 'building_id', 'building_id');
        //                                                      foreign      primary
    }

    public function _department()
    {
        return $this->belongsTo('App\Models\SaaS\Department', 'department_id', 'department_id');
        //                                                      foreign      primary
    }

    public function _floor()
    {
        return $this->belongsTo('App\Models\SaaS\Floor', 'floor_id', 'floor_id');
        //                                                foreign      primary
    }

    public function _unit()
    {
        return $this->belongsTo('App\Models\SaaS\Unit', 'unit_id', 'unit_id');
        //                                                foreign      primary
    }

   
    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

    public function _assign_by()
    {
        return $this->belongsTo('App\Models\User', 'assign_by', 'id');
        //                                          foreign      primary
    }

    public function _assign_to()
    {
        return $this->belongsTo('App\Models\User', 'assign_to', 'id');
        //                                          foreign      primary
    }

    public function _verified_by()
    {
        return $this->belongsTo('App\Models\User', 'verified_by', 'id');
        //                                          foreign      primary
    }

    public function _approved_by()
    {
        return $this->belongsTo('App\Models\User', 'approved_by', 'id');
        //                                          foreign      primary
    }

    public function _final_approved_by()
    {
        return $this->belongsTo('App\Models\User', 'final_approved_by', 'id');
        //                                          foreign      primary
    }

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign      primary
    }
}

/*

create table itasks
(
   task_id              bigint not null auto_increment,
   depend_on_task    bigint,
   task_category_id     bigint,
   task_title          varchar(200),
   task_milestone_mode  varchar(50),
   task_milestone       int default 0,
   task_description     varchar(1000),
   task_priority_id     bigint,
   task_status_id       bigint,
   task_est_start_date  datetime,
   task_est_end_date    datetime,
   task_start_date      datetime,
   task_end_date        datetime,
   task_remarks         varchar(1000),
   company_id           bigint,
   project_id           bigint,
   cluster_id           bigint,
   building_id          bigint,
   department_id        bigint,
   floor_id             bigint,
   unit_id              bigint,
   assign_by            bigint,
   assign_to            bigint,
   verified_by          bigint,
   approved_by          bigint,
   final_approved_by    bigint,
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   deleted_at           datetime,
   primary key (task_id)
);

alter table itasks add constraint FK_ref_itask_approved_by foreign key (approved_by)
      references users (id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_assign_by foreign key (assign_by)
      references users (id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_assign_to foreign key (assign_to)
      references users (id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_building_id foreign key (building_id)
      references buildings (building_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_cluster_id foreign key (cluster_id)
      references clusters (cluster_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_company_id foreign key (company_id)
      references companies (company_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_department_id foreign key (department_id)
      references departments (department_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_depend_on_task_id foreign key (depend_on_task_id)
      references itasks (task_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_final_approved_by foreign key (final_approval_by)
      references users (id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_floor_id foreign key (floor_id)
      references floors (floor_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_project_id foreign key (project_id)
      references projects (project_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_task_category_id foreign key (task_category_id)
      references itask_categories (task_category_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_task_milesotne_mode foreign key (task_milestone_mode)
      references itask_milestone_modes (task_milestone_mode) on delete restrict on update cascade;

alter table itasks add constraint FK_ref_itask_task_priority_id foreign key (task_priority_id)
      references itask_priorities (task_priority_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_task_status_id foreign key (task_status_id)
      references itask_status (task_status_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_unit_id foreign key (unit_id)
      references units (unit_id) on delete restrict on update restrict;

alter table itasks add constraint FK_ref_itask_verified_by foreign key (verified_by)
      references users (id) on delete restrict on update restrict;

*/
