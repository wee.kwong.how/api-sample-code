<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Cluster extends Model
{
    protected $table = "clusters";

    protected $fillable =
    [
        'cluster_id',
        'project_id',
        'cluster_code',
        'cluster_name',
        'cluster_start_date',
        'cluster_end_date',
        'cluster_status',
        'cluster_progress',
        'belongs_to_account',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ 'cluster_start_date', 'cluster_end_date', ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SaaS\Project', 'project_id', 'project_id');
        //                                                  foreign      primary
    }

    public function _project_status()
    {
        return $this->belongsTo('App\Models\SaaS\Status', 'cluster_status', 'status_code');
        //                                                  foreign          primary
    }

    public function _belong_to_account()
    {
        return $this->belongsTo('App\Models\SaaS\SubscriptionAccount', 'belongs_to_account', 'subscription_account_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists clusters;

create table clusters
(
   cluster_id           bigint not null auto_increment,
   project_id           bigint,
   cluster_code         varchar(50),
   cluster_name         varchar(100),
   cluster_start_date  datetime,
   cluster_end_date     datetime,
   cluster_status       varchar(50),
   cluster_progress     int default 0,
   belongs_to_account   bigint,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   active_status        tinyint(1) default 1,
   primary key (cluster_id)
);

alter table clusters add constraint FK_ref_clusters_belongs_to_account foreign key (belongs_to_account)
      references subscription_accounts (subscription_account_id) on delete restrict on update restrict;

alter table clusters add constraint FK_ref_clusters_cluster_status foreign key (cluster_status)
      references status (status_code) on delete restrict on update cascade;

alter table clusters add constraint FK_ref_clusters_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

alter table clusters add constraint FK_ref_clusters_project_id foreign key (project_id)
      references projects (project_id) on delete restrict on update restrict;



*/
