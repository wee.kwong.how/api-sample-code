<?php

namespace App\Models\SaaS;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customers";

    protected $fillable =
    [
        'customer_id',
        'customer_name',
        'contact_number',
        'customer_email',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign       primary
    }
}

/*
drop table if exists customers;

create table customers
(
   customer_id          bigint not null auto_increment,
   customer_name        varchar(300),
   contact_number       varchar(16),
   customer_email       varchar(300),
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (customer_id)
);

alter table customers add constraint FK_ref_customers_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;



*/
