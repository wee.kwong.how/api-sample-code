<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function _customer()
    // {
    //     return $this->belongsTo('App\Models\SaaS\Customer', 'customer_id', 'customer_id');
    //     //                                          foreign      primary
    // }

    public function _role()
    {
        return $this->belongsTo('App\Models\SemCorp\Role', 'role_id', 'role_id');
        //                                          foreign      primary
    }

    public function _contractor()
    {
        return $this->belongsTo('App\Models\SemCorp\Contractor', 'contractor_id', 'contractor_id');
        //                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}
