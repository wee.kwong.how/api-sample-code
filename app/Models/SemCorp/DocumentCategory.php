<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
    protected $table = "document_categories";
    protected $primaryKey = 'document_category_id';

    protected $fillable = [ ];

    protected $dates = [ ];



    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: document_categories
Columns:
document_category_id bigint AI PK
document_category varchar(50)
created_by bigint
created_at datetime
updated_at datetime
*/
