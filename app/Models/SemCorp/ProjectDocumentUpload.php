<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectDocumentUpload extends Model
{
    protected $table = "project_document_uploads";
    protected $primaryKey = 'project_document_upload_id';

    protected $dates = [ 'document_recurring_date' ];

    public function _project_document()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectDocument', 'project_document_id', 'project_document_id');
        //                                                      foreign      primary
    }

    public function _project_document_recurring()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectDocumentRecurring', 'project_document_recurring_id', 'project_document_recurring_id');
        //                                                      foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_document_uploads
Columns:
project_document_upload_id bigint AI PK
project_document_id bigint
project_document_recurring_id bigint
document_version varchar(200)
document_file varchar(1000)
created_by bigint
created_at datetime
updated_at datetime
*/
