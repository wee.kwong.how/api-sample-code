<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class AuditLogProject extends Model
{
    protected $table = "audit_log_projects";
    protected $primaryKey = 'audit_log_id';

    protected $fillable =
    [

    ];

    protected $dates = [ ];
}

/*
Table: audit_log_projects
Columns:
audit_log_id bigint AI PK
audit_table varchar(100)
audit_column varchar(100)
audit_data_id varchar(100)
audit_changed_date datetime
audit_change_by varchar(100)
audit_old_value varchar(5000)
audit_new_value varchar(5000)
*/
