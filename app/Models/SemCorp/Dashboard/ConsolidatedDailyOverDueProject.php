<?php

namespace App\Models\SemCorp\Dashboard;

use Illuminate\Database\Eloquent\Model;

class ConsolidatedDailyOverDueProject extends Model
{
    protected $table = "v_consolidated_daily_overdue_projects";

    protected $fillable =
    [

    ];

    protected $dates = [ 'for_date'];
}

/*
Table: daily_overdue_projects
Columns:
daily_overdue_project_id bigint AI PK
for_date date
project_type_id bigint
total_site bigint
total_overdue_below_30 int
total_overdue_30_to_60 int
total_overdue_60_to_90 int
total_overdue_above_90 int
created_at datetime
updated_at datetime
*/
