<?php

namespace App\Models\SemCorp\Dashboard;

use Illuminate\Database\Eloquent\Model;

class DailyOverDueProjectReturn extends Model
{
    public function _site()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectSite', 'site_id', 'site_id');
        //                                          foreign      primary
    }

    public function _user()
    {
        return $this->belongsTo('App\Models\Users', 'assign_to_user_id', 'id');
        //                                          foreign      primary
    }

}
