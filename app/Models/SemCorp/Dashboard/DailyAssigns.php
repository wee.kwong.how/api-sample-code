<?php

namespace App\Models\SemCorp\Dashboard;

use Illuminate\Database\Eloquent\Model;

class DailyAssigns extends Model
{
    protected $table = "daily_assigns";

    protected $fillable =
        [
            'completed_flag'
        ];

}

/*
Table: daily_assigns
Columns:
daily_assign_id bigint AI PK
assign_date datetime
assign_to_user_id bigint
assign_module_code varchar 100
project_id bigint
group_id bigint
assign_to_id bigint
assign_to_title bigint
assign_link bigint
created_at datetime
updated_at datetime
completed_flag tinyint 1
*/
