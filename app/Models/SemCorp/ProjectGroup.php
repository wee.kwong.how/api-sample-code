<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectGroup extends Model
{
    protected $table = "project_groups";
    protected $primaryKey = 'group_id';

    protected $fillable = [ ];

    protected $dates = [ ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _group_engineer()
    {
        return $this->belongsTo('App\Models\User', 'group_engineer', 'id');
        //                                          foreign      primary
    }

    public function _group_status()
    {
        return $this->belongsTo('App\Models\SemCorp\GroupStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_groups
Columns:
group_id bigint AI PK
project_id bigint
group_code varchar(50)
group_name varchar(300)
group_engineer bigint
group_information varchar(300)
group_location varchar(300)
group_total_installation int
group_total_task int
group_total_document int
group_progress int
status_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
