<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    protected $table = "project_types";
    protected $primaryKey = 'project_type_id';

    protected $fillable =
    [

    ];

    protected $dates = [ ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_types
Columns:
project_type_id bigint AI PK
project_type varchar(100)
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
