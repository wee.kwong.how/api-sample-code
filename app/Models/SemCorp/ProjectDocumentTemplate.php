<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectDocumentTemplate extends Model
{
    protected $table = "project_document_templates";
    protected $primaryKey = 'project_document_template_id';

    protected $dates = [ ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_document_templates
Columns:
project_document_template_id bigint AI PK
document_template_name varchar(100)
document_details_no int
created_by bigint
created_at datetime
updated_at datetime
*/
