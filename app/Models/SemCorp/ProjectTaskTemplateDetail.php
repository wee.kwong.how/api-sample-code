<?php

namespace App\Models\SemCorp;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ProjectTaskTemplateDetail extends Model
{
    use SoftDeletes;
    
    protected $table = "project_task_template_details";
    protected $primaryKey = 'project_task_template_detail_id';

    protected $fillable = [];

    protected $dates = [ ];

    public function _project_task_template()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectTaskTemplate', 'project_task_template_id', 'project_task_template_id');
        //                                                                      foreign               primary
    }

    public function _task_status()
    {
        return $this->belongsTo('App\Models\SemCorp\TaskStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_task_template_details
Columns:
project_task_template_detail_id bigint AI PK
project_task_template_id bigint
project_task_title varchar(100)
project_task_description varchar(300)
status_id bigint
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
