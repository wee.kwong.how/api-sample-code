<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class MilestoneTemplate extends Model
{
    protected $table = "milestone_templates";
    protected $primaryKey = 'milestone_template_id';

    protected $fillable =
    [
        'milestone_template_id',
        'milestone_template_code',
        'milestone_template_name',
        'milestone_details_no',
        'created_by',
        'created_at',
        'updated_at',
    ];

    protected $dates = [ ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: milestone_templates
Columns:
milestone_template_id bigint AI PK
milestone_template_code varchar(50)
milestone_template_name varchar(45)
milestone_details_no int
belongs_to_account bigint
created_by bigint
created_at datetime
updated_at datetime
*/
