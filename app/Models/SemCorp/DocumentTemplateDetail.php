<?php

namespace App\Models\SemCorp;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class DocumentTemplateDetail extends Model
{
    use SoftDeletes;
    
    protected $table = "document_template_details";
    protected $primaryKey = 'document_template_detail_id';

    protected $fillable = [ ];

    protected $dates = [ ];

    public function _document_template()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentTemplate', 'document_template_id', 'document_template_id');
        //                                                              foreign               primary
    }

    public function _document_category()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentCategory', 'document_category_id', 'document_category_id');
        //                                                              foreign               primary
    }

    public function _document_type()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentType', 'document_type_id', 'document_type_id');
        //                                                              foreign               primary
    }

    public function _recurring_interval()
    {
        return $this->belongsTo('App\Models\SemCorp\RecurringInterval', 'recurring_interval_id', 'recurring_interval_id');
        //                                                              foreign               primary
    }

    public function _milestone_template_detail()
    {
        return $this->belongsTo('App\Models\SemCorp\MilestoneTemplateDetail', 'milestone_template_detail_id', 'milestone_template_detail_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: document_template_details
Columns:
document_template_detail_id bigint AI PK
document_template_id bigint
document_template_classfication_code varchar(50)
document_template_info varchar(100)
document_template_mandatory tinyint(1)
document_type_id bigint
milestone_template_detail_id bigint
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
req_approval_project_owner tinyint(1)
req_approval_project_manager tinyint(1)
req_approval_project_engineer tinyint(1)
req_approval_engineer tinyint(1)
req_approval_qa_qc tinyint(1)
req_approval_safety tinyint(1)
req_approval_planner tinyint(1)
req_approval_purchasing tinyint(1)
req_approval_admin tinyint(1)
*/
