<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = "documents";

    protected $dates = [ ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _site()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectSite', 'site_id', 'site_id');
        //                                          foreign      primary
    }

    public function _document_type()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentType', 'document_type_id', 'document_type_id');
        //                                                              foreign               primary
    }

    public function _milestone()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectMilestone', 'milestone_id', 'milestone_id');
        //                                                          foreign      primary
    }

    public function _document_status()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }



}

/*
Table: documents
Columns:
document_id bigint AI PK
project_id bigint
site_id bigint
document_classification_code varchar(50)
document_information varchar(100)
document_version int
document_mandatory tinyint(1)
document_type_id bigint
milestone_id bigint
status_id bigint
created_by bigint
created_at datetime
updated_at datetime
from_document_template_id bigint
from_document_template_detail_id bigint
*/
