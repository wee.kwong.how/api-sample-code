<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";

    protected $fillable =
    [
        'role_id',
        'role_name',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
drop table if exists roles;


create table roles
(
   role_id              bigint not null auto_increment,
   role_name            varchar(100),
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (role_id)
);

alter table roles add constraint FK_ref_roles_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;
*/
