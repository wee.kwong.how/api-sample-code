<?php

namespace App\Models\SemCorp;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class MilestoneTemplateDetail extends Model
{
    use SoftDeletes;
    
    protected $table = "milestone_template_details";
    protected $primaryKey = 'milestone_template_detail_id';

    protected $fillable =
    [
        'milestone_template_detail_id',
        'milestone_template_id',
        'milestone_template_detail_code',
        'milestone_template_info',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _milestone_template()
    {
        return $this->belongsTo('App\Models\SemCorp\MilestoneTemplate', 'milestone_template_id', 'milestone_template_id');
        //                                                              foreign               primary
    }


    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: milestone_template_details
Columns:
milestone_template_detail_id bigint AI PK
milestone_template_id bigint
milestone_template_detail_code varchar(50)
milestone_template_info varchar(100)
belongs_to_account bigint
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
