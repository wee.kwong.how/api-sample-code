<?php

namespace App\Models\SemCorp\Procedure;

use Illuminate\Database\Eloquent\Model;

class ProjectUserListWithContractor extends Model
{

    public function _role()
    {
        return $this->belongsTo('App\Models\SemCorp\Role', 'role_id', 'role_id');
        //                                          foreign      primary
    }

    public function _contractor()
    {
        return $this->belongsTo('App\Models\SemCorp\Contractor', 'contractor_id', 'contractor_id');
        //                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }
}

/*
CREATE DEFINER=`web_db`@`%` PROCEDURE `sp_get_user_list_from_project_with_contractor`(
project_idx bigint,
myself_idx bigint,
contractor_idx bigint
)
BEGIN

	Select * from users where id = myself_idx or role_id in (8, 11, 12, 13, 14, 17, 18, 19) or id in
    (
    Select project_manager from projects where project_id = project_idx
    union all
    Select project_engineer from projects where project_id = project_idx
    union all
    Select project_safety from projects where project_id = project_idx
    );

END
*/
