<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class DocumentStatus extends Model
{
    protected $table = "status_documents";

    protected $fillable =
    [
        'status_id',
        'status_code',
        'status_info',
        'status_sequence',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];



    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: status_documents
Columns:
status_id bigint AI PK
status_code varchar(50)
status_info varchar(100)
status_sequence int
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
