<?php

namespace App\Models\SemCorp;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class TaskTemplateDetail extends Model
{
    use SoftDeletes;

    protected $table = "task_template_details";
    protected $primaryKey = 'task_template_detail_id';

    protected $fillable =
    [
        'task_template_detail_id',
        'task_template_id',
        'task_template_code',
        'task_template_info',
        'milestone_template_detail_id',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];

    public function _task_template()
    {
        return $this->belongsTo('App\Models\SemCorp\TaskTemplate', 'task_template_id', 'task_template_id');
        //                                                              foreign               primary
    }

    public function _task_status()
    {
        return $this->belongsTo('App\Models\SemCorp\TaskStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _milestone_template_detail()
    {
        return $this->belongsTo('App\Models\SemCorp\MilestoneTemplateDetail', 'milestone_template_detail_id', 'milestone_template_detail_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: task_template_details
Columns:
task_template_detail_id bigint AI PK
task_template_id bigint
task_template_code varchar(50)
task_template_info varchar(100)
milestone_template_detail_id bigint
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
