<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class DocumentComment extends Model
{
    protected $table = "document_comments";

    protected $dates = [ ];

    public function _document_detail()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentDetail', 'document_detail_id', 'document_detail_id');
        //                                                      foreign      primary
    }

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _site()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectSite', 'site_id', 'site_id');
        //                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }



}

/*
Table: document_comments

Columns:
	document_comment_id	bigint AI PK
	document_detail_id	bigint
	document_comment	varchar(1000)
	document_comment_sequence	int
	created_by	bigint
	created_at	datetime
	updated_at	datetime
*/
