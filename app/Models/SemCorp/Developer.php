<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $table = "project_developers";

    protected $fillable =
    [
        'developer_id',
        'developer_code',
        'developer_name',
        'developer_contact_person',
        'developer_contact_number',
        'developer_contact_email',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];


    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_developers
Columns:
developers_id bigint AI PK
developer_code varchar(50)
developer_name varchar(100)
developer_contact_person varchar(100)
developer_contact_number varchar(100)
developer_contact_email varchar(250)
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
