<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $table = "document_types";

    protected $fillable =
    [
        'document_type_id',
        'document_type_code',
        'document_type_info',
        'created_by',
        'created_at',
        'updated_at',
    ];

    protected $dates = [ ];



    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: document_types
Columns:
document_type_id bigint AI PK
document_type_code varchar(50)
document_type_info varchar(100)
created_by bigint
created_at datetime
updated_at datetime
*/
