<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    protected $table = "project_categories";
    protected $primaryKey = 'project_category_id';

    protected $fillable =
    [

    ];

    protected $dates = [ ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_categories
Columns:
project_category_id bigint AI PK
project_category varchar(100)
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
