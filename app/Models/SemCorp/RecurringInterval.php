<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class RecurringInterval extends Model
{
    protected $table = "recurring_intervals";
    protected $primaryKey = 'recurring_interval_id';

    protected $fillable = [ ];

    protected $dates = [ ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: document_recurring_intervals
Columns:
recurring_interval_id bigint AI PK
document_recurring_interval varchar(50)
created_by bigint
created_at datetime
updated_at datetime
*/
