<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectDocumentRecurring extends Model
{
    protected $table = "project_document_recurrings";
    protected $primaryKey = 'project_document_recurring_id';

    protected $dates = [ 'document_recurring_date' ];

    public function _project_document()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectDocument', 'project_document_id', 'project_document_id');
        //                                                      foreign      primary
    }

    public function _document_status()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentStatus', 'current_status', 'status_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_document_recurrings
Columns:
project_document_recurring_id bigint AI PK
project_document_id bigint
document_recurring_date datetime
current_status bigint
created_by bigint
created_at datetime
updated_at datetime
*/
