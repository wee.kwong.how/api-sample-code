<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class DocumentTemplate extends Model
{
    protected $table = "document_templates";
    protected $primaryKey = 'document_template_id';

    protected $fillable =
    [
        'document_template_id',
        'document_template_name',
        'document_details_no',
        'milestone_template_id',
        'created_by',
        'created_at',
        'updated_at',
    ];

    protected $dates = [ ];

    public function _milestone_template()
    {
        return $this->belongsTo('App\Models\SemCorp\MilestoneTemplate', 'milestone_template_id', 'milestone_template_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: document_templates
Columns:
document_template_id bigint AI PK
document_template_name varchar(100)
document_details_no int
milestone_template_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
