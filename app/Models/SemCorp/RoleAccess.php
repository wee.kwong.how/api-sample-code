<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    protected $table = "role_accesses";

    protected $fillable =
    [
        'role_access_id',
        'module_code',
        'role_id',
        'access_listing',
        'access_create',
        'access_show',
        'access_edit',
        'access_delete',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

    public function _module()
    {
        return $this->belongsTo('App\Models\SaaS\Module', 'module_code', 'module_code');
        //                                          foreign         primary
    }

    public function _role()
    {
        return $this->belongsTo('App\Models\SaaS\Role', 'role_id', 'role_id');
        //                                          foreign      primary
    }
}

/*

drop table if exists role_accesses;

create table role_accesses
(
   role_access_id       bigint not null auto_increment,
   module_code          varchar(50),
   role_id              bigint,
   access_listing       tinyint(1) default 1,
   access_create        tinyint(1) default 1,
   access_show          tinyint(1) default 1,
   access_edit          tinyint(1) default 1,
   access_delete        tinyint(1) default 1,
   created_by           bigint,
   created_at           datetime default CURRENT_TIMESTAMP,
   updated_at           datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (role_access_id)
);

alter table role_accesses add constraint FK_ref_role_accesses_created_by foreign key (created_by)
      references users (id) on delete restrict on update restrict;

alter table role_accesses add constraint FK_ref_role_accesses_module_code foreign key (module_code)
      references modules (module_code) on delete restrict on update restrict;

alter table role_accesses add constraint FK_ref_role_accesses_role_id foreign key (role_id)
      references roles (role_id) on delete restrict on update restrict;

*/
