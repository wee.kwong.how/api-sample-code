<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class SiteDocumentApproval extends Model
{
    protected $table = "site_document_approvals";
    protected $primaryKey = 'site_document_approval_id';

    protected $dates = [ ];

    public function _approval_role()
    {
        return $this->belongsTo('App\Models\SemCorp\Role', 'approval_role', 'role_id');
        //                                                      foreign      primary
    }

    public function _site_document()
    {
        return $this->belongsTo('App\Models\SemCorp\SiteDocument', 'site_document_id', 'site_document_id');
        //                                                              foreign         primary
    }

    public function _site_document_recurring()
    {
        return $this->belongsTo('App\Models\SemCorp\SiteDocumentRecurring', 'site_document_recurring_id', 'site_document_recurring_id');
        //                                                                  foreign      primary
    }

    public function _site_document_upload()
    {
        return $this->belongsTo('App\Models\SemCorp\SiteDocumentUpload', 'site_document_upload_id', 'site_document_upload_id');
        //                                                                  foreign                          primary
    }

    public function _document_status()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentStatus', 'for_status', 'status_id');
        //                                                              foreign               primary
    }

    public function _approval_by()
    {
        return $this->belongsTo('App\Models\User', 'approval_by', 'id');
        //                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: site_document_approvals
Columns:
site_document_approval_id bigint AI PK
site_document_id bigint
site_document_recurring_id bigint
site_document_upload_id bigint
for_status bigint
approval_role bigint
approval_by bigint
code_by_approval tinyint
approval_comments varchar(1000)
approval_attachments varchar(1000)
created_by bigint
created_at datetime
updated_at datetime
*/
