<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectTaskTemplate extends Model
{
    protected $table = "project_task_templates";
    protected $primaryKey = 'project_task_template_id';

    protected $dates = [ ];

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_task_templates
Columns:
project_task_template_id bigint AI PK
task_template_name varchar(100)
task_details_no int
created_by bigint
created_at datetime
updated_at datetime
*/
