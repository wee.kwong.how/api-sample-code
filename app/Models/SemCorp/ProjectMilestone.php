<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectMilestone extends Model
{
    protected $table = "project_milestones";
    protected $primaryKey = 'milestone_id';

    protected $dates = [ ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_milestones
Columns:
milestone_id bigint AI PK
project_id bigint
milestone_code varchar(50)
milestone_info varchar(100)
milestone_progress int
milestone_sequence int
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
from_milestone_template_id bigint
from_milestone_template_detail_id bigint
*/
