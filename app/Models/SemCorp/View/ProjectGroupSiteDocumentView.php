<?php

namespace App\Models\SemCorp\View;

use Illuminate\Database\Eloquent\Model;

class ProjectGroupSiteDocumentView extends Model
{
    protected $table = "v_project_group_site_document";

    protected $fillable =
    [

    ];

    protected $dates = [ ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _group()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectGroup', 'group_id', 'group_id');
        //                                                      foreign      primary
    }

    public function _site()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectSite', 'site_id', 'site_id');
        //                                          foreign      primary
    }

    public function _milestone()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectMilestone', 'milestone_id', 'milestone_id');
        //                                                          foreign      primary
    }

    public function _assign_to_user()
    {
        return $this->belongsTo('App\Models\User', 'assign_to_user', 'id');
        //                                                  foreign      primary
    }

    public function _contractor()
    {
        return $this->belongsTo('App\Models\SemCorp\Contractor', 'contractor_id', 'contractor_id');
        //                                                          foreign      primary
    }

    public function _document_category()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentCategory', 'document_category_id', 'document_category_id');
        //                                                              foreign               primary
    }

    public function _document_type()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentType', 'document_type_id', 'document_type_id');
        //                                                              foreign               primary
    }

    public function _recurring_interval()
    {
        return $this->belongsTo('App\Models\SemCorp\RecurringInterval', 'recurring_interval_id', 'recurring_interval_id');
        //                                                              foreign               primary
    }

    public function _document_status()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentStatus', 'status_id', 'status_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_site_contractors
Columns:
site_contractor_id bigint AI PK
site_id bigint
contractor_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
