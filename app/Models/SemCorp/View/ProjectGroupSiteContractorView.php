<?php

namespace App\Models\SemCorp\View;

use Illuminate\Database\Eloquent\Model;

class ProjectGroupSiteContractorView extends Model
{
    protected $table = "v_project_group_site_contractors";

    protected $fillable =
    [

    ];

    protected $dates = [ ];


    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _group()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectGroup', 'group_id', 'group_id');
        //                                                      foreign      primary
    }

    public function _site()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectSite', 'site_id', 'site_id');
        //                                          foreign      primary
    }

    public function _contractor()
    {
        return $this->belongsTo('App\Models\SemCorp\Contractor', 'contractor_id', 'contractor_id');
        //                                          foreign      primary
    }

}

/*
Table: project_site_contractors
Columns:
site_contractor_id bigint AI PK
site_id bigint
contractor_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
