<?php

namespace App\Models\SemCorp\View;

use Illuminate\Database\Eloquent\Model;

class ProjectDocumentRecurringView extends Model
{
    protected $table = "v_project_document_recurring_with_total_upload";
    protected $primaryKey = 'project_document_recurring_id';

    protected $dates = [ 'document_recurring_date' ];

    public function _project_document()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectDocument', 'project_document_id', 'project_document_id');
        //                                                      foreign      primary
    }

    public function _document_status()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentStatus', 'current_status', 'status_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*

*/
