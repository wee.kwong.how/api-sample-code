<?php

namespace App\Models\SemCorp\View;

use Illuminate\Database\Eloquent\Model;

class ProjectMilestoneView extends Model
{
    protected $table = "v_project_milestone_progress";
    protected $primaryKey = 'milestone_id';

    protected $dates = [ ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*

*/
