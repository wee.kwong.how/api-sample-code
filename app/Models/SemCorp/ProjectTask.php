<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model
{
    protected $table = "project_itasks";
    protected $primaryKey = 'task_id';

    protected $fillable = [] ;

    protected $dates = [ 'task_est_start_date', 'task_est_end_date', 'task_start_date', 'task_end_date'];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _assign_to_user()
    {
        return $this->belongsTo('App\Models\User', 'assign_to_user', 'id');
        //                                                  foreign      primary
    }

    public function _task_status()
    {
        return $this->belongsTo('App\Models\SemCorp\TaskStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_itasks
Columns:
task_id bigint AI PK
project_id bigint
task_title varchar(100)
assign_to_user bigint
task_description varchar(300)
task_remarks varchar(1000)
task_progress int
status_id bigint
task_est_start_date datetime
task_est_end_date datetime
task_start_date datetime
task_end_date datetime
created_by bigint
created_at datetime
updated_at datetime
from_project_task_template_id bigint
from_project_task_template_detail_id bigint
*/
