<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class SiteContractor extends Model
{
    protected $table = "project_site_contractors";

    protected $fillable =
    [
        'site_contractor_id',
        'site_id',
        'contractor_id',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];


    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

    public function _site()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectSite', 'site_id', 'site_id');
        //                                          foreign      primary
    }

    public function _contractor()
    {
        return $this->belongsTo('App\Models\SemCorp\Contractor', 'contractor_id', 'contractor_id');
        //                                          foreign      primary
    }

}

/*
Table: project_site_contractors
Columns:
site_contractor_id bigint AI PK
site_id bigint
contractor_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
