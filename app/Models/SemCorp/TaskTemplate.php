<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class TaskTemplate extends Model
{
    protected $table = "task_templates";
    protected $primaryKey = 'task_template_id';

    protected $fillable =
    [
        'task_template_id',
        'task_template_name',
        'task_details_no',
        'milestone_template_id',
        'created_by',
        'created_at',
        'updated_at',
    ];

    protected $dates = [ ];

    public function _milestone_template()
    {
        return $this->belongsTo('App\Models\SemCorp\MilestoneTemplate', 'milestone_template_id', 'milestone_template_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: task_templates
Columns:
task_template_id bigint AI PK
task_template_code varchar(50)
task_template_name varchar(100)
task_details_no int
milestone_template_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
