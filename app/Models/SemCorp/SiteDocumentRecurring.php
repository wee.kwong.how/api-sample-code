<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class SiteDocumentRecurring extends Model
{
    protected $table = "site_document_recurrings";
    protected $primaryKey = 'site_document_recurring_id';

    protected $dates = [ 'document_recurring_date' ];

    public function _site_document()
    {
        return $this->belongsTo('App\Models\SemCorp\SiteDocument', 'site_document_id', 'site_document_id');
        //                                                              foreign         primary
    }

    public function _document_status()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentStatus', 'current_status', 'status_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: site_document_recurrings
Columns:
site_document_recurring_id bigint AI PK
site_document_id bigint
document_recurring_date datetime
current_status bigint
created_by bigint
created_at datetime
updated_at datetime
*/
