<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";
    protected $primaryKey = 'project_id';

    protected $fillable = [ ];

    protected $dates = [ 'project_start_date', 'project_end_date', 'project_ppa_start_date', 'project_ppa_contractual_cod_date'];


    public function _project_category()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectCategory', 'project_category_id', 'project_category_id');
        //                                                              foreign               primary
    }

    public function _project_type()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectType', 'project_type_id', 'project_type_id');
        //                                                              foreign               primary
    }

    public function _project_manager()
    {
        return $this->belongsTo('App\Models\User', 'project_manager', 'id');
        //                                          foreign      primary
    }

    public function _project_engineer()
    {
        return $this->belongsTo('App\Models\User', 'project_engineer', 'id');
        //                                          foreign      primary
    }

    public function _project_safety()
    {
        return $this->belongsTo('App\Models\User', 'project_safety', 'id');
        //                                          foreign      primary
    }

    public function _developer()
    {
        return $this->belongsTo('App\Models\SemCorp\Developer', 'developer_id', 'developer_id');
        //                                                      foreign         primary
    }

    public function _project_status()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: projects
Columns:
project_id bigint AI PK
project_code varchar(50)
project_name varchar(300)
project_manager bigint
project_total_installation int
project_total_task int
project_total_document int
project_progress int
project_start_date datetime
project_end_date datetime
project_ppa_start_date datetime
project_ppa_contractual_cod_date datetime
developer_id bigint
status_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
