<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class check_table extends Model
{
    protected $table = "check_table";
    protected $primaryKey = 'idcheck_table';

    protected $fillable =
    [

    ];

    protected $dates = [ ];
}

/*
Table: audit_log_documents
Columns:
audit_log_id bigint AI PK
audit_table varchar(100)
audit_column varchar(100)
audit_data_id varchar(100)
audit_changed_date datetime
audit_change_by varchar(100)
audit_old_value varchar(5000)
audit_new_value varchar(5000)
*/
