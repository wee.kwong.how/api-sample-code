<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectDocument extends Model
{
    protected $table = "project_documents";
    protected $primaryKey = 'project_document_id';

    protected $dates = [ 'recurring_start_date', 'recurring_end_date' ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _assign_to_user()
    {
        return $this->belongsTo('App\Models\User', 'assign_to_user', 'id');
        //                                                  foreign      primary
    }

    public function _document_category()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentCategory', 'document_category_id', 'document_category_id');
        //                                                              foreign               primary
    }

    public function _document_type()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentType', 'document_type_id', 'document_type_id');
        //                                                              foreign               primary
    }

    public function _recurring_interval()
    {
        return $this->belongsTo('App\Models\SemCorp\RecurringInterval', 'recurring_interval_id', 'recurring_interval_id');
        //                                                              foreign               primary
    }

    public function _document_status()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentStatus', 'status_id', 'status_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_documents
Columns:
project_document_id bigint AI PK
project_id bigint
document_title varchar(100)
document_mandatory tinyint(1)
project_category_id bigint
document_type_id bigint
recurring_interval_id bigint
status_id bigint
created_by bigint
created_at datetime
updated_at datetime
req_approval_project_owner tinyint(1)
req_approval_project_manager tinyint(1)
req_approval_project_engineer tinyint(1)
req_approval_engineer tinyint(1)
req_approval_qa_qc tinyint(1)
req_approval_safety tinyint(1)
req_approval_onm tinyint(1)
req_approval_planner tinyint(1)
req_approval_purchasing tinyint(1)
req_approval_admin tinyint(1)
from_project_document_template_id bigint
from_project_document_template_detail_id bigint
*/
