<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
    protected $table = "project_contractors";

    protected $fillable =
    [
        'contractor_id',
        'contractor_code',
        'contractor_name',
        'contractor_contact_person',
        'contractor_contact_number',
        'contractor_contact_email',
        'created_by',
        'created_at',
        'updated_at',
        'active_status',
    ];

    protected $dates = [ ];


    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_contractors
Columns:
contractor_id bigint AI PK
contractor_code varchar(50)
contractor_name varchar(100)
contractor_contact_person varchar(100)
contractor_contact_number varchar(100)
contractor_contact_email varchar(250)
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
