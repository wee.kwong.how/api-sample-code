<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class SiteDocumentUpload extends Model
{
    protected $table = "site_document_uploads";
    protected $primaryKey = 'site_document_upload_id';

    protected $dates = [ 'document_recurring_date' ];

    public function _site_document()
    {
        return $this->belongsTo('App\Models\SemCorp\SiteDocument', 'site_document_id', 'site_document_id');
        //                                                              foreign         primary
    }

    public function _site_document_recurring()
    {
        return $this->belongsTo('App\Models\SemCorp\SiteDocumentRecurring', 'site_document_recurring_id', 'site_document_recurring_id');
        //                                                                  foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: site_document_uploads
Columns:
site_document_upload_id bigint AI PK
site_document_id bigint
site_document_recurring_id bigint
document_version varchar(200)
document_file varchar(1000)
created_by bigint
created_at datetime
updated_at datetime
*/
