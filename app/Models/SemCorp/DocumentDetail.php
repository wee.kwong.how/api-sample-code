<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class DocumentDetail extends Model
{
    protected $table = "document_details";

    protected $dates = [ ];

    public function _document()
    {
        return $this->belongsTo('App\Models\SemCorp\Document', 'document_id', 'document_id');
        //                                                      foreign      primary
    }

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _site()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectSite', 'site_id', 'site_id');
        //                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }



}

/*
Table: document_details

Columns:
	document_detail_id	bigint AI PK
	document_id	bigint
	document_information	varchar(300)
	document_version	int
	document_file	varchar(1000)
	document_comment_no	int
	created_by	bigint
	created_at	datetime
	updated_at	datetime


*/
