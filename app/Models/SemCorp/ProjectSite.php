<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectSite extends Model
{
    protected $table = "project_sites";
    protected $primaryKey = 'site_id';

    protected $fillable = [ ];

    protected $dates =
    [
        'site_start_date',
        'site_end_date',
        'site_contractor_start_date',
        'site_contractor_end_date',
        'site_elu_end_date',
        'site_el_expiry_date',
        'site_target_turn_on_date',
    ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    public function _group()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectGroup', 'group_id', 'group_id');
        //                                                      foreign      primary
    }

    public function _site_leader()
    {
        return $this->belongsTo('App\Models\User', 'site_leader', 'id');
        //                                          foreign      primary
    }

    public function _group_status()
    {
        return $this->belongsTo('App\Models\SemCorp\GroupStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _site_status()
    {
        return $this->belongsTo('App\Models\SemCorp\SiteStatus', 'status_id', 'status_id');
        //                                                          foreign      primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_sites
Columns:
site_id bigint AI PK
project_id bigint
group_id bigint
site_code varchar(50)
site_name varchar(300)
site_leader bigint
site_country varchar(100)
site_address varchar(200)
site_lead_contact varchar(50)
site_progress int
site_module_quantity decimal(12,2)
site_module_capacity decimal(12,2)
site_total_capacity decimal(12,2)
site_pr_capacity int
site_type_of_tension varchar(100)
site_approved_load varchar(100)
site_mssl_account_number varchar(100)
site_switchroom_tie varchar(100)
site_plant_name varchar(200)
site_folder_in_sftp_server varchar(600)
site_monitoring_application varchar(100)
site_total_inverter int
site_logger_type varchar(100)
site_total_data_logger varchar(100)
site_start_date datetime
site_end_date datetime
site_contractor_start_date datetime
site_contractor_end_date datetime
site_elu_end_date datetime
site_el_expiry_date datetime
site_elu_license_number varchar(100)
site_elu_status varchar(100)
milestone_id bigint
status_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
