<?php

namespace App\Models\SemCorp;

use Illuminate\Database\Eloquent\Model;

class ProjectSCurve extends Model
{
    protected $table = "v_project_s_curves_display";

    protected $fillable = [ ];

    protected $dates = [ ];

    public function _project()
    {
        return $this->belongsTo('App\Models\SemCorp\Project', 'project_id', 'project_id');
        //                                                      foreign      primary
    }

    // public function _created_by()
    // {
    //     return $this->belongsTo('App\Models\User', 'created_by', 'id');
    //     //                                          foreign      primary
    // }

}

/*
Table: project_groups
Columns:
group_id bigint AI PK
project_id bigint
group_code varchar(50)
group_name varchar(300)
group_engineer bigint
group_information varchar(300)
group_location varchar(300)
group_total_installation int
group_total_task int
group_total_document int
group_progress int
status_id bigint
created_by bigint
created_at datetime
updated_at datetime
*/
