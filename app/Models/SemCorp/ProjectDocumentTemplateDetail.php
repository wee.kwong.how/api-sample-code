<?php

namespace App\Models\SemCorp;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ProjectDocumentTemplateDetail extends Model
{
    use SoftDeletes;
    
    protected $table = "project_document_template_details";
    protected $primaryKey = 'project_document_template_detail_id';

    protected $dates = [ ];

    public function _project_document_template()
    {
        return $this->belongsTo('App\Models\SemCorp\ProjectDocumentTemplate', 'project_document_template_id', 'project_document_template_id');
        //                                                                      foreign                         primary
    }

    public function _recurring_interval()
    {
        return $this->belongsTo('App\Models\SemCorp\RecurringInterval', 'recurring_interval_id', 'recurring_interval_id');
        //                                                              foreign               primary
    }

    public function _document_category()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentCategory', 'document_category_id', 'document_category_id');
        //                                                              foreign               primary
    }

    public function _document_type()
    {
        return $this->belongsTo('App\Models\SemCorp\DocumentType', 'document_type_id', 'document_type_id');
        //                                                              foreign               primary
    }

    public function _created_by()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
        //                                          foreign      primary
    }

}

/*
Table: project_document_template_details
Columns:
project_document_template_detail_id bigint AI PK
project_document_template_id bigint
project_document_template_title varchar(100)
project_document_template_mandatory tinyint(1)
document_type_id bigint
created_by bigint
created_at datetime
updated_at datetime
active_status tinyint(1)
*/
