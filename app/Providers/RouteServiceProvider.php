<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            // Route::prefix('user.access')
            //     ->middleware('api')
            //     ->namespace($this->namespace)
            //     ->group(base_path('routes/SaaS/UserAccess.php'));  dashboard_route.php

            Route::prefix('dashboard')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SemCorp/dashboard_route.php'));

            Route::prefix('customer')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SaaS/Customers.php'));

            Route::prefix('lookup')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SaaS/lookup_route.php'));

            Route::prefix('setup')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SemCorp/setup_route.php'));

            Route::prefix('project')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SaaS/project_route.php'));

            Route::prefix('task')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SemCorp/task_route.php'));

            Route::prefix('document')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SemCorp/document_route.php'));

            Route::prefix('template')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SemCorp/template_route.php'));

            Route::prefix('user_access')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/SemCorp/user_access_route.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(600)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
