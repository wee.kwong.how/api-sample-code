<?php

use App\Http\Controllers\SemCorp\UserAccess\RoleAccessController;


Route::group(['middleware' => ['auth:api']], function() {

    /* Role Access */

    Route::get('role_access.show_by_role/{role_id}', [RoleAccessController::class, 'showByRole']);
    Route::get('role_access.for_each_module', [RoleAccessController::class, 'userRoleForEachModule']);
    Route::get('role_access.project_access', [RoleAccessController::class, 'projectAccess']);
    Route::get('role_access.task_access', [RoleAccessController::class, 'siteAccess']);

});

