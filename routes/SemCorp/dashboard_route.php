<?php

use App\Http\Controllers\SemCorp\Dashboard\ConsolidatedDashboardController;


Route::group(['middleware' => ['auth:api']], function() {

    /* Dashboard */
    Route::get('project_summary/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'projectSummary']);
    Route::get('over_due_project/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'overDueProject']);
    Route::get('over_due_turn_on/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'overDueTurnOn']);
    Route::get('project_turn_on/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'projectTurnOn']);
    Route::get('document_summary/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'documentSummary']);
    Route::get('document_overdue/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'documentOverdue']);
    Route::get('top_overdue_project/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'topOverdueProject']);
    Route::get('top_overdue_document/{project_type_id}/{date}', [ConsolidatedDashboardController::class, 'topOverdueDocument']);

});

