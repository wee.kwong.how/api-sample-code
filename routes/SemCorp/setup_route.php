<?php

use App\Http\Controllers\SemCorp\Setup\Developer\DeveloperController;
use App\Http\Controllers\SemCorp\Setup\Contractor\ContractorController;
use App\Http\Controllers\SemCorp\Setup\Import\ImportController;
use App\Http\Controllers\SemCorp\Setup\ProjectType\ProjectTypeController;
use App\Http\Controllers\SemCorp\Setup\ProjectCategory\ProjectCategoryController;
use App\Http\Controllers\SemCorp\Setup\User\UserController;
use App\Http\Controllers\SemCorp\Setup\Role\RoleController;

Route::group(['middleware' => ['auth:api']], function() {

    # Developer
    Route::apiResource('developer', DeveloperController::class);

    # Contractor
    Route::apiResource('contractor', ContractorController::class);
    Route::get('contractor.not_in_site_contractor/{side_id}', [ContractorController::class, 'showContractorNotInSiteContractor']);

    # import to site
    Route::post('import', [ImportController::class, 'importToSite']);

    # Project Type
    Route::apiResource('project_type', ProjectTypeController::class);

    # Project Category
    Route::apiResource('project_category', ProjectCategoryController::class);

    # User
    Route::apiResource('user', UserController::class);

    # User
    Route::apiResource('role', RoleController::class);

});

