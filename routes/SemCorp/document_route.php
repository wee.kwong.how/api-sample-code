<?php

use App\Http\Controllers\SemCorp\Document\DocumentController;
use App\Http\Controllers\SemCorp\Document\DocumentDetailsController;
use App\Http\Controllers\SemCorp\Document\DocumentCommentController;
use App\Http\Controllers\SemCorp\ProjectDocument\ProjectDocumentController;
use App\Http\Controllers\SemCorp\ProjectDocument\ProjectDocumentUploadsController;
use App\Http\Controllers\SemCorp\ProjectDocument\ProjectDocumentApprovalController;
use App\Http\Controllers\SemCorp\ProjectDocument\ProjectDocumentRecurringController;

use App\Http\Controllers\SemCorp\SiteDocument\SiteDocumentController;
use App\Http\Controllers\SemCorp\SiteDocument\SiteDocumentRecurringController;
use App\Http\Controllers\SemCorp\SiteDocument\SiteDocumentUploadsController;
use App\Http\Controllers\SemCorp\SiteDocument\SiteDocumentApprovalController;

Route::group(['middleware' => ['auth:api']], function() {

    /* document */
    Route::apiResource('document', DocumentController::class);
    Route::get('document.show_by_site/{site_id}', [DocumentController::class, 'showdocumentBySite']);
    Route::get('document.show_by_group/{group_id}', [DocumentController::class, 'showdocumentByGroup']);
    Route::get('document.show_by_project/{project_id}', [DocumentController::class, 'showdocumentProject']);
    

    /* document detail*/
    Route::apiResource('document_detail', DocumentDetailsController::class);
    Route::get('document_detail.show_by_document/{document_id}', [DocumentDetailsController::class, 'showDocumentDetailByDocument']);
    Route::get('document_detail.download/{document_detail_id}', [DocumentDetailsController::class, 'downloadDocument']);

    /* document comment*/
    Route::apiResource('document_comment', DocumentCommentController::class);
    Route::get('document_comment.show_by_document_detail/{document_detail_id}', [DocumentCommentController::class, 'showDocumentCommentByDocumentDetail']);

    /* project document */
    Route::apiResource('project_document', ProjectDocumentController::class);
    Route::get('project_document.show_by_project/{project_id}', [ProjectDocumentController::class, 'showDocumentbyProject']);
    Route::apiResource('project_document_upload', ProjectDocumentUploadsController::class);
    Route::get('project_document_upload.show_by_document/{project_document_id}', [ProjectDocumentUploadsController::class, 'showUploadsByDocument']);
    Route::get('project_document_upload.show_by_document_recurring/{project_document_recurring_id}', [ProjectDocumentUploadsController::class, 'showUploadsByDocumentRecurring']);
    Route::get('project_document_upload.download/{project_document_upload_id}', [ProjectDocumentUploadsController::class, 'downloadDocument']);
    Route::get('project_document_upload.check_allow_upload/{project_document_id}/{document_type_id}', [ProjectDocumentUploadsController::class, 'checkAllowUpload']);
    Route::get('project_document.check_asb_flag/{project_document_id}', [ProjectDocumentController::class, 'checkAllowChangeASBFlag']);

    Route::apiResource('project_document_approval', ProjectDocumentApprovalController::class);
    Route::get('project_document_approval.show_by_upload/{project_document_upload_id}', [ProjectDocumentApprovalController::class, 'showApprovalByDocument']);
    Route::get('project_document_approval.download/{project_document_approval_id}', [ProjectDocumentApprovalController::class, 'downloadDocument']);
    Route::get('project_document_approval.send_mail/{project_document_id}', [ProjectDocumentApprovalController::class, 'projectDocumentSendEmailChangeStatus']);

    Route::apiResource('project_document_recurring', ProjectDocumentRecurringController::class);
    Route::get('project_document_recurring.show_by_document/{project_document_id}', [ProjectDocumentRecurringController::class, 'showRecurringByDocument']);


    /* Site document */
    Route::apiResource('site_document', SiteDocumentController::class);
    Route::get('site_document.show_by_user/{user_id}', [SiteDocumentController::class, 'showDocumentbyUser']);
    Route::get('site_document.show_by_site/{site_id}', [SiteDocumentController::class, 'showDocumentbySite']);
    Route::get('site_document.show_by_project/{project_id}', [SiteDocumentController::class, 'showDocumentbyProject']);
    Route::get('site_document.show_by_group/{group_id}', [SiteDocumentController::class, 'showDocumentbyGroup']);
    Route::get('site_document.check_asb_flag/{site_document_id}', [SiteDocumentController::class, 'checkAllowChangeASBFlag']);

    Route::apiResource('site_document_recurring', SiteDocumentRecurringController::class);
    Route::get('site_document_recurring.show_by_document/{site_document_id}', [SiteDocumentRecurringController::class, 'showRecurringByDocument']);

    Route::apiResource('site_document_upload', SiteDocumentUploadsController::class);
    Route::get('site_document_upload.show_by_document/{site_document_id}', [SiteDocumentUploadsController::class, 'showUploadsByDocument']);
    Route::get('site_document_upload.show_by_document_recurring/{site_document_recurring_id}', [SiteDocumentUploadsController::class, 'showUploadsByDocumentRecurring']);
    Route::get('site_document_upload.download/{site_document_upload_id}', [SiteDocumentUploadsController::class, 'downloadDocument']);
    Route::get('site_document_upload.check_allow_upload/{site_document_id}/{document_type_id}', [SiteDocumentUploadsController::class, 'checkAllowUpload']);

    Route::apiResource('site_document_approval', SiteDocumentApprovalController::class);
    Route::get('site_document_approval.show_by_upload/{site_document_upload_id}', [SiteDocumentApprovalController::class, 'showApprovalByDocument']);
    Route::get('site_document_approval.download/{site_document_approval_id}', [SiteDocumentApprovalController::class, 'downloadDocument']);
    Route::get('site_document_approval.send_mail/{site_document_id}', [SiteDocumentApprovalController::class, 'siteDocumentSendEmailChangeStatus']);

});

