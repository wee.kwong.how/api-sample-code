<?php

use App\Http\Controllers\SemCorp\Task\TaskController;
use App\Http\Controllers\SemCorp\ProjectTask\ProjectTaskController;

Route::group(['middleware' => ['auth:api']], function() {

    /* task */
    Route::apiResource('task', TaskController::class);
    Route::get('task.show_by_user/{user_id}', [TaskController::class, 'showTaskByUser']);
    Route::get('task.show_by_site/{site_id}', [TaskController::class, 'showTaskBySite']);
    Route::get('task.show_by_group/{group_id}', [TaskController::class, 'showTaskByGroup']);
    Route::get('task.show_by_project/{project_id}', [TaskController::class, 'showTaskProject']);
    Route::get('task.send_mail/{task_id}', [TaskController::class, 'siteTaskSendEmailChangeStatus']);
    Route::post('task.update/{task_id}', [TaskController::class, 'updateTaskRecord']);
    Route::get('task.download/{task_id}', [TaskController::class, 'downloadDocument']);

    /* task */
    Route::apiResource('project_task', ProjectTaskController::class);
    Route::get('project_task.show_by_project/{project_id}', [ProjectTaskController::class, 'showTaskByProject']);
    Route::get('project_task.send_mail/{task_id}', [ProjectTaskController::class, 'projectTaskSendEmailChangeStatus']);
    Route::post('project_task.update/{task_id}', [ProjectTaskController::class, 'updateTaskRecord']);
    Route::get('project_task.download/{task_id}', [ProjectTaskController::class, 'downloadDocument']);
    

});

