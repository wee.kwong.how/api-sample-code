<?php

use App\Http\Controllers\SaaS\Setup\CompanyController;
use App\Http\Controllers\SaaS\Setup\DeveloperController;
use App\Http\Controllers\SaaS\Setup\ProjectController;
use App\Http\Controllers\SaaS\Setup\ClusterController;
use App\Http\Controllers\SaaS\Setup\BuildingController;
use App\Http\Controllers\SaaS\Setup\DepartmentController;
use App\Http\Controllers\SaaS\Setup\FloorController;
use App\Http\Controllers\SaaS\Setup\UnitController;
// use App\Http\Controllers\SaaS\Setup\MilestoneTemplateController;
// use App\Http\Controllers\SaaS\Setup\MilestoneTemplateDetailController;

Route::group(['middleware' => ['auth:api']], function() {

    /* company */
    Route::apiResource('company', CompanyController::class);

    /* developer */
    Route::apiResource('developer', DeveloperController::class);

    /* project */
    Route::apiResource('project', ProjectController::class);

    /* cluster */
    Route::apiResource('cluster', ClusterController::class);

    /* building */
    Route::apiResource('building', BuildingController::class);

    /* department */
    Route::apiResource('department', DepartmentController::class);

    /* floor */
    Route::apiResource('floor', FloorController::class);

    /* unit */
    Route::apiResource('unit', UnitController::class);

    // /* milestone template */
    // Route::apiResource('milestone_template', MilestoneTemplateController::class);

    // /* milestone template details*/
    // Route::apiResource('milestone_template_details', MilestoneTemplateDetailController::class);
    // Route::get('milestone_template_details.show_by_template/{template}', [MilestoneTemplateDetailController::class, 'showByTemplate']);

});

