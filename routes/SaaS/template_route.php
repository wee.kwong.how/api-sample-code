<?php

use App\Http\Controllers\SemCorp\Template\Milestone\MilestoneTemplateController;
use App\Http\Controllers\SemCorp\Template\Milestone\MilestoneTemplateDetailController;
use App\Http\Controllers\SemCorp\Template\Document\DocumentTemplateController;
use App\Http\Controllers\SemCorp\Template\Document\DocumentTemplateDetailController;

use App\Http\Controllers\SemCorp\Template\Task\TaskTemplateController;
use App\Http\Controllers\SemCorp\Template\Task\TaskTemplateDetailController;

Route::group(['middleware' => ['auth:api']], function() {

    /* milestone template */
    Route::apiResource('milestone_template', MilestoneTemplateController::class);

    /* milestone template details*/
    Route::apiResource('milestone_template_details', MilestoneTemplateDetailController::class);
    Route::get('milestone_template_details.show_by_template/{template}', [MilestoneTemplateDetailController::class, 'showByTemplate']);

    /* document template */
    Route::apiResource('document_template', DocumentTemplateController::class);
    Route::get('document_template.detail_count/{template}', [DocumentTemplateController::class, 'documentDetailCount']);
    Route::get('document_template.milestone_count/{template}', [DocumentTemplateController::class, 'milestoneDetailCount']);

    /* document template details*/
    Route::apiResource('document_template_details', DocumentTemplateDetailController::class);
    Route::get('document_template_details.show_by_template/{template}', [DocumentTemplateDetailController::class, 'showByTemplate']);

    /* task template */
    Route::apiResource('task_template', TaskTemplateController::class);
    Route::get('task_template.detail_count/{template}', [TaskTemplateController::class, 'taskDetailCount']);
    Route::get('task_template.milestone_count/{template}', [TaskTemplateController::class, 'milestoneDetailCount']);

    /* document template details*/
    Route::apiResource('task_template_details', TaskTemplateDetailController::class);
    Route::get('task_template_details.show_by_template/{template}', [TaskTemplateDetailController::class, 'showByTemplate']);
});

