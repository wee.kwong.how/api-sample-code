<?php

use App\Http\Controllers\SaaS\Customer\CustomerController;
use App\Http\Controllers\SaaS\Customer\SubscriptionAccountController;

Route::group(['middleware' => ['auth:api']], function() {

    /* Customer */
    Route::apiResource('customer', CustomerController::class);

    // /* Subscription Account */
    Route::apiResource('account', SubscriptionAccountController::class);
    Route::get('account.show_by_customer/{customer}', [SubscriptionAccountController::class, 'showByCustomer']);


});

