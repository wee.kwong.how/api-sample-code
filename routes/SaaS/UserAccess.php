<?php

use App\Http\Controllers\SaaS\UserAccess\RoleController;
use App\Http\Controllers\SaaS\UserAccess\ModuleController;
//use App\Http\Controllers\SaaS\UserAccess\RoleAccessController;

Route::group(['middleware' => ['auth:api']], function() {

    /* User Role */
	Route::apiResource('user_role', RoleController::class);

    /* Modules */
    Route::apiResource('module', ModuleController::class);

    /* Role Access */
    //Route::apiResource('role_access', RoleAccessController::class);
    //Route::get('role_access.show_by_role/{role}', [RoleAccessController::class, 'showByRole']);

});

