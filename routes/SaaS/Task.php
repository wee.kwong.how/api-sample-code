<?php

use App\Http\Controllers\SaaS\Task\Setup\TaskMileStoneModeController;
use App\Http\Controllers\SaaS\Task\Setup\TaskStatusController;
use App\Http\Controllers\SaaS\Task\Setup\TaskCategoryController;
use App\Http\Controllers\SaaS\Task\Setup\TaskPriorityController;
use App\Http\Controllers\SaaS\Task\Setup\TaskAttachmentStatusController;
use App\Http\Controllers\SaaS\Task\Task\TaskController;

Route::group(['middleware' => ['auth:api']], function() {

    /* task_milestone_mode */
    Route::apiResource('task_milestone_mode', TaskMileStoneModeController::class);

    /* task_status */
    Route::apiResource('task_status', TaskStatusController::class);

    /* task_category */
    Route::apiResource('task_category', TaskCategoryController::class);

    /* task_priority */
    Route::apiResource('task_priority', TaskPriorityController::class);

    /* task_attachment_status */
    Route::apiResource('task_attachment_status', TaskAttachmentStatusController::class);

    /* task */
    Route::apiResource('task', TaskController::class);
});

