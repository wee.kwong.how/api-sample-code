<?php

use App\Http\Controllers\SaaS\Lookup\ConfigTypeController;
use App\Http\Controllers\SaaS\Lookup\StatusController;
use App\Http\Controllers\SemCorp\Lookup\DocumentTypeController;
use App\Http\Controllers\SemCorp\Lookup\ProjectStatusController;
use App\Http\Controllers\SemCorp\Lookup\UserListController;
use App\Http\Controllers\SemCorp\Lookup\GroupStatusController;
use App\Http\Controllers\SemCorp\Lookup\SiteStatusController;
use App\Http\Controllers\SemCorp\Lookup\TaskStatusController;
use App\Http\Controllers\SemCorp\Lookup\DocumentStatusController;
use App\Http\Controllers\SemCorp\Lookup\DocumentCategoryController;
use App\Http\Controllers\SemCorp\Lookup\RoleListController;

Route::group(['middleware' => ['auth:api']], function() {

    /* ConfigType */
    Route::apiResource('config_type', ConfigTypeController::class);

    /* Status */
    Route::apiResource('status', StatusController::class);

    /* Document Type */
    Route::apiResource('document_type', DocumentTypeController::class);

    /* Project Status */
    Route::apiResource('project_status', ProjectStatusController::class);

    /* User List */
    Route::get('user_list/{userType}', [UserListController::class, 'userList']);
    Route::get('user_list', [UserListController::class, 'userListALL']);
    Route::get('user_list.project_group', [UserListController::class, 'userListProjectGroup']);

    Route::get('user_list_with_project_contractor/{project_id}/{contractor_id}', [UserListController::class, 'projectUserListWithContractor']);
    Route::get('user_list_with_project/{project_id}', [UserListController::class, 'projectUserList']);
    Route::get('user_list.team_member/{project_id}', [UserListController::class, 'getProjectTeamMember']);

    /* Group Status */
    Route::apiResource('group_status', GroupStatusController::class);

    /* Site Status */
    Route::apiResource('site_status', SiteStatusController::class);

    /* Task Status */
    Route::apiResource('task_status', TaskStatusController::class);
    Route::get('task_status.normal', [TaskStatusController::class, 'taskStatusNormal']);

    /* Document Status */
    Route::apiResource('document_status', DocumentStatusController::class);

    /* Document Status */
    Route::apiResource('document_category', DocumentCategoryController::class);

    Route::get('role_list', [RoleListController::class, 'roleListALL']);

});

