<?php

use App\Http\Controllers\SemCorp\Project\ProjectController;
use App\Http\Controllers\SemCorp\Project\ProjectGroupController;
use App\Http\Controllers\SemCorp\Project\ProjectSiteController;
use App\Http\Controllers\SemCorp\Project\ProjectMilestoneController;
use App\Http\Controllers\SemCorp\Project\SiteContractorController;

Route::group(['middleware' => ['auth:api']], function() {

    /* project */
    Route::apiResource('project', ProjectController::class);
    Route::get('project.s_curve/{project_id}', [ProjectController::class, 'projectSCurve']);
    Route::get('project.consolidated/{project_id}', [ProjectController::class, 'consolidated']);
    Route::get('project.test_get_owner', [ProjectController::class, 'testGetOwner']);
    Route::get('project.send_email/{project_id}', [ProjectController::class, 'projectSendEmailChangeStatus']);

    /* project group */
    Route::apiResource('project_group', ProjectGroupController::class);
    Route::get('project_group.show_by_project/{project_id}', [ProjectGroupController::class, 'showGroupByProject']);
    Route::get('project_group.send_email/{group_id}', [ProjectGroupController::class, 'groupSendEmailChangeStatus']);

    /* project Site */
    Route::apiResource('project_site', ProjectSiteController::class);
    Route::get('project_site.show_by_project/{project_id}', [ProjectSiteController::class, 'showSiteByProject']);
    Route::get('project_site.show_by_group/{project_id}', [ProjectSiteController::class, 'showSiteByGroup']);
    Route::get('project_site.send_email/{site_id}', [ProjectSiteController::class, 'siteSendEmailChangeStatus']);

    /* project milestone */
    Route::apiResource('project_milestone', ProjectMilestoneController::class);
    Route::get('project_milestone.show_by_project/{project_id}', [ProjectMilestoneController::class, 'showMilestoneByProject']);

    /* Site Contractor */
    Route::apiResource('site_contractor', SiteContractorController::class);
    Route::get('site_contractor.show_by_site/{site_id}', [SiteContractorController::class, 'showSiteContractorBySite']);
    Route::get('site_contractor.show_by_group/{group_id}', [SiteContractorController::class, 'showSiteContractorByGroup']);
    Route::get('site_contractor.show_by_project/{project_id}', [SiteContractorController::class, 'showSiteContractorByProject']);
});

