<?php

use App\Http\Controllers\SemCorp\Setup\Developer\DeveloperController;
use App\Http\Controllers\SemCorp\Setup\Contractor\ContractorController;
use App\Http\Controllers\SemCorp\Setup\Import\ImportController;

Route::group(['middleware' => ['auth:api']], function() {

    # Developer
    Route::apiResource('developer', DeveloperController::class);

    # Contractor
    Route::apiResource('contractor', ContractorController::class);
    Route::get('contractor.not_in_site_contractor/{side_id}', [ContractorController::class, 'showContractorNotInSiteContractor']);

    # import to site
    Route::post('import', [ImportController::class, 'importToSite']);

});

