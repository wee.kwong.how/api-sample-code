<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaaS\AuthController;
use App\Http\Controllers\SaaS\CEOController;
use App\Http\Controllers\SaaS\UserAccess\RoleController;
use App\Http\Controllers\MyProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);


Route::middleware('auth:api')->get('me', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['auth:api']], function() {
	Route::apiResource('my_profile', MyProfileController::class);
	// Route::get('ceo', [CEOController::class, 'index']);
	// Route::post('store', [CEOController::class, 'store']);

});

Route::get('test', function () {
    return response([1,2,3,4], 200);
});

// /** All User Access & Role Routes */
// require base_path('routes/UserAccess.php');

// /** All Customer Related Routes */
// require base_path('routes/Customers.php');
